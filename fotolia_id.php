<?php
session_start();
$v_sval = 1;

include 'constants.php';
include 'config.php';
include 'connect.php';
include 'functions/index.php';



date_default_timezone_set($v_server_timezone);
add_class('cls_output');
$cls_output = new cls_output($db);

$fotolia_id = 65023330;

require_once ROOT_DIR.'/api/fotolia/fotolia-api.php';
ini_set('max_execution_time', 240);

$api = new Fotolia_Api(DESIGN_FOTOLIA_KEY);
$arr_licenses_results = $api->getMediaData($fotolia_id, 400);
$arr_licenses_details = array();

$v_max_price = 0;
$v_max_license = '';
foreach ($arr_licenses_results['licenses'] as $licenseInfo) {
    $arr_license_details = $arr_licenses_results['licenses_details'][$licenseInfo['name']];
    if($licenseInfo['price']>$v_max_price){
        $v_max_price = $licenseInfo['price'];
        $v_max_license = $licenseInfo['name'];
    }
    array_push($arr_licenses_details, array("name" => $licenseInfo['name'], "price" => $licenseInfo['price'], "width" => $arr_license_details['width'], "height" => $arr_license_details['height']));
}

echo json_encode($arr_licenses_results);
echo '<br><br><br><br><br>';
echo json_encode($arr_licenses_results['licenses']);
echo '<br><br><br><br><br>';
echo json_encode($arr_licenses_details);

include 'disconnect.php';