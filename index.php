<?php
session_start();
$v_sval = 1;
$v_action = isset($_GET['a'])?$_GET['a']:'';

include 'constants.php';
include 'config.php';
include 'connect.php';
include 'functions/index.php';



date_default_timezone_set($v_server_timezone);

add_class("Browser");
$browser = new Browser();

add_class('cls_settings');
$cls_settings = new cls_settings($db, LOG_DIR);
add_class('cls_output');
$cls_output = new cls_output($db);

$arr_return = array('success'=>0, 'message'=>'Unknown request', 'data' => array());

$arr_user = create_user();
$arr_exclude = array('HM', 'API', 'DL');
if(!in_array($v_action, $arr_exclude)){
    if(!isset($arr_user['user_login']) || $arr_user['user_login']==0)
        $v_action = 'LI';
    else{
        if($v_action =='') $v_action = 'HO';
    }
}

switch($v_action){
    case 'API':
        include 'sources/output/index.php';
        break;
    case 'LO':
        include 'user_logout/index.php';
        break;
    case 'ACC':
        include 'user_account/index.php';
        break;
    case 'HO':
    case 'LI':
        include 'user_login/index.php';
        break;
    case 'HM':
        include 'sources/home/index.php';
        break;
    case 'DL':
        include 'sources/output/data/index.php';
        break;
    default:
        $cls_output->output($arr_return);
        break;
}

include 'disconnect.php';