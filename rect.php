<?php

// Draw a simple rectangle or three for the newbies.
// I'm trying to comment these as best I can for a non-OOP person.
// commets or criticism are welcome. Gary Melander

$image = new Imagick();    // Create a new instance an $image class

$width =  600;        // Some necessary dimensions
$height = 400;

// $image class now inherits some attributes. i.e. Dimensions, bkgcolor...
$image->newImage( $width, $height, new ImagickPixel( 'lightgray' ) );

$draw = new ImagickDraw();    //Create a new drawing class (?)

$draw->setFillColor('wheat');    // Set up some colors to use for fill and outline
$draw->setStrokeColor( new ImagickPixel( 'green' ) );
$draw->rectangle( 100, 100, 200, 200 );    // Draw the rectangle 

// Lets draw another
$draw->setFillColor('navy');    // Set up some colors to use for fill and outline
$draw->setStrokeColor( new ImagickPixel( 'yellow' ) );
$draw->setStrokeWidth(4);
$draw->rectangle( 150, 225, 350, 300 );    // Draw the rectangle 

// and another
$draw->setFillColor('magenta');    // Set up some colors to use for fill and outline
$draw->setStrokeColor( new ImagickPixel( 'cyan' ) );
$draw->setStrokeWidth(2);
$draw->rectangle( 380, 100, 400, 350 );    // Draw the rectangle 

$image->drawImage( $draw );    // Apply the stuff from the draw class to the image canvas

$image->setImageFormat('jpg');    // Give the image a format

header('Content-type: image/jpeg');     // Prepare the web browser to display an image
echo $image;                // Publish it to the world!

//$image->writeImage('someimage.jpg");    // ...Or just write it to a file...

?>