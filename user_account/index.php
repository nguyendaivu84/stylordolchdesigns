<?php
if(!isset($v_sval)) die();
if(!isset($arr_user['user_login']) || $arr_user['user_login']==0) die();
$v_user_type = $arr_user['user_type'];
$v_user_status = $arr_user['user_status'];
if($v_user_status==1) die('Your account is locked! Click <a href="logout/">here</a> to continue!');
add_class('cls_tb_user');
$cls_tb_user = new cls_tb_user($db);

add_class('cls_tb_company');
$cls_tb_company = new cls_tb_company($db);

/*Login by User */
switch($v_user_type){
    case 0:
    case 1:
    case 2:
        $v_template = 'user_account/admin/';
        include 'user_account/admin/index.php';
        break;
    case 3:
    case 4:
    case 5:
        $v_head  = 'user_account/customer/'.$v_company_code.'/';
        $v_dir  = 'user_account/customer/'.$v_company_code;
        if(!file_exists($v_dir)){
            $v_head  = 'user_account/customer/company/';
            $v_company_codes = 'company';
            include 'user_account/customer/company/index.php';
        }else
            include 'user_account/customer/'.$v_company_code.'/index.php';
        break;
}

?>