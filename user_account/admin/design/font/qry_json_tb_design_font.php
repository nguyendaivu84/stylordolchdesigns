<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_font_name = isset($_POST['txt_search_font_name'])?$_POST['txt_search_font_name']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
if($v_search_font_name!='') $arr_where_clause['font_name'] = new MongoRegex('/'.$v_search_font_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
if(count($arr_sort)==0) $arr_sort = array('font_name'=>1);
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_design_font_redirect']) && $_SESSION['ss_tb_design_font_redirect']==1){
	if(isset($_SESSION['ss_tb_design_font_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_design_font_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_design_font_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_design_font_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_design_font_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_fonts->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_design_font_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_design_font_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_design_font_page'] = $v_page;
$_SESSION['ss_tb_design_font_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_design_font = $cls_fonts->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_design_font as $arr){
	$v_font_id = isset($arr['font_id'])?$arr['font_id']:0;
	$v_font_name = isset($arr['font_name'])?$arr['font_name']:'';
	$v_font_key = isset($arr['font_key'])?$arr['font_key']:'';
	$v_font_regular = isset($arr['font_regular'])?$arr['font_regular']:0;
	$v_font_bold = isset($arr['font_bold'])?$arr['font_bold']:0;
	$v_font_italic = isset($arr['font_italic'])?$arr['font_italic']:0;
	$v_font_both = isset($arr['font_both'])?$arr['font_both']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_font_status = isset($arr['font_status'])?$arr['font_status']:0;
	$v_font_order = isset($arr['font_order'])?$arr['font_order']:0;
	$v_font_embed = isset($arr['font_embed'])?$arr['font_embed']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_font_desc = isset($arr['font_desc'])?$arr['font_desc']:'';
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'font_id' => $v_font_id,
		'font_name' => $v_font_name,
		'font_key' => $v_font_key,
		'font_regular' => $v_font_regular,
		'font_bold' => $v_font_bold==1,
		'font_italic' => $v_font_italic==1,
		'font_embed' => $v_font_embed==1,
		'font_both' => $v_font_both==1,
		'created_time' => date('d-M-Y',$v_created_time->sec),
		'font_status' => $v_font_status==0,
		'font_order' => $v_font_order,
		'location_id' => $v_location_id,
		'company_id' => $v_company_id,
		'user_id' => $v_user_id,
		'user_name' => $v_user_name,
		'user_ip' => $v_user_ip,
		'user_agent' => $v_user_agent,
		'font_desc' => $v_font_desc
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_font'=>$arr_ret_data);
echo json_encode($arr_return);
?>