<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_font_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_font_sort'])){
	$v_sort = $_SESSION['ss_tb_design_font_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_font = $cls_tb_design_font->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_design_font_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Design_font')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Design_font');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Key', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Regular', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Bold', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Italic', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Both', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Created Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Order', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Ip', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Agent', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Font Desc', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_design_font as $arr){
	$v_excel_col = 1;
	$v_font_id = isset($arr['font_id'])?$arr['font_id']:0;
	$v_font_name = isset($arr['font_name'])?$arr['font_name']:'';
	$v_font_key = isset($arr['font_key'])?$arr['font_key']:'';
	$v_font_regular = isset($arr['font_regular'])?$arr['font_regular']:0;
	$v_font_bold = isset($arr['font_bold'])?$arr['font_bold']:0;
	$v_font_italic = isset($arr['font_italic'])?$arr['font_italic']:0;
	$v_font_both = isset($arr['font_both'])?$arr['font_both']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_font_status = isset($arr['font_status'])?$arr['font_status']:0;
	$v_font_order = isset($arr['font_order'])?$arr['font_order']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_font_desc = isset($arr['font_desc'])?$arr['font_desc']:'';
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_key, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_regular, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_bold, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_italic, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_both, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_created_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_order, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_ip, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_agent, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_font_desc, 'left');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>