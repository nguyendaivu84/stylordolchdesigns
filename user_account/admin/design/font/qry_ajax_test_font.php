<?php
if(!isset($v_sval)) die();

$v_upload = 0;
$v_success = 0;
$v_name = '';
$v_message = '';
if(!empty($_FILES['txt_test_font'])){
    $v_upload_dir = DESIGN_TEMP_DIR;
    add_class('cls_upload');
    $up = cls_upload::factory($v_upload_dir);
    $up->file($_FILES['txt_test_font']);

    $arr_return = $up->upload();
    $v_file_name = $up->get_original_name();
    if($arr_return['status']){
        $v_current_name = $up->get_current_name();
        add_class('cls_font_attribute');
        $v_font = $v_upload_dir.DS.$v_current_name;
        if(file_exists($v_font)){
            $cls_font_attribute = new cls_font_attribute($v_font);
            $cls_font_attribute->readFontAttributes();
            $v_name = $cls_font_attribute->getFontFamily();
            $v_upload = 1;
            $v_success = $v_name!=''?1:0;
            @unlink($v_font);
        }else{
            $v_message = 'File: "'.$v_file_name.'" is not uploaded successful, but it not be found???';
        }
    }else{
        $v_message = 'File: "'.$v_file_name.'" is not uploaded successful!';
    }
}else{
    $v_message = 'The input-field: "test_font" is empty!';
}
$arr_return = array(
    'upload'=>$v_upload, 'success'=>$v_success, 'name'=>$v_name, 'message'=>$v_message
);
$cls_output->output($arr_return, true, false);
//header('Content-Type: application/json');
//die(json_encode($arr_return));