<?php
if(!isset($v_sval)) die();?>
<?php
$v_font_id = isset($_POST['txt_font_id'])?$_POST['txt_font_id']:'0';
$v_font_name = isset($_POST['txt_font_name'])?$_POST['txt_font_name']:'';
$v_font_name = strtolower($v_font_name);

settype($v_font_id, 'int');

$arr_return = array('success'=>0, 'message'=>'OK');

if($v_font_name!=''){
    $v_font_name = trim($v_font_name);
    $arr_where_clause = array();
    $arr_where_clause['font_name'] = new MongoRegex('/'.$v_font_name.'/i');
    if($v_font_id>0) $arr_where_clause['font_id'] =array('$ne'=>$v_font_id);
    $v_count = $cls_fonts->count($arr_where_clause);
    if($v_count==0)
        $arr_return['success'] = 1;
}
header("Content-type: application/json");
echo json_encode($arr_return);
?>