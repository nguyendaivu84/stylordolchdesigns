<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_font_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_font_sort'])){
	$v_sort = $_SESSION['ss_tb_design_font_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_font = $cls_tb_design_font->select($arr_where_clause, $arr_sort);
$v_dsp_tb_design_font = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_design_font .= '<tr align="center" valign="middle">';
$v_dsp_tb_design_font .= '<th>Ord</th>';
$v_dsp_tb_design_font .= '<th>Font Id</th>';
$v_dsp_tb_design_font .= '<th>Font Name</th>';
$v_dsp_tb_design_font .= '<th>Font Key</th>';
$v_dsp_tb_design_font .= '<th>Font Regular</th>';
$v_dsp_tb_design_font .= '<th>Font Bold</th>';
$v_dsp_tb_design_font .= '<th>Font Italic</th>';
$v_dsp_tb_design_font .= '<th>Font Both</th>';
$v_dsp_tb_design_font .= '<th>Created Time</th>';
$v_dsp_tb_design_font .= '<th>Font Status</th>';
$v_dsp_tb_design_font .= '<th>Font Order</th>';
$v_dsp_tb_design_font .= '<th>Location Id</th>';
$v_dsp_tb_design_font .= '<th>Company Id</th>';
$v_dsp_tb_design_font .= '<th>User Id</th>';
$v_dsp_tb_design_font .= '<th>User Name</th>';
$v_dsp_tb_design_font .= '<th>User Ip</th>';
$v_dsp_tb_design_font .= '<th>User Agent</th>';
$v_dsp_tb_design_font .= '<th>Font Desc</th>';
$v_dsp_tb_design_font .= '</tr>';
$v_count = 1;
foreach($arr_tb_design_font as $arr){
	$v_dsp_tb_design_font .= '<tr align="left" valign="middle">';
	$v_dsp_tb_design_font .= '<td align="right">'.($v_count++).'</td>';
	$v_font_id = isset($arr['font_id'])?$arr['font_id']:0;
	$v_font_name = isset($arr['font_name'])?$arr['font_name']:'';
	$v_font_key = isset($arr['font_key'])?$arr['font_key']:'';
	$v_font_regular = isset($arr['font_regular'])?$arr['font_regular']:0;
	$v_font_bold = isset($arr['font_bold'])?$arr['font_bold']:0;
	$v_font_italic = isset($arr['font_italic'])?$arr['font_italic']:0;
	$v_font_both = isset($arr['font_both'])?$arr['font_both']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_font_status = isset($arr['font_status'])?$arr['font_status']:0;
	$v_font_order = isset($arr['font_order'])?$arr['font_order']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_font_desc = isset($arr['font_desc'])?$arr['font_desc']:'';
	$v_dsp_tb_design_font .= '<td>'.$v_font_id.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_name.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_key.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_regular.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_bold.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_italic.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_both.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_created_time.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_status.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_order.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_location_id.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_company_id.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_user_name.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_user_ip.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_user_agent.'</td>';
	$v_dsp_tb_design_font .= '<td>'.$v_font_desc.'</td>';
	$v_dsp_tb_design_font .= '</tr>';
}
$v_dsp_tb_design_font .= '</table>';
?>