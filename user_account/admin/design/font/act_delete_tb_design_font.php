<?php if(!isset($v_sval)) die();?>
<?php
$v_font_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_font_id, 'int');
if($v_font_id>0){
    $v_row = $cls_fonts->select_one(array('font_id'=>$v_font_id));
    if($v_row==1){
        $v_font_key = $cls_fonts->get_font_key();
        $v_font_dir = DESIGN_FONT_DIR.DS.$v_font_key;
        if(file_exists($v_font_dir)){
            add_class('ManageFile','cls_file.php');
            $cls_file = new ManageFile($v_font_dir, DS);
            $cls_file->remove_dir_all();
        }
	    $cls_fonts->delete(array('font_id' => $v_font_id));
    }
}
$_SESSION['ss_tb_design_font_redirect'] = 1;
redir(URL.$v_admin_key);
?>