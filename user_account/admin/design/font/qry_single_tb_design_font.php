<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_font_id = 0;
$v_font_name = '';
$arr_font_name = array();
$arr_site_id = array();
$v_font_name_regular = '';
$v_font_name_bold = '';
$v_font_name_italic = '';
$v_font_name_both = '';
$v_font_key = '';
$v_font_regular = 0;
$v_font_bold = 0;
$v_font_italic = 0;
$v_font_both = 0;
$v_font_embed_regular = 0;
$v_font_embed_bold = 0;
$v_font_embed_italic = 0;
$v_font_embed_both = 0;
$v_created_time = date('Y-m-d H:i:s', time());
$v_font_status = 0;
$v_font_embed = 0;
$v_font_order = 0;
$v_location_id = 0;
$v_user_id = 0;
$v_user_name = '';
$v_user_ip = get_real_ip_address();
$v_user_agent = $browser->getUserAgent();
$v_font_desc = '';
$v_new_design_font = true;

$v_company_id = isset($_SESSION['ss_last_company_id'])?$_SESSION['ss_last_company_id']:0;
settype($v_company_id, 'int');


$v_font_dir = DESIGN_FONT_DIR;
add_class('cls_tb_design_key');
$cls_key = new cls_tb_design_key($db, LOG_DIR);
add_class('cls_ttf_info');
$cls_ttf_info = new cls_ttf_info();
add_class('cls_font_attribute');
$cls_font_attribute = new cls_font_attribute();
add_class('cls_upload');
$cls_upload = cls_upload::factory($v_font_dir);

$v_old_font_regular = 0;
$v_old_font_italic = 0;
$v_old_font_bold = 0;
$v_old_font_both = 0;

if(isset($_POST['btn_submit_tb_design_font'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_fonts->set_mongo_id($v_mongo_id);

    $v_font_id = isset($_POST['txt_font_id'])?$_POST['txt_font_id']:$v_font_id;
    $v_font_name = isset($_POST['txt_font_name'])?$_POST['txt_font_name']:$v_font_name;
    $v_font_name = trim($v_font_name);

    $v_font_name_regular = isset($_POST['txt_font_name_regular'])?$_POST['txt_font_name_regular']:$v_font_name_regular;
    $v_font_name_regular = trim($v_font_name_regular);

    settype($v_font_id, 'int');
    $v_tmp_font_id = $v_font_id;
    if($v_font_name == '')
        $v_message .= '[Font Name] is empty!<br />';
    else{
        $arr_where_clause = array();
        $arr_where_clause['font_name'] = new MongoRegex('/'.$v_font_name.'/i');
        if($v_font_id>0) $arr_where_clause['font_id'] = array('$ne'=>$v_font_id);
        if($cls_fonts->count($arr_where_clause)>0) $v_message.="+ Duplicate [Font Name]!<br />";
    }
    $cls_fonts->set_font_name($v_font_name);
    $v_font_key = seo_friendly_url($v_font_name);
    $cls_fonts->set_font_key($v_font_key);

	if(is_null($v_mongo_id)){
		//$v_font_id = $cls_fonts->select_next('font_id');
	}

	$v_font_id = (int) $v_font_id;
	$cls_fonts->set_font_id($v_font_id);

    if($v_font_id > 0){
        $v_row = $cls_fonts->select_one( array('font_id'=>$v_font_id));
        if($v_row==1){
            $v_old_font_name = $cls_fonts->get_font_name();
            $v_old_font_key = $cls_fonts->get_font_key();
            $v_old_font_regular = $cls_fonts->get_font_regular();
            $v_old_font_italic = $cls_fonts->get_font_italic();
            $v_old_font_bold = $cls_fonts->get_font_bold();
            $v_old_font_both = $cls_fonts->get_font_both();
            if($v_old_font_key!=$v_font_key){
                if(!file_exists($v_font_dir.DS.$v_font_key)) rename($v_font_dir.DS.$v_old_font_key, $v_font_dir.DS.$v_font_key);
            }
        }
    }

    $cls_upload->set_destination($v_font_dir.DS.$v_font_key);
    $v_font_regular = 0;
    if(!empty($_FILES['txt_font_regular'])){
        $cls_upload->file($_FILES['txt_font_regular']);
        $cls_upload->set_new_file_name('regular.ttf');
        $arr_result = $cls_upload->upload();
        if($arr_result['status']){
            $v_font_regular = 1;
        }
    }
    $v_font_regular |= $v_old_font_regular;
    if($v_font_regular==1){
        $v_font = $v_font_dir.DS.$v_font_key.DS.'regular.ttf';
        $cls_font_attribute = new cls_font_attribute($v_font);
        $cls_font_attribute->readFontAttributes();
        $v_font_name_regular = $cls_font_attribute->getFontFamily();
        if($v_font_name_regular=='') $v_font_name_regular = $v_font_name;
        if($v_font_name_regular=='Expressway') $v_font_name_regular .= ' Rg';
        $arr_font_name['regular'] = $v_font_name_regular;
    }
    $cls_fonts->set_font_regular($v_font_regular);

    if($v_font_regular==1){
        if(!empty($_FILES['txt_font_embed_regular'])){
            $v_font = $v_font_dir.DS.$v_font_key.DS.'regular.eot';
            if(file_exists($v_font)) @unlink($v_font);
            $cls_upload->file($_FILES['txt_font_embed_regular']);
            $cls_upload->set_new_file_name('regular.eot');
            $arr_result = $cls_upload->upload();
            $v_font_embed_regular = $arr_result['status']?1:0;
        }
    }

    $v_font_italic = 0;
    if(!empty($_FILES['txt_font_italic'])){
        $cls_upload->set_new_file_name('italic.ttf');
        $cls_upload->file($_FILES['txt_font_italic']);
        $arr_result = $cls_upload->upload();
        if($arr_result['status']){
            $v_font_italic = 1;
        }
    }
    $v_font_italic |= $v_old_font_italic;
    if($v_font_italic==1){
        $v_font = $v_font_dir.DS.$v_font_key.DS.'italic.ttf';
        $cls_font_attribute = new cls_font_attribute($v_font);
        $cls_font_attribute->readFontAttributes();
        $v_font_name_italic = $cls_font_attribute->getFontFamily();
        if($v_font_name_italic=='') $v_font_name_italic = $v_font_name;
        $arr_font_name['italic'] = $v_font_name_italic;
    }
    $cls_fonts->set_font_italic($v_font_italic);
    if($v_font_italic==1){
        if(!empty($_FILES['txt_font_embed_italic'])){
            $v_font = $v_font_dir.DS.$v_font_key.DS.'italic.eot';
            if(file_exists($v_font)) @unlink($v_font);
            $cls_upload->file($_FILES['txt_font_embed_italic']);
            $cls_upload->set_new_file_name('italic.eot');
            $arr_result = $cls_upload->upload();
            $v_font_embed_italic = $arr_result['status']?1:0;
        }
    }

    $v_font_bold = 0;
    if(!empty($_FILES['txt_font_bold'])){
        $cls_upload->file($_FILES['txt_font_bold']);
        $cls_upload->set_new_file_name('bold.ttf');
        $arr_result = $cls_upload->upload();
        if($arr_result['status']){
            $v_font_bold = 1;
        }
    }
    $v_font_bold |= $v_old_font_bold;
    if($v_font_bold==1){
        $v_font = $v_font_dir.DS.$v_font_key.DS.'bold.ttf';
        $cls_font_attribute = new cls_font_attribute($v_font);
        $cls_font_attribute->readFontAttributes();
        $v_font_name_bold = $cls_font_attribute->getFontFamily();
        if($v_font_name_bold=='') $v_font_name_bold = $v_font_name;
        $arr_font_name['bold'] = $v_font_name_bold;
    }
    $cls_fonts->set_font_bold($v_font_bold);
    if($v_font_bold==1){
        if(!empty($_FILES['txt_font_embed_bold'])){
            $v_font = $v_font_dir.DS.$v_font_key.DS.'bold.eot';
            if(file_exists($v_font)) @unlink($v_font);
            $cls_upload->file($_FILES['txt_font_embed_bold']);
            $cls_upload->set_new_file_name('bold.eot');
            $arr_result = $cls_upload->upload();
            $v_font_embed_bold = $arr_result['status']?1:0;
        }
    }

    $v_font_both = 0;
    if(!empty($_FILES['txt_font_both'])){
        $cls_upload->file($_FILES['txt_font_both']);
        $cls_upload->set_new_file_name('both.ttf');
        $arr_result = $cls_upload->upload();
        if($arr_result['status']){
            $v_font_both = 1;
        }
    }
    $v_font_both |= $v_old_font_both;
    if($v_font_both==1){
        $v_font = $v_font_dir.DS.$v_font_key.DS.'both.ttf';
        $cls_font_attribute = new cls_font_attribute($v_font);
        $cls_font_attribute->readFontAttributes();
        $v_font_name_bold = $cls_font_attribute->getFontFamily();
        if($v_font_name_both=='') $v_font_name_both = $v_font_name;
        $arr_font_name['both'] = $v_font_name_both;
    }
    $cls_fonts->set_font_both($v_font_both);
    if($v_font_both==1){
        if(!empty($_FILES['txt_font_embed_both'])){
            $v_font = $v_font_dir.DS.$v_font_key.DS.'both.eot';
            if(file_exists($v_font)) @unlink($v_font);
            $cls_upload->file($_FILES['txt_font_embed_both']);
            $cls_upload->set_new_file_name('both.eot');
            $arr_result = $cls_upload->upload();
            $v_font_embed_both = $arr_result['status']?1:0;
        }
    }

    $cls_fonts->set_array_font_name($arr_font_name);
    $cls_fonts->set_created_time(date('Y-m-d H:i:s'));

    $v_font_status = isset($_POST['txt_font_status'])?0:1;
    settype($v_font_status,"int");
    $cls_fonts->set_font_status($v_font_status);
    $v_font_embed = isset($_POST['txt_font_embed'])?1:0;
    $cls_fonts->set_font_embed($v_font_embed);
    $v_font_order = isset($_POST['txt_font_order'])?$_POST['txt_font_order']:'0';
    settype($v_font_order,"int");
    $cls_fonts->set_font_order($v_font_order);

    $arr_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:array();
    if(!is_array($arr_site_id)) $arr_site_id = array();
    for($i=0; $i<sizeof($arr_site_id);$i++)
        $arr_site_id[$i] = (int) $arr_site_id[$i];
    $cls_fonts->set_site_id($arr_site_id);

    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
    settype($v_company_id, 'int');
    $v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:'0';
    settype($v_company_id, 'int');
    if($v_company_id<0) $v_company_id = 0;
    if($v_location_id<0) $v_location_id = 0;

	$cls_fonts->set_location_id($v_location_id);
	$cls_fonts->set_company_id($v_company_id);
	$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
	$cls_fonts->set_user_id($v_user_id);
	$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:$v_user_name;
	$cls_fonts->set_user_name($v_user_name);
	$cls_fonts->set_user_ip($v_user_ip);
	$cls_fonts->set_user_agent($v_user_agent);
	$v_font_desc = isset($_POST['txt_font_desc'])?$_POST['txt_font_desc']:$v_font_desc;
	$v_font_desc = trim($v_font_desc);
	$cls_fonts->set_font_desc($v_font_desc);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_font_id = $cls_fonts->insert();
			$v_result = $v_font_id > 0;// is_object($v_mongo_id);
		}else{
			$v_result = $cls_fonts->update(array('_id' => $v_mongo_id));
			$v_new_design_font = false;
		}
		if($v_result){
            if($v_font_embed==1){
                $arr_update_font = array(
                    'regular'=>$v_font_embed_regular,
                    'italic'=>$v_font_embed_italic,
                    'bold'=>$v_font_embed_bold,
                    'both'=>$v_font_embed_both
                );
                $cls_fonts->update_upload_font_face($arr_update_font);
                //$cls_fonts->update_font_face(TTF2EOT_DIR.DS.'ttf2eot');
            }

			$_SESSION['ss_tb_design_font_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_design_font) $v_font_id = 0;
		}
	}
}else{
	$v_font_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_font_id,'int');
	if($v_font_id>0){
		$v_row = $cls_fonts->select_one(array('font_id' => $v_font_id));
		if($v_row == 1){
			$v_mongo_id = $cls_fonts->get_mongo_id();
			$v_font_id = $cls_fonts->get_font_id();
			$v_font_name = $cls_fonts->get_font_name();
			$v_font_key = $cls_fonts->get_font_key();
			$v_font_regular = $cls_fonts->get_font_regular();
			$v_font_bold = $cls_fonts->get_font_bold();
			$v_font_italic = $cls_fonts->get_font_italic();
			$v_font_both = $cls_fonts->get_font_both();
			$v_created_time = date('Y-m-d H:i:s',$cls_fonts->get_created_time());
			$v_font_status = $cls_fonts->get_font_status();
			$v_font_order = $cls_fonts->get_font_order();
			$v_location_id = $cls_fonts->get_location_id();
			$v_company_id = $cls_fonts->get_company_id();
			$v_user_id = $cls_fonts->get_user_id();
			$v_user_name = $cls_fonts->get_user_name();
			$v_user_ip = $cls_fonts->get_user_ip();
			$v_user_agent = $cls_fonts->get_user_agent();
			$v_font_desc = $cls_fonts->get_font_desc();
            $v_font_embed = $cls_fonts->get_font_embed();
            $arr_site_id = $cls_fonts->get_site_id();

            $arr_font_name = $cls_fonts->update_font_name($cls_font_attribute, 0);
            $v_font_name_regular = isset($arr_font_name['regular'])?$arr_font_name['regular']:'';
            $v_font_name_italic = isset($arr_font_name['italic'])?$arr_font_name['italic']:'';
            $v_font_name_both = isset($arr_font_name['both'])?$arr_font_name['both']:'';
            $v_font_name_bold = isset($arr_font_name['bold'])?$arr_font_name['bold']:'';

            if($v_font_embed==1){
                $v_dir = $v_font_dir.DS.$v_font_key.DS;
                $v_font_embed_regular = file_exists($v_dir.'regular.eot')?1:0;
                $v_font_embed_italic = file_exists($v_dir.'italic.eot')?1:0;
                $v_font_embed_bold = file_exists($v_dir.'bold.eot')?1:0;
                $v_font_embed_both = file_exists($v_dir.'both.eot')?1:0;
            }
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);

if($v_is_super_admin || is_administrator())
    $arr_site_where = array();
else{
    $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
    settype($v_user_id, 'int');
    $arr_site = $cls_tb_user->select_scalar('site', array('user_id'=>$v_user_id));
    if(!is_array($arr_site) || count($arr_site)==0) $arr_site = array(0);
    $arr_site_where = array('site_id'=>array('$in'=>$arr_site));
}

$arr_key = $cls_key->select($arr_site_where);
$arr_all_key = array();

foreach($arr_key as $arr){
    $v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
    $v_site_name = isset($arr['site_name'])?$arr['site_name']:'';
    $v_site_url = isset($arr['site_url'])?$arr['site_url']:'';
    $v_site_logo = isset($arr['site_logo'])?$arr['site_logo']:'';

    $arr_all_key[] = array(
        'site_id'=>$v_site_id
        ,'site_name'=>$v_site_name
        ,'site_url'=>$v_site_url
        ,'site_logo'=>$v_site_logo
    );
}


$arr_all_location = array();
$arr_all_location[] = array('location_id'=>0, 'location_name'=>'--------');
$_SESSION['ss_last_company_id'] = $v_company_id;
if($v_company_id>=0){
    $arr_location = $cls_tb_location->select(array('company_id'=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])), array('location_name'=>1));
    foreach($arr_location as $arr){
        $arr_all_location[] = array('location_id'=>$arr['location_id'], 'location_name'=>$arr['location_name']);
    }
}
?>