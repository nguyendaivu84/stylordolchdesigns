<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_font").click(function(e){
		var css = '';

		return true;
	});
	$('input#txt_created_time').kendoDatePicker({format:"dd-MMM-yyyy"});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");

    var validator = $('form#frm_design_fonts').kendoValidator().data("kendoValidator");
    $("#txt_font_order").kendoNumericTextBox({
        format: "n0",
        step: 1
    });

    var site_data = <?php echo json_encode($arr_all_key);?>;
    var site_combo = $('select#txt_site_id').width(300).kendoMultiSelect({
        dataSource:site_data,
        dataTextField: 'site_name',
        dataValueField:'site_id',
        itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="max-width: 200px" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
            '<h3>${ data.site_name }</h3>' +
            '<p>${ data.site_url }</p></div>',
        tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
            '#: data.site_name #'

    }).data("kendoMultiSelect");
    site_combo.value(<?php echo json_encode($arr_site_id);?>);

    $("select#txt_filter_site").kendoDropDownList({
        change: filter_site_changed
    });
    function filter_site_changed(){
        site_combo.options.filter = $("#txt_filter_site_value").val();
    }
    var set_search_site = function (e) {
        if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
            site_combo.search($("#txt_filter_site_value").val());
            e.preventDefault();
        }
    };
    $("button#btn_filter_site").click(set_search_site);
    $("#txt_filter_site_value").keypress(set_search_site);

    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");

    $('input#txt_font_name').focusout(function(e){
        var name = $(this).val();
        name = $.trim(name);
        if(name==''){
            $(this).val('');
            $('input#txt_hidden_font_name').val('N');
            validator.validate();
            return false;
        }
        var id = $('input#txt_font_id').val();
        $.ajax({
            url: '<?php echo URL.$v_admin_key;?>/ajax',
            type:   'POST',
            dataType:'json',
            data:   {txt_font_id: id, txt_font_name: name, txt_ajax_type:'check_font'},
            beforeSend: function(){
                $('span#sp_font_name').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_font_name').html('');
                if(data.success==1)
                    $('input#txt_hidden_font_name').val('Y');
                else
                    $('input#txt_hidden_font_name').val('');
                validator.validate();
            }
        });
        return true;
    });

    function on_success(e){
        if(e.response.upload==1 && e.response.success==1){
            $('input#txt_font_name').val(e.response.name)
        }else{
            alert(e.response.message);
        }
    }
    function on_select(e){
        if(e.files.length>1){
            e.preventDefault();
            alert('Please choose only one file!');
            return false;
        }
        var ex = e.files[0].extension.toLowerCase();
        if(ex!='.ttf'){
            e.preventDefault();
            alert('Please choose font TTF file!');
            return false;
        }
        return true;
    }

    $('input#txt_test_font').kendoUpload({
        async: {
            saveUrl:'<?php echo URL.$v_admin_key;?>/ajax',
            autoUpload:true,
            multiple: false
        }
        ,select: on_select
        ,success: on_success
    });
    function on_select_ttf(e){
        if(e.files.length>1){
            e.preventDefault();
            alert('Please choose only one file!');
            return false;
        }
        var ex = e.files[0].extension.toLowerCase();
        if(ex!='.ttf'){
            e.preventDefault();
            alert('Please choose font TTF file!');
            return false;
        }
        return true;
    }
    function on_select_eot(e){
        if(e.files.length>1){
            e.preventDefault();
            alert('Please choose only one file!');
            return false;
        }
        var ex = e.files[0].extension.toLowerCase();
        if(ex!='.eot'){
            e.preventDefault();
            alert('Please choose font EOT file!');
            return false;
        }
        return true;
    }
    $('input#txt_font_regular').kendoUpload({
        select: on_select_ttf
    });
    $('input#txt_font_bold').kendoUpload({
        select: on_select_ttf
    });
    $('input#txt_font_italic').kendoUpload({
        select: on_select_ttf
    });
    $('input#txt_font_both').kendoUpload({
        select: on_select_ttf
    });
    $('input#txt_font_embed_regular').kendoUpload({
        select: on_select_eot
    });
    $('input#txt_font_embed_bold').kendoUpload({
        select: on_select_eot
    });
    $('input#txt_font_embed_italic').kendoUpload({
        select: on_select_eot
    });
    $('input#txt_font_embed_both').kendoUpload({
        select: on_select_eot
    });
    $('.k-upload').css('width', '250px').css('float', 'left');
    //$('.k-upload').css('float', 'left');
    $('.k-upload-button span').html('Select font...');
    var location_data = <?php echo json_encode($arr_all_location);?>;
    var combo_location = $('select#txt_location_id').width(200).kendoComboBox({
        dataTextField:'location_name',
        dataValueField:'location_id',
        dataSource: location_data
    }).data('kendoComboBox');
    combo_location.value(0);
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
    <?php if(!$v_disabled_company_id){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
        $.ajax({
            url:    '<?php echo URL.$v_admin_key;?>/ajax',
            type:'POST',
            dataType:'json',
            data:{txt_company_id: company_id, txt_ajax_type:'load_location'},
            beforeSend: function(){},
            success: function(data){
                if(data.error==0){
                    var location = data.location;
                    combo_location.setDataSource(location);
                    combo_location.value(0);
                    $('form#frm_design_fonts').find('#txt_company_id').val(company_id);
                }
            }
        });

		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Font<?php echo $v_font_id>0?': '.$v_font_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_design_fonts" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_font_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_font_id" name="txt_font_id" value="<?php echo $v_font_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>TTF Font Files</li>
                        <li>Apply for sites</li>
                    </ul>

                    <div class="information div_details">
                        <?php if(($v_create_right || $v_edit_right || $v_is_super_admin )/* && ($v_font_id==0)*/){?>
                        <span class="k-required"> <strong>Note</strong>: Please select one from families of font, from which you want to upload. The system will detect font's family-name. If the system can not detect or the name is conflict with other, you can change it</span>
                            <div style="width:100%">
                                <div class="upload-section">
                                    <input name="txt_test_font" id="txt_test_font" type="file" accept="font/ttf" />
                                </div>
                            </div>
                        <?php }?>

<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr valign="top">
        <td align="right" style="width: 200px;">Location</td>
        <td  style="width:2px" align="right">&nbsp;</td>
        <td align="left">
            <select id="txt_location_id" name="txt_location_id"></select>
        </td>
    </tr>
    <tr valign="top">
        <td align="right">Fonts' Family-Name</td>
        <td align="right">&nbsp;</td>
        <td align="left"><input type="text" size="20" id="txt_font_name" name="txt_font_name" value="<?php echo $v_font_name;?>" class="text_css k-textbox" required data-required-msg="Please input Font Name" />
            <input type="hidden" id="txt_hidden_font_name" name="txt_hidden_font_name" value="<?php echo $v_font_name!=''?'Y':'N';?>" required data-required-msg="Duplicate Font Name" />
            <span id="sp_font_name"></span>
            <label id="lbl_font_name" class="k-required">(*)</label></td>
    </tr>
    <tr valign="top">
        <td align="right">Font Status</td>
        <td align="right">&nbsp;</td>
        <td align="left">
            <div class="checkbox"><input type="checkbox" id="txt_font_status" name="txt_font_status"<?php echo $v_font_status==0?' checked="checked"':'';?> /><label for="txt_font_status">Active</label></div></td>
    </tr>
    <tr valign="top">
        <td align="right">Font Order</td>
        <td align="right">&nbsp;</td>
        <td align="left"><input type="number" id="txt_font_order" name="txt_font_order" value="<?php echo $v_font_order;?>" /></td>
    </tr>
    <tr valign="top">
        <td align="right">Description</td>
        <td align="right">&nbsp;</td>
        <td align="left">
            <textarea style="width: 300px; height: 50px; padding: 5px; resize: none" class="k-textbox"><?php echo $v_font_desc;?></textarea>
        </td>
    </tr>
</table>
                    </div>
                    <div class="upload">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width: 200px;">Font Regular</td>
                                <td  style="width:2px" align="right">&nbsp;</td>
                                <td align="left">
                                    <input type="file" name="txt_font_regular" id="txt_font_regular" accept="font/ttf" />
                                    <?php if($v_font_regular==1){?>
                                        <div style="margin-left: 250px">
                                            <div class="checkbox"><input type="checkbox" name="txt_font_regular_checkbox" checked="checked" /><label></label>
                                                <img src="<?php echo $v_asset_font_url;?>name=<?php echo $v_font_name_regular;?>&key=<?php echo $v_font_key;?>&type=regular" />
                                            </div>
                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Font Italic</td>
                                <td align="right">&nbsp;</td>
                                <td align="left">
                                    <input type="file" name="txt_font_italic" id="txt_font_italic" accept="font/ttf" />
                                    <?php if($v_font_italic==1){?>
                                        <div style="margin-left: 250px">
                                            <div class="checkbox"><input type="checkbox" name="txt_font_italic_checkbox" checked="checked" /><label></label>
                                                <img src="<?php echo $v_asset_font_url;?>name=<?php echo $v_font_name_italic;?>&key=<?php echo $v_font_key;?>&type=italic" />
                                            </div>

                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Font Bold</td>
                                <td align="right">&nbsp;</td>
                                <td align="left"><input type="file" name="txt_font_bold" id="txt_font_bold" accept="font/ttf" />
                                    <?php if($v_font_bold==1){?>
                                        <div style="margin-left: 250px">
                                            <div class="checkbox"><input type="checkbox" name="txt_font_bold_checkbox" checked="checked" /><label></label>
                                                <img src="<?php echo $v_asset_font_url;?>name=<?php echo $v_font_name_bold;?>&key=<?php echo $v_font_key;?>&type=bold" />
                                            </div>
                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Font Bold-Italic</td>
                                <td align="right">&nbsp;</td>
                                <td align="left">
                                    <input type="file" name="txt_font_both" id="txt_font_both" accept="font/ttf" />
                                    <?php if($v_font_both==1){?>
                                        <div style="margin-left: 250px">
                                            <div class="checkbox"><input type="checkbox" name="txt_font_both_checkbox" checked="checked" /><label></label>
                                                <img src="<?php echo $v_asset_font_url;?>name=<?php echo $v_font_name_both;?>&key=<?php echo $v_font_key;?>&type=both" /></div>
                                        </div>
                                    <?php }?>
                                </td>
                            </tr>
                        </table>
                    </div>

    <div class="apply">
        <div class="k-required" style="margin-top: 5px; margin-bottom: 5px">
        <strong>Note: </strong> Apply only with embed font
        </div>
        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
            <tr valign="top">
                <td align="right" style="width: 200px">Filter</td>
                <td  style="width: 2px" align="right">&nbsp;</td>
                <td align="left">
                    <select id="txt_filter_site">
                        <option value="startswith">Starts with</option>
                        <option value="contains">Contains</option>
                        <option value="eq">Equal</option>
                    </select>
                    <input type="text" class="k-textbox" id="txt_filter_site_value" />
                    <button id="btn_filter_site" class="k-button">Find site</button>
                </td>
            </tr>
            <tr valign="top">
                <td align="right">Apply for Sites</td>
                <td width="0" align="right">&nbsp;</td>
                <td align="left">
                    <select id="txt_site_id" name="txt_site_id[]" multiple="multiple"></select>
                </td>
            </tr>
            <tr valign="top">
                <td align="right">Embed Font</td>
                <td align="right">&nbsp;</td>
                <td align="left">
                    <div class="checkbox"><input type="checkbox" id="txt_font_embed" name="txt_font_embed"<?php echo $v_font_embed==1?' checked="checked"':'';?> /><label for="txt_font_embed">Embed?</label></div></td>
            </tr>
            <tr valign="top">
                <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="left"><label class="k-required">Please upload EOT fonts, if you want the design will support old browsers such as IE8, IE9,...</label> </td>
            </tr>
            <tr valign="top">
                <td align="right">Font Regular</td>
                <td align="right">&nbsp;</td>
                <td align="left">
                    <input type="file" name="txt_font_embed_regular" id="txt_font_embed_regular" accept="font/eot" />
                    <?php if($v_font_embed_regular==1){?> This regular font has been uploaded. You can re-upload. <?php }?>
                </td>
            </tr>
            <tr valign="top">
                <td align="right">Font Italic</td>
                <td align="right">&nbsp;</td>
                <td align="left">
                    <input type="file" name="txt_font_embed_italic" id="txt_font_embed_italic" accept="font/eot" />
                    <?php if($v_font_embed_italic==1){?> This italic font has been uploaded. You can re-upload. <?php }?>
                </td>
            </tr>
            <tr valign="top">
                <td align="right">Font Bold</td>
                <td align="right">&nbsp;</td>
                <td align="left"><input type="file" name="txt_font_embed_bold" id="txt_font_embed_bold" accept="font/eot" />
                    <?php if($v_font_embed_bold==1){?> This bold font has been uploaded. You can re-upload. <?php }?>
                </td>
            </tr>
            <tr valign="top">
                <td align="right">Font Bold-Italic</td>
                <td align="right">&nbsp;</td>
                <td align="left">
                    <input type="file" name="txt_font_embed_both" id="txt_font_embed_both" accept="font/eot" />
                    <?php if($v_font_embed_both==1){?> This bold-italic font has been uploaded. You can re-upload. <?php }?>
                </td>
            </tr>
        </table>
                </div>


                <!--
                                    <div class="other div_details">
                                    </div>
                -->
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_font" name="btn_submit_tb_design_font" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
