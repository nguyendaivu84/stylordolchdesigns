<?php if(!isset($v_sval)) die();?>
<?php
$v_quick_search = '';
$v_search_template_name = '';
$v_company_id = isset($_SESSION['ss_last_company_id'])?$_SESSION['ss_last_company_id']:0;
$v_page = 1;
if(isset($_POST['btn_advanced_search'])){
	$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
}else if(isset($_POST['btn_advanced_reset'])){
}else{
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
	$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
    if(isset($_POST['txt_quick_search']))
        $v_quick_search = $_POST['txt_quick_search'];
    else
        $v_quick_search = isset($_SESSION['ss_tb_design_template_quick_search'])?$_SESSION['ss_tb_design_template_quick_search']:'';

	if((isset($_SESSION['ss_tb_design_template_redirect']) && $_SESSION['ss_tb_design_template_redirect']==1) || (isset($_SESSION['ss_tb_design_template']) && $_SESSION['ss_tb_design_template']==1)){
		$v_page = isset($_SESSION['ss_tb_design_template_page'])?$_SESSION['ss_tb_design_template_page']:'1';
		settype($v_page,'int');
		if($v_page<1) $v_page = 1;
		if(isset($_SESSION['ss_tb_design_template_where_clause'])){
			$arr_where_clause = unserialize($_SESSION['ss_tb_design_template_where_clause']);
			if(isset($arr_where_clause['company_id'])) $v_company_id = $arr_where_clause['company_id'];
		}
	}
}
$v_search_template_name = $v_quick_search;
$v_stop_template_id = isset($_SESSION['ss_save_tb_design_template_id'])?$_SESSION['ss_save_tb_design_template_id']:0;
settype($v_stop_template_id, 'int');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
//Add code here if required

if($v_is_super_admin || is_administrator())
    $arr_site_where = array();
else{
    $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
    settype($v_user_id, 'int');
    $arr_site = $cls_tb_user->select_scalar('site', array('user_id'=>$v_user_id));
    if(!is_array($arr_site) || count($arr_site)==0) $arr_site = array(0);
    $arr_site_where = array('site_id'=>array('$in'=>$arr_site));
}

$arr_key = $cls_key->select($arr_site_where);
$arr_all_key = array();

foreach($arr_key as $arr){
    $v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
    $v_site_name = isset($arr['site_name'])?$arr['site_name']:'';
    $v_site_url = isset($arr['site_url'])?$arr['site_url']:'';
    $v_site_logo = isset($arr['site_logo'])?$arr['site_logo']:'';

    $arr_all_key[] = array(
        'site_id'=>$v_site_id
        ,'site_name'=>$v_site_name
        ,'site_url'=>$v_site_url
        ,'site_logo'=>$v_site_logo
    );
}

$v_checked = isset($_POST['txt_checked'])?intval($_POST['txt_checked']):0;
if(isset($_POST['txt_sites'])){
    $v_sites = $_POST['txt_sites'];
    if(get_magic_quotes_gpc()) $v_sites = stripslashes($v_sites);
    $arr_sites = json_decode($v_sites, true);
    if(!is_array($arr_sites)) $arr_sites = array();
    for($i=0; $i<sizeof($arr_sites);$i++)
        $arr_sites[$i] = intval($arr_sites[$i]);
}else{
    if(isset($_SESSION['ss_template_list_site_id'])){
        $v_sites = $_SESSION['ss_template_list_site_id'];
        $arr_sites = unserialize($v_sites);
    }
    //$v_checked = isset($_SESSION['ss_template_list_site_checked'])?intval(isset($_SESSION['ss_template_list_site_checked'])):0;
}
if(isset($_SESSION['ss_template_list_site_id']))
    unset($_SESSION['ss_template_list_site_id']);

if(!isset($arr_sites) || !is_array($arr_sites)) $arr_sites = array();

//echo $_SESSION['ss_template_list_site_id'].'<br>';
//echo 'Data: '.sizeof($arr_sites).'<br>';
//echo 'Check: '.$v_checked .' -- isset: '.isset($_POST['txt_checked']);
if(sizeof($arr_sites)==0) $v_checked = 1;
$_SESSION['ss_ajax_template_first'] = 1;
?>