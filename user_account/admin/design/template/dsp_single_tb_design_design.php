<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_design").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            $('input#txt_design_name').val('');
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
        if($('input#chk_return_design').is(':checked'))
            $.cookie('ck_tab_design_cookie', 'Design');
		return true;
	});

    var validator = $('form#frm_designs').kendoValidator().data("kendoValidator");
    $("#txt_add_markup_cost").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");

    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        },
        select: function(e){
            var text = $(e.item).find("> .k-link").text();
            $.cookie('ck_tab_design_cookie', text);
        }
    }).data("kendoTabStrip");
    function select_tab_strip(){
        var text = $.cookie('ck_tab_design_cookie');
        $.removeCookie('ck_tab_design_cookie');
        if(typeof text!='undefined' || text!='')
            tab_strip.select("li:contains("+text+")");
    }

    <?php if($v_design_id>0){?>

    $('input#btn_create_vector_pdf').click(function(e){
        $.ajax({
            url             : '<?php echo URL . $v_admin_key;?>/ajax',
            dataType        : 'json',
            type            : 'POST',
            data            : {txt_design_id: '<?php echo $v_design_id;?>', txt_ajax_type: 'create_design_vector_pdf'},
            beforeSend      : function(){
                $('div#vector_pdf_link').find('a').remove();
                $('img#img_svg_loading').css('display','');
            },
            success         : function(data){
                if(data.success==1){
                    var $a = $('<a class="a-link">Download Zip</a>');
                    $a.attr('href', data.link);
                    $a.appendTo($('div#vector_pdf_link'));
                    //$('div#vector_pdf_link').insertAfter($a);
                }else{
                    alert(data.message);
                }
                $('img#img_svg_loading').css('display','none');
            },
            error           : function(xhr){
                //alert('Error: '+xhr.responseText);
            }
        });
    });

    var window_design = $('div#window_design');
    $('input#btn_launch_design').bind('click', function(){
        if(!window_design.data("kendoWindow")){
            window_design.kendoWindow({
                title: "Design tool for design<?php echo $v_design_id>0?': \"'.$v_design_name.'\" (Template: '.$v_template_name.')':''?>",
                width: "800px",
                modal: true,
                iframe: true,
                content: '<?php echo URL.$v_admin_key;?>/<?php echo $v_design_id;?>/design-design/?design_id=<?php echo $v_design_id;?>',
                type: "GET",
                actions: ["Minimize", "Maximize", "Close"],
                close: function(){
                    location.reload();
                }
            }).data('kendoWindow').bind('close', function(e){
                if(typeof view.isDesignSaved != 'undefined' && !view.isDesignSaved){
                    e.preventDefault();
                }
            });

        }
        window_design.data("kendoWindow").center().open().maximize();
    });
    <?php if($v_design_design){
    ?>
    //tab_strip.select(tab_strip.tabGroup.children("li:last"));
    <?php }?>
    <?php }?>
    select_tab_strip();
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Design (for Template <a href="<?php echo URL.$v_admin_key.'/'.$v_tempalte_id;?>/edit"><?php echo $v_template_name;?></a>)<?php echo $v_design_id>0?': '.$v_design_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select"><!--
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    //echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>-->
                        </div>
                    </div>

<form id="frm_tb_design_template" action="<?php echo URL.$v_admin_key;?>/<?php echo $v_design_id.'/design-edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_design_id" name="txt_design_id" value="<?php echo $v_design_id;?>" />
<input type="hidden" id="txt_template_id" name="txt_template_id" value="<?php echo $v_template_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" />
<input type="hidden" id="txt_user_name" name="txt_user_name" value="<?php echo $v_user_name;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <?php if($v_design_id>0){?>
                            <li>Design</li>
                        <?php }?>
                    </ul>

                    <div class="information div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width: 200px;">Design Name</td>
                                <td  style="width:2px" align="right">&nbsp;</td>
                                <td align="left"><input type="text" size="20" id="txt_design_name" name="txt_design_name" value="<?php echo $v_design_name;?>" class="text_css k-textbox" /><label id="lbl_design_name" class="k-required">(*)</label></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Theme's Name</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo $v_theme_name;?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Markup Cost</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo format_currency($v_markup_cost);?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Stock Cost</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo format_currency($v_stock_cost);?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Print Cost</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo format_currency($v_print_cost);?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Design Size</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo $v_design_width.'" &times; '.$v_design_height.'" '.$v_design_folding.' '.$v_design_direction.' '.$v_design_die_cut;?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Sample Image</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php if($v_sample_url!='') echo '<img src="'.$v_sample_url.'?'.time().'" />'; ?>&nbsp;</td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Design Status</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><div class="checkbox"><input type="checkbox" id="txt_design_status" name="txt_design_status"<?php echo $v_design_status==0?' checked="checked"':'';?> /><label for="txt_design_status"> Active</label></div></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Assigned to</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo $v_assigned_to.($v_location_name!=''?' - '.$v_location_name:'').($v_company_name!=''?' - '.$v_company_name:''); ?></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Created time</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php echo date('d-M-Y H:i:s', strtotime($v_created_time)); ?></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">&nbsp;</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><div class="checkbox"><input type="checkbox" id="chk_return_design" name="chk_return_design" /><label for="chk_return_design"> Return back for Design</label></div></td>
                            </tr>
                        </table>
                    </div>
<?php if($v_design_id>0){?>
    <div class="design div_details">
        <input id="btn_launch_design" type="button" class="k-button" value="Launch Design" />
        <div id="window_design" style="display: none">
            <?php
            //include 'qry_template_design.php';
            //include 'dsp_template_design.php';
            ?>
        </div>
        <?php
        if($v_sample_url!='') echo '<img src="'.$v_sample_url.'?'.time().'" />';
        ?>
        <div id="vector_pdf_link" style="clear: both; margin-top: 20px; margin-bottom: 5px; height: 30px">

        </div>
        <div>
            <input id="btn_create_vector_pdf" type="button" class="k-button" value="Create Vector PDF" /> &nbsp;&nbsp; <img src="<?php URL;?>images/icons/loading.gif" id="img_svg_loading" style="display: none" />
        </div>
    </div>
<?php }?>
                   </div>

                   <?php if(isset($v_act) && in_array($v_act, array('EDS', 'ADS'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_design" name="btn_submit_tb_design_design" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
