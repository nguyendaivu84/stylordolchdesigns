<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_sites = isset($_POST['sites'])?$_POST['sites']:'';
$v_checked = isset($_POST['checked'])?$_POST['checked']:0;
settype($v_checked, 'int');
if($v_checked!==0) $v_checked = 1;

settype($v_company_id, 'int');
//if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
$v_search_template_name = $v_quick_search;
if($v_search_template_name!='') $arr_where_clause['template_name'] = new MongoRegex('/'.$v_search_template_name.'/i');

if($v_checked==0){
    if(get_magic_quotes_gpc()) $v_sites = stripslashes($v_sites);
    $arr_sites = is_array($v_sites)?$v_sites:json_decode($v_sites, true);
    if(!is_array($arr_sites)) $arr_sites = array();
    for($i=0; $i<sizeof($arr_sites);$i++)
        $arr_sites[$i] = intval($arr_sites[$i]);
    if(sizeof($arr_sites)>0)
        $arr_where_clause['site_id'] = array('$in'=>$arr_sites);
    else
        $arr_where_clause['$where'] = "this.site_id=='undefined' || this.site_id.length==0";
}
if(!isset($arr_sites) || !is_array($arr_sites)) $arr_sites = array();
$_SESSION['ss_ajax_template_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_template_list_site_id'] = serialize($arr_sites);
$_SESSION['ss_template_list_site_checked'] = $v_checked;
$_SESSION['ss_template_quick_search'] = $v_quick_search;
if(isset($_SESSION['ss_ajax_template_first'])) unset($_SESSION['ss_ajax_template_first']);
/*
if(!($v_is_super_admin || is_administrator())){
    $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
    settype($v_user_id, 'int');
    $arr_site = $cls_tb_user->select_scalar('site', array('user_id'=>$v_user_id));
    if(!is_array($arr_site) || count($arr_site)==0) $arr_site = array(0);
    $arr_where_clause['site_id'] = array('$in'=>$arr_site);
}
*/

header("Content-type: application/json");
$arr_return = array('success'=>1);
echo json_encode($arr_return);
?>