<?php if(!isset($v_sval)) die();?>
<?php
add_class('cls_tb_design_design');
$cls_design = new cls_tb_design_design($db, LOG_DIR);
if(isset($_SESSION['ss_ajax_template_first'])){
    if(!$v_disabled_company_id)
        $arr_where_clause = array();
    else $arr_where_clause = $arr_global_company;
    $v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
    $v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
    $v_search_template_name = isset($_POST['txt_search_template_name'])?$_POST['txt_search_template_name']:'';
    $v_sites = isset($_POST['sites'])?$_POST['sites']:'';
    $v_checked = isset($_POST['checked'])?$_POST['checked']:0;
    settype($v_checked, 'int');
    if($v_checked!==0) $v_checked = 1;
    settype($v_company_id, 'int');
    //if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
    if($v_search_template_name!='') $arr_where_clause['template_name'] = new MongoRegex('/'.$v_search_template_name.'/i');
    if($v_checked==0){
        if(get_magic_quotes_gpc()) $v_sites = stripslashes($v_sites);
        $arr_sites = is_array($v_sites)?$v_sites:json_decode($v_sites, true);
        if(!is_array($arr_sites)) $arr_sites = array();
        for($i=0; $i<sizeof($arr_sites);$i++)
            $arr_sites[$i] = intval($arr_sites[$i]);
        if(sizeof($arr_sites)>0)
            $arr_where_clause['site_id'] = array('$in'=>$arr_sites);
        else
            $arr_where_clause['$where'] = "this.site_id=='undefined' || this.site_id.length==0";
    }
    if(!isset($arr_sites) || !is_array($arr_sites)) $arr_sites = array();
    $_SESSION['ss_template_list_site_id'] = serialize($arr_sites);
    //Sort
    $arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
    $arr_sort = array();
    if(is_array($arr_temp) && count($arr_temp)>0){
        for($i=0; $i<count($arr_temp); $i++){
            $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
        }
    }
    if(!is_array($arr_sort)) $arr_sort = array();
    //Start pagination
    $v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
    $v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
    if(isset($_SESSION['ss_tb_design_template_redirect']) && $_SESSION['ss_tb_design_template_redirect']==1){
        if(isset($_SESSION['ss_tb_design_template_where_clause'])){
            $arr_where_clause = unserialize($_SESSION['ss_tb_design_template_where_clause']);
            if(!is_array($arr_where_clause)) $arr_where_clause = array();
        }
        if(isset($_SESSION['ss_tb_design_template_sort'])){
            $arr_sort = unserialize($_SESSION['ss_tb_design_template_sort']);
            if(!is_array($arr_sort)) $arr_sort = array();
        }
        unset($_SESSION['ss_tb_design_template_redirect']);
    }



    if(!($v_is_super_admin || is_administrator())){
        $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
        settype($v_user_id, 'int');
        $arr_site = $cls_tb_user->select_scalar('site', array('user_id'=>$v_user_id));
        if(!is_array($arr_site) || count($arr_site)==0) $arr_site = array(0);
        $arr_where_clause['site_id'] = array('$in'=>$arr_site);
    }
}else{
    $v_where_clause = $_SESSION['ss_ajax_template_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
    if(!is_array($arr_where_clause)) $arr_where_clause = array();
    if(isset($_SESSION['ss_tb_design_template_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_design_template_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
    $v_page = isset($_SESSION['ss_tb_design_template_page'])?$_SESSION['ss_tb_design_template_page']:1;
    $v_page_size = isset($_SESSION['ss_tb_design_template_page_size'])?$_SESSION['ss_tb_design_template_page_size']:20;
    $v_quick_search = isset($_SESSION['ss_template_quick_search'])?$_SESSION['ss_template_quick_search']:'';
    $_SESSION['ss_ajax_template_first'] = 1;
    //unset($_SESSION['ss_ajax_template_where_clause']);
}

settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_design_template->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_design_template_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_design_template_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_design_template_page'] = $v_page;
$_SESSION['ss_tb_design_template_page_size'] = $v_page_size;
$_SESSION['ss_tb_design_template_quick_search'] = $v_quick_search;
$_SESSION['ss_tb_design_template'] = 1;
//End pagination
$arr_tb_design_template = $cls_tb_design_template->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;

$v_default_url = str_replace(ROOT_DIR,'', DESIGN_DIR);
$v_default_url = str_replace('\\','/', $v_default_url).'/template0_theme0.png';


$arr_company = array();
$arr_sites = array();

foreach($arr_tb_design_template as $arr){
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
	$v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
	$v_template_height = isset($arr['template_height'])?$arr['template_height']:0;
	$v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
	$v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
	$v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
	$v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
	$v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
	$v_template_type = isset($arr['template_type'])?$arr['template_type']:0;
	$v_template_status = isset($arr['template_status'])?$arr['template_status']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_is_already = isset($arr['is_already'])?$arr['is_already']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_template_desc = isset($arr['template_desc'])?$arr['template_desc']:'';
	$v_template_order = isset($arr['template_order'])?$arr['template_order']:0;
	$v_template_group = isset($arr['template_group'])?$arr['template_group']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
	$v_original_id = isset($arr['original_id'])?$arr['original_id']:0;
    $arr_site_id = isset($arr['site_id'])?$arr['site_id']:array();
    if(!is_array($arr_site_id)) $arr_site_id = array();

    $sites = array();
    for($i=0; $i<sizeof($arr_site_id);$i++){
        $site = intval($arr_site_id[$i]);
        if($site>0){
            if(!isset($arr_sites[$site])) $arr_sites[$site] = $cls_key->select_scalar('site_name', array('site_id'=>$site));
            $sites[] = $arr_sites[$site];
        }
    }

    $v_sample_url = $v_default_url;
    if($v_saved_dir!='' && $v_sample_image!=''){
        if(file_exists($v_saved_dir.$v_sample_image)){
            $v_sample_url = $v_saved_dir.$v_sample_image.'?'.time();
        }
    }
    if(!isset($arr_company[$v_company_id])) $arr_company[$v_company_id] = $cls_tb_company->select_scalar('company_name', array('company_id'=>$v_company_id));
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'template_id' => $v_template_id,
		'template_name' => $v_template_name,
		'template_width' => $v_template_width,
		'template_height' => $v_template_height,
		'folding_type' => $v_folding_type,
		'die_cut_type' => $v_die_cut_type,
		'stock_cost' => $v_stock_cost,
		'markup_cost' => $v_markup_cost,
		'print_cost' => $v_print_cost,
		'template_type' => $v_template_type,
		'template_status' => $v_template_status==0,
		'created_time' => date('d-M-Y',$v_created_time->sec),
		'user_id' => $v_user_id,
		'user_name' => $v_user_name,
		'is_already' => $v_is_already,
		'company_name' => $arr_company[$v_company_id],
		'sites' => implode('<br />',$sites),
		'template_group' => $v_template_group,
		'template_sample' => $v_sample_url,
        'design_count' => $cls_design->count(array('template_id'=>(int) $v_template_id))
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_template'=>$arr_ret_data);
echo json_encode($arr_return);
?>