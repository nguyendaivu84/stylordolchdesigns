<?php
if(!isset($v_sval)) die();
$v_theme_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_theme_id, 'int');

$v_design_company_id = 0;
$v_design_user_id = 0;
$v_template_id = 0;
add_class('cls_tb_design_theme');
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);
$v_row = $cls_theme->select_one(array('theme_id'=>$v_theme_id));
if($v_row==1){
    $v_template_id = $cls_theme->get_template_id();
}

$arr_product_list = array();
$v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_template_id));
if($v_row==1){
    $arr_product_list = $cls_tb_design_template->get_product_list();
    $v_design_company_id = $cls_tb_design_template->get_company_id();
    $v_design_user_id = $cls_tb_design_template->get_user_id();
}
$arr_products = array();

if(is_array($arr_product_list) && count($arr_product_list)>0){
    add_class('cls_tb_product');
    $cls_products = new cls_tb_product($db, LOG_DIR);
    $arr_tmp_products = $cls_products->select(array('company_id'=>$v_design_company_id, 'product_id'=>array('$in'=>$arr_product_list)));
    foreach($arr_tmp_products as $arr){
        $v_product_id = $arr['product_id'];
        $v_product_sku = $arr['product_sku'];
        $v_short_description = $arr['short_description'];
        $arr_products[$v_product_sku] = array(
            'id'=>$v_product_id
            ,'title'=>$v_short_description
            ,'active'=>1
        );
    }
}

$arr_products['blankDesign'] = array(
    'id'=>0
    ,'title'=>'Blank Design'
    ,'active'=>1
);
$arr_products['blank_design'] = array(
    'id'=>0
    ,'title'=>'Blank Design'
    ,'active'=>1
);

$v_admin_id = $v_design_user_id;

$v_row = 0;
if($v_admin_id>0){
    $v_contact_id = $cls_tb_user->select_scalar('contact_id', array('user_id'=>$v_design_user_id));
    $v_row = $cls_tb_contact->select_one(array('contact_id'=>$v_contact_id));
}
if($v_row==1){
    $v_first_name = $cls_tb_contact->get_first_name();
    $v_last_name = $cls_tb_contact->get_last_name();
    $v_email = $cls_tb_contact->get_email();
    $v_phone = $cls_tb_contact->get_direct_phone();
}else{
    $v_admin_id = null;
    $v_email = null;
    $v_last_name=null;
    $v_first_name=null;
    $v_phone = null;
}

$arr_design_user = array(
    'customer_id'=>$v_admin_id
    ,'visitor_id'=>$v_admin_id
    ,'session_id'=>session_id()
    ,'site_code'=>DESIGN_SITE_CODE
    ,'first_name'=>$v_first_name
    ,'last_name'=>$v_last_name
    ,'email'=>$v_email
    ,'phone'=>$v_phone
    ,"cart_items_count"=>0
    ,"ffr_cart_items_count"=>0
    ,'admin_mode'=>1
);

add_class('cls_tb_design_font');
$cls_fonts = new cls_tb_design_font($db, LOG_DIR);

$arr_tmp_fonts = $cls_fonts->select(array('font_status'=>0, 'font_regular'=>1), array('font_key'=>1));
$arr_fonts = array();
foreach($arr_tmp_fonts as $arr){
    $arr_fonts[$arr['font_name']] = array('bold'=>$arr['font_bold']==1, 'italic'=>$arr['font_italic']==1);
}
?>