<?php if(!isset($v_sval)) die();?>
<style>
    .k-tooltip{
        top: -15px !important;
    }
    .k-animation-container{
        margin-bottom: 10px !important;
        margin-top: 0 !important;
    }
</style>

<?php if($v_disabled_company_id){?>
    <script type="text/javascript">
        $(document).ready(function(e){
            var combo_company = $('select#txt_company_id').data("kendoComboBox");
            combo_company.enable(false);
        });
    </script>
<?php }?>

    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Template</h3>
                    </div>
                    <div id="div_quick" style="margin-bottom: 30px; height: auto !important">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">

                    <span class="k-textbox k-space-left" id="txt_quick_search">
                    <input type="text" name="txt_quick_search" placeholder="Search by Template Name" value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" />
                    <a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a>
                    <script type="text/javascript">
                        $(document).ready(function(e){
                            $('a#a_quick_search').click(function(e){
                                $('form#frm_quick_search').submit();
                            })
                        });
                    </script>
                    </span>

                    <input type="hidden" name="txt_company_id" id="txt_company_id" value="<?php echo $v_company_id;?>" />
                    <input type="hidden" name="txt_sites" id="txt_sites" value="<?php echo json_encode($arr_sites);?>" />
                    <input type="hidden" name="txt_checked" id="txt_checked" value="<?php echo $v_checked;?>" />
                    </form>
                    </div>
                    <div id="div_select">
                    <form id="frm_company_id" method="post">
                    <!--
                    Company: <select id="txt_company_id" name="txt_company_id" onchange="this.form.submit();" style="display: none">
                    <option value="0" selected="selected">-------</option>
                    <?php
//					/echo $v_dsp_company_option;
					?>
                    </select>
                    -->
                    <span class="checkbox">
                    <input type="checkbox" id="chk_site_all" /><label for="chk_site_all" id="lbl_site_all">All templates / Or choose site: </label>
                    </span>
                    <select id="txt_site_selected" name="txt_site_selected[]" multiple="multiple" style="margin-bottom: 5px">

                    </select>
                    <input type="hidden" name="txt_quick_search" id="txt_quick_search" value="<?php echo htmlspecialchars($v_quick_search);?>" />
                    </form>
                    </div>
                    </div>


                    <div id="grid"></div>
                    <div id="advanced_search_window" style="display:none">
                    <h2>Advanced Search for Design_template</h2>
                    <form id="frm_advanced_search" method="post" action="<?php echo URL.$v_admin_key;?>">
                    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                    <tr align="left" valign="middle">
                    <td align="right">Company:</td>
                    <td>
                    <select id="txt_search_company_id" name="txt_search_company_id">
                    <option value="0" selected="selected">--------</option>
                    <?php echo $v_dsp_company_option;?>
                    </select>
                    </td>
                    </tr>
                    <tr align="center" valign="middle">
                    <td colspan="2">
                    <input type="submit" class="k-button k-button button_css" value="Search" name="btn_advanced_search" />
                    <input type="submit" class="k-button k-button button_css" value="Reset" name="btn_advanced_reset" />
                    </td>
                    </tr>
                    </table>
                    </form>
                    </div>
				<script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        window_search = $('div#advanced_search_window');
                        $('li#icons_advanced_search').bind("click", function() {
                            if (!window_search.data("kendoWindow")) {
                                window_search.kendoWindow({
                                    width: "600px",
                                    actions: ["Maximize", "Close"],
                                    modal: true,
                                    title: "Advanced Search for Design_template"
                                });
                            }
                            window_search.data("kendoWindow").center().open();
                        });

                        var site_data = <?php echo json_encode($arr_all_key);?>;
                        var site_combo = $('select#txt_site_selected').width(500).kendoMultiSelect({
                            dataSource:site_data,
                            dataTextField: 'site_name',
                            dataValueField:'site_id',
                            itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="max-width: 200px" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
                                '<h3>${ data.site_name }</h3>' +
                                '<p>${ data.site_url }</p></div>',
                            tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
                                '#: data.site_name #'

                        }).data("kendoMultiSelect");

                        $('select#txt_site_selected').change(function(e){
                            var s = [];
                            $(this).find('option:selected').each(function(e){
                                s.push($(this).val());
                            });

                            var sites = JSON.stringify(s);
                            var chk = $('input#chk_site_all').is(':checked')?1:0;
                            var quick = $('input#txt_quick_search[type="hidden"]').val();
                            $.ajax({
                                url             : '<?php echo URL . $v_admin_key;?>/json',
                                type            : 'POST',
                                dataType        : 'json',
                                data            : {sites: sites, checked: chk, txt_quick_search: quick, txt_json_type: 'load_site_template'},
                                beforeSend      : function(){
                                    if(!$('input#chk_site_all').is(':checked'))
                                        site_combo.enable(false);
                                    $('input#chk_site_all').prop('disabled', true);
                                },
                                success         : function(data){
                                    if(!$('input#chk_site_all').is(':checked'))
                                        site_combo.enable(true);
                                    $('input#chk_site_all').prop('disabled', false);
                                    $('input#txt_checked[type="hidden"]').val(chk);
                                    if(data.success==1){
                                        grid.dataSource.read();
                                        grid.refresh();
                                    }
                                }
                            });
                        });
                        $('input#chk_site_all').click(function(e){
                            var chk = $(this).is(':checked');
                            if(chk){
                                //site_combo.value([]);
                            }
                            site_combo.enable(!chk);
                            $('select#txt_site_selected').trigger('change');
                        });
                        <?php if($v_checked==1){?>
                        site_combo.enable(false);
                        $('input#chk_site_all').prop('checked', true);
                        <?php }else{?>
                        site_combo.value(<?php echo json_encode($arr_sites);?>);
                        <?php }?>

                        var grid = $("#grid").kendoGrid({
                            dataSource: {
                                pageSize: 20,
                                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                                serverPaging: true,
                                serverSorting: true,
                                transport: {
                                    read: {
                                        url: "<?php echo URL.$v_admin_key;?>/json/",
                                        type: "POST",
                                        data: {txt_session_id:"<?php echo session_id();?>",txt_json_type: 'load_template',txt_quick_search:'<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>',txt_search_template_name:'<?php echo isset($v_search_template_name)?htmlspecialchars($v_search_template_name):'';?>', txt_search_company_id: '<?php echo $v_company_id;?>', sites: $('select#txt_site_selected').val()<?php //echo json_encode($arr_sites);?>, checked: $('input#chk_site_all[type="checkbox"]').is(':checked')?1:0<?php //echo $v_checked;?>}
                                    }
                                },
                                schema: {
                                    data: "tb_design_template"
                                    ,total: function(data){
                                        return data.total_rows;
                                    }
                                },
                                type: "json"
                            },
                            pageSize: 20,
                            height: 430,
                            scrollable: true,
                            sortable: true,
                            selectable: true,
                            dataBound: function(e){
                                var stop_id = '<?php echo $v_stop_template_id?>';
                                var g = e.sender;
                                var data = g.dataSource.view();
                                for(var i=0; i<data.length; i++){
                                    if(data[i].design_count > 0){
                                        var $a = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] a.k-grid-Delete");
                                        $a.css('display', 'none');
                                    }

                                    if(data[i].template_id+''==stop_id){
                                        var row = g.table.find("tr[data-uid='" + data[i].uid + "']");
                                        if (row.length != 0) {
                                            $("#grid div.k-grid-content").scrollTop($(row).position().top); //scroll the content
                                        }
                                        g.select(row);
                                    }
                                }
                            },
                            //selectable: "single",
                            pageable: {
                                input: true,
                                refresh: true,
                                pageSizes: [10, 20, 30, 40, 50],
                                numeric: false
                            },
						columns: [
							{field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
							{field: "template_name", title: "Template Name", type:"string", width:"100px", sortable: true, template:'<p style="margin: 2px; text-align: center">#= template_name# <br /><img style="width: 150px" src="#= template_sample#" /> </p>' },
							{field: "template_width", title: "Width", width:"30px", sortable: true, template: '<span style="float:right">#= template_width #</span>'},
							{field: "template_height", title: "Height", width:"30px", sortable: true, template: '<span style="float:right">#= template_height #</span>'},
							{field: "stock_cost", title: "Stock Cost", width:"40px", sortable: true, template: '<span style="float:right">#= kendo.toString(stock_cost,"c2") #</span>'},
							{field: "markup_cost", title: "Markup Cost", width:"40px", sortable: true, template: '<span style="float:right">#= kendo.toString(markup_cost,"c2") #</span>'},
							{field: "print_cost", title: "Print Cost", width:"40px", sortable: true, template: '<span style="float:right">#= kendo.toString(print_cost,"c2") #</span>'},
							{field: "template_status", title: "Status", type:"boolean", width:"30px", sortable: true, template: '<span style="float:right">#= template_status?"Active":"Inactive" #</span>'},
							{field: "user_name", title: "Created by", type:"string", width:"50px", sortable: true },
							{field: "sites", title: "Apply for", type:"string", width:"70px", sortable: true, encoded: false},
							{ command:  [
								{ name: "View", text:'', click: view_row, imageClass: 'k-grid-View' }
								<?php if($v_edit_right || $v_is_super_admin){?>
								,{ name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
								<?php }?>
                                <?php if($v_create_right || $v_is_super_admin){?>
                                ,{ name: "Clone", text:'', click: duplicate_row, imageClass: 'k-grid-Clone' }
                                <?php }?>
								<?php if($v_delete_right || $v_is_super_admin){?>
								,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
								<?php }?>
								],
								title: " ", width: "70px" }
						 ]
					 }).data("kendoGrid");
                        var tooltip = grid.table.kendoTooltip({
                            filter: "td:last-child a",
                            position: "top",
                            content: function (e) {
                                var target = e.target; // element for which the tooltip is shown
                                if($(target).hasClass('k-grid-View'))
                                    return 'View current template';
                                else if($(target).hasClass('k-grid-Clone'))
                                    return 'Clone current template';
                                else if($(target).hasClass('k-grid-Edit'))
                                    return 'Edit current template';
                                else if($(target).hasClass('k-grid-Delete'))
                                    return 'Delete current template';
                            }
                        }).data("kendoTooltip");
				});

                    function view_row(e) {
                        e.preventDefault();
                        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.template_id+"/view";
                    }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to edit design\'s template with name: "'+dataItem.template_name+'"?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.template_id+"/edit";
                    }
                }
                    function duplicate_row(e) {
                        e.preventDefault();
                        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                        if(confirm('Do you want to clone same template from template with name: "'+dataItem.template_name+'"?')){
                            document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.template_id+"/clone";
                        }
                    }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete design\'s template with name: "'+dataItem.template_name+'" and all themes?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.template_id+"/delete";
                    }
                }
            </script>
                </div>
            </div>
        </div>
  </div>