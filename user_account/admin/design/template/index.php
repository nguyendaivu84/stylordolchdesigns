<?php if(!isset($v_sval)) die();?>
<?php
$v_act = isset($_GET['act'])?$_GET['act']:'';
$v_design_template_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_design_template_id, 'int');
add_class('cls_tb_design_template');
$cls_tb_design_template = new cls_tb_design_template($db, LOG_DIR);
add_class('cls_tb_design_theme');
$cls_tb_design_theme = new cls_tb_design_theme($db, LOG_DIR);
add_class('ManageFile', 'cls_file.php');
$cls_file = new ManageFile(DESIGN_THEME_DIR);
add_class('cls_tb_design_key');
$cls_key = new cls_tb_design_key($db, LOG_DIR);

$v_this_module_menu = isset($v_module_menu)?$v_module_menu:''; //replace module_menu_key here
$v_user_full_name = '';
$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
$arr_user_module_rule = array();
$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
$v_row = $cls_tb_user->select_one(array('user_id'=>$v_user_id));
if($v_row == 1){
	$v_contact_id = $cls_tb_user->get_contact_id();
	$v_user_name = $cls_tb_user->get_user_name();
	$v_user_full_name = $cls_tb_contact->get_full_name_contact($v_contact_id);
	$arr_user_module_rule = $cls_tb_user->get_all_permission($db);
}
$v_view_right = isset($arr_user_module_rule[$v_this_module_menu]['view']);
$v_edit_right = isset($arr_user_module_rule[$v_this_module_menu]['edit']);
$v_create_right = isset($arr_user_module_rule[$v_this_module_menu]['create']);
$v_delete_right = isset($arr_user_module_rule[$v_this_module_menu]['delete']);
$v_report_right = isset($arr_user_module_rule[$v_this_module_menu]['report']);
$v_search_right = true;
$v_view_all_right = false;

$v_dsp_menu = $cls_tb_module->draw_kendo_menu($v_module_key, URL.'admin/', $v_is_super_admin, $arr_user_module_rule);
$v_dsp_horizontal_menu = $cls_tb_module->draw_kendo_horizontal_menu_from($v_dsp_menu);
$v_dsp_tree_menu = $cls_tb_module->draw_kendo_tree_menu_from('ANVY', URL, $v_dsp_menu, 'images/icons/logos.png');
//Show hide icon
$v_show_report_icon = $v_report_right || $v_is_super_admin;
$v_show_view_icon = $v_view_right || $v_is_super_admin;
$v_show_create_icon = $v_create_right || $v_is_super_admin;
$v_show_search_icon = $v_search_right || $v_is_super_admin;


$_SESSION['ss_design_side'] = DESIGN_SIDE_FACE;
switch($v_act){
    case 'C':
        include 'act_clone_tb_design_template.php';
        break;
	case 'N':
		if($v_create_right || $v_is_super_admin){
			include 'qry_single_tb_design_template.php';
			include 'user_account/admin/admin_header.php';
			include 'dsp_single_tb_design_template.php';
			include 'user_account/admin/admin_footer.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
	case 'V':
		if($v_view_right || $v_is_super_admin){
			include 'qry_single_tb_design_template.php';
			include 'user_account/admin/admin_header.php';
			include 'dsp_single_tb_design_template.php';
			include 'user_account/admin/admin_footer.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
	case 'E':
		if($v_edit_right || $v_is_super_admin){
			include 'qry_single_tb_design_template.php';
			include 'user_account/admin/admin_header.php';
			include 'dsp_single_tb_design_template.php';
			include 'user_account/admin/admin_footer.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
	case 'D':
		if($v_delete_right || $v_is_super_admin){
			include 'act_delete_tb_design_template.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
    case 'RTH':
        if($v_delete_right || $v_is_super_admin){
            include 'act_delete_tb_design_theme.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
    case 'RDS':
        if($v_delete_right || $v_is_super_admin){
            include 'act_delete_tb_design_design.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
	case 'J':
        $v_json_type = isset($_POST['txt_json_type'])?$_POST['txt_json_type']:'';
        if($v_json_type=='load_template')
		    include 'qry_json_tb_design_template.php';
        else if($v_json_type=='load_location')
            include 'qry_json_load_location.php';
        else if($v_json_type=='load_product')
            include 'qry_json_load_product.php';
        else if($v_json_type=='load_tag')
            include 'qry_json_load_tag.php';
        else if($v_json_type=='load_design')
            include 'qry_json_tb_design_design.php';
        else if($v_json_type=='load_site_template')
            include 'qry_ajax_tb_design_template.php';
		break;
	case 'X':
		if($v_report_right || $v_is_super_admin){
			include 'qry_export_tb_design_template.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
	case 'AJ':
		$v_ajax_type = isset($_POST['txt_ajax_type'])?$_POST['txt_ajax_type']:'';
        if($v_ajax_type=='load_company_info')
            include 'qry_ajax_load_company_info.php';
        else if($v_ajax_type=='save_design_data')
            include 'qry_ajax_save_design_data.php';
        else if($v_ajax_type=='delete_object')
            include 'qry_ajax_delete_design_data.php';
        else if($v_ajax_type=='create_template_vector_pdf')
            include 'qry_ajax_template_vector_pdf.php';
        else if($v_ajax_type=='create_design_vector_pdf')
            include 'qry_ajax_design_vector_pdf.php';
        else if($v_ajax_type=='create_theme_vector_pdf')
            include 'qry_ajax_theme_vector_pdf.php';
		break;
    case 'DTE':
        if($v_edit_right || $v_is_super_admin){
            include 'qry_template_design.php';
            include 'user_account/admin/design_header.php';
            include 'dsp_template_design.php';
            include 'user_account/admin/design_footer.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
    case 'DTH':
        if($v_edit_right || $v_is_super_admin){
            include 'qry_theme_design.php';
            include 'user_account/admin/design_header.php';
            include 'dsp_theme_design.php';
            include 'user_account/admin/design_footer.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
    case 'EDS':
        if($v_edit_right || $v_is_super_admin){
            include 'qry_single_tb_design_design.php';
            include 'user_account/admin/admin_header.php';
            include 'dsp_single_tb_design_design.php';
            include 'user_account/admin/admin_footer.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
    case 'DDS':
        if($v_edit_right || $v_is_super_admin){
            include 'qry_design_design.php';
            include 'user_account/admin/design_header.php';
            include 'dsp_design_design.php';
            include 'user_account/admin/design_footer.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
    case 'ATH':
    case 'ETH':
        if($v_edit_right || $v_is_super_admin){
            include 'qry_single_tb_design_theme.php';
            include 'user_account/admin/admin_header.php';
            include 'dsp_single_tb_design_theme.php';
            include 'user_account/admin/admin_footer.php';
        }else{
            redir(URL.'admin/error/');
        }
        break;
	case 'P':
		if($v_report_right || $v_is_super_admin){
			include 'qry_print_tb_design_template.php';
			include 'user_account/admin/print_header.php';
			include 'dsp_print_tb_design_template.php';
			include 'user_account/admin/print_footer.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
	case 'A':
	default:
		if($v_view_right || $v_is_super_admin){
			$v_act = 'A';
			include 'qry_all_tb_design_template.php';
			include 'user_account/admin/admin_header.php';
			include 'dsp_all_tb_design_template.php';
			include 'user_account/admin/admin_footer.php';
		}else{
			redir(URL.'admin/error/');
		}
		break;
}
?>