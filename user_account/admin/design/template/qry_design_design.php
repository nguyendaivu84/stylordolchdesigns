<?php
if(!isset($v_sval)) die();
if(isset($_SESSION['ss_design_side'])) unset($_SESSION['ss_design_side']);
$v_design_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_design_id, 'int');
$v_user_id = 0;
$v_template_id = 0;
$v_member_first_name = '';
$v_member_last_name = '';
$v_member_phone = '';
$v_member_email = '';
$arr_product_list = array();
$arr_product = array();
if($v_design_id>0){
    add_class('cls_tb_design_design');
    $cls_designs = new cls_tb_design_design($db, LOG_DIR);
    $v_row = $cls_designs->select_one(array('design_id'=>$v_design_id));
    if($v_row==1){
        $v_product_id = $cls_designs->get_product_id();
        $v_template_id = $cls_designs->get_template_id();
        $v_theme_id = $cls_designs->get_theme_id();
        $arr_product_list[] = $v_product_id;
        $arr_product = $cls_designs->get_product();

        $v_user_id = $cls_designs->get_user_id();
        $v_contact_id = $cls_tb_user->select_scalar('contact_id',array('user_id'=>$v_user_id));
        if($v_contact_id>0){
            $v_contact_row = $cls_tb_contact->select_one(array('contact_id'=>$v_contact_id));
            if($v_contact_row==1){
                $v_member_first_name = $cls_tb_contact->get_first_name();
                $v_member_last_name = $cls_tb_contact->get_last_name();
                $v_member_phone = $cls_tb_contact->get_direct_phone();
                $v_member_email = $cls_tb_contact->get_email();
            }
        }
    }
}else{
    add_class('cls_tb_design_template');
    $cls_templates = new cls_tb_design_template($db, LOG_DIR);
    $arr_design_product = $cls_templates->select_scalar('product_list', array('template_id'=>$v_template_id));
}

if(is_array($arr_product_list) && count($arr_product_list)>0){
    add_class('cls_tb_product');
    $cls_products = new cls_tb_product($db, LOG_DIR);
    $arr_tmp_products = $cls_products->select(array('product_id'=>array('$in'=>$arr_product_list)));
    foreach($arr_tmp_products as $arr){
        $v_product_id = $arr['product_id'];
        $v_product_sku = $arr['product_sku'];
        $v_short_description = $arr['short_description'];
        $arr_products[$v_product_sku] = array(
            'id'=>$v_product_id
            ,'title'=>$v_short_description
            ,'active'=>1
        );
    }
}

$arr_products['blankDesign'] = array(
    'id'=>0
    ,'title'=>'Blank Design'
    ,'active'=>1
);
$arr_products['blank_design'] = array(
    'id'=>0
    ,'title'=>'Blank Design'
    ,'active'=>1
);

if(is_array($arr_product) && isset($arr_product['id']) && $arr_product['id']>0 && isset($arr_product['code']) && $arr_product['code']!=''){
    $arr_products[$arr_product['code']] = array(
        'id'=>$arr_product['id'],
        'title'=>isset($arr_product['title'])?$arr_product['title']:''
        ,'active'=>1
    );
}

$arr_design_user = array(
    'customer_id'=>$v_user_id
    ,'visitor_id'=>(int) rand(0,1000000)
    ,'session_id'=>session_id()
    ,'site_code'=>DESIGN_SITE_CODE
    ,'first_name'=>$v_member_first_name
    ,'last_name'=>$v_member_last_name
    ,'email'=>$v_member_email
    ,'phone'=>$v_member_phone
    ,"cart_items_count"=>0
    ,"ffr_cart_items_count"=>0
    ,'admin_mode'=>0
);


add_class('cls_tb_design_font');
$cls_fonts = new cls_tb_design_font($db, LOG_DIR);

$arr_tmp_fonts = $cls_fonts->select(array('font_status'=>0, 'font_regular'=>1), array('font_key'=>1));
$arr_fonts = array();
foreach($arr_tmp_fonts as $arr){
    $arr_fonts[$arr['font_name']] = array('bold'=>$arr['font_bold']==1, 'italic'=>$arr['font_italic']==1);
}