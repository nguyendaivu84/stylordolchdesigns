<?php if(!isset($v_sval)) die();?>
<?php
$v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:'0';
settype($v_template_id, 'int');
$arr_where_clause = array('template_id'=> $v_template_id);
add_class('cls_tb_design_design');
$cls_tb_design_design = new cls_tb_design_design($db, LOG_DIR);
add_class('cls_tb_design_design');
$cls_tb_design_design = new cls_tb_design_design($db, LOG_DIR);


$arr_sort = array('design_id'=>-1);
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;

settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_design_design->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;


//End pagination
$arr_tb_design_design = $cls_tb_design_design->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;

$v_default_url = str_replace(ROOT_DIR,'', DESIGN_DIR);
$v_default_url = str_replace('\\','/', $v_default_url).'/template0_theme0.png';

$arr_theme = array();
$arr_contact = array();
foreach($arr_tb_design_design as $arr){
	$v_design_name = isset($arr['design_name'])?$arr['design_name']:'';
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
	$v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
	$v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
	$v_design_status = isset($arr['design_status'])?$arr['design_status']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_design_order = isset($arr['design_order'])?$arr['design_order']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_sample_url = $v_default_url;
    if($v_saved_dir!='' && $v_sample_image!=''){
        if(file_exists($v_saved_dir.$v_sample_image)){
            $v_sample_url = $v_saved_dir.$v_sample_image.'?'.time();
        }
    }
    if(!isset($arr_contact[$v_user_id])){
        $v_contact_id = $cls_tb_user->select_scalar('contact_id', array('user_id'=>$v_user_id));
        settype($v_contact_id, 'int');
        $v_full_name = $cls_tb_contact->get_full_name_contact($v_contact_id);
        $arr_contact[$v_user_id] = $v_full_name;
    }
    if(!isset($arr_theme[$v_theme_id])) $arr_theme[$v_theme_id] = $cls_tb_design_theme->select_scalar('theme_name', array('theme_id'=>$v_theme_id));
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'design_id' => $v_design_id,
		'design_name' => $v_design_name,
		'stock_cost' => $v_stock_cost,
		'markup_cost' => $v_markup_cost,
		'print_cost' => $v_print_cost,
		'design_status' => $v_design_status==0,
		'created_time' => date('d-M-Y',$v_created_time->sec),
		'user_id' => $v_user_id,
		'user_name' => $arr_contact[$v_user_id],
		'theme_name' => $arr_theme[$v_theme_id],
		'sample_image' => $v_sample_url
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_design'=>$arr_ret_data);
echo json_encode($arr_return);
?>