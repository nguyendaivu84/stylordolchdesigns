<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:'0';
$v_side = isset($_POST['txt_side'])?$_POST['txt_side']:'0';
$v_text_data = isset($_POST['txt_text_data'])?$_POST['txt_text_data']:'';
$v_image_data = isset($_POST['txt_image_data'])?$_POST['txt_image_data']:'';

settype($v_template_id, 'int');
settype($v_side, 'int');

if($v_side!=1) $v_side = 0;

$arr_return = array(
    'success'=>0, 'message'=>'Unknown Error!', 'url'=>''
);
$v_save_change = false;
if($v_template_id>0){
    $v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_template_id));
    if($v_row==1){
        $v_template_data = $cls_tb_design_template->get_template_data();
        $v_folding_type = $cls_tb_design_template->get_folding_type();
        $v_saved_dir = $cls_tb_design_template->get_saved_dir();
        $v_sample_image = $cls_tb_design_template->get_sample_image();
        $v_dpi = $cls_tb_design_template->get_template_dpi();
        if($v_folding_type<1) $v_side = 0;
        $arr_json = json_decode($v_template_data, true);
        if(!is_array($arr_json)) $arr_json = array();

        $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        if(get_magic_quotes_gpc()){
            $v_text_data = stripslashes($v_text_data);
            $v_image_data = stripslashes($v_image_data);
        }
        $arr_text_tmp = json_decode($v_text_data, true);
        $arr_image_tmp = json_decode($v_image_data, true);

        $arr_text_data = array();
        if(is_array($arr_text_tmp)){
            for($i=0; $i<sizeof($arr_text_tmp);$i++){
                if(isset($arr_text_tmp[$i]['name'])){
                    $v_name = $arr_text_tmp[$i]['name'];
                    foreach($arr_text_tmp[$i] as $key=>$value){
                        if($key!='name'){
                            if(in_array($key, array('left', 'top')))
                                $arr_text_data[$v_name][$key] = floatval($value);
                            else if(in_array($key,array('based', 'ontop')))
                                $arr_text_data[$v_name][$key] = $value==1;
                            else
                                $arr_text_data[$v_name][$key] = $value;
                        }
                    }
                }
            }
        }

        $arr_image_data = array();
        if(is_array($arr_image_tmp)){
            for($i=0; $i<sizeof($arr_image_tmp);$i++){
                if(isset($arr_image_tmp[$i]['name'])){
                    $v_name = $arr_image_tmp[$i]['name'];
                    foreach($arr_image_tmp[$i] as $key=>$value){
                        if($key!='name'){
                            if($key=='placeholder')
                                $arr_image_data[$v_name][$key] = $value;
                            else if(in_array($key, array('left', 'top'))){
                                $v_val = floatval($value);
                                $arr_image_data[$v_name][$key] = $v_val;
                            }else{
                                $v_val = intval($value);
                                $arr_image_data[$v_name][$key] = $v_val==1;
                            }
                        }
                    }
                }
            }
        }

        $arr_side = isset($arr_canvases[$v_side])?$arr_canvases[$v_side]:array();
        $v_change = false;
        $arr_texts = isset($arr_side['texts'])?$arr_side['texts']: array();
        for($i=0; $i<sizeof($arr_texts);$i++){
            $v_name = isset($arr_texts[$i]['name'])?$arr_texts[$i]['name']:'';
            if($v_name!=''){
                if(isset($arr_text_data[$v_name]['title'])){
                    $arr_texts[$i]['title'] = $arr_text_data[$v_name]['title'];
                    $v_change = true;
                }
                if(isset($arr_text_data[$v_name]['preset'])){
                    $arr_texts[$i]['preset_text'] = $arr_text_data[$v_name]['preset'];
                    $v_change = true;
                }
                if(isset($arr_text_data[$v_name]['body'])){
                    $arr_texts[$i]['body'] = $arr_text_data[$v_name]['body'];
                    $v_change = true;
                }
                if(isset($arr_text_data[$v_name]['top'])){
                    $arr_texts[$i]['top'] = $arr_text_data[$v_name]['top'];
                    $v_change = true;
                }
                if(isset($arr_text_data[$v_name]['left'])){
                    $arr_texts[$i]['left'] = $arr_text_data[$v_name]['left'];
                    $v_change = true;
                }
                $v_based = isset($arr_texts[$i]['based'])?boolval($arr_texts[$i]['based']):false;
                if($v_based){
                    if(!$arr_text_data[$v_name]['based']){
                        unset($arr_texts[$i]['based']);
                        $v_change = true;
                    }
                }else{
                    if($arr_text_data[$v_name]['based']) $arr_texts[$i]['based'] = $v_change = true;
                }
                $v_ontop = isset($arr_texts[$i]['ontop'])?boolval($arr_texts[$i]['ontop']):false;
                if($v_ontop){
                    if(!$arr_text_data[$v_name]['ontop']){
                        unset($arr_texts[$i]['ontop']);
                        $v_change = true;
                    }
                }else{
                    if($arr_text_data[$v_name]['ontop']) $arr_texts[$i]['ontop'] = $v_change = true;
                }
            }
        }
        if($v_change){
            $arr_canvases[$v_side]['texts'] = $arr_texts;
            $v_save_change = true;
        }
        $v_change = false;
        $arr_images = isset($arr_side['images'])?$arr_side['images']: array();
        for($i=0; $i<sizeof($arr_images); $i++){
            $v_name = isset($arr_images[$i]['name'])?$arr_images[$i]['name']:'';
            $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:'0';
            settype($v_image_id, 'int');
            if($v_name!=''){
                if(isset($arr_image_data[$v_name]['top'])){
                    $arr_images[$i]['top'] = $arr_image_data[$v_name]['top'];
                    $v_change = true;
                }
                if(isset($arr_image_data[$v_name]['left'])){
                    $arr_images[$i]['left'] = $arr_image_data[$v_name]['left'];
                    $v_change = true;
                }
                $v_based = isset($arr_images[$i]['based'])?boolval($arr_images[$i]['based']):false;
                if($v_based){
                    if(!$arr_image_data[$v_name]['based']){
                        unset($arr_images[$i]['based']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['based']) $arr_images[$i]['based'] = $v_change = true;
                }
                $v_ontop = isset($arr_images[$i]['ontop'])?boolval($arr_images[$i]['ontop']):false;
                if($v_ontop){
                    if(!$arr_image_data[$v_name]['ontop']){
                        unset($arr_images[$i]['ontop']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['ontop']) $arr_images[$i]['ontop'] = $v_change = true;
                }

                $v_noprint = isset($arr_images[$i]['noprint'])?boolval($arr_images[$i]['noprint']):false;
                if($v_noprint){
                    if(!$arr_image_data[$v_name]['noprint']){
                        unset($arr_images[$i]['noprint']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['noprint']){
                        $arr_images[$i]['noprint'] = $v_change = true;
                    }
                }
                /*
                $v_uploader = isset($arr_images[$i]['uploader'])?boolval($arr_images[$i]['uploader']):false;
                if($v_uploader){
                    if(!$arr_image_data[$v_name]['uploader']){
                        unset($arr_images[$i]['uploader']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['uploader']){
                        $arr_images[$i]['uploader'] = $v_change = true;
                    }
                }
                */
                $v_noselect = isset($arr_images[$i]['noselect'])?boolval($arr_images[$i]['noselect']):false;
                if($v_noselect){
                    if(!$arr_image_data[$v_name]['noselect']){
                        unset($arr_images[$i]['noselect']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['noselect']){
                        $arr_images[$i]['noselect'] = $v_change = true;
                    }
                }
                $v_noresize = isset($arr_images[$i]['noresize'])?boolval($arr_images[$i]['noresize']):false;
                if($v_noresize){
                    if(!$arr_image_data[$v_name]['noresize']){
                        unset($arr_images[$i]['noresize']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['noresize']){
                        $arr_images[$i]['noresize'] = $v_change = true;
                    }
                }
                $v_unmovable = isset($arr_images[$i]['unmovable'])?boolval($arr_images[$i]['unmovable']):false;
                if($v_unmovable){
                    if(!$arr_image_data[$v_name]['unmovable']){
                        unset($arr_images[$i]['unmovable']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['unmovable']){
                        $arr_images[$i]['unmovable'] = $v_change = true;
                    }
                }
                $v_locked = isset($arr_images[$i]['locked'])?boolval($arr_images[$i]['locked']):false;
                if($v_locked){
                    if(!$arr_image_data[$v_name]['locked']){
                        unset($arr_images[$i]['locked']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['locked']){
                        $arr_images[$i]['locked'] = $v_change = true;
                    }
                }
                $v_crop = isset($arr_images[$i]['crop'])?boolval($arr_images[$i]['crop']):false;
                if($v_crop){
                    if(!$arr_image_data[$v_name]['crop']){
                        unset($arr_images[$i]['crop']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['crop']){
                        $arr_images[$i]['crop'] = $v_change = true;
                    }
                }
                $v_release = isset($arr_images[$i]['release'])?boolval($arr_images[$i]['release']):false;
                if($v_release){
                    if(!$arr_image_data[$v_name]['release']){
                        unset($arr_images[$i]['release']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['release']){
                        $arr_images[$i]['release'] = $v_change = true;
                    }
                }
                $v_noreplace = isset($arr_images[$i]['noreplace'])?boolval($arr_images[$i]['noreplace']):false;
                if($v_noreplace){
                    if(!$arr_image_data[$v_name]['noreplace']){
                        unset($arr_images[$i]['noreplace']);
                        $v_change = true;
                    }
                }else{
                    if($arr_image_data[$v_name]['noreplace']){
                        $arr_images[$i]['noreplace'] = $v_change = true;
                    }
                }

                if($v_image_id>0){
                    $v_placeholder = isset($arr_images[$i]['placeholder'])?$arr_images[$i]['placeholder']:'none';
                    if($arr_image_data[$v_name]['placeholder']!='none'){
                        $arr_images[$i]['placeholder'] = $arr_image_data[$v_name]['placeholder'];
                        $v_change = true;
                    }else{
                        if(isset($arr_images[$i]['placeholder'])){
                            unset($arr_images[$i]['placeholder']);
                            $v_change = true;
                        }
                    }
                }
            }
        }
        if($v_change){
            $arr_canvases[$v_side]['images'] = $arr_images;
            $v_save_change = true;
        }
        //if($v_save_change)
        if($v_save_change){
            $arr_json['canvases'] = $arr_canvases;
            $v_template_data = json_encode($arr_json);
            $v_total_page = sizeof($arr_canvases);
            if($cls_tb_design_template->update_field('template_data', $v_template_data, array('template_id'=>$v_template_id))){
                $arr_return['message'] = 'Update successful!';
                $arr_return['success'] = 1;
                //Update Image

                $v_image_change = false;
                if($v_saved_dir=='' || !file_exists($v_saved_dir) || !is_dir($v_saved_dir)){
                    $v_saved_dir = str_replace(ROOT_DIR . DS, '', DESIGN_THEME_DIR);
                    $v_saved_dir = str_replace(DS,'/', $v_saved_dir).'/'.$v_template_id;
                    $v_accept = (file_exists($v_saved_dir) && is_writable($v_saved_dir)) || @mkdir($v_saved_dir);
                    if($v_accept) $v_saved_dir .= '/';
                    $v_image_change = true;
                }else $v_accept = file_exists($v_saved_dir) && is_writable($v_saved_dir);
                if($v_sample_image==''){
                    $v_ext = 'png';
                    $v_sample_image = 'template_'.$v_template_id.'_theme_0.'.$v_ext;
                    $v_image_change = true;
                }else
                    $v_ext = $cls_file->get_extension($v_sample_image);
                if($v_accept){
                    add_class('cls_draw');
                    add_class('cls_instagraph');
                    add_class('cls_tb_design_image');
                    $cls_draw = new cls_draw();
                    $cls_tmp_draw = new cls_draw();
                    $cls_images = new cls_tb_design_image($db, LOG_DIR);
                    $cls_instagraph = new cls_instagraph();
                    $image = new Imagick();
                    if($v_total_page<=1){
                        $v_page = 0;
                        $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);
                        $tmp_image = $cls_draw->create_square_thumb($image, 100);
                        $v_file_thumb = ROOT_DIR . DS. $v_saved_dir . 'thumb_'.$v_sample_image;
                        $tmp_image->setformat($v_ext);
                        $tmp_image->writeimage($v_file_thumb);
                        $tmp_image->clear();
                        $tmp_image->destroy();

                        $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);
                        $image->setimageformat($v_ext);
                        $image->writeimage(ROOT_DIR . DS . $v_saved_dir . $v_sample_image);
                        $image->clear();
                        $image->destroy();
                    }else{
                        $v_page = 1;
                        $image2 = new Imagick();
                        $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);
                        $v_page = 0;
                        $image1 = new Imagick();
                        $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);

                        $tmp_image = $cls_draw->create_square_thumb($image1, 100);
                        $v_file_thumb = ROOT_DIR .DS . $v_saved_dir . 'thumb_'.$v_sample_image;;
                        $tmp_image->setformat($v_ext);
                        $tmp_image->writeimage($v_file_thumb);
                        $tmp_image->clear();
                        $tmp_image->destroy();


                        $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

                        $image->setimageformat($v_ext);
                        $image->writeimage(ROOT_DIR . DS . $v_saved_dir . $v_sample_image);
                        $image->clear();
                        $image->destroy();
                        $image1->clear();
                        $image1->destroy();
                        $image2->clear();
                        $image2->destroy();
                    }
                    $arr_return['url'] = $v_saved_dir . $v_sample_image .'?'.time();
                    if($v_image_change){
                        $cls_tb_design_template->update_fields(array('saved_dir', 'sample_image'), array($v_saved_dir, $v_sample_image), array('template_id'=>$v_template_id));
                    }
                }

            }else{
                $arr_return['message'] = 'Update not successful!';
            }
        }else{
            $arr_return['message'] = 'Nothing has changed!';
        }
    }else{
        $arr_return['message'] = 'This template is not found!';
    }
}else{
    $arr_return['message'] = 'Template is negative!';
}

header("Content-type: application/json");
echo json_encode($arr_return);