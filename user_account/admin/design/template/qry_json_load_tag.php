<?php
if(!isset($v_sval)) die();

$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_all_tag = array();
if($v_company_id>=0){
    add_class('cls_tb_tag');
    $cls_tb_tag = new cls_tb_tag($db, LOG_DIR);
    $arr_tag = $cls_tb_tag->select(array('company_id'=>$v_company_id));
    foreach($arr_tag as $arr){
        $v_tag_id = isset($arr['tag_id'])?$arr['tag_id']:0;
        $v_tag_name = isset($arr['tag_name'])?$arr['tag_name']:'';
        $v_tag_parent = isset($arr['tag_parent'])?$arr['tag_parent']:0;
        if($v_tag_id>0 && $v_tag_name!=''){
            $arr_all_tag[] = array('tag_id'=>$v_tag_id, 'tag_name'=>$v_tag_name, 'tag_parent'=>$v_tag_parent);
        }
    }
}
header("Content-type: application/json");
echo json_encode($arr_all_tag);