<?php if(!isset($v_sval)) die();?>
<?php
$v_template_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_template_id, 'int');
if($v_template_id>0){
    add_class('cls_tb_design_design');
    $cls_designs = new cls_tb_design_design($db, LOG_DIR);
    $v_row = $cls_designs->select_one(array('template_id'=>$v_template_id));
    if($v_row==0){
        add_class('cls_tb_design_theme');
        $cls_themes = new cls_tb_design_theme($db, LOG_DIR);
        $v_result = $cls_themes->delete(array('template_id'=>$v_template_id));
        if($v_result){
            $v_row = $cls_tb_design_template->select_one( array('template_id' => $v_template_id));
            if($v_row==1){
                $v_template_data = $cls_tb_design_template->get_template_data();
                $v_result = $cls_tb_design_template->delete(array('template_id' => $v_template_id));
                if($v_result){
                    add_class('ManageFile', 'cls_file.php');
                    $mf = new ManageFile(DESIGN_THEME_DIR.DS.$v_template_id, DS);
                    $mf->remove_dir_all(DESIGN_THEME_DIR.DS.$v_template_id);

                    $arr_json = json_decode($v_template_data, true);
                    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
                    $v_size = sizeof($arr_canvases);
                    $arr_image_remove = array();
                    for($i=0; $i<$v_size; $i++){
                        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                        $v_image_size = sizeof($arr_images);
                        for($j=0; $j < $v_image_size; $j++){
                            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                            settype($v_image_id, 'int');
                            if($v_image_id > 0){
                                if(!isset($arr_image_remove[$v_image_id])){
                                    $arr_image_remove[$v_image_id] = array('template'=>-1, 'design'=>0);
                                }
                            }
                        }
                    }
                    add_class('cls_tb_design_image');
                    $cls_image = new cls_tb_design_image($db, LOG_DIR);
                    $v_total = $cls_image->update_image_used($arr_image_remove);
                }
            }
        }
    }
}
$_SESSION['ss_tb_design_template_redirect'] = 1;
redir(URL.$v_admin_key);
?>