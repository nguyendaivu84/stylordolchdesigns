<?php if(!isset($v_sval)) die();?>
<?php
$v_theme_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_theme_id, 'int');
$v_template_id = 0;
if($v_theme_id>0){
    add_class('cls_tb_design_theme');
    $cls_themes = new cls_tb_design_theme($db, LOG_DIR);
    $v_row = $cls_themes->select_one(array('theme_id'=>$v_theme_id));
    if($v_row==1){
        $v_sample_image = $cls_themes->get_sample_image();
        $v_save_dir = $cls_themes->get_saved_dir();
        $v_template_id = $cls_themes->get_template_id();
        $v_result = $cls_themes->delete(array('theme_id' => $v_theme_id));
        if($v_result){
            if(file_exists(ROOT_DIR.DS.$v_save_dir.$v_sample_image) && is_file(ROOT_DIR.DS.$v_save_dir.$v_sample_image)) @unlink(ROOT_DIR.DS.$v_save_dir.$v_sample_image);
        }
    }
}
$_SESSION['ss_tb_design_template_redirect'] = 1;
if($v_template_id==0)
    redir(URL.$v_admin_key);
else
    redir(URL.$v_admin_key.'/'.$v_template_id.'/edit');
?>