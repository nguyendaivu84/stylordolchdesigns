<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:'0';
$v_theme_id = isset($_POST['txt_theme_id'])?$_POST['txt_theme_id']:'0';

settype($v_template_id, 'int');
settype($v_theme_id, 'int');

$v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_template_id));

$v_message = 'Lost template ID';
$v_success = 0;
$v_url = '';
if($v_row==1){
    $v_work_dir = DESIGN_TEMP_DIR;
    add_class('ManageFile', 'cls_file.php');

    add_class('Template', 'xtemplate.class.php');

    $v_svg_template_data = 'svg_template_data_cmyk.svg';
    $v_svg_template_text = 'svg_template_text_cmyk.svg';
    $v_svg_template_image = 'svg_template_image_cmyk.svg';

    $arr_tpl = array($v_svg_template_text, $v_svg_template_image);
    $mf = new ManageFile($v_work_dir, DS);

    require ('lib/svglib/svglib.php');
    require ('lib/svglib/inkscape.php');
    $v_time = date('YmdHis').'_';
    $arr_files = array();


    add_class('cls_tb_design_image');
    add_class('cls_tb_design_font');
    add_class('cls_draw');
    add_class('cls_instagraph');

    $cls_image = new cls_tb_design_image($db, LOG_DIR);
    $cls_font = new cls_tb_design_font($db, LOG_DIR);
    $cls_draw = new cls_draw();
    $cls_instagraph = new cls_instagraph();

    $v_design_data = $cls_tb_design_template->get_template_data();
    $v_folding_type = $cls_tb_design_template->get_folding_type();
    $v_dpi = $cls_tb_design_template->get_template_dpi();

    $v_dpi = 90;

    $v_width_inch = $cls_tb_design_template->get_template_width();
    $v_height_inch = $cls_tb_design_template->get_template_height();

    $v_width = ceil($v_width_inch*$v_dpi);
    $v_height = ceil($v_height_inch*$v_dpi);
    $arr_json = json_decode($v_design_data, true);
    if(!is_array($arr_json)) $arr_json = array();
    $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();

    $v_pages = sizeof($arr_canvas);

    if(!in_array($v_pages, array(1,2))) $v_pages = 0;

    $v_theme_row = $cls_tb_design_theme->select_one(array('theme_id'=>$v_theme_id, 'template_id'=>$v_template_id));
    if($v_theme_row==1){
        $arr_theme_color = $cls_tb_design_theme->get_list_color();
    }
    if(!isset($arr_theme_color) || !is_array($arr_theme_color)) $arr_theme_color = array();
    if(count($arr_theme_color)>0){
        for($i=0; $i<$v_pages;$i++){

            $arr_images = isset($arr_canvas[$i]['images'])?$arr_canvas[$i]['images']:array();
            for($j=0;$j<count($arr_images);$j++){
                $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
                settype($v_image_id, 'int');
                if($v_image_id>0){

                    $v_key = 'name_'.$arr_images[$j]['name'];
                    if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                        $v_color = $arr_theme_color[$v_key]['color'];
                        $arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'];
                    }
                    $v_svg_id = isset($arr_images['svg_id'])?intval($arr_images['svg_id']):0;
                    if($v_svg_id>0){
                        $v_key = 'svg_'.$arr_images[$j]['name'];
                        if(isset($arr_theme_color[$v_key]['color'])){
                            $arr_svg_colors = $arr_theme_color[$v_key]['color'];
                            if(!is_array($arr_svg_colors)) $arr_svg_colors = array();
                            $arr_use_svg_color = array();
                            for($k = 0; $k<sizeof($arr_svg_colors); $k++){
                                $v_color = $arr_svg_colors[$k];
                                $arr_use_svg_color[] = $v_color;
                            }
                            $arr_images['svg_colors'] = $arr_use_svg_color;
                        }
                    }
                }else{
                    $v_key = 'fill_'.$arr_images[$j]['name'];
                    if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                        $v_color = $arr_theme_color[$v_key]['color'];
                        $arr_images[$j]['fill_color'] = $arr_theme_color[$v_key]['color'];
                    }
                    $v_key = 'border_'.$arr_images[$j]['name'];
                    if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                        $v_color = $arr_theme_color[$v_key]['color'];
                        $arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'];
                    }
                }
            }
            $arr_canvas[$i]['images'] = $arr_images;

            $arr_texts = isset($arr_canvas[$i]['texts'])?$arr_canvas[$i]['texts']:array();
            for($j=0;$j<count($arr_texts);$j++){
                $v_key = 'text_'.$arr_texts[$j]['name'];
                if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='text'){
                    $v_color = $arr_theme_color[$v_key]['color'];
                    $arr_texts[$j]['color'] = $v_color;
                }
            }
            $arr_canvas[$i]['texts'] = $arr_texts;
        }
        $arr_json['canvases'] = $arr_canvas;
    }

    $arr_files = array();
    for($k=0; $k<$v_pages; $k++){
        if($cls_draw->get_no_background()){
            $v_bg_color = 'ffffff';
        }else{
            $v_bg_color = isset($arr_canvas[$k]['bg_color'])?$arr_canvas[$k]['bg_color']:'ffffff';
            if(strlen($v_bg_color)!=6) $v_bg_color = 'ffffff';
        }
        $arr_cmyk = $cls_draw->hex2cymk($v_bg_color);
        $v_cmyk = $arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'];
        $v_bg_color = '#'.$v_bg_color;

        $v_side = 'side';
        if($v_pages==2){
            $v_side = $k==0?'_SIDE_A':'_SIDE_B';
        }

        $v_ext = 'svg';
        $v_file_name = $v_time.$v_side.'.'.$v_ext;
        $v_full_path = $v_work_dir.DS.$v_file_name;

        $tpl = new Template($v_svg_template_data, DESIGN_DATA_DIR);
        $tpl->set('WIDTH_PLACEHOLDER', $v_width);
        $tpl->set('HEIGHT_PLACEHOLDER', $v_height);
        $tpl->set('WIDTH_IN_PLACEHOLDER', $v_width_inch);
        $tpl->set('HEIGHT_IN_PLACEHOLDER', $v_height_inch);
        $tpl->set('BACKGROUND_PLACEHOLDER', $v_bg_color);
        $tpl->set('BACKGROUND_CMYK_PLACEHOLDER', $v_cmyk);
        $svg = new SVGDocument($tpl->output());

        //$tpl1 = new Template($v_svg_template_text, DESIGN_DATA_DIR);
        //$tpl2 = new Template($v_svg_template_image, DESIGN_DATA_DIR);
        //$tpl = array($tpl1, $tpl2);
        $svg = $cls_draw->create_svg($svg, $arr_tpl, $cls_image, $cls_font, $cls_instagraph, $arr_json, $v_dpi, $k, true);
        $svg->asXML($v_full_path, true);
        if(file_exists($v_full_path)) $arr_files[] = $v_full_path;

        //Convert from SVG to vector PDF
        if(file_exists($v_full_path)){
            $ink = new Inkscape($v_full_path);
            $v_full_path = str_replace($v_ext, 'pdf', $v_full_path);
            $ink->addParam('export-ignore-filters');
            //$ink->addParam('export-area-drawing');
            $ink->addParam('export-text-to-path');
            $arr_result_export = $ink->export('pdf', $v_full_path);
            if($arr_result_export['error']===0) $arr_files[] = $v_full_path;
        }
        //End: Convert from SVG to vector PDF
    }
    if(sizeof($arr_files)>0){
        $v_zip_file = $v_time.'output.zip';
        $mf->create_zip($arr_files, $v_work_dir.DS.$v_zip_file,true, array('pdf', 'xlsx'));

        //$v_url = str_replace(ROOT_DIR.DS,'', $v_work_dir);
        //$v_url = str_replace('\\','/', $v_url);
        $v_url = URL.'download/'.$v_zip_file;

        $v_success = 1;
        $v_message = 'OK';
    }else{
        $v_success = 0;
        $v_message = 'Empty data!';
        $v_url='';
    }

}

$arr_return = array('success'=>$v_success, 'link'=>$v_url, 'message'=>$v_message);
$cls_output->output($arr_return, true, false);