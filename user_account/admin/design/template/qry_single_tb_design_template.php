<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_template_id = 0;
$v_original_id = 0;
$v_template_name = '';
$v_template_width = 0;
$v_template_height = 0;
$v_folding_type = 0;
$v_folding_direction = 0;
$v_die_cut_type = 0;
$v_stock_cost = 0;
$v_markup_cost = 0;
$v_print_cost = 0;
$v_template_type = 0;
$v_category_id = 0;
$v_section_id = 0;
$v_industry_id = 0;
$v_style_id = 0;
$arr_site_id = array();
$v_template_bleed = 0.0;
$v_template_status = 0;
$v_created_time = date('Y-m-d H:i:s', time());
$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
$v_is_already = 1;
$v_is_home = 1;
$arr_product_list = array();
$v_location_id = 0;
$v_company_id = isset($_SESSION['ss_last_company_id'])?$_SESSION['ss_last_company_id']:0;
$v_template_desc = '';
$arr_template_tag = array();
$v_template_order = 0;
$v_template_group = 0;
$v_template_design = false;
$v_new_design_template = true;

$v_template_url = '';
$v_sample_image = '';

$v_template_data = '';

$v_template_sample = '';
if(isset($_POST['btn_submit_tb_design_template'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_design_template->set_mongo_id($v_mongo_id);
	$v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:$v_template_id;
	if(is_null($v_mongo_id)){
		//$v_template_id = $cls_tb_design_template->select_next('template_id');
	}
	$v_template_id = (int) $v_template_id;
	$cls_tb_design_template->set_template_id($v_template_id);
	$v_template_name = isset($_POST['txt_template_name'])?$_POST['txt_template_name']:$v_template_name;
	$v_template_name = trim($v_template_name);
	if($v_template_name=='') $v_error_message .= '[Template Name] is empty!<br />';
	$cls_tb_design_template->set_template_name($v_template_name);

    $v_template_bleed = isset($_POST['txt_template_bleed'])?$_POST['txt_template_bleed']:$v_template_bleed;
    $v_template_bleed = (float) $v_template_bleed;
    if($v_template_bleed<0) $v_template_bleed = 0;
    $cls_tb_design_template->set_template_bleed($v_template_bleed);

	$v_template_width = isset($_POST['txt_template_width'])?$_POST['txt_template_width']:$v_template_width;
	$v_template_width = (float) $v_template_width;
	if($v_template_width<=0) $v_error_message .= '[Template Width] is negative!<br />';
    //$v_template_width += $v_template_bleed * 2;
	$cls_tb_design_template->set_template_width($v_template_width);
	$v_template_height = isset($_POST['txt_template_height'])?$_POST['txt_template_height']:$v_template_height;
	$v_template_height = (float) $v_template_height;
	if($v_template_height<=0) $v_error_message .= '[Template Height] is negative!<br />';
    //$v_template_height += $v_template_bleed * 2;
	$cls_tb_design_template->set_template_height($v_template_height);
	$v_folding_type = isset($_POST['txt_folding_type'])?$_POST['txt_folding_type']:$v_folding_type;
	$v_folding_type = (int) $v_folding_type;
	if($v_folding_type<0) $v_error_message .= '[Folding Type] is negative!<br />';
	$cls_tb_design_template->set_folding_type($v_folding_type);
    $v_folding_direction = isset($_POST['txt_folding_direction'])?$_POST['txt_folding_direction']:$v_folding_direction;
    $v_folding_direction = (int) $v_folding_direction;
    if($v_folding_direction<0) $v_error_message .= '[Folding Direction] is negative!<br />';
    $cls_tb_design_template->set_folding_direction($v_folding_direction);
	$v_die_cut_type = isset($_POST['txt_die_cut_type'])?$_POST['txt_die_cut_type']:$v_die_cut_type;
	$v_die_cut_type = (int) $v_die_cut_type;
	if($v_die_cut_type<0) $v_error_message .= '[Die Cut Type] is negative!<br />';
	$cls_tb_design_template->set_die_cut_type($v_die_cut_type);
	$v_stock_cost = isset($_POST['txt_stock_cost'])?$_POST['txt_stock_cost']:$v_stock_cost;
	$v_stock_cost = (float) $v_stock_cost;
	$cls_tb_design_template->set_stock_cost($v_stock_cost);
	$v_markup_cost = isset($_POST['txt_markup_cost'])?$_POST['txt_markup_cost']:$v_markup_cost;
	$v_markup_cost = (float) $v_markup_cost;
	if($v_markup_cost<0) $v_error_message .= '[Markup Cost] is negative!<br />';
	$cls_tb_design_template->set_markup_cost($v_markup_cost);
	$v_print_cost = isset($_POST['txt_print_cost'])?$_POST['txt_print_cost']:$v_print_cost;
	$v_print_cost = (float) $v_print_cost;
	if($v_print_cost<0) $v_error_message .= '[Print Cost] is negative!<br />';
	$cls_tb_design_template->set_print_cost($v_print_cost);
	$v_template_type = isset($_POST['txt_template_type'])?$_POST['txt_template_type']:$v_template_type;
	$v_template_type = (int) $v_template_type;
	if($v_template_type<0) $v_error_message .= '[Template Type] is negative!<br />';
	$cls_tb_design_template->set_template_type($v_template_type);
	$v_template_status = isset($_POST['txt_template_status'])?0:1;
	$cls_tb_design_template->set_template_status($v_template_status);
	$v_created_time = date('Y-m-d H:i:s');
	$cls_tb_design_template->set_created_time($v_created_time);
    $arr_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:array();
    if(!is_array($arr_site_id)) $arr_site_id = array();
    for($i=0; $i<sizeof($arr_site_id);$i++)
        $arr_site_id[$i] = (int) $arr_site_id[$i];
    $cls_tb_design_template->set_site_id($arr_site_id);
	$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:'0';
	$v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:'';
    settype($v_user_id, 'int');
	$cls_tb_design_template->set_user_id($v_user_id);
	$v_user_name = trim($v_user_name);
	$cls_tb_design_template->set_user_name($v_user_name);
	$v_is_already = 1;
	$cls_tb_design_template->set_is_already($v_is_already);
    $v_is_home = isset($_POST['txt_is_home'])?1:0;
    $cls_tb_design_template->set_is_home($v_is_home);
	$arr_product_list = isset($_POST['txt_product_list'])?$_POST['txt_product_list']:$arr_product_list;
    if(!is_array($arr_product_list))
        $arr_product_list = array();
    else{
        for($i=0; $i<count($arr_product_list); $i++){
            $arr_product_list[$i] = (int) $arr_product_list[$i];
        }
    }
	$cls_tb_design_template->set_product_list($arr_product_list);
	$v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
	$v_location_id = (int) $v_location_id;
	if($v_location_id<0) $v_location_id = 0;
	$cls_tb_design_template->set_location_id($v_location_id);
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
	$v_company_id = (int) $v_company_id;
	if($v_company_id<0) $v_company_id = 0;
	$cls_tb_design_template->set_company_id($v_company_id);
	$v_template_desc = isset($_POST['txt_template_desc'])?$_POST['txt_template_desc']:$v_template_desc;
	$v_template_desc = trim($v_template_desc);
	$cls_tb_design_template->set_template_desc($v_template_desc);
	$arr_template_tag = isset($_POST['txt_template_tag'])?$_POST['txt_template_tag']:$arr_template_tag;

    if(!is_array($arr_template_tag))
        $arr_template_tag = array();
    else{
        for($i=0; $i<count($arr_template_tag); $i++){
            $arr_template_tag[$i] = (int) $arr_template_tag[$i];
        }
    }
	$cls_tb_design_template->set_template_tag($arr_template_tag);
	$v_template_order = isset($_POST['txt_template_order'])?$_POST['txt_template_order']:$v_template_order;
	$v_template_order = (int) $v_template_order;
	$cls_tb_design_template->set_template_order($v_template_order);
	$v_template_group = isset($_POST['txt_template_group'])?$_POST['txt_template_group']:$v_template_group;
	$v_template_group = (int) $v_template_group;
	if($v_template_group<0) $v_error_message .= '[Template Group] is negative!<br />';
	$cls_tb_design_template->set_template_group($v_template_group);

    $v_category_id = isset($_POST['txt_category_id'])?$_POST['txt_category_id']:$v_category_id;
    $v_category_id = (int) $v_category_id;
    $cls_tb_design_template->set_category_id($v_category_id);

    $v_section_id = isset($_POST['txt_section_id'])?$_POST['txt_section_id']:$v_section_id;
    $v_section_id = (int) $v_section_id;
    $cls_tb_design_template->set_section_id($v_section_id);

    $v_style_id = isset($_POST['txt_style_id'])?$_POST['txt_style_id']:$v_style_id;
    $v_style_id = (int) $v_style_id;
    $cls_tb_design_template->set_style_id($v_style_id);

    $v_industry_id = isset($_POST['txt_industry_id'])?$_POST['txt_industry_id']:$v_industry_id;
    $v_industry_id = (int) $v_industry_id;
    $cls_tb_design_template->set_industry_id($v_industry_id);

	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_template_id = $cls_tb_design_template->insert();
			$v_result = $v_template_id > 0;
		}else{
            $v_template_data = $cls_tb_design_template->select_scalar('template_data', array('_id' => $v_mongo_id));
            $arr_old_json = json_decode($v_template_data, true);
            $arr_old_canvases = isset($arr_old_json['canvases'])?$arr_old_json['canvases']:array();

            if(count($arr_old_canvases)>0){
                $arr_tmp = array(
                    'texts'=>array()
                    ,'images'=>array()
                    ,'width'=>$v_template_width
                    ,'height'=>$v_template_height
                    ,'bg_color'=> "FFFFFF"
                    ,'bleed'=>	$v_template_bleed
                    ,'droppable'=> true
                    ,'name'=> "Front Side"
                    ,"isViewed"=>false
                );

                $arr_canvases = array();
                foreach($arr_old_canvases as $arr){
                    $arr_canvases[] = $arr;
                }
                $v_count_canvases = count($arr_canvases);
                $v_canvas_new_height = $v_template_height + 2 * $v_template_bleed;
                $v_canvas_new_width = $v_template_width + 2 * $v_template_bleed;
                if($v_folding_type<1){
                    if($v_count_canvases < 1){
                        $arr_canvases = array($arr_tmp);
                    }else{
                        $arr_canvases[0]['bleed'] = $v_template_bleed;
                        $v_canvas_width = isset($arr_canvases[0]['width'])?$arr_canvases[0]['width']:0;
                        $v_canvas_height = isset($arr_canvases[0]['height'])?$arr_canvases[0]['height']:0;
                        if($v_canvas_width!=$v_canvas_new_width) $arr_canvases[0]['width'] = $v_canvas_new_width;
                        if($v_canvas_height!=$v_canvas_new_height) $arr_canvases[0]['height'] = $v_canvas_new_height;
                        $arr_canvases = array($arr_canvases[0]);
                    }
                }else if($v_folding_type==1){
                    if($v_count_canvases<1){
                        $arr_canvases = array($arr_tmp, $arr_tmp);
                        $arr_canvases[1]['name'] = 'Back Side';
                    }else if($v_count_canvases>2){
                        $arr_canvases[0]['bleed'] = $v_template_bleed;
                        $arr_canvases[1]['bleed'] = $v_template_bleed;
                        $v_canvas_width = isset($arr_canvases[0]['width'])?$arr_canvases[0]['width']:0;
                        $v_canvas_height = isset($arr_canvases[0]['height'])?$arr_canvases[0]['height']:0;
                        if($v_canvas_width!=$v_canvas_new_width){ $arr_canvases[0]['width'] = $v_canvas_new_width; $arr_canvases[1]['width'] = $v_canvas_new_width;}
                        if($v_canvas_height!=$v_canvas_new_height){ $arr_canvases[0]['height'] = $v_canvas_new_height;  $arr_canvases[1]['height'] = $v_canvas_new_height;}
                        $arr_canvases = array($arr_canvases[0], $arr_canvases[1]);
                    }else{
                        $v_canvas_width = isset($arr_canvases[0]['width'])?$arr_canvases[0]['width']:0;
                        $v_canvas_height = isset($arr_canvases[0]['height'])?$arr_canvases[0]['height']:0;
                        if($v_canvas_width!=$v_canvas_new_width){ $arr_canvases[0]['width'] = $v_canvas_new_width; $arr_canvases[1]['width'] = $v_canvas_new_width;}
                        if($v_canvas_height!=$v_canvas_new_height){ $arr_canvases[0]['height'] = $v_canvas_new_height;  $arr_canvases[1]['height'] = $v_canvas_new_height;}
                        $arr_canvases[0]['bleed'] = $v_template_bleed;
                        $arr_canvases[1]['bleed'] = $v_template_bleed;
                    }
                }else{
                    if($v_count_canvases<1){
                        $arr_canvases = array($arr_tmp, $arr_tmp);
                    }else if($v_count_canvases>2){
                        $arr_canvases = array($arr_canvases[0], $arr_canvases[1]);
                    }
                    $v_canvas_width = isset($arr_canvases[0]['width'])?$arr_canvases[0]['width']:0;
                    $v_canvas_height = isset($arr_canvases[0]['height'])?$arr_canvases[0]['height']:0;
                    if($v_canvas_width!=$v_canvas_new_width){ $arr_canvases[0]['width'] = $v_canvas_new_width; $arr_canvases[1]['width'] = $v_canvas_new_width;}
                    if($v_canvas_height!=$v_canvas_new_height){ $arr_canvases[0]['height'] = $v_canvas_new_height;  $arr_canvases[1]['height'] = $v_canvas_new_height;}
                    $arr_canvases[0]['bleed'] = $v_template_bleed;
                    $arr_canvases[1]['bleed'] = $v_template_bleed;
                    $arr_canvases[0]['name'] = 'Outside';
                    $arr_canvases[1]['name'] = 'Inside';
                }


                $arr_old_json['canvases'] = $arr_canvases;
                $v_json_width = isset($arr_old_json['width'])?$arr_old_json['width']:0;
                $v_json_height = isset($arr_old_json['height'])?$arr_old_json['height']:0;
                if($v_json_width!=$v_template_width)
                    $arr_old_json['width'] = $v_template_width;
                if($v_json_height!=$v_template_height)
                    $arr_old_json['height'] = $v_template_height;
                $v_template_data = json_encode($arr_old_json);
            }else{
                $v_template_data = json_encode(array());
            }

            $arr_fields = array('company_id', 'location_id', 'template_width', 'template_height', 'template_name', 'folding_type', 'folding_direction', 'die_cut_type', 'markup_cost', 'print_cost', 'template_desc', 'template_status', 'template_bleed', 'category_id', 'style_id', 'industry_id', 'site_id', 'template_data');
            $arr_values = array($v_company_id, $v_location_id, $v_template_width, $v_template_height, $v_template_name, $v_folding_type, $v_folding_direction, $v_die_cut_type, $v_markup_cost, $v_print_cost, $v_template_desc, $v_template_status, $v_template_bleed, $v_category_id, $v_style_id, $v_industry_id, $arr_site_id, $v_template_data);
			$v_result = $cls_tb_design_template->update_fields($arr_fields, $arr_values, array('_id' => $v_mongo_id));
			$v_new_design_template = false;
		}
		if($v_result){
            if(isset($_SESSION['ss_tb_design_template'])) unset($_SESSION['ss_tb_design_template']);
			$_SESSION['ss_tb_design_template_redirect'] = 1;
            $_SESSION['ss_save_tb_design_template_id'] = $v_template_id;
            $v_template_design = isset($_POST['txt_template_design']);
            if(!$v_template_design)
			    redir(URL.$v_admin_key);
            else{
                $_SESSION['ss_template_design_checked'] = $v_template_design?1:0;
                //die ('DESIGN: '.isset($_SESSION['ss_template_design_checked']));
                redir(URL.$v_admin_key.'/'.$v_template_id.'/edit');
            }
		}else{
			if($v_new_design_template) $v_template_id = 0;
		}
	}
}else{
	$v_template_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_template_id,'int');
	if($v_template_id>0){
		$v_row = $cls_tb_design_template->select_one(array('template_id' => $v_template_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_design_template->get_mongo_id();
			$v_template_id = $cls_tb_design_template->get_template_id();
			$v_template_name = $cls_tb_design_template->get_template_name();
			$v_template_data = $cls_tb_design_template->get_template_data();
			$v_template_width = $cls_tb_design_template->get_template_width();
			$v_template_height = $cls_tb_design_template->get_template_height();
			$v_folding_type = $cls_tb_design_template->get_folding_type();
			$v_die_cut_type = $cls_tb_design_template->get_die_cut_type();
			$v_stock_cost = $cls_tb_design_template->get_stock_cost();
			$v_markup_cost = $cls_tb_design_template->get_markup_cost();
			$v_print_cost = $cls_tb_design_template->get_print_cost();
			$v_template_type = $cls_tb_design_template->get_template_type();
			$v_template_status = $cls_tb_design_template->get_template_status();
			$v_created_time = date('Y-m-d H:i:s',$cls_tb_design_template->get_created_time());
			$v_user_id = $cls_tb_design_template->get_user_id();
			$v_user_name = $cls_tb_design_template->get_user_name();
			$v_is_already = $cls_tb_design_template->get_is_already();
			$v_is_home = $cls_tb_design_template->get_is_home();
			$arr_product_list = $cls_tb_design_template->get_product_list();
			$v_location_id = $cls_tb_design_template->get_location_id();
			$v_company_id = $cls_tb_design_template->get_company_id();
			$v_template_desc = $cls_tb_design_template->get_template_desc();
			$arr_template_tag = $cls_tb_design_template->get_template_tag();
			$v_template_order = $cls_tb_design_template->get_template_order();
			$v_template_group = $cls_tb_design_template->get_template_group();
			$v_template_bleed = $cls_tb_design_template->get_template_bleed();
			$v_category_id = $cls_tb_design_template->get_category_id();
			$v_section_id = $cls_tb_design_template->get_section_id();
			$v_style_id = $cls_tb_design_template->get_style_id();
			$v_industry_id = $cls_tb_design_template->get_industry_id();
			$v_saved_dir = $cls_tb_design_template->get_saved_dir();
			$v_sample_image = $cls_tb_design_template->get_sample_image();
			$v_original_id = $cls_tb_design_template->get_original_id();
			$arr_site_id = $cls_tb_design_template->get_site_id();
            $v_template_url = URL.$v_saved_dir;

		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);

$v_user_full_name = $v_user_name;
if($v_user_id>0){
    $v_contact_id = isset($arr_user['contact_id'])?$arr_user['contact_id']:'0';
    settype($v_contact_id, 'int');
    if($v_contact_id>0) $v_user_full_name = $cls_tb_contact->get_full_name_contact($v_contact_id);
}
if($v_user_full_name=='') $v_user_full_name = $v_user_name;

$arr_themes = array();
$v_count_design = array();
$v_image_missing = 0;
$arr_list_element = array();

$arr_saved_data = array();
$arr_saved_flag = array();

if($v_template_id>0){
    $v_template_sample = ($v_template_url!='' && $v_sample_image!='')?$v_template_url.$v_sample_image:'';
    add_class('cls_tb_design_theme');
    $cls_theme = new cls_tb_design_theme($db, LOG_DIR);
    $arr_theme = $cls_theme->select(array('template_id'=>$v_template_id), array('theme_order'=>1, 'theme_id'=>1));
    $v_row_order = 0;
    foreach($arr_theme as $arr){
        $v_theme_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
        $v_theme_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
        $v_theme_name = isset($arr['theme_name'])?$arr['theme_name']:'';
        $v_theme_color = isset($arr['theme_color'])?$arr['theme_color']:'ffffff';
        $v_theme_status = isset($arr['theme_status'])?$arr['theme_status']:0;
        $v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
        $v_theme_order = isset($arr['theme_order'])?$arr['theme_order']:0;
        $v_add_markup_cost = isset($arr['add_markup_cost'])?$arr['add_markup_cost']:0;
        if($v_theme_saved_dir=='' || $v_theme_sample_image=='')
            $v_theme_sample = $v_template_sample;
        else
            $v_theme_sample = URL.$v_theme_saved_dir.$v_theme_sample_image.'?'.time();
        $arr_themes[] = array(
            'row_order'=>++$v_row_order
            ,'theme_name'=>$v_theme_name
            ,'theme_id'=>$v_theme_id
            ,'theme_order'=>$v_theme_order
            ,'sample_image'=>$v_theme_sample
            ,'add_markup_cost'=>$v_add_markup_cost
            ,'theme_status'=>$v_theme_status==0
            ,'theme_color'=>'#'.$v_theme_color
        );
    }

    add_class('cls_tb_design_design');
    $cls_designs = new cls_tb_design_design($db, LOG_DIR);
    $v_count_design = $cls_designs->count(array('template_id'=>$v_template_id));


    //List element
    add_class('cls_tb_design_image');
    $cls_images = new cls_tb_design_image($db, LOG_DIR);
    add_class('cls_draw');
    $cls_draw = new cls_draw();
    $arr_json = json_decode($v_template_data, true);
    if(!is_array($arr_json)) $arr_json = array();
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    $v_delete_icon = URL . 'images/icons/delete.png';
    for($i=0; $i<sizeof($arr_canvases);$i++){
        $v_text = '';
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
        for($j=0; $j<sizeof($arr_texts);$j++){
            $v_name = isset($arr_texts[$j]['name'])?$arr_texts[$j]['name']:'NoName';
            $v_one = '';
            if(($v_is_super_admin || $v_edit_right) && $v_act=='E')
                $v_text .= '<tr data-name="'.$v_name.'"><td><strong>Name</strong>: '.$v_name.'</td><td><img src="'.$v_delete_icon.'" title="Delete Text\'s object" style="cursor: pointer" data-action="delete" data-name="'.$v_name.'" data-type="text" data-side="'.$i.'" data-template="'.$v_template_id.'" /></td>';
            else
                $v_text .= '<tr data-name="'.$v_name.'"><td><strong>Name</strong>: '.$v_name.'</td><td>&nbsp;</td>';

            $v_top = isset($arr_texts[$j]['top'])?floatval($arr_texts[$j]['top']):0;
            $v_left = isset($arr_texts[$j]['left'])?floatval($arr_texts[$j]['left']):0;
            $v_one .= '<label style="font-weight: bold" for="txt_top_'.$v_name.'">Top:</label> <input data-object="text" data-field="text" type="text" data-key="top" data-name="'.$v_name.'" id="txt_top_'.$v_name.'" value="'.$v_top.'" /> --- ';
            $v_one .= '<label style="font-weight: bold" for="txt_left_'.$v_name.'">Left:</label> <input data-object="text" data-field="text" data-key="left" data-name="'.$v_name.'" type="text" id="txt_left_'.$v_name.'" value="'.$v_left.'" /> --- ';
            $v_one .= '<span class="checkbox"><input data-object="text" data-field="checkbox" type="checkbox" id="based_'.$v_name.'" data-name="'.$v_name.'" data-key="based"'.(isset($arr_texts[$j]['based']) && $arr_texts[$j]['based']?' checked="checked"':'').' /><label for="based_'.$v_name.'">Based to background</label></span> --- ';
            $v_one .= '<span class="checkbox"><input data-object="text" data-field="checkbox" type="checkbox" id="ontop_'.$v_name.'" data-name="'.$v_name.'" data-key="ontop"'.(isset($arr_texts[$j]['ontop']) && $arr_texts[$j]['ontop']?' checked="checked"':'').' /><label for="based_'.$v_name.'">On top</label></span> <br /><br />';
            $v_text_title = isset($arr_texts[$j]['title'])?urldecode($arr_texts[$j]['title']):'';
            $v_text_preset = isset($arr_texts[$j]['preset_text'])?urldecode($arr_texts[$j]['preset_text']):'';
            $v_text_body = isset($arr_texts[$j]['body'])?urldecode($arr_texts[$j]['body']):'';
            $v_one .= '<strong>Title</strong>: <input type="text" data-object="text" data-field="text" data-key="title" data-name="'.$v_name.'" class="k-textbox" value="'.$v_text_title.'" /><input type="hidden" data-key="title" data-name="'.$v_name.'" value="'.$v_text_title.'" /> &nbsp;&nbsp;&nbsp;';
            $v_one .= '<strong>Preset</strong>: <input type="text" data-object="text" data-field="text" style="width:300px" data-key="preset" data-name="'.$v_name.'" class="k-textbox" value="'.$v_text_preset.'" /><input type="hidden" data-key="preset" data-name="'.$v_name.'" value="'.$v_text_preset.'" /> &nbsp;&nbsp;&nbsp;';
            $v_one .= '<strong>Body</strong>: <input type="text" data-object="text" data-field="text" style="width:300px" data-key="body" data-name="'.$v_name.'" class="k-textbox" value="'.urldecode($arr_texts[$j]['body']).'" /><input type="hidden" data-key="body" data-name="'.$v_name.'" value="'.urldecode($arr_texts[$j]['body']).'" /> &nbsp;&nbsp;&nbsp;';
            /*
            foreach($arr_texts[$j] as $key=>$value)
                if($key!='name'){
                    $v_one .= '<strong>'.$key.'</strong>: '.$value.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                }
            */
            if($v_one!=''){
                $v_text .= '<td>'.$v_one.'</td></tr>';
                $arr_saved_data['text'][$v_name] = array(
                    /*'type'=>'text', 'name'=>$v_name,*/ 'top'=>$v_top, 'left'=>$v_left
                    ,'title'=>$v_text_title, 'preset'=>$v_text_preset, 'body'=>$v_text_body
                    ,'based'=>isset($arr_texts[$j]['based']) && $arr_texts[$j]['based']
                    ,'ontop'=>isset($arr_texts[$j]['ontop']) && $arr_texts[$j]['ontop']
                );
                $arr_saved_flag['text'][$v_name] = array(
                    'top'=>true, 'left'=>true, 'title'=> true, 'preset'=>true, 'body'=>true, 'based'=>true, 'ontop'=>true, 'side'=>($i+1)
                );
            }
        }
        if($v_text!='')
            $arr_list_element[$i]['text'] = '<table data-type="text" data-side="'.$i.'" class="tbl_single"><caption style="text-align:left; font-weight: bold; font-size: 120%">Text</caption>'.$v_text.'</table>';

        $v_image = '';
        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
        for($j=0; $j<sizeof($arr_images);$j++){
            $k = sizeof($arr_saved_data);

            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
            $v_cropWidth = isset($arr_images[$j]['cropWidth'])?$arr_images[$j]['cropWidth']:1;
            $v_cropHeight = isset($arr_images[$j]['cropHeight'])?$arr_images[$j]['cropHeight']:1;
            $v_disabled_crop = $v_cropWidth!=1 || $v_cropHeight!=1;
            $v_crop = isset($arr_images[$j]['crop'])?boolval($arr_images[$j]['crop']):false;
            $v_image_thumb = '';
            $v_disabled = false;
            $v_placeholder = isset($arr_images[$j]['placeholder'])?$arr_images[$j]['placeholder']:'none';
            if(!in_array($v_placeholder, array('fitter', 'filler'))) $v_placeholder = 'none';
            if($v_placeholder!='none')  $arr_images[$j]['noprint'] = $v_disabled = true;
            settype($v_image_id, 'int');
            if($v_image_id>0){
                $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
                if($v_row==1){
                    $v_image_thumb = $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
                    if($v_image_thumb!='') $v_image_thumb = '<br /><img src="'.$v_image_thumb.'" style="max-width: 150px" />';
                }else{
                    $v_image_missing++;
                    $v_image_thumb = '<br /><label class="k-required">(This image has been removed. Template could not be launched)</label>';
                }
                //$arr_saved_data[$k]['type'] = 'image';
            }
            if($v_image_id<=0){
                //$arr_saved_data[$k]['type'] = 'shape';
                $v_image_thumb = isset($arr_images[$j]['shape_type'])?$arr_images[$j]['shape_type']:'';
                if($v_image_thumb!='') $v_image_thumb = '<br /><strong>Type: </strong>'.$v_image_thumb;
            }
            //$arr_saved_data[$k]['id'] = $v_image_id;
            $v_one = '';
            $v_name = isset($arr_images[$j]['name'])?$arr_images[$j]['name']:'NoName';
            if(($v_is_super_admin || $v_edit_right) && $v_act=='E')
                $v_image .= '<tr data-name="'.$v_name.'"><td><strong>Name</strong>: '.$v_name.($v_image_thumb!=''?$v_image_thumb:'').'</td><td><img src="'.$v_delete_icon.'" style="cursor: pointer" title="Delete '.($v_image_id>0?"image":"shape").'\'s object" data-action="delete" data-name="'.$v_name.'" data-type="'.($v_image_id>0?'image':'shape').'" data-side="'.$i.'" data-template="'.$v_template_id.'" /></td>';
            else
                $v_image .= '<tr data-name="'.$v_name.'"><td><strong>Name</strong>: '.$v_name.($v_image_thumb!=''?$v_image_thumb:'').'</td><td>&nbsp;</td>';
            $v_top = isset($arr_images[$j]['top'])?floatval($arr_images[$j]['top']):0;
            $v_left = isset($arr_images[$j]['left'])?floatval($arr_images[$j]['left']):0;
            //$arr_saved_data[$k]['name'] = $v_name;
            $v_one .= '<label style="font-weight: bold" for="txt_top_'.$v_name.'">Top:</label> <input data-object="'.($v_image_id>0?'image':'shape').'" data-field="text" data-key="top" data-name="'.$v_name.'" type="text" id="txt_top_'.$v_name.'" value="'.$v_top.'" /> --- ';
            $v_one .= '<label style="font-weight: bold" for="txt_left_'.$v_name.'">Left:</label> <input data-object="'.($v_image_id>0?'image':'shape').'" data-field="text" data-key="left" data-name="'.$v_name.'" type="text" id="txt_left_'.$v_name.'" value="'.$v_left.'" /> --- ';
            $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="based_'.$v_name.'" data-name="'.$v_name.'" data-key="based"'.(isset($arr_images[$j]['based']) && $arr_images[$j]['based']?' checked="checked"':'').' /><label for="based_'.$v_name.'">Based to background</label></span> --- ';
            $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="ontop_'.$v_name.'" data-name="'.$v_name.'" data-key="ontop"'.(isset($arr_images[$j]['ontop']) && $arr_images[$j]['ontop']?' checked="checked"':'').' /><label for="based_'.$v_name.'">On top</label></span> <br /><br />';
            /*
            if(isset($arr_images[$j]['uploader']))
                $v_one .= '<span class="checkbox"><input type="checkbox" id="uploader_'.$v_name.'" data-name="'.$v_name.'" data-key="uploader"'.($arr_images[$j]['uploader']?' checked="checked"':'').' /><label for="uploader_'.$v_name.'">Uploader</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            else
                $v_one .= '<span class="checkbox"><input type="checkbox" id="uploader_'.$v_name.'" data-name="'.$v_name.'" data-key="uploader" /><label for="uploader_'.$v_name.'">Uploader</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            */
            if(isset($arr_images[$j]['locked']))
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="locked_'.$v_name.'" data-name="'.$v_name.'" data-key="locked"'.($arr_images[$j]['locked']?' checked="checked"':'').' /><label for="locked_'.$v_name.'">Locked</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            else
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="locked_'.$v_name.'" data-name="'.$v_name.'" data-key="locked" /><label for="locked_'.$v_name.'">Locked</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            if(isset($arr_images[$j]['noselect']))
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noselect_'.$v_name.'" data-name="'.$v_name.'" data-key="noselect"'.($arr_images[$j]['noselect']?' checked="checked"':'').' /><label for="noselect_'.$v_name.'">No Select</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            else
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noselect_'.$v_name.'" data-name="'.$v_name.'" data-key="noselect" /><label for="noselect_'.$v_name.'">No Select</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            if(isset($arr_images[$j]['noresize']))
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noresize_'.$v_name.'" data-name="'.$v_name.'" data-key="noresize"'.($arr_images[$j]['noresize']?' checked="checked"':'').' /><label for="noresize_'.$v_name.'">No Resize</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            else
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noresize_'.$v_name.'" data-name="'.$v_name.'" data-key="noresize" /><label for="noresize_'.$v_name.'">No Resize</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            if(isset($arr_images[$j]['unmovable']))
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="unmovable_'.$v_name.'" data-name="'.$v_name.'" data-key="unmovable"'.($arr_images[$j]['unmovable']?' checked="checked"':'').' /><label for="unmovable_'.$v_name.'">Unmovable</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            else
                $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="unmovable_'.$v_name.'" data-name="'.$v_name.'" data-key="unmovable" /><label for="unmovable_'.$v_name.'">Unmovable</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';

            if($v_image_id>0){
                if(isset($arr_images[$j]['noprint']))
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" '.($v_disabled?'disabled="disabled"':'').' type="checkbox" id="noprint_'.$v_name.'" data-name="'.$v_name.'" data-key="noprint"'.($arr_images[$j]['noprint']?' checked="checked"':'').' /><label for="noprint_'.$v_name.'">No Print</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                else
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noprint_'.$v_name.'" data-name="'.$v_name.'" data-key="noprint" /><label for="noprint_'.$v_name.'">No Print</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                $v_one .= ' -- <label for="placeholder_'.$v_name.'">Upload for placeholder: &nbsp;</label>';
                $v_one .= '<select data-object="'.($v_image_id>0?'image':'shape').'" data-field="select" data-name="'.$v_name.'" data-key="placeholder" id="placeholder_'.$v_name.'">';
                $v_one .= '<option value="none"'.((!isset($arr_images[$j]['placeholder']) || ($arr_images[$j]['placeholder']=='none'))?' selected="selected"':'').'>None</option>';
                $v_one .= '<option value="filler"'.((isset($arr_images[$j]['placeholder']) && ($arr_images[$j]['placeholder']=='filler'))?' selected="selected"':'').'>Filler</option>';
                $v_one .= '<option value="fitter"'.((isset($arr_images[$j]['placeholder']) && ($arr_images[$j]['placeholder']=='fitter'))?' selected="selected"':'').'>Fitter</option>';
                $v_one .= '</select>';
                $v_one .= '<br /><br />';
                if($v_crop)
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="crop_'.$v_name.'" data-name="'.$v_name.'"'.($v_disabled_crop?' disabled="disabled"':'').' data-key="crop" checked="checked" /><label for="crop_'.$v_name.'">Crop</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                else
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="crop_'.$v_name.'" data-name="'.$v_name.'" data-key="crop" /><label for="crop_'.$v_name.'">Crop</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                if(isset($arr_images[$j]['release']))
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="release_'.$v_name.'"'.($v_crop?'':' disabled="disabled"').' data-name="'.$v_name.'" data-key="release"'.($arr_images[$j]['release']?' checked="checked"':'').' /><label for="release_'.$v_name.'">Allow release</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                else
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="release_'.$v_name.'"'.($v_crop?'':' disabled="disabled"').' data-name="'.$v_name.'" data-key="release" /><label for="release_'.$v_name.'">Allow release</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                if(isset($arr_images[$j]['noreplace']))
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noreplace_'.$v_name.'"'.($v_crop?'':' disabled="disabled"').' data-name="'.$v_name.'" data-key="noreplace"'.($arr_images[$j]['noreplace']?' checked="checked"':'').' /><label for="noreplace_'.$v_name.'">No Replace</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
                else
                    $v_one .= '<span class="checkbox"><input data-object="'.($v_image_id>0?'image':'shape').'" data-field="checkbox" type="checkbox" id="noreplace_'.$v_name.'"'.($v_crop?'':' disabled="disabled"').' data-name="'.$v_name.'" data-key="noreplace" /><label for="noreplace_'.$v_name.'">No Replace</label></span> &nbsp;&nbsp;&nbsp;&nbsp;';
            }
            $arr_saved_data[$v_image_id>0?'image':'shape'][$v_name] = array(
                /*'type'=>$v_image_id>0?'image':'shape', 'name'=>$v_name,*/ 'top'=>$v_top, 'left'=>$v_left
                ,'locked'=>isset($arr_images[$j]['locked']) && $arr_images[$j]['locked']
                ,'noselect'=>isset($arr_images[$j]['noselect']) && $arr_images[$j]['noselect']
                ,'noresize'=>isset($arr_images[$j]['noresize']) && $arr_images[$j]['noresize']
                ,'unmovable'=>isset($arr_images[$j]['unmovable']) && $arr_images[$j]['unmovable']
                ,'noprint'=>isset($arr_images[$j]['noprint']) && $arr_images[$j]['noprint']
                ,'release'=>isset($arr_images[$j]['release']) && $arr_images[$j]['release']
                ,'noreplace'=>isset($arr_images[$j]['noreplace']) && $arr_images[$j]['noreplace']
                ,'based'=>isset($arr_images[$j]['based']) && $arr_images[$j]['based']
                ,'ontop'=>isset($arr_images[$j]['ontop']) && $arr_images[$j]['ontop']
                ,'crop'=>$v_crop
            );
            $arr_saved_flag[$v_image_id>0?'image':'shape'][$v_name] = array(
                /*'type'=>$v_image_id>0?'image':'shape', 'name'=>$v_name,*/ 'top'=>true, 'left'=>true
                ,'locked'=>true
                ,'noselect'=>true
                ,'noresize'=>true
                ,'unmovable'=>true
                ,'noprint'=>true
                ,'release'=>true
                ,'noreplace'=>true
                ,'based'=>true
                ,'ontop'=>true
                ,'crop'=>true
                ,'side'=>($i+1)
            );

            /*
            foreach($arr_images[$j] as $key=>$value)
                if($key!='name')
                    $v_one .= '<strong>'.$key.'</strong>: '.$value.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
            */
            if($v_one!='')
                $v_image .= '<td>'.$v_one.'</td></tr>';

        }
        if($v_image!='')
            $arr_list_element[$i]['image'] = '<table data-type="image" data-side="'.$i.'" class="tbl_single"><caption style="text-align:left; font-weight: bold; font-size: 120%">Image &amp; Shape</caption>'.$v_image.'</table>';

    }
}
//add_class('cls_tb_design_key');
//$cls_key = new cls_tb_design_key($db, LOG_DIR);

if($v_is_super_admin || is_administrator())
    $arr_site_where = array();
else{
    $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
    settype($v_user_id, 'int');
    $arr_site = $cls_tb_user->select_scalar('site', array('user_id'=>$v_user_id));
    if(!is_array($arr_site) || count($arr_site)==0) $arr_site = array(0);
    $arr_site_where = array('site_id'=>array('$in'=>$arr_site));
}

$arr_key = $cls_key->select($arr_site_where);
$arr_all_key = array();

foreach($arr_key as $arr){
    $v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
    $v_site_name = isset($arr['site_name'])?$arr['site_name']:'';
    $v_site_url = isset($arr['site_url'])?$arr['site_url']:'';
    $v_site_logo = isset($arr['site_logo'])?$arr['site_logo']:'';

    $arr_all_key[] = array(
        'site_id'=>$v_site_id
        ,'site_name'=>$v_site_name
        ,'site_url'=>$v_site_url
        ,'site_logo'=>$v_site_logo
    );
}

$_SESSION['ss_last_company_id'] = $v_company_id;
if(isset($_SESSION['ss_template_design_checked'])){
    $v_template_design = $_SESSION['ss_template_design_checked']==1;
    unset($_SESSION['ss_template_design_checked']);
}
$v_template_name = htmlentities($v_template_name);
?>