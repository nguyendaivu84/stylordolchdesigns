<?php
if(!isset($v_sval)) die();

$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_all_product = array();
if($v_company_id>=0){
    add_class('cls_tb_product');
    $cls_tb_product = new cls_tb_product($db, LOG_DIR);
    $arr_product = $cls_tb_product->select(array('company_id'=>$v_company_id, 'product_status'=>3));
    foreach($arr_product as $arr){
        $v_product_id = $arr['product_id'];
        $v_product_sku = $arr['product_sku'];
        $v_short_description = $arr['short_description'];
        $v_image_file = $arr['image_file'];
        $v_saved_dir = $arr['saved_dir'];

        $arr_all_product[] = array(
            'product_id'=>$v_product_id,
            'product_sku'=>$v_product_sku
            ,'description'=>$v_short_description
            ,'product_image'=>$v_saved_dir.$v_image_file
        );
    }
}
header("Content-type: application/json");
echo json_encode($arr_all_product);