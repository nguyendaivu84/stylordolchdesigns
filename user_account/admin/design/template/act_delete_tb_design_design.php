<?php if(!isset($v_sval)) die();?>
<?php
$v_design_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_design_id, 'int');
$v_template_id=0;
if($v_design_id>0){
    add_class('cls_tb_design_design');
    $cls_designs = new cls_tb_design_design($db, LOG_DIR);
    $v_row = $cls_designs->select_one(array('design_id'=>$v_design_id));
    if($v_row==1){
        $v_design_data = $cls_designs->get_design_data();
        $v_template_id = $cls_designs->get_template_id();
        $v_sample_image = $cls_designs->get_sample_image();
        $v_save_dir = $cls_designs->get_saved_dir();
        $v_result = $cls_designs->delete(array('design_id'=>$v_design_id));
        if($v_result){
            if(file_exists(ROOT_DIR.DS.$v_save_dir.$v_sample_image) && is_file(ROOT_DIR.DS.$v_save_dir.$v_sample_image)) @unlink(ROOT_DIR.DS.$v_save_dir.$v_sample_image);

            $arr_json = json_decode($v_design_data, true);
            $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_size = sizeof($arr_canvases);
            $arr_image_remove = array();
            for($i=0; $i<$v_size; $i++){
                $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                $v_image_size = sizeof($arr_images);
                for($j=0; $j < $v_image_size; $j++){
                    $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                    settype($v_image_id, 'int');
                    if($v_image_id > 0){
                        if(!isset($arr_image_remove[$v_image_id])){
                            $arr_image_remove[$v_image_id] = array('template'=>0, 'design'=>-1);
                        }
                    }
                }
            }
            add_class('cls_tb_design_image');
            $cls_image = new cls_tb_design_image($db, LOG_DIR);
            $v_total = $cls_image->update_image_used($arr_image_remove);
        }
    }
}
$_SESSION['ss_tb_design_template_redirect'] = 1;
if($v_template_id==0)
    redir(URL.$v_admin_key);
else
    redir(URL.$v_admin_key.'/'.$v_template_id.'/edit');
?>