<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_theme").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            $('input#txt_theme_name').val('');
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
        if($('input#chk_return_design').is(':checked'))
            $.cookie('ck_tab_theme_cookie', 'Design');
		return true;
	});

    var validator = $('form#frm_tb_design_theme').kendoValidator().data("kendoValidator");
    $("#txt_add_markup_cost").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    $("#txt_theme_order").kendoNumericTextBox({
        format: "n0",
        step: 1
    });
    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");

    $('input#txt_theme_color').kendoColorPicker({
        value: '<?php echo $v_theme_color;?>',
        buttons: true,
        toolIcon: "k-foreColor"
    });
    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        },
        select: function(e){
            var text = $(e.item).find("> .k-link").text();
            $.cookie('ck_tab_theme_cookie', text);
        }
    }).data("kendoTabStrip");
    function select_tab_strip(){
        var text = $.cookie('ck_tab_theme_cookie');
        $.removeCookie('ck_tab_theme_cookie');
        if(typeof text!='undefined' || text!='')
            tab_strip.select("li:contains("+text+")");
    }

    <?php if($v_theme_id>0){?>
    $('input#btn_create_vector_pdf').click(function(e){
        $.ajax({
            url             : '<?php echo URL . $v_admin_key;?>/ajax',
            dataType        : 'json',
            type            : 'POST',
            data            : {txt_template_id: '<?php echo $v_template_id;?>', txt_theme_id: '<?php echo $v_theme_id;?>', txt_ajax_type: 'create_theme_vector_pdf'},
            beforeSend      : function(){
                $('div#vector_pdf_link').find('a').remove();
                $('img#img_svg_loading').css('display','');
            },
            success         : function(data){
                if(data.success==1){
                    var $a = $('<a class="a-link">Download Zip</a>');
                    $a.attr('href', data.link);
                    $a.appendTo($('div#vector_pdf_link'));
                    //$('div#vector_pdf_link').insertAfter($a);
                }else{
                    alert(data.message);
                }
                $('img#img_svg_loading').css('display','none');
            },
            error           : function(xhr){
                //alert('Error: '+xhr.responseText);
            }
        });
    });

    var window_design = $('div#window_design');
    $('input#btn_launch_design').bind('click', function(){
        if(!window_design.data("kendoWindow")){
            window_design.kendoWindow({
                title: "Design tool for theme<?php echo $v_theme_id>0?': \"'.$v_theme_name.'\" (Template: '.$v_template_name.')':''?>",
                width: "800px",
                modal: true,
                iframe: true,
                content: '<?php echo URL.$v_admin_key;?>/<?php echo $v_theme_id;?>/theme-design/?template_id=<?php echo $v_template_id;?>&theme_id=<?php echo $v_theme_id;?>',
                type: "GET",
                actions: ["Minimize", "Maximize", "Close"],
                close: function(){
                    location.reload();
                }
            }).data('kendoWindow').bind('close', function(e){
                if(typeof view.isDesignSaved != 'undefined' && !view.isDesignSaved){
                    e.preventDefault();
                }
            });

        }
        window_design.data("kendoWindow").center().open().maximize();
    });
    <?php if($v_theme_design){
    ?>
    //tab_strip.select(tab_strip.tabGroup.children("li:last"));
    <?php }?>
    <?php }?>
    select_tab_strip();
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Theme (for Template <a href="<?php echo URL.$v_admin_key.'/'.$v_template_id;?>/edit"><?php echo $v_template_name;?></a>)<?php echo $v_theme_id>0?': '.$v_theme_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select"><!--
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    //echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>-->
                        </div>
                    </div>

<form id="frm_tb_design_theme" action="<?php echo URL.$v_admin_key;?>/<?php echo $v_theme_id>0?$v_theme_id.'/theme-edit':$v_template_id.'/theme-add';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_theme_id" name="txt_theme_id" value="<?php echo $v_theme_id;?>" />
<input type="hidden" id="txt_template_id" name="txt_template_id" value="<?php echo $v_template_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" />
<input type="hidden" id="txt_user_name" name="txt_user_name" value="<?php echo $v_user_name;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <?php if($v_theme_id>0){?>
                            <li>Design</li>
                        <?php }?>
                    </ul>

                    <div class="information div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width: 200px;">Theme Name</td>
                                <td  style="width:2px" align="right">&nbsp;</td>
                                <td align="left"><input type="text" size="20" id="txt_theme_name" name="txt_theme_name" value="<?php echo $v_theme_name;?>" class="text_css k-textbox" /><label id="lbl_theme_name" class="k-required">(*)</label></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Theme's Color</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><input type="text" id="txt_theme_color" name="txt_theme_color" /></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Add Markup Cost</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><input type="text" id="txt_add_markup_cost" name="txt_add_markup_cost" value="<?php echo $v_add_markup_cost;?>" /><label id="lbl_add_markup_cost" class="k-required">(*)</label></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Theme's Order</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><input type="number" id="txt_theme_order" name="txt_theme_order" value="<?php echo $v_theme_order;?>" /></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Sample Image</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><?php if($v_sample_url!='') echo '<img src="'.$v_sample_url.'?'.time().'" />'; ?>&nbsp;</td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Theme Description</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <textarea class="text_css k-textbox" style="width: 300px; height: 50px; resize: none" id="txt_theme_desc" name="txt_theme_desc"><?php echo $v_theme_desc;?></textarea></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Theme Status</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><div class="checkbox"><input type="checkbox" id="txt_theme_status" name="txt_theme_status"<?php echo $v_theme_status==0?' checked="checked"':'';?> /><label for="txt_theme_status"> Active</label></div></td>
                            </tr>
                            <tr valign="top">
                                <td align="right">&nbsp;</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left"><div class="checkbox"><input type="checkbox" id="chk_return_design" name="chk_return_design" /><label for="chk_return_design"> Return back for Design</label></div></td>
                            </tr>
                        </table>
                    </div>
<?php if($v_theme_id>0){?>
    <div class="design div_details">
        <input id="btn_launch_design" type="button" class="k-button" value="Launch Design" />
        <div id="window_design" style="display: none">
            <?php
            //include 'qry_template_design.php';
            //include 'dsp_template_design.php';
            ?>
        </div>
        <?php
        if($v_sample_url!='') echo '<img src="'.$v_sample_url.'?'.time().'" />';
        ?>
        <div id="vector_pdf_link" style="clear: both; margin-top: 20px; margin-bottom: 5px; height: 30px">

        </div>
        <div>
            <input id="btn_create_vector_pdf" type="button" class="k-button" value="Create Vector PDF" /> &nbsp;&nbsp; <img src="<?php URL;?>images/icons/loading.gif" id="img_svg_loading" style="display: none" />
        </div>
    </div>
<?php }?>
                   </div>

                   <?php if(isset($v_act) && in_array($v_act, array('ETH', 'ATH'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_theme" name="btn_submit_tb_design_theme" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
