<?php
if(!isset($v_sval)) die();
$v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_design_template_id));
if($v_row==1){
    $v_template_name = $cls_tb_design_template->get_template_name();
    $v_template_width = $cls_tb_design_template->get_template_width();
    $v_template_height = $cls_tb_design_template->get_template_height();
    $v_folding_type = $cls_tb_design_template->get_folding_type();
    $v_die_cut_type = $cls_tb_design_template->get_die_cut_type();
    $v_stock_cost = $cls_tb_design_template->get_stock_cost();
    $v_markup_cost = $cls_tb_design_template->get_markup_cost();
    $v_print_cost = $cls_tb_design_template->get_print_cost();
    $v_template_type = $cls_tb_design_template->get_template_type();
    $v_template_status = $cls_tb_design_template->get_template_status();
    $v_created_time = date('Y-m-d H:i:s',$cls_tb_design_template->get_created_time());
    $v_user_id = $cls_tb_design_template->get_user_id();
    $v_user_name = $cls_tb_design_template->get_user_name();
    $v_is_already = $cls_tb_design_template->get_is_already();
    $arr_product_list = $cls_tb_design_template->get_product_list();
    $v_location_id = $cls_tb_design_template->get_location_id();
    $v_company_id = $cls_tb_design_template->get_company_id();
    $v_template_desc = $cls_tb_design_template->get_template_desc();
    $arr_template_tag = $cls_tb_design_template->get_template_tag();
    $v_template_order = $cls_tb_design_template->get_template_order();
    $v_template_group = $cls_tb_design_template->get_template_group();
    $v_template_dpi = $cls_tb_design_template->get_template_dpi();
    $v_template_bleed = $cls_tb_design_template->get_template_bleed();
    $v_category_id = $cls_tb_design_template->get_category_id();
    $v_section_id = $cls_tb_design_template->get_section_id();
    $v_style_id = $cls_tb_design_template->get_style_id();
    $v_industry_id = $cls_tb_design_template->get_industry_id();
    $v_saved_dir = $cls_tb_design_template->get_saved_dir();
    $v_sample_image = $cls_tb_design_template->get_sample_image();
    $v_template_data = $cls_tb_design_template->get_template_data();

    //$v_new_template_id = $cls_tb_design_template->select_next('template_id');


    //$cls_tb_design_template->set_template_id($v_new_template_id);
    //$cls_tb_design_template->set_saved_dir($v_new_saved_dir);
    //$cls_tb_design_template->set_sample_image($v_new_sample_image);
    $cls_tb_design_template->set_original_id($v_design_template_id);
    $cls_tb_design_template->set_template_name('Clone: '.$v_template_name);
    $cls_tb_design_template->set_created_time(date('Y-m-d H:i:s', time()));
    $cls_tb_design_template->set_user_id(isset($arr_user['user_id'])?$arr_user['user_id']:0);
    $cls_tb_design_template->set_user_name(isset($arr_user['user_name'])?$arr_user['user_name']:'');

    $v_new_template_id = $cls_tb_design_template->insert();
    $v_result = $v_new_template_id > 0;

    if($v_result){
        $v_new_saved_dir = str_replace('/'.$v_design_template_id.'/','/'.$v_new_template_id, $v_saved_dir);
        //$v_result = false;
        if(file_exists(ROOT_DIR.DS.$v_new_saved_dir) || @mkdir(ROOT_DIR.DS.$v_new_saved_dir)){
            $v_new_saved_dir .= '/';
            $v_new_sample_image = 'template_'.$v_new_template_id.'_theme_0.png';

            copy(ROOT_DIR.DS.$v_saved_dir.$v_sample_image,ROOT_DIR.DS.$v_new_saved_dir.$v_new_sample_image);
            $v_thumb_file = ROOT_DIR.DS.$v_new_saved_dir.$v_new_sample_image;
            if(file_exists(ROOT_DIR.DS.$v_saved_dir.'thumb_'.$v_sample_image)){
                copy(ROOT_DIR.DS.$v_saved_dir.'thumb_'.$v_sample_image,ROOT_DIR.DS.$v_new_saved_dir.'thumb_'.$v_new_sample_image);
            }else{
                $image = new Imagick($v_thumb_file);
                $v_thumb_file = str_replace($v_new_sample_image, 'thumb_'.$v_new_sample_image, $v_thumb_file);
                add_class('cls_draw');
                $cls_draw = new cls_draw();
                $image = $cls_draw->create_square_thumb($image);
                $image->setformat('png');
                $image->writeimage($v_thumb_file);
                $image->clear();
                $image->destroy();
            }
            $cls_tb_design_template->update_fields(array('saved_dir', 'sample_image'), array($v_new_saved_dir, $v_new_sample_image), array('template_id'=>$v_new_template_id));
        }



        $arr_themes = $cls_tb_design_theme->select(array('template_id'=>$v_design_template_id), array('theme_order'=>1, 'theme_id'=>1));
        foreach($arr_themes as $arr){
            //$v_new_theme_id = $cls_tb_design_theme->select_next('theme_id');
            //if($v_new_theme_id>0){
            $v_new_theme_name = isset($arr['theme_name'])?$arr['theme_name']:'';
            $v_new_theme_color = isset($arr['theme_color'])?$arr['theme_color']:'ffffff';
            $v_new_add_markup_cost = isset($arr['add_markup_cost'])?$arr['add_markup_cost']:0;
            $v_new_company_id = isset($arr['company_id'])?$arr['company_id']:0;
            $v_new_list_color = isset($arr['list_color'])?$arr['list_color']:array();
            $v_new_list_image = isset($arr['list_image'])?$arr['list_image']:array();
            $v_new_location_id = isset($arr['location_id'])?$arr['location_id']:0;

            $v_old_theme_image = isset($arr['sample_image'])?$arr['sample_image']:'';
            $v_old_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';

            $v_old_theme_image_file = $v_old_saved_dir . $v_old_theme_image;

            //$v_new_template_id = $cls_tb_design_theme->get_template_id();
            $v_new_theme_order = isset($arr['theme_order'])?$arr['theme_order']:0;
            $v_new_theme_status = isset($arr['theme_status'])?$arr['theme_status']:0;
            $v_new_theme_desc = isset($arr['theme_desc'])?$arr['theme_desc']:'';

            //$cls_tb_design_theme->set_theme_id($v_new_theme_id);
            $cls_tb_design_theme->set_theme_name($v_new_theme_name);
            $cls_tb_design_theme->set_theme_color($v_new_theme_color);
            $cls_tb_design_theme->set_add_markup_cost($v_new_add_markup_cost);
            $cls_tb_design_theme->set_company_id($v_new_company_id);
            $cls_tb_design_theme->set_list_color($v_new_list_color);
            $cls_tb_design_theme->set_list_image($v_new_list_image);
            $cls_tb_design_theme->set_location_id($v_new_location_id);
            $cls_tb_design_theme->set_theme_order($v_new_theme_order);
            $cls_tb_design_theme->set_theme_status($v_new_theme_status);
            $cls_tb_design_theme->set_theme_desc($v_new_theme_desc);
            $cls_tb_design_theme->set_user_id(isset($arr_user['user_id'])?$arr_user['user_id']:0);
            $cls_tb_design_theme->set_user_name(isset($arr_user['user_name'])?$arr_user['user_name']:'');
            $cls_tb_design_theme->set_created_time(date('Y-m-d H:i:s'), time());
            $cls_tb_design_theme->set_last_modified(date('Y-m-d H:i:s'), time());
            $cls_tb_design_theme->set_last_used(date('Y-m-d H:i:s'), time());
            $cls_tb_design_theme->set_saved_dir($v_old_saved_dir);
            $cls_tb_design_theme->set_sample_image($v_old_theme_image);
            $cls_tb_design_theme->set_template_id($v_new_template_id);
            $v_new_theme_id = $cls_tb_design_theme->insert();

            if($v_new_theme_id > 0){
                $v_new_theme_image = '';
                $v_new_saved_dir = '';
                if($v_old_saved_dir!='' && $v_old_theme_image!='' && file_exists($v_old_theme_image_file)){
                    $v_theme_dir = DESIGN_THEME_DIR . DS . $v_new_template_id;
                    if(file_exists($v_theme_dir) || @mkdir($v_theme_dir)){
                        $v_new_theme_image = 'template_'.$v_new_template_id.'_theme_'.$v_new_theme_id.'.'.$cls_file->get_extension($v_old_theme_image);
                        $v_new_saved_dir = str_replace(ROOT_DIR, '', $v_theme_dir).DS;
                        $v_new_saved_dir = str_replace('\\', '/', $v_new_saved_dir);
                        if(@copy($v_old_theme_image_file, $v_theme_dir.DS.$v_new_theme_image))
                            $cls_tb_design_theme->update_fields(array('saved_dir', 'sample_image'), array($v_new_saved_dir, $v_new_sample_image), array('theme_id'=>$v_new_theme_id));
                    }
                }
            }
        }
    }
    if($v_result){
        $v_design_template_id = $v_new_template_id;

        $arr_json = json_decode($v_template_data, true);
        $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $v_size = sizeof($arr_canvases);
        $arr_image_remove = array();
        for($i=0; $i<$v_size; $i++){
            $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
            $v_image_size = sizeof($arr_images);
            for($j=0; $j < $v_image_size; $j++){
                $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                settype($v_image_id, 'int');
                if($v_image_id > 0){
                    if(!isset($arr_image_remove[$v_image_id])){
                        $arr_image_remove[$v_image_id] = array('template'=>1, 'design'=>0);
                    }
                }
            }
        }
        add_class('cls_tb_design_image');
        $cls_image = new cls_tb_design_image($db, LOG_DIR);
        $v_total = $cls_image->update_image_used($arr_image_remove);

    }

        //}
}

redir(URL.$v_admin_key.'/'.$v_design_template_id.'/edit');