<?php
if(!isset($v_sval)) die();

$v_type = isset($_POST['txt_type'])?$_POST['txt_type']:'';
$v_name = isset($_POST['txt_name'])?$_POST['txt_name']:'';
$v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:'0';
$v_side = isset($_POST['txt_side'])?$_POST['txt_side']:'0';

settype($v_template_id, 'int');
settype($v_side, 'int');

$v_success = 0;
$v_message = '';
$v_image_url = '';
$arr_type = array('text', 'image', 'shape');

if($v_template_id >0 && in_array($v_type, $arr_type) & $v_name!='' && in_array($v_side, array(0,1))){
    $v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_template_id));
    if($v_row==1){
        $v_template_data = $cls_tb_design_template->get_template_data();
        $v_image_data = $cls_tb_design_template->get_template_image();
        $v_folding_type = $cls_tb_design_template->get_folding_type();
        $v_template_width = $cls_tb_design_template->get_template_width();
        $v_template_height = $cls_tb_design_template->get_template_height();
        $v_template_bleed = $cls_tb_design_template->get_template_bleed();
        $v_saved_dir = $cls_tb_design_template->get_saved_dir();
        $v_sample_image = $cls_tb_design_template->get_sample_image();
        $v_dpi = $cls_tb_design_template->get_template_dpi();

        $arr_json = json_decode($v_template_data, true);
        if(!is_array($arr_json)) $arr_json = array();

        $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();

        $arr_tmp = array(
            'texts'=>array()
            ,'images'=>array()
            ,'width'=>$v_template_width
            ,'height'=>$v_template_height
            ,'bg_color'=> "FFFFFF"
            ,'bleed'=>	$v_template_bleed
            ,'droppable'=> true
            ,'name'=> "Front Side"
            ,"isViewed"=>false
        );

        $v_total_page = count($arr_canvases);
        if($v_folding_type<=0){
            if($v_total_page>=1){
                $arr_canvas = array();
                $i = 0;
                foreach($arr_canvases as $idx=>$arr){
                    if($i==0) $arr_canvas[] = $arr;
                    $i++;
                }
            }else{
                $arr_canvas[] = $arr_tmp;
            }
            $arr_canvases = $arr_canvas;
        }else{
            if($v_total_page<1){
                $arr_canvases = array($arr_tmp, $arr_tmp);
            }else if($v_total_page==1){
                $i=0;
                $arr_canvas = array();
                foreach($arr_canvases as $idx=>$arr){
                    if($i==0) $arr_canvas[0] = $arr;
                    $i++;
                }
                $arr_canvas[1] = $arr_tmp;
            }else{
                $i=0;
                $arr_canvas = array();
                foreach($arr_canvases as $idx=>$arr){
                    if($i<=1) $arr_canvas[$i] = $arr;
                    $i++;
                }
            }
            if($v_folding_type==1)
                $arr_canvases[1]['name'] = 'Back Side';
            else{
                $arr_canvases[1]['name'] = 'InSide';
                $arr_canvases[0]['name'] = 'OutSide';
            }
        }
        $v_total_page = count($arr_canvases);
        if(min($v_side, $v_folding_type) <=1 && $v_type<=$v_folding_type){
            $v_change = false;
            $arr_side = isset($arr_canvases[$v_side])?$arr_canvases[$v_side]:array();
            if($v_type=='text'){
                $arr_texts = isset($arr_side['texts'])?$arr_side['texts']:array();
                $arr_tmp_texts = array();
                for($i=0; $i<sizeof($arr_texts); $i++){
                    $v_tmp_name = isset($arr_texts[$i]['name'])?$arr_texts[$i]['name']:'';
                    if($v_name!=$v_tmp_name){
                        $arr_tmp_texts[] = $arr_texts[$i];
                    }else $v_change = true;
                }
                if($v_change){
                    $arr_json['canvases'][$v_side]['texts'] = $arr_tmp_texts;
                }
            }else{
                $arr_images = isset($arr_side['images'])?$arr_side['images']:array();
                $v_image_id = 0;
                $arr_tmp_images = array();
                for($i=0; $i<sizeof($arr_images);$i++){
                    $v_tmp_name = isset($arr_images[$i]['name'])?$arr_images[$i]['name']:'';
                    $v_tmp_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
                    if($v_tmp_name!=$v_name)
                        $arr_tmp_images[] = $arr_images[$i];
                    else{
                        $v_change = true;
                        $v_image_id = $v_tmp_id;
                    }
                }
                if($v_change){
                    $arr_json['canvases'][$v_side]['images'] = $arr_tmp_images;
                    if($v_image_id>0){
                        $arr_data_image = json_decode($v_image_data, true);
                        if(!is_array($arr_data_image)) $arr_data_image = array();
                        $arr_tmp_images = array();
                        foreach($arr_data_image as $id=>$arr){
                            if($id!=$v_image_id)
                                $arr_tmp_images[$id] = $arr;
                        }
                        $v_image_data = json_encode($arr_tmp_images);
                    }
                }
            }
            if($v_change){
                $v_json = json_encode($arr_json);
                $v_row = $cls_tb_design_template->update_fields(array('template_data', 'template_image'), array($v_json, $v_image_data), array('template_id'=>$v_template_id));
                if($v_row==1){
                    $v_success = 1;
                    $v_message = 'Update successful!';

                    //Update thumb
                    $v_image_change = false;
                    if($v_saved_dir=='' || !file_exists($v_saved_dir) || !is_dir($v_saved_dir)){
                        $v_saved_dir = str_replace(ROOT_DIR . DS, '', DESIGN_THEME_DIR);
                        $v_saved_dir = str_replace(DS,'/', $v_saved_dir).'/'.$v_template_id;
                        $v_accept = (file_exists($v_saved_dir) && is_writable($v_saved_dir)) || @mkdir($v_saved_dir);
                        if($v_accept) $v_saved_dir .= '/';
                        $v_image_change = true;
                    }else $v_accept = file_exists($v_saved_dir) && is_writable($v_saved_dir);
                    if($v_sample_image==''){
                        $v_ext = 'png';
                        $v_sample_image = 'template_'.$v_template_id.'_theme_0.'.$v_ext;
                        $v_image_change = true;
                    }else
                        $v_ext = $cls_file->get_extension($v_sample_image);
                    if($v_accept){
                        add_class('cls_draw');
                        add_class('cls_instagraph');
                        add_class('cls_tb_design_image');
                        $cls_draw = new cls_draw();
                        $cls_tmp_draw = new cls_draw();
                        $cls_images = new cls_tb_design_image($db, LOG_DIR);
                        $cls_instagraph = new cls_instagraph();
                        $image = new Imagick();
                        if($v_total_page<=1){
                            $v_page = 0;
                            $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);
                            $tmp_image = $cls_draw->create_square_thumb($image, 100);
                            $v_file_thumb = ROOT_DIR . DS. $v_saved_dir . 'thumb_'.$v_sample_image;
                            $tmp_image->setformat($v_ext);
                            $tmp_image->writeimage($v_file_thumb);
                            $tmp_image->clear();
                            $tmp_image->destroy();

                            $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);
                            $image->setimageformat($v_ext);
                            $image->writeimage(ROOT_DIR . DS . $v_saved_dir . $v_sample_image);
                            $image->clear();
                            $image->destroy();
                        }else{
                            $v_page = 1;
                            $image2 = new Imagick();
                            $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);
                            $v_page = 0;
                            $image1 = new Imagick();
                            $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, true);

                            $tmp_image = $cls_draw->create_square_thumb($image1, 100);
                            $v_file_thumb = ROOT_DIR .DS . $v_saved_dir . 'thumb_'.$v_sample_image;;
                            $tmp_image->setformat($v_ext);
                            $tmp_image->writeimage($v_file_thumb);
                            $tmp_image->clear();
                            $tmp_image->destroy();


                            $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

                            $image->setimageformat($v_ext);
                            $image->writeimage(ROOT_DIR . DS . $v_saved_dir . $v_sample_image);
                            $image->clear();
                            $image->destroy();
                            $image1->clear();
                            $image1->destroy();
                            $image2->clear();
                            $image2->destroy();
                        }
                        $v_image_url = $v_saved_dir . $v_sample_image .'?'.time();
                        if($v_image_change){
                            $cls_tb_design_template->update_fields(array('saved_dir', 'sample_image'), array($v_saved_dir, $v_sample_image), array('template_id'=>$v_template_id));
                        }
                    }
                }else{
                    $v_message = 'Update not successful!';
                }
            }else{
                $v_message = 'No change!';
            }
        }else{
            $v_message = 'Invalid template data!';
        }
    }else{
        $v_message = 'Template is not found!';
    }
}else{
    $v_message = 'Lost data!';
}

$arr_return = array('success'=>$v_success, 'message'=>$v_message, 'url'=>$v_image_url);

$cls_output->output($arr_return, true, false);