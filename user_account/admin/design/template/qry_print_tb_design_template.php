<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_template_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_template_sort'])){
	$v_sort = $_SESSION['ss_tb_design_template_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_template = $cls_tb_design_template->select($arr_where_clause, $arr_sort);
$v_dsp_tb_design_template = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_design_template .= '<tr align="center" valign="middle">';
$v_dsp_tb_design_template .= '<th>Ord</th>';
$v_dsp_tb_design_template .= '<th>Template Id</th>';
$v_dsp_tb_design_template .= '<th>Template Name</th>';
$v_dsp_tb_design_template .= '<th>Template Width</th>';
$v_dsp_tb_design_template .= '<th>Template Length</th>';
$v_dsp_tb_design_template .= '<th>Folding Type</th>';
$v_dsp_tb_design_template .= '<th>Die Cut Type</th>';
$v_dsp_tb_design_template .= '<th>Stock Cost</th>';
$v_dsp_tb_design_template .= '<th>Markup Cost</th>';
$v_dsp_tb_design_template .= '<th>Print Cost</th>';
$v_dsp_tb_design_template .= '<th>Template Type</th>';
$v_dsp_tb_design_template .= '<th>Template Status</th>';
$v_dsp_tb_design_template .= '<th>Created Time</th>';
$v_dsp_tb_design_template .= '<th>User Id</th>';
$v_dsp_tb_design_template .= '<th>User Name</th>';
$v_dsp_tb_design_template .= '<th>Is Already</th>';
$v_dsp_tb_design_template .= '<th>Location Id</th>';
$v_dsp_tb_design_template .= '<th>Company Id</th>';
$v_dsp_tb_design_template .= '<th>Template Desc</th>';
$v_dsp_tb_design_template .= '<th>Template Order</th>';
$v_dsp_tb_design_template .= '<th>Template Group</th>';
$v_dsp_tb_design_template .= '</tr>';
$v_count = 1;
foreach($arr_tb_design_template as $arr){
	$v_dsp_tb_design_template .= '<tr align="left" valign="middle">';
	$v_dsp_tb_design_template .= '<td align="right">'.($v_count++).'</td>';
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
	$v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
	$v_template_length = isset($arr['template_length'])?$arr['template_length']:0;
	$v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
	$v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
	$v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
	$v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
	$v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
	$v_template_type = isset($arr['template_type'])?$arr['template_type']:0;
	$v_template_status = isset($arr['template_status'])?$arr['template_status']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_is_already = isset($arr['is_already'])?$arr['is_already']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_template_desc = isset($arr['template_desc'])?$arr['template_desc']:'';
	$v_template_order = isset($arr['template_order'])?$arr['template_order']:0;
	$v_template_group = isset($arr['template_group'])?$arr['template_group']:0;
	$v_dsp_tb_design_template .= '<td>'.$v_template_id.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_name.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_width.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_length.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_folding_type.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_die_cut_type.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_stock_cost.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_markup_cost.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_print_cost.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_type.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_status.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_created_time.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_user_name.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_is_already.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_location_id.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_company_id.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_desc.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_order.'</td>';
	$v_dsp_tb_design_template .= '<td>'.$v_template_group.'</td>';
	$v_dsp_tb_design_template .= '</tr>';
}
$v_dsp_tb_design_template .= '</table>';
?>