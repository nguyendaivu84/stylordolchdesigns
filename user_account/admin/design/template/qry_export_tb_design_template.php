<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_template_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_template_sort'])){
	$v_sort = $_SESSION['ss_tb_design_template_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_template = $cls_tb_design_template->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_design_template_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Design_template')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Design_template');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Width', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Length', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Folding Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Die Cut Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Stock Cost', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Markup Cost', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Print Cost', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Created Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Is Already', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Desc', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Order', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Group', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_design_template as $arr){
	$v_excel_col = 1;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
	$v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
	$v_template_length = isset($arr['template_length'])?$arr['template_length']:0;
	$v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
	$v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
	$v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
	$v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
	$v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
	$v_template_type = isset($arr['template_type'])?$arr['template_type']:0;
	$v_template_status = isset($arr['template_status'])?$arr['template_status']:0;
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_is_already = isset($arr['is_already'])?$arr['is_already']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_template_desc = isset($arr['template_desc'])?$arr['template_desc']:'';
	$v_template_order = isset($arr['template_order'])?$arr['template_order']:0;
	$v_template_group = isset($arr['template_group'])?$arr['template_group']:0;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_width, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_length, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_folding_type, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_die_cut_type, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_stock_cost, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_markup_cost, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_print_cost, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_type, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_created_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_is_already, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_desc, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_order, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_group, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>