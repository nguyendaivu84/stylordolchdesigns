<?php
if(!isset($v_sval)) die();
?>
<?php
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_return = array('error'=>0, 'message'=>'', 'location'=>array(), 'product'=>array(), 'tag'=>array());
$arr_all_location = array();
$arr_all_location[] = array('location_id'=>0, 'location_name'=>'--------');

$arr_all_product = array();

$arr_all_tag = array();
$_SESSION['ss_last_company_id'] = $v_company_id;
if($v_company_id>=0){
    $arr_location = $cls_tb_location->select(array('company_id'=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])), array('location_name'=>1));
    foreach($arr_location as $arr){
        $arr_all_location[] = array('location_id'=>$arr['location_id'], 'location_name'=>$arr['location_name']);
    }
    add_class('cls_tb_product');
    $cls_tb_product = new cls_tb_product($db, LOG_DIR);
    $arr_product = $cls_tb_product->select(array('company_id'=>$v_company_id, 'product_status'=>3));
    foreach($arr_product as $arr){
        $v_product_id = $arr['product_id'];
        $v_product_sku = $arr['product_sku'];
        $v_short_description = $arr['short_description'];
        $v_image_file = $arr['image_file'];
        $v_saved_dir = $arr['saved_dir'];

        $arr_all_product[] = array(
            'product_id'=>$v_product_id,
            'product_sku'=>$v_product_sku
            ,'description'=>$v_short_description
            ,'product_image'=>$v_saved_dir.$v_image_file
        );
    }

    add_class('cls_tb_tag');
    $cls_tb_tag = new cls_tb_tag($db, LOG_DIR);
    $arr_tag = $cls_tb_tag->select(array('company_id'=>$v_company_id));
    foreach($arr_tag as $arr){
        $v_tag_id = isset($arr['tag_id'])?$arr['tag_id']:0;
        $v_tag_name = isset($arr['tag_name'])?$arr['tag_name']:'';
        $v_tag_parent = isset($arr['tag_parent'])?$arr['tag_parent']:0;
        if($v_tag_id>0 && $v_tag_name!=''){
            $arr_all_tag[] = array('tag_id'=>$v_tag_id, 'tag_name'=>$v_tag_name, 'tag_parent'=>$v_tag_parent);
        }
    }

}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'Invalid Company ID';
}
$arr_return['location'] = $arr_all_location;
$arr_return['product'] = $arr_all_product;
$arr_return['tag'] = $arr_all_tag;
echo(json_encode($arr_return));
?>
