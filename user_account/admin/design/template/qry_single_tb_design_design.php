<?php if(!isset($v_sval)) die();?>
<?php
add_class('cls_tb_design_design');
$cls_tb_design_design = new cls_tb_design_design($db, LOG_DIR);
$v_error_message = '';
$v_mongo_id = NULL;
$v_design_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_design_id, 'int');
$v_design_name = '';
$v_design_data = '';
$v_design_width = 0;
$v_design_height = 0;
$v_design_folding = 0;
$v_design_direction = 0;
$v_design_die_cut = 0;
$v_design_image = '';
$v_design_status = 0;
$v_markup_cost = 0;
$v_print_cost = 0;
$v_stock_cost = 0;
$v_created_time = date('Y-m-d H:i:s', time());
$v_template_id = 0;
$v_theme_id = 0;
$v_product_id = 0;
$v_user_id = 0;
$v_user_name = '';
$v_user_ip = '';
$v_user_agent = '';
$v_sample_image = '';
$v_saved_dir = '';
$v_company_id = isset($_SESSION['ss_last_company_id'])?$_SESSION['ss_last_company_id']:(isset($arr_user['company_id'])?$arr_user['company_id']:'0');
settype($v_company_id, 'int');
$v_location_id = 0;
$arr_design_share = array();
$v_new_design_design = true;
$v_design_design = false;
if(isset($_POST['btn_submit_tb_design_design'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_design_design->set_mongo_id($v_mongo_id);
    $v_design_id = isset($_POST['txt_design_id'])?$_POST['txt_design_id']:$v_design_id;
    $v_design_id = (int) $v_design_id;

    $v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:$v_template_id;
    $v_template_id = (int) $v_template_id;

    $v_design_name = isset($_POST['txt_design_name'])?$_POST['txt_design_name']:$v_design_name;
    $v_design_name = trim($v_design_name);
    if($v_design_name=='') $v_error_message .= '[Design Name] is empty!<br />';
    $cls_tb_design_design->set_design_name($v_design_name);
    $v_design_status = isset($_POST['txt_design_status'])?0:1;
    $cls_tb_design_design->set_design_status($v_design_status);
    $v_markup_cost = isset($_POST['txt_markup_cost'])?$_POST['txt_markup_cost']:$v_markup_cost;
    $v_markup_cost = (float) $v_markup_cost;
    if($v_markup_cost<0) $v_error_message .= '[Markup Cost] is negative!<br />';
    $cls_tb_design_design->set_markup_cost($v_markup_cost);
    $v_print_cost = isset($_POST['txt_print_cost'])?$_POST['txt_print_cost']:$v_print_cost;
    $v_print_cost = (float) $v_print_cost;
    if($v_print_cost<0) $v_error_message .= '[Print Cost] is negative!<br />';
    $cls_tb_design_design->set_print_cost($v_print_cost);
    $v_stock_cost = isset($_POST['txt_stock_cost'])?$_POST['txt_stock_cost']:$v_stock_cost;
    $v_stock_cost = (float) $v_stock_cost;
    if($v_stock_cost<0) $v_error_message .= '[Stock Cost] is negative!<br />';
    $cls_tb_design_design->set_stock_cost($v_stock_cost);
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_result = false;
        }else{
            $arr_fields = array('design_name', 'design_status');
            $arr_values = array($v_design_name, $v_design_status);
            $v_result = $cls_tb_design_design->update_fields($arr_fields, $arr_values, array('_id' => $v_mongo_id));
            $v_new_design_design = false;
        }
        if($v_result){
            $v_design_design = isset($_POST['chk_return_design']);
            $_SESSION['ss_tb_design_design_redirect'] = 1;
            if($v_design_design)
                redir(URL.$v_admin_key.'/'.$v_design_id.'/design-edit');
            else{
                if($v_template_id>0)
                    redir(URL.$v_admin_key.'/'.$v_template_id.'/edit');
                else{
                    $v_template_id = $cls_tb_design_template->select_scalar('template_id', array('design_id'=>$v_design_id));
                    settype($v_template_id, 'int');
                    if($v_template_id>0)
                        $v_template_id = $cls_tb_design_template->select_scalar('template_id', array('design_id'=>$v_design_id));
                    else
                        redir(URL);
                }
            }
        }else{
            if($v_new_design_design) $v_design_id = 0;
        }
    }
}else{
    $v_design_id= isset($_GET['id'])?$_GET['id']:'0';
    settype($v_design_id,'int');
    if($v_design_id>0){
        $v_row = $cls_tb_design_design->select_one(array('design_id' => $v_design_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_design_design->get_mongo_id();
            $v_design_id = $cls_tb_design_design->get_design_id();
            $v_design_name = $cls_tb_design_design->get_design_name();
            $v_design_data = $cls_tb_design_design->get_design_data();
            $v_design_width = $cls_tb_design_design->get_design_width();
            $v_design_height = $cls_tb_design_design->get_design_height();
            $v_design_folding = $cls_tb_design_design->get_design_folding();
            $v_design_direction = $cls_tb_design_design->get_design_direction();
            $v_design_die_cut = $cls_tb_design_design->get_design_die_cut();
            $v_design_image = $cls_tb_design_design->get_design_image();
            $v_design_status = $cls_tb_design_design->get_design_status();
            $v_markup_cost = $cls_tb_design_design->get_markup_cost();
            $v_print_cost = $cls_tb_design_design->get_print_cost();
            $v_stock_cost = $cls_tb_design_design->get_stock_cost();
            $v_created_time = date('Y-m-d H:i:s',$cls_tb_design_design->get_created_time());
            $v_template_id = $cls_tb_design_design->get_template_id();
            $v_theme_id = $cls_tb_design_design->get_theme_id();
            $v_product_id = $cls_tb_design_design->get_product_id();
            $v_user_id = $cls_tb_design_design->get_user_id();
            $v_user_name = $cls_tb_design_design->get_user_name();
            $v_user_ip = $cls_tb_design_design->get_user_ip();
            $v_user_agent = $cls_tb_design_design->get_user_agent();
            $v_sample_image = $cls_tb_design_design->get_sample_image();
            $v_saved_dir = $cls_tb_design_design->get_saved_dir();
            $v_company_id = $cls_tb_design_design->get_company_id();
            $v_location_id = $cls_tb_design_design->get_location_id();
            $arr_design_share = $cls_tb_design_design->get_design_share();
        }
    }
}
$v_design_folding = $cls_settings->get_option_name_by_id('folding_type', $v_design_folding, 'None');
$v_design_direction = $cls_settings->get_option_name_by_id('folding_direction', $v_design_direction, 'Vertical');
$v_design_die_cut = $cls_settings->get_option_name_by_id('die_cut_type', $v_design_die_cut, 'None');
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
$v_sample_url = '';
$v_template_name = '';
$v_theme_name = '';
$v_company_name = '';
$v_location_name = '';
$v_assigned_to = '';

if($v_user_id>0){
    $v_row = $cls_tb_user->select_one(array('user_id'=>$v_user_id));
    if($v_row==1){
        $v_contact_id = $cls_tb_user->get_contact_id();
        $v_user_company_id = $cls_tb_user->get_company_id();
        $v_user_location_id = $cls_tb_user->get_location_id();

        $v_assigned_to = $cls_tb_contact->get_full_name_contact((int) $v_contact_id);
        $v_company_name = $cls_tb_company->select_scalar('company_name', array('company_id'=>$v_company_id));
        $v_location_name = $cls_tb_location->select_scalar('location_name', array('location_id'=>$v_location_id));
    }
}

if($v_theme_id>0){
    add_class('cls_tb_design_theme');
    $v_theme_name = $cls_tb_design_theme->select_scalar('theme_name', array('theme_id'=>$v_theme_id));
}
if($v_design_id>0){
    if($v_saved_dir!='' && $v_sample_image!=''){
        if(file_exists($v_saved_dir.$v_sample_image)) $v_sample_url = URL.$v_saved_dir.$v_sample_image;
    }
}
if($v_template_id>0){
    $v_row = $cls_tb_design_template->select_one(array('template_id'=> (int) $v_template_id));
    if($v_row == 1){
        $v_saved_dir = $cls_tb_design_template->get_saved_dir();
        $v_template_name = $cls_tb_design_template->get_template_name();
        if($v_sample_url==''){
            $v_sample_image = $cls_tb_design_template->get_sample_image();
            if(file_exists($v_saved_dir.$v_sample_image)) $v_sample_url = URL.$v_saved_dir.$v_sample_image;
        }
    }
}
?>