<?php if(!isset($v_sval)) die();?>
<style type="text/css">
.k-grid-content{
    height: 368px !important;
}
.k-editor-inline {
    margin: 0;
    padding: 21px 21px 11px;
    border-width: 0;
    box-shadow: none;
    background: none;
}

.k-editor-inline.k-state-active {
    border-width: 1px;
    padding: 20px 20px 10px;
    background: none;
}

#topEditor h2, .column h3 {
    font-size: 24px;
    color: #e15613;
    font-family: "Droid Sans",DroidSansWeb,"Segoe UI","Lucida Sans Unicode",Arial,Helvetica,sans-serif;
}

.k-editor-inline p {
    font-size: 13px;
}

#txt_template_desc {
    display: inline-block;
    vertical-align: top;
    width: 500px;
}

#txt_template_desc a {
    color: #e15613;
}

#txt_template_desc h3 {
    padding-top: 10px;
    font-size: 15px;
}
#txt_template_desc {
    display: inline-block;
    vertical-align: top;
    width: 500px;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_template").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            $('input#txt_template_name').val('');
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
        var check = check_object_change();
        if(check>0){
            e.preventDefault();
            alert('You have made changes on object. You must save first to continue!');
            if(tab_strip.select().index()!=check + 4) tab_strip.select(check + 4);
            return false;
        }
        var form = $('form#frm_tb_design_template');
        form.find("[data-role=editor]").each(function() {
            var editor = $(this).data("kendoEditor");

            // ... append a hidden input that holds the editor value
            $("<input type='hidden' />")
                .attr("name", editor.element.attr("id"))
                .val(editor.value())
                .appendTo(form);
        });
        if($('input#txt_template_design').is(':checked'))
            $.cookie('ck_tab_template_cookie', 'Design&nbsp;');
		return true;
	});

<?php if($v_template_id>0){?>

    $('input#btn_create_vector_pdf').click(function(e){
        $.ajax({
            url             : '<?php echo URL . $v_admin_key;?>/ajax',
            dataType        : 'json',
            type            : 'POST',
            data            : {txt_template_id: '<?php echo $v_template_id;?>', txt_ajax_type: 'create_template_vector_pdf'},
            beforeSend      : function(){
                $('div#vector_pdf_link').find('a').remove();
                $('img#img_svg_loading').css('display','');
            },
            success         : function(data){
                if(data.success==1){
                    var $a = $('<a class="a-link">Download Zip</a>');
                    $a.attr('href', data.link);
                    $a.appendTo($('div#vector_pdf_link'));
                    //$('div#vector_pdf_link').insertAfter($a);
                }else{
                    alert(data.message);
                }
                $('img#img_svg_loading').css('display','none');
            },
            error           : function(xhr){
                //alert('Error: '+xhr.responseText);
            }
        });
    });
    $.each($('img[data-action="delete"]'), function(idx, el){
        $(this).click(function(e){
            var ask = confirm('Are you sure you want to delete this object?\nYou can not undo after deleting.');
            if(!ask) return false;
            var loading = '<?php echo URL;?>images/icons/loading.gif';
            var src = $(this).attr('src');
            var $this = $(this);
            var name = $(this).attr('data-name');
            var type = $(this).attr('data-type');
            var side = $(this).attr('data-side');
            var template = $(this).attr('data-template');
            $.ajax({
                url         : '<?php echo URL . $v_admin_key;?>/ajax',
                type        : 'POST',
                dataType    : 'json',
                data        : {txt_type: type, txt_name: name, txt_side: side, txt_template_id: template, txt_ajax_type:'delete_object'},
                beforeSend  : function(){
                    $this.attr('src', loading);
                } ,
                success     : function(data){
                    if(data.success==1){
                        if(type!='text') type = 'image';
                        $this.closest('tr').remove();
                        if($this.closest('table').find('tr').index()==0) $this.closest('table').remove();
                        reset_object_change();
                        if(data.url!=''){
                            $('img#img_template_preview').attr('src', data.url);
                        }
                    }else{
                        $this.attr('src', src);
                        alert(data.message);
                    }
                },
                error       : function(xhr){
                    //alert(xhr.responseText);
                }
            });
            return true;

        });
    });

    var theme_grid_data = <?php echo json_encode($arr_themes);?>;
    var theme_grid = $("#theme_grid").kendoGrid({
        dataSource: {
            pageSize: theme_grid_data.length,
            data: theme_grid_data
        },
        height: 430,
        scrollable: true,
        sortable: false,
        pageable: false,
        columns: [
            {field: "row_order", title: " ", type:"int", width:"20px", sortable:false, template:'<span style="float:right">#= row_order#</span>'},
            {field: "theme_name", title: "Theme's Name", type:"string", width:"80px", sortable:false},
            {field: "sample_image", title: "Sample", type:"string", width:"100px", sortable: true, template: '<p style="margin: 0px; text-align: center"><img style="width: 80px" src="#= sample_image#" /> </p>'},
            {field: "theme_color", title: "Theme's Color", type:"string", width:"60px", sortable:false, template:'<p style="background-color: #= theme_color#; height: 10px; width: 90%"></p>'},
            {field: "add_markup_cost", title: "Add. Cost", type:"number", width:"60px", sortable:false, template:'<span style="float:right">#= kendo.toString(add_markup_cost, "c2")#</span>'},
            {field: "theme_order", title: "Order", type:"string", width:"30px", sortable:false, template:'<span style="float:right"> #= theme_order#</span>'},
            {field: "theme_status", title: "Status", type:"boolean", width:"50px", template: '<p style="text-align:center; margin:0">#= theme_status?"Active":"Inactive"# </p>'},
            { command:  [
                { name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
            ],
                title: "Edit", width: "40px" },
            { command:  [
                { name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
            ],
                title: "Delete", width: "40px" }
        ]
    }).data("kendoGrid");
    function edit_row(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if(confirm('Do you want to edit theme with name: "'+dataItem.theme_name+'" from "<?php echo addslashes($v_template_name);?>"?')){
            document.location = "<?php echo URL.$v_admin_key;?>/"+dataItem.theme_id+"/theme-edit/";
        }
    }
    function delete_row(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if(confirm('Do you want to delete theme with name: "'+dataItem.theme_name+'" from "<?php echo addslashes($v_template_name);?>"?')){
            document.location = "<?php echo URL.$v_admin_key;?>/"+dataItem.theme_id+"/theme-delete/";
        }
    }

    <?php if($v_count_design>0){?>

    var design_grid = $("#design_grid").kendoGrid({
        dataSource: {
            pageSize: 20,
            page: 1,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: {
                    url: "<?php echo URL.$v_admin_key;?>/json/",
                    type: "POST",
                    data: {txt_session_id:"<?php echo session_id();?>",txt_json_type: 'load_design',txt_template_id: '<?php echo $v_template_id;?>'}
                }
            },
            schema: {
                data: "tb_design_design"
                ,total: function(data){
                    return data.total_rows;
                }
            },
            type: "json"
        },
        pageSize: 20,
        height: 430,
        scrollable: true,
        sortable: true,
        //selectable: "single",
        pageable: {
            input: true,
            refresh: true,
            pageSizes: [10, 20, 30, 40, 50],
            numeric: false
        },
        columns: [
            {field: "row_order", title: " ", type:"int", width:"20px", sortable:false, template:'<span style="float:right">#= row_order#</span>'},
            {field: "sample_image", title: "Design's Name", type:"string", width:"150px", sortable: true, template: '<p style="margin: 0px; text-align: center">#= design_name#<br /><img style="width: 80px" src="#= sample_image#" /> </p>'},
            {field: "theme_name", title: "Theme", type:"string", width:"60px", sortable:false},
            {field: "user_name", title: "Assigned to", type:"string", width:"60px", sortable:false},
            {field: "created_time", title: "Created Time", type:"string", width:"50px", sortable:true, template:'<span style="float: right">#= created_time#</span> '},
            {field: "design_status", title: "Status", type:"boolean", width:"50px", template: '<p style="text-align:center; margin:0">#= design_status?"Active":"Inactive"# </p>'},
            { command:  [
                { name: "Edit", text:'', click: edit_design_row, imageClass: 'k-grid-Edit' }
            ],
                title: "Edit", width: "40px" },
            { command:  [
                { name: "Delete", text:'', click: delete_design_row, imageClass: 'k-grid-Delete' }
            ],
                title: "Delete", width: "40px" }
        ]
    }).data("kendoGrid");
    function edit_design_row(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if(confirm('Do you want to edit design with name: "'+dataItem.design_name+'" from "<?php echo addslashes($v_template_name);?>"?')){
            document.location = "<?php echo URL.$v_admin_key;?>/"+dataItem.design_id+"/design-edit/";
        }
    }
    function delete_design_row(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        if(confirm('Do you want to delete design with name: "'+dataItem.design_name+'" from "<?php echo addslashes($v_template_name);?>"?')){
            document.location = "<?php echo URL.$v_admin_key;?>/"+dataItem.design_id+"/design-delete/";
        }
    }



    <?php }?>
    <?php if(sizeof($arr_list_element)>0){?>
    $.each($('input[data-action="btn_save_data"]'), function(idx, el){
        $(this).click(function(e){
            var side = $(this).attr('data-side');
            side = parseInt(side, 10);
            if(side!=1) side = 0;
            var text = [];
            $.each($('table[data-side="'+side+'"][data-type="text"] tr'), function(){
                var name = $(this).attr('data-name');
                var title = $(this).find('input[type="text"][data-key="title"]').val()!=$(this).find('input[type="hidden"][data-key="title"]').val()?$(this).find('input[type="text"][data-key="title"]').val():null;
                var preset = $(this).find('input[type="text"][data-key="preset"]').val()!=$(this).find('input[type="hidden"][data-key="preset"]').val()?$(this).find('input[type="text"][data-key="preset"]').val():null;
                var body = $(this).find('input[type="text"][data-key="body"]').val()!=$(this).find('input[type="hidden"][data-key="body"]').val()?$(this).find('input[type="text"][data-key="body"]').val():null;
                var top = $(this).find('input[data-key="top"]').val();
                var left = $(this).find('input[data-key="left"]').val();
                var based = $(this).find('input[data-key="based"]').is(':checked');
                var ontop = $(this).find('input[data-key="ontop"]').is(':checked');
                top = parseFloat(top);
                left = parseFloat(left);
                var one = {name: name, title: title, preset: preset, body: body, top: top, left: left, based: based?1:0, ontop: ontop?1:0};
                text.push(one);
            });

            var image = [];
            $.each($('table[data-side="'+side+'"][data-type="image"] tr'), function(){
                var name = $(this).attr('data-name');
                var noprint = $(this).find('input[data-key="noprint"]').is(':checked')?1:0;
                //var uploader = $(this).find('input[data-key="uploader"]').is(':checked')?1:0;
                var noselect = $(this).find('input[data-key="noselect"]').is(':checked')?1:0;
                var noresize = $(this).find('input[data-key="noresize"]').is(':checked')?1:0;
                var unmovable = $(this).find('input[data-key="unmovable"]').is(':checked')?1:0;
                var locked = $(this).find('input[data-key="locked"]').is(':checked')?1:0;
                var placeholder = typeof $(this).find('select').val()!='undefined'?$(this).find('select').val():null;
                var top = $(this).find('input[data-key="top"]').val();
                var left = $(this).find('input[data-key="left"]').val();
                var based = $(this).find('input[data-key="based"]').is(':checked')?1:0;
                var ontop = $(this).find('input[data-key="ontop"]').is(':checked')?1:0;
                var crop = $(this).find('input[data-key="crop"]').is(':checked')?1:0;
                var release = $(this).find('input[data-key="release"]').is(':checked')?1:0;
                var noreplace = $(this).find('input[data-key="noreplace"]').is(':checked')?1:0;
                top = parseFloat(top);
                left = parseFloat(left);
                var one = {name: name, noprint: noprint, noselect: noselect, noresize: noresize, unmovable: unmovable, locked: locked, placeholder: placeholder, top: top, left: left, based: based, ontop: ontop, crop: crop, release: release, noreplace: noreplace};
                image.push(one);
            });
            var text_data = JSON.stringify(text);
            var image_data = JSON.stringify(image);
            $.ajax({
                url         : '<?php echo URL.$v_admin_key;?>/ajax',
                type        : 'POST',
                dataType    : 'json',
                data        : {txt_template_id:'<?php echo $v_template_id;?>', txt_text_data: text_data, txt_image_data: image_data, txt_side: side, txt_ajax_type: 'save_design_data'},
                beforeSend  : function(){

                },
                success     : function(data){
                    alert(data.message);
                    if(data.success==1){
                        reset_object_change();
                        if(data.url!=''){
                            $('img#img_template_preview').attr('src', data.url);
                        }
                    }
                }
            });
        })
    });
    <?php }?>
<?php }?>

    $("#txt_template_desc").kendoEditor({
        tools: [
            "bold",
            "italic",
            "underline",
            "strikethrough",
            "justifyLeft",
            "justifyCenter",
            "justifyRight",
            "justifyFull",
            "createLink",
            "unlink",
            "insertImage",
            "createTable",
            "addColumnLeft",
            "addColumnRight",
            "addRowAbove",
            "addRowBelow",
            "deleteRow",
            "deleteColumn",
            "foreColor",
            "backColor"
        ]
    });

    var site_data = <?php echo json_encode($arr_all_key);?>;
    var site_combo = $('select#txt_site_id').width(300).kendoMultiSelect({
        dataSource:site_data,
        dataTextField: 'site_name',
        dataValueField:'site_id',
        itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="max-width: 200px" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
            '<h3>${ data.site_name }</h3>' +
            '<p>${ data.site_url }</p></div>',
        tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"${data.site_logo}\" alt=\"${data.site_name}\" />' +
            '#: data.site_name #'

    }).data("kendoMultiSelect");
    site_combo.value(<?php echo json_encode($arr_site_id);?>);

    $("select#txt_filter_site").kendoDropDownList({
        change: filter_site_changed
    });
    function filter_site_changed(){
        site_combo.options.filter = $("#txt_filter_site_value").val();
    }
    var set_search_site = function (e) {
        if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
            site_combo.search($("#txt_filter_site_value").val());
            e.preventDefault();
        }
    };
    $("button#btn_filter_site").click(set_search_site);
    $("#txt_filter_site_value").keypress(set_search_site);

	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		},
        select: function(e){
            var text = $(e.item).find("> .k-link").text();
            $.cookie('ck_tab_template_cookie', text);
        }
	}).data("kendoTabStrip");

    function select_tab_strip(){
        var text = $.cookie('ck_tab_template_cookie');
        $.removeCookie('ck_tab_template_cookie');
        if(typeof text!='undefined' || text!='')
            tab_strip.select("li:contains("+text+")");
    }


	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
    var combo_location = $('select#txt_location_id').width(200).kendoComboBox({
        dataTextField:'location_name',
        dataValueField:'location_id',
        dataSource: {
            transport:{
                read:{
                    url     :'<?php echo URL.$v_admin_key;?>/json',
                    dataType:'json',
                    type    : 'POST',
                    data    :   {txt_session_id: '<?php echo session_id();?>', txt_company_id: '<?php echo $v_company_id;?>', txt_json_type: 'load_location'}
                }
            }
        }
    }).data('kendoComboBox');
    combo_location.value(<?php echo $v_location_id;?>);
    /*
    var combo_tag = $('select#txt_template_tag').width(500).kendoMultiSelect({
        dataTextField:'tag_name',
        dataValueField:'tag_id',
        dataSource: {
            transport:{
                read:{
                    url     :'<?php //echo URL.$v_admin_key;?>/json',
                    dataType:'json',
                    type    : 'POST',
                    data    :   {txt_session_id: '<?php //echo session_id();?>', txt_company_id: '<?php //echo $v_company_id;?>', txt_json_type: 'load_tag'}
                }
            }
        }
    }).data('kendoMultiSelect');
    combo_tag.value(<?php //echo json_encode($arr_template_tag);?>);
    */
    var template_width = $("#txt_template_width").kendoNumericTextBox({
        format: "#.#0 inches",
        min: 0,
        max: 100,
        step: 0.01
    }).change(function(e){
        var val = $(this).val();
        val = parseFloat(val);
        var bleed = template_bleed.value();
        bleed = parseFloat(bleed);
        canvas_width.value(val + 2 * bleed);
    }).data('kendoNumericTextBox');

    var template_height = $("#txt_template_height").kendoNumericTextBox({
        format: "#.#0 inches",
        min: 0,
        max: 100,
        step: 0.01
    }).change(function(e){
        var val = $(this).val();
        val = parseFloat(val);
        var bleed = template_bleed.value();
        bleed = parseFloat(bleed);
        canvas_height.value(val + 2 * bleed);
    }).data('kendoNumericTextBox');
    
    var canvas_width = $("#txt_canvas_width").kendoNumericTextBox({
        format: "#.#0 inches",
        min: 0,
        max: 100,
        step: 0.01
    }).change(function(e){
        var val = $(this).val();
        val = parseFloat(val);
        var bleed = template_bleed.value();
        bleed = parseFloat(bleed);
        template_width.value(val - 2 * bleed);
    }).data('kendoNumericTextBox');

    var canvas_height = $("#txt_canvas_height").kendoNumericTextBox({
        format: "#.#0 inches",
        min: 0,
        max: 100,
        step: 0.01
    }).change(function(e){
        var val = $(this).val();
        val = parseFloat(val);
        var bleed = template_bleed.value();
        bleed = parseFloat(bleed);
        template_height.value(val - 2 * bleed);
    }).data('kendoNumericTextBox');
    $("#txt_markup_cost").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    $("#txt_print_cost").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    $("#txt_template_order").kendoNumericTextBox({
        format: "n0",
        step: 1
    });
    var template_bleed = $("#txt_template_bleed").kendoNumericTextBox({
        format: "#.##0 inches",
        decimals: 3,
        step: 0.001,
        min: 0
    }).change(function (e) {
        var bleed = $(this).val();
        bleed = parseFloat(bleed);
        var width = template_width.value();
        var height = template_height.value();
        width = parseFloat(width);
        height = parseFloat(height);
        canvas_width.value(width + 2 * bleed);
        canvas_height.value(height + 2 * bleed);
    }).data('kendoNumericTextBox');
    $('select#txt_folding_type').width(200).kendoDropDownList();
    $('select#txt_folding_direction').width(200).kendoDropDownList();
    $('select#txt_die_cut_type').width(200).kendoDropDownList();
    $('select#txt_category_id').width(200).kendoDropDownList();
    $('select#txt_style_id').width(200).kendoDropDownList();
    $('select#txt_industry_id').width(200).kendoDropDownList();
    $('select[data-key="placeholder"]').width(200).kendoDropDownList();

    $.each($('select[data-key="placeholder"]'), function(idx, el){
        //var $tr = $(this).closest('tr');
        //var val = $(this).val();
        //if(val!='' && val!='none') $tr.find('input[data-key="noprint"]').prop('disabled', true);
        $(this).change(function(e){
            var $tr = $(this).closest('tr');
            var val = $(this).val();
            $tr.find('input[data-key="noprint"]').prop('checked', val!='' && val!='none');
            $tr.find('input[data-key="noprint"]').prop('disabled', val!='' && val!='none');
        });
    });

    $.each($('input[data-key="crop"]'), function(idx, el){
        //var $tr = $(this).closest('tr');
        //var val = $(this).val();
        //if(val!='' && val!='none') $tr.find('input[data-key="noprint"]').prop('disabled', true);
        $(this).click(function(e){
            var $tr = $(this).closest('tr');
            var val = $(this).is(':checked');
            $tr.find('input[data-key="release"]').prop('checked', false);
            $tr.find('input[data-key="release"]').prop('disabled', !val);
            $tr.find('input[data-key="noreplace"]').prop('checked', false);
            $tr.find('input[data-key="noreplace"]').prop('disabled', !val);
        });
    });

    $('input[data-key="top"]').kendoNumericTextBox({
        step: 0.001, format: '#.##0 inches', decimals: 3
    });
    $('input[data-key="left"]').kendoNumericTextBox({
        step: 0.001, format: '#.##0 inches', decimals: 3
    });
    /*
    $("select#txt_filter_key").kendoDropDownList({
        change: filter_type_changed
    });
    function filter_type_changed(){
        multi_product.options.filter = $("#txt_filter_key").val();
    }
    var set_search = function (e) {
        if (e.type != "keypress" || kendo.keys.ENTER == e.keyCode) {
            multi_product.search($("#txt_filter_value").val());
            e.preventDefault();
        }
    };
    $("button#btn_filter").click(set_search);
    $("#txt_filter_value").keypress(set_search);

    var multi_product = $('select#txt_product_list').width(500).kendoMultiSelect({
        dataTextField: "product_sku",
        dataValueField: "product_id",
        // define custom template
        itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="width:150px; float:left; margin-right: 5px" src=\"<?php echo URL;?>${data.product_image}\" alt=\"${data.description}\" />' +
            '<h3>${ data.product_sku }</h3>' +
            '<p>#= data.description #</p></div>',
        tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"<?php echo URL;?>${data.product_image}\" alt=\"${data.description}\" />' +
            '#: data.product_sku #',
        dataSource:{
            transport:{
                read:{
                    url     :'<?php //echo URL.$v_admin_key;?>/json',
                    dataType:'json',
                    type    : 'POST',
                    data    :   {txt_session_id: '<?php //echo session_id();?>', txt_company_id: '<?php //echo $v_company_id;?>', txt_json_type: 'load_product'}
                    ,beforeSend: function(){
                    }
                }
            }
        }
    }).data("kendoMultiSelect");
    multi_product.value(<?php //echo json_encode($arr_product_list);?>);
    */
    var window_design = $('div#window_design');
    $('input#btn_launch_design').bind('click', function(){
        if(!window_design.data("kendoWindow")){
            window_design.kendoWindow({
                title: "Design tool for template<?php echo $v_template_id>0?': \"'.$v_template_name.'\"':''?>",
                width: "800px",
                modal: true,
                iframe: true,
                content: '<?php echo URL.$v_admin_key;?>/<?php echo $v_template_id;?>/template-design/?template_id=<?php echo $v_template_id;?>',
                type: "GET",
                actions: ["Minimize", "Maximize", "Close"],
                close: function(){
                    location.reload();
                }
            }).data('kendoWindow').bind('close', function(e){
                if(typeof view.isDesignSaved != 'undefined' && !view.isDesignSaved){
                    e.preventDefault();
                }
            });

        }
        window_design.data("kendoWindow").center().open().maximize();
    });

    var objects = <?php echo json_encode($arr_saved_data);?>;
    var stores = <?php echo json_encode($arr_saved_data);?>;
    var flags = <?php echo json_encode($arr_saved_flag);?>;
    //alert(JSON.stringify(objects['text']['full_name']));
    $.each($('[data-object]'), function(idx, e){
        $(this).change(function(e){
            var val = $(this).val();
            var object = $(this).attr('data-object');
            var field = $(this).attr('data-field');
            var key = $(this).attr('data-key');
            var name = $(this).attr('data-name');
            if(field=='checkbox') val = $(this).is(':checked');
            stores[object][name][key] = val;
            flags[object][name][key] = val==objects[object][name][key];
            /*
            if(val!=objects[object][name][key]){
                stores[object][name][key] = val;
                flags[object][name][key] = false;
            }else{
                flags[object][name][key] = true;
            }
            */
        });
    });

    function check_object_change(){
        var r = 0;
        $.each(flags, function(obj){
            $.each(this, function(name){
                $.each(this, function(key){
                    if(!flags[obj][name][key]){
                        r = flags[obj][name]['side'];
                    }
                });
            });
        });
        return r;
    }

    function reset_object_change(){
        objects = stores;
        $.each(flags, function(obj){
            $.each(this, function(name){
                $.each(this, function(key){
                    flags[obj][name][key] = true;
                });
            });
        });
    }

    var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if(!$v_disabled_company_id){;?>
	$('select#txt_company_id').change(function(e){
        var company_id = $(this).val();
        company_id = parseInt(company_id, 10);
        if(isNaN(company_id) || company_id <0) company_id = 0;
        $.ajax({
            url:    '<?php echo URL.$v_admin_key;?>/ajax',
            type:'POST',
            dataType:'json',
            data:{txt_company_id: company_id, txt_ajax_type:'load_company_info'},
            beforeSend: function(){},
            success: function(data){
                if(data.error==0){
                    var location = data.location;
                    combo_location.setDataSource(location);
                    combo_location.value(0);
                    var tag = data.tag;
                    combo_tag.setDataSource(tag);
                    combo_tag.value({});
                    var product = data.product;
                    multi_product.setDataSource(product);
                    multi_product.value({});
                    $('form#frm_tb_design_template').find('#txt_company_id').val(company_id);
                }
            }
        });
	});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>

    <?php if($v_template_design){
    ?>
    //tab_strip.select(tab_strip.tabGroup.children("li:last"));
    <?php }?>
    select_tab_strip();
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Template<?php echo $v_template_id>0?': '.$v_template_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_design_template" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_template_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_template_id" name="txt_template_id" value="<?php echo $v_template_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_stock_cost" name="txt_stock_cost" value="<?php echo $v_stock_cost;?>" />
<input type="hidden" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" />
<input type="hidden" id="txt_user_name" name="txt_user_name" value="<?php echo $v_user_name;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Apply for Sites</li>
                        <li>Categories</li>
                        <!--
                        <li>Tags</li>
                        -->
                        <li>Description</li>
                        <?php if($v_template_id>0){?>
                            <li>Themes</li>
                            <?php if($v_count_design>0){?>
                                <li>Customer's Design</li>
                            <?php }?>
                            <?php if(sizeof($arr_list_element)>0){ ?>
                                <?php if(isset($arr_list_element[1])){ ?>
                                    <li>Side 1: Elements</li>
                                    <li>Side 2: Elements</li>
                                <?php }else{ ?>
                                    <li>Elements</li>
                                <?php }?>
                            <?php }?>
                            <li>Design&nbsp;</li>
                        <?php }?>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr valign="top">
        <td align="right" style="width: 200px;">Location</td>
        <td  style="width:2px" align="right">&nbsp;</td>
        <td align="left">
            <select id="txt_location_id" name="txt_location_id"></select>
        </td>
    </tr>

    <tr align="right" valign="top">
		<td>Template Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_template_name" name="txt_template_name" value="<?php echo $v_template_name;?>" /> <label id="lbl_template_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Template Width</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_template_width" name="txt_template_width" value="<?php echo $v_template_width;?>" />
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Template Height</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_template_height" name="txt_template_height" value="<?php echo $v_template_height;?>" /></td>
	</tr>
    <tr align="right" valign="top">
        <td>Template Bleed</td>
        <td>&nbsp;</td>
        <td align="left"><input type="text" id="txt_template_bleed" name="txt_template_bleed" value="<?php echo $v_template_bleed;?>" /></td>
    </tr>
    <tr align="right" valign="top">
        <td>Canvas Width</td>
        <td>&nbsp;</td>
        <td align="left"><input type="text" id="txt_canvas_width" name="txt_canvas_width" value="<?php echo $v_template_width + 2 * $v_template_bleed;?>" />
        </td>
    </tr>
    <tr align="right" valign="top">
        <td>Canvas Height</td>
        <td>&nbsp;</td>
        <td align="left"><input type="text" id="txt_canvas_height" name="txt_canvas_height" value="<?php echo $v_template_height + 2 * $v_template_bleed;?>" /></td>
    </tr>
<tr align="right" valign="top">
		<td>Folding Type</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_folding_type" name="txt_folding_type">
                <?php
                echo $cls_settings->draw_option_with_order_sort('folding_type',0, $v_folding_type);
                ?>
            </select>
        </td>
	</tr>
    <tr align="right" valign="top">
        <td>Folding Direction</td>
        <td>&nbsp;</td>
        <td align="left">
            <select id="txt_folding_direction" name="txt_folding_direction">
                <?php
                echo $cls_settings->draw_option_by_id('folding_direction',0, $v_folding_direction);
                ?>
            </select>
        </td>
    </tr>
<tr align="right" valign="top">
		<td>Die Cut Type</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_die_cut_type" name="txt_die_cut_type">
                <?php
                echo $cls_settings->draw_option_with_order_sort('die_cut_type',0, $v_die_cut_type);
                ?>
            </select>
        </td>
	</tr>
    <tr align="right" valign="top">
        <td>Markup Cost</td>
        <td>&nbsp;</td>
        <td align="left"><input type="text" id="txt_markup_cost" name="txt_markup_cost" value="<?php echo $v_markup_cost;?>" /> <label id="lbl_markup_cost" class="k-required">(*)</label></td>
    </tr>
<tr align="right" valign="top">
		<td>Print Cost</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_print_cost" name="txt_print_cost" value="<?php echo $v_print_cost;?>" /> <label id="lbl_print_cost" class="k-required">(*)</label></td>
	</tr>
<?php if($v_template_id>0){?>
    <tr align="right" valign="top">
        <td>Stock Cost</td>
        <td>&nbsp;</td>
        <td align="left"><?php echo format_currency($v_stock_cost);?>&nbsp;</td>
    </tr>
<tr align="right" valign="top">
		<td>Created by</td>
		<td>&nbsp;</td>
		<td align="left"><?php echo $v_user_full_name;?>&nbsp;</td>
	</tr>
<?php }?>
<tr align="right" valign="top">
		<td>Template Order</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_template_order" name="txt_template_order" value="<?php echo $v_template_order;?>" /></td>
	</tr>
    <tr align="right" valign="top">
        <td>Template Status</td>
        <td>&nbsp;</td>
        <td align="left">
            <div class="checkbox"><input type="checkbox" id="txt_template_status" name="txt_template_status"<?php echo $v_template_status==0?' checked="checked"':'';?> /><label for="txt_template_status"> Active</label></div></td>
    </tr>
    <tr align="right" valign="top">
        <td>Show in Home Page</td>
        <td>&nbsp;</td>
        <td align="left"><div class="checkbox"><input type="checkbox" id="txt_is_home" name="txt_is_home"<?php echo $v_is_home==1?' checked="checked"':'';?> /><label for="txt_is_home"> Show it</label></div></td>
    </tr>
    <tr align="right" valign="top">
        <td>&nbsp</td>
        <td>&nbsp;</td>
        <td align="left"><div class="checkbox"><input type="checkbox" id="txt_template_design" name="txt_template_design"<?php echo $v_template_design?' checked="checked"':'';?> /><label for="txt_template_design"> Return to design</label></div></td>
    </tr>
</table>
                    </div>

                    <div class="site div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width: 200px">Filter</td>
                                <td  style="width: 2px" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_filter_site">
                                        <option value="startswith">Starts with</option>
                                        <option value="contains">Contains</option>
                                        <option value="eq">Equal</option>
                                    </select>
                                    <input type="text" class="k-textbox" id="txt_filter_site_value" />
                                    <button id="btn_filter_site" class="k-button">Find site</button>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Apply for Sites</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_site_id" name="txt_site_id[]" multiple="multiple"></select>
                                </td>
                            </tr>
                        </table>

                    </div>
                    <!--
                    <div class="product div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width: 200px">Filter</td>
                                <td  style="width: 2px" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_filter_key">
                                        <option value="startswith">Starts with</option>
                                        <option value="contains">Contains</option>
                                        <option value="eq">Equal</option>
                                    </select>
                                    <input type="text" class="k-textbox" id="txt_filter_value" />
                                    <button id="btn_filter" class="k-button">Find product</button>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Apply for Products</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_product_list" name="txt_product_list[]" multiple="multiple"></select>
                                </td>
                            </tr>
                        </table>

                    </div>
                    -->
                    <div class="tag div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr valign="top">
                                <td align="right" style="width:200px">Product Categories</td>
                                <td style="width:2px" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_category_id" name="txt_category_id">
                                        <option value="0">--- Select Category ---</option>
                                        <?php
                                        echo $cls_settings->draw_option_with_order_sort('template_category',1, $v_category_id);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <!--
                            <tr valign="top">
                                <td align="right" style="width: 200px">Tag</td>
                                <td  style="width: 2px" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_template_tag" name="txt_template_tag[]" multiple="multiple"></select>
                                </td>
                            </tr>
                            -->
                            <tr valign="top">
                                <td align="right">Design Style</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_style_id" name="txt_style_id">
                                        <?php
                                        echo $cls_settings->draw_option_with_order_sort('template_style',0, $v_style_id);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr valign="top">
                                <td align="right">Design Industries</td>
                                <td width="0" align="right">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_industry_id" name="txt_industry_id">
                                        <?php
                                        echo $cls_settings->draw_option_with_order_sort('template_industry',0, $v_industry_id);
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="description div_details">
                        <h3 style="color: red">Click bellow to add / edit description</h3>
                        <div contentEditable id="txt_template_desc"><?php echo $v_template_desc;?></div>
                    </div>
<?php if($v_template_id>0){?>
    <div class="theme div_details">
<style type="text/css">
    ul.add_theme{
        list-style-type:none;
        height:40px;
        margin:0;
    }
    ul.add_theme li{
        list-style-type:none;
        float:right;
        margin:0 0 0 5px;
        cursor:pointer;
    }
    ul.add_theme li img{
        border:none;
    }
    ul.add_theme li p{
        text-align:center;
        margin:0px;
        font-size:11px;
    }
    ul.add_theme li a{
        color:inherit;
        text-decoration:none;
    }

</style>
        <div style="width: 100%; padding-right:10px; text-align: right">
            <ul class="add_theme">
                <li>
                    <a href="<?php echo URL.$v_admin_key;?>/<?php echo $v_template_id; ?>/theme-add" target="_self">
                        <p><img id="icons" src="<?php echo URL;?>images/icons/add.png" title="Add New" /></p><p>Add Theme</p>
                    </a>
                </li>
            </ul>

        </div>
        <div id="theme_grid">

        </div>

    </div>
    <?php if($v_count_design>0){?>
        <div class="customer div_details">
            <div id="design_grid">

            </div>
        </div>
    <?php }?>
    <?php if(sizeof($arr_list_element)>0){?>
        <?php for($i=0; $i<sizeof($arr_list_element);$i++){
        ?>
        <div class="element_<?php echo $i;?> div_details">
            <input data-action="btn_save_data" type="button" class="k-button" value="Save Changes" data-side="<?php echo $i;?>" data-template="<?php echo $v_template_id;?>" />
            <?php
            if(isset($arr_list_element[$i]['text'])) echo $arr_list_element[$i]['text'];
            if(isset($arr_list_element[$i]['image'])) echo $arr_list_element[$i]['image'];
            ?>
            <input data-action="btn_save_data" type="button" class="k-button" value="Save Changes" data-side="<?php echo $i;?>" data-template="<?php echo $v_template_id;?>" />
        </div>
        <?php
        }
        ?>
    <?php }?>
    <div class="design div_details">
        <input id="btn_launch_design" type="button" class="k-button" value="Launch Design" />
        <div id="window_design" style="display: none">
            <?php
            //include 'qry_template_design.php';
            //include 'dsp_template_design.php';
            ?>
        </div>
        <?php
        if($v_template_sample!='') echo '<img id="img_template_preview" src="'.$v_template_sample.'?'.time().'" />';
        ?>
        <div id="vector_pdf_link" style="clear: both; margin-top: 20px; margin-bottom: 5px; height: 30px">

        </div>
        <div>
            <input id="btn_create_vector_pdf" type="button" class="k-button" value="Create Vector PDF" /> &nbsp;&nbsp; <img src="<?php URL;?>images/icons/loading.gif" id="img_svg_loading" style="display: none" />
        </div>
    </div>
<?php }?>
                   </div>

                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_template" name="btn_submit_tb_design_template" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
<script type="text/javascript">
$(document).ready(function(e){
    $(window).load(function(e){
        <?php
        if($v_image_missing > 0){
            $v_msg = 'There '.($v_image_missing == 1?'is':'are').' missing '.($v_image_missing == 1?'image':'images').' from template.\nSelect tab "Elements" and remove '.($v_image_missing == 1?'it':'them').', otherwise the design could not be launched.';
        ?>
        alert('<?php echo $v_msg;?>')
        <?php }?>
    });
})
</script>