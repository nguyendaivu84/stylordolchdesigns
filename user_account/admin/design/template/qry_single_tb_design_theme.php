<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_theme_id = 0;
$v_theme_name = '';
$v_theme_status = 0;
$v_theme_color = 'ffffff';
$v_list_color = '';
$v_list_image = '';
$v_theme_order = 0;
$v_add_markup_cost = 0;
$v_created_time = date('Y-m-d H:i:s', time());
$v_template_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_template_id,'int');
$v_sample_image = '0';
$v_company_id = 0;
$v_location_id = 0;
$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
$v_theme_desc = '';
$v_saved_dir = '';
$v_new_design_theme = true;
$v_theme_design = false;
if(isset($_POST['btn_submit_tb_design_theme'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_design_theme->set_mongo_id($v_mongo_id);
    $v_theme_id = isset($_POST['txt_theme_id'])?$_POST['txt_theme_id']:$v_theme_id;
    if(is_null($v_mongo_id)){
        //$v_theme_id = $cls_tb_design_theme->select_next('theme_id');
    }
    $v_theme_id = (int) $v_theme_id;
    $cls_tb_design_theme->set_theme_id($v_theme_id);
    $v_theme_name = isset($_POST['txt_theme_name'])?$_POST['txt_theme_name']:$v_theme_name;
    $v_theme_name = trim($v_theme_name);
    if($v_theme_name=='') $v_error_message .= '[Theme Name] is empty!<br />';
    $cls_tb_design_theme->set_theme_name($v_theme_name);
    $v_theme_status = isset($_POST['txt_theme_status'])?0:1;
    $v_theme_status = (int) $v_theme_status;
    $cls_tb_design_theme->set_theme_status($v_theme_status);
    $v_theme_color = isset($_POST['txt_theme_color'])?$_POST['txt_theme_color']:$v_theme_color;
    $v_theme_color = trim($v_theme_color);
    $v_theme_color = str_replace('#', '', $v_theme_color);
    if(strlen($v_theme_color)!=6) $v_theme_color = 'ffffff';
    $cls_tb_design_theme->set_theme_color($v_theme_color);
    $v_add_markup_cost = isset($_POST['txt_add_markup_cost'])?$_POST['txt_add_markup_cost']:$v_add_markup_cost;
    $v_add_markup_cost = (float) $v_add_markup_cost;
    if($v_add_markup_cost<0) $v_error_message .= '[Add Markup Cost] is negative!<br />';
    $cls_tb_design_theme->set_add_markup_cost($v_add_markup_cost);
    $v_created_time = date('Y-m-d H:i:s');
    $cls_tb_design_theme->set_created_time($v_created_time);
    $v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:$v_template_id;
    $v_template_id = (int) $v_template_id;
    if($v_template_id<0) $v_error_message .= '[Template Id] is negative!<br />';
    $cls_tb_design_theme->set_template_id($v_template_id);
    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
    $v_company_id = (int) $v_company_id;
    if($v_company_id<0) $v_company_id = 0;
    $cls_tb_design_theme->set_company_id($v_company_id);
    $v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
    $v_location_id = (int) $v_location_id;
    if($v_location_id<0) $v_location_id =0;
    $cls_tb_design_theme->set_location_id($v_location_id);
    $v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:$v_user_id;
    $v_user_id = (int) $v_user_id;
    if($v_user_id<0) $v_user_id = 0;
    $cls_tb_design_theme->set_user_id($v_user_id);
    $v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:$v_user_name;
    $v_user_name = trim($v_user_name);
    if($v_user_name=='') $v_error_message .= '[User Name] is empty!<br />';
    $cls_tb_design_theme->set_user_name($v_user_name);
    $v_theme_desc = isset($_POST['txt_theme_desc'])?$_POST['txt_theme_desc']:$v_theme_desc;
    $v_theme_desc = trim($v_theme_desc);
    $cls_tb_design_theme->set_theme_desc($v_theme_desc);

    $v_theme_order = isset($_POST['txt_theme_order'])?$_POST['txt_theme_order']:$v_theme_order;
    settype($v_theme_order, 'int');
    $cls_tb_design_theme->set_theme_order($v_theme_order);

    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_theme_id = $cls_tb_design_theme->insert();
            $v_result = $v_theme_id > 0;
        }else{
            $arr_fields = array('theme_name', 'theme_color', 'theme_status', 'add_markup_cost', 'theme_desc', 'theme_order');
            $arr_values = array($v_theme_name, $v_theme_color, $v_theme_status, $v_add_markup_cost, $v_theme_desc, $v_theme_order);
            $v_result = $cls_tb_design_theme->update_fields($arr_fields, $arr_values, array('_id' => $v_mongo_id));
            $v_new_design_theme = false;
        }
        if($v_result){
            $v_theme_design = isset($_POST['chk_return_design']);
            if(!$v_theme_design){
                $_SESSION['ss_tb_design_theme_redirect'] = 1;
                redir(URL.$v_admin_key.'/'.$v_template_id.'/edit');
            }else{
                redir(URL.$v_admin_key.'/'.$v_theme_id.'/theme-edit');
            }
        }else{
            if($v_new_design_theme) $v_theme_id = 0;
        }
    }
}else{
    if($v_act == 'ETH'){
        $v_theme_id= isset($_GET['id'])?$_GET['id']:'0';
    }
    settype($v_theme_id,'int');
    if($v_theme_id>0){
        $v_row = $cls_tb_design_theme->select_one(array('theme_id' => $v_theme_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_design_theme->get_mongo_id();
            $v_theme_id = $cls_tb_design_theme->get_theme_id();
            $v_theme_name = $cls_tb_design_theme->get_theme_name();
            $v_theme_status = $cls_tb_design_theme->get_theme_status();
            $v_theme_color = $cls_tb_design_theme->get_theme_color();
            $v_list_color = $cls_tb_design_theme->get_list_color();
            $v_list_image = $cls_tb_design_theme->get_list_image();
            $v_add_markup_cost = $cls_tb_design_theme->get_add_markup_cost();
            $v_created_time = date('Y-m-d H:i:s',$cls_tb_design_theme->get_created_time());
            $v_template_id = $cls_tb_design_theme->get_template_id();
            $v_sample_image = $cls_tb_design_theme->get_sample_image();
            $v_company_id = $cls_tb_design_theme->get_company_id();
            $v_location_id = $cls_tb_design_theme->get_location_id();
            $v_user_id = $cls_tb_design_theme->get_user_id();
            $v_user_name = $cls_tb_design_theme->get_user_name();
            $v_theme_desc = $cls_tb_design_theme->get_theme_desc();
            $v_saved_dir = $cls_tb_design_theme->get_saved_dir();
            $v_theme_order = $cls_tb_design_theme->get_theme_order();
        }
    }else{
        $v_row = $cls_tb_design_template->select_one(array('template_id'=>$v_template_id));
        if($v_row==1){
            $v_company_id = $cls_tb_design_template->get_company_id();
            $v_location_id = $cls_tb_design_template->get_location_id();
        }
    }
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);

$v_theme_color = '#'.$v_theme_color;
$v_template_name = '';
$v_sample_url = '';
if($v_template_id>0){
    $v_theme_url = str_replace(ROOT_DIR.DS,'', DESIGN_THEME_DIR);
    $v_theme_url = str_replace('\\','/', $v_theme_url);
    $v_template_name = $cls_tb_design_template->select_scalar('template_name', array('template_id'=>$v_template_id));
    $v_sample_url = URL.$v_theme_url.'/'.$v_template_id.'/template_'.$v_template_id.'_theme_0.png';
    if($v_theme_id>0){
        if(file_exists($v_saved_dir.$v_sample_image))
            $v_sample_url = URL.$v_saved_dir.$v_sample_image;
    }
}
$v_theme_name = htmlentities($v_theme_name);
?>