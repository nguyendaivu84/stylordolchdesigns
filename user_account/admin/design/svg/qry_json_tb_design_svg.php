<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
if(sizeof($arr_sort)==0) $arr_sort = array('order'=>1);
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_design_svg_redirect']) && $_SESSION['ss_tb_design_svg_redirect']==1){
	if(isset($_SESSION['ss_tb_design_svg_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_design_svg_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_design_svg_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_design_svg_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_design_svg_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_design_svg->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_design_svg_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_design_svg_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_design_svg_page'] = $v_page;
$_SESSION['ss_tb_design_svg_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_design_svg = $cls_tb_design_svg->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_design_svg as $arr){
	$v_svg_id = isset($arr['svg_id'])?$arr['svg_id']:0;
	$v_svg_name = isset($arr['svg_name'])?$arr['svg_name']:'';
	$v_svg_file = isset($arr['svg_file'])?$arr['svg_file']:'';
	$v_svg_key = isset($arr['svg_key'])?$arr['svg_key']:'';
	$v_svg_status = isset($arr['svg_status'])?$arr['svg_status']:0;
	$v_svg_cost = isset($arr['svg_cost'])?$arr['svg_cost']:0;
	$v_svg_order = isset($arr['svg_order'])?$arr['svg_order']:0;
	$v_svg_desc = isset($arr['svg_desc'])?$arr['svg_desc']:'';
	$v_svg_data = isset($arr['svg_data'])?$arr['svg_data']:'';
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_image_file = isset($arr['image_file'])?$arr['image_file']:'';

    if($v_image_file!=''){
        if(!file_exists($v_image_file)){
            if($cls_tb_design_svg->create_thumb($v_svg_data, ROOT_DIR . DS . $v_saved_dir, $v_svg_key, DESIGN_IMAGE_THUMB_SIZE )){
                $v_image_file = $v_saved_dir . DESIGN_IMAGE_THUMB_SIZE .'_' . $v_svg_key .'.png';
                $cls_tb_design_svg->update_field('image_file', $v_image_file, array('svg_id'=>$v_svg_id));
                $v_image_file = URL . $v_image_file.'?'.time();
            }else $v_image_file = '';
        }
    }
    if($v_image_file == ''){
        $v_image_file = URL .'images/sample.png' ;
    }

	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'svg_id' => $v_svg_id,
		'svg_name' => $v_svg_name,
		'svg_file' => $v_svg_file,
		'image_file' => $v_image_file,
		'svg_key' => $v_svg_key,
		'svg_status' => $v_svg_status,
		'svg_cost' => $v_svg_cost,
		'svg_order' => $v_svg_order,
		'svg_desc' => $v_svg_desc,
		'svg_data' => $v_svg_data,
		'saved_dir' => $v_saved_dir,
		'created_time' => date('d-M-y H:i:s', $v_created_time->sec),
		'company_id' => $v_company_id,
		'location_id' => $v_location_id,
		'template_id' => $v_template_id,
		'theme_id' => $v_theme_id,
		'design_id' => $v_design_id,
		'user_id' => $v_user_id,
		'user_ip' => $v_user_ip,
		'user_name' => $v_user_name,
		'user_agent' => $v_user_agent
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_svg'=>$arr_ret_data);
echo json_encode($arr_return);
?>