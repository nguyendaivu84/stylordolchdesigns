<?php if(!isset($v_sval)) die();?>
<?php
$v_svg_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_svg_id, 'int');
if($v_svg_id>0){
    $v_row = $cls_tb_design_svg->select_one(array('svg_id'=>$v_svg_id));
    if($v_row==1){
        $v_saved_dir = $cls_tb_design_svg->get_saved_dir();
        $v_svg_file = $cls_tb_design_svg->get_svg_file();
        if($cls_tb_design_svg->delete(array('svg_id' => $v_svg_id))){
            if(file_exists(ROOT_DIR.DS.$v_saved_dir . $v_svg_file)) @unlink(ROOT_DIR.DS.$v_saved_dir . $v_svg_file);

            $v_row = $cls_tb_design_image->select_one(array('svg_id'=>$v_svg_id));
            if($v_row==1){
                $v_saved_dir = $cls_tb_design_image->get_saved_dir();
                $v_image_id = $cls_tb_design_image->get_image_id();
                $v_image_file = $cls_tb_design_image->get_image_file();
                $v_thumb_file = ROOT_DIR.DS.$v_saved_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
                $v_original_file = ROOT_DIR.DS.$v_saved_dir.IMAGE_ORIGINAL_PREFIX.'_'.$v_image_file;

                $v_image_file = ROOT_DIR.DS.$v_saved_dir.$v_image_file;
                if(file_exists($v_image_file)) @unlink($v_image_file);
                if(file_exists($v_thumb_file)) @unlink($v_thumb_file);
                if(file_exists($v_original_file)) @unlink($v_original_file);

                $v_image_file = $cls_tb_design_image->get_image_original_file();
                if($v_image_file!=''){
                    $v_image_file = ROOT_DIR.DS.$v_saved_dir.$v_image_file;
                    if(file_exists($v_image_file)) @unlink($v_image_file);
                }
                $cls_tb_design_image->delete(array('image_id' => $v_image_id));
            }
        }
    }
}
$_SESSION['ss_tb_design_svg_redirect'] = 1;
redir(URL.$v_admin_key);
?>