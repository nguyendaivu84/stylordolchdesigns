<?php
if(!isset($v_sval)) die();

$v_svg_id = isset($_POST['txt_svg_id'])?$_POST['txt_svg_id']:'0';
settype($v_svg_id, 'int');

$v_success = 0;
$v_message = '';

if($v_svg_id>0){
    $v_row = $cls_tb_design_svg->select_one(array('svg_id'=>$v_svg_id));
    if($v_row ==1){
        $v_svg_file = $cls_tb_design_svg->get_svg_file();
        $v_saved_dir = $cls_tb_design_svg->get_saved_dir();
        $arr_svg_colors = $cls_tb_design_svg->get_svg_colors();
        $v_svg_key = $cls_tb_design_svg->get_svg_key();
        $v_svg_data = $cls_tb_design_svg->get_svg_data();
        $v_svg_cost = $cls_tb_design_svg->get_svg_cost();

        $v_user_id = $cls_tb_design_svg->get_user_id();
        $v_user_name = $cls_tb_design_svg->get_user_name();
        $v_user_agent = $cls_tb_design_svg->get_user_agent();

        $fp = @fopen($v_saved_dir . $v_svg_file, 'w');
        if($fp){
            fwrite($fp, $v_svg_data, strlen($v_svg_data));
            fflush($fp);
            fclose($fp);

            $v_full_path = ROOT_DIR . DS . $v_saved_dir . $v_svg_file;
            $arr_u = array(
                'user_id'=>$v_user_id,
                'user_ip'=>$v_user_ip,
                'user_name'=>$v_user_name,
                'user_agent'=>$v_user_agent
            );
            $arr_info = array(
                'id'=>$v_svg_id,
                'key'=>$v_svg_key,
                'file'=>$v_svg_file,
                'root'=>ROOT_DIR,
                'ds'=>DS,
                'thumb'=>DESIGN_IMAGE_THUMB_SIZE,
                'dir'=>$v_saved_dir,
                'cost'=>$v_svg_cost,
                'colors'=>$arr_svg_colors
            );
            $arr_image = $cls_tb_design_image->save_svg_image($v_full_path, $arr_u, $arr_info);
            if(isset($arr_image['id']) && isset($arr_image['file'])) $cls_tb_design_svg->update_fields(array('image_file', 'image_id'), array($arr_image['file'], $arr_image['id']), array('svg_id' => $v_svg_id));
            $v_success = 1;
        }else{
            $v_message = 'Cannot save file!';
        }

    }else{
        $v_message = 'Not found info!';
    }
}else{
    $v_message = 'Lost data!';
}

$arr_return = array('success'=>$v_success, 'message'=>$v_message);
$cls_output->output($arr_return, true, false);