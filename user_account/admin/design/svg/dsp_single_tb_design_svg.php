<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_svg").click(function(e){
        /*
        var company_id = $("input#txt_company_id").val();
        company_id = parseInt(company_id, 10);
        if(isNaN(company_id) || company_id<0) company_id = 0;
        if(company_id==0){
            e.preventDefault();
            alert("Please select Company!");
            $('select#txt_company_id').focus();
            combo_company.focus();
            return false;
        }
        */
        if(!validator.validate()){
            e.preventDefault();
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
		return true;
	});

    $('input#txt_svg_cost').kendoNumericTextBox({
        format: "c2",
        min:0,
        step: 0.01
    });
    $('input#txt_svg_order').kendoNumericTextBox({
        format: "n0",
        step: 1
    });

    $('input#txt_svg_name').focusout(function(e){
        var name = $(this).val();
        name = $.trim(name);
        if(name==''){
            $(this).val('');
            $('input#txt_hidden_svg_name').val('N');
            validator.validate();
            return false;
        }
        var id = $('input#txt_svg_id').val();
        $.ajax({
            url: '<?php echo URL.$v_admin_key;?>/ajax',
            type:   'POST',
            dataType:'json',
            data:   {txt_svg_id: id, txt_svg_name: name, txt_ajax_type:'check_svg'},
            beforeSend: function(){
                $('span#sp_svg_name').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_svg_name').html('');
                if(data.success==1)
                    $('input#txt_hidden_svg_name').val('Y');
                else
                    $('input#txt_hidden_svg_name').val('');
                validator.validate();
            }
        });
        return true;
    });
    <?php if($v_svg_id>0){?>
    $('img#img_generate_svg').click(function(e){
        var loading = '<?php echo URL;?>images/icons/loading.gif';
        var src = $(this).attr('src');
        var $this = $(this);
        $.ajax({
            url             : '<?php echo URL . $v_admin_key;?>/ajax',
            type            : 'POST',
            dataType        : 'json',
            data            : {txt_svg_id: '<?php echo $v_svg_id;?>', txt_ajax_type: 'generate_svg_file'},
            beforeSend      : function(){
                $this.attr('src', loading);
            },
            success         : function(data){
                if(data.success==1){
                    $this.closest('tr').remove();
                }else{
                    alert(data.message);
                    $this.attr('src', src);
                }
            },
            error           : function(xhr){
                $this.attr('src', src);
            }
        });
    });
    var colors = <?php echo json_encode($arr_tmp_colors);?>;
    var combo_color = $('select#txt_svg_color').width(400).kendoMultiSelect({
        dataSource: colors,
        dataTextField: 'text',
        dataValueField: 'value',
        itemTemplate: '<div style="height:20px; clear:both;"><div style="width: 20px; height: 20px; float: left; background-color: #: data.text #">&nbsp;</div>' +
            '<div style="margin-left: 25px"><h3>${ data.text }</h3></div>' +
            '</div>',
        tagTemplate:  '<span class="k-tile" style="width: 20px; height: 10px; background-color: #: data.text #">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;' +
            '#: data.text #'

    }).data('kendoMultiSelect');
    combo_color.value(<?php echo json_encode($arr_svg_colors);?>);
    <?php }?>
    $('input#txt_svg_file').kendoUpload({
        select: on_select
    });
    function on_select(e){
        if(e.files.length>1){
            e.preventDefault();
            alert('Please choose only one file!');
            return false;
        }
        var ex = e.files[0].extension.toLowerCase();
        if(ex!='.svg'){
            e.preventDefault();
            alert('Please choose SVG file!');
            return false;
        }
        return true;
    }
    $('.k-upload').css('width', '250px').css('float', 'left');
    //$('.k-upload').css('float', 'left');
    $('.k-upload-button span').html('Select SVG...');

    var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_design_svg').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Design_svg</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_design_svg" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_svg_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_svg_id" name="txt_svg_id" value="<?php echo $v_svg_id;?>" />
    <input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
    <input type="hidden" id="txt_location_id" name="txt_location_id" value="<?php echo $v_location_id;?>" />
    <input type="hidden" id="txt_location_id" name="txt_location_id" value="<?php echo $v_location_id;?>" />
    <input type="hidden" id="txt_template_id" name="txt_template_id" value="<?php echo $v_template_id;?>" />
    <input type="hidden" id="txt_theme_id" name="txt_theme_id" value="<?php echo $v_theme_id;?>" />
    <input type="hidden" id="txt_design_id" name="txt_design_id" value="<?php echo $v_design_id;?>" />
    <input type="hidden" id="txt_hidden_svg_file" name="txt_hidden_svg_file" value="<?php echo $v_svg_file;?>" />
    <input type="hidden" id="txt_hidden_saved_dir" name="txt_hidden_saved_dir" value="<?php echo $v_saved_dir;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <?php if($v_svg_id > 0){?>
                        <li>Manage Colors</li>
                        <?php }?>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td style="width:200px">Name</td>
		<td style="width:2px">&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" type="text" id="txt_svg_name" name="txt_svg_name" value="<?php echo $v_svg_name;?>" required data-required-msg="Please, input SVG name" />
            <input type="hidden" id="txt_hidden_svg_name" name="txt_hidden_svg_name" value="<?php echo $v_svg_name!=''?'Y':'N';?>" required data-required-msg="Duplicate SVG Name" />
            <span class="tooltips"><a title="SVG name is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a> </span>
            <span id="sp_svg_name"></span>
            <label id="lbl_svg_name" class="k-required">(*)</label>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>SVG File</td>
		<td>&nbsp;</td>
		<td align="left">
            <input type="file" id="txt_svg_file" name="txt_svg_file" value="<?php echo $v_svg_file;?>" />
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Svg Status</td>
		<td>&nbsp;</td>
		<td align="left">
            <div class="checkbox"><input type="checkbox" id="txt_svg_status" name="txt_svg_status"<?php echo $v_svg_status==0?' checked="checked"':'';?> /><label for="txt_svg_status">Active</label></div>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Svg Cost</td>
		<td>&nbsp;</td>
		<td align="left"><input type="number" id="txt_svg_cost" name="txt_svg_cost" value="<?php echo $v_svg_cost;?>" /></td>
	</tr>
<tr align="right" valign="top">
		<td>Svg Order</td>
		<td>&nbsp;</td>
		<td align="left"><input type="number" id="txt_svg_order" name="txt_svg_order" value="<?php echo $v_svg_order;?>" /></td>
	</tr>
<tr align="right" valign="top">
		<td>Svg Desc</td>
		<td>&nbsp;</td>
		<td align="left">
            <textarea class="k-textbox" style="width: 300px; height: 100px; padding: 5px; resize: none" id="txt_svg_desc" name="txt_svg_desc"><?php echo $v_svg_desc;?></textarea>
        </td>
	</tr>
</table>
                    </div>
                <?php if($v_svg_id > 0){?>
                    <div class="color div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width:200px">Preview</td>
                                <td style="width:2px">&nbsp;</td>
                                <td align="left">
                                    <img src="<?php echo $v_image_file;?>" />
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Colors</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <select id="txt_svg_color" name="txt_svg_color[]" multiple="multiple">

                                    </select>
                                </td>
                            </tr>
                            <tr align="left">
                                <td colspan="3">
                                    <label class="k-required">
                                        (*) Please see preview, then remove unnecessary colors
                                    </label>
                                </td>
                            </tr>
                            <?php if($v_missing_data_file){?>
                                <tr align="left">
                                    <td colspan="3">
                                        <span class="k-required">
                                            Missing data file! Click <img style="vertical-align: baseline; cursor: pointer" src="<?php echo URL;?>images/icons/lightning.png" id="img_generate_svg" /> to generate it!
                                        </span>
                                    </td>
                                </tr>
                            <?php }?>
                        </table>
                    </div>
                <?php }?>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_svg" name="btn_submit_tb_design_svg" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
