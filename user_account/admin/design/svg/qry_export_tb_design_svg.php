<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_svg_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_svg_sort'])){
	$v_sort = $_SESSION['ss_tb_design_svg_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_svg = $cls_tb_design_svg->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_design_svg_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Design_svg')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Design_svg');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg File', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Key', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Cost', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Order', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Desc', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Data', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Saved Dir', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Created Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Theme Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Design Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Ip', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Agent', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_design_svg as $arr){
	$v_excel_col = 1;
	$v_svg_id = isset($arr['svg_id'])?$arr['svg_id']:0;
	$v_svg_name = isset($arr['svg_name'])?$arr['svg_name']:'';
	$v_svg_file = isset($arr['svg_file'])?$arr['svg_file']:'';
	$v_svg_key = isset($arr['svg_key'])?$arr['svg_key']:'';
	$v_svg_status = isset($arr['svg_status'])?$arr['svg_status']:0;
	$v_svg_cost = isset($arr['svg_cost'])?$arr['svg_cost']:0;
	$v_svg_order = isset($arr['svg_order'])?$arr['svg_order']:0;
	$v_svg_desc = isset($arr['svg_desc'])?$arr['svg_desc']:'';
	$v_svg_data = isset($arr['svg_data'])?$arr['svg_data']:'';
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_file, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_key, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_cost, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_order, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_desc, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_data, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_saved_dir, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_created_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_theme_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_design_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_ip, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_agent, 'left');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>