<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_svg_id = 0;
$v_svg_name = '';
$v_svg_file = '';
$v_svg_key = '';
$v_svg_status = 0;
$v_svg_cost = 0;
$v_svg_order = 0;
$v_svg_desc = '';
$arr_svg_colors = array();
$arr_svg_file_colors = array();
$v_svg_data = '';
$v_saved_dir = '';
$v_created_time = date('Y-m-d H:i:s', time());
$v_company_id = 0;
$v_location_id = 0;
$v_template_id = 0;
$v_theme_id = 0;
$v_design_id = 0;
$v_user_id = 0;
$v_user_ip = '';
$v_user_name = '';
$v_user_agent = '';
$v_new_design_svg = true;
$v_image_file = '';
$v_missing_data_file = false;
if(isset($_POST['btn_submit_tb_design_svg'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_design_svg->set_mongo_id($v_mongo_id);
	$v_svg_id = isset($_POST['txt_svg_id'])?$_POST['txt_svg_id']:$v_svg_id;
	if(is_null($v_mongo_id)){
		//$v_svg_id = $cls_tb_design_svg->select_next('svg_id');
	}
	$v_svg_id = (int) $v_svg_id;
	$cls_tb_design_svg->set_svg_id($v_svg_id);
	$v_svg_name = isset($_POST['txt_svg_name'])?$_POST['txt_svg_name']:$v_svg_name;
	$v_svg_name = trim($v_svg_name);
	if($v_svg_name=='') $v_error_message .= '[Svg Name] is empty!<br />';
	$cls_tb_design_svg->set_svg_name($v_svg_name);
    $v_svg_key = seo_friendly_url($v_svg_name);
    if($v_svg_key=='')
        $v_error_message .= '[Svg Name] is invalid!<br />';
    else{
        $arr_where_clause = array();
        $arr_where_clause['svg_key'] = $v_svg_key;
        $arr_where_clause['svg_id'] = array('$ne'=>$v_svg_id);
        if($cls_tb_design_svg->count($arr_where_clause) > 0)$v_error_message .= '[Svg Name] is unique!<br />';
    }
    $cls_tb_design_svg->set_svg_key($v_svg_key);
    $v_svg_status = isset($_POST['txt_svg_status'])?0:1;
    $cls_tb_design_svg->set_svg_status($v_svg_status);
    $v_svg_cost = isset($_POST['txt_svg_cost'])?$_POST['txt_svg_cost']:$v_svg_cost;
    $v_svg_cost = (float) $v_svg_cost;
    if($v_svg_cost<0) $v_error_message .= '[Svg Cost] is negative!<br />';
    $cls_tb_design_svg->set_svg_cost($v_svg_cost);
    $v_svg_order = isset($_POST['txt_svg_order'])?$_POST['txt_svg_order']:$v_svg_order;
    $v_svg_order = (int) $v_svg_order;
    $cls_tb_design_svg->set_svg_order($v_svg_order);
    $v_svg_desc = isset($_POST['txt_svg_desc'])?$_POST['txt_svg_desc']:$v_svg_desc;
    $v_svg_desc = trim($v_svg_desc);
    $cls_tb_design_svg->set_svg_desc($v_svg_desc);
    $v_created_time = date('Y-m-d H:i:s');
    $cls_tb_design_svg->set_created_time($v_created_time);
    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
    $v_company_id = (int) $v_company_id;
    if($v_company_id<0) $v_company_id = 0;
    $cls_tb_design_svg->set_company_id($v_company_id);
    $v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
    $v_location_id = (int) $v_location_id;
    if($v_location_id<0) $v_location_id = 0;
    $cls_tb_design_svg->set_location_id($v_location_id);
    $v_template_id = isset($_POST['txt_template_id'])?$_POST['txt_template_id']:$v_template_id;
    $v_template_id = (int) $v_template_id;
    if($v_template_id<0) $v_template_id = 0;
    $cls_tb_design_svg->set_template_id($v_template_id);
    $v_theme_id = isset($_POST['txt_theme_id'])?$_POST['txt_theme_id']:$v_theme_id;
    $v_theme_id = (int) $v_theme_id;
    if($v_theme_id<0) $v_theme_id = 0;
    $cls_tb_design_svg->set_theme_id($v_theme_id);
    $v_design_id = isset($_POST['txt_design_id'])?$_POST['txt_design_id']:$v_design_id;
    $v_design_id = (int) $v_design_id;
    if($v_design_id<0) $v_design_id = 0;
    $cls_tb_design_svg->set_design_id($v_design_id);

    $v_old_key = '';
    $v_old_file = '';
    $v_content = '';
    $v_full_path = '';
    $v_saved_dir = '';
    $v_upload_success = false;
    if(!empty($_FILES['txt_svg_file'])){
        if($v_svg_key!='' && $v_error_message==''){
            add_class('cls_upload');
            add_class('ManageFile', 'cls_file.php');
            $v_svg_dir = DESIGN_SVG_DIR;
            $mf = new ManageFile($v_svg_dir, DS);
            $up = cls_upload::factory($v_svg_dir);
            $up->file($_FILES['txt_svg_file']);
            $up->set_new_file_name($v_svg_key.'.svg');
            $arr_result = $up->upload();
            if($arr_result['status']){
                $v_old_key = $cls_tb_design_svg->select_scalar('svg_key', array('svg_id'=>$v_svg_id));
                $v_old_file = $cls_tb_design_svg->select_scalar('svg_file', array('svg_id'=>$v_svg_id));

                $v_svg_file = $up->get_current_name();
                $v_full_path = $up->get_result_path();
                //$mf->read_file($v_svg_key.'.svg');
                $mf->read_file($v_svg_file);
                $v_content = $mf->get_file_content();
                $arr_colors = array();
                preg_match_all('/#[0-9a-f]{6}/i', $v_content, $arr_colors);
                if(isset($arr_colors[0])) $arr_colors = $arr_colors[0];
                if(!is_array($arr_colors)) $arr_colors = array();
                for($i=0; $i<sizeof($arr_colors); $i++){
                    $v_color = str_replace('#', '', strtolower($arr_colors[$i]));
                    if(strlen($v_color)==6){
                        if(!in_array($v_color, $arr_svg_colors)) $arr_svg_colors[] = $v_color;
                    }
                }
                $v_upload_success = true;
                $v_saved_dir = str_replace(ROOT_DIR . DS , '', $v_svg_dir);
                $v_saved_dir = str_replace(DS, '/', $v_saved_dir) . '/';
                if($v_old_file==$v_svg_file) $v_old_file = '';
                //die;
            }
        }
    }else{
    	$v_svg_file = isset($_POST['txt_hidden_svg_file'])?$_POST['txt_hidden_svg_file']:$v_svg_file;
    	$v_saved_dir = isset($_POST['txt_hidden_saved_dir'])?$_POST['txt_hidden_saved_dir']:$v_saved_dir;
	    $v_svg_file = trim($v_svg_file);
	    $v_saved_dir = trim($v_saved_dir);
    }
    $arr_svg_file_colors = $arr_svg_colors;
    $cls_tb_design_svg->set_svg_file($v_svg_file);
    $cls_tb_design_svg->set_saved_dir($v_saved_dir);
	$cls_tb_design_svg->set_svg_colors($arr_svg_colors);
	$cls_tb_design_svg->set_svg_file_colors($arr_svg_colors);
	$cls_tb_design_svg->set_svg_data($v_content);


	if($v_error_message==''){
		if(is_null($v_mongo_id)){
            $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
            $v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
            $v_user_ip = get_real_ip_address();
            $v_user_agent = $browser->getUserAgent();

            $cls_tb_design_svg->set_user_id($v_user_id);
            $cls_tb_design_svg->set_user_ip($v_user_ip);
            $cls_tb_design_svg->set_user_name($v_user_name);
            $cls_tb_design_svg->set_user_agent($v_user_agent);

			$v_svg_id = $cls_tb_design_svg->insert();
			$v_result = $v_svg_id > 0;
            if($v_result){
                if($v_upload_success){
                    $arr_u = array(
                        'user_id'=>$v_user_id,
                        'user_ip'=>$v_user_ip,
                        'user_name'=>$v_user_name,
                        'user_agent'=>$v_user_agent
                    );
                    $arr_info = array(
                        'id'=>$v_svg_id,
                        'key'=>$v_svg_key,
                        'file'=>$v_svg_file,
                        'root'=>ROOT_DIR,
                        'ds'=>DS,
                        'thumb'=>DESIGN_IMAGE_THUMB_SIZE,
                        'dir'=>$v_saved_dir,
                        'cost'=>$v_svg_cost,
                        'colors'=>$arr_svg_colors
                    );
                    $arr_image = $cls_tb_design_image->save_svg_image($v_full_path, $arr_u, $arr_info);
                    if(isset($arr_image['id']) && isset($arr_image['file'])) $cls_tb_design_svg->update_fields(array('image_file', 'image_id'), array($arr_image['file'], $arr_image['id']), array('svg_id' => $v_svg_id));
                }
            }
		}else{
            if($v_upload_success){
                $arr_fields = array('svg_name', 'svg_key', 'svg_file', 'svg_cost', 'svg_status', 'svg_order', 'svg_desc', 'svg_data', 'svg_colors', 'svg_file_colors');
                $arr_values = array($v_svg_name, $v_svg_key, $v_svg_file, $v_svg_cost, $v_svg_status, $v_svg_order, $v_svg_desc, $v_content, $arr_svg_colors, $arr_svg_colors);
            }else{
                $arr_tmp_svg_colors = isset($_POST['txt_svg_color'])?$_POST['txt_svg_color']:array();
                if(!is_array($arr_tmp_svg_colors)){
                    $v_tmp_svg_color = $_POST['txt_svg_color'];
                    if(get_magic_quotes_gpc()) $v_tmp_svg_color = stripslashes($v_tmp_svg_color);
                    $arr_tmp_svg_colors = json_decode($v_tmp_svg_color, true);
                    if(!is_array($arr_tmp_svg_colors)) $arr_tmp_svg_colors = array();
                    $arr_tmp_svg_colors = array();
                }
                $arr_svg_colors = $arr_tmp_svg_colors;

                $arr_fields = array('svg_name', 'svg_cost', 'svg_status', 'svg_order', 'svg_desc', 'svg_colors');
                $arr_values = array($v_svg_name, $v_svg_cost, $v_svg_status, $v_svg_order, $v_svg_desc, $arr_svg_colors);
            }
			//$v_result = $cls_tb_design_svg->update(array('_id' => $v_mongo_id));
            $v_result = $cls_tb_design_svg->update_fields($arr_fields, $arr_values, array('_id' => $v_mongo_id));
            if($v_result){
                if($v_upload_success){
                    if($v_old_key!='' && file_exists($v_saved_dir . $v_old_key.'.png')) @unlink($v_saved_dir . $v_old_key.'.png');
                    if($v_old_file!='' && file_exists($v_saved_dir . $v_old_file)) @unlink($v_saved_dir . $v_old_file);
                    $arr_u = array(
                        'user_id'=>$v_user_id,
                        'user_ip'=>$v_user_ip,
                        'user_name'=>$v_user_name,
                        'user_agent'=>$v_user_agent
                    );
                    $arr_info = array(
                        'id'=>$v_svg_id,
                        'key'=>$v_svg_key,
                        'file'=>$v_svg_file,
                        'root'=>ROOT_DIR,
                        'ds'=>DS,
                        'thumb'=>DESIGN_IMAGE_THUMB_SIZE,
                        'dir'=>$v_saved_dir,
                        'cost'=>$v_svg_cost,
                        'colors'=>$arr_svg_colors
                    );
                    $arr_image = $cls_tb_design_image->save_svg_image($v_full_path, $arr_u, $arr_info);
                    if(isset($arr_image['id']) && isset($arr_image['file'])) $cls_tb_design_svg->update_fields(array('image_file', 'image_id'), array($arr_image['file'], $arr_image['id']), array('_id' => $v_mongo_id));
                }else{
                    $v_row = $cls_tb_design_svg->select_one(array('_id' => $v_mongo_id));
                    if($v_row==1){
                        $arr_u = array(
                            'user_id'=>$v_user_id,
                            'user_ip'=>$v_user_ip,
                            'user_name'=>$v_user_name,
                            'user_agent'=>$v_user_agent
                        );
                        $arr_info = array(
                            'id'=>$cls_tb_design_svg->get_svg_id(),
                            'key'=>$cls_tb_design_svg->get_svg_key(),
                            'file'=>$cls_tb_design_svg->get_svg_file(),
                            'root'=>ROOT_DIR,
                            'ds'=>DS,
                            'thumb'=>DESIGN_IMAGE_THUMB_SIZE,
                            'dir'=>$cls_tb_design_svg->get_saved_dir(),
                            'cost'=>$cls_tb_design_svg->get_svg_cost(),
                            'colors'=>$cls_tb_design_svg->get_svg_colors()
                        );
                        $arr_svg_file_colors = $cls_tb_design_svg->get_svg_file_colors();
                        $v_full_path = $cls_tb_design_svg->get_saved_dir() . $cls_tb_design_svg->get_svg_file();
                        $arr_image = $cls_tb_design_image->save_svg_image($v_full_path, $arr_u, $arr_info);
                        if(isset($arr_image['id']) && isset($arr_image['file'])) $cls_tb_design_svg->update_fields(array('image_file', 'image_id'), array($arr_image['file'], $arr_image['id']), array('_id' => $v_mongo_id));
                    }
                }
            }
			$v_new_design_svg = false;
		}
		if($v_result){
			$_SESSION['ss_tb_design_svg_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_design_svg) $v_svg_id = 0;
		}
	}
}else{
	$v_svg_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_svg_id,'int');
	if($v_svg_id>0){
		$v_row = $cls_tb_design_svg->select_one(array('svg_id' => $v_svg_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_design_svg->get_mongo_id();
			$v_svg_id = $cls_tb_design_svg->get_svg_id();
			$v_svg_name = $cls_tb_design_svg->get_svg_name();
			$v_svg_file = $cls_tb_design_svg->get_svg_file();
			$v_svg_key = $cls_tb_design_svg->get_svg_key();
			$v_svg_status = $cls_tb_design_svg->get_svg_status();
			$v_svg_cost = $cls_tb_design_svg->get_svg_cost();
			$v_svg_order = $cls_tb_design_svg->get_svg_order();
			$v_svg_desc = $cls_tb_design_svg->get_svg_desc();
			$arr_svg_colors = $cls_tb_design_svg->get_svg_colors();
			$arr_svg_file_colors = $cls_tb_design_svg->get_svg_file_colors();
			$v_svg_data = $cls_tb_design_svg->get_svg_data();
			$v_saved_dir = $cls_tb_design_svg->get_saved_dir();
			$v_created_time = date('Y-m-d H:i:s',$cls_tb_design_svg->get_created_time());
			$v_company_id = $cls_tb_design_svg->get_company_id();
			$v_location_id = $cls_tb_design_svg->get_location_id();
			$v_template_id = $cls_tb_design_svg->get_template_id();
			$v_theme_id = $cls_tb_design_svg->get_theme_id();
			$v_design_id = $cls_tb_design_svg->get_design_id();
			$v_user_id = $cls_tb_design_svg->get_user_id();
			$v_user_ip = $cls_tb_design_svg->get_user_ip();
			$v_user_name = $cls_tb_design_svg->get_user_name();
			$v_user_agent = $cls_tb_design_svg->get_user_agent();
            $v_image_file = $cls_tb_design_svg->get_image_file().'?'.time();

            $v_missing_data_file = !file_exists($v_saved_dir . $v_svg_file) || !is_file($v_saved_dir . $v_svg_file);
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);

$v_size = sizeof($arr_svg_file_colors);
$arr_tmp_colors = array();
for($i=0; $i<$v_size; $i++){
    $v_color = $arr_svg_file_colors[$i];
    $arr_tmp_colors[] = array('value'=>$v_color, 'text'=>strlen($v_color)==6?'#'.strtoupper($v_color):$v_color);
}
?>