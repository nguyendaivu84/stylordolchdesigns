<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_svg_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_svg_sort'])){
	$v_sort = $_SESSION['ss_tb_design_svg_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_svg = $cls_tb_design_svg->select($arr_where_clause, $arr_sort);
$v_dsp_tb_design_svg = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_design_svg .= '<tr align="center" valign="middle">';
$v_dsp_tb_design_svg .= '<th>Ord</th>';
$v_dsp_tb_design_svg .= '<th>Svg Id</th>';
$v_dsp_tb_design_svg .= '<th>Svg Name</th>';
$v_dsp_tb_design_svg .= '<th>Svg File</th>';
$v_dsp_tb_design_svg .= '<th>Svg Key</th>';
$v_dsp_tb_design_svg .= '<th>Svg Status</th>';
$v_dsp_tb_design_svg .= '<th>Svg Cost</th>';
$v_dsp_tb_design_svg .= '<th>Svg Order</th>';
$v_dsp_tb_design_svg .= '<th>Svg Desc</th>';
$v_dsp_tb_design_svg .= '<th>Svg Data</th>';
$v_dsp_tb_design_svg .= '<th>Saved Dir</th>';
$v_dsp_tb_design_svg .= '<th>Created Time</th>';
$v_dsp_tb_design_svg .= '<th>Company Id</th>';
$v_dsp_tb_design_svg .= '<th>Location Id</th>';
$v_dsp_tb_design_svg .= '<th>Template Id</th>';
$v_dsp_tb_design_svg .= '<th>Theme Id</th>';
$v_dsp_tb_design_svg .= '<th>Design Id</th>';
$v_dsp_tb_design_svg .= '<th>User Id</th>';
$v_dsp_tb_design_svg .= '<th>User Ip</th>';
$v_dsp_tb_design_svg .= '<th>User Name</th>';
$v_dsp_tb_design_svg .= '<th>User Agent</th>';
$v_dsp_tb_design_svg .= '</tr>';
$v_count = 1;
foreach($arr_tb_design_svg as $arr){
	$v_dsp_tb_design_svg .= '<tr align="left" valign="middle">';
	$v_dsp_tb_design_svg .= '<td align="right">'.($v_count++).'</td>';
	$v_svg_id = isset($arr['svg_id'])?$arr['svg_id']:0;
	$v_svg_name = isset($arr['svg_name'])?$arr['svg_name']:'';
	$v_svg_file = isset($arr['svg_file'])?$arr['svg_file']:'';
	$v_svg_key = isset($arr['svg_key'])?$arr['svg_key']:'';
	$v_svg_status = isset($arr['svg_status'])?$arr['svg_status']:0;
	$v_svg_cost = isset($arr['svg_cost'])?$arr['svg_cost']:0;
	$v_svg_order = isset($arr['svg_order'])?$arr['svg_order']:0;
	$v_svg_desc = isset($arr['svg_desc'])?$arr['svg_desc']:'';
	$v_svg_data = isset($arr['svg_data'])?$arr['svg_data']:'';
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_name.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_file.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_key.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_status.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_cost.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_order.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_desc.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_svg_data.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_saved_dir.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_created_time.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_company_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_location_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_template_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_theme_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_design_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_user_ip.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_user_name.'</td>';
	$v_dsp_tb_design_svg .= '<td>'.$v_user_agent.'</td>';
	$v_dsp_tb_design_svg .= '</tr>';
}
$v_dsp_tb_design_svg .= '</table>';
?>