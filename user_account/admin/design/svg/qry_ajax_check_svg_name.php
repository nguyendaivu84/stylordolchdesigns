<?php
if(!isset($v_sval)) die();
$v_svg_id = isset($_POST['txt_svg_id'])?$_POST['txt_svg_id']:'0';
$v_svg_name = isset($_POST['txt_svg_name'])?$_POST['txt_svg_name']:'';
settype($v_svg_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK', 'success'=>0);
$v_svg_key = seo_friendly_url($v_svg_name);
if($v_svg_key!=''){
   //$arr_where_clause = array('site_name'=>new MongoRegex('/'.$v_site_name.'/i'), 'site_id'=>array('$ne'=>$v_site_id));
    $arr_where_clause = array('svg_key'=>$v_svg_key, 'svg_id'=>array('$ne'=>$v_svg_id));
    if($cls_tb_design_svg->count($arr_where_clause)>0){
        $arr_return['error'] = 2;
        $arr_return['message'] = 'Duplicate SVG Name';
    }else{
        $arr_return['success'] = 1;
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'Lost data';
}

$cls_output->output($arr_return, true, false);