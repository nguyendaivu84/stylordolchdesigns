<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_site_id = 0;
$v_site_key = $cls_tb_design_key->create_key('');

$v_account_limit = false;
$arr_account = array();
$v_concurrent_limit = false;
$v_concurrent = 0;
$arr_site_feature = array();

$v_site_name = '';
$v_site_index = '';
$v_site_url = '';
$v_contact_name = '';
$v_contact_phone = '';
$v_contact_email = '';
$v_site_ip = '';
$v_site_logo = '';
$v_started_time = date('Y-m-d H:i:s', time());
$v_ended_time = date('Y-m-d H:i:s', time()+3600*24*365);
$v_site_forever = 0;
$v_site_status = 0;
$v_company_id = isset($_SESSION['ss_last_company_id'])?$_SESSION['ss_last_company_id']:'0';
settype($v_company_id, 'int');
$v_location_id = 0;
$v_new_design_key = true;
if(isset($_POST['btn_submit_tb_design_key'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_design_key->set_mongo_id($v_mongo_id);
	$v_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:$v_site_id;
    $v_tmp_site_id = $v_site_id;
	if(is_null($v_mongo_id)){
		//$v_site_id = $cls_tb_design_key->select_next('site_id');
	}
	$v_site_id = (int) $v_site_id;
	$v_tmp_site_id = (int) $v_tmp_site_id;
	$cls_tb_design_key->set_site_id($v_site_id);

	$v_site_key = isset($_POST['txt_site_key'])?$_POST['txt_site_key']:'';
	$v_site_key = trim($v_site_key);
    $v_site_key = $cls_tb_design_key->create_key($v_site_key,30, $v_tmp_site_id);
	$cls_tb_design_key->set_site_key($v_site_key);
	$v_site_name = isset($_POST['txt_site_name'])?$_POST['txt_site_name']:$v_site_name;
	$v_site_name = trim($v_site_name);
	if($v_site_name=='') $v_error_message .= '[Site Name] is empty!<br />';
    $cls_tb_design_key->set_site_name($v_site_name);

    $v_site_index = seo_friendly_url($v_site_name);
    $arr_where = array('site_index'=>$v_site_index, 'site_id'=>array('$ne'=>$v_site_id));
    if($cls_tb_design_key->count($arr_where)>0){
        $v_error_message .= '[Site Name] is unique!<br />';
    }else{
        $v_site_index = seo_friendly_url($v_site_name);
        $v_tmp_site_index = $v_site_index;
        $i = 1;
        do{
            $v_found = false;
            $arr_where = array('site_index'=>$v_tmp_site_index, 'site_id'=>array('$ne'=>$v_site_id));
            if($cls_tb_design_key->count($arr_where)>0){
                $v_found = true;
                $v_tmp_site_index = $v_site_index.'_'.$i;
                $i++;
            }
        }while($v_found);
        $v_site_index = $v_tmp_site_index;
    }

    $cls_tb_design_key->set_site_index($v_site_index);

	$v_site_url = isset($_POST['txt_site_url'])?$_POST['txt_site_url']:$v_site_url;
	$v_site_url = trim($v_site_url);
	if($v_site_url=='')
        $v_error_message .= '[Site URL] is empty!<br />';
    elseif(!is_valid_url($v_site_url)) $v_error_message .= '[Site URL] is invalid!<br />';
    else{
        $arr_where = array('site_url'=>$v_site_url, 'site_id'=>array('$ne'=>$v_site_id));
        if($cls_tb_design_key->count($arr_where)>0)
            $v_error_message .= '[Site URL] is unique!<br />';
    }
	$cls_tb_design_key->set_site_url($v_site_url);

    $v_site_logo = isset($_POST['txt_site_logo'])?$_POST['txt_site_logo']:$v_site_logo;
    $v_site_logo = trim($v_site_logo);
    if($v_site_logo=='')
        $v_error_message .= '[Site Logo] is empty!<br />';
    else if(!is_valid_url($v_site_logo)) $v_error_message .= '[Site Logo] is invalid!<br />';
    $cls_tb_design_key->set_site_logo($v_site_logo);

	$v_contact_name = isset($_POST['txt_contact_name'])?$_POST['txt_contact_name']:$v_contact_name;
	$v_contact_name = trim($v_contact_name);
	if($v_contact_name=='') $v_error_message .= '[Contact Name] is empty!<br />';
	$cls_tb_design_key->set_contact_name($v_contact_name);
	$v_contact_phone = isset($_POST['txt_contact_phone'])?$_POST['txt_contact_phone']:$v_contact_phone;
	$v_contact_phone = trim($v_contact_phone);
	//if($v_contact_phone=='') $v_error_message .= '[Contact Phone] is empty!<br />';
	$cls_tb_design_key->set_contact_phone($v_contact_phone);
	$v_contact_email = isset($_POST['txt_contact_email'])?$_POST['txt_contact_email']:$v_contact_email;
	$v_contact_email = trim($v_contact_email);
	if($v_contact_email=='')
        $v_error_message .= '[Customer Email] is empty!<br />';
    else if(!is_valid_email($v_contact_email)) $v_error_message .= '[Contact Email] is invalid!<br />';
	$cls_tb_design_key->set_contact_email($v_contact_email);
	$v_site_ip = isset($_POST['txt_site_ip'])?$_POST['txt_site_ip']:$v_site_ip;
	$v_site_ip = trim($v_site_ip);
	//if($v_site_ip=='') $v_error_message .= '[Site Ip] is empty!<br />';
	$cls_tb_design_key->set_site_ip($v_site_ip);
	$v_started_time = isset($_POST['txt_started_time'])?$_POST['txt_started_time']:$v_started_time;
    //echo 'Start: '.$v_started_time.'</br>';
	if(!check_date($v_started_time)) $v_error_message .= '[Stated Time] is invalid date/time!<br />';
    //echo 'Start: '.$v_started_time.'</br>';
	$cls_tb_design_key->set_started_time($v_started_time);
	$v_ended_time = isset($_POST['txt_ended_time'])?$_POST['txt_ended_time']:$v_ended_time;
    //die( 'Start: '.$v_ended_time.'</br>');
	if(!check_date($v_ended_time)) $v_error_message .= '[Ended Time] is invalid date/time!<br />';
	$cls_tb_design_key->set_ended_time($v_ended_time);
	$v_site_forever = isset($_POST['txt_site_forever'])?1:0;
	$cls_tb_design_key->set_site_forever($v_site_forever);
	$v_site_status = isset($_POST['txt_site_status'])?0:1;
	$cls_tb_design_key->set_site_status($v_site_status);
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
	$v_company_id = (int) $v_company_id;
	if($v_company_id<0) $v_company_id = 0;
	$cls_tb_design_key->set_company_id($v_company_id);
	$v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
	$v_location_id = (int) $v_location_id;
	if($v_location_id<0) $v_location_id = 0;
	$cls_tb_design_key->set_location_id($v_location_id);

    $v_site_feature = isset($_POST['txt_site_feature'])?$_POST['txt_site_feature']:'';
    if($v_site_feature!=''){
        $v_site_feature = stripcslashes($v_site_feature);
        $arr_site_feature = json_decode($v_site_feature, true);
        if(!is_array($arr_site_feature)) $arr_site_feature = array();
    }
    $arr_tmp = array();
    foreach($arr_site_feature as $key=> $arr){
        for($i=0;$i<count($arr);$i++)
            $arr_tmp[$key][$arr[$i]] = 1;
    }
    $cls_tb_design_key->set_feature($arr_tmp);

    $v_account_limit = isset($_POST['txt_account_limit']);
    if(!$v_account_limit){
        $arr_account = isset($_POST['txt_account'])?$_POST['txt_account']:array();
        if(!is_array($arr_account) || sizeof($arr_account)==0) $arr_account = array('no_account');
    }
    $v_concurrent_limit = isset($_POST['txt_concurrent_limit']);
    if($v_concurrent_limit){
        $v_concurrent = 0;
    }else{
        $v_concurrent = isset($_POST['txt_concurrent'])?$_POST['txt_concurrent']:1;
        settype($v_concurrent, 'int');
        if($v_concurrent<1) $v_concurrent = 1;
    }
    $cls_tb_design_key->set_concurrent_session($v_concurrent);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_site_id = $cls_tb_design_key->insert();
			$v_result = $v_site_id > 0;
		}else{
			$v_result = $cls_tb_design_key->update(array('_id' => $v_mongo_id));
			$v_new_design_key = false;
		}
		if($v_result){
			$_SESSION['ss_tb_design_key_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_design_key) $v_site_id = 0;
		}
	}
}else{
	$v_site_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_site_id,'int');
	if($v_site_id>0){
		$v_row = $cls_tb_design_key->select_one(array('site_id' => $v_site_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_design_key->get_mongo_id();
			$v_site_id = $cls_tb_design_key->get_site_id();
			$v_site_key = $cls_tb_design_key->get_site_key();
			$v_site_name = $cls_tb_design_key->get_site_name();
			$v_site_index = $cls_tb_design_key->get_site_index();
			$v_site_url = $cls_tb_design_key->get_site_url();
			$v_contact_name = $cls_tb_design_key->get_contact_name();
			$v_contact_phone = $cls_tb_design_key->get_contact_phone();
			$v_contact_email = $cls_tb_design_key->get_contact_email();
			$v_site_ip = $cls_tb_design_key->get_site_ip();
			$v_site_logo = $cls_tb_design_key->get_site_logo();
			$v_started_time = date('Y-m-d H:i:s',$cls_tb_design_key->get_started_time());
			$v_ended_time = date('Y-m-d H:i:s',$cls_tb_design_key->get_ended_time());
			$v_site_forever = $cls_tb_design_key->get_site_forever();
			$v_site_status = $cls_tb_design_key->get_site_status();
			$v_company_id = $cls_tb_design_key->get_company_id();
			$v_location_id = $cls_tb_design_key->get_location_id();
            $arr_site_feature = $cls_tb_design_key->get_feature();
            $arr_account = $cls_tb_design_key->get_account();
            $v_concurrent = $cls_tb_design_key->get_concurrent_session();
            $v_account_limit = sizeof($arr_account)==0;
            $v_concurrent_limit = $v_concurrent ==0;
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);
$v_started_time = date('d-M-Y H:i:s', strtotime($v_started_time));
$v_ended_time  = date('d-M-Y H:i:s', strtotime($v_ended_time));

$arr_all_user = array();
$arr_tmp_user = $cls_tb_user->select(array('company_id'=>$v_company_id, 'user_status'=>0, 'user_name'=>array('$nin'=>$arr_excluded_user)));
foreach($arr_tmp_user as $arr){
    $arr_all_user[] = array(
        'user_id'=>$arr['user_id']
        ,'user_name'=>$arr['user_name']
    );
}

$arr_feature_text = $cls_settings->select_scalar('option', array('setting_name'=>'feature_text'));
$arr_feature_shape = $cls_settings->select_scalar('option', array('setting_name'=>'feature_shape'));
$arr_feature_image = $cls_settings->select_scalar('option', array('setting_name'=>'feature_image'));
if(!is_array($arr_feature_text)) $arr_feature_text = array();
if(!is_array($arr_feature_shape)) $arr_feature_shape = array();
if(!is_array($arr_feature_image)) $arr_feature_image = array();

$v_dsp_tr = '';
$arr_tmp = array();
$v_count_feature = sizeof($arr_feature_text);
for($i=0; $i<$v_count_feature;$i++){
    $v_status = isset($arr_feature_text[$i]['status'])?$arr_feature_text[$i]['status']:1;
    if($v_status==0)
        $arr_tmp[] = $arr_feature_text[$i];
}
$arr_feature_text = $arr_tmp;
$arr_key_feature = array();
$v_count_feature = sizeof($arr_feature_text);
if($v_count_feature>0){
    //$v_count = 0;
    $v_one = '';
    $j=0;
    for($i=1; $i<$v_count_feature;$i++){
        $v_key = $arr_feature_text[$i]['key'];
        $v_status = isset($arr_feature_text[$i]['status'])?$arr_feature_text[$i]['status']:1;

        $arr_key_feature['text'][] = $v_key;
        $v_one .= '<tr valign="middle" align="left"><td>'.$arr_feature_text[$i]['name'].'</td>';
        $v_one .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_text_'.$i.'" data-feature="text" value="'.$v_key.'"'.(isset($arr_site_feature['text'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_text_'.$i.'">'.$arr_feature_text[$i]['name'].'</label></div></td></tr>';
        $j++;
    }
    $v_key = $arr_feature_text[0]['key'];
    $arr_key_feature['text'][] = $v_key;
    $v_dsp_tr .= '<tr valign="middle" align="left"><td align="right" rowspan="'.(++$j).'">Text\'s Features: </td>';
    $v_dsp_tr .= '<td>'.$arr_feature_text[0]['name'].'</td>';
    $v_dsp_tr .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_text_0" data-feature="text" value="'.$v_key.'"'.(isset($arr_site_feature['text'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_text_0">'.$arr_feature_text[0]['name'].'</label></div></td></tr>';
    $v_dsp_tr .= $v_one;
}

$arr_tmp = array();
$v_count_feature = sizeof($arr_feature_image);
for($i=0; $i<$v_count_feature;$i++){
    $v_status = isset($arr_feature_image[$i]['status'])?$arr_feature_image[$i]['status']:1;
    if($v_status==0)
        $arr_tmp[] = $arr_feature_image[$i];
}
$arr_feature_image = $arr_tmp;
$v_count_feature = sizeof($arr_feature_image);
if($v_count_feature>0){
    $v_one = '';
    $j=0;
    for($i=1; $i<$v_count_feature;$i++){
        $v_key = $arr_feature_image[$i]['key'];
        $v_status = isset($arr_feature_image[$i]['status'])?$arr_feature_image[$i]['status']:1;
        $arr_key_feature['image'][] = $v_key;
        $v_one .= '<tr valign="middle" align="left"><td>'.$arr_feature_image[$i]['name'].'</td>';
        $v_one .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_image_'.$i.'" data-feature="image" value="'.$v_key.'"'.(isset($arr_site_feature['image'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_image_'.$i.'">'.$arr_feature_image[$i]['name'].'</label></div></td></tr>';
        $j++;
    }
    $v_key = $arr_feature_image[0]['key'];
    $arr_key_feature['image'][] = $v_key;
    $v_dsp_tr .= '<tr valign="middle" align="left"><td align="right" rowspan="'.(++$j).'">Image\'s Features: </td>';
    $v_dsp_tr .= '<td>'.$arr_feature_image[0]['name'].'</td>';
    $v_dsp_tr .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_image_0" data-feature="image" value="'.$v_key.'"'.(isset($arr_site_feature['image'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_image_0">'.$arr_feature_image[0]['name'].'</label></div></td></tr>';
    $v_dsp_tr .= $v_one;
}

$arr_tmp = array();
$v_count_feature = sizeof($arr_feature_shape);
for($i=0; $i<$v_count_feature;$i++){
    $v_status = isset($arr_feature_shape[$i]['status'])?$arr_feature_shape[$i]['status']:1;
    if($v_status==0)
        $arr_tmp[] = $arr_feature_shape[$i];
}
$arr_feature_shape = $arr_tmp;

$v_count_feature = sizeof($arr_feature_shape);
if($v_count_feature>0){
    $v_one = '';
    $j=0;
    for($i=1; $i<$v_count_feature;$i++){
        $v_key = $arr_feature_shape[$i]['key'];
        $v_status = isset($arr_feature_shape[$i]['status'])?$arr_feature_shape[$i]['status']:1;
        $arr_key_feature['shape'][] = $v_key;
        $v_one .= '<tr valign="middle" align="left"><td>'.$arr_feature_shape[$i]['name'].'</td>';
        $v_one .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_shape_'.$i.'" data-feature="shape" value="'.$v_key.'"'.(isset($arr_site_feature['shape'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_shape_'.$i.'">'.$arr_feature_shape[$i]['name'].'</label></div></td></tr>';
        $j++;
    }
    $v_key = $arr_feature_shape[0]['key'];
    $arr_key_feature['shape'][] = $v_key;
    $v_dsp_tr .= '<tr valign="middle" align="left"><td align="right" rowspan="'.(++$j).'">Shape\'s Features: </td>';
    $v_dsp_tr .= '<td>'.$arr_feature_shape[0]['name'].'</td>';
    $v_dsp_tr .= '<td><div class="checkbox"><input type="checkbox" name="chk_feature" id="chk_feature_shape_0" data-feature="shape" value="'.$v_key.'"'.(isset($arr_site_feature['shape'][$v_key])?' checked="checked"':''). '/> <label for="chk_feature_shape_0">'.$arr_feature_shape[0]['name'].'</label></div></td></tr>';
    $v_dsp_tr .= $v_one;
}

if($v_dsp_tr!=''){
    $v_dsp_tr = '<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                <tr align="center" valign="middle">
                <th width="30%">Feature Name</th>
                <th width="40%">All Features</th>
                <th width="40%">Assigned Features</th>
                </tr>'.$v_dsp_tr.
                '</table>';
}

$v_delete_log = $cls_settings->get_option_name_by_key('manage_log','delete_log', 0);
settype($v_delete_log, 'int');
$v_delete_log = $v_delete_log==1;

$v_max_times = $cls_settings->get_option_name_by_key('manage_log','delete_day', 30);
settype($v_max_times, 'int');
if($v_max_times<0) $v_max_times = 0;

$v_max_rows = $cls_settings->get_option_name_by_key('manage_log','delete_row', 10000);
settype($v_max_rows, 'int');
if($v_max_rows<0) $v_max_rows = 0;
$v_max_times = time() - 24*3600*$v_max_times;
$v_max_times = date('d-M-Y H:i:s', $v_max_times);

$v_operator = 0;
?>