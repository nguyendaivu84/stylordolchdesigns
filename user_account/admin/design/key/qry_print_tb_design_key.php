<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_key_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_key_sort'])){
	$v_sort = $_SESSION['ss_tb_design_key_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_key = $cls_tb_design_key->select($arr_where_clause, $arr_sort);
$v_dsp_tb_design_key = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_design_key .= '<tr align="center" valign="middle">';
$v_dsp_tb_design_key .= '<th>Ord</th>';
$v_dsp_tb_design_key .= '<th>Key Id</th>';
$v_dsp_tb_design_key .= '<th>Key Value</th>';
$v_dsp_tb_design_key .= '<th>Key Name</th>';
$v_dsp_tb_design_key .= '<th>Customer Site</th>';
$v_dsp_tb_design_key .= '<th>Customer Name</th>';
$v_dsp_tb_design_key .= '<th>Customer Phone</th>';
$v_dsp_tb_design_key .= '<th>Customer Email</th>';
$v_dsp_tb_design_key .= '<th>Customer Ip</th>';
$v_dsp_tb_design_key .= '<th>Stated Time</th>';
$v_dsp_tb_design_key .= '<th>Ended Time</th>';
$v_dsp_tb_design_key .= '<th>Customer Forever</th>';
$v_dsp_tb_design_key .= '<th>Customer Status</th>';
$v_dsp_tb_design_key .= '<th>Company Id</th>';
$v_dsp_tb_design_key .= '<th>Location Id</th>';
$v_dsp_tb_design_key .= '</tr>';
$v_count = 1;
foreach($arr_tb_design_key as $arr){
	$v_dsp_tb_design_key .= '<tr align="left" valign="middle">';
	$v_dsp_tb_design_key .= '<td align="right">'.($v_count++).'</td>';
	$v_key_id = isset($arr['key_id'])?$arr['key_id']:0;
	$v_key_value = isset($arr['key_value'])?$arr['key_value']:'';
	$v_key_name = isset($arr['key_name'])?$arr['key_name']:'';
	$v_customer_site = isset($arr['customer_site'])?$arr['customer_site']:'';
	$v_customer_name = isset($arr['customer_name'])?$arr['customer_name']:'';
	$v_customer_phone = isset($arr['customer_phone'])?$arr['customer_phone']:'';
	$v_customer_email = isset($arr['customer_email'])?$arr['customer_email']:'';
	$v_customer_ip = isset($arr['customer_ip'])?$arr['customer_ip']:'';
	$v_stated_time = isset($arr['stated_time'])?$arr['stated_time']:(new MongoDate(time()));
	$v_ended_time = isset($arr['ended_time'])?$arr['ended_time']:(new MongoDate(time()));
	$v_customer_forever = isset($arr['customer_forever'])?$arr['customer_forever']:'0';
	$v_customer_status = isset($arr['customer_status'])?$arr['customer_status']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_dsp_tb_design_key .= '<td>'.$v_key_id.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_key_value.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_key_name.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_site.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_name.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_phone.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_email.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_ip.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_stated_time.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_ended_time.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_forever.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_customer_status.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_company_id.'</td>';
	$v_dsp_tb_design_key .= '<td>'.$v_location_id.'</td>';
	$v_dsp_tb_design_key .= '</tr>';
}
$v_dsp_tb_design_key .= '</table>';
?>