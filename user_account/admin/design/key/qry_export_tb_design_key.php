<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_key_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_key_sort'])){
	$v_sort = $_SESSION['ss_tb_design_key_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_key = $cls_tb_design_key->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_design_key_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Design_key')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Design_key');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Key Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Key Value', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Key Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Site', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Phone', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Email', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Ip', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Stated Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ended Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Forever', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Customer Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_design_key as $arr){
	$v_excel_col = 1;
	$v_key_id = isset($arr['key_id'])?$arr['key_id']:0;
	$v_key_value = isset($arr['key_value'])?$arr['key_value']:'';
	$v_key_name = isset($arr['key_name'])?$arr['key_name']:'';
	$v_customer_site = isset($arr['customer_site'])?$arr['customer_site']:'';
	$v_customer_name = isset($arr['customer_name'])?$arr['customer_name']:'';
	$v_customer_phone = isset($arr['customer_phone'])?$arr['customer_phone']:'';
	$v_customer_email = isset($arr['customer_email'])?$arr['customer_email']:'';
	$v_customer_ip = isset($arr['customer_ip'])?$arr['customer_ip']:'';
	$v_stated_time = isset($arr['stated_time'])?$arr['stated_time']:(new MongoDate(time()));
	$v_ended_time = isset($arr['ended_time'])?$arr['ended_time']:(new MongoDate(time()));
	$v_customer_forever = isset($arr['customer_forever'])?$arr['customer_forever']:'0';
	$v_customer_status = isset($arr['customer_status'])?$arr['customer_status']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_key_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_key_value, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_key_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_site, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_phone, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_email, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_ip, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_stated_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_ended_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_forever, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_customer_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>