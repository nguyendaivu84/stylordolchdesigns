<?php
if(!isset($v_sval)) die();
?>
<?php
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_all_location = array();
$arr_all_location[] = array('location_id'=>0, 'location_name'=>'--------');

$_SESSION['ss_last_company_id'] = $v_company_id;
if($v_company_id>0){
    $arr_location = $cls_tb_location->select(array('company_id'=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])));
    foreach($arr_location as $arr){
        $arr_all_location[] = array('location_id'=>$arr['location_id'], 'location_name'=>$arr['location_name']);
    }
}
header('Content-Type: application/json');
echo(json_encode($arr_all_location));