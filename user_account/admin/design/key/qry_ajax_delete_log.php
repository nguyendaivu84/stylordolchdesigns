<?php
if(!isset($v_sval)) die();

$v_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:0;
$v_operator = isset($_POST['txt_operator'])?$_POST['txt_operator']:0;
$v_row = isset($_POST['txt_row'])?$_POST['txt_row']:0;
$v_time = isset($_POST['txt_time'])?$_POST['txt_time']:date('Y-m-d H:i:s');
settype($v_site_id, 'int');

if(!check_date($v_time)) $v_time = date('Y-m-d H:i:s');

$v_time = strtotime($v_time);
//$v_time = new MongoDate($v_time);
settype($v_row, 'int');
if($v_row<0) $v_row = 0;
settype($v_operator, 'int');
if($v_operator!=1) $v_operator=0;

$v_status = 0;
$v_message = '';
$v_success = 1;

add_class('cls_tb_site_log');
$cls_log = new cls_tb_site_log($db, LOG_DIR);
if(($v_delete_right || $v_is_super_admin)){
    if($v_operator==0){
        $v_count = $cls_log->count(array('site_id'=>$v_site_id));
        if($v_count>$v_row){

            $v_count_time = $cls_log->count(array('site_id'=>$v_site_id, array('log_time'=>array('$gt'=>new MongoDate($v_time)))));
            if($v_count_time>=$v_row){
                $arr_where_clause = array('log_time'=>array('$lt'=>new MongoDate($v_time)), 'site_id'=>$v_site_id);
                $v_count = $cls_log->count($arr_where_clause);
                if($v_count>0){
                    if($cls_log->delete($arr_where_clause))
                        $v_message = 'Over '.$v_count.' rows have deleted';
                    else{
                        $v_status = 4;
                        $v_message = '1. Unknown error has occurred';
                    }
                }else{
                    $v_message = 'Not any rows have deleted';
                }
            }else{
                $v_status = 1;
                $v_message = 'No log is before '.date('d-M-Y H:i:s');
            }
        }else{
            $v_status = 2;
            $v_message = 'This log is only '.$v_count.' rows';
        }
    }else{
        $v_total_row = $cls_log->count(array('site_id'=>$v_site_id));
        if($v_total_row<$v_row){
            $arr_where_clause = array('site_id'=>$v_site_id, 'log_time'=>array('$lt'=>new MongoDate($v_time)));
            $v_count = $cls_log->count($arr_where_clause);
            if($v_count>0){
                if($cls_log->delete($arr_where_clause)){
                    $v_message = 'Over '.$v_count.' rows have deleted';
                }else{
                    $v_status = 4;
                    $v_message = 'Unknown error has occurred';
                }
            }else{
                $v_status = 3;
                $v_message = '1. Not any rows have deleted';
            }
        }else{
            $v_time_fix = new MongoDate($v_time);
            $arr_log = $cls_log->select_limit($v_row+1,1, array('site_id'=>$v_site_id), array('log_time'=>-1));
            foreach($arr_log as $arr){
                $v_tmp = isset($arr['log_time'])?$arr['log_time']:null;
                if(!is_null($v_tmp)){
                    if($v_time_fix < $v_tmp){
                        $v_time_fix = $v_tmp;
                    }
                }
            }
            $arr_where_clause = array('site_id'=>$v_site_id, 'log_time'=>array('$lt'=>$v_time_fix));
            $v_count = $cls_log->count($arr_where_clause);
            if($v_count>0){
                if($cls_log->delete($arr_where_clause))
                    $v_message = 'Over '.$v_count.' rows have deleted';
                else{
                    $v_status = 5;
                    $v_message = 'Unknown error has occurred';
                }
            }else{
                $v_message = '2. Not any rows have deleted';
            }
        }
    }
}else{
    $v_status = 100;
    $v_message = 'You have no permissions';
}
$arr_return = array('status'=>$v_status, 'success'=>$v_success, 'message'=>$v_message);
$cls_output->output($arr_return);