<?php
if(!isset($v_sval)) die();
$v_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:'0';
settype($v_site_id, 'int');

add_class('cls_tb_design_key');
add_class('cls_tb_site_log');
$cls_site_log = new cls_tb_site_log($db, LOG_DIR);
$cls_key = new cls_tb_design_key($db, LOG_DIR);



$arr_where_clause = array();
if($v_site_id>0) $arr_where_clause['site_id'] = $v_site_id;
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_site_log->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;


$arr_key = array();
$arr_return = array();
$arr_ret_data = array();
$arr_log = $cls_site_log->select_limit($v_skip, $v_page_size, $arr_where_clause, array('log_time'=>-1));
$v_row = $v_skip;
foreach($arr_log as $arr){
    $v_log_action = isset($arr['log_action'])?$arr['log_action']:'';
    $v_log_time = isset($arr['log_time'])?$arr['log_time']:new MongoDate(time());
    $v_log_action = isset($arr['log_action'])?$arr['log_action']:'';
    $v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
    $v_log_ip = isset($arr['log_ip'])?$arr['log_ip']:'';
    $v_log_uri = isset($arr['log_uri'])?$arr['log_uri']:'';
    $arr_log_device = isset($arr['log_device'])?$arr['log_device']:array();

    if(!isset($arr_key[$v_site_id])) $arr_key[$v_site_id] = $cls_key->select_scalar('site_name', array('site_id'=>$v_site_id));

    $arr_ret_data[] = array(
        'row_order'=>++$v_row
        ,'site_name'=>$arr_key[$v_site_id]
        ,'log_time'=>date('d-M-Y H:i:s', $v_log_time->sec)
        ,'log_ip'=>$v_log_ip
        ,'log_action'=>$v_log_action
        ,'log_uri'=>$v_log_uri
        ,'device'=>isset($arr_log_device['platform'])?$arr_log_device['platform']:''
        ,'browser'=>isset($arr_log_device['browser'])?$arr_log_device['browser']:''
    );
}



header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_site_log'=>$arr_ret_data);
echo json_encode($arr_return);