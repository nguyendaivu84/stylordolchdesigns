<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_site_name = isset($_POST['txt_search_site_name'])?$_POST['txt_search_site_name']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
if($v_search_site_name!='') $arr_where_clause['site_name'] = new MongoRegex('/'.$v_search_site_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_design_key_redirect']) && $_SESSION['ss_tb_design_key_redirect']==1){
	if(isset($_SESSION['ss_tb_design_key_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_design_key_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_design_key_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_design_key_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_design_key_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_design_key->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_design_key_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_design_key_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_design_key_page'] = $v_page;
$_SESSION['ss_tb_design_key_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_design_key = $cls_tb_design_key->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_design_key as $arr){
	$v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
	$v_site_key = isset($arr['site_key'])?$arr['site_key']:'';
	$v_site_name = isset($arr['site_name'])?$arr['site_name']:'';
	$v_site_url = isset($arr['site_url'])?$arr['site_url']:'';
	$v_contact_name = isset($arr['contact_name'])?$arr['contact_name']:'';
	$v_contact_phone = isset($arr['contact_phone'])?$arr['contact_phone']:'';
	$v_contact_email = isset($arr['contact_email'])?$arr['contact_email']:'';
	$v_site_ip = isset($arr['site_ip'])?$arr['site_ip']:'';
	$v_site_logo = isset($arr['site_logo'])?$arr['site_logo']:'';
	$v_started_time = isset($arr['started_time'])?$arr['started_time']:(new MongoDate(time()));
	$v_ended_time = isset($arr['ended_time'])?$arr['ended_time']:(new MongoDate(time()));
	$v_site_forever = isset($arr['site_forever'])?$arr['site_forever']:'0';
	$v_site_status = isset($arr['site_status'])?$arr['site_status']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;

    if($v_site_forever==1){
        $v_started_time = '-----';
        $v_ended_time = '-----';
    }else{
        $v_started_time = date('d-M-Y H:i:s', $v_started_time->sec);
        $v_ended_time = date('d-M-Y H:i:s', $v_ended_time->sec);
    }
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'site_id' => $v_site_id,
		'site_key' => $v_site_key,
		'site_name' => $v_site_name,
		'site_url' => $v_site_url,
		'contact_name' => $v_contact_name,
		'contact_phone' => $v_contact_phone,
		'contact_email' => $v_contact_email,
		'site_ip' => $v_site_ip,
		'site_logo' => $v_site_logo,
		'started_time' => $v_started_time,
		'ended_time' => $v_ended_time,
		'site_forever' => $v_site_forever==1,
		'site_status' => $v_site_status==0,
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_key'=>$arr_ret_data);
echo json_encode($arr_return);
?>