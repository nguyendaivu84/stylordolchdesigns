<?php
if(!isset($v_sval)) die();
$v_site_id = isset($_POST['txt_site_id'])?$_POST['txt_site_id']:'0';
$v_site_name = isset($_POST['txt_site_name'])?$_POST['txt_site_name']:'';
settype($v_site_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_site_name!=''){
   //$arr_where_clause = array('site_name'=>new MongoRegex('/'.$v_site_name.'/i'), 'site_id'=>array('$ne'=>$v_site_id));
    $v_site_index = seo_friendly_url($v_site_name);
    $arr_where_clause = array('site_index'=>$v_site_index, 'site_id'=>array('$ne'=>$v_site_id));
    if($cls_tb_design_key->count($arr_where_clause)>0){
        $arr_return['error'] = 2;
        $arr_return['message'] = 'Duplicate Site Name';
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'Lost data';
}

$cls_output->output($arr_return, true, false);