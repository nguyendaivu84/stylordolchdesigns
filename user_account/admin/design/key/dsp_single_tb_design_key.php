<?php if(!isset($v_sval)) die();?>
<style type="text/css">
.k-grid-content{
    height: 368px !important;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_design_key").click(function(e){
		var css = '';
        if(!validator.validate()){
            e.preventDefault();
            return false;
        }
        var text = [];
        var image = [];
        var shape = [];
        $('input[name="chk_feature"]').each(function(index, element){
            var chk = $(this).is(':checked');
            var key = $(this).attr('data-feature');
            var value = $(this).val();
            if(chk){
                switch (key){
                    case 'text':
                        text.push(value);
                        break;
                    case 'image':
                        image.push(value);
                        break;
                    case 'shape':
                        shape.push(value);
                        break;
                }
                //ft.push(text);
                //ft.push(image);
                //ft.push(shape);
                //ft['text'] = text;
                //ft['image'] = image;
                //ft['shape'] = shape;
            }
        });
        var ft = {text:text, image:image, shape:shape};
        $('input#txt_started_time').val(kendo.toString(open_date.value(),'yyyy-MM-dd HH:mm:ss'));
        $('input#txt_ended_time').val(kendo.toString(close_date.value(),'yyyy-MM-dd HH:mm:ss'));
        $('input#txt_site_feature').val(JSON.stringify(ft));
		return true;
	});

    var account_data = <?php echo json_encode($arr_all_user);?>;
    var account_combo = $('#txt_account').width(300).kendoMultiSelect({
        dataTextField:'user_name',
        dataValueField:'user_name',
        dataSource: account_data
    }).data("kendoMultiSelect");
    account_combo.value(<?php echo json_encode($arr_account);?>);
    $('#txt_account_limit').click(function(){
        var is_check = $(this).is(':checked');
        if(is_check){
            account_combo.value([]);
        }
        account_combo.enable(!is_check);
    });

    var concurrent = $("#txt_concurrent").kendoNumericTextBox({
        format: "n0",
        min: 1,
        step: 1
    }).data("kendoNumericTextBox");
    $('#txt_concurrent_limit').click(function(){
        var is_check = $(this).is(':checked');
        concurrent.enable(!is_check);
    });

    <?php
    if($v_account_limit) echo 'account_combo.enable(false);';
    if($v_concurrent_limit) echo 'concurrent.enable(false);';
    ?>
    var key_feature = <?php echo json_encode($arr_key_feature);?>;
    $('input#txt_site_name').focusout(function(e){
        var name = $.trim($(this).val());
        //alert(name);
        if(name==''){
            $(this).val('');
            $('input#txt_hidden_site_name').val('N');
            validator.validate();
            return false;
        }
        var site_id = $('input#txt_site_id').val();
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type:   'POST',
            dataType:'json',
            data:   {txt_session_id: '<?php echo session_id();?>', txt_site_name: name, txt_site_id:site_id, txt_ajax_type: 'check_site_name'},
            beforeSend: function(){
                $('span#sp_site_name').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_site_name').html('');
                //var ret = $.parseJSON(data);
                $('input#txt_hidden_site_name').val(data.error==0?'Y':'');
                validator.validate();
            }
        });
        return true;
    });

    $('input#txt_site_url').focusout(function(e){
        var url = $.trim($(this).val());
        if(url==''){
            $(this).val('');
            $('input#txt_hidden_site_url').val('N');
            validator.validate();
            return false;
        }
        var site_id = $('input#txt_site_id').val();
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type:   'POST',
            dataType: 'json',
            data:   {txt_session_id: '<?php echo session_id();?>', txt_site_url: url, txt_site_id:site_id, txt_ajax_type: 'check_site_url'},
            beforeSend: function(){
                $('span#sp_site_url').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_site_url').html('');
                //var ret = $.parseJSON(data);
                $('input#txt_hidden_site_url').val(data.error==0?'Y':'');
                validator.validate();
            }
        });
        return true;
    });

    var open_date = $('input#txt_started_time').kendoDateTimePicker(
        {
            format:"dd-MMM-yyyy HH:mm:ss",
            change: change_open_date
        }
    ).data("kendoDateTimePicker");
    var close_date = $('input#txt_ended_time').kendoDateTimePicker(
        {
            format:"dd-MMM-yyyy HH:mm:ss",
            change: change_close_date
        }
    ).data("kendoDateTimePicker");
    function change_open_date() {
        var openDate = open_date.value(),
            closeDate = close_date.value();
        if (openDate) {
            openDate = new Date(openDate);
            openDate.setDate(openDate.getDate());
            close_date.min(openDate);
        } else if (closeDate) {
            open_date.max(new Date(closeDate));
        } else {
            closeDate = new Date();
            open_date.max(closeDate);
            close_date.min(closeDate);
        }
    }

    function change_close_date() {
        var closeDate = close_date.value(),
            openDate = open_date.value();

        if (closeDate) {
            closeDate = new Date(closeDate);
            closeDate.setDate(closeDate.getDate());
            open_date.max(closeDate);
        } else if (openDate) {
            close_date.min(new Date(openDate));
        } else {
            closeDate = new Date();
            open_date.max(closeDate);
            close_date.min(closeDate);
        }
    }
    open_date.max(<?php echo $v_site_forever?'close_date.value()':'new Date()';?>);
    close_date.min(open_date.value());

    $('input#txt_site_forever').click(function(e){
        var chk = $(this).is(':checked');
        open_date.enable(!chk);
        close_date.enable(!chk);
    });
    open_date.enable(<?php echo $v_site_forever==0;?>);
    close_date.enable(<?php echo $v_site_forever==0;?>);
    $('input#btn_create_key').click(function(e){
        var $this = $(this);
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type:'POST',
            dataType:'json',
            data: {txt_ajax_type: 'create_key'},
            beforeSend: function(){
                $this.prop('disabled', true);
            },
            success: function(data){
                $this.prop('disabled', false);
                $('input#txt_site_key').val(data.key);
            }
        });
    });
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");

	var tooltip = $("span.tooltips").kendoTooltip({
	    filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	var combo_location = $('select#txt_location_id').width(200).kendoComboBox({
        dataSource:{
            transport:{
                read:{
                    url: '<?php echo URL.$v_admin_key;?>/json',
                    dataType:'json',
                    type:'post',
                    data: {txt_json_type:'load_location', txt_company_id:'<?php echo $v_company_id;?>'}
                }
            }
        },
        dataTextField: "location_name",
        dataValueField: "location_id"
    }).data('kendoComboBox');

    combo_location.value(<?php echo $v_location_id;?>);
    <?php if(!$v_disabled_company_id){?>
    $('select#txt_company_id').change(function(e){
        var company_id = $(this).val();
        company_id = parseInt(company_id, 10);
        $("#txt_company_id").val(company_id);
        if(isNaN(company_id) || company_id <0) company_id = 0;
        var $this = $(this);
        $.ajax({
            url     : '<?php echo URL.$v_admin_key;?>/ajax',
            type    : 'POST',
            data    : {txt_session_id: '<?php echo session_id();?>', txt_company_id: company_id, txt_ajax_type:'load_info'},
            beforeSend: function(){
                $this.prop("disabled", true);
                combo_company.enable(false);
            },
            success: function(data, status){
                var ret = $.parseJSON(data);
                if(ret.error==0){
                    var location_data = ret.location;
                    var account_data = ret.user;
                    combo_location.setDataSource(location_data);
                    combo_location.value(0);
                    account_combo.setDataSource(account_data);
                    account_combo.value([]);
                    $('form#frm_tb_design_key').find('#txt_company_id').val(company_id);
                }
                combo_company.enable(true);
            }
        });
    });
	<?php }else{?>
		combo_company.enable(false);
	<?php }?>

    <?php
    if($v_site_id>0){
    ?>
    $("#txt_max_rows").kendoNumericTextBox({
        format: "n0",
        min: 0,
        step: 1
    });
    var max_time = $('input#txt_max_times').kendoDateTimePicker(
        {
            format:"dd-MMM-yyyy HH:mm:ss"
        }
    ).data("kendoDateTimePicker");
    $('input#btn_delete_log').click(function(e){
        var operator = $('input#txt_and_or[value="0"]').is(':checked')?0:1;
        var time = kendo.toString(max_time.value(), 'yyyy-MM-dd HH:mm:ss');
        var row = $('input#txt_max_rows').val();
        $.ajax({
            url     : '<?php echo URL.$v_admin_key;?>/ajax',
            type    : 'POST',
            dataType: 'json',
            data    : {txt_ajax_type:'delete_log', txt_site_id:'<?php echo $v_site_id;?>', txt_time: time, txt_row:row, txt_operator: operator},
            beforeSend: function(){
                //alert('Operator: '+operator+' --- Row: '+row+' --- Time: '+time);
            },
            success:function(data){
                alert(data.message);
                if(data.status==0){
                    grid.dataSource.page(1);
                    grid.dataSource.read();
                }
            }
        });
    });
    var grid = $("#log_grid").kendoGrid({
        dataSource: {
            pageSize: 20,
            page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: {
                    url: "<?php echo URL.$v_admin_key;?>/json/",
                    type: "POST",
                    data: {txt_session_id:"<?php echo session_id();?>",txt_json_type:'load_log', txt_site_id: '<?php echo $v_site_id;?>'}
                }
            },
            schema: {
                data: "tb_site_log"
                ,total: function(data){
                    return data.total_rows;
                }
            },
            type: "json"
        },
        pageSize: 20,
        height: 430,
        scrollable: true,
        sortable: true,
        //selectable: "single",
        pageable: {
            input: true,
            refresh: true,
            pageSizes: [10, 20, 30, 40, 50],
            numeric: false
        },
        columns: [
            {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
            {field: "log_action", title: "Action", type:"string", width:"80px", sortable: true },
            {field: "log_time", title: "Time", type:"string", width:"100px", sortable: true, template:'<span style="float:right">#= log_time #</span>' },
            {field: "log_ip", title: "IP Address", type:"string", width:"50px", sortable: true, template:'<span style="float:right">#= log_ip #</span>' },
            {field: "device", title: "Device", type:"string", width:"80px", sortable: true},
            {field: "browser", title: "Browser", type:"string", width:"80px", sortable: true},
            {field: "log_uri", title: "URI", type:"string", width:"120px"},
            {field: "site_name", title: "Site", type:"string", width:"50px", sortable: true }
        ]
    }).data("kendoGrid");
    <?php
    }
     ?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>ImageStylor Key<?php echo $v_site_id>0?': '.$v_site_name.' ('.$v_site_index.')':'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_design_key" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_site_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_site_id" name="txt_site_id" value="<?php echo $v_site_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_site_feature" name="txt_site_feature" value="" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Limitation</li>
                        <li>Feature</li>
                        <?php if($v_site_id>0){?>
                        <li>Access Logs</li>
                        <?php }?>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
        <td style="width:200px">Location</td>
        <td style="width:2px">&nbsp;</td>
        <td align="left">
            <select id="txt_location_id" name="txt_location_id">

            </select>
        </td>
    </tr>
<tr align="right" valign="top">
		<td>ImageStylor Key</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" type="text" id="txt_site_key" name="txt_site_key" value="<?php echo $v_site_key;?>" readonly="readonly" /> <input type="button" class="k-button" value="Create Key" id="btn_create_key" /> </td>
	</tr>
<tr align="right" valign="top">
		<td>Site Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" type="text" id="txt_site_name" name="txt_site_name" value="<?php echo $v_site_name;?>" required data-required-msg="Please input Key Name" />
            <input type="hidden" id="txt_hidden_site_name" name="txt_hidden_site_name" value="<?php echo $v_site_name!=''?'Y':'N';?>" required data-required-msg="Site Name is unique!" />
            <span id="sp_site_name"></span>
            <span class="tooltips"><a title="Site's Name is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a> </span>
            <label id="lbl_site_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Website URL</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" type="url" id="txt_site_url" name="txt_site_url" value="<?php echo $v_site_url;?>" required data-required-msg="Please input valid url" data-url-msg="Invalid format URL" />
            <input type="hidden" id="txt_hidden_site_url" name="txt_hidden_site_url" value="<?php echo $v_site_url!=''?'Y':'N';?>" required data-required-msg="The website address is unique" />
            <span id="sp_site_url"></span>
            <span class="tooltips"><a title="Website URL is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a> </span>
            <label id="lbl_site_url" class="k-required">(*)</label></td>
	</tr>
    <tr align="right" valign="top">
        <td>Website Logo</td>
        <td>&nbsp;</td>
        <td align="left"><input class="text_css k-textbox" type="url" id="txt_site_logo" name="txt_site_logo" value="<?php echo $v_site_logo;?>" required data-required-msg="Please input valid url" data-url-msg="Invalid format URL" /> <label id="lbl_site_logo" class="k-required">(*)</label></td>
    </tr>
<tr align="right" valign="top">
		<td>Contact Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_contact_name" name="txt_contact_name" value="<?php echo $v_contact_name;?>" required data-required-msg="Please input Contact Name" /> <label id="lbl_contact_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Contact Phone</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="tel" id="txt_contact_phone" name="txt_contact_phone" value="<?php echo $v_contact_phone;?>" pattern="\d{3}.\d{3}.\d{4}" placeholder="Please input ten digit number such as 432.548.2233" validationMessage="Please input ten digit number such as 432.548.2233" /> </td>
	</tr>
<tr align="right" valign="top">
		<td>Contact Email</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="email" id="txt_contact_email" name="txt_contact_email" value="<?php echo $v_contact_email;?>" required data-required-msg="Please input valid email" validationMessage="Invalid email format" /> <label id="lbl_contact_email" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Site Ip</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_site_ip" name="txt_site_ip" value="<?php echo $v_site_ip;?>" /></td>
	</tr>
<tr align="right" valign="top">
		<td>Site Status</td>
		<td>&nbsp;</td>
		<td align="left"><div class="checkbox"><input type="checkbox" id="txt_site_status" name="txt_site_status"<?php echo $v_site_status==0?' checked="checked"':'';?> /><label for="txt_site_status"> Active?</label> </div></td>
	</tr>
</table>
                    </div>
                        <div class="limitation div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Time life</td>
                                    <td style="width:2px">&nbsp;</td>
                                    <td align="left"><div class="checkbox"><input type="checkbox" id="txt_site_forever" name="txt_site_forever" value="<?php echo $v_site_forever;?>"<?php echo $v_site_forever==1?' checked="checked"':'';?> /><label for="txt_site_forever"> Site Forever?</label></div></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Stated Time</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input type="text" id="txt_started_time" name="txt_started_time" value="<?php echo $v_started_time;?>" /></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Ended Time</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><input type="text" id="txt_ended_time" name="txt_ended_time" value="<?php echo $v_ended_time;?>" /> </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Account</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div class="checkbox"><input type="checkbox" id="txt_account_limit" name="txt_account_limit" value="<?php echo $v_account_limit;?>"<?php echo $v_account_limit?' checked="checked"':'';?> /><label for="txt_account_limit"> Unlimited?</label></div>
                                        <select id="txt_account" name="txt_account[]" multiple="multiple">

                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Concurrent session</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <div class="checkbox"><input type="checkbox" id="txt_concurrent_limit" name="txt_concurrent_limit" value="<?php echo $v_concurrent_limit;?>"<?php echo $v_concurrent_limit?' checked="checked"':'';?> /><label for="txt_concurrent_limit">Unlimited?</label></div>
                                        <input type="number" name="txt_concurrent" id="txt_concurrent" value="<?php echo $v_concurrent;?>" />
                                    </td>
                                </tr>
                            </table>
                        </div>
        <div class="feature div_details">
            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                <?php echo $v_dsp_tr;?>
            </table>
        </div>
<?php if($v_site_id>0 && ($v_delete_right || $v_is_super_admin)){?>
        <div class="other div_details">
            <?php if($v_delete_log){?>
            <div id="log_delete_condition">
                <table width="100%" cellpadding="3" cellspacing="0" align="center">
                    <tr align="right" valign="middle">
                        <td style="width:300px">Delete, when log's rows is more than</td>
                        <td style="text-align: left;"><input type="number" id="txt_max_rows" name="txt_max_rows" value="<?php echo $v_max_rows;?>" /></td>
                    </tr>
                    <tr align="right" valign="middle">
                        <td>&nbsp;</td>
                        <td style="text-align: left;"><input type="radio" id="txt_and" name="txt_and_or" value="0" checked="checked" /><label for="txt_and">And</label>
                        <input type="radio" id="txt_or" name="txt_and_or" value="1" /><label for="txt_or">Or</label></td>
                    </tr>
                    <tr align="right" valign="middle">
                        <td>Time before</td>
                        <td style="text-align: left;"><input type="text" id="txt_max_times" name="txt_max_times" value="<?php echo $v_max_times;?>" /> </td>
                    </tr>
                    <tr align="right" valign="middle">
                        <td>&nbsp;</td>
                        <td style="text-align: left;"><input type="button" id="btn_delete_log" value="Delete Log" class="k-button" /> </td>
                    </tr>
                </table>
            </div>
            <?php }?>
            <div id="log_grid"></div>
        </div>
<?php }?>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_key" name="btn_submit_tb_design_key" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
