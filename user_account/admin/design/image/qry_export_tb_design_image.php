<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_design_image_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_design_image_sort'])){
	$v_sort = $_SESSION['ss_tb_design_image_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_design_image = $cls_tb_design_image->select($arr_where_clause, $arr_sort);
@ob_clean();
$v_sheet_index = 0;
$v_excel_file = 'export_design_image_'.date('Y_m_d_H_i_s').'.xls';
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel.php');
require_once('lib/PHPExcel.1.7.8/Classes/PHPExcel/IOFactory.php');
$v_row_height = 15;
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Anvy")
	->setLastModifiedBy("Anvy")
	->setTitle('Design_image')
	->setSubject("Office 2003 XLS Test Document")
	->setDescription("Test document for Office 2003 XLS, generated using PHP classes.")
	->setKeywords("office 2003 openxml php")
	->setCategory("Report from Anvy");
$objPHPExcel->getDefaultStyle()->getFont()->setName('Tahoma');
$objPHPExcel->getDefaultStyle()->getFont()->setSize(8);
$v_row = 0;
$v_excel_row = 1;
$sheet = $objPHPExcel->setActiveSheetIndex($v_sheet_index);
$v_excel_col = 1;
$sheet->getDefaultRowDimension()->setRowHeight($v_row_height);
$sheet->setTitle('Design_image');
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image File', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Key', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Width', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Height', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Size', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Type', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Dpi', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Status', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Cost', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Extension', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Image Stock Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Saved Dir', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Created Time', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Company Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Location Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Template Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Theme Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Design Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Name', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'User Agent', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Fotolia Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Fotolia Size', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Is Vector', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Is Public', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Id', 'center', true, true, 20, '', true);
create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, 'Svg Original Color', 'center', true, true, 20, '', true);
$v_excel_row++;
foreach($arr_tb_design_image as $arr){
	$v_excel_col = 1;
	$v_image_id = isset($arr['image_id'])?$arr['image_id']:0;
	$v_image_name = isset($arr['image_name'])?$arr['image_name']:'';
	$v_image_file = isset($arr['image_file'])?$arr['image_file']:'';
	$v_image_key = isset($arr['image_key'])?$arr['image_key']:'';
	$v_image_width = isset($arr['image_width'])?$arr['image_width']:0;
	$v_image_height = isset($arr['image_height'])?$arr['image_height']:0;
	$v_image_size = isset($arr['image_size'])?$arr['image_size']:0;
	$v_image_type = isset($arr['image_type'])?$arr['image_type']:0;
	$v_image_dpi = isset($arr['image_dpi'])?$arr['image_dpi']:0;
	$v_image_status = isset($arr['image_status'])?$arr['image_status']:0;
	$v_image_cost = isset($arr['image_cost'])?$arr['image_cost']:0;
	$v_image_extension = isset($arr['image_extension'])?$arr['image_extension']:'0';
	$v_image_stock_id = isset($arr['image_stock_id'])?$arr['image_stock_id']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_fotolia_id = isset($arr['fotolia_id'])?$arr['fotolia_id']:'';
	$v_fotolia_size = isset($arr['fotolia_size'])?$arr['fotolia_size']:'';
	$v_is_vector = isset($arr['is_vector'])?$arr['is_vector']:0;
	$v_is_public = isset($arr['is_public'])?$arr['is_public']:0;
	$v_svg_id = isset($arr['svg_id'])?$arr['svg_id']:0;
	$v_svg_original_color = isset($arr['svg_original_color'])?$arr['svg_original_color']:0;
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, ++$v_row, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_file, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_key, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_width, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_height, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_size, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_type, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_dpi, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_status, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_cost, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_extension, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_image_stock_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_saved_dir, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_created_time, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_company_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_location_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_template_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_theme_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_design_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_name, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_user_agent, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_fotolia_id, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_fotolia_size, 'left');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_is_vector, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_is_public, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_id, 'right');
	create_one_cell_full( $sheet, $v_excel_col++, $v_excel_row, $v_svg_original_color, 'right');
	$v_excel_row++;
}
$sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
$sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
$sheet->getPageSetup()->setHorizontalCentered(true);
$sheet->getPageSetup()->setFitToPage(true);
$sheet->getPageSetup()->setFitToWidth(1);
$sheet->getPageSetup()->setFitToHeight(0);
$sheet->setShowGridlines(false);
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$v_excel_file.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
die();
?>