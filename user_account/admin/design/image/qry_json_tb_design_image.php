<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_search_image_name = isset($_POST['txt_search_image_name'])?$_POST['txt_search_image_name']:'';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
if($v_search_image_name!='') $arr_where_clause['image_name'] = new MongoRegex('/'.$v_search_image_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_design_image_redirect']) && $_SESSION['ss_tb_design_image_redirect']==1){
	if(isset($_SESSION['ss_tb_design_image_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_design_image_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_design_image_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_design_image_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_design_image_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_images->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_design_image_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_design_image_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_design_image_page'] = $v_page;
$_SESSION['ss_tb_design_image_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_design_image = $cls_images->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_design_image as $arr){
	$v_image_id = isset($arr['image_id'])?$arr['image_id']:0;
	$v_image_name = isset($arr['image_name'])?$arr['image_name']:'';
	$v_image_file = isset($arr['image_file'])?$arr['image_file']:'';
	$v_image_key = isset($arr['image_key'])?$arr['image_key']:'';
	$v_image_width = isset($arr['image_width'])?$arr['image_width']:0;
	$v_image_height = isset($arr['image_height'])?$arr['image_height']:0;
	$v_image_size = isset($arr['image_size'])?$arr['image_size']:0;
	$v_image_type = isset($arr['image_type'])?$arr['image_type']:0;
	$v_image_dpi = isset($arr['image_dpi'])?$arr['image_dpi']:0;
	$v_image_status = isset($arr['image_status'])?$arr['image_status']:0;
	$v_image_cost = isset($arr['image_cost'])?$arr['image_cost']:0;
	$v_image_extension = isset($arr['image_extension'])?$arr['image_extension']:'0';
	$v_image_stock_id = isset($arr['image_stock_id'])?$arr['image_stock_id']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
	$v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
	$v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
	$v_fotolia_id = isset($arr['fotolia_id'])?$arr['fotolia_id']:'';
	$v_fotolia_size = isset($arr['fotolia_size'])?$arr['fotolia_size']:'';
	$v_is_vector = isset($arr['is_vector'])?$arr['is_vector']:0;
	$v_is_public = isset($arr['is_public'])?$arr['is_public']:0;
	$v_svg_id = isset($arr['svg_id'])?$arr['svg_id']:0;
	$v_svg_original_color = isset($arr['svg_original_colors'])?$arr['svg_original_colors']:'';
    //$v_image_url = $v_saved_dir.$v_image_file;



    $v_image_dir = ROOT_DIR.DS.$v_saved_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
    if(!file_exists($v_image_dir)){
        $v_image_url = URL.$v_admin_key.'/'.$v_image_id.'/choose';
    }else{
        $v_image_url = URL.$v_saved_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
    }

	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'image_id' => $v_image_id,
		'image_name' => $v_image_name,
		'image_file' => $v_image_file,
		'image_key' => $v_image_key,
		'image_width' => $v_image_width,
		'image_height' => $v_image_height,
		'image_size' => $v_image_size,
		'image_type' => $v_image_type,
		'image_dpi' => $v_image_dpi,
		'image_status' => $v_image_status==0,
		'image_cost' => $v_image_cost,
		'image_extension' => $v_image_extension,
		'is_vector' => $v_is_vector==1,
		'is_public' => $v_is_public==1,
		'image_url' => $v_image_url
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_design_image'=>$arr_ret_data);
echo json_encode($arr_return);
?>