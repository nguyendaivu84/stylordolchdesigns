<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(e){
	$("input#btn_submit_tb_design_image").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            return false;
        }
		return true;
	});
    $('#txt_image_cost').kendoNumericTextBox({
        format: "c2",
        min:0,
        step: 0.01
    });
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_design_image').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Image<?php echo $v_image_id>0?': '.$v_image_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_design_image" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_image_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_image_id" name="txt_image_id" value="<?php echo $v_image_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="right" valign="top">
        <td style="width:200px">Image</td>
        <td style="width:2px">&nbsp;</td>
        <td align="left">
            <img style="width: 200px" src="<?php echo $v_image_url;?>" />
        </td>
    </tr>
<tr align="right" valign="top">
		<td>Image Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" type="text" id="txt_image_name" name="txt_image_name" value="<?php echo $v_image_name;?>" required data-required-msg="Please input Image Name" /> <label id="lbl_image_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Image Size</td>
		<td>&nbsp;</td>
		<td align="left">
            <?php echo $v_image_width .' &times; '.$v_image_height.' px ('.ceil($v_image_size/1024).' KBs)';?>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Image Type</td>
		<td>&nbsp;</td>
		<td align="left">
            <?php echo $v_image_extension.' - DPI: '.$v_image_dpi;?>&nbsp;
        </td>
	</tr>
<tr align="right" valign="top">
        <td>Image Used</td>
        <td>&nbsp;</td>
        <td align="left">
            for template: <?php echo $v_template_used;?>&nbsp; -- &nbsp; for design: <?php echo $v_design_used;?>
        </td>
    </tr>
<tr align="right" valign="top">
		<td>Image Status</td>
		<td>&nbsp;</td>
		<td align="left">
            <div class="checkbox"><input type="checkbox" id="txt_image_status" name="txt_image_status"<?php echo $v_image_status==0?' checked="checked"':'';?> /><label for="txt_image_status">Active</label></div> </td>
	</tr>
<tr align="right" valign="top">
		<td>Image Cost</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_image_cost" name="txt_image_cost" value="<?php echo $v_image_cost;?>" /></td>
	</tr>
<tr align="right" valign="top">
		<td>Assigned to</td>
		<td>&nbsp;</td>
		<td align="left"><?php echo $v_assigned_to;?>&nbsp;</td>
	</tr>
<tr align="right" valign="top">
		<td>Is Vector</td>
		<td>&nbsp;</td>
		<td align="left"><?php echo $v_is_vector==1?"Yes":"No";?></td>
	</tr>
<tr align="right" valign="top">
		<td>Is Public</td>
		<td>&nbsp;</td>
		<td align="left">
            <div class="checkbox"><input type="checkbox" id="txt_is_public" name="txt_is_public"<?php echo $v_is_public==1?' checked="checked"':'';?> /><label>Used for all templates</label></div></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_design_image" name="btn_submit_tb_design_image" value="Save" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
