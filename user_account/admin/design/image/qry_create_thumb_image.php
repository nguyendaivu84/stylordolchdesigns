<?php
if(!isset($v_sval)) die();

$v_image_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_image_id, 'int');

if($v_image_id>0){
    $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
    if($v_row==1){
        $v_image_file = $cls_images->get_image_file();
        $v_saved_dir = $cls_images->get_saved_dir();
        $v_svg_id = $cls_images->get_svg_id();
        if($v_svg_id > 0){
            add_class('cls_tb_design_svg');
            $cls_svg = new cls_tb_design_svg($db, LOG_DIR);
            $v_row = $cls_svg->select_one(array('svg_id'=>$v_svg_id));
            if($v_row==1){
                $v_svg_data = $cls_svg->get_svg_data();
                $v_dir = ROOT_DIR . DS . $cls_svg->get_saved_dir();
                if($cls_svg->create_thumb($v_svg_data, $v_dir, $cls_svg->get_svg_key(), DESIGN_IMAGE_THUMB_SIZE)){
                    $v_image_file = $v_dir . DESIGN_IMAGE_THUMB_SIZE . '_'. $cls_svg->get_svg_key() .'png';
                    $image = new Imagick($v_image_file);
                    $image->setimageformat('jpg');
                    header('Content-type: image/jpeg');
                    echo $image;

                    $image->clear();
                    $image->destroy();
                }else{
                    echo null;
                }
            }else echo null;
        }else{
            $v_thumb_width = DESIGN_IMAGE_THUMB_SIZE;
            $v_dir = $v_saved_dir.$v_image_file;
            if(file_exists($v_dir)){
                $image = new Imagick();
                add_class('cls_draw');
                $cls_draw = new cls_draw();
                $cls_draw->create_thumb($image, ROOT_DIR.DS.$v_dir, $v_thumb_width);
                $v_pos = strrpos($v_image_file, '.');
                if($v_pos>0){
                    $v_ext = strtolower(substr($v_image_file, $v_pos+1));
                    if(strlen($v_ext==3))
                        $image->setimageformat($v_ext);
                }
                $image->writeimage(ROOT_DIR.DS.$v_saved_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file);

                header('Content-type: image/jpeg');
                echo $image;

                $image->clear();
                $image->destroy();
            }else echo null;
        }
    }else echo null;
}else echo null;