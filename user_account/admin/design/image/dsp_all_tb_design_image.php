<?php if(!isset($v_sval)) die();?>
<?php if($v_disabled_company_id){?>
    <script type="text/javascript">
        $(document).ready(function(e){
            var combo_company = $('select#txt_company_id').data("kendoComboBox");
            combo_company.enable(false);
        });
    </script>
<?php }?>
<script type="text/javascript">
    $(document).ready(function(e){
        var combo_company = $('select#txt_company_id').data("kendoComboBox");
        function on_success(e){
            if(e.response.upload==1 && e.response.insert==1){
                var data = grid.dataSource.data();
                var tmp = [];
                tmp.push(e.response.image)
                for(var i=0; i<data.length; i++){
                    data[i].row_order = i+2;
                    tmp.push(data[i]);
                }
                grid.dataSource.data(tmp);
                grid.refresh();
            }
        }
        function on_select(e){
            if(e.files.length>2){
                e.preventDefault();
                alert('Please choose only max-two files!');
                return false;
            }
            return true;
        }

        $('input#txt_files').kendoUpload({
            async: {
                saveUrl:'<?php echo URL.$v_admin_key;?>/'+combo_company.value()+'/upload/',
                autoUpload:true
            }
            ,success: on_success
            ,select: on_select
        });
        $('.k-upload-button').css('width', '200px');
        $('.k-upload-button span').html('Select images...');
        var grid = $("#grid").kendoGrid({
            dataSource: {
                pageSize: 20,
                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                serverPaging: true,
                serverSorting: true,
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key;?>/json/",
                        type: "POST",
                        data: {txt_session_id:"<?php echo session_id();?>",txt_quick_search:'<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>', txt_search_company_id: '<?php echo $v_company_id;?>',txt_search_image_name:'<?php echo isset($v_search_image_name)?htmlspecialchars($v_search_image_name):'';?>'}
                    }
                },
                schema: {
                    data: "tb_design_image"
                    ,total: function(data){
                        return data.total_rows;
                    }
                },
                type: "json"
            },
            pageSize: 20,
            height: 430,
            scrollable: true,
            sortable: true,
            //selectable: "single",
            pageable: {
                input: true,
                refresh: true,
                pageSizes: [10, 20, 30, 40, 50],
                numeric: false
            },
            columns: [
                {field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
                {field: "image_name", title: "Image Name", type:"string", width:"100px", sortable: true, template:'<p style="margin:0;text-align: center"><img style="width:150px;margin:5px" src="#= image_url#" /><br />#= image_name#</p>' },
                {field: "image_width", title: "Width", width:"50px", sortable: true, template: '<span style="float:right">#= image_width # px</span>'},
                {field: "image_height", title: "Height", width:"50px", sortable: true, template: '<span style="float:right">#= image_height # px</span>'},
                {field: "image_size", title: "Size", type:"int", width:"50px", sortable: true, template: '<span style="float:right">#= image_size # bytes</span>'},
                {field: "image_extension", title: "Type", type:"int", width:"20px", sortable: true},
                {field: "image_cost", title: "Cost", type:"numeric", width:"30px", sortable: true, template: '<span style="float:right">#= kendo.toString(image_cost,"c2") #</span>'},
                {field: "is_vector", title: "Is Vector", type:"boolean", width:"50px", sortable: true, template: '<span style="float:right">#= is_vector?"Yes":"No" #</span>'},
                {field: "image_status", title: "Status", type:"boolean", width:"50px", sortable: true, template: '<span style="float:right">#= image_status?"Active":"Inactive" #</span>'},
                { command:  [
                    { name: "View", text:'', click: view_row, imageClass: 'k-grid-View' }
                    <?php if($v_edit_right || $v_is_super_admin){?>
                    ,{ name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
                    <?php }?>
                    <?php if($v_delete_right || $v_is_super_admin){?>
                    ,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
                    <?php }?>
                ],
                    title: " ", width: "70px" }
            ]
        }).data("kendoGrid");
        function view_row(e) {
            e.preventDefault();
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.image_id+"/view";
        }
        function edit_row(e) {
            e.preventDefault();
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            if(confirm('Do you want to edit info for image with name: "'+dataItem.image_name+'"?')){
                document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.image_id+"/edit";
            }
        }
        function delete_row(e) {
            e.preventDefault();
            var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
            if(confirm('Do you want to delete image with name: "'+dataItem.image_name+'"?')){
                document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.image_id+"/delete";
            }
        }

    });
</script>

    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Images</h3>
                    </div>
                    <div id="div_quick">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">

                    <span class="k-textbox k-space-left" id="txt_quick_search">
                    <input type="text" name="txt_quick_search" placeholder="Search by Image Name" value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" />
                    <a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a>
                    <script type="text/javascript">
                        $(document).ready(function(e){
                            $('a#a_quick_search').click(function(e){
                                $('form#frm_quick_search').submit();
                            })
                        });
                    </script>
                    </span>

                    <input type="hidden" name="txt_company_id" id="txt_company_id" value="<?php echo $v_company_id;?>" />
                    </form>
                    </div>
                    <div id="div_select">
                    <form id="frm_company_id" method="post">
                    Company: <select id="txt_company_id" name="txt_company_id" onchange="this.form.submit();">
                    <option value="0" selected="selected">-------</option>
                    <?php
					echo $v_dsp_company_option;
					?>
                    </select>
                    <input type="hidden" name="txt_quick_search" id="txt_quick_search" value="<?php echo htmlspecialchars($v_quick_search);?>" />
                    </form>
                    </div>
                    </div>
                    <?php if($v_create_right || $v_edit_right || $v_is_super_admin ){?>
                    <div style="width:45%">
                        <div class="upload-section">
                            <input name="txt_files" id="txt_files" type="file" accept="image/gif, image/png, image/jpg" />
                        </div>
                    </div>
                    <?php }?>
                    <div id="grid"></div>
                    <div id="advanced_search_window" style="display:none">
                    <h2>Advanced Search for Design_image</h2>
                    <form id="frm_advanced_search" method="post" action="<?php echo URL.$v_admin_key;?>">
                    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                    <tr align="left" valign="middle">
                    <td align="right">Company:</td>
                    <td>
                    <select id="txt_search_company_id" name="txt_search_company_id">
                    <option value="0" selected="selected">--------</option>
                    <?php echo $v_dsp_company_option;?>
                    </select>
                    </td>
                    </tr>
                    <tr align="center" valign="middle">
                    <td colspan="2">
                    <input type="submit" class="k-button k-button button_css" value="Search" name="btn_advanced_search" />
                    <input type="submit" class="k-button k-button button_css" value="Reset" name="btn_advanced_reset" />
                    </td>
                    </tr>
                    </table>
                    </form>
                    </div>
				<script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        window_search = $('div#advanced_search_window');
                        $('li#icons_advanced_search').bind("click", function() {
                            if (!window_search.data("kendoWindow")) {
                                window_search.kendoWindow({
                                    width: "600px",
                                    actions: ["Maximize", "Close"],
                                    modal: true,
                                    title: "Advanced Search for Design_image"
                                });
                            }
                            window_search.data("kendoWindow").center().open();
                        });
                    });
                </script>
                </div>
            </div>
        </div>
  </div>