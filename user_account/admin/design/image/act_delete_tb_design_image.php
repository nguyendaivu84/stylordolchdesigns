<?php if(!isset($v_sval)) die();?>
<?php
$v_image_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_image_id, 'int');
if($v_image_id>0){
    $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
    if($v_row==1){
        $v_saved_dir = $cls_images->get_saved_dir();
        $v_image_file = $cls_images->get_image_file();
        $v_thumb_file = ROOT_DIR.DS.$v_saved_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
        $v_original_file = ROOT_DIR.DS.$v_saved_dir.IMAGE_ORIGINAL_PREFIX.'_'.$v_image_file;

        $v_image_file = ROOT_DIR.DS.$v_saved_dir.$v_image_file;
        if(file_exists($v_image_file)) @unlink($v_image_file);
        if(file_exists($v_thumb_file)) @unlink($v_thumb_file);
        if(file_exists($v_original_file)) @unlink($v_original_file);

        $v_image_file = $cls_images->get_image_original_file();
        if($v_image_file!=''){
            $v_image_file = ROOT_DIR.DS.$v_saved_dir.$v_image_file;
            if(file_exists($v_image_file)) @unlink($v_image_file);
        }
	    $cls_images->delete(array('image_id' => $v_image_id));
    }
}
$_SESSION['ss_tb_design_image_redirect'] = 1;
redir(URL.$v_admin_key);
?>