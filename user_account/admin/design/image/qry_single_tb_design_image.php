<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_image_id = 0;
$v_image_name = '';
$v_image_file = '';
$v_image_key = '';
$v_image_width = 0;
$v_image_height = 0;
$v_image_size = 0;
$v_image_type = 0;
$v_image_dpi = 0;
$v_image_status = 0;
$v_image_cost = 0;
$v_image_extension = '0';
$v_image_stock_id = 0;
$v_saved_dir = '';
$v_created_time = date('Y-m-d H:i:s', time());
$v_company_id = 0;
$v_location_id = 0;
$v_template_id = 0;
$v_theme_id = 0;
$v_design_id = 0;
$v_user_id = 0;
$v_user_name = '';
$v_user_agent = '';
$v_fotolia_id = '';
$v_fotolia_size = '';
$v_is_vector = 0;
$v_is_public = 0;
$v_svg_id = 0;
$v_image_url = '';
$arr_image_used = array();
$v_svg_original_color = 0;
$v_new_design_image = true;
if(isset($_POST['btn_submit_tb_design_image'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_images->set_mongo_id($v_mongo_id);
	$v_image_id = isset($_POST['txt_image_id'])?$_POST['txt_image_id']:$v_image_id;
	$v_image_id = (int) $v_image_id;
	$v_image_name = isset($_POST['txt_image_name'])?$_POST['txt_image_name']:$v_image_name;
	$v_image_name = trim($v_image_name);
    $v_image_key = seo_friendly_url($v_image_name);
	if($v_image_name=='') $v_error_message .= '[Image Name] is empty!<br />';
	$v_image_status = isset($_POST['txt_image_status'])?0:1;
	$cls_images->set_image_status($v_image_status);
	$v_image_cost = isset($_POST['txt_image_cost'])?$_POST['txt_image_cost']:$v_image_cost;
	$v_image_cost = (float) $v_image_cost;
	if($v_image_cost<0) $v_error_message .= '[Image Cost] is negative!<br />';
	$cls_images->set_image_cost($v_image_cost);
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
	$v_company_id = (int) $v_company_id;
	if($v_company_id<0) $v_company_id = 0;
	$v_is_public = isset($_POST['txt_is_public'])?1:0;
	$cls_images->set_is_public($v_is_public);
	$v_svg_id = isset($_POST['txt_svg_id'])?$_POST['txt_svg_id']:$v_svg_id;
	if($v_error_message==''){
		if(!is_null($v_mongo_id) && $v_image_id>0){
            $arr_fields = array('image_name', 'image_key', 'image_cost', 'image_status', 'is_public', 'company_id');
            $arr_values = array($v_image_name, $v_image_key, $v_image_cost, $v_image_status, $v_is_public, $v_company_id);
			$v_result = $cls_images->update_fields($arr_fields, $arr_values, array('_id' => $v_mongo_id));
            if($v_result){
                $_SESSION['ss_tb_design_image_redirect'] = 1;
                redir(URL.$v_admin_key);
            }
        }
	}
}else{
	$v_image_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_image_id,'int');
	if($v_image_id>0){
		$v_row = $cls_images->select_one(array('image_id' => $v_image_id));
		if($v_row == 1){
			$v_mongo_id = $cls_images->get_mongo_id();
			$v_image_id = $cls_images->get_image_id();
			$v_image_name = $cls_images->get_image_name();
			$v_image_file = $cls_images->get_image_file();
			$v_image_key = $cls_images->get_image_key();
			$v_image_width = $cls_images->get_image_width();
			$v_image_height = $cls_images->get_image_height();
			$v_image_size = $cls_images->get_image_size();
			$v_image_type = $cls_images->get_image_type();
			$v_image_dpi = $cls_images->get_image_dpi();
			$v_image_status = $cls_images->get_image_status();
			$v_image_cost = $cls_images->get_image_cost();
			$v_image_extension = $cls_images->get_image_extension();
			$v_image_stock_id = $cls_images->get_image_stock_id();
			$v_saved_dir = $cls_images->get_saved_dir();
			$v_created_time = date('Y-m-d H:i:s',$cls_images->get_created_time());
			$v_company_id = $cls_images->get_company_id();
			$v_location_id = $cls_images->get_location_id();
			$v_template_id = $cls_images->get_template_id();
			$v_theme_id = $cls_images->get_theme_id();
			$v_design_id = $cls_images->get_design_id();
			$v_user_id = $cls_images->get_user_id();
			$v_user_name = $cls_images->get_user_name();
			$v_user_agent = $cls_images->get_user_agent();
			$v_fotolia_id = $cls_images->get_fotolia_id();
			$v_fotolia_size = $cls_images->get_fotolia_size();
			$v_is_vector = $cls_images->get_is_vector();
			$v_is_public = $cls_images->get_is_public();
			$v_svg_id = $cls_images->get_svg_id();
            $arr_image_used = $cls_images->get_image_used();
			$v_svg_original_color = $cls_images->get_svg_original_colors();

            $v_image_thumb = $v_saved_dir . DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
            if(file_exists($v_image_thumb))
                $v_image_url = URL . $v_image_thumb;
            else
                $v_image_url = URL . $v_admin_key . '/' . $v_image_id .'/choose';
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id);

$v_assigned_to = '';
if($v_user_id>0){
    $v_contact_id = $cls_tb_user->select_scalar('contact_id', array('user_id'=>$v_user_id));
    settype($v_contact_id, 'int');
    if($v_contact_id>0){
        $v_assigned_to = $cls_tb_contact->get_full_name_contact($v_contact_id);
    }
    if($v_assigned_to=='') $v_assigned_to = isset($arr_user['user_name'])?$arr_user['user_name']:'';
}

$v_template_used = isset($arr_image_used['template'])?$arr_image_used['template']:0;
$v_design_used = isset($arr_image_used['design'])?$arr_image_used['design']:0;
?>