<?php
if(!isset($v_sval)) die();
?>
<?php
$v_company_id = isset($_GET['id'])?$_GET['id']:'0';
settype($v_company_id, 'int');
add_class('cls_upload');
add_class('Browser');
add_class('cls_validation');
$v_upload_dir = DESIGN_TEMPLATE_DIR;

$arr_return = array('message'=>'OK', 'upload'=>0, 'insert'=>0, 'image'=>null);

$browser = new Browser();
$v_user_agent = $browser->getUserAgent();
$v_user_ip = get_real_ip_address();

$validate = new cls_validation();
$validate->set_file_extension(array('png', 'jpg', 'gif'));
$validate->set_max_file_size(DESIGN_MAX_SIZE_UPLOAD);
$validate->set_file_name_length(50);

$up = cls_upload::factory($v_upload_dir);
$up->set_max_file_size(DESIGN_MAX_SIZE_UPLOAD);

if(!empty($_FILES['txt_files'])){
    $up->file($_FILES['txt_files']);
    //$up->callbacks($validate, array('check_file_extension', 'check_max_file_size', 'check_file_name_length'));
    $arr_result = $up->upload();

    $v_root_dir = ROOT_DIR;
    $arr_ext = array('', 'gif', 'jpg', 'png');

    if($arr_result['status']){
        $v_current_full_path = $up->get_result_path();
        $v_image_file = $up->get_current_name();
        $v_image_original_file = $v_image_file;
        list($width, $height, $type) = getimagesize($v_current_full_path);
        if($type>3) $type = 0;
        if($type>0){
            $v_extension = '.'.$arr_ext[$type];
            $v_extension_len = strlen($v_extension);
            $v_full_path_len = strlen($v_current_full_path)-$v_extension_len;

            if(strrpos($v_current_full_path, $v_extension)!==$v_full_path_len){
                $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).$v_extension;
                $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).$v_extension;
                $v_count = 0;
                $v_is_exist = file_exists($v_new_full_path);
                while($v_is_exist){
                    $v_count++;
                    $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).'('.$v_count.')'.$v_extension;
                    $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).'('.$v_count.')'.$v_extension;
                    $v_is_exist = file_exists($v_new_full_path);
                }
                $v_image_original_file = $v_new_image_file;
                @rename($v_current_full_path, $v_new_full_path);
                $v_current_full_path = $v_new_full_path;
                $v_image_file = $v_new_image_file;
            }

            $v_image_original_file = $cls_images->backup_original_image($v_current_full_path, $v_image_file, $arr_ext[$type], IMAGE_MAX_SIZE_USE, array('w'=>IMAGE_MAX_WIDTH_USE, 'h'=>IMAGE_MAX_HEIGHT_USE), IMAGE_ORIGINAL_PREFIX, DS);

            $v_dir = str_replace($v_root_dir.DS,'', $v_current_full_path);

            $v_saved_dir = str_replace('\\', '/', $v_dir);
            //$v_saved_dir = substr($v_saved_dir, 1);
            $v_saved_dir = str_replace($v_image_file,'', $v_saved_dir);

            $v_url = URL. str_replace('\\', '/', $v_dir);

            $v_size = (int) (isset($arr_result['size_in_bytes'])?$arr_result['size_in_bytes']:'0');

            $v_dpi = ceil($width*$height / pow(96,2));


            $v_image_name = str_replace('.'.$arr_ext[$type],'',$v_image_file);

            do{
                $p = strpos($v_image_name,'  ');
                if($p!==false){
                    $v_image_name = str_replace('  ',' ', $v_image_name);
                }
            }while($p!==false);
            //$v_image_id = $cls_images->select_next('image_id');
            $v_image_key = $v_image_name;
            $v_image_key = seo_friendly_url($v_image_key);
            $v_image_key = str_replace('-','_', $v_image_key);
            $v_image_key = str_replace('img','pic', $v_image_key);

            $i = 0;
            $v_tmp_image_key = $v_image_key;
            do{
                $arr_tmp_where = array('image_key'=>$v_tmp_image_key/*, 'image_id'=>array('$ne'=>$v_image_id)*/);
                $v_tmp_row = $cls_images->select_one($arr_tmp_where);
                if($v_tmp_row>0){
                    $i++;
                    $v_tmp_image_key = $v_image_key.'_'.$i;
                }
            }while($v_tmp_row>0);
            $v_image_key = $v_tmp_image_key;

            //$cls_images->set_image_id($v_image_id);
            $cls_images->set_image_name($v_image_name);
            $cls_images->set_image_file($v_image_file);
            $cls_images->set_image_original_file($v_image_original_file);
            $cls_images->set_image_key($v_image_key);
            $cls_images->set_image_desc('');
            $cls_images->set_image_type($type);
            $cls_images->set_image_dpi($v_dpi);
            $cls_images->set_image_extension($arr_ext[$type]);
            $cls_images->set_image_height($height);
            $cls_images->set_image_width($width);
            $cls_images->set_image_cost(0);
            $cls_images->set_image_size($arr_result['size_in_bytes']);
            $cls_images->set_image_status(0);
            $cls_images->set_is_public(1);
            $cls_images->set_image_stock_id(0);
            $cls_images->set_user_id(isset($arr_user['user_id'])?$arr_user['user_id']:0);
            $cls_images->set_user_name(isset($arr_user['user_name'])?$arr_user['user_name']:'');
            $cls_images->set_design_id(0);
            $cls_images->set_fotolia_id('');
            $cls_images->set_fotolia_size('');
            $cls_images->set_template_id(0);
            $cls_images->set_theme_id(0);
            $cls_images->set_company_id($v_company_id);
            $cls_images->set_location_id(0);
            $cls_images->set_saved_dir($v_saved_dir);
            $cls_images->set_created_time(date('Y-m-d H:i:s'));
            $cls_images->set_is_admin($v_disabled_company_id?0:1);
            $cls_images->set_user_ip($v_user_ip);
            $cls_images->set_user_agent($v_user_agent);
            $cls_images->set_image_order(0);
            $v_image_id = $cls_images->insert();
            $v_result = $v_image_id >0;

            if($v_result>0){
                $arr_image = array(
                    'image_id'=>$v_image_id
                    ,'image_name'=>$v_image_name
                    ,'image_width'=>$width
                    ,'image_height'=>$height
                    ,'image_size'=>$arr_result['size_in_bytes']
                    ,'image_extension'=>$arr_ext[$type]
                    ,'image_dpi'=>$v_dpi
                    ,'image_url'=>URL.$v_admin_key.'/'.$v_image_id.'/choose'
                    ,'image_cost'=>0
                    ,'image_status'=>true
                    ,'is_public'=>true
                    ,'is_vector'=>false
                    ,'row_order'=>1
                );
                $arr_return['insert']=1;
                $arr_return['image']=$arr_image;
                add_class('cls_draw');
                $cls_draw = new cls_draw();
                $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
            }
        }else{
            $arr_return['insert']=0;
        }
    }
    $arr_return['upload'] = $arr_result['status']?1:0;
}
$arr_return['message'] = $up->get_errors();
ob_end_clean();
ob_start();
header("Content-type: application/json");
echo json_encode($arr_return);