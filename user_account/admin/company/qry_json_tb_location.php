<?php if(!isset($v_sval)) die();?>
<?php
// keep search for feature function( i guess)
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
$_SESSION['ss_last_company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
settype($v_page, 'int');
settype($v_page_size, 'int');
add_class("cls_tb_location");
$cls_tb_location = new cls_tb_location($db);
$v_total_rows = $cls_tb_location->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("location_name"=>1);
$arr_tb_location = $cls_tb_location->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
add_class("cls_tb_contact");
$cls_tb_contact = new cls_tb_contact($db);

add_class("cls_tb_region");
$cls_tb_region = new cls_tb_region($db);

foreach($arr_tb_location as $arr){
    $v_location_name= isset($arr['location_name'])?$arr['location_name']:'';
    $v_location_number = isset($arr['location_number'])?$arr['location_number']:'';
    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    $v_location_region = isset($arr['location_region'])?$arr['location_region']:'';
    $v_location_main_contact = isset($arr['main_contact'])?$arr['main_contact']:0;
    $v_location_status = isset($arr['status'])?$arr['status']:0;
    $v_location_order_approver = isset($arr['approved_contact'])?$arr['approved_contact']:0;

    $v_location_status = $cls_settings->get_option_name_by_id("location_status",$v_location_status);

    $v_location_region = $cls_tb_region->select_scalar("region_contact",array("region_id"=>$v_location_region));//
    $v_location_region = $cls_tb_contact->get_full_name_contact((int)$v_location_region);;
    $v_location_order_approver = $cls_tb_contact->get_full_name_contact((int)$v_location_order_approver);
    $v_location_main_contact = $cls_tb_contact->get_full_name_contact((int)$v_location_main_contact);

    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'location_id' => $v_location_id,
        'location_name' => $v_location_name,
        'location_number' => $v_location_number,
        'location_region' => $v_location_region,
        'location_main_contact' => $v_location_main_contact,
        'location_status' => $v_location_status,
        'location_approver' => $v_location_order_approver
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_location'=>$arr_ret_data);
echo json_encode($arr_return);
?>