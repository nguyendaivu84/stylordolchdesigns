<?php if(!isset($v_sval)) die();?>
<?php
$arr_user_contact_module_rule = array();
$v_this_module_contact_menu = "manage_location";
$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;
$v_row = $cls_tb_user->select_one(array('user_id'=>$v_user_id));
if($v_row == 1){
    $v_contact_id = $cls_tb_user->get_contact_id();
    $v_user_name = $cls_tb_user->get_user_name();
    $v_user_full_name = $cls_tb_contact->get_full_name_contact($v_contact_id);
    $arr_user_contact_module_rule = $cls_tb_user->get_all_permission($db);
}
$v_view_location_right = isset($arr_user_contact_module_rule[$v_this_module_contact_menu]['view']);
//die("$v_view_contact_right");
$v_edit_location_right = isset($arr_user_contact_module_rule[$v_this_module_contact_menu]['edit']);
$v_create_location_right = isset($arr_user_contact_module_rule[$v_this_module_contact_menu]['create']);
$v_delete_location_right = isset($arr_user_contact_module_rule[$v_this_module_contact_menu]['delete']);

$v_has_location = 0;
$v_title = '';
$v_content = '';
$v_error_message = '';
$v_mongo_id = NULL;
$v_company_id = 0;
$v_company_name = '';
$v_company_code = '';
$v_sales_rep_email = '';
$v_relationship = 0;
$v_default_kitting_cost = 10;
$v_logo_file = '';
$v_modules = '';
$v_csr_id = 0;
$v_sales_rep_id = 0;
$v_bussines_type = 0;
$v_industry = 0;
$v_website = '';
$v_status = '0';
$v_new_company = true;
$v_company_module = '';
add_class("cls_tb_rule");
$cls_tb_rule = new cls_tb_rule($db);
add_class("cls_settings");
$cls_tb_setting = new cls_settings($db);
if(isset($_POST['btn_submit_tb_company'])){
    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
    //=== xu ly company module
    $v_rule_list = (isset($_REQUEST['check'])) ?$_REQUEST['check'] :'';
    if($v_rule_list!=''){
        $v_total = sizeof($v_rule_list);
        $v_tmp_user_rule = '';
        if($v_total>0)
        {
            for($i=0;$i<$v_total;$i++)
            {
                $v_rule_key = $v_rule_list[$i];
                if($v_rule_key!='')
                    $v_tmp_user_rule .= $v_rule_key .',';
            }
            $v_company_module = $v_tmp_user_rule;
        }
    }
    //
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_company->set_mongo_id($v_mongo_id);
	if(is_null($v_mongo_id)){
		$v_company_id = $cls_tb_company->select_next('company_id');
	}
	$v_company_id = (int) $v_company_id;
	$cls_tb_company->set_company_id($v_company_id);
	$v_company_name = isset($_POST['txt_company_name'])?$_POST['txt_company_name']:$v_company_name;
	$v_company_name = trim($v_company_name);
	if($v_company_name=='') $v_error_message .= '[Company Name] is empty!<br />';
	$cls_tb_company->set_company_name($v_company_name);
	$v_company_code = isset($_POST['txt_company_code'])?$_POST['txt_company_code']:$v_company_code;
	$v_company_code = trim($v_company_code);
    $v_company_code = seo_friendly_url($v_company_code);
	if($v_company_code=='')
        $v_error_message .= '[Company Code] is empty!<br />';
    else{
        if($cls_tb_company->count(array('$where'=>"this.company_code.toLowerCase()=='".strtolower($v_company_code)."'", 'company_id'=>array('$ne'=>$v_company_id)))>0) $v_error_message .= '+ Duplicate Company Code.<br />';
    }
	$cls_tb_company->set_company_code($v_company_code);
	$v_sales_rep_email = isset($_POST['txt_sales_rep_email'])?$_POST['txt_sales_rep_email']:$v_sales_rep_email;
    $v_sales_rep_email = trim($v_sales_rep_email);
	if($v_sales_rep_email!='' && !is_valid_email($v_sales_rep_email)) $v_error_message .= '+ Sales Rep Email] is invalid.<br />';
	$cls_tb_company->set_sales_rep_email($v_sales_rep_email);
	$v_relationship = isset($_POST['txt_relationship'])?$_POST['txt_relationship']:$v_relationship;
	$v_relationship = (int) $v_relationship;
	$cls_tb_company->set_relationship($v_relationship);
	$cls_tb_company->set_modules($v_modules);
	$v_csr_id = isset($_POST['txt_csr_id'])?$_POST['txt_csr_id']:$v_csr_id;
	$v_csr_id = (int) $v_csr_id;
	$cls_tb_company->set_csr($v_csr_id);
	$v_sales_rep_id = isset($_POST['txt_sales_rep_id'])?$_POST['txt_sales_rep_id']:$v_sales_rep_id;
	$v_sales_rep_id = (int) $v_sales_rep_id;
	$cls_tb_company->set_sales_rep($v_sales_rep_id);
	$v_bussines_type = isset($_POST['txt_business_type'])?$_POST['txt_business_type']:$v_bussines_type;
	$v_bussines_type = (int) $v_bussines_type;
	$cls_tb_company->set_bussines_type($v_bussines_type);
	$v_industry = isset($_POST['txt_industry'])?$_POST['txt_industry']:$v_industry;
	$v_industry = (int) $v_industry;
	$cls_tb_company->set_industry($v_industry);
	$v_website = isset($_POST['txt_website'])?$_POST['txt_website']:$v_website;
	$v_website = trim($v_website);
	if($v_website!='' && !is_valid_url($v_website)) $v_error_message .= '[Website] is Invalid!<br />';
	$cls_tb_company->set_website($v_website);
	$v_status = isset($_POST['txt_status'])?$_POST['txt_status']:1;
	$v_status = (int) $v_status;
	$cls_tb_company->set_status($v_status);
    $v_default_kitting_cost = isset($_POST['txt_default_kitting_cost']) ? $_POST['txt_default_kitting_cost'] : 0;
    $cls_tb_company->set_default_kitting_cost($v_default_kitting_cost);
	$cls_tb_company->set_modules($v_company_module);
    if($v_error_message==''){
        $v_dir = ROOT_DIR.DS.'resources'.DS.$v_company_code;
        $v_allow = file_exists($v_dir) || @mkdir($v_dir);
        if($v_allow){

            add_class('clsupload');
            $cls_upload = new clsupload();
            $cls_upload->set_destination_dir($v_dir);
            $cls_upload->set_allow_array_extension(array('png', 'gif', 'jpg'));
            $cls_upload->set_field_name('txt_logo_file');
            $cls_upload->set_allow_overwrite();
            $cls_upload->upload_process();
            if($cls_upload->get_error_number()==0){
                $v_file_name = $cls_upload->get_filename();
                $v_pos = strrpos($v_file_name, '.');
                if($v_pos>0){
                    $v_ext = substr($v_file_name, $v_pos);
                    if(file_exists($v_dir.DS.'logo'.$v_ext)) @unlink($v_dir.DS.'logo'.$v_ext);
                    rename($v_dir.DS.$v_file_name, $v_dir.DS.'logo'.$v_ext);

                    $v_logo_file = 'logo'.$v_ext;
                }
            }
            $v_product_dir = $v_dir.DS.'products';
            if(!file_exists($v_product_dir)) @mkdir($v_product_dir);
            $v_signage_layout_dir = $v_dir.DS.'signage_layout';
            if(!file_exists($v_signage_layout_dir)) @mkdir($v_signage_layout_dir);
            $v_slide_dir = $v_dir.DS.'slider';
            if(!file_exists($v_slide_dir)) @mkdir($v_slide_dir);
        }else{
            $v_error_message .= '+ Cannot create resource for Company in: "'.$v_dir.'".<br />';
        }
    }
    if($v_logo_file=='') $v_logo_file = isset($_POST['txt_hidden_logo_file'])?$_POST['txt_hidden_logo_file']:'';
    $cls_tb_company->set_logo_file($v_logo_file);

	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_company->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_company->update(array('_id' => $v_mongo_id));
			$v_new_company = false;
		}
		if($v_result){
			$_SESSION['ss_tb_company_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_company) $v_company_id = 0;
		}
	}
}else{
	$v_company_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_company_id,'int');
	if($v_company_id>0){
		$v_row = $cls_tb_company->select_one(array('company_id' => $v_company_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_company->get_mongo_id();
			$v_company_id = $cls_tb_company->get_company_id();
			$v_company_name = $cls_tb_company->get_company_name();
			$v_company_code = $cls_tb_company->get_company_code();
			$v_sales_rep_email = $cls_tb_company->get_sales_rep_email();
			$v_relationship = $cls_tb_company->get_relationship();
			$v_logo_file = $cls_tb_company->get_logo_file();
			$v_modules = $cls_tb_company->get_modules();
			$v_csr_id = $cls_tb_company->get_csr();
			$v_sales_rep_id = $cls_tb_company->get_sales_rep();
			$v_bussines_type = $cls_tb_company->get_bussines_type();
			$v_industry = $cls_tb_company->get_industry();
			$v_website = $cls_tb_company->get_website();
			$v_status = $cls_tb_company->get_status();
            $v_default_kitting_cost = $cls_tb_company->get_default_kitting_cost();
		}
	}
}

$v_dir_file = ROOT_DIR.DS.'resources'.DS.$v_company_code.DS.$v_logo_file;
$v_logo_url = '';
if(file_exists($v_dir_file) && $v_logo_file!=''){
    $v_logo_url = URL.'resources/'.$v_company_code.'/'.$v_logo_file;
    $v_logo_url = '<img title="'.$v_company_name.'" src="'.$v_logo_url.'" style="border:none" />';
}

$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company);

$arr_all_contact = array();
$arr_all_contact[] = array(
    'contact_id'=>0,
    'contact_name'=>'--------',
    'contact_info'=>''
);

$arr_contact = $cls_tb_contact->select(array('location_id'=>10000));
$v_tmp_csr_id =0;
$v_tmp_sales_rep_id = 0;
foreach($arr_contact as $arr){
    $v_contact_id = isset($arr['contact_id'])?$arr['contact_id']:0;
    $v_first_name = isset($arr['first_name'])?$arr['first_name']:'';
    $v_last_name = isset($arr['last_name'])?$arr['last_name']:'';
    $v_address_unit = isset($arr['address_unit'])?$arr['address_unit']:'';
    $v_address_line_1 = isset($arr['address_line_1'])?$arr['address_line_1']:'';
    $v_address_line_2 = isset($arr['address_line_2'])?$arr['address_line_2']:'';
    $v_address_line_3 = isset($arr['address_line_3'])?$arr['address_line_3']:'';
    $v_address_city = isset($arr['address_city'])?$arr['address_city']:'';
    $v_address_province = isset($arr['address_province'])?$arr['address_province']:'';
    $v_address_postal = isset($arr['address_postal'])?$arr['address_postal']:'';
    $v_direct_phone = isset($arr['direct_phone'])?$arr['direct_phone']:'';
    $v_email = isset($arr['email'])?$arr['email']:'';

    $v_full_name = $v_first_name.' ' .$v_last_name;
    $v_info = ($v_address_unit!=''?$v_address_unit.',':'') . ($v_address_line_1!=''?$v_address_line_1. '<br>':'');
    $v_info .= (trim($v_address_line_2)!="" ? "-". $v_address_line_2.'<br>': '');
    $v_info .= (trim($v_address_line_3)!="" ? "-". $v_address_line_3.'<br>': '');
    $v_info .=  $v_address_city.'&nbsp&nbsp' .$v_address_province .'&nbsp&nbsp'.   $v_address_postal.'<br>' ;
    $v_info .=  ($v_direct_phone!=''?$v_direct_phone .'<br>':'') ;
    $v_info .=  $v_email;
    if($v_csr_id==$v_contact_id) $v_tmp_csr_id = $v_contact_id;
    if($v_sales_rep_id==$v_contact_id) $v_tmp_sales_rep_id = $v_contact_id;
    $arr_all_contact[] = array(
        'contact_id'=>$v_contact_id,
        'contact_name'=>$v_full_name,
        'contact_info'=>$v_info
    );
}
$v_csr_id = $v_tmp_csr_id;
$v_sales_rep_id = $v_tmp_sales_rep_id;
//==
$v_temp_rule = '-1';
$arr_tb_rule = $cls_tb_rule->select(array('rule_comp'=>1));
$v_user_rule = $cls_tb_company->select_scalar('modules',array('company_id'=>(int)$v_company_id));
$v_count = 0;
$arr_setting = array();
foreach($arr_tb_rule as $arr){

    $v_rule_id = isset($arr['rule_id'])?$arr['rule_id']:0;
    $v_rule_title = isset($arr['rule_title'])?$arr['rule_title']:'';
    $v_rule_key = isset($arr['rule_key'])?$arr['rule_key']:'';
    $v_rule_type = isset($arr['rule_type'])?$arr['rule_type']:0;
    $v_rule_comp = isset($arr['rule_comp'])?$arr['rule_comp']:0;
    $v_rule_description = isset($arr['rule_description'])?$arr['rule_description']:'';
    $v_checked = '';
    if($v_temp_rule!=$v_rule_type)
    {
        if(!isset($arr_setting[$v_rule_type]))
            $arr_setting[$v_rule_type] = $cls_tb_setting->get_option_name_by_id('rule_type',$v_rule_type);

        $v_temp_rule = $v_rule_type;
        //$v_dsp_tb_rule .= '<tr align="left" valign="middle">
    //<th colspan="6"> Order Type: '.$arr_setting[$v_rule_type].'</th>
//</tr>';
    }
    if(strpos($v_user_rule,$v_rule_key)!== false) $v_checked = 'checked';
    if($v_rule_comp==1)
    {
        $v_company_module .= '<tr align="right" valign="top">';
            $v_company_module .= '<td align="center">'.'<input class="check" type="checkbox" id="check[]" name="check[]" '. $v_checked. ' value= '.$v_rule_key.' >'.'</td>';
            $v_company_module .= '<td align="center">'.(++$v_count).'</td>';
            $v_company_module .= '<td align="left">'.$v_rule_title.'</td>';
            $v_company_module .= '<td align="left">'.$v_rule_description.'</td>';
            $v_company_module .= '<td align="left">'.$arr_setting[$v_rule_type].'</td>';
        $v_company_module .= '</tr>';
    }
}
$v_content = '';
$v_title = '';
add_class("cls_tb_help");
$cls_tb_help = new cls_tb_help($db);
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$v_module_id = $cls_tb_module->select_scalar("module_id",array("module_menu"=>"manage_company"));
$v_row = $cls_tb_help->select_one(array("help_status"=>0,"help_module"=>(int)$v_module_id,"help_type"=>"admin_page"));
if($v_row == 1){
    $v_content = $cls_tb_help->get_help_content();
    $v_title = $cls_tb_help->get_help_title();
}
else{
    $v_content = "Help on this topic is not yet avaible";
    $v_title = "Help on company";
}
?>
