<style>
    .table_parent{
        padding-right:10px;
        width:32px;
        height:32px;
        padding-bottom:10px;
        background-repeat:no-repeat;
    }
    .td_class{
        padding-right:20px;
        font-weight:bold;
        padding-bottom:10px;
    }
    #popupWindow{margin: 0 auto;}
    .class_input{
        margin-right:5px;
        border: 1px solid #666;
        background-color:#0d0d0d;
        color:#d59eff;
        font-weight:bold;
        -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        border-radius: 6px;
        font-size: 13px;
        padding: 4px 10px;
        outline: 0;
        -webkit-appearance: none;
        cursor:pointer;
        float:right;
    }
    .class_input:hover {
        background-color:#1d1d1d;
    }
    #btn_add_new_contact{

        z-index: 1;
        position: absolute;
        clear: both;
        margin-top: -15px;
    }
    #pop_up input:last-child {
        margin-right: 5px;
    }
    #pop_up input:first-child {
        margin-left: -118px;
    }
    #pop_up input {
        margin-right: 125px;
    }
    .k-grid-content{
        height: 368px !important;
    }
</style>
<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
    windo_help = $('div#help_window');
    $('li#icon_help').bind("click", function() {
        if (!windo_help.data("kendoWindow")) {
            windo_help.kendoWindow({
                width: "990px",
                height: "500px",
                actions: ["Maximize", "Close"],
                modal: true,
                title: "<?php echo $v_title; ?>"
            });
        }
        windo_help.data("kendoWindow").center().open();
    });
    var length = $('div.order_detail table tr').length;
    var is_admin = '<?php echo $v_is_super_admin; ?>';
    if(length<=1 && !is_admin){
        $(".order_detail").hide();
    }
    popupWindow = $("#popupWindow").kendoWindow({
        actions: ["Close"],
        draggable: true,
        modal: true,
        resizable: false,
        visible: false,
        title: "Confirm action"
    }).data("kendoWindow");

//$v_dsp_order_status
    <?php if(isset($v_act) && $v_act=='E'){ ?>
    $("#btn_add_new_contact").on("click",function(){
        popupOpen("Confirm dialog box","You have unsaved changes. Do you want to save before continuing?","hung",["Cancel","No","Yes"],myTestClose,0,"Add");
    });
    <?php }?>
    $('#checkall').on('change', function () {
        $('.check').prop('checked', $(this).is(':checked'));
    });
	$("input#btn_submit_tb_company").click(function(e){
        if(!validator.validate()){
            e.preventDefault();
            return false;
        }
		return true;
	});
    var contact_data = <?php echo json_encode($arr_all_contact);?>;
    var combo_csr = $('select#txt_csr_id').width(200).kendoComboBox({
        filter:"startswith",
        dataSource: contact_data,
        dataTextField: "contact_name",
        dataValueField: "contact_id",
        template: '<h3>#= contact_name #</h3>' +
            '<p>#= contact_info #</p>'
    }).data("kendoComboBox");
    var combo_sales_rep_id = $('select#txt_sales_rep_id').width(200).kendoComboBox({
        filter:"startswith",
        dataSource: contact_data,
        dataTextField: "contact_name",
        dataValueField: "contact_id",
        template: '<h3>#= contact_name #</h3>' +
            '<p>#= contact_info #</p>'
    }).data("kendoComboBox");

    combo_csr.value(<?php echo $v_csr_id;?>);
    combo_sales_rep_id.value(<?php echo $v_sales_rep_id;?>);

    $('input#txt_logo_file').kendoUpload({
        multiple: false
    });
    <?php if($v_is_super_admin) {?>
    $("#txt_default_kitting_cost").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.1
    });
    <?php }?>
    $('input#txt_company_code').focusout(function(e){
        var val = $.trim($(this).val());
        if(val==''){
            $(this).val('');
            $('input#txt_hidden_company_code').val('N');
            return false;
        }
        var company_id = $('input#txt_company_id').val();
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type: 'POST',
            data:   {txt_session_id:'<?php echo session_id();?>', txt_company_id: company_id, txt_company_code: val, txt_ajax_type: 'check_company_code'},
            beforeSend: function(){
                $('span#sp_company_code').html('Checking...');
            },
            success: function(data, status){
                var ret = $.parseJSON(data);
                $('span#sp_company_code').html('');
                $('input#txt_hidden_company_code').val(ret.error==0?'Y':'');
                validator.validate();
            }
        });
    });
    $('select#txt_relationship').width(150).kendoDropDownList();
    $('select#txt_industry').width(150).kendoDropDownList();
    $('select#txt_business_type').width(150).kendoDropDownList();
    $('select#txt_status').width(100).kendoDropDownList();
    var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
        filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
});
// new function handel contacs tab
function ajax_tab_contact_handler(value,type){
    var company_id = <?php echo $v_company_id; ?>;
    $.ajax({
        url     : '<?php echo URL.$v_admin_key;?>/ajax',
        type    : 'POST',
        data    : {txt_session_id: '<?php echo session_id();?>',txt_company_id: company_id,txt_location_id:value,txt_type:type, txt_ajax_type:'tab_location_handler'},
        beforeSend: function(){
        }
        ,success: function(data, status){
        }
        ,error:function(data){
            //alert(data.responseText);
        }
    });
}
// new handler code for tab contact
function myTestClose(popupResult2,value,bntType){
    if(bntType=='View'){
        if(popupResult2 == 'Yes'){
            //ajax_tab_contact_handler(value,popupResult2);
            var short_path = value+"/view";
            $("input#txt_direct_link").val(short_path);
            $("input#btn_submit_tb_company").trigger("click");
            //document.location = "http://wt.delevlop.net/admin/contact/contact/"+value+"/view";
        }
        else if(popupResult2 == 'No'){
            document.location = '<?php echo URL; ?>'+"admin/company/location/"+value+"/view";
        }
    }
    else if(bntType=='Edit'){
        if(popupResult2 == 'Yes'){
            var short_path = value+"/edit";
            $("input#txt_direct_link").val(short_path);
            //ajax_tab_contact_handler(value,popupResult2);
            $("input#btn_submit_tb_company").trigger("click");
            //document.location = "http://wt.delevlop.net/admin/contact/contact/"+value+"/edit";
        }
        else if(popupResult2 == 'No'){
            document.location = '<?php echo URL; ?>'+"admin/company/location/"+value+"/edit";
        }
    }
    else if(bntType=='Add'){
        if(popupResult2 == 'Yes'){
            var short_path = "add";
            $("input#txt_direct_link").val(short_path);
            $("input#btn_submit_tb_company").trigger("click");
        }
        else if(popupResult2 == 'No'){
            document.location = '<?php echo URL; ?>'+"admin/company/location/add";
        }
    }
    else if(bntType=='Delete'){
        //ajax_tab_contact_handler(value,popupResult2); code newver go here because catch ajax event above
    }

}
var popupResult = "Cancel";
var popupResultValue = "0";
var popupButtonType = "View";
var popupWindow;
var popupCallback;

function popupClose(BtnResult,value,buttontType){
    popupResult = BtnResult;
    popupResultValue = value;
    popupButtonType = buttontType;
    popupWindow.close();
};

function popuploseCallBack(e){
    popupWindow.unbind("close", popuploseCallBack);
    if (popupCallback !== null){
        popupCallback(popupResult,popupResultValue,popupButtonType);
    }
}

function popupOpen(Title,Message,Type,Buttons,theFunction,value,buttontype){
    var popupData = '<table cellpadding="0" cellspacing="0"><tr><td><div class="table_parent ' + Type + '"></div></td>'+
        '<td><div class="td_class">'+Message+'</div></td></tr></table><div id="pop_up">';
    for(var i in Buttons){
        var s = Buttons[i];
        popupData += '<input class="class_input" data-contact-id="'+value+'" type="button" onclick="popupClose(\''+s+'\' ,\''+value+'\' , \''+buttontype+'\')" value="'+s+'">';
    }
    popupData += '</div>';
    popupResult = "Cancel";
    if (theFunction !== undefined){
        popupCallback = theFunction;
    } else {
        popupCallback = null;
    }
    popupWindow.bind("close", popuploseCallBack);
    popupWindow.title(Title);
    popupWindow.center();
    popupWindow.content(popupData);
    popupWindow.open();
}
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Company<?php echo $v_company_id>0?': '.$v_company_name:'';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select"><!--
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    //echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        --></div>
                        <div id="help_window" style="display:none">
                            <?php
                                echo $v_content ;
                            ?>
                        </div>
                    </div>

<form id="frm_tb_company" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_company_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_direct_link" name="txt_direct_link" />
    <?php if(!$v_is_super_admin) {?>
        <input type="hidden" id="txt_default_kitting_cost" name="txt_default_kitting_cost" value="<?php echo $v_default_kitting_cost;?>"  />
    <?php }?>
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <?php if($v_is_super_admin || $v_company_id ==10000 || $v_company_id ==0){?>
                        <li class="">Modules</li>
                        <?php } ?>
                        <li class="order_detail">Other Details</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td style="width: 200px">Company Name</td>
		<td style="width: 1px">&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_company_name" name="txt_company_name" value="<?php echo $v_company_name;?>" required data-required-msg="Please input Company Name" /> <label id="lbl_company_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Company Code</td>
		<td>&nbsp;</td>
		<td align="left">
            <?php if($v_company_id<=0){?>
            <input class="k-textbox" type="text" id="txt_company_code" name="txt_company_code" value="<?php echo $v_company_code;?>" required data-required-msg="Please input Company Code" pattern="\w+" validationMessage="Only normal character allowed" />
                <input type="hidden" id="txt_hidden_company_code" name="txt_hidden_company_code" value="<?php echo $v_company_code!=''?'Y':'N';?>" required data-required-msg="Duplicate Company Code" />
                <span id="sp_company_code"></span>
                <span class="tooltips"><a title="Company Code is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                <label id="lbl_company_code" class="k-required">(*)</label>
            <?php }else{?>
                <span><?php echo $v_company_code;?></span>
                <input type="hidden" value="<?php echo $v_company_code;?>" id="txt_company_code" name="txt_company_code" />
            <?php }?>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Relationship</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_relationship" name="txt_relationship">
                <?php echo $cls_settings->draw_option_by_id('relationship', 0, $v_relationship);?>
            </select>
        </td>
	</tr>
    <tr align="right" valign="top">
        <td>Industry</td>
        <td>&nbsp;</td>
        <td align="left">
            <select id="txt_industry" name="txt_industry">
                <?php echo $cls_settings->draw_option_by_id('industry', 0, $v_industry);?>
            </select>
        </td>
    </tr>
    <tr align="right" valign="top">
        <td>Business Type</td>
        <td>&nbsp;</td>
        <td align="left">
            <select id="txt_business_type" name="txt_business_type">
                <?php echo $cls_settings->draw_option_by_id('bussiness_type', 0, $v_bussines_type);?>
            </select>
        </td>
    </tr>
<tr align="right" valign="top">
		<td>Anvy CSR</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_csr_id" name="txt_csr_id">

            </select></td>
	</tr>
<tr align="right" valign="top">
		<td>Anvy Sales Rep</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_sales_rep_id" name="txt_sales_rep_id">

            </select>
            </td>
	</tr>
<tr align="right" valign="top">
		<td>Website</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="url" id="txt_website" name="txt_website" value="<?php echo $v_website;?>" data-url-msg="Invalid URL" /></td>
	</tr>
    <tr align="right" valign="top">
        <td>Email</td>
        <td>&nbsp;</td>
        <td align="left"><input class="text_css k-textbox" size="50" type="email" id="txt_sales_rep_email" name="txt_sales_rep_email" value="<?php echo $v_sales_rep_email;?>" data-email-msg="Invalid Email" /></td>
    </tr>
<tr align="right" valign="top">
		<td>Status</td>
		<td>&nbsp;</td>
		<td align="left">
            <select name="txt_status" id="txt_status">
                <?php echo $cls_settings->draw_option_by_id('status',0,$v_status); ?>
            </select>
        </td>
	</tr>
    <tr align="right" valign="top">
        <td>Logo File</td>
        <td>&nbsp;</td>
        <td align="left"><input type="file" id="txt_logo_file" name="txt_logo_file" accept="image/*" />
        <input type="hidden" name="txt_hidden_logo_file" value="<?php echo $v_logo_file;?>" />
        </td>
    </tr>
    <tr align="right" valign="top">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td align="left"><?php if($v_logo_url!='') echo $v_logo_url;?>&nbsp;</td>
    </tr>
</table>
                    </div>
                <?php if($v_is_super_admin || $v_company_id ==10000 || $v_company_id ==0){?>
                    <div class="modules div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width: 30px;text-align: center">
                                    <input type="checkbox" id="checkall" name="checkall"  ></td>
                                <td style="width: 50px; text-align: center">Ord</td>
                                <td style="width: 200px;text-align: center">Rule Title</td>
                                <td style="width: 500px;text-align: center">Description</td>
                                <td style="width: 100px;text-align: center">Type</td>
                            </tr>
                            <?php echo $v_company_module; ?>
                        </table>
                    </div>
                <?php }?>
                    <div class="order_detail div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <?php if($v_is_super_admin) {?>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Default kitting cost </td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input type="number" id="txt_default_kitting_cost" name="txt_default_kitting_cost" value="<?php echo $v_default_kitting_cost;?>"  />
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_company" name="btn_submit_tb_company" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
<div id="popupWindow"></div>