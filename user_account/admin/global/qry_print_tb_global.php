<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_global_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_global_sort'])){
	$v_sort = $_SESSION['ss_tb_global_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_global = $cls_tb_global->select($arr_where_clause, $arr_sort);
$v_dsp_tb_global = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_global .= '<tr align="center" valign="middle">';
$v_dsp_tb_global .= '<th>Ord</th>';
$v_dsp_tb_global .= '<th>Global Id</th>';
$v_dsp_tb_global .= '<th>Global Key</th>';
$v_dsp_tb_global .= '<th>Global Name</th>';
$v_dsp_tb_global .= '<th>Global Description</th>';
$v_dsp_tb_global .= '<th>Global Value</th>';
$v_dsp_tb_global .= '<th>Setting Name</th>';
$v_dsp_tb_global .= '<th>Setting Key</th>';
$v_dsp_tb_global .= '</tr>';
$v_count = 1;
foreach($arr_tb_global as $arr){
	$v_dsp_tb_global .= '<tr align="left" valign="middle">';
	$v_dsp_tb_global .= '<td align="right">'.($v_count++).'</td>';
	$v_global_id = isset($arr['global_id'])?$arr['global_id']:0;
	$v_global_key = isset($arr['global_key'])?$arr['global_key']:'';
	$v_global_name = isset($arr['global_name'])?$arr['global_name']:'';
	$v_global_description = isset($arr['global_description'])?$arr['global_description']:'';
	$v_global_value = isset($arr['global_value'])?$arr['global_value']:'';
	$v_setting_name = isset($arr['setting_name'])?$arr['setting_name']:'';
	$v_setting_key = isset($arr['setting_key'])?$arr['setting_key']:'';
	$v_dsp_tb_global .= '<td>'.$v_global_id.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_global_key.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_global_name.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_global_description.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_global_value.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_setting_name.'</td>';
	$v_dsp_tb_global .= '<td>'.$v_setting_key.'</td>';
	$v_dsp_tb_global .= '</tr>';
}
$v_dsp_tb_global .= '</table>';
?>