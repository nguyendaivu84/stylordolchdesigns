<?php
if(!isset($v_sval)) die();?>
<?php
$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:'0';
$v_role_id = isset($_POST['txt_role_id'])?$_POST['txt_role_id']:'0';
settype($v_user_id, 'int');
settype($v_role_id, 'int');
$arr_return = array('error'=>0, 'message'=>'OK');
if($v_user_view_right && $v_user_edit_right){
    if($v_role_id>0 && $v_user_id>0){
        $arr_role = $cls_tb_user->select_scalar('user_role', array('user_id'=>$v_user_id));
        if(is_array($arr_role)){
            $arr_tmp = array();
            for($i=0; $i<count($arr_role); $i++){
                if($arr_role[$i]!=$v_role_id)
                    $arr_tmp[] = $arr_role[$i];
            }
            $v_result = $cls_tb_user->update_field('user_role', $arr_tmp, array('user_id'=>$v_user_id));
            if(!$v_result){
                $arr_return['error'] = 2;
                $arr_return['message']= 'Error to revoke role!';
            }
        }else{
            $arr_return['error'] = 3;
            $arr_return['message']= 'Role for user not exist!';
        }
    }else{
        $arr_return['error'] = 1;
        $arr_return['message']= 'Lost data';
    }
}else{
    $arr_return['error'] = 4;
    $arr_return['message']= 'Sorry! You may not have permission!';
}
echo json_encode($arr_return);
?>