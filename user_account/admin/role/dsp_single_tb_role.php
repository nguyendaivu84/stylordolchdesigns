<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
    windo_help = $('div#help_window');
    $('li#icon_help').bind("click", function() {
        if (!windo_help.data("kendoWindow")) {
            windo_help.kendoWindow({
                width: "990px",
                height: "500px",
                actions: ["Maximize", "Close"],
                modal: true,
                title: "<?php echo $v_title; ?>"
            });
        }
        windo_help.data("kendoWindow").center().open();
    });
    $("input#btn_submit_tb_role").click(function(e){
		var css = '';
		var company_id = $("select#txt_company_id").val();
		company_id = parseInt(company_id, 10);
		css = isNaN(company_id)?'':(company_id<0?'':'none');

		if(css == ''){
            e.preventDefault();
            alert('Choose company before, please!');
            combo_company.focus();
            return false;
        }
        if(!validator.validate()){
            e.preventDefault();
            tab_strip.select(0);
            return false;
        }
        var tmp = new Array();
        for(var i=0; i<per.length; i++){
            if(per[i].status==0){
                var t = new Array(per[i].menu,per[i].key,per[i].description);
                tmp.push(t);
            }
        }
        var permission = JSON.stringify(tmp);

        $('input#txt_role_content').val(permission);

		return true;
	});
    $('input#chk_all_rule').click(function(){
        $(this).each(function(){
            var chk = $(this).prop('checked');
            //alert(chk);
            var menu = $(this).attr('data-menu');
            $('input#txt_permission').each(function(){
                var sub_menu = $(this).attr('data-menu');
                if(menu==sub_menu){
                    $(this).prop('checked', chk);
                    var key = $(this).val();
                    //var menu = $(this).attr('data-menu');
                    var desc = $(this).attr('title');
                    var i = 0, p = -1;
                    while(i<per.length && p<0){
                        if(per[i].menu==menu && per[i].key==key){
                            p = i;
                        }
                        i++;
                    }
                    if($(this).prop('checked')){
                        if(p<0) p = per.length;
                        per[p] = new Permission(menu, key, desc);
                    }else{
                        if(p>=0) per[p].status = 1;
                    }
                }
            });
        });
    });


    var validator = $('div.information').kendoValidator().data("kendoValidator");
    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");
    var locations = <?php echo json_encode($arr_all_location);?>;
    $("select#txt_location_id").width(250).kendoComboBox({
        dataSource: locations,
        dataTextField:'location_name',
        dataValueField:'location_id'
    });
    $('select#txt_role_type').kendoDropDownList();
    var locations_data = $("select#txt_location_id").data("kendoComboBox");
    locations_data.value(<?php echo $v_location_id;?>);
    var combo_company = $("select#txt_company_id").data("kendoComboBox");
    $("select#txt_color").width(150).kendoComboBox();
    $("select#txt_user_type").width(150).kendoComboBox();
    <?php if(!$v_disabled_company_id){?>

    $('select#txt_company_id').change(function(e) {
        var $this = $(this);
        var company_id = $(this).val();
        company_id = parseInt(company_id, 10);
        if(isNaN(company_id) || company_id<0) company_id = 0;
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type    : 'POST',
            data    :   {txt_company_id: company_id, txt_ajax_type:'load_location'},
            beforeSend: function(){
                $this.prop('disabled', true);
                combo_company.enable(false);
            },
            success: function(data, type){
                var ret = $.parseJSON(data);
                if(ret.error==0){
                    var locations = ret.location;
                    locations_data.setDataSource(locations);
                    locations_data.value(0);
                    $('form#frm_tb_role').find('#txt_company_id').val(company_id);
                }else{
                    alert(ret.message);
                }
                $this.prop('disabled', false);
                combo_company.enable(true);
            }
        });
    });
    <?php }else{?>
        combo_company.enable(false);
    <?php }?>

    $('input#txt_permission[type="checkbox"]').click(function(e) {
        $(this).each(function(index, element) {
            var key = $(this).val();
            var menu = $(this).attr('data-menu');
            var desc = $(this).attr('title');
            var i = 0, p = -1;
            while(i<per.length && p<0){
                if(per[i].menu==menu && per[i].key==key){
                    p = i;
                }
                i++;
            }
            if($(this).prop('checked')){
                if(p<0) p = per.length;
                per[p] = new Permission(menu, key, desc);
            }else{
                if(p>=0) per[p].status = 1;
            }
        });
    });
    $('input#txt_role_key').focusout(function(e){
        var key = $.trim($(this).val());
        if(key==''){
            $(this).val('');
            $('input#txt_hidden_role_key').val('N');
            validator.validate();
            return false;
        }
        var company_id = $('input#txt_company_id').val();
        var role_id = $('input#txt_role_id').val();
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type:   'POST',
            data:   {txt_session_id: '<?php echo session_id();?>', txt_ajax_type: 'check_role_key', txt_company_id: company_id, txt_role_id: role_id, txt_role_key: key},
            beforeSend: function(){
                $('span#sp_role_key').html('Checking...');
            },
            success: function(data, status){
                $('span#sp_role_key').html('');
                var ret = $.parseJSON(data);
                $('input#txt_hidden_role_key').val(ret.error==0?'Y':'');
                validator.validate();
            }
        });
    });

    <?php if($v_role_id>0){?>
    var granted_user = <?php echo json_encode($arr_granted_user);?>;
    var granted_user_data = new kendo.data.DataSource({
        data: granted_user,
        pageSize: granted_user.length
    });
    granted_user_data.read();

    var granted_grid = $('div#granted_grid').kendoGrid({
        dataSource:granted_user_data,
        height: 310,
        scrollable: true,
        sortable: false,
        filterable: true,
        pageable:false,
        <?php if($v_user_edit_right && $v_user_view_right){?>
        dataBound: on_data_bound,
        <?php }?>
        columns: [
            /*{field: "row_order", title: "&nbsp;", type:"int", width:"20px", sortable: false,filterable: false, template: '<span style="float:right">#= row_order #</span>'},*/
            {field: "user_name", title: "User Name", type:"string", width:"60px", sortable: false },
            {field: "full_name", title: "Full Name", type:"string", width:"70px", sortable: false },
            {field: "user_type", title: "User Type", type:"string", width:"50px", sortable: false},
            {field: "location_name", title: "Location", type:"string", width:"100px", sortable: false},
            {field: "company_name", title: "Company", type:"string", width:"100px", sortable: false},
            {field: "default_role", title: "Default Role", type:"boolean", width:"50px", sortable: false, filterable: false, template:'<span style="float:right">#= default_role?"Yes":"No"#</span>'},
            {field: "user_status", title: "User Status", type:"boolean", width:"50px", sortable: false, filterable: false, template:'<span style="float:right">#= user_status?"Active":"Inactive"#</span>'}
            <?php if($v_user_edit_right && $v_user_view_right){?>
            ,{command:{text:"Revoke", click: revoke_role}, title:" ", width: "50px"}
            <?php }?>
        ]

    }).data("kendoGrid");
    <?php if($v_user_edit_right && $v_user_view_right){?>
    function on_data_bound(e){
        var grid = e.sender;
        var data = grid.dataSource.view();
        for(var i=0; i<data.length; i++){
            if(data[i].stranger==0){
                grid.tbody.find("tr[data-uid='" + data[i].uid + "']").find("td:last .k-grid-Revoke").css("display", "none");
            }
        }
    }

    function revoke_role(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var user_id = dataItem.user_id;
        var role_id = '<?php echo $v_role_id;?>';
        var role_title = '<?php echo addslashes($v_role_title);?>';
        var ask = confirm("Do you want to revoke role '"+role_title+"' from '"+dataItem.full_name+"'?");
        if(ask){
            //dataItem.remove();
            $.ajax({
                url:   '<?php echo URL.$v_admin_key;?>/ajax',
                type: 'POST',
                dataType :'json',
                data : {txt_session_id:'<?php echo session_id();?>', txt_role_id: role_id, txt_user_id: user_id, txt_ajax_type:'revoke_role'},
                beforeSend: function(){

                },
                success: function(data){
                    if(data.error==0)
                        granted_grid.removeRow($(e.currentTarget).closest("tr"));
                    else
                        alert(data.message);
                }
            });
        }
    }
    <?php }// End if user right?>
    <?php }// End if role_id>0?>
    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        }
    }).data("kendoTabStrip");
    $('select#txt_role_type').change(function(e){
        var idx = $(this).val();
        idx = parseInt(idx, 10);
        if(isNaN(idx)) idx = 0;
        if(idx!=1) idx = 0;
        $(this).val(idx);
        var tabIndex = 1;
        var tab = tab_strip.tabGroup.children("li").eq(tabIndex);

        tab_strip.enable(tab, idx==0);
    });
});
function Permission(menu, key, description){
    this.menu = menu;
    this.key = key;
    this.description = description;
    this.status = 0;
}
    <?php
    echo $v_dsp_script;
    ?>
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Role<?php if($v_role_id>0) echo ': '.$v_role_title;?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                        <div id="help_window" style="display:none">
                            <?php
                            echo $v_content ;
                            ?>
                        </div>
                    </div>

<form id="frm_tb_role" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_role_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_role_id" name="txt_role_id" value="<?php echo $v_role_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
<input type="hidden" id="txt_role_content" name="txt_role_content" value="" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Permission</li>
                        <?php if($v_role_id>0){?>
                        <li>Granted users</li>
                        <?php }?>
                    </ul>
                    <div class="information div_details">
<table id="tbl_required" align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
    <tr align="right" valign="top">
        <td style="width:200px">Location</td>
        <td style="width:1px">&nbsp;</td>
        <td align="left">
            <select id="txt_location_id" name="txt_location_id" disabled="disabled">
            </select>
        </td>
    </tr>
<tr align="right" valign="top">
		<td>Role Title</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_role_title" name="txt_role_title" value="<?php echo $v_role_title;?>"  required validationMessage="Please input Role Title" /> <label id="lbl_role_title" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Role Type</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_role_type" name="txt_role_type">
                <?php echo $v_draw_role_type;?>
            </select>
            </td>
	</tr>
<tr align="right" valign="top">
		<td>Role Key</td>
		<td>&nbsp;</td>
		<td align="left">
            <input class="text_css k-textbox" size="50" type="text" id="txt_role_key" name="txt_role_key" value="<?php echo $v_role_key;?>" required validationMessage="Please input Role Key" />
            <input type="hidden" id="txt_hidden_role_key" name="txt_hidden_role_key" value="<?php echo $v_role_key!=''?'Y':'N';?>" required validationMessage="Role Key is unique" />
            <span id="sp_role_key"></span>
            <span class="tooltips"><a title="Role Key is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
            <label id="lbl_role_key" class="k-required">(*)</label>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Status</td>
		<td>&nbsp;</td>
		<td align="left"><div class="checkbox"><input type="checkbox" id="txt_status" name="txt_status"<?php echo $v_status?'':' checked="checked"';?>" /><label for="txt_status"> Active</label></div></td>
	</tr>
<tr align="right" valign="top">
		<td>Default Role</td>
		<td>&nbsp;</td>
		<td align="left"><div class="checkbox"><input type="checkbox" id="txt_default_role" name="txt_default_role"<?php echo $v_default_role?' checked="checked"':'';?>" /><label for="txt_default_role"> Default Role for: </label>
        <select id="txt_user_type" name="txt_user_type">
            <?php
            echo $cls_settings->draw_option_by_id('user_type',0, $v_user_type);
            ?>
        </select></div>
        </td>
	</tr>
<tr align="right" valign="top">
		<td>Color</td>
		<td>&nbsp;</td>
		<td align="left">
            <select id="txt_color" name="txt_color">
            <option value="">------</option>
                <?php echo $cls_tb_color->draw_option('color_code', 'color_name', $v_color);?>
            </select>
            </td>
	</tr>
<tr align="right" valign="top">
		<td>Bold</td>
		<td>&nbsp;</td>
		<td align="left"><div class="checkbox"><input type="checkbox" id="txt_bold" name="txt_bold"<?php echo $v_bold?' checked="checked"':'';?>" /><label></label></div></td>
	</tr>
<tr align="right" valign="top">
		<td>Italic</td>
		<td>&nbsp;</td>
		<td align="left"><div class="checkbox"><input type="checkbox" id="txt_italic" name="txt_italic"<?php echo $v_italic?' checked="checked"':'';?>" /><label></label></div></td>
	</tr>
</table>
                    </div>
                    <div class="permission div_details">
                        <?php echo $v_dsp_modules;?>
                    </div>

<?php if($v_role_id>0){?>
    <div class="granted div_details">
        <div id="granted_grid"></div>
    </div>
<?php }?>

                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_role" name="btn_submit_tb_role" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
