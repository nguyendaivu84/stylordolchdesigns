<?php if(!isset($v_sval)) die(); ?>
<?php

$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
$v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:'0';
$arr_return = array('error'=>0, 'message'=>'', 'data'=>'');

if($v_location_id==0)
    $arr_where  = array('company_id'=>(int)$v_company_id ) ;
else
    $arr_where  = array('company_id'=>(int)$v_company_id,'location_id'=>(int)$v_location_id );
$_SESSION['ss_last_company_id'] = $v_company_id;
if($v_company_id>=0){
    $arr_contact = $cls_tb_contact->select($arr_where, array('contact_name'=>1));
    $v_dsp_contact = '';
    foreach($arr_contact as $arr){
        $v_dsp_contact .= '<option value="'.$arr['contact_id'].'">'.$arr['first_name']. ', ' .$arr['last_name'] .'</option>';
    }
    $v_dsp_contact = '<option value="0" selected="selected">------</option>'.$v_dsp_contact;
    if($v_dsp_contact!=''){
        $arr_return['error'] = 0;
        $arr_return['message']= 'OK!';
        $arr_return['data'] = $v_dsp_contact;
    }else{
        $arr_return['error'] = 2;
        $arr_return['message']= 'Cannot load contact!';
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message']= 'Conpany ID or Location ID is negative!';
}
die(json_encode($arr_return));
?>