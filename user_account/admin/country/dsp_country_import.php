<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <form id="frm_tb_import" action="<?php echo URL.$v_admin_key;?>/import" method="POST">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Review Country Data (The exist record will not be record)</h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                    <div id="div_select">
                        &nbsp;
                    </div>
                </div>
                <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Review Information</li>
                    </ul>
                    <div class="information div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="center" valign="top">
                                <td style="width: 20px">Ord</td>
                                <td style="width: 30px">Exist</td>
                                <td style="width: 50px">Country Id</td>
                                <td style="width: 100px">Country Name</td>
                                <td style="width: 100px">Country Key</td>
                                <td style="width: 100px">Country Order</td>
                                <td style="width: 50px">Country Status</td>
                            </tr>
                            <?php echo  $dsp_import;?>
                        </table>
                    </div>
                </div>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_import" name="btn_submit_import" value="Submit" class="k-button button_css" />
                        </div>
            </div>
            </form>
        </div>
    </div>
</div>