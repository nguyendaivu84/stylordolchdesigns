<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_tb_tag_where_clause'])){
    $v_where_clause = $_SESSION['ss_tb_tag_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_tag_sort'])){
    $v_sort = $_SESSION['ss_tb_tag_sort'];
    $arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_tag = $cls_tb_country->select($arr_where_clause, $arr_sort);
$v_dsp_tb_country = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="1" align="center">';

$v_dsp_tb_country .= '<tr align="center" valign="middle">';
$v_dsp_tb_country .= '<th>Ord</th>';
$v_dsp_tb_country .= '<th>Country Id</th>';
$v_dsp_tb_country .= '<th>Country Name</th>';
$v_dsp_tb_country .= '<th>Country Key</th>';
$v_dsp_tb_country .= '<th>Country Order</th>';
$v_dsp_tb_country .= '<th>Country Status</th>';
$v_dsp_tb_country .= '</tr>';
$v_count = 1;
foreach($arr_tb_tag as $arr){
    $v_dsp_tb_country .= '<tr align="left" valign="middle">';
    $v_dsp_tb_country .= '<td align="center">'.($v_count++).'</td>';
    $v_country_id = isset($arr['country_id'])?$arr['country_id']:0;
    $v_country_name = isset($arr['country_name'])?$arr['country_name']:'';
    $v_country_key = isset($arr['country_key'])?$arr['country_key']:0;
    $v_country_order = isset($arr['country_order'])?$arr['country_order']:0;
    $v_country_status = isset($arr['country_status'])?$arr['country_status']:0;
    $v_country_status = $v_country_status==0?"Active":"Inactive";

    $v_dsp_tb_country .= '<td align="center">'.$v_country_id.'</td>';
    $v_dsp_tb_country .= '<td>'.$v_country_name.'</td>';
    $v_dsp_tb_country .= '<td align="center">'.$v_country_key.'</td>';
    $v_dsp_tb_country .= '<td align="center">'.$v_country_order.'</td>';
    $v_dsp_tb_country .= '<td>'.$v_country_status.'</td>';
    $v_dsp_tb_country .= '</tr>';
}
$v_dsp_tb_country .= '</table>';
?>