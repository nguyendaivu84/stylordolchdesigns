<?php if(!isset($v_sval)) die();?>
<?php
    @session_start();
    @ob_start();
    if(isset($_POST['btn_submit_import'])){
        $v_allowedExts = array("xls");
        $v_extension = end(explode(".", $_FILES["txt_file_select"]["name"]));
        if($v_extension=='') $_SESSION['error_reading'] = "Please choose a file to open";
        else if(in_array($v_extension, $v_allowedExts)==false) $_SESSION['error_reading'] = "Please choose a xls file";
        if($v_extension=='xls'){
            move_uploaded_file($_FILES["txt_file_select"]["tmp_name"], $_FILES["txt_file_select"]["name"]);
            $_SESSION['import_name'] = $_FILES["txt_file_select"]["name"];
        }
        $v_path = URL.$v_admin_key."/review";
        header("location:$v_path");
    }
?>
<script>
    $(document).ready(function(){
        $('input#txt_file_select').kendoUpload({
            multiple: false
        });
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <form id="frm_tb_import" method="POST"  enctype="multipart/form-data" action="">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Select File Country Data <span style="color: red"><?php if(isset($_SESSION['error_reading'])) echo "(". $_SESSION['error_reading'] . ")"; ?></span> </h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                            &nbsp;
                        </div>
                        <div id="div_select">
                            <input type="file" id="txt_file_select" name="txt_file_select" accept="application/vnd.sealed.xls,application/vnd.ms-excel" />
                        </div>
                    </div>
                    <div id="div_quick" style="margin-bottom:20px;height: 10px;">

                    </div>
                    <div class="k-block k-widget div_buttons">
                        <input type="submit" id="btn_submit_import" name="btn_submit_import" value="Submit" class="k-button button_css" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>