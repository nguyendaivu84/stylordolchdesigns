<?php if(!isset($v_sval)) die();?>
<?php
    $dsp_import = '';
    $v_file_name = $_SESSION['import_name'];
    require_once 'import/Excel/reader.php';
    $data = new Spreadsheet_Excel_Reader();
    $data->setOutputEncoding('UTF-8');
    if(is_readable($v_file_name)==false){
        $_SESSION['error_reading'] = "Please choose another file";
        $v_path = URL.$v_admin_key.'/select';
        header("location:$v_path");
    }
    if($data->read($v_file_name) == false){
        $_SESSION['error_reading'] = "Please choose a xls file 2";
        $v_path = URL.$v_admin_key.'/select';
        header("location:$v_path");
    }
    $data->read($v_file_name);
    $v_total = 1;
    $arr_country_name = array("");
    $arr_country_key = array("");
    $arr_cls_tb_country = array();
    $arr_country = $cls_tb_country->select();
    $v_exist = '';
    $v_count = 0;
    $v_country_id_next = $cls_tb_country->select_next('country_id');
    foreach($arr_country as $arr){
        array_push($arr_country_name, $arr['country_name']);
        array_push($arr_country_key, $arr['country_key']);
    }
    for ($x = 3; $x <= count($data->sheets[0]["cells"]); $x++) {
        $v_country_id  = $v_country_id_next++;
        $v_country_name = $data->sheets[0]["cells"][$x][1];
        $v_country_key = $data->sheets[0]["cells"][$x][2];
        $v_country_status = $data->sheets[0]["cells"][$x][3];
        $v_country_order = $data->sheets[0]["cells"][$x][4];
        $v_temp = $v_country_status==0?"Active":"Inactive";
        if(in_array($v_country_name,$arr_country_name)
            || in_array($v_country_key,$arr_country_key) ){
            //continue;
            $v_exist = 'Yes';
            $v_span = '<span style="color:red">';
        }
        else{
            $arr_cls_tb_country[$v_count++] = array("country_id" =>$v_country_id,"country_name"=>$v_country_name
            ,"country_key"=>$v_country_key ,"country_order"=>$v_country_order,"country_status"=>$v_country_status);
            $v_exist = 'No';
            $v_span = '<span style="color:blue">';
        }
        $dsp_import .='<tr align="right" valign="top"><td style="width:20px; text-align:center">'.$v_total++.'</td>';
        $dsp_import .='<td style="width:30px; text-align:center">'.$v_span.$v_exist++.'<span></td>';
        $dsp_import .='<td style="width:50px ;text-align:center">'.$v_country_id.'</td>';
        $dsp_import .='<td style="width:100px; text-align:left; padding-left:5px;">'.$v_country_name.'</td>';
        $dsp_import .='<td style="width:100px;text-align:left; padding-left:5px;">'.strtoupper($v_country_key).'</td>';
        $dsp_import .='<td style="width:100px ; text-align:center">'.$v_country_order.'</td>';
        $dsp_import .='<td style="width:50px ; text-align:center">'.$v_temp.'</td>';
        $dsp_import .='</tr>';
}
$_SESSION['arr_cls_tb'] = $arr_cls_tb_country;
?>