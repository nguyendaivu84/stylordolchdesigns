<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        $("input#btn_submit_tb_faq").click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
            var company_id = $("select#txt_company_id").val();
            $("input#txt_hidden_company_id").val(company_id);
            return true;
        });
        var editor = $('textarea#txt_faq_answer').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false
        });//.data("kendoEditor");
        $('select#txt_faq_keyword').width(300).kendoDropDownList();
        $('#txt_faq_order').kendoNumericTextBox({
            format:"n0",
            min:0,
            step:1
        });
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        var validator = $("div.information").kendoValidator().data("kendoValidator");
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>FAQ<?php echo $v_faq_id>0?': "'.$v_faq_question.'"':''?></h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                    <div id="div_select">
                        <form id="frm_company_id" method="post">
                            Company: <select id="txt_company_id" name="txt_company_id">
                                <option value="0" selected="selected">-------</option>
                                <?php
                                    echo $v_dsp_company_option;
                                ?>
                            </select>
                        </form>
                    </div>
                </div>

                <form id="frm_tb_email_templates" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_faq_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_help_id" name="txt_help_id" value="<?php echo $v_faq_id;?>" />
                    <input type="hidden" id="txt_hidden_company_id" name="txt_hidden_company_id" value="<?php echo $v_company_id;?>" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                        </ul>
                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">FAQ Question</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="text_css k-textbox" size="50" type="text" id="txt_faq_question" name="txt_faq_question" value="<?php echo isset($v_faq_question)?htmlspecialchars($v_faq_question):'';?>" required />
                                        <input type="hidden" id="txt_hidden_faq_question" name="txt_hidden_faq_question" value="<?php echo $v_faq_question!=''?'Y':'N';?>"  required />
                                        <label id="lbl_email_key" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>FAQ Sort order</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <input style="width: 300px" type="text" id="txt_faq_order" name="txt_faq_order" value="<?php echo $v_faq_order;?>" required />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>FAQ Keyword</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_faq_keyword" name="txt_faq_keyword">
                                            <?php echo $v_dsp_faq_keyword; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>FAQ Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><label><input type="checkbox" id="txt_faq_status" name="txt_faq_status" value="<?php echo $v_faq_status;?>"<?php echo $v_faq_status==0?' checked="checked"':'';?> /> Active?</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>FAQ Answer</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 97%; height: 200px; padding:5px;"id="txt_faq_answer" name="txt_faq_answer"><?php echo $v_faq_answer;?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_faq" name="btn_submit_tb_faq" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
