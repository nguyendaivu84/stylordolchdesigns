<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_faq_id = 0;
$v_faq_status = 0;
$v_faq_question = '';
$v_faq_answer = '';
$v_faq_order = 0;
$v_faq_keyword = 0;
$v_company_id = 0;
add_class("cls_settings");
$cls_tb_setting = new cls_settings($db);
if(isset($_POST['btn_submit_tb_faq'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_faq->set_mongo_id($v_mongo_id);
    $v_faq_id = isset($_POST['txt_faq_id'])?$_POST['txt_faq_id']:$v_faq_id;
    if(is_null($v_mongo_id)){
        $v_faq_id = $cls_tb_faq->select_next('faq_id');
    }
    $v_faq_id = (int) $v_faq_id;
    $cls_tb_faq->set_faq_id($v_faq_id);

    $v_faq_question = isset($_POST['txt_faq_question'])?$_POST['txt_faq_question']:$v_faq_question;
    $v_faq_question = trim($v_faq_question);
    if($v_faq_question=='') $v_error_message .= '[faq question] is empty!<br />';
    $cls_tb_faq->set_faq_question($v_faq_question);

    $v_faq_status = isset($_POST['txt_faq_status'])?0:1;
    $cls_tb_faq->set_faq_status($v_faq_status);

    $v_faq_answer = isset($_POST['txt_faq_answer'])?$_POST['txt_faq_answer']:$v_faq_answer;
    $v_faq_answer = trim($v_faq_answer);
    $cls_tb_faq->set_faq_answer($v_faq_answer);

    $v_faq_order = isset($_POST['txt_faq_order'])?$_POST['txt_faq_order']:$v_faq_order;
    settype($v_faq_order,"int");;
    $cls_tb_faq->set_faq_order($v_faq_order);

    $v_faq_keyword = isset($_POST['txt_faq_keyword'])?$_POST['txt_faq_keyword']:$v_faq_keyword;
    if($v_faq_keyword=='') $v_error_message .= '[faq Keyword] is empty!<br />';
    $cls_tb_faq->set_faq_keyword($v_faq_keyword);

    $v_company_id = isset($_POST['txt_hidden_company_id'])?$_POST['txt_hidden_company_id']:$v_company_id;
    settype($v_company_id,"int");
    $cls_tb_faq->set_company_id($v_company_id);

    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_faq->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_faq->update(array('_id' => $v_mongo_id));
        }
        if($v_result){
            $_SESSION['ss_tb_faq_redirect'] = 1;
            redir(URL.$v_admin_key);
        }
    }
}else{
    $v_faq_id= isset($_GET['id'])?$_GET['id']:'0';
    settype($v_faq_id,'int');
    if($v_faq_id>0){
        $v_row = $cls_tb_faq->select_one(array('faq_id' => $v_faq_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_faq->get_mongo_id();
            $v_faq_id = $cls_tb_faq->get_faq_id();
            $v_faq_question = $cls_tb_faq->get_faq_question();
            $v_faq_status = $cls_tb_faq->get_faq_status();
            settype($v_faq_status,"int");
            $v_faq_answer = $cls_tb_faq->get_faq_answer();
            $v_faq_keyword = $cls_tb_faq->get_faq_keyword();
            $v_faq_order = $cls_tb_faq->get_faq_order();
            $v_company_id = $cls_tb_faq->get_company_id();
            settype($v_faq_module,"int");
        }
    }
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company);
$v_dsp_faq_keyword = $cls_tb_setting->draw_option_by_key("faq_type",0,$v_faq_keyword);
$_SESSION['ss_last_company_id'] = (int)$v_company_id;
?>