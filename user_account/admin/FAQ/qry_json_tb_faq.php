<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_title = isset($_POST['txt_search_title'])?$_POST['txt_search_title']:'';

settype($v_company_id, 'int');
if($v_company_id > 0){
    $arr_where_clause['company_id'] = $v_company_id;
    $_SESSION['ss_last_company_id'] = $v_company_id;
}
if($v_search_title!='') $arr_where_clause['faq_question'] = new MongoRegex('/'.$v_search_title.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        if($arr_temp[$i]['field']=='status') $arr_sort = array("help_status"=>$arr_temp[$i]['dir']=='asc'?1:-1);
        else $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_faq_redirect']) && $_SESSION['ss_tb_faq_redirect']==1){
    if(isset($_SESSION['ss_tb_faq_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_faq_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_faq_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_faq_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_faq_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_faq->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_faq_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_faq_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_faq_page'] = $v_page;
$_SESSION['ss_tb_faq_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("faq_question"=>1);
$arr_tb_faq = $cls_tb_faq->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$arr_company = array();
$arr_location = array();
foreach($arr_tb_faq as $arr){
    $v_faq_id = isset($arr['faq_id'])?$arr['faq_id']:0;
    $v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
    $v_faq_question = isset($arr['faq_question'])?$arr['faq_question']:'';
    $v_faq_answer = isset($arr['faq_answer'])?$arr['faq_answer']:'';
    $v_is_answer = $v_faq_answer!=''?"Yes":"No";
    $v_faq_order = isset($arr['faq_order'])?$arr['faq_order']:'';
    $v_faq_status = isset($arr['faq_status'])?$arr['faq_status']:1;
    settype($v_faq_status,"int");
    $v_faq_status = $v_faq_status==0 ?"Active":"Inactive";
    $v_faq_keyword = isset($arr['faq_keyword'])?$arr['faq_keyword']:'';
    if(!isset($arr_company[$v_company_id]) && $v_company_id!=0) $arr_company[$v_company_id] = $cls_tb_company->select_scalar('company_name', array('company_id'=>$v_company_id));
    else if($v_company_id==0) $arr_company[$v_company_id] = 'All company';
    $v_company_id = $arr_company[$v_company_id];

    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'faq_id' => $v_faq_id,
        'faq_question' => $v_faq_question,
        'faq_answer' => $v_faq_answer,
        'faq_is_answer' => $v_is_answer,
        'faq_keyword' => $v_faq_keyword,
        'company_name' => $v_company_id,
        'faq_status' => $v_faq_status,
        'order' => $v_faq_order
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_faq'=>$arr_ret_data);
echo json_encode($arr_return);
?>