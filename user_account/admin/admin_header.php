<?php if(!isset($v_sval)) die();?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <title><?php echo $v_main_site_title .' - '.  $v_sub_site_title;?> </title>
    <base href="<?php echo URL;?>" />
    <link href="<?php echo URL;?>lib/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/css/special.css" rel="stylesheet" type="text/css" />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo URL;?>lib/kendo/css/kendo.common.min.css" rel="stylesheet" />
    <link id="link_theme" href="<?php echo URL;?>lib/css/theme.<?php echo $v_default_theme;?>.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.resize.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/kendo/js/kendo.web.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo URL; ?>lib/js/common.js"></script>
    <?php echo js_fancybox();?>
    <script type="text/javascript">
        $(document).ready(function(e) {
            $("#menu").kendoMenu({
                dataUrlField: "LinksTo",
                <?php echo $v_dsp_horizontal_menu;?>
            });
            var treeview = $("#div_treeview").kendoTreeView({
                dataUrlField: "LinksTo",
                <?php echo $v_dsp_tree_menu;?>
            }).data("kendoTreeView");
            $('select#txt_company_id').width(250).kendoComboBox();
            $('select#txt_search_company_id').width(250).kendoComboBox();
            $('div#div_logo').bind('click', function(){
                document.location = '<?php echo URL;?>';
            });
            $('div#div_right_pane').resize(function(){
                var splitter = $("#div_splitter_content").data("kendoSplitter");
                if(splitter) splitter.trigger("resize");
            });
        });
    </script>

</head>
<body>
<div id="div_wrapper">
    <div id="div_header" class="k-widget">
        <div id="div_logo_new">Visual <span class="orgcolor">Impact</span></div>
        <div id="div_info">
            <div id="div_account">
                        Hello <a id="a_user_account" style="cursor: pointer; font-weight: bold"><?php echo (isset($arr_user['contact_name']) && $arr_user['contact_name']!='')?$arr_user['contact_name']:(isset($arr_user['user_name'])?$arr_user['user_name']:'');?></a>
                      | <a href="<?php echo URL;?>logout">Logout</a>
            </div>
            <div id="div_icon">
                <ul class="icons">
                    <li style="display: none">
                        <p><img id="icons_splitter" src="images/icons/arrow_right.png" title="Theme" /></p><p>Expand Pane</p>
                    </li>
                    <li>
                        <p><img id="icons_theme" src="images/icons/color_wheel.png" title="Theme" /></p><p>Theme</p>
                    </li>
                    <?php if($v_show_function_icon){?>
                        <?php echo isset($v_dsp_show_action_icons)?$v_dsp_show_action_icons:'';?>
                        <?php if($v_show_report_icon && @$v_act=='A'){?>
                            <li>
                                <a href="<?php echo URL.$v_admin_key;?>/print" target="_blank">
                                    <p><img id="icons" src="images/icons/printer.png" title="Print" /></p><p>Print</p>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo URL.$v_admin_key;?>/export" target="_blank">
                                    <p><img id="icons" src="images/icons/excel.png" title="Export" /></p><p>Export</p>
                                </a>
                            </li>
                        <?php }?>
                        <?php if(isset($v_show_import_icon) && $v_show_import_icon==1 && @$v_act=='A'){?>
                            <li>
                                <a href="<?php echo URL.$v_admin_key;?>/select" target="_blank">
                                    <p><img id="icons" src="images/icons/import.png" title="Export" /></p><p>Import</p>
                                </a>
                            </li>
                        <?php }?>
                        <?php if($v_show_view_icon&& @$v_act!='A'){?>
                            <li>
                                <a href="<?php echo URL.$v_admin_key;?>">
                                    <p><img id="icons" src="images/icons/all.png" title="View All" /></p><p>View</p>
                                </a>
                            </li>
                        <?php }?>
                        <?php if($v_show_create_icon && @$v_act=='A'){?>
                            <li>
                                <a href="<?php echo URL.$v_admin_key.'/add';?>">
                                    <p><img id="icons_add_new" src="images/icons/add.png" title="Add New" /></p><p>Add New</p>
                                </a>
                            </li>
                        <?php }?>
                        <?php if($v_show_search_icon && @$v_act=='A'){?>
                            <li id="icons_advanced_search">
                                <p><img id="icons_advanced_search" src="images/icons/zoom.png" title="Advanced Search" /></p><p>Advanced</p>
                            </li>
                        <?php }?>
                        <?php if(isset($v_show_help_icon) && $v_show_help_icon==1 && (@$v_act=='A' || @$v_act=='N' || @$v_act=='E')){?>
                            <li id="icon_help">
                                <p><img id="icons_help" src="images/icons/help.png" title="Help" /></p><p>Help</p>
                            </li>
                        <?php }?>
                    <?php }?>
                </ul>
            </div>
        </div>
        <div id="div_header_top">&nbsp;</div>
        <div id="div_header_menu">
            <div id="menu"></div>
        </div>
    </div>

    <div id="user_change_password" style="display:none">
        <form id="frm_change_password" method="post" action="<?php echo URL.'admin/user';?>">
            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                <tr align="left" valign="middle">
                    <td align="right">Current password:</td>
                    <td>
                        <input type="password" class="k-textbox" id="txt_user_old_password" />
                    </td>
                </tr>
                <tr align="left" valign="middle">
                    <td align="right">New password:</td>
                    <td>
                        <input type="password" class="k-textbox" id="txt_user_new_password" />
                    </td>
                </tr>
                <tr align="left" valign="middle">
                    <td align="right">Repeat password:</td>
                    <td>
                        <input type="password" class="k-textbox" id="txt_user_repeat_password" />
                    </td>
                </tr>
                <tr align="center" valign="middle">
                    <td colspan="2">
                        <input type="hidden" id="txt_current_user_id" value="<?php echo isset($arr_user['user_id'])?$arr_user['user_id']:0;?>" />
                        <input type="hidden" id="txt_current_user_name" value="<?php echo isset($arr_user['user_name'])?$arr_user['user_name']:'';?>" />
                        <input type="button" class="k-button k-button button_css" value="Save Change" id="btn_change_user_password" />
                    </td>
                </tr>
            </table>
        </form>
    </div>
<script language="javascript" type="text/javascript">
    var window_change_password;
    $(document).ready(function(e){
        window_change_password = $('div#user_change_password');
        $('a#a_user_account').bind("click", function() {
            if (!window_change_password.data("kendoWindow")) {
                window_change_password.kendoWindow({
                    width: "600px",
                    actions: ["Close"],
                    resizable:false,
                    modal: true,
                    title: "Change user's password"
                });
            }
            window_change_password.data("kendoWindow").center().open();
        });
        $('input#btn_change_user_password').click(function(e){
            var user_id = $('input#txt_current_user_id').val();
            var user_name = $('input#txt_current_user_name').val();
            var old_password = $('input#txt_user_old_password').val();
            var new_password = $('input#txt_user_new_password').val();
            var repeat_password = $('input#txt_user_repeat_password').val();
            user_id = parseInt(user_id,10);
            if(isNaN(user_id)) user_id = 0;
            if(user_id<0){
                e.preventDefault();
                alert('You cannot change your password now!');
                return;
            }
            if(user_name==''){
                e.preventDefault();
                alert('You cannot change your password now!');
                return;
            }
            if(old_password==''){
                e.preventDefault();
                alert('Please input your current password!');
                return;
            }
            if(new_password.length <6 ){
                e.preventDefault();
                alert('Your new password must be at least 6 characters!');
                return;
            }
            if(new_password!=repeat_password){
                e.preventDefault();
                alert('The repeat password is not matched!');
                return;
            }
            $.ajax({
                url         : '<?php echo URL;?>admin/contact/user/ajax',
                type        : 'POST',
                dataType    :   'json',
                data        : {txt_user_id: user_id, txt_user_name: user_name, txt_old_password: old_password, txt_new_password: new_password, txt_repeat_password: repeat_password, txt_ajax_type:'change_current_password'},
                beforeSend  : function(){

                },
                success     : function(data){
                    if(data.success>=0){
                        alert(data.message);
                    }else{
                        alert('Unknown error!!!');
                    }
                }
            });
        });
    });
</script>
