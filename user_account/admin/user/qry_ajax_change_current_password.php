<?php
if(!isset($v_sval)) die();

$v_user_id = isset($_POST['txt_user_id'])?$_POST['txt_user_id']:0;
$v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:'';
$v_old_password = isset($_POST['txt_old_password'])?$_POST['txt_old_password']:'';
$v_new_password = isset($_POST['txt_new_password'])?$_POST['txt_new_password']:'';
$v_repeat_password = isset($_POST['txt_repeat_password'])?$_POST['txt_repeat_password']:'';


settype($v_user_id, 'int');
$v_current_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:0;

$arr_return = array(
    'success'=>0,
    'message'=>'The new password has been saved!'
);

if($v_current_user_id==$v_user_id && $v_user_id>0){
    if($v_old_password!=''){
        if(strlen($v_new_password)>=6 && $v_new_password==$v_repeat_password){
            $v_row = $cls_tb_user->select_one(array('user_id'=>$v_user_id));
            if($v_row==1){
                if($v_user_name==$cls_tb_user->get_user_name()){
                    if(md5($v_old_password)==$cls_tb_user->get_user_password()){
                        $v_new_password = md5($v_new_password);
                        $v_result = $cls_tb_user->update_field('user_password', $v_new_password, array('user_id'=>$v_user_id));
                        if(! $v_result){
                            $arr_return['success'] = 7;
                            $arr_return['message'] = 'The new password is not changed! Try again!';
                        }
                    }else{
                        $arr_return['success'] = 6;
                        $arr_return['message'] = 'The current password is not matched!';
                    }
                }else{
                    $arr_return['success'] = 5;
                    $arr_return['message'] = 'The user_name is not matched!';
                }
            }else{
                $arr_return['success'] = 4;
                $arr_return['message'] = 'The user is not found!';
            }
        } else{
            $arr_return['success'] = 3;
            $arr_return['message'] = 'The new password is short or the repeat password is not matched';
        }
    }else{
        $arr_return['success'] = 2;
        $arr_return['message'] = 'Old password is empty';
    }
}else{
    $arr_return['success'] = 1;
    $arr_return['message'] = 'Not match user_id';
}

header('Content-Type:application/json');
echo json_encode($arr_return);
