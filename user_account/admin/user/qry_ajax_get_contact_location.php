<?php if(!isset($v_sval)) die(); ?>
<?php
    $v_contact_id = isset($_POST['txt_contact_id'])?$_POST['txt_contact_id']:0;
    settype($v_contact_id,"int");
    $arr_contact_location[] = array("location_name"=>"------","location_id"=>0);
    $arr_return = array("error"=>0,"message"=>"","location_contact"=>"","location_id"=>0,"count"=>0);
    $v_location_id = 0;
    if($v_contact_id > 0){
        $v_location_id = $cls_tb_contact->select_scalar("location_id",array("contact_id"=>$v_contact_id));
        settype($v_location_id,"int");
        $v_count = $cls_tb_location->count(array("location_id"=>$v_location_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])));
        if($v_count != 1){
            $arr_return['error'] = 1;
            $arr_return['message'] = "Location not opened/planned.Please check location and try again";
        }
        else{
            $arr_return['count'] = 1;
            $v_location_name = $cls_tb_location->select_scalar("location_name",array("location_id"=>$v_location_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])));
            $arr_contact_location[] = array("location_id"=>$v_location_id,"location_name"=>$v_location_name);
        }
    }
    $arr_return['location_id'] = $v_location_id;
    $arr_return['location_contact'] = $arr_contact_location;
    echo json_encode($arr_return);
?>