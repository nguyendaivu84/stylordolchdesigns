<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
    $(document).ready(function(){
        $("input#btn_submit_tb_help").click(function(e){
            if(!validator.validate()){
                e.preventDefault();
                if(tab_strip.select().index()!=0) tab_strip.select(0);
                return false;
            }
            var company_id = $("select#txt_company_id").val();
            $("input#txt_hidden_company_id").val(company_id);
            return true;
        });
        var editor = $('textarea#txt_help_content').kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "formatBlock",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "viewHtml"
            ],
            encoded: false,
            imageBrowser: {
                path:"/upload/images/",
                dataType:'json',
                transport: {
                    read: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'list_images'}
                    },
                    destroy: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'delete_image_folder'}
                    },
                    create: {
                        url: "<?php echo URL.$v_admin_key.'/ajax';?>",
                        type: "POST",
                        dataType:'json',
                        data: {txt_ajax_type: 'create_image_folder'}
                    },
                    uploadUrl: "<?php echo URL.$v_admin_key.'/'.$v_help_id;?>/upload-image",
                    thumbnailUrl: "<?php echo URL.$v_admin_key.'/'.$v_help_id;?>/thumb-image"
                    //imageUrl: "/service/ImageBrowser/Image?path={0}"
//                    imageUrl: "{0}"
                }
            }
        }).data("kendoEditor");
        $('select#txt_help_module').width(300).kendoDropDownList();
        $('select#txt_help_type').width(300).kendoDropDownList();
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("span.tooltips").kendoTooltip({
            filter: 'a',
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        var validator = $("div.information").kendoValidator().data("kendoValidator");
    });
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Help<?php echo $v_help_id>0?': "'.$v_help_title.'"':''?></h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                    <div id="div_select">
                        <form id="frm_company_id" method="post">
                            Company: <select id="txt_company_id" name="txt_company_id">
                                <option value="0" selected="selected">-------</option>
                                <?php
                                echo $v_dsp_company_option;
                                ?>
                            </select>
                        </form>
                    </div>
                </div>

                <form id="frm_tb_email_templates" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_help_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_help_id" name="txt_help_id" value="<?php echo $v_help_id;?>" />
                    <input type="hidden" id="txt_hidden_company_id" name="txt_hidden_company_id" value="<?php echo $v_company_id;?>" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                            <li>Content</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Help Title</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_help_title" name="txt_help_title" value="<?php echo isset($v_help_title)?htmlspecialchars($v_help_title):'';?>" required />
                                        <input type="hidden" id="txt_hidden_email_key" name="txt_hidden_email_key" value="<?php echo $v_help_title!=''?'Y':'N';?>"  required />
                                        <label id="lbl_email_key" class="k-required">(*)</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Help Module</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_help_module" name="txt_help_module">
                                            <option value=0>-------</option>
                                            <?php echo $cls_tb_module->draw_tree_module_option(0, '-', $v_help_module);//echo $cls_settings->draw_option_by_id('email_type',0, 0);?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Type</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_help_type" name="txt_help_type">
                                            <?php echo $cls_tb_setting->draw_option_by_key("help_type",0,$v_help_type); ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left"><label><input type="checkbox" id="txt_help_status" name="txt_help_status" value="<?php echo $v_help_status;?>"<?php echo $v_help_status==0?' checked="checked"':'';?> /> Active?</label></td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Description</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <textarea class="k-textbox" style="width: 500px; height: 200px; padding:5px;"id="txt_help_description" name="txt_help_description"><?php echo $v_help_description;?></textarea></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="content div_details">
                            <textarea id="txt_help_content" name="txt_help_content" style="width:90%; height:300px; padding:5px">
                                <?php echo $v_help_content;?>
                            </textarea>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if($v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="submit" id="btn_submit_tb_help" name="btn_submit_tb_help" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
