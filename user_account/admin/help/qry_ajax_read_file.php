<?php if(!isset($v_sval)) die();?>
<?php
$arr_image_file = array();
$v_dir = new DirectoryIterator("upload/images/");
foreach ($v_dir as $v_fileinfo) {
    $v_file = (string)$v_fileinfo->getFilename();
    if($v_file!= '.' || $v_file!= '..'){
        $arr_image_file [] = array('name'=>$v_file,'size'=>$v_fileinfo->getSize(),'type'=>$v_fileinfo->getType());
    }
}
$arr_temp = array();
for($i=0;$i<count($arr_image_file);$i++){
    if(strlen($arr_image_file[$i]['name'])>2) {
        $arr_temp [] = array('name'=>URL.'upload/images/'.$arr_image_file[$i]['name'],'type'=>$arr_image_file[$i]['type'],'size'=>$arr_image_file[$i]['size']);
    }
}
echo json_encode($arr_temp);
?>