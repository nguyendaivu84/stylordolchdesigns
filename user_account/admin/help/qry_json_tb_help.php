<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_title = isset($_POST['txt_search_title'])?$_POST['txt_search_title']:'';

settype($v_company_id, 'int');
if($v_company_id > 0){
    $arr_where_clause['company_id'] = $v_company_id;
    $_SESSION['ss_last_company_id'] = $v_company_id;
}
if($v_search_title!='') $arr_where_clause['po_number'] = new MongoRegex('/'.$v_search_title.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        if($arr_temp[$i]['field']=='status') $arr_sort = array("help_status"=>$arr_temp[$i]['dir']=='asc'?1:-1);
        else $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_help_redirect']) && $_SESSION['ss_tb_help_redirect']==1){
    if(isset($_SESSION['ss_tb_help_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_help_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_help_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_help_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_help_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_help->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_help_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_help_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_help_page'] = $v_page;
$_SESSION['ss_tb_help_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("help_title"=>1);
$arr_tb_contact = $cls_tb_help->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$arr_company = array();
$arr_location = array();
foreach($arr_tb_contact as $arr){
    $v_help_id = isset($arr['help_id'])?$arr['help_id']:0;
    $v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
    $v_help_title = isset($arr['help_title'])?$arr['help_title']:'';
    $v_help_module = isset($arr['help_module'])?$arr['help_module']:'';
    settype($v_help_module,"int");
    if($v_help_module>0)
        $v_help_module = $cls_tb_module->select_scalar("module_menu",array("module_id"=>(int)$v_help_module));
    else $v_help_module = 'N/A';
    $v_help_description = isset($arr['help_description'])?$arr['help_description']:'';
    $v_help_type = isset($arr['help_type'])?$arr['help_type']:'';
    $v_help_status = isset($arr['help_status'])?$arr['help_status']:'';
    $v_help_status = $v_help_status==0?"Active":"Inactive";
    if(!isset($arr_company[$v_company_id]) && $v_company_id!=0) $arr_company[$v_company_id] = $cls_tb_company->select_scalar('company_name', array('company_id'=>$v_company_id));
    else if($v_company_id==0) $arr_company[$v_company_id] = 'All company';
    $v_company_id = $arr_company[$v_company_id];

    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'help_id' => $v_help_id,
        'company_name' => $v_company_id,
        'help_title' => $v_help_title,
        'help_module' => $v_help_module,
        'help_description' => $v_help_description,
        'help_status' => $v_help_status,
        'help_type' => $v_help_type
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_help'=>$arr_ret_data);
echo json_encode($arr_return);
?>