<?php if(!isset($v_sval)) die();?>
<?php
$v_help_keyword = '';
$v_error_message = '';
$v_mongo_id = NULL;
$v_help_id = 0;
$v_help_status = 0;
$v_help_title = '';
$v_help_description = '';
$v_help_module = 0;
$v_help_content = '';
$v_help_type = '';
$v_company_id = 0;
add_class("cls_settings");
$cls_tb_setting = new cls_settings($db);
if(isset($_POST['btn_submit_tb_help'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_help->set_mongo_id($v_mongo_id);
    $v_help_id = isset($_POST['txt_help_id'])?$_POST['txt_help_id']:$v_help_id;
    if(is_null($v_mongo_id)){
        $v_help_id = $cls_tb_help->select_next('help_id');
    }
    $v_help_id = (int) $v_help_id;
    $cls_tb_help->set_help_id($v_help_id);

    $v_help_title = isset($_POST['txt_help_title'])?$_POST['txt_help_title']:$v_help_title;
    $v_help_title = trim($v_help_title);
    if($v_help_title=='') $v_error_message .= '[Help Title] is empty!<br />';
    $cls_tb_help->set_help_title($v_help_title);

    $v_help_status = isset($_POST['txt_help_status'])?0:1;
    $v_help_status = (int) $v_help_status;
    $cls_tb_help->set_help_status($v_help_status);

    $v_help_description = isset($_POST['txt_help_description'])?$_POST['txt_help_description']:$v_help_description;
    $v_help_description = trim($v_help_description);
    $cls_tb_help->set_help_description($v_help_description);

    $v_help_module = isset($_POST['txt_help_module'])?$_POST['txt_help_module']:$v_help_module;
    $v_help_module = trim($v_help_module);
    if($v_help_module=='') $v_error_message .= '[Help Module] is empty!<br />';
    settype($v_help_module,"int");
    $cls_tb_help->set_help_module($v_help_module);

    $v_help_content = isset($_POST['txt_help_content'])?$_POST['txt_help_content']:$v_help_content;
    $v_help_content = trim($v_help_content);
    if($v_help_content=='') $v_error_message .= '[Help Content] is empty!<br />';
    $cls_tb_help->set_help_content($v_help_content);
    $v_help_type = isset($_POST['txt_help_type'])?$_POST['txt_help_type']:$v_help_type;
    //if($v_help_type==0) $v_error_message .= '[Help Type] is empty!<br />';
    $cls_tb_help->set_help_type($v_help_type);


    $v_help_keyword = isset($_POST['txt_help_keyword'])?$_POST['txt_help_keyword']:$v_help_keyword;
    $cls_tb_help->set_help_keyword($v_help_keyword);

    $v_company_id = isset($_POST['txt_hidden_company_id'])?$_POST['txt_hidden_company_id']:$v_company_id;
    settype($v_company_id,"int");
    $cls_tb_help->set_company_id($v_company_id);

    //die("$v_company_id");
    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_help->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_help->update(array('_id' => $v_mongo_id));
        }
        if($v_result){
            $_SESSION['ss_tb_help_redirect'] = 1;
            redir(URL.$v_admin_key);
        }
    }
}else{
    $v_help_id= isset($_GET['id'])?$_GET['id']:'0';
    settype($v_help_id,'int');
    if($v_help_id>0){
        $v_row = $cls_tb_help->select_one(array('help_id' => $v_help_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_help->get_mongo_id();
            $v_help_id = $cls_tb_help->get_help_id();
            $v_help_title = $cls_tb_help->get_help_title();
            $v_help_status = $cls_tb_help->get_help_status();
            settype($v_help_status,"int");
            $v_help_description = $cls_tb_help->get_help_description();
            $v_help_content = $cls_tb_help->get_help_content();
            $v_help_module = $cls_tb_help->get_help_module();
            $v_help_type = $cls_tb_help->get_help_type();
            $v_company_id = $cls_tb_help->get_company_id();
            $v_help_keyword = $cls_tb_help->get_help_keyword();
            settype($v_help_module,"int");
        }
    }
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company);
?>