<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_company_id = $_SESSION['company_id'];
$v_search_color_name = isset($_POST['txt_search_color_name'])?$_POST['txt_search_color_name']:'';
settype($v_company_id, 'int');
$v_search_color_name = htmlspecialchars_decode($v_search_color_name);
if($v_search_color_name!='') $arr_where_clause['color_name'] = new MongoRegex('/'.$v_search_color_name.'/i');

//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_color_redirect']) && $_SESSION['ss_tb_color_redirect']==1){
    if(isset($_SESSION['ss_tb_color_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_color_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_color_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_color_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_color_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_color->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_color_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_color_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_color_page'] = $v_page;
$_SESSION['ss_tb_color_quick_search'] = $v_quick_search;
//End pagination
if(is_null($arr_sort) || count($arr_sort)<=0) $arr_sort = array("color_name"=>1);
$arr_tb_material = $cls_tb_color->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_material as $arr){
    $v_mongo_id = $cls_tb_color->get_mongo_id();
    $v_color_id = isset($arr['color_id'])?$arr['color_id']:0;
    $v_color_name = isset($arr['color_name'])?$arr['color_name']:'0';
    $v_color_code_hex = isset($arr['color_code_hex'])?$arr['color_code_hex']:'';
    $v_color_code = isset($arr['color_code'])?$arr['color_code']:'';
    $v_color_status = isset($arr['color_status'])?$arr['color_status']:0;
    settype($v_color_status, 'int');
    $v_color_status = $v_color_status==0?'Active':'Inactive';

    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'color_id' => $v_color_id,
        'color_name' => $v_color_name,
        'color_code_hex' => $v_color_code_hex,
        'color_code' => $v_color_code,
        'color_status' => $v_color_status
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_color'=>$arr_ret_data);
echo json_encode($arr_return);
?>