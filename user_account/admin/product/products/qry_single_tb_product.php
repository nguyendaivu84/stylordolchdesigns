<?php if(!isset($v_sval)) die();?>
<?php
$v_company_code = '';
$v_is_out_source = 0;
$v_vendor_id = 0;
$dsp_vendor_company = '';
$v_extra_price = 1;
$v_error_message = '';
$v_mongo_id = NULL;
$v_product_id = 0;
$v_product_sku = '';
$v_short_description = '';
$v_long_description = '';
$v_product_detail = '';
$v_size_option = 0;
$arr_product_tag  = array();
$arr_template  = array();
$v_image_option = 0;
$v_image_choose = 0;
$v_print_type = 0;
$v_num_images = 1;
$v_package_type = 0;
$v_package_quantity = 1;
$arr_package_content = array();
$arr_map_content = array();
$v_allow_single = 1;
$v_image_file = '';
$v_image_desc='';
$v_saved_dir='';
$arr_material = array();
$v_text_option = 0;
$arr_text = array();
$v_sold_by = 0;
$v_default_price = 0;
$v_product_status = 0;
$v_company_id = $_SESSION['company_id'];
$v_company_id = $_SESSION['ss_last_company_id'];
$v_product_company_id = 0;
$v_location_id = 0;
$v_user_name = '';
$v_user_type = 0;
$v_product_threshold_group_id = 0;
$v_product_threshold = -1;
$v_excluded_location = '';
$v_file_hd = '';
//$v_tmp_package_type = 0;
$v_page = isset($_REQUEST['txt_page']) ? $_REQUEST['txt_page'] : 1;
$v_date_created = date('Y-m-d H:i:s');
$v_edit = false;
$v_total_product_images  = 0;
add_class('clsupload');
$cls_upload = new clsupload;
$v_new_product = true;
add_class('cls_tb_product_images');
$cls_product_images = new cls_tb_product_images($db, LOG_DIR);

add_class('cls_tb_product');
$cls_tb_product = new cls_tb_product($db, LOG_DIR);

if(isset($_POST['btn_submit_tb_product'])){
    $v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
    if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
    $cls_tb_product->set_mongo_id($v_mongo_id);
    $v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:$v_product_id;
    $v_product_id = (int) $v_product_id;
    $v_tmp_product_id = $v_product_id;

    if(is_null($v_mongo_id)){
        $v_product_id = $cls_tb_product->select_next('product_id');
        $v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
        $v_user_type = isset($arr_user['user_type'])?$arr_user['user_type']:0;
        settype($v_user_type, 'int');
        $v_date_created = time();
    }else{
        $v_row = $cls_tb_product->select_one(array('_id'=>$v_mongo_id));
        if($v_row==1){
            $arr_package_content = $cls_tb_product->get_package_content();
            $arr_map_content = $cls_tb_product->get_map_content();
            $v_product_id = $cls_tb_product->get_product_id();
            $v_user_name = $cls_tb_product->get_user_name();
            $v_user_type = $cls_tb_product->get_user_type();
            $v_tmp_package_type = $cls_tb_product->get_package_type();
            $v_date_created = $cls_tb_product->get_date_created();
        }
    }

    if($v_product_id<0) $v_error_message .= 'Product Id is negative!<br />';
    $cls_tb_product->set_product_id($v_product_id);
    $v_product_sku = isset($_POST['txt_product_sku'])?$_POST['txt_product_sku']:$v_product_sku;
    $v_product_sku = trim($v_product_sku);
    if($v_product_sku=='')
        $v_error_message .= 'Product Sku is empty!<br />';
    else{
        if($cls_tb_product->count(array('product_sku'=>$v_product_sku, 'product_id'=>array('$ne'=>$v_product_id)))>0) $v_error_message.='+ Duplicate Product Sku<br />';
    }
    $cls_tb_product->set_product_sku($v_product_sku);

    $v_is_out_source = isset($_POST['txt_out_source'])?$_POST['txt_out_source']:$v_is_out_source;
    $v_vendor_id = isset($_POST['txt_vendor_id'])?$_POST['txt_vendor_id']:$v_vendor_id;

    $v_vendor_id = $v_is_out_source==1 ? $v_vendor_id : -1;
    $v_extra_price = isset($_POST['txt_extra_price'])?$_POST['txt_extra_price']:$v_extra_price;

    $v_extra_price = (float) $v_extra_price;
    $v_vendor_id = (int) $v_vendor_id;

    $cls_tb_product->set_vendor_id($v_vendor_id);
    $cls_tb_product->set_out_source($v_is_out_source);
    $cls_tb_product->set_price_display($v_extra_price);


    $v_product_threshold_group_id = isset($_POST['txt_product_threshold_group_id'])?$_POST['txt_product_threshold_group_id']:$v_product_threshold_group_id;
    $v_product_threshold_group_id = (int) $v_product_threshold_group_id;
    $cls_tb_product->set_product_threshold_group_id($v_product_threshold_group_id);

    $v_short_description = isset($_POST['txt_short_description'])?$_POST['txt_short_description']:$v_short_description;
    $v_short_description = trim($v_short_description);
    //if($v_short_description=='') $v_error_message .= '[short_description] is empty!<br />';
    $cls_tb_product->set_short_description($v_short_description);
    $v_long_description = isset($_POST['txt_long_description'])?$_POST['txt_long_description']:$v_long_description;
    $v_long_description = trim($v_long_description);
    //if($v_long_description=='') $v_error_message .= '[long_description] is empty!<br />';
    $cls_tb_product->set_long_description($v_long_description);
    $v_product_detail = isset($_POST['txt_product_detail'])?$_POST['txt_product_detail']:$v_product_detail;
    $v_product_detail = trim($v_product_detail);
    //if($v_product_detail=='') $v_error_message .= '[product_detail] is empty!<br />';
    $cls_tb_product->set_product_detail($v_product_detail);

    $v_file_hd = isset($_POST['txt_file_hd'])?$_POST['txt_file_hd']:$v_file_hd;
    $cls_tb_product->set_file_hd($v_file_hd);
    $arr_product_tag = isset($_POST['txt_tag'])?$_POST['txt_tag']:$arr_product_tag;
    //$v_product_tag = trim($v_product_tag);
    if(!is_array($arr_product_tag)) $arr_product_tag = array();
    for($i=0; $i<count($arr_product_tag); $i++){
        $arr_product_tag[$i] = (int) $arr_product_tag[$i];
    }
    $cls_tb_product->set_tag($arr_product_tag);

    $arr_template = isset($_POST['txt_template'])?$_POST['txt_template']:$arr_template;
    if(!is_array($arr_template)) $arr_template = array();
    for($i=0; $i<count($arr_template); $i++){
        $arr_template[$i] = (int) $arr_template[$i];
    }
    $cls_tb_product->set_template($arr_template);

    $v_size_option = isset($_POST['txt_size_option'])?1:0;
    $cls_tb_product->set_size_option($v_size_option);

    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
    $v_company_id = (int) $v_company_id;
    if($v_company_id<0) $v_error_message .= 'Please choose Company!<br />';
    $cls_tb_product->set_company_id($v_company_id);

    $v_print_type = isset($_POST['txt_print_type'])?$_POST['txt_print_type']:'0';
    settype($v_print_type, 'int');
    if($v_print_type<0 || $v_print_type>2) $v_print_type = 0;
    $cls_tb_product->set_print_type($v_print_type);

    $v_image_option = isset($_POST['txt_image_option'])?1:0;
    $cls_tb_product->set_image_option($v_image_option);
    $v_image_choose = isset($_POST['txt_image_choose'])?1:0;
    $cls_tb_product->set_image_choose($v_image_choose);

    $v_num_images = isset($_POST['txt_num_images'])?$_POST['txt_num_images']:1;
    settype($v_num_images, 'int');
    if($v_num_images<1 || $v_num_images>9) $v_num_images = 1;
    $cls_tb_product->set_num_images($v_num_images);

    $v_upload_dir = "";
    $cls_upload->set_allow_overwrite(1);
    $cls_tb_company = new cls_tb_company($db,LOG_DIR);
    $cls_tb_company->select_one(array("company_id"=>$v_company_id));
    $v_company_code = $cls_tb_company->get_company_code();


    if($v_company_code==''){
        $v_company_name = $cls_tb_company->get_company_name();
        $arr_company_name = explode(' ', $v_company_name);
        for($i=0; $i<count($arr_company_name); $i++){
            $v_tmp = trim($arr_company_name[$i]);
            if($v_tmp!='')
                $v_company_code.= substr($v_tmp,0,1);
        }
        $v_company_code = remove_invalid_char($v_company_code);
        $v_row = $cls_tb_company->select_one(array('company_code'=>$v_company_code));
        if($v_row==1) $v_company_code.='_'.$v_company_id;
        $cls_tb_company->update_field('company_code', $v_company_code, array('company_id'=>$v_company_id));
    }
    $v_upload_dir = PRODUCT_IMAGE_DIR.DS.$v_company_code;
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    if($v_allow_upload) $v_upload_dir.=DS.'products';
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);
    if($v_allow_upload) $v_upload_dir.=DS.$v_product_id;
    $v_allow_upload = file_exists($v_upload_dir) || @mkdir($v_upload_dir);

    $v_has_upload = false;
    if($v_allow_upload && isset($_FILES['txt_image_file']) ){
        $cls_upload->set_destination_dir($v_upload_dir);
        $cls_upload->set_field_name('txt_image_file');
        $cls_upload->set_allow_array_extension(array('jpg', 'png'));
        $cls_upload->set_max_size(PRODUCT_UPLOAD_SIZE);
        $cls_upload->upload_process();
        $v_tmp_image_file = '';
        if($cls_upload->get_error_number()==0){
            $v_image_file = $cls_upload->get_filename();
            $v_image_desc = 'Image for current product';
            $v_width = 0;
            list($width, $height) = @getimagesize($v_upload_dir.DS.$v_image_file);
            for($i=0; $i<count($arr_product_image_size); $i++){
                $v_width = $arr_product_image_size[$i];
                if($v_width < $width){
                    images_resize_by_width($v_width, $v_upload_dir.DS.$v_image_file,$v_upload_dir.DS.$v_width.'_'.$v_image_file );
                    if($v_width==PRODUCT_IMAGE_THUMB) $v_tmp_image_file = PRODUCT_IMAGE_THUMB.'_'.$v_image_file;
                }
            }
            if($v_tmp_image_file!='') $v_image_file = $v_tmp_image_file;
            $v_has_upload = true;
            $v_saved_dir = str_replace(ROOT_DIR.DS, '', $v_upload_dir);
            $v_saved_dir = str_replace(DS, '/', $v_saved_dir).'/';
        }
    }
    if(!$v_has_upload){
        $v_image_file = isset($_POST['txt_hidden_image_file'])?$_POST['txt_hidden_image_file']:'';
        $v_image_desc = isset($_POST['txt_hidden_image_desc'])?$_POST['txt_hidden_image_desc']:'';
        $v_saved_dir = isset($_POST['txt_hidden_saved_dir'])?$_POST['txt_hidden_saved_dir']:'';
    }
    if($v_saved_dir!='' && (strrpos($v_saved_dir,'/')!==strlen($v_saved_dir)-1)) $v_saved_dir.='/';
    $cls_tb_product->set_saved_dir($v_saved_dir);
    $cls_tb_product->set_image_desc($v_image_desc);
    $cls_tb_product->set_image_file($v_image_file);

    $v_material = isset($_POST['txt_product_material'])?$_POST['txt_product_material']:'';
    $arr_material = array();
    if($v_material!=''){
        $v_material = stripcslashes($v_material);
        $arr_material = json_decode($v_material, true);
    }
    $v_location_threshold = isset($_POST['txt_hidden_location_threshold'])?$_POST['txt_hidden_location_threshold']:'';
    if($v_location_threshold!=''){
        $v_location_threshold = stripcslashes($v_location_threshold);
        $arr_location_threshold = json_decode($v_location_threshold, true);
    }else{
        $arr_location_threshold = array();
    }

    $cls_tb_product->set_material($arr_material);
    $v_text_option = isset($_POST['txt_text_option'])?1:0;
    $cls_tb_product->set_text_option($v_text_option);
    $arr_text = array();
    $arr_temp = isset($_POST['txt_product_text'])?$_POST['txt_product_text']:array();

    for($i=0; $i<count($arr_temp); $i++){
        $arr_text[] = array('text'=>$arr_temp[$i], 'color'=>'#000000', 'font-name'=>'Verdana', 'font-size'=>'14pt', 'font-bold'=>1, 'font-italic'=>0, 'left'=>0, 'top'=>0);
    }
    $cls_tb_product->set_text($arr_text);
    $v_sold_by = isset($_POST['txt_sold_by'])?$_POST['txt_sold_by']:$v_sold_by;
    settype($v_sold_by, 'int');
    if($v_sold_by!=1) $v_sold_by=0;
    $cls_tb_product->set_sold_by($v_sold_by);
    $v_default_price = isset($_POST['txt_default_price'])?$_POST['txt_default_price']:$v_default_price;
    $v_default_price = (float) $v_default_price;
    if($v_default_price<0) $v_error_message .= 'Default Price is negative!<br />';
    $cls_tb_product->set_default_price($v_default_price);
    $v_product_status = isset($_POST['txt_product_status'])?$_POST['txt_product_status']:$v_product_status;
    $v_product_status = (int) $v_product_status;
    if($v_product_status<=0) $v_error_message .= 'Please choose Product Status!<br />';
    $cls_tb_product->set_product_status($v_product_status);

    if(isset($_POST['txt_is_threshold'])){
        $v_product_threshold = isset($_POST['txt_product_threshold'])?$_POST['txt_product_threshold']:-1;
        settype($v_product_threshold, 'int');
        if($v_product_threshold<-1) $v_product_threshold = -1;
    }
    $cls_tb_product->set_product_threshold($v_product_threshold);

    $v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
    $v_location_id = (int) $v_location_id;
    $cls_tb_product->set_location_id($v_location_id);

    $arr_excluded_location = isset($_POST['txt_excluded_location'])?$_POST['txt_excluded_location']:'';
    $v_excluded_location = is_array($arr_excluded_location)?implode(',',$arr_excluded_location):'';
    $cls_tb_product->set_excluded_location($v_excluded_location);

    $v_package_type = isset($_POST['txt_package_type'])?1:0;
    $cls_tb_product->set_package_type($v_package_type);
    $v_package_quantity = 1;
    $v_allow_single = 1;

    $v_threshold_product_id = $v_product_id;
    if($v_package_type==1){
        $v_package_quantity = isset($_POST['txt_package_quantity'])?$_POST['txt_package_quantity']:'1';
        settype($v_package_quantity, 'int');
        if($v_package_quantity<=1) $v_error_message.='+ Quantity of Multiple must be greater than 1.<br />';
        $v_allow_single = isset($_POST['txt_allow_single'])?1:0;
        if($v_tmp_product_id > 0 && $v_error_message=='' && isset($v_tmp_package_type) && $v_tmp_package_type==0){
            //$v_tmp_product_id = $v_product_id;
            $v_next_product_id = $cls_tb_product->select_next('product_id');

            $cls_product = new cls_tb_product($db, LOG_DIR);
            $v_row = $cls_product->select_one(array('product_id'=>$v_tmp_product_id));
            if($v_row==1){
                $v_threshold_product_id = $v_next_product_id;

                $cls_product->set_product_id($v_next_product_id);
                $cls_product->set_package_quantity(1);
                $cls_product->set_image_option(0);
                $cls_product->set_image_choose($v_image_choose);
                $cls_product->set_size_option(0);
                $cls_product->set_text_option(0);
                //$cls_product->set_material(array());
                $cls_product->set_map_content(array());
                $cls_product->set_sold_by(1);
                $cls_product->set_package_type(1);
                $cls_product->set_allow_single(1);
                $cls_product->set_product_threshold($v_product_threshold);
                $cls_product->set_excluded_location($v_excluded_location);
                $cls_product->set_product_threshold_group_id($v_product_threshold_group_id);
                $cls_product->set_product_status(3);
                $cls_product->set_user_name($v_user_name);
                $cls_product->set_user_type($v_user_type);
                $cls_product->set_date_created(date('Y-m-d H:i:s'));
                $cls_product->set_material(array());
                $cls_product->set_text(array());

                $arr_tmp_package_content = array();
                $arr_tmp_package_content[0] = array(
                    'package_name'=>$cls_tb_product->get_product_sku()
                ,'package_type'=>0
                ,'quantity'=>$v_package_quantity
                ,'price'=>$cls_tb_product->get_default_price()
                ,'refer_id'=>$v_tmp_product_id
                ,'package_image'=>$cls_tb_product->get_image_file()
                ,'saved_dir'=>$cls_tb_product->get_saved_dir()
                ,'location_id'=>$cls_product->get_location_id()
                ,'status'=>0
                );
                $cls_product->set_package_content($arr_tmp_package_content);
                $cls_product->set_date_created(date('Y-m-d H:i:s',time()));
                $cls_product->insert();
            }
        }


        if($v_allow_single==0){
            $cls_tb_product->set_image_option(0);
            $cls_tb_product->set_size_option(0);
            $cls_tb_product->set_text_option(0);
            $cls_tb_product->set_package_quantity(1);
            $cls_tb_product->set_allow_single(0);
        }
        $cls_tb_product->set_package_type(0);
    }
    $cls_tb_product->set_package_content($arr_package_content);
    $cls_tb_product->set_allow_single($v_allow_single);
    $cls_tb_product->set_package_quantity($v_package_quantity);
    $cls_tb_product->set_map_content($arr_map_content);
    $cls_tb_product->set_user_name($v_user_name);
    $cls_tb_product->set_user_type($v_user_type);
    $cls_tb_product->set_date_created(date('Y-m-d H:i:s',$v_date_created));

    if($v_error_message==''){
        if(is_null($v_mongo_id)){
            $v_mongo_id = $cls_tb_product->insert();
            $v_result = is_object($v_mongo_id);
        }else{
            $v_result = $cls_tb_product->update(array('_id' => $v_mongo_id));
            $v_new_product = false;
        }
        if($v_result){
            if(is_array($arr_location_threshold)){
                $v_result = $cls_tb_threshold->delete(array('company_id'=>$v_company_id, 'product_id'=>$v_threshold_product_id));
                if(count($arr_location_threshold)>0){
                    for($i=0; $i<count($arr_location_threshold);$i++){
                        $v_threshold_location_id = (int) $arr_location_threshold[$i]['location_id'];
                        $v_threshold = (int) $arr_location_threshold[$i]['threshold'];
                        $v_overflow = (int) $arr_location_threshold[$i]['overflow'];
                        if($v_overflow!=1) $v_overflow = 0;
                        /*
                        $v_row = $cls_tb_threshold->select_one(array('location_id'=>$v_threshold_location_id, 'product_id'=>$v_threshold_product_id));
                        if($v_row==1){
                            $cls_tb_threshold->set_product_threshold($v_threshold);
                            $cls_tb_threshold->set_is_overflow($v_overflow);
                            $cls_tb_threshold->update();
                        }else{
                        */
                        $v_location_threshold_id = $cls_tb_threshold->select_next('location_threshold_id');
                        if($v_location_threshold_id>0 && $v_threshold_location_id>0){
                            $cls_tb_threshold->set_is_overflow($v_overflow);
                            $cls_tb_threshold->set_location_threshold_id($v_location_threshold_id);
                            $cls_tb_threshold->set_company_id($v_company_id);
                            $cls_tb_threshold->set_product_threshold($v_threshold);
                            $cls_tb_threshold->set_status(0);
                            $cls_tb_threshold->set_location_id($v_threshold_location_id);
                            $cls_tb_threshold->set_product_id($v_threshold_product_id);
                            $cls_tb_threshold->insert();
                        }
                        //}
                    }
                }
            }
            /*Upload more images */
            if(isset($_FILES['txt_more_image_file'])){
                $v_image_file = '';
                foreach($_FILES['txt_more_image_file']['tmp_name'] as $v_key => $v_tmp_name ){
                    $v_file_name = $_FILES['txt_more_image_file']['name'][$v_key];
                    $v_file_size =$_FILES['txt_more_image_file']['size'][$v_key];
                    $v_file_tmp =$_FILES['txt_more_image_file']['tmp_name'][$v_key];
                    $v_file_type=$_FILES['txt_more_image_file']['type'][$v_key];
                    /* Upload images */
                    $v_width = 0;
                    if($v_file_size >0){
                        $arr_image = explode('.', $v_file_name);
                        $v_file_type = $arr_image[sizeof($arr_image)-1];
                        $v_file_name = $arr_image[0];
                        $v_file_name = remove_invalid_char($v_file_name);
                        $v_new_file_name =$v_file_name .".".$v_file_type;

                        if(!move_uploaded_file($v_file_tmp,$v_upload_dir.DS. $v_new_file_name)){
                            $v_error_message .="Can't upload images ". $v_new_file_name.'<br>';
                        }
                        else{

                            list($width, $height) = @getimagesize($v_upload_dir.DS.$v_new_file_name);
                            for($i=0; $i<count($arr_product_image_size); $i++){
                                $v_width = $arr_product_image_size[$i];
                                if($v_width < $width){
                                    images_resize_by_width($v_width, $v_upload_dir.DS.$v_new_file_name,$v_upload_dir.DS.$v_width.'_'.$v_new_file_name );
                                    if($v_width==PRODUCT_IMAGE_THUMB) $v_tmp_image_file = PRODUCT_IMAGE_THUMB.'_'.$v_new_file_name;
                                }
                                /*Insert into tb_product_images */
                            }

                            $v_saved_dir = str_replace(ROOT_DIR.DS, '', $v_upload_dir);
                            $v_saved_dir = str_replace(DS, '/', $v_saved_dir).'/';

                            $v_product_images_id = $cls_product_images->select_next('product_images_id');
                            $cls_product_images->set_company_id($v_company_id);
                            $cls_product_images->set_location_id($v_location_id);
                            $cls_product_images->set_image_size($v_width);
                            $cls_product_images->set_image_height(isset($height)?$height:0);
                            $cls_product_images->set_image_width(isset($width)?$width:0);
                            $cls_product_images->set_product_id($v_product_id);
                            $cls_product_images->set_product_image($v_new_file_name);
                            $cls_product_images->set_saved_dir($v_saved_dir);
                            $cls_product_images->set_low_res_image($v_width.'_'.$v_new_file_name);
                            $cls_product_images->set_date_created(date('Y-m-d H:i:s', time()));
                            $cls_product_images->set_user_name($arr_user['user_name']);
                            $cls_product_images->set_user_type($arr_user['user_type']);
                            $cls_product_images->set_map_content(array());
                            $cls_product_images->set_image_type(1);
                            $cls_product_images->set_status(0);
                            $cls_product_images->set_product_images_id((int)$v_product_images_id);
                            $act_insert = $cls_product_images->insert();
                        }
                    }
                }
            }
        }
        if($v_result) {
            $_SESSION['ss_tb_product_redirect'] = 1;
            redir(URL.$v_admin_key);
        }else{
            if($v_new_product) $v_product_id = 0;
        }
    }

}else{
    $cls_tb_company = new cls_tb_company($db,LOG_DIR);
    $v_product_id = isset($_GET['id'])?$_GET['id']:0;
    settype($v_product_id, 'int') ;
    if($v_product_id>0){
        $v_row = $cls_tb_product->select_one(array('product_id' => $v_product_id));
        if($v_row == 1){
            $v_mongo_id = $cls_tb_product->get_mongo_id();
            $v_product_id = $cls_tb_product->get_product_id();
            $v_product_sku = $cls_tb_product->get_product_sku();
            $v_product_threshold_group_id = $cls_tb_product->get_product_threshold_group_id();
            $v_short_description = $cls_tb_product->get_short_description();
            $v_long_description = $cls_tb_product->get_long_description();
            $v_product_detail = $cls_tb_product->get_product_detail();
            $v_image_option = $cls_tb_product->get_image_option();
            $v_size_option = $cls_tb_product->get_size_option();
            $v_image_choose = $cls_tb_product->get_image_choose();
            $v_num_images = $cls_tb_product->get_num_images();
            $v_image_file = $cls_tb_product->get_image_file();
            $v_image_desc = $cls_tb_product->get_image_desc();
            $v_saved_dir = $cls_tb_product->get_saved_dir();
            if($v_saved_dir!='' && strrpos($v_saved_dir,'/')!==strlen($v_saved_dir)-1) $v_saved_dir.='/';
            $arr_material = $cls_tb_product->get_material();
            $v_text_option = $cls_tb_product->get_text_option();
            $arr_text = $cls_tb_product->get_text();
            $v_sold_by = $cls_tb_product->get_sold_by();
            settype($v_sold_by,'int');
            if($v_sold_by!=1) $v_sold_by = 0;
            $v_default_price = $cls_tb_product->get_default_price();
            $v_product_status = $cls_tb_product->get_product_status();
            $v_product_threshold = $cls_tb_product->get_product_threshold();
            $v_excluded_location = $cls_tb_product->get_excluded_location();
            $v_company_id = $cls_tb_product->get_company_id();
            $v_product_company_id = $cls_tb_product->get_company_id();
            $v_location_id = $cls_tb_product->get_location_id();
            $arr_product_tag = $cls_tb_product->get_tag();
            $v_package_type = $cls_tb_product->get_package_type();
            $v_package_quantity = $cls_tb_product->get_package_quantity();
            $arr_package_content = $cls_tb_product->get_package_content();
            $v_allow_single = $cls_tb_product->get_allow_single();

            $v_vendor_id = $cls_tb_product->get_vendor_id();
            $v_is_out_source = $cls_tb_product->get_out_source();
            $v_extra_price = $cls_tb_product->get_price_display();
            $v_edit = true;
            $cls_tb_company->select_one(array("company_id"=>(int)$v_company_id));
            $v_company_code = $cls_tb_company->get_company_code();
            $v_file_hd = $cls_tb_product->get_file_hd();
            $v_print_type = $cls_tb_product->get_print_type();
            $arr_template = $cls_tb_product->get_template();

            /* Total images */
            $v_total_product_images = $cls_product_images->count(array('product_id'=>(int)$v_product_id));
            if($v_image_file!='') $v_total_product_images++;

        }
    }
    add_class("cls_settings");
    $cls_settings = new cls_settings($db);
    $v_business_type = $cls_settings->get_option_id_by_key("relationship","vendor");
    $dsp_vendor_company = $cls_tb_company->draw_option('company_id', 'company_name', $v_vendor_id,array("relationship"=>$v_business_type),array("company_name"=>1));
}

/*More images */

$v_lst_image = '';
if($v_product_id>0){
    $arr_product_images = $cls_product_images->select(array('product_id'=>(int)$v_product_id ));
    foreach ($arr_product_images as $arr) {
        $v_product_images_id = isset($arr['product_images_id']) ? $arr['product_images_id'] : 0;
        $v_image = isset($arr['product_image']) ? $arr['product_image'] :'';
        $v_saved_dir = isset($arr['saved_dir']) ? $arr['saved_dir'] :'';
        if($v_saved_dir!='' && strrpos($v_saved_dir,'/')!==strlen($v_saved_dir)-1) $v_saved_dir.='/';

        if($v_is_super_admin || $v_edit_right){
            if(file_exists($v_saved_dir .DS . PRODUCT_IMAGE_THUMB.'_'. $v_image))
                $v_lst_image .= '<div class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$v_product_images_id.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'.PRODUCT_IMAGE_THUMB.'_'. $v_image. '">
                                     </div> ' ;
            else
                $v_lst_image .= '<div class="img_more_products" ><p> <img rel="images_product" product_images_id="'.$v_product_images_id.'" class="icon" src="'.URL.'images/icons/cancel.png" title="Delete image"></p>
                                        <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'. $v_image. '">
                                    </div> ' ;
        }else{
            if(file_exists($v_saved_dir .DS . PRODUCT_IMAGE_THUMB.'_'. $v_image))
                $v_lst_image .= '<div class="img_more_products" >
                                        <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'.PRODUCT_IMAGE_THUMB.'_'. $v_image. '">
                                     </div> ' ;
            else
                $v_lst_image .= '<div class="img_more_products" >
                                        <img  src="'. RESOURCE_URL . '/' .$v_company_code . '/' .'products'.'/'. $v_product_id .'/'. $v_image. '">
                                    </div> ' ;
        }
    }
}
if($v_company_id<=0){
    $v_company_id = $_SESSION['company_id'];
    $v_company_id = $_SESSION['ss_last_company_id'];
}
settype($v_company_id,"int");
if($v_product_company_id==0) $v_product_company_id = $v_company_id;
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_product_company_id,$arr_global_company,$arr_sort);
$v_dsp_material_draw = $cls_tb_material->draw_option('material_id', 'material_name', 0, array('status'=>0, '$where'=>'this.material_option!=null && this.material_option.length>0'),array('material_name'=>1));

//Combobox for locations
$v_tmp_location_id = $v_location_id;

$arr_all_location = get_array_data($cls_tb_location, 'location_id', 'location_name', $arr_all_location, array(0,'--------'), array('company_id'=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])));
$v_location_id = $v_tmp_location_id;

//Combobox for product group
$v_tmp_product_threshold_group_id = $v_product_threshold_group_id;
$arr_all_group = get_array_data($cls_tb_product_group, 'product_group_id', 'product_group_name', $v_tmp_product_threshold_group_id, array(0,'--------'), array('company_id'=>$v_product_company_id));
$v_product_threshold_group_id = $v_tmp_product_threshold_group_id;
//Multi select for product tag
//$arr_all_tag = array();
$v_tmp = 0;

//$arr_all_tag = get_array_data($cls_tb_tag,'tag_id', 'tag_name',$v_tmp, array(), array('company_id'=>$v_company_id));
$arr_all_tag = array();
//$arr_all_tag[] = array('tag_id'=>0, 'tag_name'=>'', 'tag_parent'=>0);
$arr_tag = $cls_tb_tag->select(array('company_id'=>$v_product_company_id,"tag_status"=>0));
foreach($arr_tag as $arr){
    $v_tag_id = isset($arr['tag_id'])?$arr['tag_id']:0;
    $v_tag_name = isset($arr['tag_name'])?$arr['tag_name']:'';
    $v_tag_parent = isset($arr['tag_parent'])?$arr['tag_parent']:0;
    if($v_tag_id>0 && $v_tag_name!=''){
        $arr_all_tag[] = array('tag_id'=>$v_tag_id, 'tag_name'=>$v_tag_name, 'tag_parent'=>$v_tag_parent);
    }
}
$v_dsp_excluded_location = '';
$v_dsp_location_threshold = '';
$v_dsp_script_threshold = '';
if($v_excluded_location!=''){
    $arr_excluded_location = explode(',', $v_excluded_location);
    $arr_where = array();
    $j=0;
    for($i=0; $i<count($arr_excluded_location); $i++){
        $v_tmp_location_id = (int) $arr_excluded_location[$i];
        if($v_tmp_location_id>0){
            $arr_where[$j] = array('location_id'=>$v_tmp_location_id);
            $j++;
        }
    }
    if($j<=1)
        $arr_where_clause = $arr_where[0];
    else
        $arr_where_clause = array('$or'=>$arr_where);
    //print_r($arr_where_clause);
    $arr_location = $cls_tb_location->select($arr_where_clause, array('location_number'=>1));
    foreach($arr_location as $arr){
        $v_ex_location_id = $arr['location_id'];
        $v_ex_location_number = $arr['location_number'];
        $v_ex_location_name = $arr['location_name'];
        $v_ex_location_name = pad_left($v_ex_location_number, 10, '&nbsp;').'  |  '.$v_ex_location_name;
        $v_dsp_excluded_location .= '<option value="'.$v_ex_location_id.'" selected="selected">'.$v_ex_location_name.'</option>';
    }
}

$arr_threshold = $cls_tb_threshold->select(array('product_id'=>$v_product_id, 'company_id'=>$v_product_company_id));
$i=0;
foreach($arr_threshold as $arr){
    $v_threshold_location_id = $arr['location_id'];
    $v_threshold = $arr['product_threshold'];
    $v_overflow = $arr['is_overflow'];
    $v_threshold_location_name = $cls_tb_location->select_scalar('location_name', array('location_id'=>$v_threshold_location_id));

    $v_dsp_location_threshold .= '<option value="'.$v_threshold_location_id.'"'.($v_overflow==1?' selected="selected"':'').'>['.$v_threshold.'] '.$v_threshold_location_name.'</option>';
    $v_dsp_script_threshold .= 'list_threshold['.$i++.'] = new Threshold('.$v_threshold_location_id.',"'.$v_threshold_location_name.'",'.$v_threshold.','.$v_overflow.')'."\n";
}


$v_dsp_product_status_draw = $cls_settings->draw_option_by_id('product_status', 1, $v_product_status);

//$v_dsp_size_unit_draw = $cls_settings->draw_option_by_key('size_unit', 0, '');
$v_dsp_size_unit_draw = '';
$arr_size_unit = array();
$arr_option = $cls_settings->select_scalar('option', array('setting_name'=>'size_unit'));
for($i=0; $i<count($arr_option); $i++){
    $v_dsp_size_unit_draw .= '<option value="'.$arr_option[$i]['key'].'">'.$arr_option[$i]['name'].'</option>';
    $arr_size_unit[] = array('unit_key'=>$arr_option[$i]['key'], 'unit_name'=>$arr_option[$i]['name']);
}

$v_dsp_sold_by = $cls_settings->draw_option_by_id('sold_by',0,$v_sold_by);

$v_dsp_size_list = '';
$v_dsp_size_script = '';
$v_dsp_size_option = '';
$v_dsp_material_list = '';
$v_dsp_material_script = '';
$v_dsp_text_list = '';
$v_image_url = '';
$v_text_count = 0;

$arr_data_material = array();
for($i=0; $i<count($arr_material); $i++){
    $id = isset($arr_material[$i]['id'])?$arr_material[$i]['id']:0;
    $m = isset($arr_material[$i]['name'])?$arr_material[$i]['name']:'';
    $w = isset($arr_material[$i]['width'])?$arr_material[$i]['width']:0;
    $l = isset($arr_material[$i]['length'])?$arr_material[$i]['length']:0;
    $us = isset($arr_material[$i]['usize'])?$arr_material[$i]['usize']:'in';
    $t = isset($arr_material[$i]['thick'])?$arr_material[$i]['thick']:0;
    $c = isset($arr_material[$i]['color'])?$arr_material[$i]['color']:'';
    $ut = isset($arr_material[$i]['uthick'])?$arr_material[$i]['uthick']:'mm';
    $p = isset($arr_material[$i]['price'])?$arr_material[$i]['price']:0;
    $size = isset($arr_material[$i]['size'])?$arr_material[$i]['size']:0;
    $sided = isset($arr_material[$i]['sided'])?$arr_material[$i]['sided']:0;
    settype($id, 'int');
    settype($w, 'float');
    settype($l, 'float');
    settype($t, 'float');
    settype($p, 'float');
    settype($size, 'int');
    settype($sided, 'int');
    $m = addslashes($m);
    $c = addslashes($c);

    $arr_data_material[] = array(
        'rowid'=>($i+1)
    ,'id'=>$id
    ,'name'=>$m
    ,'width'=>$w
    ,'length'=>$l
    ,'usize'=>$us
    ,'thick'=>$t
    ,'uthick'=>$ut
    ,'size'=>$size==1
    ,'sided'=>$sided==1
    ,'color'=>$c
    ,'price'=>$p
    );


    $v_dsp_material_script.="\r\nmaterial.push(new Material({$id},'{$m}','{$c}',{$w},{$l},'{$us}',{$t}, '{$ut}',{$sided}, {$p}, {$size}));";


}
for($i=0; $i<count($arr_text);$i++){
    $v_dsp_text_list .= '<p text="'.$i.'" class="one_text">';
    $v_dsp_text_list .= '<input data-text="'.$i.'" class="text_css k-textbox" size="50" type="text" id="txt_product_text" name="txt_product_text[]" value="'.$arr_text[$i]['text'].'" />';
    if($i>0){
        $v_dsp_text_list .= '<img class="img_action" data-flag="text" data-text="'.$i.'" style="cursor:pointer" src="images/icons/delete.png" />';
    }
    $v_dsp_text_list .= '</p>';
}
$v_text_count = $i;

if($v_edit){
    $v_product_dir = PRODUCT_IMAGE_DIR.DS.$v_company_code.DS.'products'.DS.$v_product_id.DS;
    $v_product_file = PRODUCT_IMAGE_DIR.DS.$v_company_code.DS.'products'.DS.$v_product_id.DS.$v_image_file;
    if(file_exists($v_product_file)){
        $v_src_url =URL.'resources/'.$v_company_code.'/products/'.$v_image_file;
        $v_product_dir_thumb = PRODUCT_IMAGE_DIR.DS.$v_company_code.DS.'products'.DS. $v_product_id .'/'.PRODUCT_IMAGE_THUMB .'_'.$v_image_file;

        if(file_exists($v_product_dir_thumb))
            $v_image_url = '<img src="'.URL.'resources/'.$v_company_code.'/products/'.$v_product_id .'/'. PRODUCT_IMAGE_THUMB .'_'.$v_image_file.'" title="'.$v_image_desc.'" />';
        else
            $v_image_url = '<img src="'.URL.'resources/'.$v_company_code.'/products/'.$v_product_id .'/'.$v_image_file.'" title="'.$v_image_desc.'" />';

    }
}else{
    $v_dsp_text_list .= '<p text="0" class="one_text">';
    $v_dsp_text_list .= '<input text="0" class="text_css k-textbox" size="50" type="text" id="txt_product_text" name="txt_product_text[]" value="" />';
    $v_dsp_text_list .= '</p>';
    $v_text_count=1;
}
$v_content = '';
$v_title = '';
add_class("cls_tb_help");
$cls_tb_help = new cls_tb_help($db);
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$v_module_id = $cls_tb_module->select_scalar("module_id",array("module_menu"=>"manage_product"));
$v_row = $cls_tb_help->select_one(array("help_status"=>0,"help_module"=>(int)$v_module_id,"help_type"=>"admin_page"));
if($v_row == 1){
    $v_content = $cls_tb_help->get_help_content();
    $v_title = $cls_tb_help->get_help_title();
}
else{
    $v_content = "Help on this topic is not yet avaible";
    $v_title = "Help on product";
}
?>