<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
var material_grid;
var duplicate_material = false;
$(document).ready(function(){
    windo_help = $('div#help_window');
    $('li#icon_help').bind("click", function() {
        if (!windo_help.data("kendoWindow")) {
            windo_help.kendoWindow({
                width: "990px",
                height: "500px",
                actions: ["Maximize", "Close"],
                modal: true,
                title: "<?php echo $v_title; ?>"
            });
        }
        windo_help.data("kendoWindow").center().open();
    });
    var is_out_source = '<?php echo $v_is_out_source; ?>';
    if(is_out_source <=0) $(".vendor_id").hide();
    $("input#btn_submit_tb_product,input#btn_submit_tb_product2").click(function(e){
        var company_id = $("select#txt_company_id").val();
        company_id = parseInt(company_id, 10);
        if(isNaN(company_id)||company_id<0) company_id = 0;
        if(company_id==0){
            e.preventDefault();
            alert('Please, choose company first!');
            $("select#txt_company_id").focus();
            return false;
        }
        if(!validator.validate()){
            e.preventDefault();
            if(tab_strip.select().index()!=0) tab_strip.select(0);
            return false;
        }
        var material = material_grid.dataSource.data();
        if(material.length <= 0){
            alert("Please input material to continue!");
           /*
            $('li[title=menu]').removeClass('k-state-active"');
            $('div.div_details').hide();
            $('div.material').addClass('k-item k-state-default k-tab-on-top k-state-active"');
            $('div.material').show();
            */
            return false;
        }
        duplicate_material = check_duplicate_data_row();
        if(duplicate_material){
            alert("The are maybe more than one duplicate material. Please remove them and try submit again!");
            if(tab_strip.select().index()!=2) tab_strip.select(2);
            return false;
        }
        var a_material = [];
        for(var i=0; i<material.length; i++){
            //if(material[i].status==0){
                var one = {
                    id: material[i].id,
                    name: material[i].name,
                    color: material[i].color,
                    width: material[i].width,
                    length:material[i].length,
                    usize:material[i].usize,
                    thick: material[i].thick,
                    uthick:material[i].uthick,
                    sided:material[i].sided?1:0,
                    price:material[i].price,
                    size:material[i].size?1:0,
                    status:0
                };

                //var t = new Material(material[i].id, material[i].name, material[i].color, material[i].width, material[i].length, material[i].usize, material[i].thick, material[i].uthick,material[i].sided, material[i].price, material[i].size);
                a_material.push(one);
            //}
        }
        $('input#txt_product_material').val(JSON.stringify(a_material));

        var threshold = '';
        if(list_threshold.length>0){
            $('select#txt_location_threshold option').each(function(index, element){
                if($(this).prop('selected')){
                    list_threshold[index].overflow = 1;
                }else{
                    list_threshold[index].overflow = 0;
                }
            });
            threshold = JSON.stringify(list_threshold);
        }
        $('input#txt_hidden_location_threshold').val(threshold);
        return true;
    });
    $("#txt_out_source").on("change",function(){
        var check = $(this).is(":checked");
        if(check == true) $(".vendor_id").show();
        else $(".vendor_id").hide();
    });
    $("select#txt_vendor_id").width(250).kendoComboBox({
        dataTextField:'vendor_name',
        dataValueField:'vendor_id'
    });
    $("img[rel=images_product]").click(function(){
        if(confirm("Do you want to delete this image ? ")){
            var product_images_id = $(this).attr('product_images_id');
            var product_id = <?php echo $v_product_id; ?>;
            $.ajax({
                url	    :'<?php echo URL.$v_admin_key ?>/ajax',
                type	:'POST',
                data	:{txt_ajax_type:'product_images',txt_action:'delete-images', txt_product_id:product_id, txt_product_images_id:product_images_id,txt_company_code:'<?php echo $v_company_code; ?>'},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    $('td#td_more_images').html('Loading..');
                    if(ret.error==0)
                        $('td#td_more_images').html(ret.data);
                    else
                        alert(ret.message);
                }
            });
        }
    });
    $("#txt_extra_price").kendoNumericTextBox({
        min: 1,
        step: 0.01
    });
    var validator = $('div.information').kendoValidator().data("kendoValidator");


    var tags = <?php echo json_encode($arr_all_tag);?>;
    var locations = <?php echo json_encode($arr_all_location);?>;
	var groups = <?php echo json_encode($arr_all_group);?>;

    var multi_template = $('select#txt_template').width(500).kendoMultiSelect({
        dataTextField: "template_name",
        dataValueField: "template_id",
        // define custom template
        itemTemplate: '<div style="height:auto; overflow: auto; clear:both;"><img style="width:150px; float:left; margin-right: 5px" src=\"<?php echo URL;?>${data.template_image}\" alt=\"${data.template_name}\" />' +
            '<h3>${ data.template_name }</h3>' +
            '<p>#= data.description #</p></div>',
        tagTemplate:  '<img style=\"width: auto;height: 18px;margin-right: 5px;vertical-align: top\" src=\"<?php echo URL;?>${data.template_image}\" alt=\"${data.description}\" />' +
            '#: data.template_name #',
        dataSource:{
            transport:{
                read:{
                    url     :'<?php echo URL.$v_admin_key;?>/ajax',
                    dataType:'json',
                    type    : 'POST',
                    data    :   {txt_session_id: '<?php echo session_id();?>', txt_company_id: '<?php echo $v_company_id;?>', txt_ajax_type: 'load_template'}
                    ,beforeSend: function(){
                    }
                }
            }
        }
    }).data("kendoMultiSelect");
    multi_template.value(<?php echo json_encode($arr_template);?>);


    $("#txt_tag").width(300).kendoMultiSelect({
        dataSource: tags,
        dataTextField: "tag_name",
        dataValueField: "tag_id",
        itemTemplate: '#= tag_parent==1?"":tag_name#',
        tagTemplate: '#= tag_parent==1?"":tag_name#'
    });
    $("select#txt_location_id").width(250).kendoComboBox({
        dataSource: locations,
        dataTextField:'location_name',
        dataValueField:'location_id'
    });
	$('select#txt_product_threshold_group_id').width(200).kendoComboBox({
		dataSource: groups,
		dataValueField: "product_group_id",
		dataTextField: "product_group_name"
	});;
    var locations_data = $("select#txt_location_id").data("kendoComboBox");
    locations_data.value(<?php echo $v_location_id;?>);
    var groups_data = $("select#txt_product_threshold_group_id").data("kendoComboBox");
    groups_data.value(<?php echo $v_product_threshold_group_id;?>);

    $("select#txt_product_status").width(200).kendoComboBox();
    $("select#txt_sold_by").width(200).kendoComboBox();
    var product_tag = $("#txt_tag").data("kendoMultiSelect");
    product_tag.value(<?php echo json_encode($arr_product_tag);?>);
    var combo_company = $("select#txt_company_id").data("kendoComboBox");
    <?php if($v_disabled_company_id){?>
    combo_company.enable(false);
    <?php }?>
    $('select#txt_num_images').width(50).kendoComboBox();
    $('select#txt_size_unit').width(100).kendoComboBox();
    $('select#txt_material_id').width(250).kendoComboBox();
    var combo_material = $('select#txt_material_id').data("kendoComboBox");
    var thicks = [{"thick":0}];
    var thicks_unit = [{"unit_code":"", "unit_name":"----"}];
    var colors = [{"color_code":"", "color_name":"------"}];
    $('select#txt_thick').width(145).kendoComboBox({
        dataSource:thicks,
        dataValueField: "thick",
        dataTextField: "thick"
    });
    var combo_thicks = $('select#txt_thick').data("kendoComboBox");
    $('select#txt_thick_unit').width(145).kendoComboBox({
        dataSource:thicks_unit,
        dataValueField: "unit_code",
        dataTextField: "unit_name"
    });
    var combo_thicks_unit = $('select#txt_thick_unit').data("kendoComboBox");
    $('select#txt_color').width(100).kendoComboBox({
        dataSource:colors,
        dataValueField: "color_code",
        dataTextField: "color_name"
    });
    var combo_color = $('select#txt_color').data("kendoComboBox");
    var combo_size = $("#txt_size_width").kendoNumericTextBox({
        format: "n2",
        min: 0,
        step: 0.01
    });
    $("#txt_size_length").kendoNumericTextBox({
        format: "n2",
        min: 0,
        max: 100,
        step: 0.01
    });
    $("#txt_price").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    $("#txt_default_price").kendoNumericTextBox({
        format: "c2",
        min: 0,
        step: 0.01
    });
    $('#txt_product_threshold').kendoNumericTextBox({
       format:"n0",
        min:0,
        max:100,
        step:1
    });
    $('#txt_package_quantity').kendoNumericTextBox({
        format:"n0",
        min:0,
        max:100,
        step:1
    });
    var thresholds = $('#txt_product_threshold').data("kendoNumericTextBox");

    $('input#txt_image_file').kendoUpload({
        multiple: false
        ,select: on_select
        /*
        <?php if($v_product_id>0){?>

            ,async: {
            saveUrl: "<?php echo URL.$v_admin_key;?>/ajax",
                autoUpload: true
            }
            ,upload: on_upload
        <?php }?>
        */
    });

    $('input#txt_more_image_file').kendoUpload({
        multiple: true
    });


    <?php if($v_product_id>0){?>
    function on_upload(e){
        e.data = {txt_ajax_type: "upload_thumbnail", txt_product_id: <?php echo $v_product_id;?>};
    }
    <?php }?>
    function on_select(e){
        //alert(e.files[0].name);
    }
    $('input#txt_package_type').click(function(e) {
        var c = $(this).prop('checked');
        if(c){
            $('span#sp_multiple').css('display', '');
            var q = $('input#txt_package_quantity').val();
            q = parseInt(q, 10);
            if(isNaN(q) || q<=1) q = 10;
            $('input#txt_package_quantity').val(q);
        }else{
            $('span#sp_multiple').css('display', 'none');
        }
    });
    $('input#txt_is_threshold').click(function(e) {
        var chk = $(this).prop('checked');
        if(chk){
            var old = $('input#txt_hidden_threshold').val();
            old = parseInt(old, 10);
            if(isNaN(old) || old<0) old = 0;
            thresholds.value(old);
            thresholds.enable(true);
        }else{
            thresholds.value([]);
            thresholds.enable(false);
        }
    });

    var material_data = <?php echo json_encode($arr_data_material);?>;
    var size_unit_data = <?php echo json_encode($arr_size_unit);?>;

    material_grid = $('#material_grid').kendoGrid({
        dataSource:{
            data: material_data,
            schema:{
                model:{
                    fields:{
                        id: {type: "int", editable: false},
                        name: {type: "string", editable: false},
                        width: {type: "number",validation: { required: true, min: 0}},
                        length: {type: "number",validation: { required: true, min: 0}},
                        usize: {type: "string"},
                        thick: {type: "number", editable: false,validation: { required: true, min: 0}},
                        uthick: {type: "string", editable: false},
                        color: {type: "string", editable: false},
                        size: {type: "boolean"},
                        sided: {type: "boolean"},
                        price: {type: "number", validation: { required: true, min: 0}}
                    }
                }
            }
            ,pageSize:20
        },
        height: 300,
        scrollable: true,
        pageable: {
            input: true,
            numeric: false
        }
        ,save : function(e){
            var data = e.model;
            var field = e.values ;
            if(e.values.width)
                data.width = e.values.width;
            else if(e.values.length)
                data.length = e.values.length;
            else if(e.values.usize)
                data.usize = e.values.usize;

            var rows = check_duplicate(data);
            var row;
            if(rows.length>1){
                alert("There are "+rows.length+" rows having the same data!");
                for(var i=0; i<rows.length; i++){
                    row = material_grid.tbody.find(">tr").eq(rows[i]);
                    material_grid.select(row);
                }
            }
        }
        ,selectable: "multiple row"
        ,editable: true
        ,columns: [
            {field: "name", title:"Material", width: "100px", editable: false},
            { field: "thick", title: "Thick", width: "40px", format: "{0:n2}", editable: false, template: '<span style="float: right">#= thick #</span>' },
            { field: "uthick", title: "Thick's Unit", width: "50px", editable: false },
            { field: "color", title: "Color", width: "60px", editable: false },
            { field: "width", title: "Width", format: "{0:n2}", width: "40px", template: '<span style="float: right">#= width #</span>' },
            { field: "length", title: "Length", format: "{0:n2}", width: "40px", template: '<span style="float: right">#= length #</span>' },
            { field: "usize", title: "Size's Unit", width: "50px",editor: usize_DropDownEditor },
            { field: "size", title: "Size Option", width: "60px", template: '<p style="margin: 0; padding:0; text-align:center">#= size?"Yes":"No" #</p>' },
            { field: "sided", title: "2-sided Print", width: "60px", template: '<p style="margin: 0; padding:0; text-align:center">#= sided?"Yes":"No" #</p>' },
            { field: "price", title:"Price", width: "50px", format: "{0:c2}", template: '<span style="float: right">#= kendo.toString(price, "c2") #</span>' },
            { command: [{name:"Remove", click: remove_row, imageClass:"k-icon k-delete"}], title: "&nbsp;", width: "50px" }
        ]

    }).data("kendoGrid");
    function remove_row(e){
        e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        $("#txt_size_width").data("kendoNumericTextBox").value(dataItem.width);
        $("#txt_size_length").data("kendoNumericTextBox").value(dataItem.length);
        $("select#txt_size_unit").data("kendoComboBox").value(dataItem.usize);
        $("select#txt_material_id").data("kendoComboBox").value(dataItem.id);
        $('select#txt_material_id').trigger('change');
        $("#txt_price").data("kendoNumericTextBox").value(dataItem.price);
        material_grid.removeRow($(e.currentTarget).closest("tr"));
    }
    function check_duplicate_data_row(){
        var ret = false;
        var rows = [];
        var data = material_grid.dataSource.data();
        for(var i=0; i<data.length-1; i++){
            var row = data[i];
            rows[0] = i;
            for(var j=i+1; j<data.length; j++){
                if(row.id==data[j].id && row.color==data[j].color && row.width==data[j].width && row.length == data[j].length && row.usize == data[j].usize && row.thick==data[j].thick && row.uthick==data[j].uthick){
                    ret = true;
                    rows[1] = j;
                    j = data.length+1;
                    i = data.length+1;
                }
            }
        }
        if(rows.length>1){
            for(var i=0; i<rows.length; i++){
                row = material_grid.tbody.find(">tr").eq(rows[i]);
                material_grid.select(row);
            }
        }
        return ret;
    }
    function check_duplicate(row){
        var data = material_grid.dataSource.data();
        var ret = [];
        for(var j=0; j<data.length; j++){
            if(row.id==data[j].id && row.color==data[j].color && row.width==data[j].width && row.length == data[j].length && row.usize == data[j].usize && row.thick==data[j].thick && row.uthick==data[j].uthick){
                //alert(row.id +'=='+ data[j].id +' ---- '+ row.color+'=='+data[j].color +' ---- '+ row.width+'=='+data[j].width +' ---- '+ row.length +'=='+ data[j].length +' ---- '+ row.usize +'=='+ data[j].usize +' ---- '+ row.thick+'=='+data[j].thick +' ---- '+ row.uthick+'=='+data[j].uthick);
                ret.push(j);
            }
        }
        return ret;
    }
    function usize_DropDownEditor(container, options) {
        $('<input required data-text-field="unit_name" data-value-field="unit_key" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoDropDownList({
                autoBind: false,
                dataSource: size_unit_data
            });
    }

    var tab_strip = $("#data_single_tab").kendoTabStrip({
        animation:  {
            open: {
                effects: "fadeIn"
            }
        }
    }).data("kendoTabStrip");
    var tooltip = $("span.tooltips").kendoTooltip({
        filter: 'a',
        width: 120,
        position: "top"
    }).data("kendoTooltip");

    $('select#txt_company_id').change(function(e) {
        var $this = $(this);
        var company_id = $(this).val();
        company_id = parseInt(company_id, 10);
        if(isNaN(company_id) || company_id<0) company_id = 0;
        $.ajax({
            url : '<?php echo URL.$v_admin_key;?>/ajax',
            type    : 'POST',
            data    :   {txt_company_id: company_id, txt_ajax_type:'product_info'},
            beforeSend: function(){
                $this.prop('disabled', true);
                combo_company.enable(false);
            },
            success: function(data, type){
                var ret = $.parseJSON(data);
                if(ret.error==0){
                    var locations = ret.location;
                    var tags = ret.tag;
                    var groups = ret.group;
                    locations_data.setDataSource(locations);
                    product_tag.setDataSource(tags);
                    groups_data.setDataSource(groups);
                    locations_data.value(0);
                    product_tag.value([]);
                    groups_data.value(0);
                    $('form#frm_tb_product').find('#txt_company_id').val(company_id);//;.val($(this).val(company_id));
                }else{
                    alert(ret.message);
                }
                $this.prop('disabled', false);
                combo_company.enable(true);
            }
        });
    });

    $('input#txt_product_sku').focusout(function(){
        var v_old_product_sku = '<?php echo $v_product_sku; ?>';
        var v_new_product_sku = $.trim($(this).val());
        if((v_old_product_sku + v_new_product_sku)=='') return false;
        if(v_old_product_sku==v_new_product_sku) return false;
        if(v_new_product_sku==''){
            $(this).val('');
            $('input#txt_hidden_product_sku').val('N');
            validator.validate();
            return false;
        }
        var company_id = $('input#txt_company_id').val();
        var product_id = $('input#txt_product_id').val();
        $.ajax({
            url	    :'<?php echo URL.$v_admin_key ?>/ajax',
            type	:'POST',
            data	:{txt_ajax_type:'product_sku',txt_product_sku:$.trim(v_new_product_sku), txt_product_id:product_id, txt_company_id:company_id},
            beforeSend: function(){
                $('span#sp_product_sku').html('Checking!....');
            },

            success: function(data, type){
                var ret = $.parseJSON(data);
                $('span#sp_product_sku').html('');
                var val = ret.error==0?v_new_product_sku:'';
                $('input#txt_hidden_product_sku').val(val);
                validator.validate();
            }
        });
    });
    $('select#txt_material_id').change(function(e) {
        var id = $(this).val();
        id = parseInt(id, 10);
        if(isNaN(id)) id = 0;
        if(id<=0) return;
        var $this = $(this);
        $.ajax({
            type	: 'POST',
            url:	"<?php echo URL.$v_admin_key;?>/ajax",
            data	: {txt_material_id: id, txt_ajax_type: "material_option"},
            beforeSend: function(){
                $this.prop('disabled', true);
                combo_material.enable(false);
            },
            success: function(data, status){
                var content = $.parseJSON(data);
                if(content.error == 0){
                    var option = content.option;
                    opt = new Array();
                    for(var i=0; i<option.length;i++){
                        opt[i] = new Option(option[i].thick, option[i].unit_code, option[i].unit_name, option[i].color_code, option[i].color_name,option[i].sided);
                    }
                    var list_thick='';
                    var thicks = [];
                    var j=0;
                    for(var i=0; i<opt.length; i++){
                        if(list_thick.indexOf(opt[i].thick+',')==-1){
                            list_thick += opt[i].thick+',';
                            var one = {"thick":opt[i].thick};
                            thicks.push(one);
                            j++;
                        }
                    }
                    if(j>0){
                        combo_thicks.setDataSource(thicks);
                        combo_thicks.select(0);
                        //$('select#txt_thick').kendoComboBox().trigger("change");
                        $('select#txt_thick').trigger('change');
                    }else{
                        combo_thicks.select(-1);
                    }

                }else{
                    alert(content.message);
                }
                $this.prop('disabled', false);
                combo_material.enable(true);
            }
        });
    });

    $('select#txt_thick').change(function(e){
        var thick = $(this).val();
        thick = parseFloat(thick);
        var list_unit = '';
        var j = 0;
        var units = [];
        for(var i=0; i<opt.length; i++){
            if(thick==opt[i].thick){
                if(list_unit.indexOf(opt[i].unit_code+',')==-1){
                    list_unit += opt[i].unit_code+',';
                    var one = {"unit_code":opt[i].unit_code, "unit_name":opt[i].unit_name};
                    units.push(one);
                    j++;
                }
            }
        }
        combo_thicks_unit.setDataSource(units);
        if(j>0){
            combo_thicks_unit.select(0);
            $('select#txt_thick_unit').trigger("change");
        }else{
            combo_thicks_unit.select(-1);
        }

    });

    $('select#txt_thick_unit').change(function(e) {
        var unit_code = $(this).val();
        var thick = $('select#txt_thick').val();
        thick = parseFloat(thick);
        var list_color = '';
        //$('select#txt_color option').remove();
        var colors = [];
        var j = 0;
        for(var i=0; i<opt.length; i++){
            if(thick==opt[i].thick && unit_code==opt[i].unit_code){
                if(list_color.indexOf(opt[i].color_code+',')==-1){
                    list_color += opt[i].color_code+',';
                    //var $opt = $('<option value="'+opt[i].color_code+'">'+opt[i].color_name+'</option>');
                    //$('select#txt_color').append($opt);
                    var one = {"color_code":opt[i].color_code, "color_name":opt[i].color_name};
                    colors.push(one);
                    j++;
                }
            }
        }
        combo_color.setDataSource(colors);
        combo_color.select(j>0?0:-1);
        if(j>0){
            $('select#txt_color').trigger("change");
        }
    });
    $('select#txt_color').change(function(){
        var color = $(this).val();
        var thick = $('select#txt_thick').val();
        thick = parseInt(thick);
        var unit = $('select#txt_thick_unit').val();
        var j=-1;
        for(var i=0; i<opt.length && j==-1; i++){
            if(thick==opt[i].thick && unit==opt[i].unit_code && color==opt[i].color_code){
                j = i;
            }
        }
        if(j>=0){
            var sided = opt[j].two_sided==1;
            $('input#txt_two_sided').prop("disabled", !sided);
            if(!sided)
                $('input#txt_two_sided').prop("checked", false);
        }
    });
    function compare(m, c, w, l, us, t, ut, sd){
        var data = material_grid.dataSource.data();
        var i= 0, found = false;
        while(i<data.length && !found){
            //alert(data[i].id +'=='+ m +' -- '+ data[i].color+'=='+c +' -- '+ data[i].width +'=='+ w +' -- '+ data[i].length+'=='+l +' -- '+ data[i].thick+'=='+t +' -- '+ data[i].usize+'=='+us +' -- '+ data[i].uthick +'=='+ ut +' -- '+ data[i].sided+'=='+sd);
            found = data[i].id == m && data[i].color==c && data[i].width == w && data[i].length==l && data[i].thick==t && data[i].usize==us && data[i].uthick == ut && data[i].sided==sd;
            i++;
        }
        return found;
    }

    $('img#add_material').click(function(e){
        var mid = $('select#txt_material_id').val();
        var m = $('select#txt_material_id option:selected').text();
        mid = parseInt(mid, 10);

        var w = $('input#txt_size_width').val();
        w = parseFloat(w);
        if(isNaN(w)) w = 0;

        var l = $('input#txt_size_length').val();
        l = parseFloat(l);
        if(isNaN(l)) l = 0;

        if (l*w == 0) {
            if ((l + w) != 0) {
                alert('<?php echo $cls_tb_message->select_value('invalid_size_is_not_customizable'); ?>');
                return;
            }
        }
        var us = $('select#txt_size_unit').val();
        if (l*w != 0) {
            if (us == 'none') {
                alert('<?php echo $cls_tb_message->select_value('invaild_input_unit_of_size'); ?>');
                return;
            }
        }

        var thick = $('select#txt_thick').val();
        thick = $.trim(thick);

        var ut = $('select#txt_thick_unit').val();
        var p = $('input#txt_price').val();
        p = $.trim(p);
        if(p!='') p = parseFloat(p);
        if (l*w != 0) {
            if(isNaN(p) || p<0){
                alert('<?php echo $cls_tb_message->select_value('invalid_input_valid_value_price'); ?>');
                $('input#txt_price').focus();
                return;
            }
        }
        if(mid > 0){
            if(isNaN(p) || p<0){
                alert('<?php echo $cls_tb_message->select_value('invalid_input_valid_value_price_material'); ?>');
                $('input#txt_price').focus();
                return;
            }
        }


        current_color = $('select#txt_color').val();
        var size = $('input#txt_allow_size_option').prop('checked');
        if (size) {
            if(p=='' || isNaN(p) || p<=0){
                alert('<?php echo $cls_tb_message->select_value('invalid_input_valid_value_price_size_customizable'); ?>');
                $('input#txt_price').focus();
                return;
            }
        }
        if (w == 0 && l == 0 && !size) {
            alert('<?php echo $cls_tb_message->select_value('invalid_input_size_option'); ?>');
            $('input#txt_allow_size_option').focus();
            return;
        }
        var sd = $('input#txt_two_sided').prop("checked");


        if(!compare(mid, current_color, w, l, us, thick, ut, sd)){
            var data = material_grid.dataSource.data();
            var tmp = [];
            var one = {rowid: 1, id: mid, name: m, width: w, length: l, usize: us, thick: thick, uthick: ut, size: size, sided: sd, color: current_color, price:p};
            //alert(data.length);
            tmp.push(one);
            for(var i= 0; i<data.length; i++){
                data[i].rowid = i+2;
                tmp.push(data[i]);
            }
            material_grid.dataSource.data(tmp);
            material_grid.refresh();
        }
    });
});
var current_color = '#000000';
var count_text = <?php echo $v_text_count;?>;

function Option(thick, unit_code, unit_name, color_code, color_name, two_sided){
    thick = parseFloat(thick);
    this.thick = thick;
    this.unit_code = unit_code;
    this.unit_name = unit_name;
    this.color_code = color_code;
    this.color_name = color_name;
    this.two_sided = two_sided;
}
var opt = new Array();
function add_text(){
    var $p = $('<p text="'+count_text+'" class="one_text"></p>');
    var $t = $('<input data-text="'+count_text+'" class="text_css k-textbox" size="50" type="text" id="txt_product_text" name="txt_product_text[]" value="" />');
    var $i = $('<img class="img_action" data-flag="text" data-text="'+count_text+'" style="cursor:pointer" src="images/icons/delete.png" />');
    $i.bind('click', function(){
        $(this).parent().remove();
    });
    var $l = $('<label text="'+count_text+'" id="lbl_product_text" style="color:red;display:none;">(*)</label>');
    $p.append($t);
    $p.append($i);
    $p.append($l);
    $('div#product_text').append($p);
}
function remove_text(obj){
    var id = obj.id;
    $('img#'+id).parent().remove();
}
function Threshold(location_id, location_name, threshold, overflow){
    var val = 0;
    val = location_id;
    val = parseInt(val, 10);
    if(isNaN(val) || val<0) val = 0;
    this.location_id = val;
    this.location_name = location_name;
    val = threshold;
    val = parseInt(val, 10);
    if(isNaN(val) || val<0) val = 0;
    this.threshold = threshold;
    this.overflow = overflow==1?1:0;
}
var list_excluded = new Array();
var list_threshold = new Array();
<?php
echo $v_dsp_script_threshold;
?>

</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Product<?php if($v_product_id>0) echo ': '.$v_product_sku.' ('.$v_short_description.')';?></h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                            &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                        <div id="help_window" style="display:none">
                            <?php
                            echo $v_content ;
                            ?>
                        </div>
                    </div>

<form id="frm_tb_product" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_product_id.'/edit';?>" method="POST" enctype="multipart/form-data">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_product_id" name="txt_product_id" value="<?php echo $v_product_id;?>" />
<input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_product_company_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li title="menu" class="k-state-active">Information</li>
                        <li title="menu">Image</li>
                        <li title="menu">Material</li>
                        <li title="menu">Text</li>
                        <li title="menu">Threshold</li>
                        <li title="menu">Tag</li>
                        <li title="menu">Template</li>
                    </ul>

                    <div class="information div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                        <tr align="right" valign="top">
                                <td style="width:200px">Location</td>
                                <td style="width:1px">&nbsp;</td>
                                <td align="left">
                                    <select id="txt_location_id" name="txt_location_id">
                                    </select>
                                </td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Product Sku</td>
                                <td>&nbsp;</td>
                                <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_sku" name="txt_product_sku" value="<?php echo $v_product_sku;?>" required data-required-msg="Please input Product Sku" />
                                    <input type="hidden" style="width:0px !important; border:none" name="txt_hidden_product_sku" id="txt_hidden_product_sku" value="<?php echo $v_product_sku==''?'Y':'N';?>" required validationMessage="This Product Sku is existed!" />
                                    <span id="sp_product_sku"></span>
                                    <span class="tooltips"><a title="Product Sku is unique">&nbsp;&nbsp;&nbsp;&nbsp;</a></span>
                                    <label id="lbl_product_sku" class="k-required">(*)</label>
                        
                                </td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Short Description</td>
                                <td>&nbsp;</td>
                                <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_short_description" name="txt_short_description" value="<?php echo $v_short_description;?>" required data-required-msg="Please input Short Description" />
                                    <label id="lbl_short_description" class="k-required">(*)</label></td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Long Description</td>
                                <td>&nbsp;</td>
                                <td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_long_description" name="txt_long_description" value="<?php echo $v_long_description;?>" /> <label id="lbl_long_description" style="color:red;display:none;">(*)</label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Product Details</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <textarea class="text_css k-textbox" cols="40" style="height:80px" id="txt_product_detail" name="txt_product_detail"><?php echo $v_product_detail;?></textarea> <label id="lbl_long_description" style="color:red;display:none;">(*)</label>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Is Outsourced?</td>
                                <td>&nbsp;</td>
                                <td align="left" colspan="2"><label>
                                        <input type="checkbox" id="txt_out_source" name="txt_out_source" value="1"<?php echo $v_is_out_source==1?' checked="checked"':'';?> /></label>
                                </td>
                            </tr>
                            <tr class="vendor_id" align="right" valign="top">
                                <td>Vendor company</td>
                                <td>&nbsp;</td>
                                <td align="left" colspan="2"><label>
                                        <select name="txt_vendor_id" id="txt_vendor_id">
                                            <?php echo $dsp_vendor_company; ?>
                                        </select>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Is Multiple?</td>
                                <td>&nbsp;</td>
                                <td align="left" colspan="2"><label>
                                    <input disabled="disabled" type="checkbox" id="txt_package_type" name="txt_package_type" value="1"<?php echo $v_package_type==1?' checked="checked"':'';?> /><span style="color: red">(*) Not implemented</span> </label> &nbsp;&nbsp;&nbsp;&nbsp;<span id="sp_multiple"<?php echo $v_package_type==0?' style="display:none"':'';?>>Quantity for Multiple <input type="text" name="txt_package_quantity" size="10" id="txt_package_quantity" value="<?php echo $v_package_type==1?$v_package_quantity:'1';?>" /> <label style="color:red; display:none;" id="lbl_package_quantity">(*)</label> &nbsp;&nbsp;&nbsp;&nbsp;<label>Allow Single <input type="checkbox" id="txt_allow_single" name="txt_allow_single" disabled="disabled" value="1"<?php //echo $v_allow_single==1?' checked="checked"':'';?> /></label></span>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Sold By</td>
                                <td>&nbsp;</td>
                                <td align="left" colspan="2">
                                    <select id="txt_sold_by" name="txt_sold_by">
                                        <?php echo $v_dsp_sold_by;?>
                                    </select>
                                    <label id="lbl_sold_by" style="color:red;display:none;">(*)</label></td>
                            </tr>
                        <tr align="right" valign="top">
                                <td>Default price</td>
                                <td>&nbsp;</td>
                                <td align="left"><input type="text" id="txt_default_price" name="txt_default_price" value="<?php echo $v_default_price;?>" /> <label id="lbl_default_price" style="color:red;display:none;">(*)</label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Franchisee Factor:</td>
                                <td>&nbsp;</td>
                                <td align="left"><input type="text" id="txt_extra_price" name="txt_extra_price" value="<?php echo $v_extra_price;?>" /> <label id="lbl_default_price" style="color:red;display:none;">(*)</label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Status</td>
                                <td>&nbsp;</td>
                                <td align="left" colspan="2">
                                    <select id="txt_product_status" name="txt_product_status">
                                        <?php
                                        echo $v_dsp_product_status_draw;
                                        ?>
                                    </select>
                                    <label id="lbl_product_status" style="color:red;display:none;">(*)</label></td>
                            </tr>
                        <!--<tr align="right" valign="top">
                                <td>Tag</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <select id="txt_tag" name="txt_tag[]" multiple="multiple">
                                    </select>
                                </td>
                            </tr>-->
                        </table>
                    </div>
                    <div class="image div_details">
                        <div id="window"></div>
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width:200px">Thumbnail for Product</td>
                                <td style="width:1px">&nbsp;</td>
                                <td align="left">
                                    <input type="hidden" name="txt_hidden_image_file"  value="<?php echo $v_image_file;?>" />
                                    <input type="hidden" name="txt_hidden_image_desc"  value="<?php echo $v_image_desc;?>" />
                                    <input type="hidden" name="txt_hidden_saved_dir"  value="<?php echo $v_saved_dir;?>" />
                                    <input type="file" id="txt_image_file" name="txt_image_file" accept="image/*" />
                                </td>
                                <td rowspan="6" width="300" align="center" valign="middle">
                                    <?php
                                     echo $v_image_url!=''? $v_image_url :'';
                                    ?>
                                    &nbsp;
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Number of Images for this Product</td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <select id="txt_num_images" name="txt_num_images" disabled="disabled">
                                        <?php
                                        for($i=1; $i<=9;$i++){
                                            echo '<option value="'.$i.'"'.($i==$v_num_images?' selected="selected"':'').'>0'.$i.'</option>';
                                        }
                                        ?>
                                    </select> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <label><input disabled="disabled" type="checkbox" name="txt_image_choose" id="txt_image_choose" value="1"<?php echo $v_image_choose==1?' checked="checked"':'';?> /> Allow choose image for product<span style="color: red">(*) Not implemented</span> </label>
                                </td>
                            </tr>

                            <tr align="right" valign="top">
                                <td>Upload more images </td>
                                <td>&nbsp;</td>
                                <td align="left">
                                    <input disabled="disabled" type="file" id="txt_more_image_file" name="txt_more_image_file[]" accept="image/*" />
                                    <span style="color: red">(*) Not implemented</span>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Image Option</td>
                                <td>&nbsp;</td>
                                <td align="left"><label><input type="checkbox" id="txt_image_option" name="txt_image_option" value="1"<?php echo $v_image_option==1?' checked="checked"':'';?> /> Allow customize<span style="color: red">(*) Not implemented</span> </label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>File Hd</td>
                                <td>&nbsp;</td>
                                <td align="left"><input disabled="disabled" class="text_css k-textbox" size="50" type="text" id="txt_file_hd" name="txt_file_hd" value="<?php echo $v_file_hd;?>" /> <span style="color: red">(*) Not implemented</span>  <label id="lbl_file_hd" style="color:red;display:none;">(*)</label></td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>The images of products </td>
                                <td>&nbsp;</td>
                                <td align="left" id="td_more_images">
                                    <?php echo $v_lst_image;?>
                                </td>


                            </tr>
                        </table>
                    </div>

                    <div class="material div_details">
                        <div id="size_option">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width:200px">Size Option</td>
                                <td align="left">
                                    <label><input disabled="disabled" type="checkbox" id="txt_size_option" name="txt_size_option" value="1"<?php echo $v_size_option==1?' checked="checked"':'';?> /> Allow customize</label>
                                    <span style="color: red">(*) Not implemented</span>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Print's Type</td>
                                <td align="left" colspan="2"><label>
                                        <input type="radio" value="0" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==0?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',0);?></label> /
                                    <label><input type="radio" value="1" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==1?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',1);?></label> /
                                    <label><input type="radio" value="2" id="txt_print_type" name="txt_print_type"<?php echo $v_print_type==2?' checked="checked"':'';?> /> <?php echo $cls_settings->get_option_name_by_id('print_type',2);?></label>
                                </td>
                            </tr>



                            <tr align="right" valign="top">
                                <td>
                                	Material<br /><br /><br />
                                	
                                </td>
                                <td align="left" colspan="2">
                                    <div class="grid_table">
                                        <div class="fielddis">
                                            <label>Width:</label>
                                            <input type="text" id="txt_size_width" name="txt_size_width" value="0" />
                                        </div>
                                        <div class="fielddis">
                                            <label>Length:</label>
                                            <input type="text" id="txt_size_length" name="txt_size_length" value="0" />
                                        </div>
                                        <div class="fielddis">
                                            <label>Size Unit</label>
                                            <select id="txt_size_unit" name="txt_size_unit">
                                                <?php echo $v_dsp_size_unit_draw;?>
                                            </select>
                                    	</div>
                                        <div class="fielddis dismaterial">
                                            <label for="txt_material_id">Material: </label>
                                            <select id="txt_material_id" name="txt_material_id">
                                                <option value="0" selected="selected">-------</option>
                                                <?php echo $v_dsp_material_draw;?>
                                            </select>
										</div>
                                        <div class="fielddis">
                                            <label for="txt_thick">Thickness:</label>
                                            <select id="txt_thick" name="txt_thick">
                                                <option value="0" selected="selected">----</option>
                                            </select>
                                        </div>
                                        <div class="fielddis">
                                            <label>Thickness Unit:</label>
                                            <select id="txt_thick_unit" name="txt_thick_unit">
                                                <option value="" selected="selected">----</option>
                                            </select>
                                   		</div>
                                        <div class="fielddis">
                                            <label for="txt_color">Color: </label>
                                            <span id="sp_color">
                                                <select id="txt_color" name="txt_color">
                                                </select>
                                            </span>
                                        </div>
                                        <div class="fielddis">
                                            <label>&nbsp;</label>
                                             <input type="checkbox" id="txt_two_sided" name="txt_two_sided" disabled="disabled" />
                                             Allow two-sided print?
                                    	</div>
                                        <div class="fielddis">
                                            <label for="txt_price">Price (<?php echo $v_sign_money;?>):</label>
                                            <input type="text" value="" id="txt_price" name="txt_price" /> 
                                        </div>
                                        <div class="fielddis">
                                            <label for="txt_allow_size_option">Allow size option </label>
                                            <input disabled="disabled" type="checkbox" value="1" id="txt_allow_size_option" name="txt_allow_size_option" />
                                            <span style="color: red">(*) Not implemented</span>
                                        </div>
                                       
                                         <div class="fielddis" style="clear:both;">
                                            <label for="txt_allow_size_option"> Add </label>
                                            <img id="add_material" src="images/icons/add-icon.png" style="cursor:pointer" title="Add Material" width="25" />
                                    	</div>
                                    </div>
                                    <div id="product_material">
										<?php echo $v_dsp_material_list;?>
                                    </div>
                                    <input type="hidden" id="txt_product_material" name="txt_product_material" />
                                     
                                </td>
                            </tr>
                        </table>
                        </div>
                        
                        <div id="material_memory">
                            <ul>
                                <li>Click icon <img src="images/icons/add.png"" /> to bring information to the list after completing all the necessary data.</li>
                                <li>Information material will be stored temporarily in the list below.</li>
                                <li>Can be fixed quickly following columns: Width, Length, Size's Unit, Size Option, 2-Sided Print and Price (by clicking on them)</li>
                            </ul>
                            <input type="submit" id="btn_submit_tb_product2" name="btn_submit_tb_product" value="Save All" class="k-button button_css" style=" margin:0px 0px 10px 0;display:table;" />
                        </div>
                        
                        <div id="material_grid"></div>
                    </div>
                    <div class="text div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width:200px">Text Option</td>
                                <td style="width:20px">&nbsp;</td>
                                <td align="left">
                                    <label><input disabled="disabled" type="checkbox" id="txt_text_option" name="txt_text_option" value="1"<?php echo $v_text_option==1?' checked="checked"':'';?> /> Allow customize</label>
                                    <span style="color: red">(*) Not implemented</span>
                                </td>
                            </tr>
                            <tr align="right" valign="top">
                                <td>Text</td>
                                <td align="center" valign="middle">&nbsp;<img src="images/icons/add.png" style="cursor:pointer" title="Add" onclick="add_text(this)" /></td>
                                <td align="left" colspan="2">
                                    <div id="product_text">
                                        <?php
                                        echo $v_dsp_text_list;
                                        ?>
                                    </div>
                                    <span style="color: red">(*) Not implemented</span>
                                </td>
                            </tr>
                        </table>
                    </div>
                        <div class="threshold div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Product group for threshold</td>
                                    <td style="width:20px">&nbsp;</td>
                                    <td align="left">
                                <select id="txt_product_threshold_group_id" name="txt_product_threshold_group_id">
                                </select>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Approved Threshold</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label><input type="checkbox" name="txt_is_threshold" id="txt_is_threshold" value="1"<?php echo $v_product_threshold>=0?' checked="checked"':'';?> />Threshold?</label> &nbsp;
                                        <input type="text" size="10" name="txt_product_threshold" id="txt_product_threshold" value="<?php echo $v_product_threshold>=0?$v_product_threshold:'';?>"<?php echo $v_product_threshold<0?' disabled="disabled"':'';?> />
                                        <input type="hidden" id="txt_hidden_threshold" value="<?php echo $v_product_threshold;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Locations' Threshold</td>
                                    <td>
                                        <?php if($v_is_super_admin || $v_edit_right){?>
                                        <img id="img_location_threshold" width="16" style="cursor: pointer" src="<?php echo IMAGE_URL.'icons/tab_edit.png';?>" border="0" title="Edit list locations threshold" />
                                        <?php }else{?>&nbsp;<?php }?>
                                    </td>
                                    <td align="left" colspan="2">
                                        <div <?php echo $v_dsp_location_threshold==''?' style="display:none"':'';?> id="location_threshold">
                                            <select id="txt_location_threshold" name="txt_location_threshold[]" multiple="multiple" style="width:300px; height:200px" class="k-textbox k-input">
                                                <?php echo $v_dsp_location_threshold;?>
                                            </select> <br />
                                            <input type="hidden" name="txt_hidden_location_threshold" id="txt_hidden_location_threshold" />
                                            <span style="color:#00709F">Selected item means that locations are allowed to exceed threshold.</span>
                                        </div>&nbsp;
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Excluded Locations</td>
                                    <td><?php if($v_is_super_admin || $v_edit_right){?>
                                        <img id="img_excluded_location" style="cursor: pointer" width="16" src="<?php echo IMAGE_URL.'icons/tab_edit.png';?>" border="0" title="Edit list excluded locations" />
                                        <?php }else{?>&nbsp;<?php }?>
                                    </td>
                                    <td align="left" colspan="2">
                                        <div <?php echo $v_dsp_excluded_location==''?' style="display:none"':'';?> id="excluded_location">
                                            <select id="txt_excluded_location" name="txt_excluded_location[]" multiple="multiple" style="width:300px; height:200px" class="k-textbox k-input">
                                                <?php echo $v_dsp_excluded_location;?>
                                            </select> <br />
                                            <!--<span style="color:#00709F">Hold down "Control", or "Command" on a Mac, to select more than one.</span>-->
                                        </div>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                        
                        <div class="tag div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width:200px">Tag</td>
                                    <td align="left">
            							<select id="txt_tag" name="txt_tag[]" multiple="multiple"> </select>
                                    </td>
                                </tr>
                            </table>
                            
                        </div>

                    <div class="template div_details">
                        <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                            <tr align="right" valign="top">
                                <td style="width:200px">Template</td>
                                <td align="left">
                                    <select id="txt_template" name="txt_template[]" multiple="multiple"> </select>
                                </td>
                            </tr>
                        </table>

                    </div>

                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_product" name="btn_submit_tb_product" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
<div id="div_location_threshold" style="display: none">

</div>
<div id="div_excluded_location" style="display: none">

</div>
<script type="text/javascript">
    var window_location_threshold_close_flag = false, window_excluded_location_close_flag = false;
    var window_location_threshold, window_excluded_location;
    $(document).ready(function(){
        window_location_threshold = $("div#div_location_threshold");
        window_excluded_location = $("div#div_excluded_location");
        $("img#img_location_threshold").bind("click", function(){
            if (!window_location_threshold.data("kendoWindow")) {
                window_location_threshold.kendoWindow({
                    width: "800px",
                    height: "500px",
                    actions: ["Maximize", "Close"],
                    modal: true,
                    iframe:true,
                    content:"<?php echo URL.$v_admin_key.'/'.$v_product_id?>/threshold",
                    title: "Location Threshold for Product",
                    close: window_threshold_close
                });
            }
            window_location_threshold.data("kendoWindow").center().open();

        });
        $("img#img_excluded_location").bind("click", function(){
            if (!window_excluded_location.data("kendoWindow")) {
                window_excluded_location.kendoWindow({
                    width: "800px",
                    height: "500px",
                    actions: ["Maximize", "Close"],
                    modal: true,
                    iframe:true,
                    content:"<?php echo URL.$v_admin_key.'/'.$v_product_id?>/exclude",
                    title: "Excluded Location for Product",
                    close: window_excluded_close
                });
            }
            window_excluded_location.data("kendoWindow").center().open();

        });

        function window_threshold_close(){
            if(window_location_threshold_close_flag){
                $('select#txt_location_threshold option').remove();
                for(var i=0; i<list_threshold.length; i++){
                    var $opt = $('<option value="'+list_threshold[i].location_id+'"'
                        +(list_threshold[i].overflow==1?' selected="selected"':'')+'>['+list_threshold[i].threshold+'] '
                        +list_threshold[i].location_name+'</option>');
                    $('select#txt_location_threshold').append($opt);
                }
                $('div#location_threshold').css('display',$('select#txt_location_threshold option').length>0?'':'none');
                window_location_threshold_close_flag = false;
            }
    }
    function window_excluded_close(){
        if(window_excluded_location_close_flag){
            if(list_excluded.length>0){
                $('select#txt_excluded_location option').remove();
                for(var i=0; i<list_excluded.length; i++){
                    var $opt = $('<option value="'+list_excluded[i][0]+'" selected="selected">'+list_excluded[i][1]+'</option>');
                    $('select#txt_excluded_location').append($opt);
                }
                $('div#excluded_location').css('display','');
            }

            window_excluded_location_close_flag = false;
        }
    }
    });
</script>