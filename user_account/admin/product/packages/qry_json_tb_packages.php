<?php if(!isset($v_sval)) die();?>
<?php
if(!$v_disabled_company_id)
    $arr_where_clause = array();
else $arr_where_clause = $arr_global_company;
$arr_where_clause['package_type'] = array('$gt'=>1);
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
$v_search_tag_name = isset($_POST['txt_search_tag_name'])?trim($_POST['txt_search_tag_name']):'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
$_SESSION['ss_last_company_id'] = $v_company_id;
if($v_search_tag_name!='') $arr_where_clause['product_sku'] = new MongoRegex('/'.$v_search_tag_name.'/i');
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
    for($i=0; $i<count($arr_temp); $i++){
        $arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
    }
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_tag_redirect']) && $_SESSION['ss_tb_tag_redirect']==1){
    if(isset($_SESSION['ss_tb_tag_where_clause'])){
        $arr_where_clause = unserialize($_SESSION['ss_tb_tag_where_clause']);
        if(!is_array($arr_where_clause)) $arr_where_clause = array();
    }
    if(isset($_SESSION['ss_tb_tag_sort'])){
        $arr_sort = unserialize($_SESSION['ss_tb_tag_sort']);
        if(!is_array($arr_sort)) $arr_sort = array();
    }
    unset($_SESSION['ss_tb_tag_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_product->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_tag_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_tag_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_tag_page'] = $v_page;
$_SESSION['ss_tb_tag_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_tag = $cls_tb_product->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
$arr_company = array();
$arr_location = array();
foreach($arr_tb_tag as $arr){
    $v_package_id = isset($arr['product_id'])?$arr['product_id']:0;
    $v_package_name = isset($arr['product_sku'])?$arr['product_sku']:'';
    $v_description = isset($arr['short_description'])?$arr['short_description']:'';
    $v_package_image = isset($arr['image_file'])?$arr['image_file']:'';
    $v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_status = isset($arr['status'])?$arr['status']:0;
    $v_package_type = isset($arr['package_type'])?$arr['package_type']:0;
    $arr_package_content = isset($arr['package_content'])?$arr['package_content']:array();
    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    $v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
    $v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
    $v_package_price = isset($arr['default_price'])?$arr['default_price']:0;
    $v_date_created = isset($arr['date_created'])?$arr['date_created']:(new MongoDate(time()));
    $v_date_created = date('d-M-Y', $v_date_created->sec);
    $v_package_content = '';
    for($i=0; $i<count($arr_package_content); $i++){
        $v_name = $arr_package_content[$i]['package_name'];
        $v_type = $arr_package_content[$i]['package_type'];

        if(!isset($arr_settings[$v_type]))
            $arr_settings[$v_type] = $cls_settings->get_option_name_by_id('package_type', $v_type);
        $v_type = $arr_settings[$v_type];
        $v_quantity = $arr_package_content[$i]['quantity'];
        $v_price = $arr_package_content[$i]['price'];
        $v_package_content .= '<label>Name: '.$v_name.' - Type: '.$v_type.' - Quantity: '.$v_quantity.' - Price: '.$v_price;

        if($i<count($arr_package_content)-1) $v_package_content.='<br />';
    }
    if($v_package_image!='' && $v_saved_dir!='' && file_exists($v_saved_dir.'/'.$v_package_image))
        $v_package_image = '<img src="'.$v_saved_dir.'/'.PRODUCT_IMAGE_THUMB.'_'.$v_package_image.'" style="max-width: 200px;" />';
    else
        $v_package_image = '&nbsp;';
    $v_package_type = $cls_settings->get_option_name_by_id('package_type', $v_package_type);
    $v_status = $v_status==0?'Active':'Inactive';
    if(!isset($arr_company[$v_company_id])) $arr_company[$v_company_id] = $cls_tb_company->select_scalar('company_name', array('company_id'=>$v_company_id));
    if(!isset($arr_location[$v_location_id])) $arr_location[$v_location_id] = $cls_tb_location->select_scalar('location_name', array('location_id'=>$v_location_id));
    $arr_ret_data[] = array(
        'row_order'=>++$v_row,
        'package_id' => $v_package_id,
        'package_name' => $v_package_name,
        'status' => $v_status,
        'description' => $v_description,
        'package_image' => $v_package_image,
        'package_type' => $v_package_type,
        'package_content' => $v_package_content,
        'price' => format_currency($v_package_price),
        'location' => $arr_location[$v_location_id],
        'company_name' => $arr_company[$v_company_id],
        'user_name' => $v_user_name,
        'date_created' => $v_date_created
    );
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_product'=>$arr_ret_data);
echo json_encode($arr_return);
?>