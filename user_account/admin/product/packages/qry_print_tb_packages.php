<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
    $v_where_clause = $_SESSION['ss_tb_tag_where_clause'];
    $arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_tag_sort'])){
    $v_sort = $_SESSION['ss_tb_tag_sort'];
    $arr_sort = unserialize($v_sort);
}
if(isset($_SESSION['company_id'])){
    if(isset($_SESSION['company_search_id']) && $_SESSION['company_id'] == 10000 ){
        $v_company_id = $_SESSION['company_search_id'];
    }else $v_company_id = $_SESSION['company_id'];
    if($v_company_id!=0){
        settype($v_company_id,"int");
        $arr_where_clause['company_id'] = $v_company_id;
    }
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_where_clause['package_type'] = array('$gt'=>1);
$arr_tb_packages = $cls_tb_product->select($arr_where_clause, $arr_sort);
$v_dsp_tb_packages = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="1" align="center">';

$v_dsp_tb_packages .= '<tr align="center" valign="middle">';
$v_dsp_tb_packages .= '<th>Ord</th>';
$v_dsp_tb_packages .= '<th>Product Id</th>';
$v_dsp_tb_packages .= '<th>Package Name</th>';
$v_dsp_tb_packages .= '<th>Package Content</th>';
$v_dsp_tb_packages .= '<th>Package Price</th>';
$v_dsp_tb_packages .= '<th>Location Id</th>';
$v_dsp_tb_packages .= '<th>Company Id</th>';
$v_dsp_tb_packages .= '<th>User Name</th>';
$v_dsp_tb_packages .= '<th>Date Created</th>';
$v_dsp_tb_packages .= '</tr>';
$v_count = 1;
foreach($arr_tb_packages as $arr){
    $v_dsp_tb_packages .= '<tr align="left" valign="middle">';
    $v_dsp_tb_packages .= '<td align="right">'.($v_count++).'</td>';
    $v_tag_id = isset($arr['product_id'])?$arr['product_id']:0;
    $v_tag_name = isset($arr['product_sku'])?$arr['product_sku']:'';
    $v_price = isset($arr['default_price'])?$arr['default_price']:'';
    $arr_package_content = isset($arr['package_content'])?$arr['package_content']:0;
    $v_package_content = '';
    for($i=0; $i<count($arr_package_content); $i++){
        $v_package_content = '';
        $v_name = $arr_package_content[$i]['package_name'];
        $v_type = $arr_package_content[$i]['package_type'];

        if(!isset($arr_settings[$v_type]))
            $arr_settings[$v_type] = $cls_settings->get_option_name_by_id('package_type', $v_type);
        $v_type = $arr_settings[$v_type];
        $v_quantity = $arr_package_content[$i]['quantity'];
        $v_price = $arr_package_content[$i]['price'];
        $v_package_content .= '<label>Name: '.$v_name.' - Type: '.$v_type.' - Quantity: '.$v_quantity.' - Price: '.$v_price;

        if($i<count($arr_package_content)-1) $v_package_content.='<br />';
    }
    $v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
    $v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
    $v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
    $v_date_created = isset($arr['date_created'])?$arr['date_created']:(new MongoDate(time()));
    $v_dsp_tb_packages .= '<td>'.$v_tag_id.'</td>';
    $v_dsp_tb_packages .= '<td>'.$v_tag_name.'</td>';
    $v_dsp_tb_packages .= '<td>'.$v_package_content.'</td>';
    $v_dsp_tb_packages .= '<td align="center">'.format_currency($v_price).'</td>';
    $v_dsp_tb_packages .= '<td>'.$cls_tb_location->select_scalar("location_name",array("location_id"=>(int)$v_location_id)).'</td>';
    $v_dsp_tb_packages .= '<td>'.$cls_tb_company->select_scalar("company_name",array("company_id"=>(int)$v_company_id)).'</td>';
    $v_dsp_tb_packages .= '<td>'.$v_user_name.'</td>';
    $v_dsp_tb_packages .= '<td>'.date('d-M-y',$v_date_created->sec).'</td>';
    $v_dsp_tb_packages .= '</tr>';
}
$v_dsp_tb_packages .= '</table>';
?>