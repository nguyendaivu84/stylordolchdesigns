<?php
if(!isset($v_sval)) die();
?>
<?php
add_class("cls_tb_tag");
$cls_tb_tag = new cls_tb_tag($db);
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:'0';
settype($v_company_id, 'int');
$arr_return = array('error'=>0, 'message'=>'', 'tag'=>array());
$arr_all_tag = array();
if($v_company_id>0){
    $arr_tag = $cls_tb_tag->select(array('company_id'=>$v_company_id));
    foreach($arr_tag as $arr){
        $arr_all_tag[] = array('tag_id'=>$arr['tag_id'], 'tag_name'=>$arr['tag_name']);
    }
}else{
    $arr_return['error'] = 1;
    $arr_return['message'] = 'Invalid Company ID';
}
$arr_return['tag'] = $arr_all_tag;
echo(json_encode($arr_return));
?>
