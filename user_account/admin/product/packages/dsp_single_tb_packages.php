<?php if(!isset($v_sval)) die();?>
<style>
    div.node {
        background-color: #FFFFFF;
        border: 1px outset #0099CC;
        border-radius: 5px 5px 5px 5px;
        box-shadow: 0 1px 1px #FFFFFF inset;
        float: left;
        margin-right: 5px;
        max-width: 200px;
        overflow: hidden;
        padding: 0px;
        padding-left:2px;
    }
    div.div_wrapper {
        float: left;
        height: 25px;
        margin-bottom: 5px;
        margin-left: 5px;
        padding-left: 2px;
        padding-top: 3px;
        text-align: left;
        width: 100%;
    }
    div.div_package {
        border: 1px solid #0033CC;
        border-radius: 3px 3px 3px 3px;
        float: left;
        height: 25px;
        margin-bottom: 3px;
        margin-left: 5px;
        max-width: 100px;
        padding-left: 2px;
        padding-top: 3px;
        text-align: left;
        width: 80px;
    }
</style>
<script type="text/javascript" src="lib/js/yahoo-min.js"></script>
<script type="text/javascript" src="lib/js/json-min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        set_cookie('ck_package_company', <?php echo $v_company_id;?>,60,'/');
        $('select#txt_location_id').change(function(){
            var parent_location_id = $(this).val();
            var parent_location_id = parseInt(parent_location_id, 10);
            set_cookie('ck_package_location', parent_location_id, 60,'/');
        });
        $('select#txt_package_type').change(function(e) {
            var val = $(this).val();
            val = parseInt(val, 10);
            if(val==2){
                $('div.div_wrapper').css('display','');
                $('div#div_wrapper_single').css('display','');
                $('div#div_wrapper_multiple').css('display','');
                $('div#div_wrapper_set').css('display','none');
                $('div#div_wrapper_package').css('display','none');
                $('div#div_wrapper_kit').css('display','none');
            }else if(val==3){
                $('div.div_wrapper').css('display','');
                $('div#div_wrapper_single').css('display','');
                $('div#div_wrapper_multiple').css('display','');
                $('div#div_wrapper_set').css('display','');
                $('div#div_wrapper_package').css('display','none');
                $('div#div_wrapper_kit').css('display','none');
            }else if(val==4){
                $('div.div_wrapper').css('display','');
                $('div#div_wrapper').css('display','');
                $('div.div_wrapper_single').css('display','');
                $('div#div_wrapper_multiple').css('display','');
                $('div#div_wrapper_set').css('display','');
                $('div#div_wrapper_package').css('display','');
                $('div#div_wrapper_kit').css('display','none');
            }else{
                $('div.div_wrapper').css('display','none');
            }
        });
        $("a[rel=rel_package]").fancybox({
            'showNavArrows'         : false,
            'width'                 : '700',
            'height'                : '600',
            'transitionIn'	        :	'elastic',
            'transitionOut'	        :	'elastic',
            'overlayShow'	        :	true,
            'type'                 : 'iframe',
            'hideOnOverlayClick'	: false,
            onClosed	:	function(){
            }
        });
        $("a[rel=product_images]").fancybox({
            'showNavArrows'         : false,
            'width'                 : '65%',
            'height'                : '85%',
            'transitionIn'	        :	'elastic',
            'transitionOut'	        :	'elastic',
            'overlayShow'	        :	true,
            'type'                 : 'iframe'
        });
        $('img.img_action').each(function(index, element) {
            $(this).click(function(e) {
                var id = $(this).attr('data-id');
                var type = $(this).attr('data-type');

                if(set_remove(id, type)) $(this).parent().remove();
            });
        });
        $("form#frm_tb_tag").submit(function(){
            var css = '';
            var package_sku = $("input#txt_package_sku").val();
            package_sku = $.trim(package_sku);
            var package_type = $("select#txt_package_type").val();
            package_type = parseInt(package_type, 10);
            css = isNaN(package_type)||package_type<=0?'':'none';
            $("label#lbl_package_type").css("display",css);
            if(css == '') return false;
            var package_price = $("input#txt_package_price").val();
            package_price = parseFloat(package_price);
            css = isNaN(package_price) || package_price<0?'':'none';
            $("label#lbl_package_price").css("display",css);
            if(css == '') return false;
            var company_id = $("select#txt_company_id").val();
            company_id = parseInt(company_id, 10);
            css = isNaN(company_id)?'':'none';
            $("label#lbl_company_id").css("display",css);
            if(css == '') return false;
            var tmp = new Array();
            var j = 0;
            for(var i=0; i< arr_package.length; i++){
                if(arr_package[i].status==0 && arr_package[i].package_type < package_type){
                    tmp[j] = new Package(arr_package[i].package_name, arr_package[i].package_type, arr_package[i].refer_id, arr_package[i].quantity,
                        arr_package[i].price, arr_package[i].package_image, arr_package[i].saved_dir , arr_package[i].location_id);
                    j++;
                }
            }
            var content = YAHOO.lang.JSON.stringify(tmp);
            $('input#txt_hidden_package_content').val(content);
            return true;
        });
        var tab_strip = $("#data_single_tab").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        }).data("kendoTabStrip");
        var tooltip = $("#tooltip").kendoTooltip({
            width: 120,
            position: "top"
        }).data("kendoTooltip");
        $("#txt_tag").width(300).kendoMultiSelect({
            dataTextField: "tag_name",
            dataValueField: "tag_id"
        });
        var combo_tag = $("#txt_tag").data("kendoMultiSelect");
        $('input#txt_package_image').kendoUpload({
            multiple: false
        });
        if(tooltip) tooltip.show();
        var combo_package_type = $('select#txt_package_type').width(200).kendoComboBox({

            dataTextField: "packagetype_name",
            dataValueField: "packagetype_id"
        }).data("kendoComboBox");
        var validator = $("div.information").kendoValidator().data("kendoValidator");
        var combo_company = $('select#txt_company_id').data('kendoComboBox');
        var location_data = <?php echo json_encode($arr_all_location);?>;
        var combo_location = $('select#txt_location_id').width(200).kendoComboBox({
            dataSource: location_data,
            dataTextField: "location_name",
            dataValueField: "location_id"
        }).data("kendoComboBox");
        combo_location.value(<?php echo $v_location_id;?>);
        $('input#txt_tag_order').kendoNumericTextBox({
            format: "n0",
            step: 1
        });
        <?php if(!$v_disabled_company_id){;?>
        $('select#txt_company_id').change(function(e){
            var company_id = $(this).val();
            company_id = parseInt(company_id, 10);
            if(isNaN(company_id) || company_id <0) company_id = 0;
            var $this = $(this);
            $.ajax({
                url     : '<?php echo URL.$v_admin_key;?>/ajax',
                type    :   'POST',
                data    :   {txt_session_id:'<?php echo session_id();?>', txt_company_id: company_id, txt_ajax_type: 'load_location'},
                beforeSend: function(){
                    combo_company.enable(false);
                    $this.prop("disabled", true);
                },
                success: function(data, status){
                    var ret = $.parseJSON(data);
                    if(ret.error==0){
                        var location_data = ret.location;
                        combo_location.setDataSource(location_data);
                        combo_location.value(0);
                        $('form#frm_tb_tag').find('#txt_company_id').val(company_id);
                    }
                    combo_company.enable(true);
                    $this.prop("disabled", false);
                }
            });
            $.ajax({
                url : '<?php echo URL.$v_admin_key;?>/ajax',
                type    : 'POST',
                data    :   {txt_company_id: company_id, txt_ajax_type:'load_tag'},
                success: function(data, type){
                    var ret = $.parseJSON(data);
                    if(ret.error==0){
                        var tags = ret.tag;
                        combo_tag.setDataSource(tags);
                    }else{
                        alert(ret.message);
                    }
                    $this.prop('disabled', false);
                    combo_company.enable(true);
                }
            });

        });
        <?php }else{?>
        combo_company.enable(false);
        <?php };?>
    });
    function Package(name, type, refer, quantity, price, image, saved_dir, location_id){
        this.package_name = name;
        this.package_type = type;
        this.quantity = quantity;
        this.price = price;
        this.refer_id = refer;
        this.package_image = image;
        this.saved_dir = saved_dir;
        this.location_id = location_id;
        this.status = 0;
        //alert('name: '+name+' -type: '+type+' -refer: '+refer+' -quantity: '+quantity+' -price: '+price+' -image: '+image+' -dir: '+saved_dir+' -location: '+location_id);
    }
    Package.prototype.total = function(){
        return this.quantity * this.price;
    }

    function set_remove(id, type){
        if(!confirm('Are you sure you want to remove this item?')) return false;
        for(var i=0; i<arr_package.length; i++){
            if(arr_package[i].refer_id==id && arr_package[i].package_type == type) arr_package[i].status = 1;
        }
        return true;
    }
    function update_package(){
        $('div.node').each(function(index, element) {
            $(this).remove();
        });
        var quantity=0;
        var name='';
        var type = 0;
        var id = 0;
        var $div;
        for(var i=0; i< arr_package.length; i++){
            if(arr_package[i].status == 0){
                name =  arr_package[i].package_name;
                quantity = arr_package[i].quantity;
                id = arr_package[i].refer_id;
                type = arr_package[i].package_type;
                var $label = $('<label>'+name+' ['+quantity+']</label>');
                var $node = $('<div class="node"><div>');

                //alert(arr_package[i].package_type);
                switch(type){
                    case 0:
                        $div = $('div#div_wrapper_multiple');
                        break;
                    case 1:
                        $div = $('div#div_wrapper_package');
                        break;
                    case 2:
                        $div = $('div#div_wrapper_kit');
                        break;
                }


                var $img = $('<img class="img_action" id="img_size_'+i+'" data-id="'+id+'" data-type="'+type+'" src="images/icons/delete.png" />');
                $img.bind('click', function(){
                    var id = $(this).attr('data-id');
                    var type = $(this).attr('data-type');
                    if(set_remove(id, type)) $(this).parent().remove();

                });
                $node.append($label);
                $node.append($img);
                $div.append($node);
            }
        }
    }
    <?php echo $v_dsp_script; ?>
</script>
<div id="div_body">
    <div id="div_splitter_content" style="height: 100%; width: 100%;">
        <div id="div_left_pane">
            <div class="pane-content">
                <div id="div_treeview"></div>
            </div>
        </div>
        <div id="div_right_pane">
            <div class="pane-content">
                <div id="div_title" class="k-block k-widget">
                    <h3>Package <?php echo (isset($v_package_id) && $v_package_id>0)?': '.$v_package_sku:'';?></h3>
                </div>
                <div id="div_quick">
                    <div id="div_quick_search">
                        &nbsp;
                    </div>
                    <div id="div_select">
                        <form id="frm_company_id" method="post">
                            Company: <select id="txt_company_id" name="txt_company_id">
                                <option value="0" selected="selected">-------</option>
                                <?php
                                echo $v_dsp_company_option;
                                ?>
                            </select>
                        </form>
                    </div>
                </div>

                <form id="frm_tb_tag" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_package_id.'/edit';?>" method="POST">
                    <input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
                    <input type="hidden" id="txt_package_id" name="txt_package_id" value="<?php echo $v_package_id;?>" />
                    <input type="hidden" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" />
                    <div id="data_single_tab">
                        <ul>
                            <li class="k-state-active">Information</li>
                        </ul>

                        <div class="information div_details">
                            <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Location</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_location_id" name="txt_location_id">
                                        </select>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Package Sku</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_package_sku" name="txt_package_sku" value="<?php echo $v_package_sku;?>" required validationMessage="Please input Package Sku" /> <label id="lbl_tag_name" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Short Description</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_package_short_des" name="txt_package_short_des" value="<?php echo $v_package_short_des;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Long Description</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_package_long_des" name="txt_package_long_des" value="<?php echo $v_package_long_des;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Package Detail</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <input class="k-textbox" type="text" id="txt_package_detail" name="txt_package_detail" value="<?php echo $v_package_detail;?>" />
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width:200px">Thumbnail for Package</td>
                                    <td style="width:1px">&nbsp;</td>
                                    <td align="left">
                                        <input type="hidden" name="txt_hidden_image_file"  value="<?php echo $v_image_file;?>" />
                                        <input type="hidden" name="txt_hidden_image_desc"  value="<?php echo $v_image_desc;?>" />
                                        <input type="hidden" name="txt_hidden_saved_dir"  value="<?php echo $v_saved_dir;?>" />
                                        <input type="file" id="txt_package_image" value="" name="txt_package_image" accept="image/*" />
                                        <?php
                                        //echo $v_image_url!=''? $v_image_url :'';
                                        ?>
                                    </td>
                                    <!--td rowspan="4" width="300" align="center" valign="middle">

                                        &nbsp;
                                    </td-->
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Package Type</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_package_type" name="txt_package_type" required validationMessage="Please chose Package Type">
                                            <option value="0" selected="selected">-------</option>
                                            <?php
                                              echo $v_package_type;
                                            ?>
                                        </select>
                                        <label id="lbl_tag_name" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Package Content</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        <?php echo $v_dsp_package_content;?>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td style="width: 200px">Prices</td>
                                    <td style="width: 1px">&nbsp;</td>
                                    <td align="left">
                                        $ <input class="k-textbox" type="text" id="txt_package_price" name="txt_package_price" value="<?php echo $v_package_price;?>" required validationMessage="Please input Tag Name" /> <label id="lbl_tag_name" class="k-required">(*)</label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Status</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <label>
                                            <input type="checkbox" id="txt_package_status" name="txt_package_status"<?php echo isset($v_tag_status)&&$v_tag_status==0?' checked="checked"':'';?> /> Active?
                                        </label>
                                    </td>
                                </tr>
                                <tr align="right" valign="top">
                                    <td>Package Tag</td>
                                    <td>&nbsp;</td>
                                    <td align="left">
                                        <select id="txt_tag" name="txt_tag[]" multiple="multiple" style="width:150px">
                                            <?php echo $v_dsp_tag_draw_multi; ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                        <?php if(isset($v_error_message) && $v_error_message!=''){?>
                            <div class="k-block k-widget k-error-colored div_errors">
                                <?php echo $v_error_message;?>
                            </div>
                        <?php }?>
                        <div class="k-block k-widget div_buttons">
                            <input type="hidden" id="txt_hidden_package_content" name="txt_hidden_package_content" value="" />
                            <input type="submit" id="btn_submit_tb_tag" name="btn_submit_tb_tag" value="Submit" class="k-button button_css" />
                        </div>
                    <?php }?>

                </form>
            </div>
        </div>
    </div>
</div>
