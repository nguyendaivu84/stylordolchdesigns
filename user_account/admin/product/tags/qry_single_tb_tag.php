<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_tag_id = 0;
$v_tag_name = '';
$v_tag_status = 0;
$v_tag_order = 0;
$v_tag_parent = 1;
$v_location_id = 0;
$v_company_id = $_SESSION['company_id'];
$v_company_id = $_SESSION['ss_last_company_id'];
$v_user_name = '';
$v_date_created = date('Y-m-d H:i:s', time());
$v_new_tag = true;
$arr_tag_group = array();
$arr_tag_parents = array();
if(isset($_POST['btn_submit_tb_tag'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_tag->set_mongo_id($v_mongo_id);
	$v_tag_id = isset($_POST['txt_tag_id'])?$_POST['txt_tag_id']:$v_tag_id;
	if(is_null($v_mongo_id)){
		$v_tag_id = $cls_tb_tag->select_next('tag_id');
	}
	$v_tag_id = (int) $v_tag_id;
	$cls_tb_tag->set_tag_id($v_tag_id);
	$v_tag_name = isset($_POST['txt_tag_name'])?$_POST['txt_tag_name']:$v_tag_name;
	$v_tag_name = trim($v_tag_name);
	if($v_tag_name=='') $v_error_message .= '[Tag Name] is empty!<br />';
	$cls_tb_tag->set_tag_name($v_tag_name);
	$v_tag_status = isset($_POST['txt_tag_status'])?0:1;
	$v_tag_status = (int) $v_tag_status;
	$cls_tb_tag->set_tag_status($v_tag_status);
	$v_tag_order = isset($_POST['txt_tag_order'])?$_POST['txt_tag_order']:$v_tag_order;
	$v_tag_order = (int) $v_tag_order;
	$cls_tb_tag->set_tag_order($v_tag_order);
    $v_tag_parent = isset($_POST['txt_tag_parent'])?1:0;
    $v_tag_parent = (int) $v_tag_parent;
    $cls_tb_tag->set_tag_parents($v_tag_parent);

    $v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
	$v_location_id = (int) $v_location_id;
	$cls_tb_tag->set_location_id($v_location_id);
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
	$v_company_id = (int) $v_company_id;
	if($v_company_id<0) $v_error_message .= '[Company Id] is negative!<br />';
	$cls_tb_tag->set_company_id($v_company_id);
	$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
	$cls_tb_tag->set_user_name($v_user_name);
	$v_date_created = date('Y-m-d H:i:s', time());
	$cls_tb_tag->set_date_created($v_date_created);
    /*
      $arr_tag_group = isset($_POST['txt_tag_group'])?$_POST['txt_tag_group']:'';
        if(!is_array($arr_tag_group)) $arr_tag_group = array();
        for($i=0; $i<count($arr_tag_group); $i++){
            $arr_tag_group[$i] = (int) $arr_tag_group[$i];
        }

    $arr_tag_parents_old = $cls_tb_tag->get_tag_parentsgroup();
	$arr_tag_parents = isset($_POST['txt_tag_parents'])?$_POST['txt_tag_parents']:'';
	if(!is_array($arr_tag_parents)) $arr_tag_parents = array();
    for($i=0; $i<count($arr_tag_parents); $i++){
        $arr_tag_parents[$i] = (int) $arr_tag_parents[$i];
    }

	// kiem tra is group va reset nhom tag truoc khi set vao data
	if($v_tag_parent==1){
		$arr_tag_parents = array();
		// nếu chuyển sang tag parent thì update cac san pham co chua tag nay
		if($v_tag_id!=''){
			add_class('cls_tb_product');
			$cls_tb_product = new cls_tb_product($db, LOG_DIR);
			$cls_tb_product->update_products_tag($v_tag_id);

		}
	}else
		$arr_tag_group = array();
    $cls_tb_tag->set_tag_group($arr_tag_group);

    if(count($arr_tag_group)>0)
        $cls_tb_tag->set_tag_parents(1);
     */
    /* hung edit */
    // neu dc check la` tag parent thi lay tag con
    if($v_tag_parent==1){
        $arr_tag_group = isset($_POST['txt_tag_group'])?$_POST['txt_tag_group']:'';
        if(!is_array($arr_tag_group)) $arr_tag_group = array();
        for($i=0; $i<count($arr_tag_group); $i++){
            $arr_tag_group[$i] = (int) $arr_tag_group[$i];
        }
        $cls_tb_tag->set_tag_group($arr_tag_group);
    }else {
        // lay het ca danh sach tag cha luon. Co nhieu bo vao` nhieu khong lay cai cu~ bo chung cai moi
        $cls_tb_tag->set_tag_group(array());
        $arr_tag_parents = isset($_POST['txt_tag_parents'])?$_POST['txt_tag_parents']:array();
        if(count($arr_tag_parents)>0){
            if(!is_array($arr_tag_parents)) $arr_tag_parents = array();
            for($i=0; $i<count($arr_tag_parents); $i++){
                $arr_tag_parents[$i] = (int) $arr_tag_parents[$i];
            }
            // mang tag cha cu
            $arr_tag_group_temp = $cls_tb_tag->select(array("tag_parent"=>1));
            foreach($arr_tag_group_temp as $arr_parent_temp){
                $arr_tag_group_temp = $arr_parent_temp['tag_group'];
                $v_tag_id_temp = $arr_parent_temp['tag_id'];
                $arr_new_tag_group = array();
                for($i=0;$i<count($arr_tag_group_temp);$i++){
                    if(!in_array($v_tag_id,$arr_tag_group_temp))
                        $arr_new_tag_group[] = $arr_tag_group_temp[$i];
                }
                $cls_tb_tag->update_field("tag_group",$arr_new_tag_group,array("tag_id"=>$v_tag_id_temp));
            }
            // mang tag cha moi
            for($i=0; $i<count($arr_tag_parents); $i++){
                $v_tag_id_temp = $arr_tag_parents[$i];
                $arr_tag_group_temp = $cls_tb_tag->select_scalar("tag_group",array("tag_id"=>$v_tag_id_temp));
                //$arr_tag_group_temp = $cls_tb_tag->select(array("tag_parent"=>1));
                if(!in_array($v_tag_id,$arr_tag_group_temp)){
                    $arr_tag_group_temp[] = $v_tag_id;
                    $cls_tb_tag->update_field("tag_group",$arr_tag_group_temp,array("tag_id"=>$v_tag_id_temp));
                }
            }
        }else{
            $arr_tag_parent = $cls_tb_tag->select(array("tag_parent"=>1));
            foreach($arr_tag_parent as $arr_temp){
                $arr_new_tag_group = array();
                $v_tag_id_temp = $arr_temp['tag_id'];
                settype($v_tag_id_temp,"int");
                $arr_old_tag_group = $arr_temp['tag_group'];
                for($k=0;$k<count($arr_old_tag_group);$k++){
                    if($arr_old_tag_group[$k]!=$v_tag_id)
                        $arr_new_tag_group[] = $arr_old_tag_group[$k];
                }
                $cls_tb_tag->update_field("tag_group",$arr_new_tag_group,array("tag_id"=>$v_tag_id_temp));
            }
        }
    }

	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_tag->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_tag->update(array('_id' => $v_mongo_id));
			
			$v_new_tag = false;
		}
		//$s_test = $cls_tb_tag->update_group($arr_tag_parents_old,$arr_tag_parents);

		if($v_result){
			$_SESSION['ss_tb_tag_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_tag) $v_tag_id = 0;
		}
	}
	
}else{
	$v_tag_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_tag_id,'int');
	if($v_tag_id>0){
		$v_row = $cls_tb_tag->select_one(array('tag_id' => $v_tag_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_tag->get_mongo_id();
			$v_tag_id = $cls_tb_tag->get_tag_id();
			$v_tag_name = $cls_tb_tag->get_tag_name();
			$v_tag_status = $cls_tb_tag->get_tag_status();
			$v_tag_order = $cls_tb_tag->get_tag_order();
            $v_tag_parent = $cls_tb_tag->get_tag_parents();
            $v_location_id = $cls_tb_tag->get_location_id();
			$v_company_id = $cls_tb_tag->get_company_id();
			$v_user_name = $cls_tb_tag->get_user_name();
            $arr_tag_group = $cls_tb_tag->get_tag_group();
			$arr_tag_parents = $cls_tb_tag->get_tag_parentsgroup();
			$v_date_created = date('Y-m-d H:i:s',$cls_tb_tag->get_date_created());
		}
	}
}
$v_tmp = 0;
//$v_company_id = $_SESSION['company_id'];
$arr_tag_where = array("company_id"=>$v_company_id);//$arr_global_company;
$arr_tag_where['tag_id'] = array('$ne' => $v_tag_id);
$arr_tag_where['tag_parent'] = 1;

$arr_all_tag_parent = get_array_data($cls_tb_tag,'tag_id', 'tag_name',$v_tmp,array(),$arr_tag_where);

$arr_tag_where['tag_parent'] = array('$ne' => 1);
$arr_all_tag_child = get_array_data($cls_tb_tag,'tag_id', 'tag_name',$v_tmp,array(),$arr_tag_where);
$arr_all_tag = get_array_data($cls_tb_tag,'tag_id', 'tag_name',$v_tmp,array(),$arr_global_company);

//$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
//settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company,$arr_sort);


$v_tmp_location_id = $v_location_id;
$arr_all_location = get_array_data($cls_tb_location, 'location_id', 'location_name', $v_tmp_location_id, array(0,'--------'), array("company_id"=>$v_company_id,"status"=>array('$in'=>$_SESSION['location_accept_status'])));
$v_location_id=$v_tmp_location_id;
$v_content = '';
$v_title = '';
add_class("cls_tb_help");
$cls_tb_help = new cls_tb_help($db);
add_class("cls_tb_module");
$cls_tb_module = new cls_tb_module($db);
$v_module_id = $cls_tb_module->select_scalar("module_id",array("module_menu"=>"product_tags"));
$v_row = $cls_tb_help->select_one(array("help_status"=>0,"help_module"=>(int)$v_module_id,"help_type"=>"admin_page"));
if($v_row == 1){
    $v_content = $cls_tb_help->get_help_content();
    $v_title = $cls_tb_help->get_help_title();
}
else{
    $v_content = "Help on this topic is not yet avaible";
    $v_title = "Help on tag";
}
?>