<?php if(!isset($v_sval)) die();?>
<?php
$arr_where_clause = array();
$v_company_id = isset($_POST['txt_search_company_id'])?$_POST['txt_search_company_id']:'0';
$v_quick_search = isset($_POST['txt_quick_search'])?$_POST['txt_quick_search']:'';
settype($v_company_id, 'int');
if($v_company_id > 0) $arr_where_clause['company_id'] = $v_company_id;
//Sort
$arr_temp = isset($_REQUEST['sort'])?$_REQUEST['sort']:array();
$arr_sort = array();
if(is_array($arr_temp) && count($arr_temp)>0){
	for($i=0; $i<count($arr_temp); $i++){
		$arr_sort[$arr_temp[$i]['field']] = $arr_temp[$i]['dir']=='asc'?1:-1;
	}
}
if(!is_array($arr_sort)) $arr_sort = array();
//Start pagination
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_page_size = isset($_REQUEST['pageSize'])?$_REQUEST['pageSize']:10;
if(isset($_SESSION['ss_tb_product_images_redirect']) && $_SESSION['ss_tb_product_images_redirect']==1){
	if(isset($_SESSION['ss_tb_product_images_where_clause'])){
		$arr_where_clause = unserialize($_SESSION['ss_tb_product_images_where_clause']);
		if(!is_array($arr_where_clause)) $arr_where_clause = array();
	}
	if(isset($_SESSION['ss_tb_product_images_sort'])){
		$arr_sort = unserialize($_SESSION['ss_tb_product_images_sort']);
		if(!is_array($arr_sort)) $arr_sort = array();
	}
	unset($_SESSION['ss_tb_product_images_redirect']);
}
settype($v_page, 'int');
settype($v_page_size, 'int');
$v_total_rows = $cls_tb_product_images->count($arr_where_clause);
if($v_page<1) $v_page = 1;
if($v_page_size<10) $v_page_size = 10;
$v_total_pages = ceil($v_total_rows/$v_page_size);
$v_skip = ($v_page - 1) * $v_page_size;
$_SESSION['ss_tb_product_images_where_clause'] = serialize($arr_where_clause);
$_SESSION['ss_tb_product_images_sort'] = serialize($arr_sort);
$_SESSION['ss_tb_product_images_page'] = $v_page;
$_SESSION['ss_tb_product_images_quick_search'] = $v_quick_search;
//End pagination
$arr_tb_product_images = $cls_tb_product_images->select_limit($v_skip, $v_page_size, $arr_where_clause, $arr_sort);
$arr_ret_data = array();
$v_row = $v_skip;
foreach($arr_tb_product_images as $arr){
	$v_product_images_id = isset($arr['product_images_id'])?$arr['product_images_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
	$v_low_res_image = isset($arr['low_res_image'])?$arr['low_res_image']:'';
	$v_image_size = isset($arr['image_size'])?$arr['image_size']:0;
	$v_image_width = isset($arr['image_width'])?$arr['image_width']:0;
	$v_image_height = isset($arr['image_height'])?$arr['image_height']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_status = isset($arr['status'])?$arr['status']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_type = isset($arr['user_type'])?$arr['user_type']:0;
	$v_date_created = isset($arr['date_created'])?$arr['date_created']:(new MongoDate(time()));
	$arr_ret_data[] = array(
		'row_order'=>++$v_row,
		'product_images_id' => $v_product_images_id,
		'product_id' => $cls_tb_product->select_scalar('product_name',array('product_id'=>(int)$v_product_id )) ,
		'company_id' => $cls_tb_company->select_scalar('company_name', array('company_id'=>(int)$v_company_id )) ,
		'location_id' => $cls_tb_location->select_scalar('location_name',array('location_id'=>(int)$v_location_id )) ,
		'product_image' => $v_product_image,
		'low_res_image' => $v_low_res_image,
		'image_size' => $v_image_size,
		'image_width' => $v_image_width,
		'image_height' => $v_image_height,
		'saved_dir' => $v_saved_dir,
		'status' => $cls_settings->get_option_name_by_id('status',$v_status),
		'user_name' => $v_user_name,
		'user_type' => $v_user_type,
		'date_created' => $v_date_created
	);
}
header("Content-type: application/json");
$arr_return = array('total_rows'=>$v_total_rows, 'tb_product_images'=>$arr_ret_data);
echo json_encode($arr_return);
?>