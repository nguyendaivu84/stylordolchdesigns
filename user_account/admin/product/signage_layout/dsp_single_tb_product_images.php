<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_product_images").click(function(e){
		var css = '';

		var product_images_id = $("input#txt_product_images_id").val();
		product_images_id = parseInt(product_images_id, 10);
		css = isNaN(product_images_id)?'':'none';
		$("label#lbl_product_images_id").css("display",css);
		if(css == '') return false;
		var product_id = $("input#txt_product_id").val();
		product_id = parseInt(product_id, 10);
		css = isNaN(product_id)?'':'none';
		$("label#lbl_product_id").css("display",css);
		if(css == '') return false;
		var company_id = $("input#txt_company_id").val();
		company_id = parseInt(company_id, 10);
		css = isNaN(company_id)?'':'none';
		$("label#lbl_company_id").css("display",css);
		if(css == '') return false;
		var location_id = $("input#txt_location_id").val();
		location_id = parseInt(location_id, 10);
		css = isNaN(location_id)?'':'none';
		$("label#lbl_location_id").css("display",css);
		if(css == '') return false;
		var map_content = $("input#txt_map_content").val();
		var product_image = $("input#txt_product_image").val();
		product_image = $.trim(product_image);
		css = product_image==''?'':'none';
		$("label#lbl_product_image").css("display",css);
		if(css == '') return false;
		var low_res_image = $("input#txt_low_res_image").val();
		low_res_image = $.trim(low_res_image);
		css = low_res_image==''?'':'none';
		$("label#lbl_low_res_image").css("display",css);
		if(css == '') return false;
		var image_size = $("input#txt_image_size").val();
		image_size = parseInt(image_size, 10);
		css = isNaN(image_size)?'':'none';
		$("label#lbl_image_size").css("display",css);
		if(css == '') return false;
		var image_width = $("input#txt_image_width").val();
		image_width = parseInt(image_width, 10);
		css = isNaN(image_width)?'':'none';
		$("label#lbl_image_width").css("display",css);
		if(css == '') return false;
		var image_height = $("input#txt_image_height").val();
		image_height = parseInt(image_height, 10);
		css = isNaN(image_height)?'':'none';
		$("label#lbl_image_height").css("display",css);
		if(css == '') return false;
		var saved_dir = $("input#txt_saved_dir").val();
		saved_dir = $.trim(saved_dir);
		css = saved_dir==''?'':'none';
		$("label#lbl_saved_dir").css("display",css);
		if(css == '') return false;
		var status = $("input#txt_status").val();
		status = parseInt(status, 10);
		css = isNaN(status)?'':'none';
		$("label#lbl_status").css("display",css);
		if(css == '') return false;
		var user_name = $("input#txt_user_name").val();
		user_name = $.trim(user_name);
		css = user_name==''?'':'none';
		$("label#lbl_user_name").css("display",css);
		if(css == '') return false;
		var user_type = $("input#txt_user_type").val();
		user_type = parseInt(user_type, 10);
		css = isNaN(user_type)?'':'none';
		$("label#lbl_user_type").css("display",css);
		if(css == '') return false;
		var date_created = $("input#txt_date_created").val();
		css = check_date(date_created)?'':'none';
		$("label#lbl_date_created").css("display",css);
		if(css == '') return false;
		return true;
	});
	$('input#txt_date_created').kendoDatePicker({format:"dd-MMM-yyyy"});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_product_images').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>Product_images</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_product_images" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_product_images_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_product_images_id" name="txt_product_images_id" value="<?php echo $v_product_images_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>Product Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_id" name="txt_product_id" value="<?php echo $v_product_id;?>" /> <label id="lbl_product_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Company Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_company_id" name="txt_company_id" value="<?php echo $v_company_id;?>" /> <label id="lbl_company_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Location Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_location_id" name="txt_location_id" value="<?php echo $v_location_id;?>" /> <label id="lbl_location_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Map Content</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_map_content" name="txt_map_content" value="<?php echo $arr_map_content;?>" /> <label id="lbl_map_content" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Product Image</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_product_image" name="txt_product_image" value="<?php echo $v_product_image;?>" /> <label id="lbl_product_image" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Low Res Image</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_low_res_image" name="txt_low_res_image" value="<?php echo $v_low_res_image;?>" /> <label id="lbl_low_res_image" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Image Size</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_image_size" name="txt_image_size" value="<?php echo $v_image_size;?>" /> <label id="lbl_image_size" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Image Width</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_image_width" name="txt_image_width" value="<?php echo $v_image_width;?>" /> <label id="lbl_image_width" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Image Height</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_image_height" name="txt_image_height" value="<?php echo $v_image_height;?>" /> <label id="lbl_image_height" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Saved Dir</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_saved_dir" name="txt_saved_dir" value="<?php echo $v_saved_dir;?>" /> <label id="lbl_saved_dir" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Status</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_status" name="txt_status" value="<?php echo $v_status;?>" /> <label id="lbl_status" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Name</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_name" name="txt_user_name" value="<?php echo $v_user_name;?>" /> <label id="lbl_user_name" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Type</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_type" name="txt_user_type" value="<?php echo $v_user_type;?>" /> <label id="lbl_user_type" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Date Created</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_date_created" name="txt_date_created" value="<?php echo $v_date_created;?>" /> <label id="lbl_date_created" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_product_images" name="btn_submit_tb_product_images" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
