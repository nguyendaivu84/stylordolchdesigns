<?php if(!isset($v_sval)) die();?>
<?php
$v_error_message = '';
$v_mongo_id = NULL;
$v_product_images_id = 0;
$v_product_id = 0;
$v_company_id = 0;
$v_location_id = 0;
$arr_map_content = array();
$v_product_image = '';
$v_low_res_image = '';
$v_image_size = 0;
$v_image_width = 0;
$v_image_height = 0;
$v_saved_dir = '';
$v_status = 0;
$v_user_name = '';
$v_user_type = 0;
$v_date_created = date('Y-m-d H:i:s', time());
$v_new_product_images = true;
if(isset($_POST['btn_submit_tb_product_images'])){
	$v_mongo_id = isset($_POST['txt_mongo_id'])?$_POST['txt_mongo_id']:NULL;
	if(trim($v_mongo_id)!='') $v_mongo_id = new MongoID($v_mongo_id); else $v_mongo_id = NULL;
	$cls_tb_product_images->set_mongo_id($v_mongo_id);
	$v_product_images_id = isset($_POST['txt_product_images_id'])?$_POST['txt_product_images_id']:$v_product_images_id;
	if(is_null($v_mongo_id)){
		$v_product_images_id = $cls_tb_product_images->select_next('product_images_id');
	}
	$v_product_images_id = (int) $v_product_images_id;
	$cls_tb_product_images->set_product_images_id($v_product_images_id);
	$v_product_id = isset($_POST['txt_product_id'])?$_POST['txt_product_id']:$v_product_id;
	$v_product_id = (int) $v_product_id;
	if($v_product_id<0) $v_error_message .= '[Product Id] is negative!<br />';
	$cls_tb_product_images->set_product_id($v_product_id);
	$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:$v_company_id;
	$v_company_id = (int) $v_company_id;
	if($v_company_id<0) $v_error_message .= '[Company Id] is negative!<br />';
	$cls_tb_product_images->set_company_id($v_company_id);
	$v_location_id = isset($_POST['txt_location_id'])?$_POST['txt_location_id']:$v_location_id;
	$v_location_id = (int) $v_location_id;
	if($v_location_id<0) $v_error_message .= '[Location Id] is negative!<br />';
	$cls_tb_product_images->set_location_id($v_location_id);
	$arr_map_content = isset($_POST['txt_map_content'])?$_POST['txt_map_content']:$arr_map_content;
	$cls_tb_product_images->set_map_content($arr_map_content);
	$v_product_image = isset($_POST['txt_product_image'])?$_POST['txt_product_image']:$v_product_image;
	$v_product_image = trim($v_product_image);
	if($v_product_image=='') $v_error_message .= '[Product Image] is empty!<br />';
	$cls_tb_product_images->set_product_image($v_product_image);
	$v_low_res_image = isset($_POST['txt_low_res_image'])?$_POST['txt_low_res_image']:$v_low_res_image;
	$v_low_res_image = trim($v_low_res_image);
	if($v_low_res_image=='') $v_error_message .= '[Low Res Image] is empty!<br />';
	$cls_tb_product_images->set_low_res_image($v_low_res_image);
	$v_image_size = isset($_POST['txt_image_size'])?$_POST['txt_image_size']:$v_image_size;
	$v_image_size = (int) $v_image_size;
	if($v_image_size<0) $v_error_message .= '[Image Size] is negative!<br />';
	$cls_tb_product_images->set_image_size($v_image_size);
	$v_image_width = isset($_POST['txt_image_width'])?$_POST['txt_image_width']:$v_image_width;
	$v_image_width = (int) $v_image_width;
	if($v_image_width<0) $v_error_message .= '[Image Width] is negative!<br />';
	$cls_tb_product_images->set_image_width($v_image_width);
	$v_image_height = isset($_POST['txt_image_height'])?$_POST['txt_image_height']:$v_image_height;
	$v_image_height = (int) $v_image_height;
	if($v_image_height<0) $v_error_message .= '[Image Height] is negative!<br />';
	$cls_tb_product_images->set_image_height($v_image_height);
	$v_saved_dir = isset($_POST['txt_saved_dir'])?$_POST['txt_saved_dir']:$v_saved_dir;
	$v_saved_dir = trim($v_saved_dir);
	if($v_saved_dir=='') $v_error_message .= '[Saved Dir] is empty!<br />';
	$cls_tb_product_images->set_saved_dir($v_saved_dir);
	$v_status = isset($_POST['txt_status'])?$_POST['txt_status']:$v_status;
	$v_status = (int) $v_status;
	if($v_status<0) $v_error_message .= '[Status] is negative!<br />';
	$cls_tb_product_images->set_status($v_status);
	$v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:$v_user_name;
	$v_user_name = trim($v_user_name);
	if($v_user_name=='') $v_error_message .= '[User Name] is empty!<br />';
	$cls_tb_product_images->set_user_name($v_user_name);
	$v_user_type = isset($_POST['txt_user_type'])?$_POST['txt_user_type']:$v_user_type;
	$v_user_type = (int) $v_user_type;
	if($v_user_type<0) $v_error_message .= '[User Type] is negative!<br />';
	$cls_tb_product_images->set_user_type($v_user_type);
	$v_date_created = isset($_POST['txt_date_created'])?$_POST['txt_date_created']:$v_date_created;
	if(!check_date($v_date_created)) $v_error_message .= '[Date Created] is invalid date/time!<br />';
	$cls_tb_product_images->set_date_created($v_date_created);
	if($v_error_message==''){
		if(is_null($v_mongo_id)){
			$v_mongo_id = $cls_tb_product_images->insert();
			$v_result = is_object($v_mongo_id);
		}else{
			$v_result = $cls_tb_product_images->update(array('_id' => $v_mongo_id));
			$v_new_product_images = false;
		}
		if($v_result){
			$_SESSION['ss_tb_product_images_redirect'] = 1;
			redir(URL.$v_admin_key);
		}else{
			if($v_new_product_images) $v_product_images_id = 0;
		}
	}
}else{
	$v_product_images_id= isset($_GET['id'])?$_GET['id']:'0';
	settype($v_product_images_id,'int');
	if($v_product_images_id>0){
		$v_row = $cls_tb_product_images->select_one(array('product_images_id' => $v_product_images_id));
		if($v_row == 1){
			$v_mongo_id = $cls_tb_product_images->get_mongo_id();
			$v_product_images_id = $cls_tb_product_images->get_product_images_id();
			$v_product_id = $cls_tb_product_images->get_product_id();
			$v_company_id = $cls_tb_product_images->get_company_id();
			$v_location_id = $cls_tb_product_images->get_location_id();
			$arr_map_content = $cls_tb_product_images->get_map_content();
			$v_product_image = $cls_tb_product_images->get_product_image();
			$v_low_res_image = $cls_tb_product_images->get_low_res_image();
			$v_image_size = $cls_tb_product_images->get_image_size();
			$v_image_width = $cls_tb_product_images->get_image_width();
			$v_image_height = $cls_tb_product_images->get_image_height();
			$v_saved_dir = $cls_tb_product_images->get_saved_dir();
			$v_status = $cls_tb_product_images->get_status();
			$v_user_name = $cls_tb_product_images->get_user_name();
			$v_user_type = $cls_tb_product_images->get_user_type();
			$v_date_created = date('Y-m-d H:i:s',$cls_tb_product_images->get_date_created());
		}
	}
}
$v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:(isset($v_company_id)?$v_company_id:'0');
$v_company_id = $_SESSION['company_id'];
settype($v_company_id, 'int');
$v_dsp_company_option = $cls_tb_company->draw_option('company_id', 'company_name', $v_company_id,$arr_global_company);
?>