<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_product_images_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_product_images_sort'])){
	$v_sort = $_SESSION['ss_tb_product_images_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_product_images = $cls_tb_product_images->select($arr_where_clause, $arr_sort);
$v_dsp_tb_product_images = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_product_images .= '<tr align="center" valign="middle">';
$v_dsp_tb_product_images .= '<th>Ord</th>';
$v_dsp_tb_product_images .= '<th>Product Images Id</th>';
$v_dsp_tb_product_images .= '<th>Product Id</th>';
$v_dsp_tb_product_images .= '<th>Company Id</th>';
$v_dsp_tb_product_images .= '<th>Location Id</th>';
$v_dsp_tb_product_images .= '<th>Product Image</th>';
$v_dsp_tb_product_images .= '<th>Low Res Image</th>';
$v_dsp_tb_product_images .= '<th>Image Size</th>';
$v_dsp_tb_product_images .= '<th>Image Width</th>';
$v_dsp_tb_product_images .= '<th>Image Height</th>';
$v_dsp_tb_product_images .= '<th>Saved Dir</th>';
$v_dsp_tb_product_images .= '<th>Status</th>';
$v_dsp_tb_product_images .= '<th>User Name</th>';
$v_dsp_tb_product_images .= '<th>User Type</th>';
$v_dsp_tb_product_images .= '<th>Date Created</th>';
$v_dsp_tb_product_images .= '</tr>';
$v_count = 1;
foreach($arr_tb_product_images as $arr){
	$v_dsp_tb_product_images .= '<tr align="left" valign="middle">';
	$v_dsp_tb_product_images .= '<td align="right">'.($v_count++).'</td>';
	$v_product_images_id = isset($arr['product_images_id'])?$arr['product_images_id']:0;
	$v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
	$v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
	$v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
	$v_product_image = isset($arr['product_image'])?$arr['product_image']:'';
	$v_low_res_image = isset($arr['low_res_image'])?$arr['low_res_image']:'';
	$v_image_size = isset($arr['image_size'])?$arr['image_size']:0;
	$v_image_width = isset($arr['image_width'])?$arr['image_width']:0;
	$v_image_height = isset($arr['image_height'])?$arr['image_height']:0;
	$v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
	$v_status = isset($arr['status'])?$arr['status']:0;
	$v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
	$v_user_type = isset($arr['user_type'])?$arr['user_type']:0;
	$v_date_created = isset($arr['date_created'])?$arr['date_created']:(new MongoDate(time()));
	$v_dsp_tb_product_images .= '<td>'.$v_product_images_id.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_product_id.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_company_id.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_location_id.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_product_image.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_low_res_image.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_image_size.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_image_width.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_image_height.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_saved_dir.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_status.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_user_name.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_user_type.'</td>';
	$v_dsp_tb_product_images .= '<td>'.$v_date_created.'</td>';
	$v_dsp_tb_product_images .= '</tr>';
}
$v_dsp_tb_product_images .= '</table>';
?>