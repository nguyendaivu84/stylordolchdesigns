<?php if(!isset($v_sval)) die();?>
<script type="text/javascript">
$(document).ready(function(){
	$("input#btn_submit_tb_user_log").click(function(e){
		var css = '';

		var user_id = $("input#txt_user_id").val();
		user_id = parseInt(user_id, 10);
		css = isNaN(user_id)?'':'none';
		$("label#lbl_user_id").css("display",css);
		if(css == '') return false;
		var user_password = $("input#txt_user_password").val();
		user_password = $.trim(user_password);
		css = user_password==''?'':'none';
		$("label#lbl_user_password").css("display",css);
		if(css == '') return false;
		var log_ipaddress = $("input#txt_log_ipaddress").val();
		log_ipaddress = $.trim(log_ipaddress);
		css = log_ipaddress==''?'':'none';
		$("label#lbl_log_ipaddress").css("display",css);
		if(css == '') return false;
		var log_url = $("input#txt_log_url").val();
		log_url = $.trim(log_url);
		css = log_url==''?'':'none';
		$("label#lbl_log_url").css("display",css);
		if(css == '') return false;
		var log_datetime = $("input#txt_log_datetime").val();
		css = check_date(log_datetime)?'':'none';
		$("label#lbl_log_datetime").css("display",css);
		if(css == '') return false;
		return true;
	});
	$('input#txt_log_datetime').kendoDatePicker({format:"dd-MMM-yyyy"});
	var tab_strip = $("#data_single_tab").kendoTabStrip({
		animation:  {
			open: {
				effects: "fadeIn"
			}
		}
	}).data("kendoTabStrip");
	var tooltip = $("span.tooltips").kendoTooltip({
	filter:"a",
		width: 120,
		position: "top"
	}).data("kendoTooltip");
	var validator = $("div.information").kendoValidator().data("kendoValidator");
	var combo_company = $('select#txt_company_id').data('kendoComboBox');
	<?php if($v_company_id <= 0){;?>
	$('select#txt_company_id').change(function(e){
		var company_id = $(this).val();
		company_id = parseInt(company_id, 10);
		if(isNaN(company_id) || company_id <0) company_id = 0;
		$('form#frm_tb_user_log').find('#txt_company_id').val(company_id);
		});
	<?php }else{;?>
		combo_company.enable(false);
	<?php };?>
});
</script>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>User_log</h3>
                    </div>
                    <div id="div_quick">
                        <div id="div_quick_search">
                        &nbsp;
                        </div>
                        <div id="div_select">
                            <form id="frm_company_id" method="post">
                        Company: <select id="txt_company_id" name="txt_company_id">
                                    <option value="0" selected="selected">-------</option>
                                    <?php
                                    echo $v_dsp_company_option;
                                    ?>
                                </select>
                            </form>
                        </div>
                    </div>

<form id="frm_tb_user_log" action="<?php echo URL.$v_admin_key;?>/<?php echo is_null($v_mongo_id)?'add':$v_log_id.'/edit';?>" method="POST">
<input type="hidden" id="txt_mongo_id" name="txt_mongo_id" value="<?php echo $v_mongo_id;?>" />
<input type="hidden" id="txt_log_id" name="txt_log_id" value="<?php echo $v_log_id;?>" />
                    <div id="data_single_tab">
                    <ul>
                        <li class="k-state-active">Information</li>
                        <li>Other</li>
                    </ul>

                    <div class="information div_details">
<table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
<tr align="right" valign="top">
		<td>User Id</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_id" name="txt_user_id" value="<?php echo $v_user_id;?>" /> <label id="lbl_user_id" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>User Password</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_user_password" name="txt_user_password" value="<?php echo $v_user_password;?>" /> <label id="lbl_user_password" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Log Ipaddress</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_log_ipaddress" name="txt_log_ipaddress" value="<?php echo $v_log_ipaddress;?>" /> <label id="lbl_log_ipaddress" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Log Url</td>
		<td>&nbsp;</td>
		<td align="left"><input class="text_css k-textbox" size="50" type="text" id="txt_log_url" name="txt_log_url" value="<?php echo $v_log_url;?>" /> <label id="lbl_log_url" class="k-required">(*)</label></td>
	</tr>
<tr align="right" valign="top">
		<td>Log Datetime</td>
		<td>&nbsp;</td>
		<td align="left"><input type="text" id="txt_log_datetime" name="txt_log_datetime" value="<?php echo $v_log_datetime;?>" /> <label id="lbl_log_datetime" class="k-required">(*)</label></td>
	</tr>
</table>
                    </div>
                    <div class="other div_details">
                    </div>
                   </div>
                   <?php if(isset($v_act) && in_array($v_act, array('E', 'N'))){?>
                   <?php if($v_error_message!=''){?>
                    <div class="k-block k-widget k-error-colored div_errors">
                    <?php echo $v_error_message;?>
                    </div>
                    <?php }?>
                    <div class="k-block k-widget div_buttons">
                    <input type="submit" id="btn_submit_tb_user_log" name="btn_submit_tb_user_log" value="Submit" class="k-button button_css" />
                    </div>
                    <?php }?>

</form>
                </div>
            </div>
        </div>
  </div>
