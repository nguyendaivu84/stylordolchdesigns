<?php if(!isset($v_sval)) die();?>
<?php
    $v_company_id = isset($_POST['txt_company_id'])?$_POST['txt_company_id']:0;
    $v_quick_search = isset($_POST['quick_search'])?$_POST['quick_search']:0;
    settype($v_company_id,"int");
    settype($v_quick_search,"int");
    $arr_return = array("error"=>0,"message"=>"");
    $arr_where_clause = array();
    if($v_company_id > 0){
        if($v_quick_search >0){
            $v_ts = time() - $v_quick_search*3600*24;
            $arr_where_clause['log_datetime'] = array('$gte' => new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts ))),
                '$lte' => new MongoDate(strtotime(date("Y-m-d 23:59:59", time()))));
        }
        //$arr_where_clause['log_post'] = array('$or'=>array("",array()));
        $arr_where_clause['company_id'] = $v_company_id;
        $cls_tb_user_log->delete($arr_where_clause);
        insert_user_log($db,$arr_device,$arr_user,array("J",100),array("delete_record"),"user_account/admin/log/");
    }else{
        if($v_quick_search >0){
            $v_ts = time() - $v_quick_search*3600*24;
            $arr_where_clause['log_datetime'] = array('$gte' => new MongoDate(strtotime(date("Y-m-d 00:00:00", $v_ts ))),
                '$lte' => new MongoDate(strtotime(date("Y-m-d 23:59:59", time()))));
        }
        //$arr_where_clause['log_post'] = array('$or'=>array("",array()));
        $cls_tb_user_log->delete($arr_where_clause);
        insert_user_log($db,$arr_device,$arr_user,array("J",100),array("delete_record"),"user_account/admin/log/");
        //$arr_return = array("error"=>1,"message"=>"Company is not existed");
    }
    echo json_encode($arr_return);
?>