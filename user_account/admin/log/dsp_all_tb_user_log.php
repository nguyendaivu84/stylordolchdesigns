<?php if(!isset($v_sval)) die();?>
    <div id="div_body">
        <div id="div_splitter_content" style="height: 100%; width: 100%;">
            <div id="div_left_pane">
                <div class="pane-content">
                	<div id="div_treeview"></div>
                </div>
            </div>
            <div id="div_right_pane">
                <div class="pane-content">
                    <div id="div_title" class="k-block k-widget">
                        <h3>User_log</h3>
                    </div>
                    <div id="div_quick">
                    <div id="div_quick_search">
                    <form method="post" id="frm_quick_search">

                    <!--span class="k-textbox k-space-left" id="txt_quick_search"-->
                    <!--input type="text" name="txt_quick_search" placeholder="Search by ..." value="<?php echo isset($v_quick_search)?htmlspecialchars($v_quick_search):'';?>" /-->
                        <span style="float:right">
                            <select name="search_by_date" id="search_by_date">
                                <option value="0" <?php if($v_quick_search==0) echo "selected"; else echo ""; ?> >All</option>
                                <option value="1" <?php if($v_quick_search==1) echo "selected"; else echo ""; ?>>Yesterday</option>
                                <option value="3" <?php if($v_quick_search==3) echo "selected"; else echo ""; ?>>Last 3 day</option>
                                <option value="7" <?php if($v_quick_search==7) echo "selected"; else echo ""; ?>>Last week</option>
                                <option value="30" <?php if($v_quick_search==30) echo "selected"; else echo ""; ?>>Last month</option>
                            </select>
                        </span>
                        <!--a id="a_quick_search" style="cursor: pointer" class="k-icon k-i-search"></a-->
                        <script type="text/javascript">
                            $(document).ready(function(e){
                                $("#search_by_date").on("change",function(){
                                    var val = $(this).val();
                                    $("#txt_quick_search").val(val);
                                    $('form#frm_company_id').submit();
                                });
                                $("#search_by_date").width(190).kendoComboBox({});
                            });
                        </script>
                    <!--/span-->
                        <input type="hidden" name="txt_company_id" id="txt_company_id" value="<?php echo $v_company_id;?>" />
                    </form>
                    </div>
                    <div id="div_select">
                    <form id="frm_company_id" method="post">
                    Company: <select id="txt_company_id" name="txt_company_id" onchange="this.form.submit();">
                    <option value="0" selected="selected">-------</option>
                    <?php
					echo $v_dsp_company_option;
					?>
                    </select>
                    <input type="hidden" name="txt_quick_search" id="txt_quick_search" value="<?php echo $v_quick_search;?>" />
                    </form>
                    </div>
                    </div>


                    <div id="grid"></div>
                    <div class="k-block k-widget div_buttons">
                        <input id="btn_delete_log" name="btn_delete_log" type="button" value="Delete record" class="k-button button_css" />
                    </div>
                    <div id="advanced_search_window" style="display:none">
                    <h2>Advanced Search for User_log</h2>
                    <form id="frm_advanced_search" method="post" action="<?php echo URL.$v_admin_key;?>">
                    <table align="center" width="100%" border="1" class="list_table" cellpadding="3" cellspacing="0">
                    <tr align="left" valign="middle">
                    <td align="right">Company:</td>
                    <td>
                    <select id="txt_search_company_id" name="txt_search_company_id">
                    <option value="0" selected="selected">--------</option>
                    <?php echo $v_dsp_company_option;?>
                    </select>
                    </td>
                    </tr>
                    <tr align="center" valign="middle">
                    <td colspan="2">
                    <input type="submit" class="k-button k-button button_css" value="Search" name="btn_advanced_search" />
                    <input type="submit" class="k-button k-button button_css" value="Reset" name="btn_advanced_reset" />
                    </td>
                    </tr>
                    </table>
                    </form>
                    </div>
				<script type="text/javascript">
					var window_search;
                    $(document).ready(function() {
                        $("#btn_delete_log").on("click",function(){
                            var company_id = $("#txt_company_id").val();
                            var quick_search = $("#txt_quick_search").val();
                            $.ajax({
                                url:'<?php echo URL.$v_admin_key;?>/ajax'
                                ,type: 'POST'
                                ,data:{txt_ajax_type:'delete_record',txt_company_id:company_id,txt_time:quick_search}
                                ,beforeSend:function(){}
                                ,success:function(data){
                                    var ret = $.parseJSON(data);
                                    if(ret.error == 0){
                                        grid.dataSource.read();
                                    }else{
                                        alert(ret.message);
                                    }
                                }
                                ,error:function(error){alert(error.responseText);}

                            });
                        });
                        window_search = $('div#advanced_search_window');
                        $('li#icons_advanced_search').bind("click", function() {
                            if (!window_search.data("kendoWindow")) {
                                window_search.kendoWindow({
                                    width: "600px",
                                    actions: ["Maximize", "Close"],
                                    modal: true,
                                    title: "Advanced Search for User_log"
                                });
                            }
                            window_search.data("kendoWindow").center().open();
                        });

                        var grid = $("#grid").kendoGrid({
                            dataSource: {
                                pageSize: 20,
                                page: <?php echo (isset($v_page) && $v_page>0)?$v_page:1;?>,
                                serverPaging: true,
                                serverSorting: true,
                                transport: {
                                    read: {
                                        url: "<?php echo URL.$v_admin_key;?>/json/",
                                        type: "POST",
                                        data: {txt_session_id:"<?php echo session_id();?>",txt_quick_search: <?php echo $v_quick_search; ?>, txt_search_company_id: '<?php echo $v_company_id;?>'}
                                    }
                                },
                                schema: {
                                    data: "tb_user_log"
                                    ,total: function(data){
                                        return data.total_rows;
                                    }
                                },
                                type: "json"
                            },
                            pageSize: 20,
                            height: 430,
                            scrollable: true,
                            sortable: true,
                            //selectable: "single",
                            pageable: {
                                input: true,
                                refresh: true,
                                pageSizes: [10, 20, 30, 40, 50],
                                numeric: false
                            },
                        dataBound: on_data_bound,
						columns: [
							{field: "row_order", title: "&nbsp;", type:"int", width:"15px", sortable: false, template: '<span style="float:right">#= row_order #</span>'},
							//{field: "log_id", title: "Log Id", type:"int", width:"50px", sortable: true, template: '<span style="float:right">#= kendo.toString(log_id,"n0") #</span>'},
							{field: "user_name", title: "User Name", type:"string", width:"50px", sortable: true},
							{field: "company_name", title: "Company Name", type:"string", width:"70px", sortable: true},
							{field: "log_ipaddress", title: "Log Ipaddress", type:"string", width:"30px", sortable: true },
							{field: "log_url", title: "Log Url", type:"string", width:"100px", sortable: true },
							{field: "log_get", title: "Log Get", type:"string", width:"50px", sortable: true },
							{field: "log_post", title: "Log Post", type:"string", width:"50px", sortable: true },
							{field: "log_device", title: "User device", type:"string", width:"100px", sortable: true },
							{field: "log_datetime", title: "Log Datetime", type:"string", width:"50px", sortable: true, template: '<span style="float:right">#= log_datetime #</span>'},
							{ command:  [
								{ name: "View", text:'', click: view_row, imageClass: 'k-grid-View' }
								<?php if($v_edit_right || $v_is_super_admin){?>
								,{ name: "Edit", text:'', click: edit_row, imageClass: 'k-grid-Edit' }
								<?php }?>
								<?php if($v_delete_right || $v_is_super_admin){?>
								,{ name: "Delete", text:'', click: delete_row, imageClass: 'k-grid-Delete' }
								<?php }?>
								],
								title: " ", width: "70px" }
						 ]
					 }).data("kendoGrid");
				});
              function view_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.log_id+"/view";
                }
                function edit_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to edit user_log with ID: '+dataItem.log_id+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.log_id+"/edit";
                    }
                }
                function delete_row(e) {
                    e.preventDefault();
                    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
                    if(confirm('Do you want to delete user_log with ID: '+dataItem.log_id+'?')){
                        document.location = "<?php echo URL.$v_admin_key.'/';?>"+dataItem.log_id+"/delete";
                    }
                }
                function on_data_bound(e){
                    var grid = e.sender;
                    var data = grid.dataSource.view();
                    for(var i=0; i<data.length; i++){
                        if(data[i].log_post=='delete_record'){
                            var $a = grid.tbody.find("tr[data-uid='" + data[i].uid + "'] a.k-grid-Delete");
                            $a.css('display', 'none');
                        }
                    }
                }
            </script>
                </div>
            </div>
        </div>
  </div>