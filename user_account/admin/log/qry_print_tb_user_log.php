<?php if(!isset($v_sval)) die();?>
<?php
if(isset($_SESSION['ss_location_where_clause'])){
	$v_where_clause = $_SESSION['ss_tb_user_log_where_clause'];
	$arr_where_clause = unserialize($v_where_clause);
}
if(!isset($arr_where_clause) || !is_array($arr_where_clause)) $arr_where_clause = array();
if(isset($_SESSION['ss_tb_user_log_sort'])){
	$v_sort = $_SESSION['ss_tb_user_log_sort'];
	$arr_sort = unserialize($v_sort);
}
if(!isset($arr_sort) || !is_array($arr_sort)) $arr_sort = array();
$arr_tb_user_log = $cls_tb_user_log->select($arr_where_clause, $arr_sort);
$v_dsp_tb_user_log = '<table class="list_table" width="100%" cellpadding="3" cellspacing="0" border="0" align="center">';

$v_dsp_tb_user_log .= '<tr align="center" valign="middle">';
$v_dsp_tb_user_log .= '<th>Ord</th>';
$v_dsp_tb_user_log .= '<th>Log Id</th>';
$v_dsp_tb_user_log .= '<th>User Id</th>';
$v_dsp_tb_user_log .= '<th>User Password</th>';
$v_dsp_tb_user_log .= '<th>Log Ipaddress</th>';
$v_dsp_tb_user_log .= '<th>Log Url</th>';
$v_dsp_tb_user_log .= '<th>Log Datetime</th>';
$v_dsp_tb_user_log .= '</tr>';
$v_count = 1;
foreach($arr_tb_user_log as $arr){
	$v_dsp_tb_user_log .= '<tr align="left" valign="middle">';
	$v_dsp_tb_user_log .= '<td align="right">'.($v_count++).'</td>';
	$v_log_id = isset($arr['log_id'])?$arr['log_id']:0;
	$v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
	$v_user_password = isset($arr['user_password'])?$arr['user_password']:'';
	$v_log_ipaddress = isset($arr['log_ipaddress'])?$arr['log_ipaddress']:'';
	$v_log_url = isset($arr['log_url'])?$arr['log_url']:'';
	$v_log_datetime = isset($arr['log_datetime'])?$arr['log_datetime']:(new MongoDate(time()));
	$v_dsp_tb_user_log .= '<td>'.$v_log_id.'</td>';
	$v_dsp_tb_user_log .= '<td>'.$v_user_id.'</td>';
	$v_dsp_tb_user_log .= '<td>'.$v_user_password.'</td>';
	$v_dsp_tb_user_log .= '<td>'.$v_log_ipaddress.'</td>';
	$v_dsp_tb_user_log .= '<td>'.$v_log_url.'</td>';
	$v_dsp_tb_user_log .= '<td>'.$v_log_datetime.'</td>';
	$v_dsp_tb_user_log .= '</tr>';
}
$v_dsp_tb_user_log .= '</table>';
?>