<?php
/* Constants changed on server */
if ($_SERVER['SERVER_NAME'] == 'localhost')
	define('URL','http://'.$_SERVER['SERVER_NAME'].'/new_stylor');
else
	define('URL','http://'.$_SERVER['SERVER_NAME'].'/');

define('INKSCAPE_PATH','inkscape');
/* End: Constants changed on server */

define('ROOT_DIR', getcwd());
define('DS', DIRECTORY_SEPARATOR);
define('LOG_DIR', ROOT_DIR.DS.'logs');

define('UPLOAD_DIR', ROOT_DIR.DS.'upload');
define('ASSET_DIR', UPLOAD_DIR.DS.'assets');
define('DESIGN_DIR', ASSET_DIR.DS.'designs');
define('DESIGN_TEMPLATE_DIR', DESIGN_DIR.DS.'templates');//save images for template
define('DESIGN_THEME_DIR', DESIGN_DIR.DS.'themes');//save images for theme
define('DESIGN_FONT_DIR', DESIGN_DIR.DS.'fonts');//save font for template
define('DESIGN_TEMP_DIR', DESIGN_DIR.DS.'temps'); // save preview
define('DESIGN_DESIGN_DIR', DESIGN_DIR.DS.'designs'); // save images for design
define('DESIGN_FOTOLIA_DIR', DESIGN_DIR.DS.'fotolia'); // save download images from fotolia
define('DESIGN_STOCK_DIR', DESIGN_DIR.DS.'stocks'); // save stocks images
define('DESIGN_USER_DIR', DESIGN_DIR.DS.'users'); // save user upload images
define('DESIGN_DATA_DIR', DESIGN_DIR.DS.'data'); // contain data for converting image
define('DESIGN_SVG_DIR', DESIGN_DIR . DS .'svg');//contain data for SVG images 
define('DESIGN_MAX_SIZE_UPLOAD', 10485760); // save user upload images
define('DESIGN_IMAGE_THUMB_SIZE', 200); // save user upload images
define('DESIGN_IMAGE_SMALL_SIZE', 300); // save user upload images
define('DESIGN_IMAGE_NORMAL_SIZE', 450); // save user upload images
define('DESIGN_NORMAL_PREFIX', 'normal_');
define('DESIGN_THUMB_PREFIX', 'thumb_');
define('DESIGN_SMALL_PREFIX', 'small_');
define('IMAGE_ORIGINAL_PREFIX', 'original_');
define('IMAGE_MAX_SIZE_USE', 1048576);
define('IMAGE_MAX_WIDTH_USE', 1000);
define('IMAGE_MAX_HEIGHT_USE', 1000);

define('STRANGER_KEY', 'GO_HELL');
define('DESIGN_SITE_CODE', 'AV'); //
define('DESIGN_SIDE_FACE', 'admin_panel'); //
define('DESIGN_FOTOLIA_KEY', 'yfi64LivT5TeR6HDRPTrKobgLDDt8wOx');

define('THEMES_SAVED', 'user.themes.txt');

/* Set up variables */
$arr_setting_type=array();
$arr_setting_type[0]="-------";
$arr_setting_type[1]="Develop";
$arr_setting_type[2]="Template";

?>
