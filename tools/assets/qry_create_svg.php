<?php
if(!isset($v_sval)) die();?>
<?php
$v_svg_id = isset($_GET['svg_id'])?$_GET['svg_id']:'0';
$v_rotation = isset($_GET['rotation'])?$_GET['rotation']:'0';
$v_color = isset($_GET['colors'])?$_GET['colors']:'';
//$v_border_color = isset($_GET['border_color'])?$_GET['border_color']:'';
//$v_border_width = isset($_GET['border_width'])?$_GET['border_width']:'0';

//svg_id="+jimage.svg_id+"&rotation="+jimage.rotation+"&colors

//if($v_filter=='undefined') $v_filter = 'none';

settype($v_svg_id, 'int');
settype($v_rotation, 'int');
add_class('cls_tb_design_svg');
$cls_svg = new cls_tb_design_svg($db, LOG_DIR);
$v_row = $cls_svg->select_one(array('svg_id'=>$v_svg_id,'svg_status'=>0));

$v_image_name = '';
$v_image_extension = 'svg';
$v_image_type = 0;

$image = new Imagick();
if($v_row==1){
    $v_saved_dir = $cls_svg->get_saved_dir();
    $v_svg_data = $cls_svg->get_svg_data();
    $arr_current_colors = $cls_svg->get_svg_colors();

    if($v_color!='')
        $arr_colors = explode(',', $v_color);
    else
        $arr_colors = array();
    for($i=0; $i < sizeof($arr_colors) && $i <sizeof($arr_current_colors); $i++){
        $v_color = $arr_colors[$i];
        $v_current_color = '#'.$arr_current_colors[$i];
        if(strlen($v_color)==6) $v_color = '#'.$v_color;
        $v_svg_data = str_ireplace($v_current_color, $v_color, $v_svg_data);
    }
    $image->setbackgroundcolor(new ImagickPixel('transparent'));
    $image->readimageblob($v_svg_data);
    $image->setimageformat('png32');

}else{
    $v_full_path = DESIGN_DIR. DS. 'missing.png';
    $v_image_name = 'missing.png';
    $v_image_type = 3;
    $v_image_extension = 'png';
    $image->readimage($v_full_path);
}
if($v_rotation!=0){
    $pixel = new ImagickPixel('transparent');
    $image->rotateimage($pixel, $v_rotation);
}
$image->setimageformat('png');
header('Content-type: image/png');
echo $image;

$image->clear();
$image->destroy();