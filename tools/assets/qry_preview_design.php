<?php
$v_sval = 1;

$v_json = isset($_REQUEST['json'])?$_REQUEST['json']:'';
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:'0';
$v_dpi = isset($_REQUEST['dpi'])?$_REQUEST['dpi']:'0';
$v_hide = isset($_REQUEST['hide'])?$_REQUEST['hide']:'0';
$v_ext = isset($_REQUEST['ext'])?$_REQUEST['ext']:'png';
$v_see_canvas = isset($_REQUEST['see_canvas'])?$_REQUEST['see_canvas']:0;

settype($v_page, 'int');
settype($v_dpi, 'int');
settype($v_hide, 'int');

$v_hide = $v_hide==1;

$arr_json = json_decode($v_json, true);
//change for see canvas
if($v_see_canvas==1){
	$arr_json['width']=$arr_json['canvases'][0]['width'];
	$arr_json['height']=$arr_json['canvases'][0]['height'];
	$arr_json['canvases'][0]['bleed'] = 0;
}

add_class('cls_tb_design_image');
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
$image = new Imagick();

$cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);

$v_original_name = 'tmp_'.session_id().'_'.date('YmdHis');
$v_dir = DESIGN_TEMP_DIR.DS;
$v_url_dir = ROOT_DIR.DS;
$v_url_dir = str_replace($v_url_dir,'',$v_dir);

$v_file_name = $v_original_name.'.'.$v_ext;
$v_full_path = $v_dir.$v_file_name;
$v_accept = false;
$i = 1;
while(!$v_accept){
    $v_accept = !file_exists($v_full_path);
    if(!$v_accept){
        $v_file_name = $v_original_name.'('.($i++).').'.$v_ext;
        $v_full_path = $v_dir.$v_file_name;
    }
}

$image->setimageformat($v_ext);
$image->writeimage($v_full_path);

$v_width = $image->getimagewidth();
$v_height = $image->getimageheight();
$image->clear();
$image->destroy();
$arr_return = array('file'=>URL.str_replace(DS,'/', $v_url_dir).$v_file_name, 'page'=>$v_page, 'hide'=>$v_hide);
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');