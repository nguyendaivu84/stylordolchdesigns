<?php if(!isset($v_sval)) die();?>
<?php
$arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
$arr_design = isset($arr_canvas[$v_page])?$arr_canvas[$v_page]:array();

$v_folding_type = isset($arr_json['folding'])?$arr_json['folding']:'none';
$v_die_cut_type = isset($arr_json['dieCutType'])?$arr_json['dieCutType']:'none';
if($v_die_cut_type=='undefined' || $v_die_cut_type=='') $v_die_cut_type = 'none';
$v_folding_direction = isset($arr_json['foldingDirection'])?$arr_json['foldingDirection']:'none';
$v_bleed = isset($arr_design['bleed'])?$arr_design['bleed']:'0';
settype($v_bleed, 'float');
$v_page_count = 1;
if($v_bleed>0){
    if($v_folding_type!='none' && $v_die_cut_type=='none'){
        if(in_array($v_folding_type, array('zFold', 'triFold','letterFold'))){
            $v_page_count = 3;
        }else if(in_array($v_folding_type, array('accordionFold','rollFold'))){
            $v_page_count = 4;
        }
    }
}
$v_width = isset($arr_design['width'])?$arr_design['width']:0;
$v_height = isset($arr_design['height'])?$arr_design['height']:0;
$v_bg_color = isset($arr_design['bg_color'])?$arr_design['bg_color']:'transparent';
if(strlen($v_bg_color)!=6)
    $v_bg_color = 'transparent';
else
    $v_bg_color = '#'.$v_bg_color;

$v_width = ceil($v_width * $v_dpi);
$v_height = ceil($v_height * $v_dpi);
if($v_width<=0) $v_width = 50;
if($v_height<=0) $v_height = 50;


$image = new Imagick();
$pixel = new ImagickPixel($v_bg_color);
$image->newimage($v_width, $v_height, $pixel);

$cls_draw = new cls_draw();

if(isset($_SESSION['ss_design_face']) && $_SESSION['ss_design_face']=='back-end') $v_is_template = 1;

//Gradient if exists
if($v_page_count>1){
    $j = 0;
    if($v_folding_direction=='vertical'){
        $v_distance = ceil($v_width/$v_page_count);
        for($i=$v_page_count; $i>=0; $i--){
            $v_tmp_height = ceil($v_distance/6);
            $v_tmp_width = $v_height;
            $v_gradient_string = $j%2==0?'gradient:white-lightgray':'gradient:lightgray-white';
            $v_tmp_x_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
            $v_tmp_y_pos = 0;
            $v_rotation = 90;
            $cls_draw->set_x_pos($v_tmp_x_pos);
            $cls_draw->set_y_pos($v_tmp_y_pos);
            $cls_draw->set_width($v_tmp_width);
            $cls_draw->set_height($v_tmp_height);
            $cls_draw->gradient($image, $v_gradient_string, $v_rotation);

            $j++;
        }
    }else{
        $v_distance = ceil($v_height/$v_page_count);
        for($i=$v_page_count; $i>=0; $i--){
            $v_tmp_height = ceil($v_distance/6);
            $v_tmp_width = $v_width;
            $v_gradient_string = $j%2==0?'gradient:lightgray-white':'gradient:white-lightgray';
            $v_tmp_y_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
            $v_tmp_x_pos = 0;
            $v_rotation = 0;
            $cls_draw->set_x_pos($v_tmp_x_pos);
            $cls_draw->set_y_pos($v_tmp_y_pos);
            $cls_draw->set_width($v_tmp_width);
            $cls_draw->set_height($v_tmp_height);
            $cls_draw->gradient($image, $v_gradient_string, $v_rotation);

            $j++;
        }
    }
}


//Images
$arr_images = isset($arr_design['images'])?$arr_design['images']:array();
if(count($arr_images)>1) $arr_images = $cls_draw-> record_sort($arr_images, 'zindex');
//$v_str = '';

add_class('cls_tb_design_image');
add_class('cls_tb_design_font');
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$cls_font = new cls_tb_design_font($db, LOG_DIR);


$v_root_dir = ROOT_DIR.DS;
$v_font_dir = DESIGN_FONT_DIR.DS;


for($i=0; $i<count($arr_images);$i++){
    $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
    $v_width = isset($arr_images[$i]['width'])?$arr_images[$i]['width']:0;
    $v_height = isset($arr_images[$i]['height'])?$arr_images[$i]['height']:0;
    $v_rotation = isset($arr_images[$i]['rotation'])?$arr_images[$i]['rotation']:0;
    $v_left = isset($arr_images[$i]['left'])?$arr_images[$i]['left']:0;
    $v_top = isset($arr_images[$i]['top'])?$arr_images[$i]['top']:0;
    $v_crop = isset($arr_images[$i]['crop'])?$arr_images[$i]['crop']:false;
    $v_noprint = isset($arr_images[$i]['noprint']);
    if($v_noprint) $v_noprint = $arr_images[$i]['noprint'];
    $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
    $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
    $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
    $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;
    $v_border_width = isset($arr_images[$i]['border_width'])?$arr_images[$i]['border_width']:0;
    $v_border_color = isset($arr_images[$i]['border_color'])?$arr_images[$i]['border_color']:'transparent';

    if(strlen($v_border_color)<6)
        $v_border_color = 'transparent';
    else{
        $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
    }
    $v_border_width = ceil($v_border_width);
    settype($v_border_width, 'float');
    settype($v_cropLeft, 'float');
    settype($v_cropTop, 'float');
    settype($v_cropWidth, 'float');
    settype($v_cropHeight, 'float');

    $v_top *= $v_dpi;
    $v_left *= $v_dpi;
    $v_width *= $v_dpi;
    $v_height *= $v_dpi;
    $v_width = ceil($v_width);
    $v_height = ceil($v_height);
    $v_top = ceil($v_top);
    $v_left = ceil($v_left);
    $v_noprint = $v_noprint && is_admin()==0;
    if(!$v_noprint){
        if($v_image_id>0){
            $v_row = $cls_image->select_one(array('image_id'=>$v_image_id));

            if($v_row==1){
                $v_directory = $cls_image->get_saved_dir();
                $v_directory = $v_root_dir.$v_directory;
                $v_image = $cls_image->get_image_file();
                $v_image = $v_directory.$v_image;
                if(file_exists($v_image)){
                    if($v_border_width>0){
                        $v_border_width = ceil($v_border_width*$v_dpi/(72*1.2));
                    }
                    $cls_draw->set_stroke_color($v_border_color);
                    $cls_draw->set_stroke_width($v_border_width);
                    $cls_draw->set_x_pos($v_left);
                    $cls_draw->set_y_pos($v_top);
                    $cls_draw->set_width($v_width);
                    $cls_draw->set_rotation($v_rotation);
                    $cls_draw->set_height($v_height);
                    $cls_draw->add_image($image, $v_image, $v_cropLeft, $v_cropTop, $v_cropWidth, $v_cropHeight);
                }
            }

        }else{
            $v_shape_type = isset($arr_images[$i]['shape_type'])?$arr_images[$i]['shape_type']:'hline';
            $v_border_stroke = isset($arr_images[$i]['border_stroke'])?$arr_images[$i]['border_stroke']:'solid';
            $v_shape_fill_color = isset($arr_images[$i]['fill_color'])?$arr_images[$i]['fill_color']:'transparent';

            $v_shape_fill_color = trim($v_shape_fill_color);

            if(strlen($v_shape_fill_color)<6)
                $v_shape_fill_color = 'transparent';
            else{
                $v_shape_fill_color = $v_shape_fill_color == 'transparent'?$v_shape_fill_color: '#'.substr($v_shape_fill_color, 0, 6);
            }

            $cls_draw->set_x_pos($v_left);
            $cls_draw->set_y_pos($v_top);
            $cls_draw->set_width($v_width);
            $cls_draw->set_height($v_height);
            $cls_draw->set_fill_color($v_shape_fill_color);
            $cls_draw->set_stroke_color($v_border_color);
            $cls_draw->set_stroke_width($v_border_width);
            $cls_draw->set_stroke_style($v_border_stroke);
            $cls_draw->set_rotation(0);
            $cls_draw->add_shape($image, $v_shape_type);

        }
    }

}

$arr_texts = isset($arr_design['texts'])?$arr_design['texts']:array();

for($i=0; $i<count($arr_texts);$i++){

    $v_color = isset($arr_texts[$i]['color'])?$arr_texts[$i]['color']:'black';
    if(strlen($v_color)<6)
        $v_color = 'black';
    else{
        $v_color = '#'.substr($v_color,0,6);
    }
    $v_top = (float) $arr_texts[$i]['top'];
    $v_left = (float) $arr_texts[$i]['left'];
    $v_width = (float) $arr_texts[$i]['width'];
    $v_height = (float) $arr_texts[$i]['height'];
    $v_body = isset($arr_texts[$i]['body'])?$arr_texts[$i]['body']:'';
    $v_bold = isset($arr_texts[$i]['bold'])?$arr_texts[$i]['bold']:false;
    $v_italic = isset($arr_texts[$i]['italic'])?$arr_texts[$i]['italic']:false;
    $v_gravity = isset($arr_texts[$i]['gravity'])?$arr_texts[$i]['gravity']:'center';
    $v_rotation = isset($arr_texts[$i]['rotation'])?$arr_texts[$i]['rotation']:0;
    $v_point_size = isset($arr_texts[$i]['pointsize'])?$arr_texts[$i]['pointsize']:0;

    $v_bullet_style = isset($arr_texts[$i]['bullet_style'])?$arr_texts[$i]['bullet_style']:'undefined';


    if($v_body==''){
        if(isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE){
            $v_body = isset($arr_texts[$i]['preset_text'])?$arr_texts[$i]['preset_text']:'';
        }
    }
    $v_point = $v_point_size;
    $v_point_size = ceil($v_point_size * $v_dpi/72);
    $v_font_name = $v_font_key = '';
    $v_font_name =  isset($arr_texts[$i]['font'])?$arr_texts[$i]['font']:'Arial';
    $v_font_key = seo_friendly_url($v_font_name);
    //$v_row = $cls_font->select_one(' AND `font_key`='."'{$v_font_key}' AND `font_regular`=1");
    $v_default_font = $v_font_dir.'arial'.DS.'regular.ttf';
    //if($v_row==1){
    //$v_bold = $cls_font->get_font_bold();
    //$v_italic = $cls_font->get_font_italic();
    //$v_bold_italic = $cls_font->get_font_bold_italic();
    if($v_bold && $v_italic){
        $v_font = $v_font_dir.$v_font_key.DS.'both.ttf';
    }else if($v_bold && !$v_italic)
        $v_font = $v_font_dir.$v_font_key.DS.'bold.ttf';
    else if(!$v_bold && $v_italic)
        $v_font = $v_font_dir.$v_font_key.DS.'italic.ttf';
    else
        $v_font = $v_font_dir.$v_font_key.DS.'regular.ttf';
    if(!file_exists($v_font)) $v_font = $v_default_font;
    //}else
    //    $v_font = $v_default_font;

    //$v_body = html_entity_decode($v_body);
    //$v_body = urldecode($v_body);

    $v_top *= $v_dpi;
    $v_left *= $v_dpi;
    $v_width *= $v_dpi;
    $v_height *= $v_dpi;

    $v_top = ceil($v_top);
    $v_left = ceil($v_left);
    $v_width = ceil($v_width);
    $v_height = ceil($v_height);

    if($v_rotation==90 || $v_rotation==270){
        $v_tmp = $v_width;
        $v_width = $v_height;
        $v_height = $v_tmp;
    }
    //$cls_draw->set_fill_color($v_color);
    //$cls_draw->set_height($v_height);
    //$cls_draw->set_width($v_width);
    //$cls_draw->set_rotation($v_rotation);
    //$cls_draw->set_x_pos($v_left);
    //$cls_draw->set_y_pos($v_top);

    //$cls_draw->add_text($image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style );

    $cls_draw->set_fill_color($v_color);
    $cls_draw->set_height($v_height);
    $cls_draw->set_width($v_width);
    $cls_draw->set_rotation($v_rotation);
    $cls_draw->set_x_pos($v_left);
    $cls_draw->set_y_pos($v_top);

    $tmp_image = new Imagick();

    $cls_draw->text($tmp_image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style);
    $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $v_left, $v_top);
    $tmp_image->clear();
    $tmp_image->destroy();
}