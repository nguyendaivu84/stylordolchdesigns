<?php
if(!isset($v_sval)) die();

$v_json = isset($_POST['json'])?$_POST['json']:'';
$v_dpi = isset($_POST['dpi'])?$_POST['dpi']:0;
$v_page = isset($_POST['page'])?$_POST['page']:0;
$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:0;
$v_theme_id = isset($_POST['theme_id'])?$_POST['theme_id']:0;
$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:0;
settype($v_dpi, 'int');
settype($v_page, 'int');
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');
if($v_page != 1) $v_page = 0;
if($v_json!=''){
    if(get_magic_quotes_gpc()) $v_json = stripslashes($v_json);
    $arr_json = json_decode($v_json, true);
}else{
    $arr_json = array();
}
if(!is_array($arr_json)) $arr_json = array();


$arr_json = json_decode($v_json, true);
add_class('cls_tb_design_image');
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
$image = new Imagick();

$cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);

$v_ext = 'pdf';
$v_original_name = 'tmp_'.session_id().'_'.date('YmdHis');
$v_dir = DESIGN_TEMP_DIR.DS;
$v_url_dir = ROOT_DIR.DS;
$v_url_dir = str_replace($v_url_dir,'',$v_dir);

$v_file_name = $v_original_name.'.'.$v_ext;
$v_full_path = $v_dir.$v_file_name;
$v_accept = false;
$i = 1;
while(!$v_accept){
    $v_accept = !file_exists($v_full_path);
    if(!$v_accept){
        $v_file_name = $v_original_name.'('.($i++).').'.$v_ext;
        $v_full_path = $v_dir.$v_file_name;
    }
}
$image->setimageformat($v_ext);
$image->writeimage($v_full_path);

$image->clear();
$image->destroy();
redir(URL.'download/'.$v_file_name);