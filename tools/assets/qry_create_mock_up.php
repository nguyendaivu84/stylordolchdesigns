<?php
if(!isset($v_sval)) die();?>
<?php
$v_image_id = isset($_GET['image_id'])?$_GET['image_id']:'0';
$v_rotation = isset($_GET['rotation'])?$_GET['rotation']:'0';
$v_filter = isset($_GET['filter'])?$_GET['filter']:'none';
$v_flip = isset($_GET['flip'])?$_GET['flip']:'none';
$v_border_color = isset($_GET['border_color'])?$_GET['border_color']:'';
$v_border_width = isset($_GET['border_width'])?$_GET['border_width']:'0';
$v_width = isset($_GET['width'])?$_GET['width']:'0';

if($v_filter=='undefined') $v_filter = 'none';

if(strlen($v_border_color)<6)
    $v_border_color = 'transparent';
else{
    $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
}

settype($v_image_id, 'int');
settype($v_rotation, 'int');
settype($v_border_width, 'int');
settype($v_width, 'int');
add_class('cls_tb_design_image');
$cls_images = new cls_tb_design_image($db, LOG_DIR);
$v_row = $cls_images->select_one(array('image_id'=>$v_image_id,'image_status'=>0));

$v_image_name = '';
$v_image_extension = 'png';
$v_image_type = 3;
$arr_type = array(
    'image/jpeg', 'image/gif', 'image/jpeg', 'image/png'
);
if($v_row==1){
    $v_saved_dir = $cls_images->get_saved_dir();
    $v_image_name = $cls_images->get_image_file();
    $v_image_extension = $cls_images->get_image_extension();
    $v_image_type = $cls_images->get_image_type();
    $v_full_path = $v_root_dir. $v_saved_dir. $v_image_name;

    if(!file_exists($v_full_path)) $v_row = 0;
}
if($v_row==0){
    $v_full_path = DESIGN_DIR. DS. 'missing.png';
    $v_image_name = 'missing.png';
    $v_image_type = 3;
    $v_image_extension = 'png';
}

$v_temp = false;
if($v_filter!='none'){
    add_class('cls_instagraph');
    $cls_instagraph = new cls_instagraph();
    $cls_instagraph->set_input($v_full_path);
    $v_output = DESIGN_TEMP_DIR.DS.date('YmdHis').'.'.$v_image_extension;
    $cls_instagraph->set_output($v_output);
    $v_filter = ucwords($v_filter);
    if($cls_instagraph->process($v_filter)){
        $v_full_path = $v_output;
        //$cls_instagraph->output();
        $v_temp = true;
    }
}

add_class('cls_draw');
$cls_draw = new cls_draw();
$image = new Imagick($v_full_path);
$pixel = new ImagickPixel('transparent');
$cls_draw->flip($image, $v_flip);

/*
if($v_border_width>0){
    $v_ratio = $image->getimagewidth() / $v_width;
    $v_border_width *= .013836 * 2;
    $v_border_width = ceil($v_border_width);
    $draw = new ImagickDraw();
    $draw->setstrokecolor(new ImagickPixel($v_border_color));
    $draw->setstrokewidth($v_border_width);
    $draw->setfillcolor(new ImagickPixel('transparent'));
    $draw->rectangle($v_border_width/2,$v_border_width/2,$image->getimagewidth() - $v_border_width/2, $image->getimageheight() - $v_border_width/2);
    $image->drawimage($draw);
    $draw->clear();
    $draw->destroy();
}
*/
if($v_rotation!=0) $image->rotateimage($pixel, $v_rotation);
$image->setimageformat('png');
header('Content-type: image/png');
echo $image;

$image->clear();
$image->destroy();
if($v_temp){
    if(file_exists($v_full_path)) @unlink($v_full_path);
}