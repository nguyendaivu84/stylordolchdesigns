<?php
if(!isset($v_sval)) die();
?>
<?php
$v_width = isset($_GET['width'])?$_GET['width']:'0';
$v_height = isset($_GET['height'])?$_GET['height']:'0';
$v_dpi = isset($_GET['dpi'])?$_GET['dpi']:'0';
$v_page = isset($_GET['page'])?$_GET['page']:'0';
$v_type = isset($_GET['dieCutType'])?$_GET['dieCutType']:'minor';
$v_bleed = isset($_GET['bleed'])?$_GET['bleed']:'0';

settype($v_width, 'float');
settype($v_height, 'float');
settype($v_dpi, 'int');
settype($v_page, 'int');
if(!in_array($v_page, array(0,1))) $v_page = 0;
settype($v_bleed, 'float');


$v_width = ceil($v_width*$v_dpi);
$v_height = ceil($v_height*$v_dpi);
$v_bleed = ceil($v_bleed*$v_dpi);
if($v_type=='minor')
    $v_distance = ceil(0.25*$v_dpi);
else
    $v_distance = $v_dpi;
$image = new Imagick();
$pixel = new ImagickPixel('transparent');

$v_new_width = $v_width + 2*$v_bleed;
$v_new_height = $v_height + 2*$v_bleed;

$image->newimage($v_new_width, $v_new_height, $pixel);

$cls_draw->set_fill_color('transparent');
$cls_draw->set_stroke_color('red');
$cls_draw->set_width($v_width);
$cls_draw->set_height($v_height);
$cls_draw->set_stroke_style('solid');
$cls_draw->set_stroke_width(1);
$cls_draw->set_rotation(0);
$cls_draw->create_die_cut($image, $v_bleed, $v_type, $v_page);

$image->setimageformat('png');    // Give the image a format

header('Content-type: image/png');     // Prepare the web browser to display an image
echo $image;

$pixel->clear();
$pixel->destroy();
$image->clear();
$image->destroy();
?>