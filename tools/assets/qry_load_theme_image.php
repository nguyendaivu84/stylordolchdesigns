<?php
if(!isset($v_sval)) die();
$v_theme_id = isset($_GET['theme_id'])?$_GET['theme_id']:'0';
settype($v_theme_id, 'int');
$arr_return = array('success'=>0, 'href'=>'');

add_class('cls_tb_design_theme');
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);
if($v_theme_id>0){
    $v_row = $cls_theme->select_one(array('theme_id'=>$v_theme_id));
    if($v_row==1){
        $v_saved_dir = $cls_theme->get_saved_dir();
        $v_sample_image = $cls_theme->get_sample_image();
        if(file_exists($v_saved_dir.$v_sample_image) && is_file($v_saved_dir.$v_sample_image)){
            $arr_return['success'] = 1;
            $arr_return['href'] = URL.$v_saved_dir.$v_sample_image;
        }
    }
}
$cls_output->output($arr_return);