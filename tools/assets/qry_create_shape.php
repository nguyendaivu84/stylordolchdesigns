<?php
if(!isset($v_sval)) die();

$v_shape_type = isset($_GET['shape_type'])?$_GET['shape_type']:'rect';
$v_dpi = isset($_GET['dpi'])?$_GET['dpi']:19;
settype($v_dpi, 'int');
$v_width = isset($_GET['width'])?$_GET['width']:8;
settype($v_width, 'float');
$v_height = isset($_GET['height'])?$_GET['height']:8;
settype($v_height, 'float');
$v_border_width = isset($_GET['border_width'])?$_GET['border_width']:2;
settype($v_border_width, 'int');
$v_border_color = isset($_GET['border_color'])?$_GET['border_color']:'000000';
$v_rotation = isset($_GET['rotation'])?$_GET['rotation']:'0';
settype($v_rotation, 'int');
$v_fill_color = isset($_GET['fill_color'])?$_GET['fill_color']:'FFFFFF';
$v_border_stroke = isset($_GET['border_stroke'])?$_GET['border_stroke']:'solid';
$v_border_width = isset($_GET['border_width'])?$_GET['border_width']:'1';
settype($v_border_width, 'float');
if($v_border_width<0) $v_border_width = 0;
$v_flip = isset($_GET['flip'])?$_GET['flip']:'none';

//if(strlen($v_border_color)!=6) $v_border_color = '000000';
//if(strlen($v_fill_color)!=6) $v_fill_color = 'FFFFFF';
//if($v_border_color=='000000') $v_border_color = '010101';

if($v_border_color!='transparent') $v_border_color = '#'.$v_border_color;
if($v_fill_color!='transparent') $v_fill_color = '#'.$v_fill_color;

$v_width = ceil($v_width*$v_dpi);
$v_height = ceil($v_height*$v_dpi);
$v_border_width = ceil($v_border_width);
try{
    //$v_border_color = new ImagickPixel("#{$v_border_color}");

    $image = new Imagick();
    $pixel = new ImagickPixel( 'transparent' );

    $cls_draw = new cls_draw();
    $cls_draw->set_fill_color($v_fill_color);
    $cls_draw->set_height($v_height);
    $cls_draw->set_width($v_width);
    $cls_draw->set_rotation(0);
    $cls_draw->set_stroke_color($v_border_color);
    $cls_draw->set_stroke_style($v_border_stroke);
    $cls_draw->set_stroke_width($v_border_width);

    $image->newimage($v_width, $v_height, $pixel);
    if($v_shape_type=='hline' || $v_shape_type=='vline'){
        $cls_draw->line($image, $v_shape_type, $v_rotation);
    }else if($v_shape_type=='circle'){
        $cls_draw->ellipse($image, $v_rotation);
    }else if($v_shape_type=='triangle'){
        $cls_draw->triangle($image, $v_rotation);
    }else if($v_shape_type=='rounded'){
        $cls_draw->rounded_rectangle($image, 10, $v_rotation);
    }else if($v_shape_type=='ellipse'){
        $cls_draw->ellipse($image, $v_rotation);
    }else if($v_shape_type=='polygon'){
        $cls_draw->polygon($image, $v_width/2, $v_height/2, min($v_width, $v_height)/2, 6, $v_rotation);
    }else{
        $cls_draw->rectangle($image, $v_rotation);
    }
    $cls_draw->flip($image, $v_flip);
    $image->setimageformat( "png" );
    header( "Content-Type: image/png" );
    echo $image;
    $image->clear();
    $image->destroy();
}catch(Exception $e){

    echo $e->getMessage();
}