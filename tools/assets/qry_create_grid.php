<?php
if(!isset($v_sval)) die();
?>
<?php
$v_width = isset($_GET['width'])?$_GET['width']:'0';
$v_height = isset($_GET['height'])?$_GET['height']:'0';
$v_dpi = isset($_GET['dpi'])?$_GET['dpi']:'0';
$v_page = isset($_GET['page'])?$_GET['page']:'0';
$v_type = isset($_GET['type'])?$_GET['type']:'minor';
$v_bleed = isset($_GET['bleed'])?$_GET['bleed']:'0';

settype($v_width, 'float');
settype($v_height, 'float');
settype($v_dpi, 'int');
settype($v_page, 'int');
if(!in_array($v_page, array(0,1))) $v_page = 1;
settype($v_bleed, 'float');


$v_width = ceil($v_width*$v_dpi);
$v_height = ceil($v_height*$v_dpi);
$v_bleed = ceil($v_bleed*$v_dpi);
if($v_type=='minor')
    $v_distance = ceil(0.25*$v_dpi);
else
    $v_distance = $v_dpi;

$image = new Imagick();
$pixel = new ImagickPixel('transparent');
$image->newimage($v_width, $v_height, $pixel);
$v_gray = new ImagickPixel('gray');
$draw = new ImagickDraw();

$draw->setstrokewidth(1);
$draw->setfillcolor($v_gray);
$draw->setstrokecolor($v_gray);

for($i = $v_bleed + $v_distance; $i<$v_width; $i+=$v_distance){
    $v_x1 = $i;
    $v_y1 = 0;
    $v_x2 = $i;
    $v_y2 = $v_height;
    $draw->line($v_x1, $v_y1, $v_x2, $v_y2);
}

for($i = $v_bleed + $v_distance; $i<$v_height; $i+=$v_distance){
    $v_x1 = 0;
    $v_y1 = $i;
    $v_x2 = $v_width;
    $v_y2 = $i;
    $draw->line($v_x1, $v_y1, $v_x2, $v_y2);
}


$image->drawimage($draw);




$image->setimageformat('png');    // Give the image a format

header('Content-type: image/png');     // Prepare the web browser to display an image
echo $image;

$pixel->clear();
$pixel->destroy();
$v_gray->clear();
$v_gray->destroy();
$draw->clear();
$draw->destroy();
$image->clear();
$image->destroy();
?>s