<?php
if(!isset($v_sval)) die();

$v_body = isset($_GET['body'])?$_GET['body']:'';
$v_font = isset($_GET['font'])?$_GET['font']:'Arial';
$v_fill = isset($_GET['fill'])?$_GET['fill']:'010101';
$v_width = isset($_GET['width'])?$_GET['width']:'0';
$v_height = isset($_GET['height'])?$_GET['height']:'0';
$v_bold = isset($_GET['bold'])?$_GET['bold']:'false';
$v_italic = isset($_GET['italic'])?$_GET['italic']:'false';
$v_line = isset($_GET['line'])?$_GET['line']:'none';
$v_flip = isset($_GET['flip'])?$_GET['flip']:'none';
$v_rotation = isset($_GET['rotation'])?$_GET['rotation']:'0';
$v_gravity = isset($_GET['gravity'])?$_GET['gravity']:'center';
$v_density = isset($_GET['density'])?$_GET['density']:'0';
$v_point_size = isset($_GET['pointsize'])?$_GET['pointsize']:'0';
$v_bullet_style = isset($_GET['bullet_style'])?$_GET['bullet_style']:'undefined';
$v_character = isset($_GET['character'])?$_GET['character']:'none';
//$v_fit_to_box = isset($_GET['fit_to_box'])?$_GET['fit_to_box']:'false';

$arr_bullet_style = array(
    'undefined'=>''
    ,'black_circle'=>"&#9679; "
    ,'white_circle'=>"&#9675; "
    ,'white_square'=>"&#9633; "
    ,'black_square'=>"&#9632; "
    ,'dash'=>"&ndash; "
    ,'number_dot'=>". "
    ,'number_dash'=>"- "
    ,'number_parenth'=>") "
);

if(!isset($arr_bullet_style[$v_bullet_style])) $v_bullet_style = 'undefined';

settype($v_width, 'float');
settype($v_height, 'float');
settype($v_point_size, 'float');
settype($v_density, 'int');
settype($v_rotation, 'int');

$v_font_key = seo_friendly_url($v_font);
$v_dir = DESIGN_FONT_DIR.DS.$v_font_key.DS;
$v_bold = $v_bold=='true';
$v_italic = $v_italic=='true';

if($v_bold && $v_italic)
    $v_font = $v_dir.'both.ttf';
else if($v_bold && !$v_italic)
    $v_font = $v_dir.'bold.ttf';
else if(!$v_bold && $v_italic)
    $v_font = $v_dir.'italic.ttf';
else
    $v_font = $v_dir.'regular.ttf';


if($v_point_size>0){
    $v_point_size = ($v_point_size * $v_density)/72;
}
//if($v_height==0){
    //$v_height = ceil($v_point_size * 1.3333333333333);
    $v_width =  ceil($v_width * $v_density);
//}

//if($v_width==0){
    //$v_width = ceil($v_point_size * 1.3333333333333);
    $v_height = ceil($v_height * $v_density);
    //$v_width = $v_height;
//}
if($v_point_size==0){
    $v_height = ceil( $v_height * $v_density);
    $v_width = ceil( $v_width * $v_density);
    if($v_height>0){
        $v_point_size = ceil($v_height/1.3333333333333);
    }else
        $v_point_size = ceil($v_width/1.3333333333333);
    $v_point_size /= 3;
}

$v_point_size = ceil($v_point_size);
if($v_fill!='transparent') $v_fill = '#'.$v_fill;

$cls_draw = new cls_draw();
$cls_draw->set_fill_color($v_fill);
$cls_draw->set_height($v_height);
$cls_draw->set_width($v_width);
$cls_draw->set_rotation($v_rotation);

$image = new Imagick();

if($v_character=='upper')
    $v_body = strtoupper($v_body);
else if($v_character=='lower')
    $v_body = strtolower($v_body);
else if($v_character=='capitalize')
    $v_body = ucwords($v_body);
$cls_draw->text($image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style, $v_line);

$image->setimageformat('png');
$cls_draw->flip($image, $v_flip);
/* Output the image with headers */
header('Content-type: image/png');
echo $image;

$image->clear();
$image->destroy();