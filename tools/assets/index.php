<?php
if(!isset($v_sval)) die();?>
<?php
$v_action_type = isset($_GET['action_type'])?$_GET['action_type']:'';


if($v_site_id>0){
    $arr_switch = array(
        'theme_sample'=>'Get Theme\'s Sample'
        ,'font_sample'=>'Get Font\'s Sample'
        ,'create_text'=>'Create Text'
        ,'create_shape'=>'Create Shape'
        ,'create_mock_up'=>'Create Mock Up'
        ,'create_grid'=>'Create Grid'
        ,'create_die_cut'=>'Create Die Cut'
        ,'preview_design'=>'Preview Design'
        ,'create_svg'=>'Create SVG'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot(),
        "cross"=>$v_is_cross_domain?1:0
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_action_type])?$arr_switch[$v_action_type]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_action_type){
    case 'theme_sample':
        include 'qry_load_theme_image.php';
        break;
    case 'font_sample':
        include 'qry_load_font_sample.php';
        break;
    case 'create_text':
        include 'qry_create_text.php';
        break;
    case 'create_shape':
        include 'qry_create_shape.php';
        break;
    case 'create_mock_up':
        include 'qry_create_mock_up.php';
        break;
    case 'create_svg':
        include 'qry_create_svg.php';
        break;
    case 'create_grid':
        include 'qry_create_grid.php';
        break;
    case 'preview_design':
        include 'qry_preview_design.php';
        break;
    case 'create_die_cut':
        include 'qry_create_die_cut.php';
        break;
    case 'download_pdf_proof':
        include 'qry_download_pdf_proof.php';
        break;
}