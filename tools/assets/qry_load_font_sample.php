<?php
if(!isset($v_sval)) die();
?>
<?php
$v_height = 20;
$v_width = 160;
$v_point_size = 15;

//$v_name = isset($_GET['name'])?$_GET['name']:'Arial';
//$v_key = isset($_GET['key'])?$_GET['key']:seo_friendly_url($v_name);
//$v_type = isset($_GET['type'])?$_GET['type']:'regular';

$v_name = isset($_GET['name'])?$_GET['name']: 'Arial';
$v_type = isset($_GET['type'])?$_GET['type']: 'regular';
$v_key = isset($_GET['key'])?$_GET['key']: seo_friendly_url($v_name);

add_class('cls_tb_design_font');

$v_image = DESIGN_FONT_DIR.DS.$v_key.DS.'sample.png';
$v_font = DESIGN_FONT_DIR.DS.$v_key.DS.$v_type.'.ttf';
if(file_exists($v_image) && $v_type=='regular'){
    $image = new Imagick();
    $image->readimage($v_image);
    header('Content-type: image/png');
    echo $image;
    $image->clear();
    $image->destroy();
}else{
    if(file_exists($v_font)){
        $image = new Imagick();
        $draw = new ImagickDraw();
        $pixel = new ImagickPixel( 'transparent' );
        //die();
        /* New image */
        $image->newimage($v_width, $v_height, $pixel);

        /* Black text */
        $draw->setfillcolor('black');

        /* Font properties */

        $draw->setfont($v_font);
        $draw->setfontsize($v_point_size);

        /* Create text */
        $image->annotateimage($draw, 2, 18, 0, $v_name);

        /* Give image a format */
        $image->setimageformat('png');

        if($v_type=='regular') $image->writeimage($v_image);


        /* Output the image with headers */
        header('Content-type: image/png');
        echo $image;

        $pixel->clear();
        $pixel->destroy();
        $draw->clear();
        $draw->destroy();
        $image->clear();
        $image->destroy();

    }else echo  null;
}