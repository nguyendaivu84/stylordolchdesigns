<?php
if(!isset($v_sval)) die();
$v_template_id = isset($_GET['template_id'])?$_GET['template_id']:'0';
$v_theme_id = isset($_GET['theme_id'])?$_GET['theme_id']:'0';
$v_design_id = isset($_GET['design_id'])?$_GET['design_id']:'0';

settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');

$_SESSION['ss_design_template'] = $v_template_id;
$_SESSION['ss_design_theme'] = $v_theme_id;
$_SESSION['ss_design_design'] = $v_design_id;

if($v_design_id>0){
    include 'qry_load_design_design.php';
}else if($v_template_id>0 && $v_theme_id>0){
    include 'qry_load_theme_design.php';
}else if($v_template_id>0){
    include 'qry_load_template_design.php';
}