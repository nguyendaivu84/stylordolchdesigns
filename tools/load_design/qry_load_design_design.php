<?php
if(!isset($v_sval)) die();?>
<?php
$v_folding_type = "none";
$v_die_cut_type = "none";

$arr_data = array('success'=>0, 'error'=>1, 'message'=>'Design No# '.$v_design_id.' is not found!');
if($v_design_id>0){
    $arr_where_clause = array('design_id'=>$v_design_id);
    add_class('cls_tb_design_design');

    $cls_designs = new cls_tb_design_design($db, LOG_DIR);
    $v_row = $cls_designs->select_one($arr_where_clause);

    if($v_row==1){
        $v_width = $cls_designs->get_design_width();
        $v_height = $cls_designs->get_design_height();
        $v_folding = $cls_designs->get_design_folding();
        $v_die_cut = $cls_designs->get_design_die_cut();
        //$arr_product_list = $cls_designs->get_product_list();
        //$v_template_name = $cls_designs->get_template_name();
        $v_stock_cost = $cls_designs->get_stock_cost();
        $v_markup_cost = $cls_designs->get_markup_cost();
        $v_created_time = $cls_designs->get_created_time();
        $v_assign_to = $cls_designs->get_user_id();
        $v_design_status = $cls_designs->get_design_status()==0?1:0;
        $v_design_bleed = $cls_designs->get_design_bleed();
        $v_template_id = $cls_designs->get_template_id();
        $arr_product = $cls_designs->get_product();
        $v_product_id = isset($arr_product['id'])?$arr_product['id']:0;
        $v_product_code = isset($arr_product['code'])?$arr_product['code']:'blank_design';
        $v_product_title = isset($arr_product['title'])?$arr_product['title']:'Blank Design';

        $v_design_data = $cls_designs->get_design_data();
        $v_design_image = $cls_designs->get_design_image();
        $v_design_name = $cls_designs->get_design_name();
        $v_folding_direction = $cls_designs->get_design_direction();

        $v_folding_type = $cls_settings->get_option_key_by_id('folding_type', $v_folding, 'none');
        $v_die_cut_type = $cls_settings->get_option_key_by_id('die_cut_type', $v_die_cut, 'none');
        $v_folding_direction = $cls_settings->get_option_key_by_id('folding_direction', $v_folding_direction, 'vertical');


        $arr_images = array();
        $arr_json = array();

        if($v_design_image!='')
            $arr_images = json_decode($v_design_image, true);

        if(!is_array($arr_images))  $arr_images = array();
        $arr_fotolia_licenses = array();
        $arr_fotolia_child = array();
        $arr_temp = array();
        $arr_stock_image = array();
        foreach($arr_images as $image_id=>$arr_i){
            $v_image_id = $arr_i['id'];
            $license = isset($arr_i['licenses'])?$arr_i['licenses']:array();
            if(!is_array($license)) $license = array();
            foreach($license as $fotolia => $arr){
                $arr_fotolia_licenses[$fotolia] = $arr;
                $arr_temp[$fotolia] = array('id'=>$v_image_id, 'name'=>$arr_i['name']);
            }
        }
        foreach($arr_fotolia_licenses as $fotolia=>$arr){
            $v_size = sizeof($arr)-1;
            if($v_size>=0){
                $arr_fotolia_child[] = array(
                    'fotolia_id'=>$fotolia,
                    'fotolia_license'=>$arr[$v_size]['name'],
                    'price'=>$arr[$v_size]['price'],
                    'width'=>$arr[$v_size]['width'],
                    'height'=>$arr[$v_size]['height'],
                    'image_id'=>$arr_temp[$fotolia]['id'],
                    'name'=>$arr_temp[$fotolia]['name']
                );
                $arr_stock_image[$fotolia] = array(
                    'fotoliaId'=>$fotolia,
                    'maxLicense'=>$arr[$v_size]['name'],
                    'assigned'=>true
                );
            }
        }


        if($v_design_data!='') $arr_json = json_decode($v_design_data, true);

        $arr_tmp = array(
            'texts'=>array()
            ,'images'=>array()
            ,'width'=>($v_width + 2 * $v_design_bleed)
            ,'height'=>($v_height + 2 * $v_design_bleed)
            ,'bg_color'=> "FFFFFF"
            ,'bleed'=>	$v_design_bleed
            ,'droppable'=> true
            ,'name'=> "Front Side"
            ,"isViewed"=>false
        );

        $v_change = false;
        if(!(isset($arr_json['canvases'])) || !is_array($arr_json['canvases'])){
            $arr_canvases = array();
            if($v_folding<=1)
                $arr_canvases = array($arr_tmp);
            else{
                $arr_canvases = array($arr_tmp, $arr_tmp);
                if($v_folding==1)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding>=2){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
            $v_change = true;
        }else{
            $arr_canvases = $arr_json['canvases'];
            if($v_folding<1){
                if(count($arr_canvases)>1){
                    $v_change = true;
                    $i=0;
                    $arr_canvas = array();
                    foreach($arr_canvases as $idx=>$arr){
                        if($i==0) $arr_canvas[] = $arr;
                        $i++;
                    }
                    $arr_canvases = $arr_canvas;
                }
            }else{
                if(count($arr_canvases)==1){
                    $arr_canvas = $arr_canvases[0];
                    $arr_canvases = array($arr_canvas, $arr_tmp);
                    $v_change = true;
                }else if(count($arr_canvases)>2){
                    $v_change = true;
                    $arr_canvas = array();
                    $i=0;
                    foreach($arr_canvases as $idx=>$arr){
                        if($i<=1){
                            $arr_canvas[$i] = $arr;
                        }
                        $i++;
                    }
                    $arr_canvases = $arr_canvas;
                }

                if($v_folding==1)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding>=2){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
        }
        if($v_change) $arr_json['canvases'] = $arr_canvases;

        $arr_data['width'] = $v_width;
        $arr_data['height'] = $v_height;
        $arr_data['folding'] = $v_folding_type;
        $arr_data['diecut'] = $v_die_cut_type;
        $arr_data['title'] = $v_design_name;
        $arr_data['id'] = $v_template_id;
        $arr_data['price_markup'] = $v_markup_cost;
        $arr_data['assign_to'] = $v_assign_to;
        $arr_data['product_id'] = $v_product_id;
        $arr_data['product_title'] = $v_product_title;
        $arr_data['product_url_code'] = $v_product_code;
        $arr_data['stock_image_code'] = $v_stock_cost;
        $arr_data['size_title'] = $v_width. '" &times; '.$v_height.'"';
        $arr_data['set_id'] = $v_template_id;
        $arr_data['set_title'] = $v_design_name;
        $arr_data['status_id'] = $v_design_status;
        $arr_data['success'] = 1;
        $arr_data['images']= array();//
        $arr_data['json']= array();//
        $arr_data['fotoliaLicenses']= $arr_fotolia_licenses;
        $arr_data['fotoliaChildItems']= $arr_fotolia_child;
        $arr_data['themes']= array();


        $arr_json = array(
            'version'=>'2.0',
            'canvases'=>$arr_canvases
            ,'width'=>$v_width
            ,'height'=>$v_height
            ,'dieCutType'=>$v_die_cut_type
            ,'folding'=>$v_folding_type
            ,'foldingDirection'=>$v_folding_direction
            ,'product'=>$v_product_code
            ,'stockImages'=>$arr_stock_image
            ,'wrap_size'=>0
        );


        $arr_data['save_action'] = 'save_design';
        $arr_data['images'] = $arr_images;
        $arr_data['json'] = $arr_json;

        $cls_designs->update_field('last_used', new MongoDate(time()), array('design_id'=>$v_design_id));

    }
}
header("Content-type: application/json");
if($v_is_cross_domain)
    $cls_output->output($arr_data);
else
    $cls_output->output_jsonp($arr_data, 'jsonp_callback');
//echo json_encode($arr_data);