<?php
if(!isset($v_sval)) die();

$v_json = isset($_POST['json'])?$_POST['json']:'';
$v_name = isset($_POST['name'])?$_POST['name']:'';
$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_theme_id = isset($_POST['theme_id'])?$_POST['theme_id']:'0';
$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';
$v_save_action = isset($_POST['save_action'])?$_POST['save_action']:'';
$v_dpi = isset($_POST['dpi'])?$_POST['dpi']:'0';

settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');
settype($v_dpi, 'int');

switch($v_save_action){
    case 'save_template':
        include 'qry_save_template_design.php';
        break;
    case 'save_theme':
        include 'qry_save_theme_design.php';
        break;
    case 'save_design':
        include 'qry_save_design_design.php';
        break;
    default:
        break;
}