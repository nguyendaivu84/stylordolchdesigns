<?php
if(!isset($v_sval)) die();

$v_design_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
settype($v_admin_id, 'int');

add_class('cls_tb_design_template');
add_class('cls_tb_design_image');
add_class('cls_tb_design_stock');

$cls_templates = new cls_tb_design_template($db, LOG_DIR);
$cls_images = new cls_tb_design_image($db, LOG_DIR);
$cls_stocks = new cls_tb_design_stock($db, LOG_DIR);

$v_row = $cls_templates->select_one(array('template_id'=>$v_template_id));
$v_success = 0;
$v_design_id = 0;

$v_str = '';
$v_prefix = ''; //admin?
$v_image_data = '';
$v_front_side_data = '';
$v_back_side_data = '';
$v_total_image_cost = 0;
$arr_color = array();

if($v_row==1){
    $v_template_name = $cls_templates->get_template_name();
    $v_template_image = $cls_templates->get_sample_image();
    $v_template_saved_dir = $cls_templates->get_saved_dir();
    $v_folding_type = $cls_templates->get_folding_type();
    $v_template_width = $cls_templates->get_template_width();
    $v_template_height = $cls_templates->get_template_height();
    $v_template_bleed = $cls_templates->get_template_bleed();

    $v_old_template_data = $cls_templates->get_template_data();
    $arr_old_image = array();
    $arr_json = json_decode($v_old_template_data, true);
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    if(is_array($arr_canvases)){
        $v_size = sizeof($arr_canvases);
        if($v_size>0 && $v_size<=2){
            for($i=0; $i<$v_size; $i++){
                $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                if(is_array($arr_images)){
                    $v_image_size = sizeof($arr_images);
                    for($j=0; $j < $v_image_size; $j++){
                        $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                        settype($v_image_id, 'int');
                        $arr_images[$v_image_id] = true;
                    }
                }
            }
        }
    }

    $arr_image_used = array();
    $arr_image_name = array();
    $arr_text_name = array();
    $v_json = stripslashes($v_json);
    $arr_json = json_decode($v_json, true);
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    $arr_tmp_images = array();
    $v_tmp_text_color = '';
    $v_image_pos = strpos($v_json, "images:");

    $arr_tmp = array(
        'texts'=>array()
        ,'images'=>array()
        ,'width'=>($v_template_width + 2 * $v_template_bleed)
        ,'height'=>($v_template_height + 2 * $v_template_bleed)
        ,'bg_color'=> "FFFFFF"
        ,'bleed'=>	$v_template_bleed
        ,'droppable'=> true
        ,'name'=> "Front Side"
        ,"isViewed"=>false
    );

    $v_total_page = count($arr_canvases);
    if($v_folding_type<=0){
        if($v_total_page>=1){
            $arr_canvas = array();
            $i = 0;
            foreach($arr_canvases as $idx=>$arr){
                if($i==0) $arr_canvas[] = $arr;
                $i++;
            }
        }else{
            $arr_canvas[] = $arr_tmp;
        }
        $arr_canvases = $arr_canvas;
    }else{
        if($v_total_page<1){
            $arr_canvas = array($arr_tmp, $arr_tmp);
        }else if($v_total_page==1){
            $i=0;
            $arr_canvas = array();
            foreach($arr_canvases as $idx=>$arr){
                if($i==0) $arr_canvas[0] = $arr;
                $i++;
            }
            $arr_canvas[1] = $arr_tmp;
        }else{
            $i=0;
            $arr_canvas = array();
            foreach($arr_canvases as $idx=>$arr){
                if($i<=1) $arr_canvas[$i] = $arr;
                $i++;
            }
        }
        if($v_folding_type==1)
            $arr_canvas[1]['name'] = 'Back Side';
        else{
            $arr_canvas[1]['name'] = 'InSide';
            $arr_canvas[0]['name'] = 'OutSide';
        }
        $arr_canvases = $arr_canvas;
    }
    $v_total_page = count($arr_canvases);
    //$arr_fotolia_license = array();
    for($i=0; $i<$v_total_page; $i++){
        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
        $arr_extra_image = array();
        for($j=0;$j<count($arr_images);$j++){
            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
            settype($v_image_id, 'int');
            if($v_image_id>0){

                $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
                if($v_row==1){
                    $v_image_name = $cls_images->get_image_name();
                    $v_image_key = $cls_images->get_image_key();
                    $v_image_file = $cls_images->get_image_file();
                    $v_image_width = $cls_images->get_image_width();
                    $v_image_height = $cls_images->get_image_height();
                    $v_image_dpi = $cls_images->get_image_dpi();
                    $v_image_extension = $cls_images->get_image_extension();
                    $v_is_public = $cls_images->get_is_public();
                    $v_is_vector = $cls_images->get_is_vector();
                    $v_fotolia_id = $cls_images->get_fotolia_id();
                    $v_fotolia_size = $cls_images->get_fotolia_size();
                    $v_customer_id = $cls_images->get_user_id();
                    $v_created_time = $cls_images->get_created_time();
                    $v_image_stock_id = $cls_images->get_image_stock_id();
                    $v_svg_id = $cls_images->get_svg_id();
                    $arr_svg_colors = $cls_images->get_svg_original_colors();
                    if(!is_array($arr_svg_colors)) $arr_svg_colors = array();

                    if(isset($arr_old_image[$v_image_id])){
                        $arr_old_image[$v_image_id] = false;
                    }else{
                        $arr_image_used[$v_image_id] = array(
                            'template'=>1, 'design'=>0
                        );
                    }

                    $arr_license = array();

                    if($v_image_stock_id>0){
                        $v_stock_row = $cls_stocks->select_one(array('stock_id'=>$v_image_stock_id));
                        if($v_stock_row==1){
                            $arr_images[$j]['fotoliaId'] = $v_fotolia_id;
                            $arr_images[$j]['fotoliaLicense'] = $v_fotolia_size;
                            $license = $cls_stocks->get_licenses();
                            if(is_array($license)){
                                for($k=0; $k<sizeof($license);$k++){
                                    if(isset($license[$k]['dimensions'])) unset($license[$k]['dimensions']);
                                    $license[$k]['id'] = (intval($v_image_id) * 100) + $k;
                                    $license[$k]['fotolia_id'] = $v_fotolia_id;
                                    $license[$k]['license_value'] = $license[$k]['price'];
                                    $license[$k]['is_on_disk'] = 0;
                                    $license[$k]['use_for_comp'] = 0;
                                }
                                $arr_license[$v_fotolia_id] = $license;
                            }
                        }
                    }

                    if(is_object($v_created_time))
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time->sec);
                    else
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time);

                    $v_total_image_cost += $cls_images->get_image_cost();
                    $arr_tmp_images[$v_image_id] = array(
                        "id"=>$v_image_id.''
                        ,'customer_id'=> "".$v_customer_id
                        ,'session_id'=> "".session_id()
                        ,'site_code'=> DESIGN_SITE_CODE
                        ,'asset_name'=> ""
                        ,'is_vector'=> $v_is_vector
                        ,'name'=> $v_image_file
                        ,'extension'=> $v_image_extension
                        ,'width'=> $v_image_width.''
                        ,'height'=> $v_image_height.''
                        ,'dpi'=> $v_image_dpi
                        ,'added_on'=> $v_created_time
                        ,'is_public'=> $v_is_public
                        ,'delete_flag'=> "1"
                        ,'licenses'=>$arr_license
                        ,'svg_original_colors'=>$arr_svg_colors
                        ,'svg_id'=>$v_svg_id

                    );

                    $v_name = $v_prefix.$v_image_key;
                    if(isset($arr_image_name[$v_name])){
                        $k = 1;
                        do{
                            $v_name = $v_prefix.$v_image_key.'_'.$k;
                            if(!isset($arr_image_name[$v_name])){
                                $arr_images[$j]['name'] = $v_name;
                                $arr_image_name[$v_name] = 1;
                                $k = -1;
                            }else{
                                $k++;
                            }
                        }while($k>0);
                    }else{
                        $arr_images[$j]['name'] = $v_name;
                        $arr_image_name[$v_name] = 1;
                    }

                    //$v_no_print = isset($arr_images[$j]['noprint']);
                    //if($v_no_print) $v_no_print = $arr_images[$j]['noprint'];

                    //if(!$v_no_print){
                        $v_border_color = isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'';
                        if($v_border_color!='' && $v_border_color!='transparent'){
                            $arr_color['image_'.$v_name] = array(
                                'type'=>'image'
                                ,'color'=>$v_border_color
                            );
                        }
                    //}
                    if(sizeof($arr_svg_colors)>0){
                        $arr_color['svg_'.$v_name] = array(
                            'type'=>'image',
                            'color'=>$arr_svg_colors
                        );
                    }

                    $v_type = isset($arr_images[$j]['type'])?$arr_images[$j]['type']:'';
                    /*
                    if($v_type=='mask'){

                        $k=0;
                        $x=-1;
                        for($k=0; $k<count($arr_images) && $x==-1;$k++){
                            if(!isset($arr_images[$k]['image_id'])){
                                if(isset($arr_images[$k]['refer']) && $arr_images[$k]['refer']==$arr_images[$j]['name']){
                                    $x=$k;
                                }
                            }
                        }
                        if($v_fotolia_id>0){
                            $v_name = $arr_images[$j]['name'];
                            if(strpos($v_name, $v_prefix)!==0){
                                $v_name = $v_prefix.$v_name;
                                $arr_images[$j]['name'] = $v_name;
                                $arr_image_name[$v_name]=1;
                            }
                        }
                        if($x>=0){
                            $arr_images[$x] = array(
                                "zindex"=>$arr_images[$j]['zindex']-1
                                ,"fixedratio"=>false
                                ,"noselect"=>false
                                ,"noresize"=>false
                                ,"border_width"=>0
                                ,"border_color"=>isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'ffffff'
                                ,"fill_color"=>'ffffff'
                                ,"border_stroke"=>"solid"
                                ,"shape_type"=>"rect"
                                ,'name'=>$arr_images[$j]['name'].'_1'
                                ,"width"=>$arr_images[$j]['width']
                                ,"height"=>$arr_images[$j]['height']
                                ,"left"=>$arr_images[$j]['left']
                                ,"top"=>$arr_images[$j]['top']
                                ,"crop"=>false
                                ,"type"=>"mask"
                                ,"placeholder"=>"filler"
                                ,"noprint"=>true
                                ,"unmovable"=>false
                                ,"invisible"=>false
                                ,"uploader"=>false
                                ,"refer"=>$arr_images[$j]['name']
                            );
                        }else{
                            $arr_extra_image[] = array(
                                "zindex"=>$arr_images[$j]['zindex']-1
                                ,"fixedratio"=>false
                                ,"noselect"=>false
                                ,"noresize"=>false
                                ,"border_width"=>0
                                ,"border_color"=>isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'ffffff'
                                ,"fill_color"=>'ffffff'
                                ,"border_stroke"=>"solid"
                                ,"shape_type"=>"rect"
                                ,'name'=>$arr_images[$j]['name'].'_1'
                                ,"width"=>$arr_images[$j]['width']
                                ,"height"=>$arr_images[$j]['height']
                                ,"left"=>$arr_images[$j]['left']
                                ,"top"=>$arr_images[$j]['top']
                                ,"crop"=>false
                                ,"type"=>"mask"
                                ,"placeholder"=>"filler"
                                ,"noprint"=>true
                                ,"unmovable"=>false
                                ,"invisible"=>false
                                ,"uploader"=>false
                                ,"refer"=>$arr_images[$j]['name']
                            );
                        }
                        if(isset($arr_images[$j]['placeholder']) && $arr_images[$j]['placeholder']=='fitter'){
                            unset($arr_images[$j]['placeholder']);
                            $arr_images[$j]['fitted'] = true;
                        }

                    }
                    */

                }
            }else{
                $v_fill_color = isset($arr_images[$j]['fill_color'])?$arr_images[$j]['fill_color']:'transparent';
                $v_fill_color = str_replace('#','',$v_fill_color);
                if($v_fill_color!=''){
                    if(strpos($v_tmp_text_color, $v_fill_color.',')===false) $v_tmp_text_color .= $v_fill_color.',';
                }
                $v_name = $v_prefix.$arr_images[$j]['name'];
                $arr_color['fill_'.$v_name] = array(
                    'type'=>'image'
                    ,'color'=>$v_fill_color
                );

                $v_fill_color = isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'';
                $v_fill_color = str_replace('#','',$v_fill_color);
                if($v_fill_color!=''){
                    if(strpos($v_tmp_text_color, $v_fill_color.',')===false) $v_tmp_text_color .= $v_fill_color.',';
                }
                $arr_color['border_'.$v_name] = array(
                    'type'=>'image'
                    ,'color'=>$v_fill_color
                );


            }
        }
        //for($k=0; $k<count($arr_extra_image);$k++)
        //    $arr_images[] = $arr_extra_image[$k];
        $arr_canvases[$i]['images'] = $arr_images;

        for($j=0;$j<count($arr_texts);$j++){
            $v_color = isset($arr_texts[$j]['color'])?$arr_texts[$j]['color']:'000000';
            $v_title = isset($arr_texts[$j]['title'])?$arr_texts[$j]['title']:'';
            if($v_title=='') $v_title = 'Enter Text';
            if($v_title!=''){
                $v_title = html_entity_decode($v_title);
                $v_title = urldecode($v_title);
                $v_title = seo_friendly_url($v_title);
                $v_title = $v_prefix.str_replace('-','_', $v_title);
                if(isset($arr_text_name[$v_title])){
                    $k = 1;
                    do{
                        $v_new_title = $v_title.'_'.$k;
                        if(!isset($arr_text_name[$v_new_title])){
                            $arr_texts[$j]['name'] = $v_new_title;
                            $arr_text_name[$v_new_title] = 1;
                            $k = -1;
                        }else{
                            $k++;
                        }
                    }while($k>0);
                }else{
                    $arr_texts[$j]['name'] = $v_title;
                    $arr_text_name[$v_title] = 1;
                }
            }

            $v_color = str_replace('#', '', $v_color);
            if(strlen($v_color)=='') $v_color = '000000';
            if(strpos($v_tmp_text_color, $v_color.',')===false) $v_tmp_text_color .= $v_color.',';
            $arr_color['text_'.$arr_texts[$j]['name']] = array(
                'type'=>'text'
                ,'color'=>$v_color
            );

        }
        $arr_canvases[$i]['texts'] = $arr_texts;
    }

    $v_image_data = json_encode($arr_tmp_images);
    $arr_json['canvases'] = $arr_canvases;
    $v_json = json_encode($arr_json);
        $v_ext = 'png';
        $v_image_sample = 'template_'.$v_template_id.'_theme_'.$v_theme_id.'.'.$v_ext;
        $v_dir = DESIGN_THEME_DIR.DS;
        if(file_exists($v_dir.$v_template_id) || @mkdir($v_dir.$v_template_id)){
            $v_dir .= $v_template_id .DS;
        }

        $v_saved_dir = str_replace($v_root_dir,'',$v_dir);
        $v_saved_dir = str_replace('\\','/', $v_saved_dir);

        $v_full_path = $v_dir.$v_image_sample;
        $v_accept = true;
        if(file_exists($v_full_path)){
            $v_accept = @unlink($v_full_path);
        }


    $cls_tmp_draw = new cls_draw();
    $image = new Imagick();
    if($v_total_page<=1){
        $v_page = 0;
        $v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
        $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
        if($v_accept){

            $tmp_image = $cls_draw->create_square_thumb($image, 100);
            $v_file_thumb = str_replace($v_image_sample, 'thumb_'.$v_image_sample,$v_full_path);
            $tmp_image->setformat('png');
            $tmp_image->writeimage($v_file_thumb);
            $tmp_image->clear();
            $tmp_image->destroy();

            $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);
            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
        }
    }else{
        if($v_accept){
            $v_page = 1;
            $image2 = new Imagick();
            $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
            $v_page = 0;
            $image1 = new Imagick();
            $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());

            $tmp_image = $cls_draw->create_square_thumb($image1, 100);
            $v_file_thumb = str_replace($v_image_sample, 'thumb_'.$v_image_sample,$v_full_path);
            $tmp_image->setformat('png');
            $tmp_image->writeimage($v_file_thumb);
            $tmp_image->clear();
            $tmp_image->destroy();


            $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
            $image1->clear();
            $image1->destroy();
            $image2->clear();
            $image2->destroy();

        }
    }



    $arr_fields = array('template_data', 'template_image', 'template_color', 'stock_cost', 'saved_dir', 'sample_image', 'template_dpi', 'last_modified');
    $arr_values = array($v_json, $v_image_data, $arr_color, $v_total_image_cost, $v_saved_dir, $v_image_sample, $v_dpi, new MongoDate(time()));
    $v_result = $cls_templates->update_fields($arr_fields, $arr_values, array("template_id"=>$v_template_id));
    $v_success = 1;
    if($v_result){
        foreach($arr_old_image as $v_image_id=>$no_used){
            if($no_used){
                $arr_image_used[$v_image_id] = array(
                    'template'=>-1, 'design'=>0
                );
            }
        }
        $cls_images->update_image_used($arr_image_used);
    }
}
$arr_return = array('success'=>$v_success, 'design_id'=>0, 'theme_id'=>0);
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
//header("Content-type: application/json");
//echo json_encode(array('success'=>$v_success, 'design_id'=>0, 'theme_id'=>0));