<?php
if(!isset($v_sval)) die();

$v_design_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
settype($v_admin_id, 'int');

add_class('cls_tb_design_template');
add_class('cls_tb_design_theme');
add_class('cls_tb_design_image');

$cls_templates = new cls_tb_design_template($db, LOG_DIR);
$cls_themes = new cls_tb_design_theme($db, LOG_DIR);
$cls_images = new cls_tb_design_image($db, LOG_DIR);

$v_row = $cls_templates->select_one(array('template_id'=>$v_template_id));
$v_success = 0;
$v_design_id = 0;

$v_str = '';
$v_image_data = '';
$v_front_side_data = '';
$v_back_side_data = '';
$v_total_image_cost = 0;

$arr_color = array();

$arr_template_color = array();
if($v_row==1){
    $v_template_name = $cls_templates->get_template_name();
    $v_template_image = $cls_templates->get_sample_image();
    $v_template_saved_dir = $cls_templates->get_saved_dir();
    $v_folding_type = $cls_templates->get_folding_type();
    $arr_template_color = $cls_templates->get_template_color();

    $arr_image_name = array();
    $arr_text_name = array();
    $v_json = stripslashes($v_json);
    $arr_json = json_decode($v_json, true);
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    $arr_tmp_images = array();
    $v_image_pos = strpos($v_json, "images:");

    $v_total_page = count($arr_canvases);
    $v_total_page = $v_folding_type<1?1:2;

    for($i=0; $i<$v_total_page; $i++){
        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
        $arr_extra_image = array();
        for($j=0;$j<count($arr_images);$j++){
            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
            $v_name = isset($arr_images[$j]['name'])?$arr_images[$j]['name']:'';
            settype($v_image_id, 'int');
            if($v_image_id>0){

                $v_border_color = isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'';
                if($v_border_color!='' && $v_border_color!='transparent'){
                    if(isset($arr_template_color['image_'.$v_name]['type']) && $arr_template_color['image_'.$v_name]['type']=='image'){
                        if(isset($arr_template_color['image_'.$v_name]['color']) && $arr_template_color['image_'.$v_name]['color']!=$v_border_color){
                            $arr_color['image_'.$v_name] = array(
                                'type'=>'image'
                                ,'color'=>$v_border_color
                            );
                        }
                    }
                }

                $v_svg_color = isset($arr_images[$j]['svg_colors'])?$arr_images[$j]['svg_colors']:'';
                $arr_svg_colors = json_decode($v_svg_color, true);
                if(!is_array($arr_svg_colors)) $arr_svg_colors = array();
                if(isset($arr_template_color['svg_'.$v_name]['type'])){
                    $arr_color['svg_'.$v_name] = array(
                        'type'=>'image',
                        'color'=>$arr_svg_colors
                    );
                }
            }else{
                $v_fill_color = isset($arr_images[$j]['fill_color'])?$arr_images[$j]['fill_color']:'transparent';
                $v_fill_color = str_replace('#','',$v_fill_color);
                $v_name = $arr_images[$j]['name'];

                if(isset($arr_template_color['fill_'.$v_name]['type']) && $arr_template_color['fill_'.$v_name]['type']=='image'){
                    if(isset($arr_template_color['fill_'.$v_name]['color']) && $arr_template_color['fill_'.$v_name]['color']!=$v_fill_color){
                        $arr_color['fill_'.$v_name] = array(
                            'type'=>'image'
                            ,'color'=>$v_fill_color
                        );
                    }
                }

                $v_fill_color = isset($arr_images[$j]['border_color'])?$arr_images[$j]['border_color']:'';
                $v_fill_color = str_replace('#','',$v_fill_color);
                if(isset($arr_template_color['border_'.$v_name]['type']) && $arr_template_color['border_'.$v_name]['type']=='image'){
                    if(isset($arr_template_color['border_'.$v_name]['color']) && $arr_template_color['border_'.$v_name]['color']!=$v_fill_color){
                        $arr_color['border_'.$v_name] = array(
                            'type'=>'image'
                            ,'color'=>$v_fill_color
                        );
                    }
                }


            }
        }
        for($j=0;$j<count($arr_texts);$j++){
            $v_color = isset($arr_texts[$j]['color'])?$arr_texts[$j]['color']:'000000';
            $v_name = isset($arr_texts[$j]['name'])?$arr_texts[$j]['name']:'';

            $v_color = str_replace('#', '', $v_color);
            if(strlen($v_color)=='') $v_color = '000000';
            $v_name = $arr_texts[$j]['name'];
            if(isset($arr_template_color['text_'.$v_name]['type']) && $arr_template_color['text_'.$v_name]['type']=='text'){
                if(isset($arr_template_color['text_'.$v_name]['color']) && $arr_template_color['text_'.$v_name]['color']!=$v_color){
                    $arr_color['text_'.$v_name] = array(
                        'type'=>'text'
                        ,'color'=>$v_color
                    );
                }
            }
        }
    }


        $v_ext = 'png';
        $v_image_sample = 'template_'.$v_template_id.'_theme_'.$v_theme_id.'.'.$v_ext;

        $v_dir = DESIGN_THEME_DIR.DS;
        if(file_exists($v_dir.$v_template_id) || @mkdir($v_dir.$v_template_id)){
             $v_dir .= $v_template_id .DS;
        }

        $v_saved_dir = str_replace($v_root_dir,'',$v_dir);
        $v_saved_dir = str_replace('\\','/', $v_saved_dir);

        $v_full_path = $v_dir.$v_image_sample;
        $v_accept = true;
        if(file_exists($v_full_path)){
            $v_accept = @unlink($v_full_path);
        }

    $cls_tmp_draw = new cls_draw();
    $image = new Imagick();
    if($v_total_page<=1){
        $v_page = 0;
        $v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
        $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
        if($v_accept){

            $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);

            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
        }
    }else{
        if($v_accept){
            $v_page = 1;
            $image2 = new Imagick();
            $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
            $v_page = 0;
            $image1 = new Imagick();
            $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
            $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
            $image1->clear();
            $image1->destroy();
            $image2->clear();
            $image2->destroy();

        }
    }



    $arr_fields = array('list_color', 'saved_dir', 'sample_image', 'last_modified');
    $arr_values = array($arr_color, $v_saved_dir, $v_image_sample, new MongoDate(time()));
    $cls_themes->update_fields($arr_fields, $arr_values, array("theme_id"=>$v_theme_id));
    $v_success = 1;
}

$arr_return = array('success'=>$v_success, 'design_id'=>0, 'theme_id'=>$v_theme_id);
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
//header("Content-type: application/json");
//echo json_encode(array('success'=>$v_success, 'design_id'=>0, 'theme_id'=>$v_theme_id));