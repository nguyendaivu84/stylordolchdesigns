<?php
if(!isset($v_sval)) die();
$v_action = isset($_GET['action'])?$_GET['action']:'';
$v_is_cross_domain = isset($_REQUEST['cross'])?$_REQUEST['cross']:'false';
$v_is_cross_domain = $v_is_cross_domain===true || $v_is_cross_domain=='true';

add_class('cls_settings');
add_class('cls_tb_site_log');
add_class('cls_draw');
add_class('cls_output');
add_class('Browser');

add_class('cls_instagraph');
$cls_instagraph = new cls_instagraph();

$cls_settings = new  cls_settings($db, LOG_DIR);
$cls_output = new  cls_output($db);
$cls_draw = new cls_draw();
$browser = new Browser();
$cls_site_log = new cls_tb_site_log($db, LOG_DIR);

//Remove temporary file
$v_count = $cls_draw->remove_temp_file(DESIGN_TEMP_DIR, time(), 1);

$v_root_dir = ROOT_DIR.DS;
$v_user_company_id = isset($arr_user['company_id'])?$arr_user['company_id']:'0';
$v_user_location_id = isset($arr_user['location_default'])?$arr_user['location_default']:'0';
$v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
$v_user_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';

$v_user_ip = get_real_ip_address();
$v_user_agent = $browser->getUserAgent();

settype($v_user_company_id, 'int');
settype($v_user_location_id, 'int');
settype($v_user_id, 'int');
$arr_excluded_user = list_excluded_users($cls_settings);
switch($v_action){
    case 'load_design':
        include 'load_design/index.php';
        break;
    case 'add_image':
        include 'image/index.php';
        break;
    case 'save_design':
        include 'save_design/index.php';
        break;
    case 'product_design':
        include 'product_design/index.php';
        break;
    case 'canvas3d':
        include 'canvas3d/index.php';
        break;
    case 'assets':
    default:
        include 'assets/index.php';
        break;
}