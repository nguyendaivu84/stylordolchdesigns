<?php
if(!isset($v_sval)) die();

$v_site_index = $cls_key->select_scalar('site_index', array('site_id'=>$v_site_id));
switch($v_site_index){
    case 'worktraq':
    case 'worktraq-dev':
    case 'sprucemeadows':
	    include 'qry_load_worktraq_user_template.php';
        break;
    case 'gotodisplay';
    case 'vi':
        include 'qry_load_gotodisplay_user_template.php';
        break;
    case 'golf-imagestylor':
        include 'qry_load_golf_user_template.php';
        break;
    default:
        include 'qry_load_user_template.php';
        break;
}