<?php
if(!isset($v_sval)) die();?>
<?php

$v_product_code = isset($_GET['product_code'])?$_GET['product_code']:'blank_design';
$v_product_title = isset($_GET['product_title'])?$_GET['product_title']:'Blank Design';
$v_product_key = /*$v_product_code=='blank_design'?'blankDesign':*/$v_product_code;

$v_option = isset($_GET['option'])?$_GET['option']:'';
$v_custom = false;
$v_ontop = false;
$v_logo_index = 501;
$v_custom_width = $v_custom_height = $v_end_with = $v_end_height = 0;
if($v_option!='' && strpos($v_option,'-')>0){
    $arr_option = explode('-', $v_option);
    if(is_array($arr_option) && sizeof($arr_option)==2){
        $v_custom_width = $arr_option[0];
        $v_custom_height = $arr_option[1];
        settype($v_custom_width, 'float');
        settype($v_custom_height, 'float');
        $v_custom = $v_custom_height>0 && $v_custom_width>0;
    }
}


$v_folding_type = "none";
$v_die_cut_type = "none";

$arr_data = array('success'=>0);

$v_index_color = 0;
$arr_color = array();
$arr_use_color = array();
$v_product_row = 0;



if($v_template_id>0){
    //add_class('cls_tb_product');
    //$cls_product = new cls_tb_product($db, LOG_DIR);
    //$v_product_row = $cls_product->select_one(array('product_id'=>$v_product_id));

    $arr_where_clause = array('template_id'=>$v_template_id);
    add_class('cls_tb_design_template');

    $cls_templates = new cls_tb_design_template($db, LOG_DIR);
    $v_row = $cls_templates->select_one($arr_where_clause);


    if($v_row==1){
        $v_template_bleed = $cls_templates->get_template_bleed();
        add_class('cls_tb_design_theme');
        $cls_themes = new cls_tb_design_theme($db, LOG_DIR);
        $v_theme_row = $cls_themes->select_one(array('theme_id'=>$v_theme_id, 'theme_status'=>0));
        $v_theme_color = 'ffffff';
        $v_theme_name = '';
        if($v_theme_row==1){
            $arr_theme_color = $cls_themes->get_list_color();
            $v_theme_color = $cls_themes->get_theme_color();
            $v_theme_name = $cls_themes->get_theme_name();
        }
        if(!isset($arr_theme_color) || !is_array($arr_theme_color)) $arr_theme_color = array();


        $v_width = $cls_templates->get_template_width();
        $v_height = $cls_templates->get_template_height();
        $v_folding = $cls_templates->get_folding_type();
        $v_die_cut = $cls_templates->get_die_cut_type();
        $arr_product_list = $cls_templates->get_product_list();
        $v_template_name = $cls_templates->get_template_name();
        $v_stock_cost = $cls_templates->get_stock_cost();
        $v_markup_cost = $cls_templates->get_markup_cost();
        $v_created_time = $cls_templates->get_created_time();
        $v_assign_to = $cls_templates->get_user_id();
        $v_template_status = $cls_templates->get_template_status();

        $arr_template_color = $cls_templates->get_template_color();
        $v_template_data = $cls_templates->get_template_data();
        $v_template_image = $cls_templates->get_template_image();
        $v_folding_direction = $cls_templates->get_folding_direction();
        if(!$v_custom){
            $v_custom_width = $v_width;
            $v_custom_height = $v_height;
            //$v_custom = true;
        }

        $v_folding_type = $cls_settings->get_option_key_by_id('folding_type', $v_folding, 'none');
        $v_die_cut_type = $cls_settings->get_option_key_by_id('die_cut_type', $v_die_cut, 'none');
        $v_folding_direction = $cls_settings->get_option_key_by_id('folding_direction', $v_folding_direction, 'vertical');

        $arr_template_images =  json_decode($v_template_image, true);
        if(!is_array($arr_template_images)) $arr_template_images = array();


        $arr_fotolia_licenses = array();
        $arr_fotolia_child = array();
        $arr_temp = array();
        $arr_stock_image = array();
        foreach($arr_template_images as $image_id=>$arr_i){
            $v_image_id = $arr_i['id'];
            $license = isset($arr_i['licenses'])?$arr_i['licenses']:array();
            if(!is_array($license)) $license = array();
            foreach($license as $fotolia => $arr){
                $arr_fotolia_licenses[$fotolia] = $arr;
                $arr_temp[$fotolia] = array('id'=>$v_image_id, 'name'=>$arr_i['name']);
            }
        }
        foreach($arr_fotolia_licenses as $fotolia=>$arr){
            $v_size = sizeof($arr)-1;
            if($v_size>=0){
                $arr_fotolia_child[] = array(
                    'fotolia_id'=>$fotolia,
                    'fotolia_license'=>$arr[$v_size]['name'],
                    'price'=>$arr[$v_size]['price'],
                    'width'=>$arr[$v_size]['width'],
                    'height'=>$arr[$v_size]['height'],
                    'image_id'=>$arr_temp[$fotolia]['id'],
                    'name'=>$arr_temp[$fotolia]['name']
                );
                $arr_stock_image[$fotolia] = array(
                    'fotoliaId'=>$fotolia,
                    'maxLicense'=>$arr[$v_size]['name'],
                    'assigned'=>true
                );
            }
        }

        $arr_json = array();
        if($v_template_data!='') $arr_json = json_decode($v_template_data, true);

        $arr_tmp = array(
            'texts'=>array()
            ,'images'=>array()
            ,'width'=> ($v_custom_width + 2 * $v_template_bleed)
            ,'height'=>($v_custom_height + 2 * $v_template_bleed)
            ,'bg_color'=> "FFFFFF"
            ,'bleed'=>	$v_template_bleed
            ,'droppable'=> true
            ,'name'=> "Front Side"
            ,"isViewed"=>false
        );

        $v_change = false;
        if(!(isset($arr_json['canvases'])) || !is_array($arr_json['canvases'])){
            $arr_canvases = array();
            if($v_folding<=1)
                $arr_canvases = array($arr_tmp);
            else{
                $arr_canvases = array($arr_tmp, $arr_tmp);
                if($v_folding==1)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding>=2){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
            $v_change = true;
        }else{
            $arr_canvases = $arr_json['canvases'];
            if($v_folding<1){
                if(count($arr_canvases)>1){
                    $v_change = true;
                    $i=0;
                    $arr_canvas = array();
                    foreach($arr_canvases as $idx=>$arr){
                        if($i==0) $arr_canvas[] = $arr;
                        $i++;
                    }
                    $arr_canvases = $arr_canvas;
                }
            }else{
                if(count($arr_canvases)==1){
                    $arr_canvas = $arr_canvases[0];
                    $arr_canvases = array($arr_canvas, $arr_tmp);
                    $v_change = true;
                }else if(count($arr_canvases)>2){
                    $v_change = true;
                    $arr_canvas = array();
                    $i=0;
                    foreach($arr_canvases as $idx=>$arr){
                        if($i<=1){
                            $arr_canvas[$i] = $arr;
                        }
                        $i++;
                    }
                    $arr_canvases = $arr_canvas;
                }

                if($v_folding==1)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding>=2){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
        }
        if($v_change) $arr_json['canvases'] = $arr_canvases;

        $arr_data['width'] = $v_custom_width;
        $arr_data['height'] = $v_custom_height;
        $arr_data['folding'] = $v_folding_type;
        $arr_data['diecut'] = $v_die_cut_type;
        $arr_data['title'] = $v_template_name;
        $arr_data['id'] = $v_template_id;
        $arr_data['price_markup'] = $v_markup_cost;
        $arr_data['assign_to'] = $v_assign_to;
        $arr_data['product_id'] = isset($v_product_id)?$v_product_id:0;
        $arr_data['product_title'] = $v_product_title;
        $arr_data['product_url_code'] = $v_product_code;
        $arr_data['stock_image_code'] = $v_stock_cost;
        $arr_data['size_title'] = "";
        $arr_data['set_id'] = $v_template_id;
        $arr_data['set_title'] = $v_template_name;
        $arr_data['status_id'] = $v_template_status;
        $arr_data['success'] = 1;
        $arr_data['images']= array();//
        $arr_data['json']= array();//
        $arr_data['fotoliaLicenses']= $arr_fotolia_licenses;
        $arr_data['fotoliaChildItems']= $arr_fotolia_child;
        $arr_data['themes']= array();

        if(count($arr_theme_color)>0){
            for($i=0; $i<count($arr_canvases);$i++){


                $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                for($j=0;$j<count($arr_images);$j++){
                    $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
                    settype($v_image_id, 'int');
                    if($v_image_id>0){

                        $v_key = 'name_'.$arr_images[$j]['name'];
                        if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                            $v_color = $arr_theme_color[$v_key]['color'];
                            if(!isset($arr_color[$v_color])) $arr_color[$v_color] = $v_index_color++;
                            $v_index = $arr_color[$v_color];
                            $arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'].'#'.$v_index;
                            $arr_use_color[$v_index] = array('name'=>$v_key, 'color'=>$v_color);
                        }

                        $v_svg_id = isset($arr_images['svg_id'])?intval($arr_images['svg_id']):0;
                        if($v_svg_id>0){
                            $v_key = 'svg_'.$arr_images[$j]['name'];
                            if(isset($arr_theme_color[$v_key]['color'])){
                                $arr_svg_colors = $arr_theme_color[$v_key]['color'];
                                if(!is_array($arr_svg_colors)) $arr_svg_colors = array();
                                $arr_use_svg_color = array();
                                for($k = 0; $k<sizeof($arr_svg_colors); $k++){
                                    $v_color = $arr_svg_colors[$k];
                                    if(!isset($arr_color[$v_color])) $arr_color[$v_color] = $v_index_color++;
                                    $v_index = $arr_color[$v_color];
                                    $arr_use_svg_color[] = $v_color.'#'.$v_index;
                                }
                                $arr_images['svg_colors'] = $arr_use_svg_color;
                            }
                        }
                    }else{
                        $v_key = 'fill_'.$arr_images[$j]['name'];
                        if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                            $v_color = $arr_theme_color[$v_key]['color'];
                            if(!isset($arr_color[$v_color])) $arr_color[$v_color] = $v_index_color++;
                            $v_index = $arr_color[$v_color];
                            $arr_images[$j]['fill_color'] = $arr_theme_color[$v_key]['color'].'#'.$v_index;
                            $arr_use_color[$v_index] = array('name'=>$v_key, 'color'=>$v_color);
                        }
                        $v_key = 'border_'.$arr_images[$j]['name'];
                        if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
                            $v_color = $arr_theme_color[$v_key]['color'];
                            if(!isset($arr_color[$v_color])) $arr_color[$v_color] = $v_index_color++;
                            $v_index = $arr_color[$v_color];
                            $arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'].'#'.$v_index;
                            $arr_use_color[$v_index] = array('name'=>$v_key, 'color'=>$v_color);
                        }
                    }
                }
                $arr_canvases[$i]['images'] = $arr_images;

                $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
                for($j=0;$j<count($arr_texts);$j++){
                    $v_key = 'text_'.$arr_texts[$j]['name'];
                    if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='text'){
                        $v_color = $arr_theme_color[$v_key]['color'];
                        if(!isset($arr_color[$v_color])) $arr_color[$v_color] = $v_index_color++;
                        $v_index = $arr_color[$v_color];
                        $arr_texts[$j]['color'] = $arr_theme_color[$v_key]['color'].'#'.$v_index;
                        $arr_use_color[$v_index] = array('name'=>$v_key, 'color'=>$v_color);
                    }
                }
                $arr_canvases[$i]['texts'] = $arr_texts;
            }

            $arr_colors = array();

            for($i=0;$i<count($arr_use_color);$i++){
                $arr_colors[] = $arr_use_color[$i]['color'];
            }
            $arr_colors[] = $v_theme_color;
            $arr_theme = array(
                "id"=>$v_theme_id.""
                ,"name"=>$v_theme_name
                ,"delete_flag"=>0
                ,"colors"=>$arr_colors
                ,'color'=>$v_theme_color
            );
            $arr_themes = array();
            $arr_all_themes = $cls_themes->select(array('template_id'=>$v_template_id, 'theme_status'=>0));
            foreach($arr_all_themes as $arr){
                $v_tmp_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
                $v_tmp_theme_name = isset($arr['theme_name'])?$arr['theme_name']:0;
                $v_tmp_theme_color = isset($arr['theme_color'])?$arr['theme_color']:'ffffff';
                $arr_tmp_theme_color = isset($arr['list_color'])?$arr['list_color']:array();

                if($v_theme_id==$v_tmp_theme_id){
                    $arr_themes[] = $arr_theme;
                }else{
                    $arr_colors = array();
                    for($i=0;$i<count($arr_use_color);$i++){
                        $v_name = $arr_use_color[$i]['name'];
                        if(isset($arr_tmp_theme_color[$v_name]['color'])){
                            $arr_colors[] = $arr_tmp_theme_color[$v_name]['color'];
                        }else{
                            $arr_colors[] = 'ffffff';
                        }
                    }
                    $arr_colors[] = $v_tmp_theme_color;
                    $arr_themes[] = array(
                        "id"=>$v_tmp_theme_id.""
                        ,"name"=>$v_tmp_theme_name
                        ,"delete_flag"=>0
                        ,"colors"=>$arr_colors
                        ,'color'=>$v_tmp_theme_color
                    );

                }
            }
            $arr_data['theme'] = $arr_theme;
            $arr_data['themes'] = $arr_themes;
        }

        //Calculate for CUSTOM
        $v_ratio_width = $v_custom_width/$v_width;
        $v_ratio_height = $v_custom_height/$v_height;

        $v_canvases_count = sizeof($arr_canvases);
        for($i=0; $i<$v_canvases_count;$i++){
            $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
            $v_texts_count = sizeof($arr_texts);
            for($j=0; $j<$v_texts_count;$j++){
                $v_left = isset($arr_texts[$j]['left'])?$arr_texts[$j]['left']:0;
                $v_top = isset($arr_texts[$j]['top'])?$arr_texts[$j]['top']:0;
                $v_text_width = isset($arr_texts[$j]['width'])?$arr_texts[$j]['width']:0;
                $v_text_height = isset($arr_texts[$j]['height'])?$arr_texts[$j]['height']:0;
                $v_pointsize = isset($arr_texts[$j]['pointsize'])?$arr_texts[$j]['pointsize']:0;

                $v_new_left = $v_left * $v_ratio_width;
                $v_new_top = $v_top * $v_ratio_height;
                if($v_ratio_height<$v_ratio_width){
                    $v_new_w = $v_text_width * $v_ratio_height;
                    $v_new_h = $v_text_height * $v_ratio_height;
                    $v_pointsize = round($v_pointsize * $v_ratio_height);
                }else{
                    $v_new_w = $v_text_width * $v_ratio_width;
                    $v_new_h = $v_text_height * $v_ratio_width;
                    $v_pointsize = round($v_pointsize * $v_ratio_width);
                }
                $arr_texts[$j]['left'] = $v_new_left;
                $arr_texts[$j]['top'] = $v_new_top;
                $arr_texts[$j]['width'] = $v_new_w;
                $arr_texts[$j]['height'] = $v_new_h;
                $arr_texts[$j]['pointsize'] = $v_pointsize;
            }
            $arr_canvases[$i]['texts'] = $arr_texts;

            $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
            $v_images_count = sizeof($arr_images);
            for($j=0;$j<$v_images_count;$j++){
                $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                $v_image_name = isset($arr_images[$j]['name'])?$arr_images[$j]['name']:'';
                $v_image_w = isset($arr_images[$j]['width'])?$arr_images[$j]['width']:0;
                $v_image_h = isset($arr_images[$j]['height'])?$arr_images[$j]['height']:0;
                $v_left = isset($arr_images[$j]['left'])?$arr_images[$j]['left']:0;
                $v_top = isset($arr_images[$j]['top'])?$arr_images[$j]['top']:0;
                $v_zindex = isset($arr_images[$j]['zindex'])?$arr_images[$j]['zindex']:0;
                $v_crop = isset($arr_images[$j]['crop'])?$arr_images[$j]['crop']:false;
                $v_cropTop = isset($arr_images[$j]['cropTop'])?$arr_images[$j]['cropTop']:0;
                $v_cropLeft = isset($arr_images[$j]['cropLeft'])?$arr_images[$j]['cropLeft']:0;
                $v_cropWidth = isset($arr_images[$j]['cropWidth'])?$arr_images[$j]['cropWidth']:1;
                $v_cropHeight = isset($arr_images[$j]['cropHeight'])?$arr_images[$j]['cropHeight']:1;
                $v_border_width = isset($arr_images[$j]['border_width'])?$arr_images[$j]['border_width']:0;

                //$v_current_left = $v_new_left;
                //$v_current_top = $v_new_top;
                $v_current_x = $v_left + $v_image_w/2;
                $v_current_y = $v_top + $v_image_h/2;

                //$v_new_left = $v_current_left * $v_ratio_width;
                //$v_new_top = $v_current_top * $v_ratio_height;


                if($v_image_id>0){ // image
                    if($v_custom){
                        if($v_crop){
                            if($v_ratio_height!=$v_ratio_width){
                                $v_new_image_w = $v_image_w * $v_ratio_width;
                                $v_new_image_h = $v_image_h * $v_ratio_height;

                                $v_current_left = $v_left * $v_ratio_width;
                                $v_current_top = $v_top * $v_ratio_height;
                                if($v_ratio_height<$v_ratio_width){
                                    $v_new_cropLeft = $v_cropLeft;
                                    $v_new_cropWidth = $v_cropWidth;

                                    $v_right_height = $v_cropHeight>0?($v_image_h * $v_ratio_width) / $v_cropHeight:0;
                                    $v_crop_height = $v_cropHeight * $v_right_height;
                                    $v_height_lost = $v_crop_height - $v_new_image_h;
                                    if($v_height_lost>0){
                                        //$v_new_cropTop = $v_cropTop + $v_height_lost/(2*$v_right_height);
                                        $v_new_cropTop = $v_cropTop + $v_height_lost/(2*$v_right_height);
                                        //$v_new_cropHeight = $v_cropHeight - $v_height_lost/(2*$v_right_height);
                                        $v_new_cropHeight = $v_new_image_h/($v_right_height);

                                    }else{
                                        $v_new_cropTop = $v_cropTop;
                                        $v_new_cropHeight = $v_new_image_h / $v_right_height;
                                    }
                                }else{
                                    $v_new_cropTop = $v_cropTop;
                                    $v_new_cropHeight = $v_cropHeight;

                                    $v_right_width = $v_cropWidth>0?($v_image_w * $v_ratio_height) / $v_cropWidth:0;
                                    $v_crop_width = $v_cropWidth * $v_right_width;
                                    $v_width_lost = $v_crop_width - $v_new_image_w;
                                    if($v_width_lost>0){
                                        $v_new_cropLeft = $v_cropLeft + $v_width_lost/(2*$v_right_width);
                                        $v_new_cropWidth = $v_new_image_w / $v_right_width;
                                    }else{
                                        $v_new_cropLeft = $v_cropLeft;
                                        $v_new_cropWidth = $v_new_image_w / $v_right_width;
                                    }
                                }
                                $arr_images[$j]['width'] = $v_new_image_w;
                                $arr_images[$j]['height'] = $v_new_image_h;
                                $arr_images[$j]['left'] = $v_current_left;
                                $arr_images[$j]['top'] = $v_current_top;
                                $arr_images[$j]['cropTop'] = $v_new_cropTop;
                                $arr_images[$j]['cropHeight'] = $v_new_cropHeight;
                                $arr_images[$j]['cropLeft'] = $v_new_cropLeft;
                                $arr_images[$j]['cropWidth'] = $v_new_cropWidth;
                                //$arr_images[$j]['unmovable'] = true;
                                //$arr_images[$j]['noresize'] = true;
                                //$arr_images[$j]['release'] = true;
                            }else{
                                $v_new_image_w = $v_image_w * $v_ratio_width;
                                $v_new_image_h = $v_image_h * $v_ratio_width;
                                $v_current_left = $v_left * $v_ratio_width;
                                $v_current_top = $v_top * $v_ratio_width;
                                $arr_images[$j]['width'] = $v_new_image_w;
                                $arr_images[$j]['height'] = $v_new_image_h;
                                $arr_images[$j]['left'] = $v_current_left;
                                $arr_images[$j]['top'] = $v_current_top;
                                $arr_images[$j]['cropLeft'] = $v_cropLeft;
                                $arr_images[$j]['cropTop'] = $v_cropTop;
                                $arr_images[$j]['cropWidth'] = $v_cropWidth;
                                $arr_images[$j]['cropHeight'] = $v_cropHeight;
                            }
                            $arr_images[$j]['fitted'] = true;
                            //$arr_images[$j]['noresize'] = true;
                            //$arr_images[$j]['unmovable'] = true;
                            //$arr_images[$j]['noreplace'] = true;
                            //$arr_images[$j]['placeholder'] = 'filler';
                        }else{
                            if($v_ratio_height<$v_ratio_width){
                                $v_new_image_w = $v_image_w * $v_ratio_height;
                                $v_new_image_h = $v_image_h * $v_ratio_height;
                            }else{
                                $v_new_image_w = $v_image_w * $v_ratio_width;
                                $v_new_image_h = $v_image_h * $v_ratio_width;
                            }
                            $v_new_x = $v_current_x * $v_ratio_width;
                            $v_new_y = $v_current_y * $v_ratio_height;
                            $v_new_left = $v_new_x - $v_new_image_w/2;
                            $v_new_top = $v_new_y - $v_new_image_h/2;

                            $arr_images[$j]['top'] = $v_new_top;
                            $arr_images[$j]['left'] = $v_new_left;
                            $arr_images[$j]['width'] = $v_new_image_w;
                            $arr_images[$j]['height'] = $v_new_image_h;
                        }
                    }else{
                        if($v_ratio_height<$v_ratio_width){
                            $v_new_image_w = $v_image_w * $v_ratio_height;
                            $v_new_image_h = $v_image_h * $v_ratio_height;
                        }else{
                            $v_new_image_w = $v_image_w * $v_ratio_width;
                            $v_new_image_h = $v_image_h * $v_ratio_width;
                        }
                        $v_new_x = $v_current_x * $v_ratio_width;
                        $v_new_y = $v_current_y * $v_ratio_height;
                        $v_new_left = $v_new_x - ceil($v_new_image_w/2);
                        $v_new_top = $v_new_y - ceil($v_new_image_h/2);
                        $arr_images[$j]['top'] = $v_new_top;
                        $arr_images[$j]['left'] = $v_new_left;
                        $arr_images[$j]['width'] = $v_new_image_w;
                        $arr_images[$j]['height'] = $v_new_image_h;
                    }
                }else{ //shape
                    $v_new_image_w = $v_image_w * $v_ratio_width;
                    $v_new_image_h = $v_image_h * $v_ratio_height;
                    $v_new_left = $v_left * $v_ratio_width;
                    $v_new_top = $v_top * $v_ratio_height;
                    $arr_images[$j]['top'] = $v_new_top;
                    $arr_images[$j]['left'] = $v_new_left;
                    $arr_images[$j]['width'] = $v_new_image_w;
                    $arr_images[$j]['height'] = $v_new_image_h;
                }
                if(isset($arr_images[$j]['ontop']) && $arr_images[$j]['ontop']){
                    $arr_images[$j]['type'] = 'ontop';
                    $v_ontop = true;
                }
            }
            $arr_canvases[$i]['images'] = $arr_images;
            $arr_canvases[$i]['width'] = $v_custom_width;
            $arr_canvases[$i]['height'] = $v_custom_height;
        }


        $arr_json = array(
            'version'=>'2.0',
            'canvases'=>$arr_canvases
            ,'width'=>$v_custom_width
            ,'height'=>$v_custom_height
            ,'dieCutType'=>$v_die_cut_type
            ,'folding'=>$v_folding_type
            ,'foldingDirection'=>$v_folding_direction
            ,'product'=>$v_product_key
            ,'stockImages'=>$arr_stock_image
            ,'wrap_size'=>0
        );

        $arr_data['size_title'] = $v_custom_width.'" &times; '.$v_custom_height.'"';
        $arr_data['save_action'] = 'save_theme';
        $arr_data['images'] = $arr_template_images;
        $arr_data['json'] = $arr_json;
        $arr_data['ontop'] = $v_ontop?$v_logo_index:0;
        //$v_order_id = isset($_SESSION['ss_design_order_id']) ? $_SESSION['ss_design_order_id'] : 0;
        //$v_order_item_id = isset($_SESSION['ss_design_order_item_id']) ? $_SESSION['ss_design_order_item_id'] : 0;
        //$arr_data['order_id'] = $v_order_id;
        //$arr_data['order_item_id'] = $v_order_item_id;
        //$arr_data['url'] = URL;
    }
}

if($v_is_cross_domain)
    $cls_output->output($arr_data);
else
    $cls_output->output_jsonp($arr_data, 'jsonp_callback');