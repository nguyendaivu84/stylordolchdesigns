<?php
if(!isset($v_sval)) die();
?>
<?php
$v_json = isset($_REQUEST['json'])?$_REQUEST['json']:'';
$v_name = isset($_REQUEST['name'])?$_REQUEST['name']:'';
$v_dpi = isset($_REQUEST['dpi'])?$_REQUEST['dpi']:'0';

$v_product_code = isset($_REQUEST['product_code'])?$_REQUEST['product_code']:'';
$v_product_title = isset($_REQUEST['product_title'])?$_REQUEST['product_title']:'';
$v_user_id = isset($_REQUEST['user_id'])?$_REQUEST['user_id']:'0';
$v_user_name = isset($_REQUEST['user_name'])?$_REQUEST['user_name']:'';
$v_user_ip = isset($_REQUEST['user_ip'])?$_REQUEST['user_ip']:'';
$v_user_agent = isset($_REQUEST['user_agent'])?$_REQUEST['user_agent']:'';
$v_user_company_id = isset($_REQUEST['user_company_id'])?$_REQUEST['user_company_id']:'0';
$v_user_location_id = isset($_REQUEST['user_location_id'])?$_REQUEST['user_location_id']:'0';


settype($v_user_id, 'int');
settype($v_user_company_id, 'int');
settype($v_user_location_id, 'int');

settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');
settype($v_product_id, 'int');
add_class('cls_tb_design_image');
add_class('cls_tb_design_design');
add_class('cls_tb_design_template');
add_class('cls_tb_design_stock');

$arr_product = array('id'=>$v_product_id, 'code'=>$v_product_code, 'title'=>$v_product_title);

$cls_images = new cls_tb_design_image($db,LOG_DIR);
$cls_designs = new cls_tb_design_design($db,LOG_DIR);
$cls_templates = new cls_tb_design_template($db,LOG_DIR);
$cls_stocks = new cls_tb_design_stock($db,LOG_DIR);


$v_row = $cls_designs->select_one(array('design_id'=>$v_design_id));
$v_success = 0;

$v_str = '';
$v_image_data = '';
$v_sample_image = '';
$v_prefix = '';
$v_stock_cost = 0;
$v_markup_cost = 0;
$v_print_cost = 0;

$arr_tmp_images = array();
if(get_magic_quotes_gpc())
    $v_json = stripslashes($v_json);
$arr_json = json_decode($v_json, true);
$arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();

$v_saved_dir = '';
$v_sample_image = '';
$v_design_unused = false;
$arr_image_used = array();
$v_old_design_data = '';
if($v_row==1){
    $v_saved_dir = $cls_designs->get_saved_dir();
    $v_sample_image = $cls_designs->get_sample_image();
    $v_design_width = $cls_designs->get_design_width();
    $v_design_height = $cls_designs->get_design_height();
    $v_design_bleed = $cls_designs->get_design_bleed();
    $v_folding_type = $cls_designs->get_design_folding();
    $v_folding_direction = $cls_designs->get_design_direction();
    $v_die_cut_type = $cls_designs->get_design_die_cut();
    $v_old_design_data = $cls_designs->get_design_data();
    $v_design_unused = true;
}else{
    $v_row = $cls_templates->select_one(array('template_id'=>$v_template_id));
    if($v_row==1){
        $v_design_width = $cls_templates->get_template_width();
        $v_design_height = $cls_templates->get_template_height();
        $v_design_bleed = $cls_templates->get_template_bleed();
        $v_folding_type = $cls_templates->get_folding_type();
        $v_folding_direction = $cls_templates->get_folding_direction();
        $v_die_cut_type = $cls_templates->get_die_cut_type();
        $v_old_design_data = $cls_templates->get_template_data();

    }else{
        $v_design_width = isset($arr_json['width'])?$arr_json['width']:0;
        $v_design_height = isset($arr_json['height'])?$arr_json['height']:0;
        $v_folding_type = 0;
        $v_folding_direction = 0;
        $v_die_cut_type = 0;
        $v_design_bleed = 0;
    }
}
$arr_old_image = array();
if($v_design_unused){
    $arr_json = json_decode($v_old_design_data, true);
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    if(is_array($arr_canvases)){
        $v_size = sizeof($arr_canvases);
        if($v_size>0 && $v_size<=2){
            for($i=0; $i<$v_size; $i++){
                $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                if(is_array($arr_images)){
                    $v_image_size = sizeof($arr_images);
                    for($j=0; $j < $v_image_size; $j++){
                        $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                        settype($v_image_id, 'int');
                        $arr_images[$v_image_id] = true;
                    }
                }
            }
        }
    }
}

$arr_tmp_images = array();
if(get_magic_quotes_gpc())
    $v_json = stripslashes($v_json);
$arr_json = json_decode($v_json, true);
$arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();

$arr_tmp = array(
    'texts'=>array()
    ,'images'=>array()
    ,'width'=>($v_design_width + 2 * $v_design_bleed)
    ,'height'=>($v_design_height + 2 * $v_design_bleed)
    ,'bg_color'=> "FFFFFF"
    ,'bleed'=>	$v_design_bleed
    ,'droppable'=> true
    ,'name'=> "Front Side"
    ,"isViewed"=>false
);

$v_total_page = count($arr_canvases);

if($v_folding_type<=0){
    if($v_total_page>=1){
        $arr_canvas = array();
        $i = 0;
        foreach($arr_canvases as $idx=>$arr){
            if($i==0) $arr_canvas[] = $arr;
            $i++;
        }
    }else{
        $arr_canvas[] = $arr_tmp;
    }
    $arr_canvases = $arr_canvas;
}else{
    if($v_total_page<1){
        $arr_canvases = array($arr_tmp, $arr_tmp);
    }else if($v_total_page==1){
        $i=0;
        $arr_canvas = array();
        foreach($arr_canvases as $idx=>$arr){
            if($i==0) $arr_canvas[0] = $arr;
            $i++;
        }
        $arr_canvas[1] = $arr_tmp;
    }else{
        $i=0;
        $arr_canvas = array();
        foreach($arr_canvases as $idx=>$arr){
            if($i<=1) $arr_canvas[$i] = $arr;
            $i++;
        }
    }
    if($v_folding_type==1)
        $arr_canvases[1]['name'] = 'Back Side';
    else{
        $arr_canvases[1]['name'] = 'InSide';
        $arr_canvases[0]['name'] = 'OutSide';
    }
}

$arr_json['canvases'] = $arr_canvases;
$v_total_page = count($arr_canvases);


$v_page = 0;



$v_ext = 'png';
$v_accept = true;
if($v_sample_image!='' && $v_saved_dir!=''){
    if(!file_exists($v_root_dir.$v_saved_dir.$v_sample_image))
        $v_sample_image = '';
    else{
        $v_accept = @unlink($v_root_dir.$v_saved_dir.$v_sample_image);
    }
}else $v_sample_image = '';
if($v_row!=1 || $v_sample_image=='')
    $v_sample_image = 'template'.$v_template_id.'_theme'.$v_theme_id.'_'.date('YmdHis').'.'.$v_ext;

$v_dir = DESIGN_DESIGN_DIR.DS;

if(file_exists($v_dir.$v_template_id) || @mkdir($v_dir.$v_template_id)){
    $v_dir .= $v_template_id .DS;
}

$v_saved_dir = str_replace($v_root_dir,'',$v_dir);
$v_saved_dir = str_replace('\\','/', $v_saved_dir);

$v_full_path = $v_dir.$v_sample_image;

$cls_tmp_draw = new cls_draw();
$image = new Imagick();
if($v_total_page<=1){
    $v_page = 0;
    $v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
    $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
    if($v_accept){
        $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);
        $image->setimageformat('png');
        $image->writeimage($v_full_path);
        $image->clear();
        $image->destroy();
    }
}else{
    if($v_accept){
        $v_page = 1;
        $image2 = new Imagick();
        $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
        $v_page = 0;
        $image1 = new Imagick();
        $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
        $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

        $image->setimageformat('png');
        $image->writeimage($v_full_path);
        $image->clear();
        $image->destroy();
        $image1->clear();
        $image1->destroy();
        $image2->clear();
        $image2->destroy();

    }
}

$arr_text_name = array();
    for($i=0; $i<count($arr_canvases); $i++){
        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();

        for($j=0;$j<count($arr_images);$j++){
            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
            $v_noprint = isset($arr_images[$j]['noprint'])?$arr_images[$j]['noprint']:'false';
            settype($v_image_id, 'int');
            if($v_image_id>0){
                $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
                if($v_row==1){
                    $v_image_name = $cls_images->get_image_name();
                    $v_image_width = $cls_images->get_image_width();
                    $v_image_height = $cls_images->get_image_height();
                    $v_image_dpi = $cls_images->get_image_dpi();
                    $v_image_extension = $cls_images->get_image_extension();
                    $v_is_public = $cls_images->get_is_public();
                    $v_is_vector = $cls_images->get_is_vector();
                    $v_customer_id = $cls_images->get_user_id();
                    $v_created_time = $cls_images->get_created_time();

                    $v_fotolia_id = $cls_images->get_fotolia_id();
                    $v_fotolia_size = $cls_images->get_fotolia_size();
                    $v_image_stock_id = $cls_images->get_image_stock_id();
                    $v_svg_id = $cls_images->get_svg_id();
                    $arr_svg_colors = $cls_images->get_svg_original_colors();

                    if(isset($arr_old_image[$v_image_id])){
                        $arr_old_image[$v_image_id] = false;
                    }else{
                        $arr_image_used[$v_image_id] = array(
                            'template'=>0, 'design'=>1
                        );
                    }

                    $arr_license = array();

                    if($v_image_stock_id>0){
                        $v_stock_row = $cls_stocks->select_one(array('stock_id'=>$v_image_stock_id));
                        if($v_stock_row==1){
                            $v_change = true;
                            $arr_images[$j]['fotoliaId'] = $v_fotolia_id;
                            $arr_images[$j]['fotoliaLicense'] = $v_fotolia_size;
                            $license = $cls_stocks->get_licenses();
                            if(is_array($license)){
                                for($k=0; $k<sizeof($license);$k++){
                                    if(isset($license[$k]['dimensions'])) unset($license[$k]['dimensions']);
                                    $license[$k]['id'] = (intval($v_image_id) * 100) + $k;
                                    $license[$k]['fotolia_id'] = $v_fotolia_id;
                                    $license[$k]['license_value'] = $license[$k]['price'];
                                    $license[$k]['is_on_disk'] = 0;
                                    $license[$k]['use_for_comp'] = 0;
                                }
                                $arr_license[$v_fotolia_id] = $license;
                            }
                        }
                    }


                    if(is_object($v_created_time))
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time->sec);
                    else
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time);

                    $v_stock_cost += $cls_images->get_image_cost();
                    $arr_tmp_images[$v_image_id] = array(
                        "id"=>$v_image_id.''
                        ,'customer_id'=> "".$v_customer_id
                        ,'session_id'=> "".session_id()
                        ,'site_code'=> DESIGN_SITE_CODE
                        ,'asset_name'=> ""
                        ,'is_vector'=> $v_is_vector
                        ,'name'=> $v_image_name
                        ,'extension'=> $v_image_extension
                        ,'width'=> $v_image_width.''
                        ,'height'=> $v_image_height.''
                        ,'dpi'=> $v_image_dpi
                        ,'added_on'=> $v_created_time
                        ,'is_public'=> $v_is_public
                        ,'delete_flag'=> "1"
                        ,'licenses'=>$arr_license
                        ,'svg_original_colors'=>$arr_svg_colors
                        ,'svg_id'=>$v_svg_id
                    );
                }
            }
        }
        for($j=0;$j<count($arr_texts);$j++){
            $v_title = isset($arr_texts[$j]['title'])?$arr_texts[$j]['title']:'';
            if($v_title=='') $v_title = 'Enter Text';
            if($v_title!=''){
                $v_title = html_entity_decode($v_title);
                $v_title = urldecode($v_title);
                $v_title = seo_friendly_url($v_title);
                $v_title = $v_prefix.str_replace('-','_', $v_title);
                if(isset($arr_text_name[$v_title])){
                    $k = 1;
                    do{
                        $v_new_title = $v_title.'_'.$k;
                        if(!isset($arr_text_name[$v_new_title])){
                            $arr_texts[$j]['name'] = $v_new_title;
                            $arr_text_name[$v_new_title] = 1;
                            $k = -1;
                        }else{
                            $k++;
                        }
                    }while($k>0);
                }else{
                    $arr_texts[$j]['name'] = $v_title;
                    $arr_text_name[$v_title] = 1;
                }
            }

        }
        $arr_canvases[$i]['texts'] = $arr_texts;

    }
    $v_image_data = json_encode($arr_tmp_images);
    $arr_json['canvases'] = $arr_canvases;
    $v_json = json_encode($arr_json);


    $arr_fields = array('design_data', 'design_image', 'stock_cost', 'saved_dir', 'sample_image', 'design_dpi', 'last_modified', 'product');
    $arr_values = array($v_json, $v_image_data, $v_stock_cost, $v_saved_dir, $v_sample_image, $v_dpi, new MongoDate(time()), $arr_product);

    $v_result_id = $cls_designs->update_fields($arr_fields, $arr_values, array('design_id'=>$v_design_id));
    if($v_result_id){
        $v_success = 1;
        foreach($arr_old_image as $v_image_id=>$no_used){
            if($no_used){
                $arr_image_used[$v_image_id] = array(
                    'template'=>0, 'design'=>$v_design_unused? -1:0
                );
            }
        }
        $cls_images->update_image_used($arr_image_used);
    }

$arr_return = array('success'=>$v_success, 'design_id'=>$v_design_id, 'theme_id'=>$v_theme_id);
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');