<?php
if(!isset($v_sval)) die();?>
<?php
$v_folding_type = "none";
$arr_data = array('success'=>0);

add_class('cls_design_designs');
$cls_designs = new cls_design_designs($cn, '', _PREFIX_TBL, false);
$v_row = $cls_designs->select_one(" AND `design_id`='{$v_design_id}' AND `design_status`=1");
if($v_row==1){
    $v_template_id = $cls_designs->get_template_id();
    $v_theme_id = $cls_designs->get_theme_id();
    $v_product_id = $cls_designs->get_product_id();
    $v_design_data = $cls_designs->get_design_data();
    $v_image_data = $cls_designs->get_image_data();
    $v_design_name = $cls_designs->get_design_name();

    add_class('cls_products');
    $cls_products = new cls_products($cn, '', _PREFIX_TBL, false);
    $v_product_row = $cls_products->select_one(" AND `id`='{$v_product_id}'");

    add_class('cls_design_type');
    $cls_design_type = new cls_design_type($cn, '', _PREFIX_TBL, false);
    $v_where_clause = " AND `template_id`='{$v_template_id}'";
    add_class('cls_design_templates');
    $cls_templates = new cls_design_templates($cn, '', _PREFIX_TBL, false);
    $v_row = $cls_templates->select_one($v_where_clause);

    if($v_row==1){
        $v_width = $cls_templates->get_template_width();
        $v_height = $cls_templates->get_template_height();
        $v_folding = $cls_templates->get_folding_type();
        $v_is_diecut = $cls_templates->get_is_die_cut();
        $v_template_name = $cls_templates->get_template_name();
        $v_stock_image_cost = $cls_templates->get_stock_image_cost();
        $v_price_markup = $cls_templates->get_price_markup();
        $v_created_time = $cls_templates->get_created_time();
        $v_assign_to = $cls_templates->get_assigned_to();
        $v_section_id = $cls_templates->get_section_id();
        $v_category_id = $cls_templates->get_category_id();
        $v_template_status = $cls_templates->get_template_status();

        $v_folding_name = $cls_design_type->get_scalar('type_name', " AND `type_id`='{$v_folding}'");
        $v_folding_type = $cls_design_type->get_scalar('type_key', " AND `type_id`='{$v_folding}'");
        if($v_folding_type=='') $v_folding_type = 'none';
        //$v_folding_type = $v_folding<1?"none": "letterFold";


        $arr_data['width'] = $v_width;
        $arr_data['height'] = $v_height;
        $arr_data['folding'] = $v_folding_type;
        $arr_data['diecut'] = "none";
        $arr_data['title'] = $v_template_name;
        $arr_data['id'] = $v_template_id;
        $arr_data['theme_id'] = $v_theme_id;
        $arr_data['price_markup'] = $v_price_markup;
        $arr_data['assign_to'] = $v_assign_to;
        $arr_data['product_id'] = $v_product_id;
        $arr_data['product_title'] = $v_product_row==1?$cls_products->get_title(): 'Blank Design';
        $arr_data['product_url_code'] = $v_product_row==1?$cls_products->get_short_title(): 'blank_design';
        $arr_data['stock_image_cost'] = $v_stock_image_cost;
        $arr_data['size_title'] = $v_width.'" &times; '.$v_height.'" '.$v_folding_name;
        $arr_data['set_id'] = $v_design_id;
        $arr_data['set_title'] = $v_design_name;
        $arr_data['status_id'] = $cls_designs->get_design_status();
        $arr_data['success'] = 1;
        $arr_data['images']= array();//
        $arr_data['json']= array();//
        $arr_data['fotoliaLicenses']= array();
        $arr_data['themes']= array();
        $arr_data['save_action']= 'design';//temporary

        $v_where_clause = " AND `template_id`='{$v_template_id}' AND `theme_id`='{$v_theme_id}'";
        add_class('cls_design_themes');
        $cls_themes = new cls_design_themes($cn, '', _PREFIX_TBL, false);
        $v_row = $cls_themes->select_one($v_where_clause);

        $arr_images = json_decode($v_image_data, true);
        if(!is_array($arr_images)) $arr_images = array();
        $arr_json = json_decode($v_design_data, true);
        if(!is_array($arr_json)){
            $arr_tmp = array(
                'texts'=>array()
                ,'images'=>array()
                ,'width'=>$v_width
                ,'height'=>$v_height
                ,'bg_color'=> "FFFFFF"
                ,'bleed'=>	0.125
                ,'droppable'=> true
                ,'name'=> "Front Side"
                ,"isViewed"=>false
            );
            $arr_canvases = array();
            if($v_folding==1)
                $arr_canvases = array($arr_tmp);
            else{
                $arr_canvases = array($arr_tmp, $arr_tmp);
                if($v_folding==2)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding==3){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
            //$arr_canvases['product'] = $v_product_row==1?$cls_product->get_keywords(): "blankDesign";
            $arr_json = array(
                'version'=>'1.0',
                'canvases'=>$arr_canvases
                ,'width'=>$v_width
                ,'height'=>$v_height
                ,'dieCutType'=>"none"
                ,'folding'=>$v_folding_type
                ,'foldingDirection'=>"vertical"
                ,'product'=>$v_product_row==1?$cls_products->get_keywords(): "blank_design"
                ,'stockImages'=>array()
                ,'wrap_size'=>0
            );
        }else{
            $arr_json['product']=$v_product_row==1?$cls_products->get_keywords(): "blank_design";
        }
        $arr_data['images'] = $arr_images;
        $arr_data['json'] = $arr_json;
    }
}

header("Content-type: application/json");
echo json_encode($arr_data);