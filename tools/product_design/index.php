<?php
if(!isset($v_sval)) die();
$v_template_id = isset($_REQUEST['template_id'])?$_REQUEST['template_id']:'0';
$v_theme_id = isset($_REQUEST['theme_id'])?$_REQUEST['theme_id']:'0';
$v_design_id = isset($_REQUEST['design_id'])?$_REQUEST['design_id']:'0';
$v_product_id = isset($_REQUEST['product_id'])?$_REQUEST['product_id']:'0';
$v_sub_action = isset($_GET['action_type'])?$_GET['action_type']:'';

settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');
settype($v_product_id, 'int');

if(isset($_SESSION['ss_design_side'])) unset($_SESSION['ss_design_side']);

$v_is_template = 0;

add_class('cls_tb_contact');
$cls_tb_contact = new cls_tb_contact($db, LOG_DIR);
$v_contact_id = isset($arr_user['contact_id'])?$arr_user['contact_id']:'0';
settype($v_contact_id, 'int');
$v_row = $cls_tb_contact->select_one(array('contact_id'=>$v_contact_id));

$v_member_first_name = $v_row==1?$cls_tb_contact->get_first_name():'Guest';
$v_member_last_name = $v_row==1?$cls_tb_contact->get_last_name():'';
$v_member_phone = $v_row==1?$cls_tb_contact->get_direct_phone():'';
$v_member_email = $v_row==1?($cls_tb_contact->get_email()):'';


if($v_site_id>0){
    $arr_switch = array(
        'save_design'=>'Save User\'s Design'
        ,'save_as_design'=>'Save As User\'s Design'
        ,'load_template'=>'Load Template'
        ,'load_design'=>'Load User\'s Design'
        ,'email_friend'=>'Email Friend'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot(),
        "cross"=>$v_is_cross_domain?1:0
    );
    if(isset($arr_switch[$v_sub_action]))
        $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_sub_action])?$arr_switch[$v_sub_action]:'Unknown', get_real_ip_address(), $arr_device);
}
switch($v_sub_action){
    case 'save_design':
        include 'qry_save_user_design.php';
        break;
    case 'save_as_design':
        include 'qry_save_as_user_design.php';
        break;
    case 'load_template':
        if($v_template_id>0)
            //include 'qry_load_user_template.php';
            include 'template/index.php';
        else
            include 'qry_load_user_empty_template.php';
        break;
    case 'load_design':
        include 'qry_load_user_design.php';
        break;
    case 'reload_design':
        include 'qry_reload_user_design.php';
        break;
    case 'email_friend':
        include 'qry_email_to_friend.php';
        break;
    default:
        $v_main_site_title = $cls_settings->get_option_name_by_key('website_attribute', 'website_main_title','AnvyDigital WorkTraq Website');
        $v_sub_site_title = 'Design Tool';
        include 'qry_product_design.php';
        include 'user_account/admin/design_header.php';
        include 'dsp_product_design.php';
        include 'user_account/admin/design_footer.php';
        break;
}