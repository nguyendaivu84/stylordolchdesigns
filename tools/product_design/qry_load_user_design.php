<?php
if(!isset($v_sval)) die();?>
<?php
$v_folding_type = "none";
$arr_data = array('success'=>0, 'error'=>1, 'message'=>'Design No# '.$v_design_id.' not found!');

add_class('cls_tb_design_design');
$cls_designs = new cls_tb_design_design($db, LOG_DIR);
//$v_row = $cls_designs->select_one(array('design_id'=>$v_design_id, 'design_status'=>0, 'user_id'=>$v_user_id));
$v_row = $cls_designs->select_one(array('design_id'=>$v_design_id, 'design_status'=>0));
if($v_row==1){
    $v_template_id = $cls_designs->get_template_id();
    $v_theme_id = $cls_designs->get_theme_id();
    $v_product_id = $cls_designs->get_product_id();
	$arr_product = $cls_designs->get_product();
    $v_design_data = $cls_designs->get_design_data();
    $v_image_data = $cls_designs->get_design_image();
    $v_design_name = $cls_designs->get_design_name();
    $v_user_id = $cls_designs->get_user_id();
    $v_markup_cost = $cls_designs->get_markup_cost();
    $v_stock_cost = $cls_designs->get_stock_cost();

    $v_width = $cls_designs->get_design_width();
    $v_height = $cls_designs->get_design_height();
    $v_bleed = $cls_designs->get_design_bleed();

    $v_folding = $cls_designs->get_design_folding();
    $v_folding_direction = $cls_designs->get_design_direction();
    $v_die_cut = $cls_designs->get_design_die_cut();

    $v_folding_type = $cls_settings->get_option_key_by_id('folding_type', $v_folding, 'none');
    $v_folding_direction = $cls_settings->get_option_key_by_id('folding_direction', $v_folding_direction, 'vertical');
    $v_die_cut = $cls_settings->get_option_key_by_id('die_cut_type', $v_die_cut, 'none');

    //add_class('cls_tb_product');
    //$cls_products = new cls_tb_product($db, LOG_DIR);
    //$v_product_row = $cls_products->select_one(array('product_id'=>$v_product_id));
	$v_product_title = isset($arr_product['title'])?$arr_product['title']:'Blank Design';
	$v_product_code = isset($arr_product['code'])?$arr_product['code']:'blank_design';


        $arr_data['width'] = $v_width;
        $arr_data['height'] = $v_height;
        $arr_data['folding'] = $v_folding_type;
        $arr_data['diecut'] = "none";
        $arr_data['title'] = $v_design_name;
        $arr_data['id'] = $v_template_id;
        $arr_data['theme_id'] = $v_theme_id;
        $arr_data['price_markup'] = $v_markup_cost;
        $arr_data['assign_to'] = $v_user_id;
        $arr_data['product_id'] = $v_product_id;
        $arr_data['product_title'] = $v_product_title;
        $arr_data['product_url_code'] = $v_product_code;
        $arr_data['stock_image_cost'] = $v_stock_cost;
        $arr_data['size_title'] = $v_width.'" &times; '.$v_height.'" '.$v_folding_type;
        $arr_data['set_id'] = $v_design_id;
        $arr_data['set_title'] = $v_design_name;
        $arr_data['status_id'] = $cls_designs->get_design_status();
        $arr_data['success'] = 1;
        $arr_data['images']= array();//
        $arr_data['json']= array();//
        $arr_data['fotoliaLicenses']= array();
        $arr_data['themes']= array();
        $arr_data['save_action']= 'design';//temporary

        $arr_images = json_decode($v_image_data, true);
        if(!is_array($arr_images)) $arr_images = array();
        $arr_json = json_decode($v_design_data, true);
        if(!is_array($arr_json)){
            $arr_tmp = array(
                'texts'=>array()
                ,'images'=>array()
                ,'width'=>($v_width + 2 * $v_bleed)
                ,'height'=>($v_height + 2 * $v_bleed)
                ,'bg_color'=> "FFFFFF"
                ,'bleed'=>	$v_bleed
                ,'droppable'=> true
                ,'name'=> "Front Side"
                ,"isViewed"=>false
            );
            $arr_canvases = array();
            if($v_folding<1)
                $arr_canvases = array($arr_tmp);
            else{
                $arr_canvases = array($arr_tmp, $arr_tmp);
                if($v_folding==2)
                    $arr_canvases[1]['name'] = 'Back Side';
                else if($v_folding==3){
                    $arr_canvases[1]['name'] = 'InSide';
                    $arr_canvases[0]['name'] = 'OutSide';
                }
            }
            //$arr_canvases['product'] = $v_product_row==1?$cls_product->get_keywords(): "blank_design";
            $arr_json = array(
                'version'=>'1.0',
                'canvases'=>$arr_canvases
                ,'width'=>$v_width
                ,'height'=>$v_height
                ,'dieCutType'=>"none"
                ,'folding'=>$v_folding_type
                ,'foldingDirection'=>"vertical"
                ,'product'=>$v_product_code// $v_product_row==1?$cls_products->get_product_sku(): "blank_design"
                ,'stockImages'=>array()
                ,'wrap_size'=>0
            );
        }else{
            if($v_folding<1){
                $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']: array();
                if(sizeof($arr_canvases)>1){
                    $arr_canvases = array(isset($arr_canvases[0])?$arr_canvases[0]:array());
                    $arr_json['canvases'] = $arr_canvases;
                }
            }

            $arr_json['product']= $v_product_code;// $v_product_row==1?$cls_products->get_product_sku(): "blank_design";
        }
        $arr_data['images'] = $arr_images;
        $arr_data['json'] = $arr_json;

}

//header("Content-type: application/json");
//echo json_encode($arr_data);
if($v_is_cross_domain)
    $cls_output->output($arr_data);
else
    $cls_output->output_jsonp($arr_data, 'jsonp_callback');