<?php
if(!isset($v_sval)) die();

$v_json = isset($_POST['json'])?$_POST['json']:'';
$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_theme_id = isset($_POST['theme_id'])?$_POST['theme_id']:'0';
$v_option = isset($_POST['option'])?$_POST['option']:'';

settype($v_template_id, 'int');
settype($v_theme_id, 'int');

$arr_option = array();
if(is_array($v_option))
    $arr_option = $v_option;
else{
    if($v_option!=''){
        if(get_magic_quotes_gpc()) $v_option = stripslashes($v_option);
        $arr_option = json_decode($v_option, true);
        if(!is_array($arr_option)) $arr_option = array();
    }
}

$v_width = isset($arr_option['width'])?$arr_option['width']:0;
$v_height = isset($arr_option['height'])?$arr_option['height']:0;


$v_success = 0;
$v_message = 'Error';

if($v_width>0 && $v_height>0){
    if(get_magic_quotes_gpc()) $v_json = stripslashes($v_json);
    $arr_json = json_decode($v_json, true);
    if(is_array($arr_json)){
        if(isset($arr_json['canvases'])){
            //$v_tmp = $v_width;
            //$v_width = $v_height;
            //$v_height = $v_tmp;
            $v_old_width = isset($arr_json['width'])?$arr_json['width']:0;
            $v_old_height = isset($arr_json['height'])?$arr_json['height']:0;
            $v_old_ratio = $v_old_width / $v_old_height;
            $v_ratio = $v_width / $v_height;
            $v_width_ratio = $v_width / $v_old_width;
            $v_height_ratio = $v_height / $v_old_height;

            $arr_json['width'] = $v_width;
            $arr_json['height'] = $v_height;
            for($i=0; $i<sizeof($arr_json['canvases']);$i++){
                $arr_json['canvases'][$i]['width'] = $v_width;
                $arr_json['canvases'][$i]['height'] = $v_height;

                $arr_texts = isset($arr_json['canvases'][$i]['texts'])?$arr_json['canvases'][$i]['texts']:array();
                for($j=0;$j<sizeof($arr_texts);$j++){
                    $v_left = isset($arr_texts[$j]['left'])?$arr_texts[$j]['left']:0;
                    $v_top = isset($arr_texts[$j]['top'])?$arr_texts[$j]['top']:0;
                    $v_point = isset($arr_texts[$j]['pointsize'])?$arr_texts[$j]['pointsize']:0;
                    $v_left *= $v_width_ratio;
                    $v_top *= $v_height_ratio;
                    $v_point = $v_width_ratio > $v_height_ratio?$v_point*$v_height_ratio:$v_width_ratio*$v_point;
                    $arr_texts[$j]['left'] = $v_left;
                    $arr_texts[$j]['top'] = $v_top;
                    $arr_texts[$j]['pointsize'] = $v_point;
                }
                $arr_json['canvases'][$i]['texts'] = $arr_texts;

                $arr_images = isset($arr_json['canvases'][$i]['images'])?$arr_json['canvases'][$i]['images']:array();
                for($j=0; $j<sizeof($arr_images);$j++){
                    $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                    $v_width = isset($arr_images[$j]['width'])?$arr_images[$j]['width']:0;
                    $v_height = isset($arr_images[$j]['height'])?$arr_images[$j]['height']:0;
                    $v_left = isset($arr_images[$j]['left'])?$arr_images[$j]['left']:0;
                    $v_top = isset($arr_images[$j]['top'])?$arr_images[$j]['top']:0;
                    $v_border_width = isset($arr_images[$j]['border_width'])?$arr_images[$j]['border_width']:0;
                    $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
                    $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
                    $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
                    $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;

                    $v_left *= $v_width_ratio;
                    $v_top *= $v_height_ratio;
                    $v_border_width *= min($v_width_ratio, $v_height_ratio);
                    if($v_image_id>0){
                        $v_width *= $v_width_ratio;
                        $v_height *= $v_width_ratio;
                    }else{
                        $v_height *= $v_height_ratio;
                        $v_width *= $v_width_ratio;
                    }

                    $arr_images[$j]['left'] = $v_left;
                    $arr_images[$j]['top'] = $v_top;
                    $arr_images[$j]['border_width'] = $v_border_width;
                    $arr_images[$j]['width'] = $v_width;
                    $arr_images[$j]['height'] = $v_height;
                }
                $arr_json['canvases'][$i]['images'] = $arr_images;
            }
            $v_success = 1;
            $v_message = 'OK';
        }
    }
}else{
    $arr_json = array();
}

$arr_data = array(
    'success'=>$v_success
    ,'message'=>$v_message
    ,'json'=>$arr_json
);
if($v_is_cross_domain)
    $cls_output->output($arr_data);
else
    $cls_output->output_jsonp($arr_data, 'jsonp_callback');