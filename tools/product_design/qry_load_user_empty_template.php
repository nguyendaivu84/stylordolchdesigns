<?php
if(!isset($v_sval)) die();?>
<?php
$v_option = isset($_GET['option'])?$_GET['option']:'';

//$_SESSION['ss_design_template'] = $v_template_id;
//$_SESSION['ss_design_theme'] = $v_theme_id;
//$_SESSION['ss_design_product'] = $v_product_id;

$v_folding_type = "none";

//add_class('cls_products');
//$cls_product = new cls_products($cn, '', _PREFIX_TBL, false);

$v_product_row = 0;
//if($v_product_id>0){
//    $v_product_row = $cls_product->select_one(" AND `id`='{$v_product_id}'");
//}

$v_product_code = isset($_GET['product_code'])?$_GET['product_code']:'blank_design';
$v_product_title = isset($_GET['product_title'])?$_GET['product_title']:'Blank Design';
$v_product_key = /*$v_product_code=='blank_design'?'blankDesign':*/$v_product_code;

$arr_data = array();

$arr_option = explode('-', $v_option);
$v_width = isset($arr_option[0])?$arr_option[0]:'10';
$v_height = isset($arr_option[1])?$arr_option[1]:'10';
settype($v_height, 'float');
settype($v_width, 'float');
if($v_width<=0) $v_width = 10;
if($v_height<=0) $v_height = 10;
$v_bleed = 0.125;
$arr_data['width'] = $v_width;
$arr_data['height'] = $v_height;
$arr_data['folding'] = $v_folding_type;
$arr_data['diecut'] = "none";
$arr_data['title'] = "Blank Design";
$arr_data['id'] = $v_template_id;
$arr_data['theme_id'] = 0;
$arr_data['price_markup'] = 0;
$arr_data['assign_to'] = 0;
$arr_data['product_id'] = isset($v_product_id)?$v_product_id:'0';
$arr_data['product_title'] = $v_product_title;
$arr_data['product_url_code'] = $v_product_code;
$arr_data['stock_image_code'] = 0;
$arr_data['size_title'] = $v_width.'" &times; '.$v_height.'" ';
$arr_data['set_id'] = $v_template_id;
$arr_data['set_title'] = "Blank Design";
$arr_data['status_id'] = 4;
$arr_data['success'] = 1;
$arr_data['images']= array();//
$arr_data['json']= array();//
$arr_data['fotoliaLicenses']= array();
$arr_data['themes']= array();

//$v_where_clause = " AND `template_id`='{$v_template_id}' AND `theme_id`='{$v_theme_id}'";
//add_class('cls_design_themes');
//$cls_themes = new cls_design_themes($cn, '', _PREFIX_TBL, false);
//$v_row = $cls_themes->select_one($v_where_clause);

$arr_images = array();
$arr_json = array();
$v_is_template = 0;
$v_current_text_color = '';
$v_current_theme_name = '';
$v_current_theme_color = '';

$v_theme_data = '';
$v_theme_image = '';

$arr_images = array();

if($v_theme_data!='') $arr_json = json_decode($v_theme_data, true);
if($v_theme_data=='' || !is_array($arr_json)){
    $arr_tmp = array(
        'texts'=>array()
        ,'images'=>array()
        ,'width'=>($v_width + 2 * $v_bleed)
        ,'height'=>($v_height + 2 * $v_bleed)
        ,'bg_color'=> "FFFFFF"
        ,'bleed'=>	$v_bleed
        ,'droppable'=> true
        ,'name'=> "Front Side"
        ,"isViewed"=>false
    );
    $arr_canvases = array();
    $arr_canvases = array($arr_tmp);

    $arr_json = array(
        'version'=>'2',
        'canvases'=>$arr_canvases

        ,'width'=>$v_width
        ,'height'=>$v_height
        ,'dieCutType'=>"none"
        ,'folding'=>$v_folding_type
        ,'foldingDirection'=>"vertical"
        ,'product'=>$v_product_key
        ,'stockImages'=>array()
        ,'wrap_size'=>0
    );

}else{
    $arr_json['product'] = $v_product_key;
}

$arr_json['folding'] = $v_folding_type;
$arr_data['images'] = $arr_images;
$arr_data['json'] = $arr_json;

if($v_is_cross_domain)
    $cls_output->output($arr_data, true, true);
else{
    $cls_output->output_jsonp($arr_data, 'jsonp_callback');
}
//header("Content-type: application/json");
//echo json_encode($arr_data);