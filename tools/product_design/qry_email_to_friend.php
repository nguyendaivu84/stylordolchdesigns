<?php
$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';
$v_sender_name = isset($_POST['sender_name'])?$_POST['sender_name']:'';
$v_receiver_email = isset($_POST['receiver_email'])?$_POST['receiver_email']:'';
$v_message = isset($_POST['message'])?$_POST['message']:'';
$v_share_link = isset($_POST['share_link'])?$_POST['share_link']:'Broken';
settype($v_design_id,'int');

$v_receiver_email = trim($v_receiver_email);
$v_receiver_email = strtolower($v_receiver_email);

$v_success = -1;
$v_error = 'Lost data!';
if(!is_valid_email($v_receiver_email)) $v_error = 'Invalid receiver email!';
if($v_design_id>0 && is_valid_email($v_receiver_email) && is_valid_url($v_share_link)){

    if($v_sender_name=='') $v_sender_name = 'Unnamed';

    $v_user_id = isset($arr_user['user_id'])?$arr_user['user_id']:'0';
    $v_contact_id = isset($arr_user['contact_id'])?$arr_user['contact_id']:'0';
    settype($v_user_id, 'int');
    settype($v_contact_id, 'int');

    add_class('cls_tb_email_templates');
    add_class('cls_tb_design_design');

    $cls_tb_email = new cls_tb_email_templates($db, LOG_DIR);
    $cls_designs = new cls_tb_design_design($db, LOG_DIR);

    $arr_design_share = $cls_designs->select_scalar('design_share', array('design_id'=>$v_design_id));
    if(!is_array($arr_design_share)) $arr_design_share = array();


    $v_full_name = $v_member_first_name.' '.$v_member_last_name;
    $v_email = $v_member_email;
    if($v_full_name=='') $v_full_name = isset($arr_user['user_name'])?$arr_user['user_name']:'';
    if($v_email!='') $v_full_name.=' ('.$v_email.')';

    $v_row = $cls_tb_email->select_one(array('email_key'=>'tpl_email_design_to_friend'));
    $v_support_email = $cls_settings->get_option_name_by_key('email','support_email', 'info@anvydigital.com');
    $v_mail_from = $cls_settings->get_option_name_by_key('email','email_orgin');
    //$v_share_link = URL.'tools.php?action=product_design&design_id='.$v_design_id;
    if($v_row==1){
        $v_template = $cls_tb_email->get_email_file();
        require ROOT_DIR.'/classes/xtemplate.class.php';
        $v_dir_templates = ROOT_DIR.DS.'mail';
        $tpl_template = new Template($v_template, $v_dir_templates);
        $tpl_template->set('URL', URL);
        $tpl_template->set('RECEIVER', $v_sender_name);
        $tpl_template->set('USER_NAME', $v_full_name);
        $tpl_template->set('SHARE_LINK', $v_share_link);
        $tpl_template->set('SUPPORT_EMAIL', $v_support_email);
        $tpl_template->set('EXTRA_MESSAGE', $v_message);
        $v_body = $tpl_template->output();
    }else{
        $v_body = '<p style="text-align:justify">'.$v_message.'</p>';
        $v_body .= '<p style="color: indianred">This email is sent to you from ANVYDIGITAL Design Online by "'.$v_sender_name.'".</p>';
        $v_body .= '<p style="color: indianred; font-weight: bold">Note: Do not reply this email</p>';
        $v_body .= '<p>Link to view design: '.$v_share_link;
    }

    //if(mysql_affected_rows()>0){
    require ROOT_DIR.'/classes/class.phpmailer.php';

        $mail = new PHPMailer();
        $mail->IsSendmail();
        $mail->SetFrom($v_support_email, 'Design Online '.URL);
        $mail->AddAddress($v_receiver_email);
        $mail->Subject = 'View Design Online of your friend';
        $mail->IsHTML();
        $mail->Body = $v_body;
        if($mail->Send()){
            $v_error = 'Successful send email!';
            $v_success = 1;
        }else{
            $v_error = 'Can\'t send mail!';
            $v_success = 0;
        }
    //}else $v_error = 'Cannot access data!';

    $arr_design_share[] = array(
        'from'=>isset($arr_user['user_name'])?$arr_user['user_name']:''
        ,'to'=>$v_sender_name
        ,'email'=>$v_receiver_email
        ,'time'=>date('Y-m-d H:i:s')
        ,'message'=>$v_message
        ,'success'=>$v_success
    );

    $cls_designs->update_field('design_share', $arr_design_share, array('design_id'=>$v_design_id));
}
$arr_return = array('success'=>$v_success, 'message'=>$v_error);
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
//echo json_encode(array('success'=>$v_success, 'message'=>$v_error));