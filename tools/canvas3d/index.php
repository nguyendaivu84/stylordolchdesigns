<?php
    $v_image = isset($_REQUEST['img'])?$_REQUEST['img']:'';

    $inch_width = isset($_REQUEST['w'])?(float)$_REQUEST['w']:1; //inch
    $inch_height = isset($_REQUEST['h'])?(float)$_REQUEST['h']:1; //inch
    $inch_bleed = isset($_REQUEST['b'])?(float)$_REQUEST['b']:0.15; //inch
    $v_edge = isset($_REQUEST['edge'])?$_REQUEST['edge']:'natural';

    $bw = $inch_bleed/$inch_width;
    $bh = $inch_bleed/$inch_height;

    $folder = 'tools/canvas3d/';

    if($v_image!=''){

        $widthdepth = 10;

        $link_temp_doc = $_SERVER['DOCUMENT_ROOT'].'/upload/assets/designs/temps/';
        $link_temp = 'upload/assets/designs/temps/';
        $link_img = $link_temp_doc.$v_image.'.jpg';

        list($width, $height) = getimagesize($link_img);

        $image = new Imagick($link_img);
        $image->cropImage(($width - $width* $bw * 2),($height - $height*$bh *2), ($width* $bw), ($height * $bh));
        $image->writeImage($link_temp_doc.'view3d-center.jpg');


        if($v_edge=='natural'){
            $image = new Imagick($link_img);
            $image->cropImage(($width - $width * $bw * 2),($height * $bh), ($width * $bw), 0);
            $image->writeImage($link_temp_doc.'view3d-top.jpg');
        
            $image = new Imagick($link_img);
            $image->cropImage(($width * $bw),($height - $height * $bh * 2), 0, ($height * $bh));
            $image->writeImage($link_temp_doc.'view3d-left.jpg');
        
            $image = new Imagick($link_img);
            $image->cropImage(($width * $bw),($height - $height * $bh * 2), ($width - $width* $bw), ($height * $bh));
            $image->writeImage($link_temp_doc.'view3d-right.jpg');
        
            $image = new Imagick($link_img);
            $image->cropImage(($width - $width* $bw * 2),($height * $bh), ($width* $bw), ($height - $height*$bh ));
            $image->writeImage($link_temp_doc.'view3d-bottom.jpg');

        }else{
            if($v_edge=='black')
                $color = '#000';
            elseif($v_edge=='m_wrap')
                $color = '#000';
            elseif($v_edge=='red')
                $color = '#8f4027';
            else
                $color = '#fff';
            $image = new Imagick();
            $image->newImage(($width - $width * $bw * 2), ($height * $bh), $color);
            $image->writeImage($link_temp_doc.'view3d-top.jpg');
            
            $image->newImage(($width * $bw),($height - $height * $bh * 2), $color);
            $image->writeImage($link_temp_doc.'view3d-left.jpg');

            $image->newImage(($width * $bw),($height - $height * $bh * 2), $color);
            $image->writeImage($link_temp_doc.'view3d-right.jpg');

            $image->newImage(($width - $width* $bw * 2),($height * $bh), $color);
            $image->writeImage($link_temp_doc.'view3d-bottom.jpg');

        }
    
    
        
    
        list($width3d, $height3d) = getimagesize($link_temp_doc.'view3d-center.jpg');
        $ratio = $width3d/$height3d;
        $ratio_depth = $width3d/$widthdepth;
        while($width3d > 12)
            $width3d = round($width3d / 2);
        list($widthdepth, $heightdepth) = getimagesize($link_temp_doc.'view3d-left.jpg');
        $height3d= round($width3d / $ratio);
        $widthdepth= round($width3d / $ratio_depth);
        if($widthdepth < 0.5)
            $widthdepth = 0.5;
    ?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
        <style>
            body {
                font-family: Monospace;
                background-color: #f0f0f0;
                margin: 0px;
                overflow: hidden;
            }
        </style>
        <script type="text/javascript" src="<?php echo $folder;?>three.min.js"></script>
        <script type="text/javascript" src="<?php echo $folder;?>requestAnimFrame.js"></script>
        <script type="text/javascript" src="<?php echo $folder;?>OrbitControls.js"></script>
        <script type="text/javascript" src="<?php echo $folder;?>Detector.js"></script>
        <!--make sure this is last-->
        <style>
            body { margin: 0; padding: 0; overflow: hidden; }

        </style>
    </head>
    <body>
        <div id="container"></div>
        <script type="text/javascript">
            if ( ! Detector.webgl ) Detector.addGetWebGLMessage();
            var container;
            var camera, controls, scene, renderer;
            init();
            (function(){
              var waitToLoaded = setInterval(function() {
                if (document.readyState == "complete"){
                    render();
                    clearInterval(waitToLoaded);
                }
              }, 50);
            })();

            function animate() {
                requestAnimationFrame(animate);
                controls.update();
            }

            function init() {
                camera = new THREE.PerspectiveCamera( 20, window.innerWidth / window.innerHeight, 1, 1000 );
                camera.position.z = 50;
                controls = new THREE.OrbitControls( camera );
                controls.damping = 0.2;
                controls.addEventListener( 'change', render );

                scene = new THREE.Scene();
                scene.fog = new THREE.Fog( 0x808080, 0.002 );

                // world
                var left    = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture( '<?php echo $link_temp;?>view3d-left.jpg'  )} );
                var right   = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture( '<?php echo $link_temp;?>view3d-right.jpg' ) } );
                var top     = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture( '<?php echo $link_temp;?>view3d-top.jpg'   )} );
                var bottom  = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture( '<?php echo $link_temp;?>view3d-bottom.jpg' )  } );
                var center  = new THREE.MeshLambertMaterial( { map: THREE.ImageUtils.loadTexture( '<?php echo $link_temp;?>view3d-center.jpg' ) } );
                var back    = new THREE.MeshLambertMaterial( { color: 0x333333 } );
                var materials = [
                    right,      // Right side
                    left,       // Left side
                    top,        // Top side
                    bottom,     // Bottom side
                    center,     // Center side
                    back,       // Back side
                  ];

                var Pic3D =  new THREE.Mesh( new THREE.BoxGeometry( <?php echo $width3d; ?>, <?php echo $height3d; ?>, <?php echo $widthdepth; ?>) ,  new THREE.MeshFaceMaterial( materials ));
                scene.add( Pic3D );

                // lights
                light = new THREE.PointLight(0xffffff, .4 );
                light.position.set( 50, 50, 50 );
                scene.add( light );

                ambientLight = new THREE.AmbientLight( 0xbbbbbb  );
                scene.add( ambientLight );

                // renderer


                renderer = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
                renderer.setClearColor( scene.fog.color, 0 );
                renderer.setSize( window.innerWidth-4, window.innerHeight-4 );

                container = document.getElementById( 'container' );
                container.appendChild( renderer.domElement );

                //

                window.addEventListener( 'resize', onWindowResize, false );

                controls.addEventListener('change', render);
                animate();

            }

            function onWindowResize() {

                camera.aspect = window.innerWidth / window.innerHeight;
                camera.updateProjectionMatrix();

                renderer.setSize( window.innerWidth-4, window.innerHeight-4 );

                render();

            }

            function render() {
                renderer.render( scene, camera );

            }

        </script>
        <!-- <div id="container"></div> -->
    </body>
</html>
<?php
    } else {
    ?>
    Can not find your images.
<?php } ?>
