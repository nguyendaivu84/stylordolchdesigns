<?php
if(!isset($v_sval)) die();?>
<?php
$v_base_url = URL;
$v_action_type = isset($_GET['action_type'])?$_GET['action_type']:'';

if($v_site_id>0){
    $arr_switch = array(
        'image_template'=>'Load Template\'s Images'
        ,'image_design'=>'Load Design\'s Images'
        ,'image_thumb'=>'Get Image\'s Thumb'
        ,'image_info'=>'Get Image\'s Info'
        ,'image_upload'=>'Upload Image'
        ,'image_delete'=>'Delete Image'
        ,'get_fotolia'=>'Get Search Fotolia'
        ,'use_fotolia'=>'Download Fotolia'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot(),
        "cross"=>$v_is_cross_domain?1:0
    );
    if(isset($arr_switch[$v_action_type]))
        $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_action_type])?$arr_switch[$v_action_type]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_action_type){
    case 'image_template':// for admin
        if(isset($_SESSION['ss_design_side']) && ($_SESSION['ss_design_side']==DESIGN_SIDE_FACE))
            include 'qry_image_template.php';
        else
            include 'qry_image_design.php';
        break;
    case 'image_design':// for user
        include 'qry_image_design.php';
        break;
    case 'image_thumb':
        include 'qry_image_thumb.php';
        break;
    case 'image_info':
        include 'qry_image_info.php';
        break;
    case 'image_upload':
        include 'qry_upload_image.php';
        break;
    case 'get_image_upload':
        include 'qry_get_upload_image.php';
        break;
    case 'image_delete':
        include 'qry_image_delete.php';
        break;
    case 'get_fotolia':
        include 'qry_get_fotolia.php';
        break;
    case 'use_fotolia':
        include 'qry_fotolia_comp.php';
        break;
    default:
        include 'dsp_image_display.php';
        break;
}
?>