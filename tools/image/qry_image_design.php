<?php
if(!isset($v_sval)) die();?>
<?php
$v_template_id = isset($_SESSION['ss_design_template'])?$_SESSION['ss_design_template']:'0';
$v_theme_id = isset($_SESSION['ss_design_theme'])?$_SESSION['ss_design_theme']:'0';
$v_design_id = isset($_SESSION['ss_design_design'])?$_SESSION['ss_design_design']:'0';

$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
$v_limit = isset($_REQUEST['limit'])?$_REQUEST['limit']:20;

$v_member_id = isset($_REQUEST['txt_member_id'])?$_REQUEST['txt_member_id']:0;
$v_member_name = isset($_REQUEST['txt_member_name'])?$_REQUEST['txt_member_name']:'Unknown';
settype($v_member_id, 'int');

settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');
settype($v_page, 'int');
settype($v_limit, 'int');

add_class('cls_tb_design_image');
$cls_images = new cls_tb_design_image($db, LOG_DIR);

$arr_info = array();
if($v_site_id>0){
    if($v_member_id>0)
        $arr_where_clause = array('image_status'=>0, 'site_id'=>$v_site_id, 'user_id'=>$v_member_id);
    else{
        $arr_where_clause = array('image_status'=>0, 'site_id'=>$v_site_id, 'user_id'=>0, 'user_ip'=>$v_user_ip, 'user_agent'=>$v_user_agent);
    }
}else
    $arr_where_clause = array('$or'=>array(array('company_id'=>$v_user_company_id), array('user_id'=>$v_user_id)), 'image_status'=>0);

$v_offset = ($v_page - 1)*$v_limit;
$arr_image = $cls_images->select_limit($v_offset, $v_limit, $arr_where_clause, array('image_id'=>-1));

foreach($arr_image as $arr){
    $v_image_id = $arr['image_id'];
    $v_image_width = $arr['image_width'];
    $v_image_height = $arr['image_height'];
    $v_image_name = $arr['image_name'];
    $v_image_key = $arr['image_key'];
    $v_image_file = $arr['image_file'];
    $v_image_extension = $arr['image_extension'];
    $v_image_type = $arr['image_type'];
    $v_image_dpi = $arr['image_dpi'];
    $v_fotolia_id = $arr['fotolia_id'];
    $v_saved_dir = $arr['saved_dir'];
    $v_svg_id = $arr['svg_id'];
    $v_svg_original_colors = $arr['svg_original_colors'];
    $v_is_public = $arr['is_public'];
    $v_is_vector = $arr['is_vector'];

    if(!file_exists($v_root_dir.$v_saved_dir.$v_image_file)) continue;
    $arr_info[] = array(
        "dpi"=>$v_image_dpi
        ,"extension"=>$v_image_extension
        ,"fotolia_id"=>$v_fotolia_id
        ,"height"=>$v_image_height
        ,"width"=>$v_image_width
        ,"id"=>$v_image_id
        ,"is_public"=>$v_is_public
        ,"is_vector"=>$v_is_vector
        ,"name"=>$v_image_name
        ,"svg_id"=>$v_svg_id
        ,"svg_original_colors"=>$v_svg_original_colors
    );

}

$arr_return = array(
    'rows'=>$arr_info
    ,'success'=>1
);

if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');