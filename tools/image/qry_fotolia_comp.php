<?php
$v_member_id = isset($_REQUEST['txt_member_id'])?$_REQUEST['txt_member_id']:0;
$v_member_name = isset($_REQUEST['txt_member_name'])?$_REQUEST['txt_member_name']:'Unknown';
settype($v_member_id, 'int');

require_once ROOT_DIR.'/api/fotolia/fotolia-api.php';
$v_stock_vendor_name = 'FOTOLIA';

$api = new Fotolia_Api(DESIGN_FOTOLIA_KEY);
$v_id = isset($_GET['fotolia_id'])?$_GET['fotolia_id']:null;

if($v_site_id>0)
    $v_assign_to = $v_member_id;
else
    $v_assign_to = isset($arr_user['user_id'])?$arr_user['user_id']:0;

//{"success" : 1 , "uid" : 1456339 , "name": "Stock Image #49509474 " , "width" : 400 , "height" : 123 , "fotoliaId" : 49509474 , "licenses" : [{"name":"XS","price":1,"width":624,"height":192},{"name":"S","price":3,"width":1249,"height":384},{"name":"M","price":5,"width":2485,"height":765},{"name":"L","price":7,"width":3467,"height":1067},{"name":"XL","price":8,"width":5034,"height":1549},{"name":"XXL","price":10,"width":6981,"height":2149}] }
$arr_ext = array('', 'gif', 'jpg', 'png');
if(!empty($v_id) && !is_null($v_id)){
    add_class('cls_tb_design_image');
    add_class('cls_tb_design_stock');
    $cls_images = new cls_tb_design_image($db, LOG_DIR);
    $cls_stocks = new cls_tb_design_stock($db, LOG_DIR);

    $v_file_name = $v_id.'.jpg';
    $arr_result = $api->getMediaComp($v_id);
    $v_save_file = DESIGN_FOTOLIA_DIR;
    $v_save_file .= DS.$v_file_name;
    $api->downloadMediaComp($arr_result['url'], $v_save_file);
    list($width, $height, $type) = getimagesize($v_save_file);
    $v_size = filesize($v_save_file);
    $v_dpi = ceil($width*$height / pow(96,2));

    $v_saved_dir = str_replace($v_root_dir,'', DESIGN_FOTOLIA_DIR);
    $v_saved_dir = str_replace('\\','/', $v_saved_dir).'/';

    $arr_licenses_results = $api->getMediaData($v_id, 400);
    $arr_licenses_details = array();
    $v_max_price = 0;
    $v_max_license = '';
    foreach ($arr_licenses_results['licenses'] as $licenseInfo) {
        $arr_license_details = $arr_licenses_results['licenses_details'][$licenseInfo['name']];
        if($licenseInfo['price']>$v_max_price){
            $v_max_price = $licenseInfo['price'];
            $v_max_license = $licenseInfo['name'];
        }
        array_push($arr_licenses_details, array("name" => $licenseInfo['name'], "price" => $licenseInfo['price'], "dpi"=>$arr_license_details['width'], "ratio"=>$arr_license_details['ratio'],  "dimensions"=>$arr_license_details['dimensions'], "width" => $arr_license_details['width'], "height" => $arr_license_details['height'], "update_on"=>date('Y-m-d H:i:s', time())));
    }
    $v_image_title = isset($arr_licenses_results['title'])?$arr_licenses_results['title']:'';
    //$v_assigned_to = isset($arr_user['user_id'])?$arr_user['user_id']:0;
    settype($v_assigned_to, 'int');
    $v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
    if($v_is_admin){
        $v_is_admin = 1;
    }else{
        $v_is_admin =0;
    }

    //$v_license_info = json_encode($arr_license_details);
    //if(!get_magic_quotes_gpc()) $v_license_info = mysql_real_escape_string($v_license_info);
    $v_id = $v_id."";
    $arr_where_clause = array('stock_vendor_name'=>$v_stock_vendor_name, 'vendor_stock_id'=>$v_id);
    $v_row = $cls_stocks->select_one($arr_where_clause);
    $v_image_id = 0;
    if($v_row==1){
        $v_mongo_id = $cls_stocks->get_mongo_id();
        $v_stock_id = $cls_stocks->get_stock_id();
        $arr_fields = array('licenses', 'max_license');
        $arr_values = array($arr_licenses_details, $v_max_license);

        $cls_stocks->update_field($arr_fields, $arr_values, array('_id'=>$v_mongo_id));
        $v_image_id = $cls_images->select_scalar(array('image_stock_id'=>$v_stock_id));
    }else{
        //$v_stock_id = $cls_stocks->select_next('stock_id');
        //$cls_stocks->set_stock_id($v_stock_id);
        $cls_stocks->set_user_name(isset($arr_user['user_name'])?$arr_user['user_name']:'');
        $cls_stocks->set_company_id($v_user_company_id);
        $cls_stocks->set_company_id($v_user_location_id);
        $cls_stocks->set_admin_side($v_is_admin);
        $cls_stocks->set_vendor_stock_id($v_id);
        $cls_stocks->set_stock_vendor_name($v_stock_vendor_name);
        $cls_stocks->set_image_title($v_image_title);
        $cls_stocks->set_licenses($arr_licenses_details);
        $cls_stocks->set_max_license($v_max_license);
        $cls_stocks->set_downloaded_time(date('Y-m-d H:i:s'));
        $cls_stocks->set_user_id($v_assign_to);
        $cls_stocks->set_site_id($v_site_id);
        $cls_stocks->set_user_ip($v_user_ip);
        $cls_stocks->set_user_agent($v_user_agent);

        $v_stock_id = $cls_stocks->insert();
        if($v_stock_id >0){
            //$v_image_id = $cls_images->select_next('image_id');

            //$cls_images->set_image_id($v_image_id);
            $cls_images->set_user_id($v_assign_to);
            $cls_images->set_fotolia_id($v_id);
            $cls_images->set_is_admin($v_is_admin);
            $cls_images->set_design_id(0);
            $cls_images->set_site_id($v_site_id);
            $cls_images->set_fotolia_size($v_max_license);
            $cls_images->set_image_dpi($v_dpi);
            $cls_images->set_image_extension($arr_ext[$type]);
            $cls_images->set_image_file($v_file_name);
            $cls_images->set_image_height($height);
            $cls_images->set_image_width($width);
            $cls_images->set_image_key($v_id);
            $cls_images->set_image_name($v_id);
            $cls_images->set_image_cost((float) $v_max_price);
            $cls_images->set_image_status(0);
            $cls_images->set_image_size($v_size);
            $cls_images->set_image_stock_id($v_stock_id);
            $cls_images->set_is_vector(0);
            $cls_images->set_is_public(1);
            $cls_images->set_saved_dir($v_saved_dir);
            $cls_images->set_svg_id(0);
            $cls_images->set_svg_original_colors('');
            $cls_images->set_image_type((int) $type);
            $cls_images->set_user_ip($v_user_ip);
            $cls_images->set_user_agent($v_user_agent);
            $cls_images->set_company_id($v_user_company_id);
            $cls_images->set_location_id($v_user_location_id);
            $cls_images->set_created_time(date('Y-m-d H:i:s'));
            $cls_images->set_user_name(isset($arr_user['user_name'])?$arr_user['user_name']:'');
            $v_image_id = $cls_images->insert();
            //if(!$v_result_id) $v_image_id = 0;
        }

    }
    if($v_image_id>0)
        $arr_return = array('success'=>1,'id'=>$v_image_id,'width'=> $width,'height'=>$height,'name'=>'Stock Image #'.$v_id,'fotolia_id'=>$v_id,'licenses' => $arr_licenses_details , 'is_vector'=>0, 'svg_id'=>null, 'is_public'=>0, 'dpi'=>$v_dpi, 'svg_original_colors'=>null, 'extension'=>$arr_ext[$type]);
    else
        $arr_return = array('success'=>0);

}else{
    $arr_return = array('success'=>0);
}
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
?>