<?php
$v_sval = 1;

$v_sid = isset($_GET['sid'])?$_GET['sid']:'';
$v_image_id = isset($_GET['image_id'])?$_GET['image_id']:'0';
settype($v_image_id, 'int');
$v_member_id = isset($_REQUEST['txt_member_id'])?$_REQUEST['txt_member_id']:0;
$v_member_name = isset($_REQUEST['txt_member_name'])?$_REQUEST['txt_member_name']:'Unknown';
settype($v_member_id, 'int');

add_class('cls_tb_design_image');
$cls_images = new cls_tb_design_image($db, LOG_DIR);

if($v_site_id>0){
    $v_assign_to = $v_member_id;
    $arr_where_clause = array('image_id'=>$v_image_id, 'image_status'=>0, 'site_id'=>$v_site_id);
}else{
    $v_assign_to = $v_user_id;
    $arr_where_clause = array('image_id'=>$v_image_id, 'image_status'=>0);
}

$v_row = $cls_images->select_one($arr_where_clause);
$v_success = 0;
$v_width = 0;
$v_height = 0;
$v_is_public = 0;
$v_is_vector = 0;
$v_extension = 'jpg';
$v_fotolia_id = 0;
$v_svg_id = 0;
$v_svg_original_color = '';
$v_image_name = '';
if($v_row==1){
    $v_image_dir = $v_root_dir.DS.$cls_images->get_saved_dir();
    $v_image_file = $cls_images->get_image_file();
    $v_width = $cls_images->get_image_width();
    $v_height = $cls_images->get_image_height();
    $v_extension = $cls_images->get_image_extension();
    $v_image_name = $cls_images->get_image_file();
    $v_svg_id = $cls_images->get_svg_id();
    $v_fotolia_id = $cls_images->get_fotolia_id();
    $v_image_user_id = $cls_images->get_user_id();


    $v_allow = ($v_image_user_id == $v_assign_to) || is_admin();


    if($v_allow){
        if(file_exists($v_image_dir.$v_image_file)){
            unlink($v_image_dir.$v_image_file);
        }
        $v_thumb_file = $v_image_dir.DESIGN_IMAGE_THUMB_SIZE.'_'.$v_image_file;
        $v_original_file = $v_image_dir.IMAGE_ORIGINAL_PREFIX.'_'.$v_image_file;
        if(file_exists($v_thumb_file)) @unlink($v_thumb_file);
        if(file_exists($v_original_file)) @unlink($v_original_file);

        $cls_images->delete(array('image_id'=>$v_image_id));
    }
}

$arr_return = array(
    "dpi"=>0
    ,"extension"=>$v_extension
    ,"fotolia_id"=>$v_fotolia_id!=0?:null
    ,"height"=>$v_width
    ,"width"=>$v_height
    ,"id"=>$v_image_id
    ,"is_public"=>$v_is_public
    ,"is_vector"=>$v_is_vector
    ,"name"=>$v_image_name
    ,"success"=>$v_success
    ,"svg_id"=>$v_svg_id!=0?:null
    ,"svg_original_colors"=>$v_svg_original_color!=''?:null
);

if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');