<?php
if(!isset($v_sval)) die();
$v_random = isset($_REQUEST['random'])?$_REQUEST['random']:'';
$v_random = seo_friendly_url($v_random);
$v_save_content = '';
if($v_random!=''){
    $v_save_file = DESIGN_TEMP_DIR.DS.$v_random.'.txt';
    add_class('cls_file');
    $mf = new ManageFile(DESIGN_TEMP_DIR, DS);
    if($mf->read_file($v_random.'.txt', DESIGN_TEMP_DIR)==1){
        $v_save_content = $mf->get_file_content();
        $mf->remove_file($v_random.'.txt', DESIGN_TEMP_DIR);
    }
}
if($v_save_content!=''){
    $arr_return = json_decode($v_save_content, true);
    if(!isset($arr_return)) $arr_return = array('error'=>1);
    if(!isset($arr_return['error']))
        $arr_return['error'] = 1;
    else
        $arr_return['error'] = intval($arr_return['error']);
}else{
    $arr_return = array('error'=>1);
}
$cls_output->output_jsonp($arr_return, 'jsonp_callback');
