<?php
if(!isset($v_sval)) die();
$v_template_id = isset($_SESSION['ss_design_template'])?$_SESSION['ss_design_template']:'0';
$v_theme_id = isset($_SESSION['ss_design_theme'])?$_SESSION['ss_design_theme']:'0';
$v_design_id = isset($_SESSION['ss_design_design'])?$_SESSION['ss_design_design']:'0';
settype($v_template_id, 'int');
settype($v_template_id, 'int');
settype($v_design_id, 'int');

$v_random = isset($_REQUEST['random'])?$_REQUEST['random']:'';
add_class('cls_file');
add_class('cls_upload');
add_class('cls_validation');
add_class('cls_tb_design_image');
$mf = new ManageFile(DESIGN_TEMP_DIR, DS);
$cls_images = new cls_tb_design_image($db, LOG_DIR);
$v_upload_dir = DESIGN_TEMPLATE_DIR;

$arr_return = array('message'=>'Error', 'success'=>0, 'error'=>1);
$validate = new cls_validation();
$validate->set_file_extension(array('png', 'jpg', 'gif'));
$validate->set_max_file_size(DESIGN_MAX_SIZE_UPLOAD);
$validate->set_file_name_length(50);
$v_assign_to = $v_user_id;
$v_admin = $v_user_company_id == 10000?1:0 && is_admin();

$v_member_id = isset($_REQUEST['txt_member_id'])?$_REQUEST['txt_member_id']:0;
$v_member_name = isset($_REQUEST['txt_member_name'])?$_REQUEST['txt_member_name']:'Unknown';
settype($v_member_id, 'int');
if($v_admin==0){
    if($v_site_id>0){
        $v_assign_to = $v_member_id;
        $v_user_name = $v_member_name;
    }
    $v_upload_dir = DESIGN_USER_DIR;
    if($v_design_id>0){
        if(file_exists($v_upload_dir.DS.$v_design_id) || @mkdir($v_upload_dir.DS.$v_design_id)){
            if(is_dir($v_upload_dir.DS.$v_design_id) && is_writable($v_upload_dir.DS.$v_design_id))
                $v_upload_dir .= DS.$v_design_id;
        }
    }
}else{
    if($v_template_id>0){
        if(file_exists($v_upload_dir.DS.$v_template_id) || @mkdir($v_upload_dir.DS.$v_template_id)){
            if(is_dir($v_upload_dir.DS.$v_template_id) && is_writable($v_upload_dir.DS.$v_template_id))
                $v_upload_dir .= DS.$v_template_id;
        }
    }
}

$up = cls_upload::factory($v_upload_dir);
$up->set_max_file_size(DESIGN_MAX_SIZE_UPLOAD);
if(!empty($_FILES['Filedata'])){
    $up->file($_FILES['Filedata']);
    //$up->callbacks($validate, array('check_file_extension', 'check_max_file_size', 'check_file_name_length'));
    $arr_result = $up->upload();
    $arr_ext = array('', 'gif', 'jpg', 'png', 'pdf');
    if($arr_result['status']){
        $v_current_full_path = $up->get_result_path();
        $v_image_file = $up->get_current_name();
        $v_image_original_file = $v_image_file;
        if(mime_content_type($v_current_full_path)=='application/pdf'){
            $arr_info = $cls_draw->convert_pdf2png($v_current_full_path);
            if($arr_info['name']!=''){
                $v_current_full_path = $arr_info['file'];
                $v_image_file = $arr_info['name'];
            }
        }

        list($width, $height, $type) = getimagesize($v_current_full_path);
        if($type>3) $type = 0;
        if($type>0){
            $v_extension = '.'.$arr_ext[$type];
            $v_extension_len = strlen($v_extension);
            $v_full_path_len = strlen($v_current_full_path)-$v_extension_len;

            if(strrpos($v_current_full_path, $v_extension)!==$v_full_path_len){
                $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).$v_extension;
                $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).$v_extension;
                $v_count = 0;
                $v_is_exist = file_exists($v_new_full_path);
                while($v_is_exist){
                    $v_count++;
                    $v_new_full_path = substr($v_current_full_path, 0, $v_full_path_len).'('.$v_count.')'.$v_extension;
                    $v_new_image_file = substr($v_image_file, 0, strlen($v_image_file)-$v_extension_len).'('.$v_count.')'.$v_extension;
                    $v_is_exist = file_exists($v_new_full_path);
                }
                $v_image_original_file = $v_new_image_file;
                @rename($v_current_full_path, $v_new_full_path);

                $v_current_full_path = $v_new_full_path;
                $v_image_file = $v_new_image_file;
            }

            $v_image_original_file = $cls_images->backup_original_image($v_current_full_path, $v_image_file, $arr_ext[$type], IMAGE_MAX_SIZE_USE, array('w'=>IMAGE_MAX_WIDTH_USE, 'h'=>IMAGE_MAX_HEIGHT_USE), IMAGE_ORIGINAL_PREFIX, DS);

            $v_dir = str_replace($v_root_dir,'', $v_current_full_path);

            $v_saved_dir = str_replace('\\', '/', $v_dir);
            //$v_saved_dir = substr($v_saved_dir, 1);
            $v_saved_dir = str_replace($v_image_file,'', $v_saved_dir);

            $v_url = URL. str_replace('\\', '/', $v_dir);

            $v_size = (int) (isset($arr_result['size_in_bytes'])?$arr_result['size_in_bytes']:'0');

            $v_dpi = ceil($width*$height / pow(96,2));


            $v_image_name = str_replace('.'.$arr_ext[$type], '', $v_image_file);
            do{
                $p = strpos($v_image_name,'  ');
                if($p!==false){
                    $v_image_name = str_replace('  ',' ', $v_image_name);
                }
            }while($p!==false);
            $v_image_key = seo_friendly_url($v_image_name);
            $v_image_key = str_replace('-','_', $v_image_key);

            $v_image_key = str_replace('img','pic', $v_image_key);

            $i = 0;
            $v_tmp_image_key = $v_image_key;
            do{
                $arr_tmp_where = array('image_key'=>$v_tmp_image_key/*, 'image_id'=>array('$ne'=>$v_image_id)*/);
                $v_tmp_row = $cls_images->select_one($arr_tmp_where);
                if($v_tmp_row>0){
                    $i++;
                    $v_tmp_image_key = $v_image_key.'_'.$i;
                }
            }while($v_tmp_row>0);
            $v_image_key = $v_tmp_image_key;

            //$v_image_id = $cls_images->select_next('image_id');
            //$cls_images->set_image_id($v_image_id);
            $cls_images->set_image_name($v_image_name);
            $cls_images->set_image_file($v_image_file);
            $cls_images->set_image_original_file($v_image_original_file);
            $cls_images->set_image_key($v_image_key);
            $cls_images->set_image_type($type);
            $cls_images->set_image_dpi($v_dpi);
            $cls_images->set_image_extension($arr_ext[$type]);
            $cls_images->set_image_height($height);
            $cls_images->set_image_width($width);
            $cls_images->set_image_cost(0);
            $cls_images->set_image_size($arr_result['size_in_bytes']);
            $cls_images->set_image_status(0);
            $cls_images->set_is_public(0);
            $cls_images->set_image_stock_id(0);
            $cls_images->set_design_id($v_design_id);
            $cls_images->set_fotolia_id('');
            $cls_images->set_fotolia_size('');
            $cls_images->set_template_id($v_template_id);
            $cls_images->set_theme_id($v_theme_id);
            $cls_images->set_saved_dir($v_saved_dir);
            $cls_images->set_created_time(date('Y-m-d H:i:s'), false);
            $cls_images->set_user_id($v_assign_to);
            $cls_images->set_is_admin($v_admin);
            $cls_images->set_user_ip($v_user_ip);
            $cls_images->set_user_name($v_user_name);
            $cls_images->set_user_agent($v_user_agent);
            $cls_images->set_company_id($v_user_company_id);
            $cls_images->set_site_id($v_site_id);
            $cls_images->set_location_id(0);
            $v_image_id = $cls_images->insert();
            $v_result = $v_image_id > 0;


            if($v_result){
                $arr_return = array(
                    "dpi"=>0
                    ,"extension"=>$arr_ext[$type]
                    ,"fotolia_id"=>null
                    ,"height"=>$height
                    ,"width"=>$width
                    ,"id"=>$v_image_id
                    ,"sid"=>session_id()
                    ,"is_public"=>0
                    ,"is_vector"=>0
                    ,"name"=>$up->get_current_name()
                    ,"success"=>1
                    ,"error"=>0
                    ,"svg_id"=>null
                    ,"svg_original_colors"=>null
                );
                add_class('cls_draw');
                $cls_draw = new cls_draw();
                $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
            }
        }
    }
}
$arr_return['message'] = $up->get_errors();

if($v_is_cross_domain)
    $cls_output->output($arr_return);
else{
    $v_random = seo_friendly_url($v_random);
    if($v_random!=''){
        $v_save_file = DESIGN_TEMP_DIR.DS.$v_random.'.txt';
        $mf->set_file_content(json_encode($arr_return));
        $mf->write_file($v_random.'.txt', DESIGN_TEMP_DIR);
    }
    die();
}
//    $cls_output->output_jsonp($arr_return, 'jsonp_callback');