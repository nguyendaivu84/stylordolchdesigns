<?php if(!isset($v_sval)) die();?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <title>Insert Image</title>

    <link href="<?php echo URL;?>lib/design/css/vi.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/design/js/helpers.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/design/js/vi.js"></script>
    <link rel="stylesheet" href="<?php echo URL;?>lib/design/css/colorbox.css" />



    <script src="<?php echo URL;?>lib/design/js/jquery.ui.widget.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/design/js/jquery.fileupload.js" ></script>

    <link href="<?php echo URL;?>lib/design/css/add_image.css" rel="stylesheet" type="text/css">

</head>

<body>
<div class="please-wait"><div>Please wait...</div></div>

<div id="wrapper">
    <div id="close_link"></div>
    <!--
        <div class="sign-in-guide">To retrieve your previously uploaded images, <span class="fake-link" id="login_link" >sign in here</span>.</div>
    -->
    <div id="top_section">
        Insert Image
    </div>

    <div id="image_panel_left_section">
        <div class="left-tab" id="user_tab" >Template's Images</div>
        <!--
        <div class="left-tab" id="our_tab">Our Images</div>
        -->
        <div class="left-tab" id="stock_tab">Stock Images</div>
        <!--
        <div class="left-tab" id="facebook_tab"><div class="fb-icon"></div>Facebook</div>
        -->
    </div>

    <div id="image_panel_right_section">

        <div class="right-panel" id="stock_panel">
            <div id="stock_search_bar">
                <div id="stock_image_hint">Search from millions of high quality stock images. Prices start from only $1.</div>
                <div>
                    <input id="stock_search_term" type="text" />
                    <button class="action-button red" id="stock_search_button">Search Images</button>
                </div>
                <div class="image-cont"></div>

            </div>
        </div>
        <!--
        <div class="right-panel" id="our_panel">
            <div id="image_divider"></div>
        </div>
        -->
        <div class="right-panel" id="user_panel">
            <div id="image_drop_zone">
                <div class="upload-guide">
                    <div class="upload-drag dnd-only">DRAG IMAGE FILES FROM YOUR COMPUTER HERE</div>
                    <div class="upload-format">We accept JPEG, PNG, GIF. &nbsp;&nbsp;&nbsp; Max file size: 2MB</div>
                </div>
                <div class="upload-or dnd-only">OR</div>
                <div id="btn_upload" class="action-button red">
                    Click to Upload
                    <input id="fileupload" name="Filedata" type="file" class="styled-upload" multiple="1" accept="image/png,image/jpg,image/jpeg,image/gif" data-url="/tools.php?action=add_image&action_type=image_upload" />
                </div>
            </div>
            <div style="text-align: left">
                <input type="checkbox" id="chk_svg" /> <label class="upload-drag" style="font-size: 12px" for="chk_svg">Displayed only vector images</label>
            </div>

            <div id="image_divider"></div>
            <div id="fotolia_divider"></div>

        </div>
        <!--
        <div class="right-panel" id="facebook_panel">
            <div id="facebook_login" class="centered"><div class="fb-login-button" scope="user_photos">Connect to Facebook</div></div>
        </div>
        -->
    </div>

    <div id="bottom_section">
        <button id="select_image_button" class="action-button green disabled">Insert Image</button>
    </div>

</div>
<iframe id="help_frame" src='' height='0' width='0' frameborder='0'></iframe>
</body>
<script type="text/javascript">
var glob = {} ; // global namespace
glob.userPage = 0;
asset_name = '' ;
uploadInstance = 0 ;

var is_cross_domain = -1;
if ('withCredentials' in new XMLHttpRequest()) {
    is_cross_domain = 1;
}
else if(typeof XDomainRequest !== "undefined"){
    is_cross_domain = 0;
}
if(is_cross_domain==-1)
    alert("WARNING:\r Your browser does not support AJAX. That work is stopped here!\nThank you!");
is_cross_domain = is_cross_domain==1;
var jsonp_data = null;

function create_random_string(len){
    var str = 'qwertyuiopasdfghjklzxcvbnm1234567890-';
    var i = 0, k =0;
    var r = '';
    do{
        k = Math.floor(Math.random()*str.length);
        r += str.charAt(k);
        i++;
    }while(i<len);
    return r;
}
var random = create_random_string(20);
var domItem = null;


$(document).on('click' , '.left-tab' , function(){
    $(this).addClass('on').siblings().removeClass('on') ;
    $('#' + $(this).attr('id').replace('tab' , 'panel') ).show().siblings().hide();

    if( $(this).attr('id') == 'upload_tab'){
        $('#select_image_button').hide();
        if(!uploadInstance)
            $('#upload_overlay').hide();
    }
    else
        $('#select_image_button').show();

    if($(this).attr('id') == 'stock_tab')
        $('#stock_search_term').focus();

});
$(document).on('dblclick' , '.image-item' , function(){ $('#select_image_button').click() });
$(document).bind('drop dragover', function (e) {
    e.preventDefault();
});

$(document).bind('dragover dragenter', function (e) {
    $('#image_drop_zone').addClass('over');
});
$(document).bind('dragleave dragend drop', function (e) {
    $('#image_drop_zone').removeClass('over');
});
$('#user_tab').on('click' , function(){
    populateUserImages();
});

$('#stock_search_button').click(function(){
    if($(this).hasClass('disabled'))
        return ;
    $('#stock_panel .image-cont').html('');
    $('#stock_panel').scrollTop(0);
    glob.stockPage = 0 ;
    populateStockImages();
});



$('body').on('click' , deselectImages ) ;

$(document).on('click' , '.image-item' , function(){
    $(this).addClass('selected');
    $('#select_image_button').removeClass('disabled');
});

$(document).on('click' , '.image-delete-button' , function(){
//		if(! confirm('Are you sure you want to remove this image?')) return ;
    var _this = this ;
    $.ajax({
        url: "/tools.php?action=add_image&action_type=image_delete" ,
        dataType: is_cross_domain? 'json':'jsonp' ,
        jsonpCallback: is_cross_domain?null:'jsonp_callback',
        data: {"image_id" : $(_this).closest('.image-item').attr('id').split('_').pop(), sid: '<?php echo session_id();?>', cross: is_cross_domain } ,
        success: function(data){
            if(!is_cross_domain) data = jsonp_data;
            $(_this).closest('.image-item').fadeOut(300);
        }

    });
});
function populateUserImages(){
    //$('#user_panel .image-item').remove();
    glob.userPage++;
    $.ajax({
        <?php if(isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE){?>
        url: "/tools.php?action=add_image&action_type=image_template" ,
        <?php }else{?>
        url: "/tools.php?action=add_image&action_type=image_design" ,
        <?php }?>
        dataType: is_cross_domain? 'json':'jsonp' ,
        jsonpCallback: is_cross_domain?null:'jsonp_callback',
        data: {sid:'<?php echo session_id();?>', cross: is_cross_domain, limit: 20, page: glob.userPage, svg_only: $('input#chk_svg').is(':checked')?1:0},
        success: function(data){
            glob.moreUserResultRequested = false;
            $.each(data.rows , function(k,v){
                addImageToList(v, false, true);
            });
        } ,
        error: function(){
            console.log('error uploading');
        }
    });
}
function addImageToList(v, select_after_insert, is_user){
    var d = $('<div />').attr('id' , 'image_' + v.id).attr('title', v.name).addClass('image-item');
    $('<img />').attr('src' , '/tools.php?action=add_image&action_type=image_thumb&size=100&image_id=' + v.id+'&sid=<?php echo session_id();?>').appendTo(d);

    if(is_user) $('<div />').addClass('image-delete-button').appendTo(d);

    if(v.fotolia_id){
        $('<div />').addClass('dollar').appendTo(d);
        d.insertAfter('#fotolia_divider') ;
    }
    else{
        if(is_user)
            d.insertAfter('#user_panel #image_divider') ;
        else
            d.insertAfter('#our_panel #image_divider') ;
    }
    if(select_after_insert){
        d.click();
    }

}

function deselectImages(){
    $('.image-item').removeClass('selected');
    $('#select_image_button').addClass('disabled');
}

$(document).ready(function(){
    var matched, browser;

// Use of jQuery.browser is frowned upon.
// More details: http://api.jquery.com/jQuery.browser
// jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

    $('#user_tab').click();
    if($.browser.msie)
        $('.dnd-only').hide();
});

$(document).ready(function(e){
    $('input#chk_svg').click(function(e){
        glob.userPage = 0;
        $('div#user_panel').find('.image-item').remove();
        populateUserImages();
    });

    $('#select_image_button').click(function(){

        var img = $('.image-item.selected') ;
        if(img.length != 1)
            return ;
        $('.please-wait').show();
        if(img.hasClass('fotolia-result')){
            $.ajax({
                url : '/tools.php?action=add_image&action_type=use_fotolia' ,
                data: {"fotolia_id" : img.attr('id').split('_').pop() , "asset_name" : asset_name, cross: is_cross_domain } ,
                dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
                success: function(data){
                    if(!is_cross_domain) data = jsonp_data;
                    $('.please-wait').hide();
                    useImageInParent(data);
                } ,
                error: function(){
                    $('.please-wait').hide();
                    alert('Sorry! An error occured while retrieving the stock image. Please try again.');
                }
            })
        }
        else if(img.hasClass('fb-photo-item')){
            $.ajax({
                //url : '/ajax/facebook/registerImage.php' ,
                data: {"photo_object_id" : img.attr('id').split('_').pop() , "asset_name" : asset_name, cross: is_cross_domain} ,
                dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
                success: function(data){
                    if(!is_cross_domain) data = is_cross_domain;
                    $('.please-wait').hide();
                    useImageInParent(data);
                }
            });
        }
        else {
            $.ajax({
                url : '/tools.php?action=add_image&action_type=image_info' ,
                data: {"image_id" : img.attr('id').split('_').pop(), 'sid': '<?php echo session_id();?>', cross: is_cross_domain} ,
                dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
                success: function(data){
                    if(!is_cross_domain) data = jsonp_data;
                    $('.please-wait').hide();
                    useImageInParent(data);
                }
            })

        }

    });
});

$(document).on('dblclick' , '.image-item' , function(){ $('#select_image_button').click() })

$('#stock_search_term').keydown(function(e){ if(e.keyCode == 13)  $('#stock_search_button').click(); }) ;
function populateStockImages(){
    glob.stockPage++ ;
    if(glob.stockPage > 20)
        return ;
    $('#stock_search_button').html('Please Wait...').addClass('disabled');
    $.ajax({
        url : '/tools.php?action=add_image&action_type=get_fotolia' ,
        dataType: is_cross_domain? 'json':'jsonp' ,
        jsonpCallback: is_cross_domain?null:'jsonp_callback',
        data: {"words" : $('#stock_search_term').val() , limit: 50 , "page" :  glob.stockPage, cross: is_cross_domain } ,
        success: function(data){
            if(!is_cross_domain) data = jsonp_data;
            $('#stock_search_button').html('Search Images').removeClass('disabled');
            glob.moreStockResultRequested = false ;
            $.each(data, function(k,v) {
                if(k == 'nb_results' && v == 0){
                    if(glob.stockPage == 1)
                        alert('Sorry! No results found.') ;
                    return ;
                }
                var d = $('<div />').attr('id' , 'fotolia_' + v.id ).addClass('image-item fotolia-result').appendTo('#stock_panel .image-cont');
//alert(v.licenses.length);
                $('<div />').addClass('stock-price').html('From $' + v.licenses[0].price).appendTo(d) ;
                $('<img />').attr('src' , v.thumb).appendTo(d);
//                $('<img />').attr('src' , v.thumbnail_110_url).appendTo(d);
            });
        }
    });
}

$('#stock_panel').scroll(function(){
    if($('#stock_panel')[0].scrollHeight -	$('#stock_panel').scrollTop() - $('#stock_panel').height() < 100 && ! glob.moreStockResultRequested ){
        glob.moreStockResultRequested = true ;
        populateStockImages();
    }
}) ;

$('#user_panel').scroll(function(){
    if($('#user_panel')[0].scrollHeight -	$('#user_panel').scrollTop() - $('#user_panel').height() < 100 && ! glob.moreUserResultRequested ){
        glob.moreUserResultRequested = true ;
        populateUserImages();
    }
}) ;


function useImageInParent(data){
    window.parent.useImage(data);
}

$(document).ready(function(e){
    $('#close_link').click(function(){
        window.parent.cancelAddImage();
    });
});

$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
//		forceIframeTransport: true,
        done: function (e, data) {
            console.log();
        }
    });

    $('#upload_redirect_url').val(window.location.href);


    $('#fileupload').bind('fileuploadprogress', function (e, data) {
        data.domItem.find('.upload-item-progress').css('width', data.loaded / data.total * 100  + '%');
    })

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
        random = create_random_string(20);
        console.log('fileuploadsubmit' , data);
        data.formData = {cross : is_cross_domain, random: random};
        var name = data.files[0].name || data.files[0].fileName  || '' ;
        var size = data.files[0].size || data.files[0].fileSize || 0 ;

        data.domItem = $('<div />').addClass('upload-item').insertBefore('#user_panel #image_divider');
        var ph = $('<div />').addClass('image-ph').appendTo(data.domItem);
        var st = $('<div />').addClass('upload-item-status').appendTo(ph).html('Uploading');
        $('<div />').addClass('upload-item-name').appendTo(ph).html(name);
        var pgph = $('<div />').addClass('upload-item-progress-ph').appendTo(ph);
        $('<div />').addClass('upload-item-progress').appendTo(pgph);
        domItem = data.domItem;

        if(name.length > 0 && $.inArray(name.split('.').pop().toLowerCase() , ['jpg','jpeg','png','pdf','gif'] ) == -1  ){
            data.domItem.addClass('error');
            st.html('.' + name.split('.').pop().toLowerCase() + ' files are not supported.');
            data.domItem.delay(4000).fadeOut() ;
            return false;
        }

        if(size > 20 * 1024 * 1024 ){
            data.domItem.addClass('error');
            st.html('File is too large.');
            data.domItem.delay(4000).fadeOut();
            return false ;
        }

    });

    $('#fileupload').bind('fileuploaddone', function (e, data) {
        if(is_cross_domain){
            console.log('fileuploaddone' , data);
            if(data.result.error){
                data.domItem.addClass('error');
                data.domItem.find('.upload-item-status').html('Invalid File.');
                data.domItem.delay(4000).fadeOut();
                return false;
            }
            //alert(data.result.success);
            data.domItem.remove();
            addImageToList(data.result, true, true);
        }else{
            $.ajax({
                url     : '/tools.php?action=add_image&action_type=get_image_upload',
                dataType: 'jsonp',
                jsonpCallback: 'jsonp_callback',
                data    : {random: random},
                async   : false,
                success : function(data){
                    data = jsonp_data;
                    if(data.error){
                        domItem.addClass('error');
                        domItem.find('.upload-item-status').html('Invalid File.');
                        domItem.delay(4000).fadeOut();
                    }else{
                        //alert(data.result.success);
                        domItem.remove();
                        addImageToList(data, true, true);
                    }
                }
            });
            return true;
        }

    });


    $('#fileupload').bind('fileuploadsend', function (e, data) {
//		data.domItem.html('send');
    });

});

function jsonp_callback(response){
    jsonp_data = response;
}
</script>
</html>