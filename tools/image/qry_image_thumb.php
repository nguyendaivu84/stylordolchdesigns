<?php
if(!isset($v_sval)) die();
?>
<?php
$v_image_id = isset($_GET['image_id'])?$_GET['image_id']:'0';
$v_sid = isset($_GET['sid'])?$_GET['sid']:'';
$v_size = isset($_GET['size'])?$_GET['size']:'100';

settype($v_image_id, 'int');
settype($v_size, 'int');

if($v_image_id>0){
    add_class('cls_tb_design_image');
    $cls_image = new cls_tb_design_image($db, LOG_DIR);
    add_class('cls_tb_design_svg');
    $cls_svg = new cls_tb_design_svg($db, LOG_DIR);
    $v_row = $cls_image->select_one(array('image_id'=>$v_image_id, 'image_status'=>0));
    if($v_row==1){
        $v_saved_dir = $cls_image->get_saved_dir();
        $v_image_name = $cls_image->get_image_name();
        $v_image_type = $cls_image->get_image_type();
        $v_image_file = $cls_image->get_image_file();
        $v_svg_id = $cls_image->get_svg_id();
        if($v_svg_id>0){
            $v_row = $cls_svg->select_one(array('svg_id'=>$v_svg_id));
            if($v_row==1){
                $v_image_file = DESIGN_IMAGE_THUMB_SIZE . '_' . $v_image_file;
            }else{
                echo 'Empty Data';
            }
        }
        if(file_exists($v_root_dir.$v_saved_dir.$v_image_file)){
            $image = new Imagick();
            $image->readimage($v_root_dir.$v_saved_dir.$v_image_file);
            $v_width = $image->getimagewidth();
            $v_height = $image->getimageheight();

            $v_ratio = $v_width/100;
            $v_new_height = $v_height / $v_ratio;

            $image->resizeimage($v_size, $v_new_height,Imagick::FILTER_LANCZOS,1);
            $image->setimageformat('png');


            /* Output the image with headers */
            header('Content-type: image/png');
            echo $image;

            $image->clear();
            $image->destroy();
        }else{
            echo 'Image not found!';
        }
    }else{
        echo 'Not select data!';
    }
}else{
    echo 'Empty image id!';
}