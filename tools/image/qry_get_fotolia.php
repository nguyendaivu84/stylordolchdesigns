<?php
require_once ROOT_DIR.'/api/fotolia/fotolia-api.php';
ini_set('max_execution_time', 240);

$api = new Fotolia_Api(DESIGN_FOTOLIA_KEY);
//yfi64LivT5TeR6HDRPTrKobgLDDt8wOx
$words = $_GET['words'];
$limit = $_GET['limit'];
$page = $_GET['page'];

$start = $page * $limit;

if(!empty($words)){
    $results = $api->getSearchResults(
        array(
            'words' => $words,
            'order' => 'nb_views',
            'language_id' => Fotolia_Api::LANGUAGE_ID_EN_US,
            'limit' => $limit,
            'offset' => $start,
            'filters' => array(
                'content_type:video' => 0,
                'content_type:photo' => 1,
                'content_type:illustration' => 1,
                'content_type:vector' => 1,
            ),
        ));

    $i = 0;
    $a = array();
    foreach ($results as $key => $value) {
        // iterating only over numeric keys and silently skip other keys
        if (is_numeric($key)) {
            $price = 0;
            $v_name = '';
            $arr_license = array();
            foreach ($value['licenses'] as $license) {
                if ($license['name'] == 'L') {
                    $price = $license['price'] * 1.30;
                    $v_name = $license['name'];
                    $arr_license[] = array('price'=>$price, 'name'=>$v_name);
                    break;
                }
            }

            if ($price == 0) {
                foreach ($value['licenses'] as $license) {
                    if ($license['name'] == 'M') {
                        $price = $license['price'] * 1.30;
                        $v_name = $license['name'];
                        $arr_license[] = array('price'=>$price, 'name'=>$v_name);
                        break;
                    }
                }
            }

            $a[$i] = array('id'=>$value['id'],'thumb'=>$value['thumbnail_url'],'price'=>floor($price), 'licenses'=>$arr_license);
            $i = $i + 1;
        }
    }
    //$arr_return = (object)$a;
    $arr_return = $a;

} else {
    $arr_return = array('nb_results'=>0);
}
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else{
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
}
