<?php
if(!isset($v_sval)) die();
?>
<?php
$v_image_id = isset($_GET['image_id'])?$_GET['image_id']:'0';
$v_sid = isset($_GET['sid'])?$_GET['sid']:'';
$v_size = isset($_GET['size'])?$_GET['size']:'100';

settype($v_image_id, 'int');
settype($v_size, 'int');
$arr_return = array('success'=>0, 'message'=>'');

if($v_image_id>0){
    add_class('cls_tb_design_image');
    $cls_images = new cls_tb_design_image($db, LOG_DIR);
    add_class('cls_tb_design_svg');
    $cls_svg = new cls_tb_design_svg($db, LOG_DIR);
    $v_row = $cls_images->select_one(array("image_id"=>$v_image_id));
    if($v_row==1){
        $v_saved_dir = $cls_images->get_saved_dir();
        $v_image_name = $cls_images->get_image_name();
        $v_image_file = $cls_images->get_image_file();
        $v_image_key = $cls_images->get_image_key();
        $v_image_type = $cls_images->get_image_type();
        $v_is_public = $cls_images->get_is_public();
        $v_image_status = $cls_images->get_image_status();
        $v_fotolia_id = $cls_images->get_fotolia_id();
        $v_stock_id = $cls_images->get_image_stock_id();
        $v_dpi = $cls_images->get_image_dpi();
        $v_svg_id = $cls_images->get_svg_id();
        $arr_licenses = array();
        if($v_fotolia_id.''=='')
            $v_fotolia_id = null;
        else{
            $v_fotolia_id = (int) $v_fotolia_id;
            add_class('cls_tb_design_stock');
            $cls_stocks = new cls_tb_design_stock($db, LOG_DIR);
            $arr_licenses = $cls_stocks->select_scalar('licenses', array('stock_id'=>$v_stock_id));
        }
        if($v_image_status==0){
            if($v_svg_id > 0){
                if($cls_svg->count(array('svg_id'=> (int) $v_svg_id))!=1) $v_svg_id = 0;
            }
            if($v_svg_id > 0 || file_exists($v_root_dir.$v_saved_dir.$v_image_file)){
                $arr_return = array(
                    "dpi"=>$v_dpi
                    ,"extension"=>$cls_images->get_image_extension()
                    ,"fotolia_id"=>$v_fotolia_id
                    ,"height"=>$cls_images->get_image_height()
                    ,"width"=>$cls_images->get_image_width()
                    ,"id"=>$cls_images->get_image_id()
                    ,"is_public"=>$v_is_public
                    ,"is_vector"=>$v_svg_id > 0?1:0
                    ,"name"=>$v_image_key
                    ,"success"=>1
                    ,"svg_id"=>$v_svg_id > 0?$v_svg_id:null
                    ,"svg_original_colors"=>$cls_images->get_svg_original_colors()
                    ,'licenses'=>$arr_licenses
                    ,'message'=>'OK'
                );
            }else{
                $arr_return['message']= 'Image not found!';
            }
        }else{
            $arr_return['message']= 'Access denied!';
        }
    }else{
        $arr_return['message']= 'Not select data!';
    }
}else{
    $arr_return['message']= 'Empty image id!';
}
if($v_is_cross_domain)
    $cls_output->output($arr_return);
else
    $cls_output->output_jsonp($arr_return, 'jsonp_callback');
