var model,
    router,
    categories = {
        0: "Uncategorized",
        1: "Celebrities",
        2: "Film",
        3: "Journalism",
        4: "Nude",
        5: "Black and White",
        6: "Still Life",
        7: "People",
        8: "Landscapes",
        9: "City and Architecture",
        10: "Abstract",
        11: "Animals",
        12: "Macro",
        13: "Travel",
        14: "Fashion",
        15: "Commercial",
        16: "Concert",
        17: "Sport",
        18: "Nature",
        19: "Performing Arts",
        20: "Family",
        21: "Street",
        22: "Underwater",
        23: "Food",
        24: "Fine Art",
        25: "Wedding",
        26: "Transporation",
        27: "Urban Exploration"
    },
    imagePlaceHolder = { image_url: "", calculatedWidth: "400px", noAvatar: true },
    //popularQuery = { url: "/templates", settings: { feature: "popular", exclude: "Nude", page: 1, image_size: 1, rpp: 50 } };
    popularQuery = { url: template_url+"/home/template", settings: { feature: "popular", exclude: "Nude", page: 1, image_size: 1, rpp: 50 } };

/*
var templates = new kendo.data.DataSource({
    transport: {
        read: function(options) {
            alert(options.data.url);
            _500px.api(options.data.url, options.data.settings, function(response) {
                options.success(response.data.templates);
            });
        }
    }
});
*/


var templates = new kendo.data.DataSource({
    transport: {
        read: {
            url: template_url+'home/template',
            dataType:'json',
            data: {}
        }
    }
});
//var templates = dataSource.data.templates;

$(document).on("touchmove", false);

model = kendo.observable({
    templates: templates,
    current: null,
    currentTemplate: imagePlaceHolder,
    query: "",
    showDetails: true,
    slideShow: false,
    currentQuery: null,

/*
    show: function(templateID) {
        this.set("currentPhoto", imagePlaceHolder);
        this.set("current", parseInt(templateID));

        _500px.api('/templates/' + templateID, { image_size: 4 }, function(response) {
            var template = response.data.template;
alert(JSON.stringify(template));
            imagePlaceHolder.calculatedWidth = template.calculatedWidth = (($("#image-inner-wrap").find(".image").height() / template.height) * template.width) + "px";
            template.categoryName = template.category ? categories[template.category] : "";
            template.noAvatar = template.user.userpic_url == "/graphics/userpic.png";
            template.backgroundPhoto = template.image_url ? "url('" + template.image_url + "')" : "";
            model.set("currentPhoto", template);
        });
    },
*/

    show: function(templateID) {
        this.set("currentTemplate", imagePlaceHolder);
        this.set("current", parseInt(templateID));
        $.ajax({
            url: template_url+'home/template/'+templateID,
            success: function(data){
                var template = data.template;
                var templateRatio = template.height / template.width;
                var ratio, calWidth;
                $("#image-inner-wrap").find(".image").height(300);
                if(templateRatio > 1){
                    //$("#image-inner-wrap").find(".image").height($("#image-inner-wrap").find(".loading").height());
                    if(template.height>300){
                        ratio = $("#image-inner-wrap").find(".image").height() / template.height;
                        calWidth = ratio * template.width;
                    }else{
                        calWidth = template.width;
                    }
                    $("#image-inner-wrap").find(".image").width(Math.ceil(calWidth));

                    //ratio = $("#image-inner-wrap").find(".image").height() / template.height;
                }else{
                    if(template.width>500){
                        calWidth = 500;
                        //ratio = $("#image-inner-wrap").find(".image").height() / template.height;
                        //calWidth = ratio * template.width;
                    }else if(template.width<200)
                        calWidth = 200;
                    else
                        calWidth = template.width;
                    ratio = calWidth / template.width;
                    //$("#image-inner-wrap").find(".image").width($("#image-inner-wrap").find(".loading").width());
                    //ratio = $("#image-inner-wrap").find(".image").width() / template.width;
                    $("#image-inner-wrap").find(".image").height(Math.ceil(template.height*ratio));
                }

                //var ratio = $("#image-inner-wrap").find(".image").height() / template.height;
                //calWidth = Math.ceil(ratio * template.width);

                //if(calWidth>500)
                //    calWidth = 500;
                //else if(calWidth<300)
                //    calWidth = 300;
                //    ratio = calWidth / template.width;
                    //alert($("#image-inner-wrap").find(".image").height());
                    //$("#image-inner-wrap").find(".image").height(template.height*ratio);
                    //$("#image-inner-wrap").find(".image").width(calWidth);
                    //alert('Af: '+$("#image-inner-wrap").find(".image").height());
                    //$("#image-inner-wrap").find(".image").css('margin', '0 auto');
                //}
                //imagePlaceHolder.calculatedWidth = template.calculatedWidth = (($("#image-inner-wrap").find(".image").height() / template.height) * template.width) + "px";
                imagePlaceHolder.calculatedWidth = template.calculatedWidth = calWidth + "px";
                template.categoryName = template.category ? categories[template.category] : "";
                template.noAvatar = template.user.userpic_url == "/graphics/userpic.png";
                template.backgroundPhoto = template.image_url ? "url('" + template.image_url + "')" : "";
                model.set("currentTemplate", template);
            }
        });
    },



    thumbHref: function(item) {
        var currentQuery = this.get("currentQuery");

        if (currentQuery) {
            return "#search/" + encodeURIComponent(currentQuery) + "/templates/" + item.get("id");
        } else {
            return "#templates/" + item.get("id");
        }
    },

    thumbClass: function(item) {
        return item.get("id") === this.get("current") ? "selected" : "";
    },

    performSearch: function(e) {
        e.preventDefault();

        var query = this.get("query");

        if (query) {
            router.navigate("search/" + encodeURIComponent(query));
        } else {
            router.navigate("");
        }
    },

    performLogin: function(e) {
        e.preventDefault();

        var username = this.get("username");
        var password = this.get("password");
        var $this = e.target;
        var id = $this.id;
        $this = $('input#'+id);
		username = $.trim(username);
		if(username==''){
			popupOpen("Warning!","Please, input valid username!","",["Yes"],null,0,"Login");
			return false;
		}
		if(password+''=='undefined' || password==''){
			popupOpen("Warning!","Please, input your password!","",["Yes"],null,0,"Login");
			return false;
		}
        $.ajax({
            url: template_url+'login',
            type: 'POST',
            data: {txt_user_name: username, txt_user_password: password, txt_ajax_action:'login'},
            dataType:'json',
            beforeSend: function(){
                $this.css('display', 'none');
                $('label#lbl_loading').css('display', '');

            }
            , success: function(data){
                if(data.error==0){
                    $('label#lbl_loading').css('display', 'none');
                    $('label#lbl_redirect').css('display', '');
                    document.location = redirect_url;
                }else{
					popupOpen("Warning!",data.message,"",["Yes"],null,0,"Login");
                    $('label#lbl_loading').css('display', 'none');
                    $('label#lbl_redirect').css('display', 'none');
                    $this.css('display', '');
                }
            }
        });
		return true;
    },

    confirmLogout: function(e){
        e.preventDefault();
        popupOpen("Confirm","Do you want to logout?","",["No","Yes"],popup_close_logout,0,"Logout");
    },

    toggleFocus: function() {
        $(document.body).toggleClass("focused");
    },

    searchFor: function(query, showFirst) {
        var that = this,
            currentQuery = this.get("currentQuery");

        if (currentQuery !== query) {
            this.set("currentQuery", query);
            this.set("query", query);
            if (query) {
                templates.read({ url: '/templates/search', settings: { term: query, page: 1, image_size: 1, rpp: 50 } });
            } else {
                templates.read(popularQuery);
            }

            if (showFirst) {
                templates.one("change", function() {
                    var data = this.data();
                    if (data.length) {
                        model.show(data[0].id);
                    } else {
                        alert('No results found, how about some kittens instead?');
                        router.navigate("search/kittens");
                    }
                })
            }
        }
    },

    scrollRight: function() {
        var width = $("#template-thumbs").width();
        $("#template-thumbs").animate({ scrollLeft: "+=" + width }, 500);
    },

    scrollLeft: function() {
        var width = $("#template-thumbs").width();
        $("#template-thumbs").animate({ scrollLeft: "-=" + width }, 500);
    },

    toggleShowDetails: function(e) {
        this.set("showDetails", !this.get("showDetails"));
        e.stopImmediatePropagation();
    },

    slideShowButtonText: function() {
        return this.get("slideShow") ? "stop" : "play";
    },

    detailsClass: function() {
        return this.get("showDetails") ? "visible" : "";
    },

    toggleSlideShow: function() {
        var slideShow = !this.get("slideShow");
        this.set("slideShow", slideShow);
        if (slideShow) {
            this.interval = setInterval(this.moveNext, 5000);
        } else {
            this.stopSlideShow();
        }
    },

    stopSlideShow: function() {
        clearInterval(this.interval);
    },

    moveNext: function() {
        var currentItem = templates.get(model.currentPhoto.id);
        var currentItemIndex = templates.data().indexOf(currentItem);

        if (currentItemIndex === -1) {
            return;
        }

        if (currentItemIndex == templates.data().length - 1) {
            currentItemIndex = 0;
        }

        var nextItem = templates.at(currentItemIndex + 1);
        if (model.currentQuery) {
            router.navigate("search/" + encodeURIComponent(model.currentQuery) + "/templates/" + nextItem.id);
        } else {
            router.navigate("templates/" + nextItem.id);
        }
    }
});

var main = new kendo.View("main", { model: model });

router = new kendo.Router({
    init: function() {
        main.render("#root");
    }
});

router.route("/", function(id) {
    model.searchFor("", true);
});

router.route("search/:query/templates/:id", function(query, id) {
    model.show(id);
    model.searchFor(query, false);
});

router.route("search/:query", function(query) {
    model.searchFor(query, true);
});

router.route("templates/:id", function(id) {
    model.show(id);
    model.searchFor("", false);
});

$(function() {
    /*
    _500px.init({
        sdk_key: 'a3be03a8a98d6e05af17f60d2cf4bf696eb47555'
    });
    */
    router.start();
});

function popup_close_logout(popupResult,value,bntType){
    if(popupResult=='Yes') document.location = template_url + 'logout';
}