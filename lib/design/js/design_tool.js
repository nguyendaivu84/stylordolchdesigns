$(document).ready(function(){
    var matched, browser;

// Use of jQuery.browser is frowned upon.
// More details: http://api.jquery.com/jQuery.browser
// jQuery.uaMatch maintained for back-compat
    jQuery.uaMatch = function( ua ) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
            /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
            /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
            /(msie) ([\w.]+)/.exec( ua ) ||
            ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
            [];

        return {
            browser: match[ 1 ] || "",
            version: match[ 2 ] || "0"
        };
    };

    matched = jQuery.uaMatch( navigator.userAgent );
    browser = {};

    if ( matched.browser ) {
        browser[ matched.browser ] = true;
        browser.version = matched.version;
    }

// Chrome is Webkit, but Webkit is also Safari.
    if ( browser.chrome ) {
        browser.webkit = true;
    } else if ( browser.webkit ) {
        browser.safari = true;
    }

    jQuery.browser = browser;

});
///End jQuery.browser

var is_cross_domain = -1;
if ('withCredentials' in new XMLHttpRequest()) {
    /* supports cross-domain requests */
    //alert("CORS supported (XHR)");
    is_cross_domain = 1;
}
else if(typeof XDomainRequest !== "undefined"){
    //Use IE-specific "CORS" code with XDR
    //alert("CORS supported (XDR)");
    is_cross_domain = 0;
}
if(is_cross_domain==-1)
    alert("WARNING:\r Your browser does not support AJAX. That work is stopped here!\nThank you!");
is_cross_domain = is_cross_domain==1;
var jsonp_data = null;


var view = {} ;
var resize = {} ; // resize namespace
var config = config || {} ;

var tempTextImages = [] ;
var tempTextTimeouts = [] ;
var tempTextTimestamps = [] ;

config.maxZoomLevel = 6;
config.minZoomLevel = -2 ;
config.maxUploadSizeMB = 20 ;
config.dragLockMargin = user.admin_mode === 1 ? 1 : 8 ;  // admins don't like drag lock
config.snapMargin = 3 ;   
config.minFotoliaRes = 200 ;
config.noticeRes = 200 ;
config.warningRes = 100 ;
config.fontSizes = [7, 8, 9, 10, 11, 12, 14, 16, 18, 20, 22, 24, 30, 36, 48, 60, 72, 96, 150, 200, 300, 400, 600, 800, 1000, 1200, 1400]; 
config.borderSizes = [0, 1, 2, 3, 4, 5, 6, 8, 10, 12, 14, 16, 18, 20, 24,40];
config.color_palette = ['transparent','ffffff','ebebeb','d6d6d6','c0c0c0','a9a9a9','929292','797979','5f5f5f','424242','222222','000000',
						// '00364a','162255','162255','2f143c','381421','561a17','58230f','553715','524019','67632d','515726','2c3e20',
						// '004d65','2e3a77','201350','431957','51192c','821810','7b2e0e','794d1b','775a24','8b893e','6f793a','3c572a',
						// '006e90','304fa1','322e72','60347a','791a3d','b43129','ad4327','a76a2e','a67d34','c3bd41','99a74a','4f7b41',
						// '008eb5','4666b3','403c8e','794099','992850','e3382f','da5430','d58634','d29e37','f4eb04','c3d142','669e51',
						// '00a4d7','5071b9','5746a4','914da5','ba3360','f7482b','f86c29','fca921','fcc616','faee46','d8e144','77bb57',
						// '46c4ef','5a82c2','6364b1','9c66af','e73d7a','f96455','fa864c','fcb343','fec93e','fcf072','e3e66a','98cd63',
						// '66d2f5','809fd2','786db4','a86eb1','ee709f','fa8b82','fca47e','fdc678','ffd778','fdf398','e9eb93','b3d78d',
						// '9cdef7','aec3e5','9a89c1','c295c5','f3a3c0','fcb3b0','fcc3ab','fedaa8','fee4a9','fdf6bb','f2f1b9','cfe5b5',
						// 'ceedf9','d7e1f3','d4c8e2','e4c9e1','f8d3e0','fcd1d0','fddbcd','fee8cc','fdefcb','fdfdd5','f6fad3','d9eaca'
						'4ED7C','F4ED47','F9E814','C6AD0F','AD9B0C','82750F','F7E859','F9E526','F9DD16','F9D616','D8B511','AA930A','99840A','F9E55B','F9E24C','F9E04C','FCD116','F7B50C','C6A00C','AA8E0A','897719','F9E27F','F7E8AA','F9E070','F9E08C','FCD856','FFCC49','FFC61E','FCB514','E0AA0F','BF910C','B58C0A','A37F14','A38205','7C6316','F4E287','F4DB60','F2D13D','EAAF0F','C6930A','9E7C0A','705B0A','FFD87F','FFD691','FCC963','FCCE87','FCBF49','FCBA5E','FCA311','F99B0C','D88C02','CC7A02','AF7505','996007','7A5B11','6B4714','F2CE68','F2BF49','EFB22D','E28C05','C67F07','9E6B05','725E26','FFD69B','FFB777','FCCC93','FF993F','FCAD56','F47C00','F77F00','DD7500','B55400','BC6D0A','8C4400','995905','4C280F','F4DBAA','F9BF9E','F2C68C','FCA577','EDA04F','FC8744','E87511','F96B07','C66005','D15B05','9E540A','A04F11','633A11','843F0F','F9C6AA','F9A58C','FC9E70','F98E6D','FC7F3F','F97242','F96302','EA4F00','F95602','DD5900','DD4F05','BC4F07','A53F0F','6D3011','843511','F9BAAA','F98972','F9603A','F74902','D14414','933311','6D3321','F9AFAD','F99EA3','F9B2B7','F9827F','F9848E','FC6675','F95E59','FC4F59','F43F4F','EF2B2D','E23D28','D62828','CC2D30','C13828','AF2626','A03033','7C2D23','7C211E','5B2D28','F9BFC1','FC8C99','FC5E72','E8112D','D11600','CE1126','AF1E2D','7C2128','FFA3B2','FCBFC9','FC758E','FC9BB2','F4476B','F4547C','E5053A','E00747','DB828C','C10538','992135','A80C35','931638','F4C9C9','EF99A3','772D35','D81C3F','C41E3A','A32638','8C2633','F2AFC1','ED7A9E','E54C7C','D30547','BAAA9E','8E2344','75263D','FFA0BF','FF77A8','F94F8E','EA0F6B','CC0256','A50544','7C1E3F','F4BFD1','ED72AA','E23282','AA004F','930042','70193D','F993C4','F46BAF','ED2893','D60270','AD005B','8C004C','6D213F','FFA0CC','FC70BA','F43FA5','CE007C','AA0066','8E0554','F9AFD3','F7C4D8','F484C4','EA6BBF','ED4FAF','DB28A5','E0219E','C4057C','C4008C','C40F89','A8007A','AD0075','9B0070','7C1C51','87005B','F2BAD8','EDA0D3','E87FC9','CC00A0','B7008E','A3057F','7F2860','EDC4DD','E29ED6','D36BC6','AF23A5','A02D96','772D6B','E5C4D6','D8A8D8','D1A0CC','BF93CC','D3A5C9','C687D1','BA7CBC','AA72BF','9B4F96','AA47BA','9E4FA5','8E47AD','72166B','930FA5','720082','872B93','66008C','681E5B','820C8E','70147A','5B027A','5E2154','701E72','66116D','560C70','542344','602D59','5B195E','4C145E','E0CEE0','C9ADD8','C6AADB','B591D1','9663C4','9B6DC6','6D28AA','894FBF','59118E','4F2170','56008C','442359','44235E','BAAFD3','AD9ED3','D1CEDD','BFD1E5','AFBCDB','9E91C6','937ACC','A5A0D6','A5BAE0','5B77CC','8977BA','7251BC','6656BC','5E68C4','3044B5','38197A','4F0093','4930AD','2D008E','2B1166','3F0077','3F2893','1C146B','1E1C77','260F54','35006D','332875','141654','192168','2B2147','2B0C56','2B265B','14213D','112151','B5D1E8','99BADD','6689CC','002B7F','002868','002654','9BC4E2','75AADB','3A75C4','0038A8','003893','00337F','002649','C4D8E2','93C6E0','A8CEE2','60AFDD','75B2DD','008ED6','0051BA','005BBF','003F87','0054A0','00386B','003D6B','002D47','00334C','82C6E2','BAE0E2','51B5E0','51BFE2','00A3DD','007FCC','00A5DB','0072C6','0084C9','005B99','00709E','004F6D','00546B','003F54','004454','A5DDE2','70CEE2','00BCE2','00A3D1','007AA5','00607C','003F49','72D1DD','7FD6DB','28C4D8','2DC6D6','00ADC6','00B7C6','0099B5','009BAA','00829B','00848E','006B77','006D75','00494F','00565B','C9E8DD','93DDDB','4CCED1','009EA0','007F82','008789','007272','006663','AADDD6','87DDD1','8CE0D1','7AD3C1','56C9C1','56D6C9','47D6C1','35C4AF','00B2AA','00C1B5','00C6B2','00AF99','008C82','008977','00AA9E','00B2A0','009B84','007770','008C82','009987','008270','006D66','006056','008272','006B5B','005951','00493F','004F42','004438','BAEAD6','A0E5CE','5EDDC1','00997C','007C66','006854','9BDBC1','8EE2BC','7AD1B5','54D8A8','00B28C','00C993','009977','00B27A','007A5E','007C59','006B54','006847','00563F','024930','B5E2BF','96D8AF','70CE9B','009E60','008751','006B3F','234F33','B5E8BF','99E5B2','84E2A8','00B760','009E49','007A3D','215B33','AADD96','A0DB8E','60C659','1EB53A','339E35','3D8E33','3A7728','D3E8A3','C4E58E','AADD6D','5BBF21','009E0F','56AA1C','568E14','566B21','D8ED96','CEEA82','BAE860','8CD600','54BC00','7FBA00','709302','566314','E0EA68','D6E542','CCE226','BAD80A','9EC400','A3AF07','939905','707014','E8ED60','E0ED44','D6E80F','CEE007','BAC405','9E9E07','848205','F2EF87','F2ED6D','EAED35','EFEA07','E5E811','EDE211','E0E20C','E8DD11','C1BF0A','B5A80C','AFA80A','998C0A','998E07','6D6002','D1C6B5','C1B5A5','AFA593','998C7C','827566','6B5E4F','CEC1B5','A8998C','99897C','7C6D63','66594C','3D3028','C6C1B2','B5AFA0','A39E8C','8E8C7A','777263','605E4F','282821','D1CCBF','BFBAAF','AFAAA3','96938E','827F77','60605B','2B2B28','DDDBD1','D1CEC6','ADAFAA','919693','666D70','444F51','30383A','0A0C11','E0D1C6','D3BFB7','BCA59E','8C706B','593F3D','493533','3F302B','D1D1C6','BABFB7','A3A8A3','898E8C','565959','494C49','3F3F38','54472D','604C11','544726','877530','60542B','A09151','ADA07A','BCAD75','C4B796','009944','CCBF8E','D6CCAF','DBCEA5','E2D8BF','E5DBBA','665614','998714','B59B0C','DDCC6B','E2D67C','EADD96','EDE5AD','5B4723','472311','755426','8C5933','876028','704214','B28260','C1A875','C49977','D1BF91','D8B596','DDCCA5','E5C6AA','E2D6B5','EDD3BC','603311','51261C','9B4F19','7C513D','BC5E1E','A34402','99705B','EAAA7A','B5917C','F4C4A0','CCAF9B','F4CCAA','D8BFAA','F7D3B5','E2CCBA','593D2B','633826','7A3F28','AF8970','D3B7A3','E0CCBA','E5D3C1','6B3021','9B301C','D81E05','CC0C00','ED9E84','EFB5A0','F2C4AF','F2D1BF','5B2626','752828','913338','F2ADB2','F4BCBF','F7C9C6','512826','441E1C','6D332B','844949','7A382D','A56B6D','CE898C','BC8787','EAB2B2','D8ADA8','F2C6C4','E2BCB7','F4D1CC','EDCEC6','511E26','661E2B','7A2638','D8899B','E8A5AF','F2BABF','F4C6C9','602144','4F213A','84216B','754760','9E2387','936B7F','D884BC','AD8799','E8A3C9','CCAFB7','F2BAD3','E0C9CC','F4CCD8','E8D6D1','512D44','472835','63305E','593344','703572','8E6877','B58CB2','B5939B','C6A3C1','CCADAF','D3B7CC','DDC6C4','E2CCD3','E5D3CC','512654','35264F','68217A','493D63','7A1E99','605677','AF72C1','8C8299','CEA3D3','B2A8B5','D6AFD6','CCC1C6','E5C6DB','DBD3D3','353842','353F5B','3A4972','9BA3B7','ADB2C1','C4C6CE','D6D3D6','003049','02283A','00335B','3F6075','003F77','607C8C','6693BC','8499A5','93B7D1','AFBCBF','B7CCDB','C4CCCC','C4D3DD','D6D8D3','0C3844','00353A','193833','003F54','26686D','3A564F','004459','609191','667C72','5E99AA','8CAFAD','91A399','87AFBF','AAC4BF','AFBAB2','A3C1C9','CED8D1','C9CEC4','C4D6D6','D6DDD6','CED1C6','234435','213D30','195E47','4F6D5E','076D54','779182','7AA891','96AA99','A3C1AD','AFBFAD','B7CEBC','C4CEBF','C6D6C4','D8DBCC','2B4C3F','233A2D','266659','546856','1E7A6D','728470','7FBCAA','9EAA99','05705E','BCC1B2','BCDBCC','C6CCBA','D1E2D3','D6D6C6','265142','008772','7FC6B2','AADBC6','BCE2CE','CCE5D6','495928','3F4926','424716','547730','5E663A','6B702B','608E3A','777C4F','8C914F','B5CC8E','9B9E72','AAAD75','C6D6A0','B5B58E','C6C699','C9D6A3','C6C6A5','D3D1AA','D8DDB5','D8D6B7','E0DDBC','605E11','494411','878905','75702B','AABA0A','9E9959','CED649','B2AA70','DBE06B','CCC693','E2E584','D6CEA3','E8E89B','E0DBB5','F4EDAF','F2ED9E','F2EA87','EDE85B','E8DD21','DDCE11','D3BF11','F2EABC','EFE8AD','EAE596','E2DB72','D6CE49','C4BA00','AFA00C','EAE2B7','E2DBAA','DDD69B','CCC47C','B5AA59','968C28','847711','D8DDCE','C1D1BF','A5BFAA','7FA08C','5B8772','21543F','0C3026','CCE2DD','B2D8D8','8CCCD3','54B7C6','00A0BA','007F99','00667F','BAE0E0','99D6DD','6BC9DB','00B5D6','00A0C4','008CB2','007AA5','D1D8D8','C6D1D6','9BAFC4','7796B2','5E82A3','26547C','00305E','D6D6D8','BFC6D1','9BAABF','6D87A8','335687','0F2B5B','0C1C47','D6DBE0','C1C9DD','A5AFD6','7F8CBF','5960A8','2D338E','0C1975','E2D3D6','D8CCD1','C6B5C4','A893AD','7F6689','664975','472B59','F2D6D8','EFC6D3','EAAAC4','E08CB2','D36B9E','BC3877','A00054','EDD6D6','EACCCE','E5BFC6','D39EAF','B7728E','A05175','7F284F','EFCCCE','EABFC4','E0AABA','C9899E','B26684','934266','702342','EFD1C9','E8BFBA','DBA8A5','C98C8C','B26B70','8E4749','7F383A','F7D1CC','F7BFBF','F2A5AA','E8878E','D6606D','B73844','9E2828','F9DDD6','FCC9C6','FCADAF','F98E99','F26877','E04251','D12D33','FFD3AA','F9C9A3','F9BA82','FC9E49','F28411','D36D00','BF5B00','F4D1AF','EFC49E','E8B282','D18E54','BA7530','8E4905','753802','EDD3B5','E2BF9B','D3A87C','C18E60','AA753F','723F0A','60330A','00AACC','0089AF','60DD49','1CCE28','FFED38','FFD816','FF9338','FF7F1E','F95951','F93A2B','FF0093','F7027C','D6009E','BF008C','00B59B','00A087','DDE00F','D6D60C','FFCC1E','FFBC21','FF7247','FF5416','FC2366','FC074F','E50099','8C60C1','703FAF'];


config.preloadImages = ['images/ajax-loader.gif', 'images/ajax-loader-s.gif' ] ;
$.each(config.preloadImages, function(k,v){
	(new Image()).src = v ;
});


function proceedToNextPage(){
	$('#gray_box').click();
	
	if (! view.isDesignSaved || ! view.designId) {
		view.saveAsName = view.designName ; 
		saveDesign(proceedToNextPage, false) ;
		return 
	}
	window.location = config.nextPageUrl + '?design_id=' + view.designId ;
}

function downloadPdfProof(){
	if 	(user.customer_id == null) {
		popupLoginHandler(downloadPdfProof) ; 
		return ;
	}	

	if (!user.download_pdf) return ;
	$('#pdf_proof_json').val(JSON.stringify(view.jdesign) );
	$('#pdf_proof_form').submit();
	
	
}
function shareOnFacebook() {
	//alert('shareOnFacebook');
	$.ajax({
		url : "/ajax/design/saveForShare.php"  , 
		type: 'post' , 
		data: {'json' : JSON.stringify(view.jdesign) , 'template_id' : view.templateId  } ,
		dataType: 'json' , 
		success: function(data){
			if(! data.success){
				showAlert('An error occured while sharing your design. Please try again.') ;
				return;
			}
				
			var obj = {
				method: 'feed',
				link: config.homeUrl+'design/' + data.token ,
				picture: config.homeUrl+'ajax/raster/getDesignThumb.php?long_side=150&design_id=' + data.design_id ,
				name: 'Check out the ' + products[view.jdesign.product].title + ' I created on gotoCloudPrint.com!' ,
//				caption: '',
				description: 'On gotoCloudPrint.com you can customize and order thousands of designs from Business Cards and Stationary to Flyers, Banners and Postcards.'
			};
		
			FB.ui(obj, function(data){console.log(data)});
			
		} , 
		error: function(){
				showAlert('An error occured. Please try again.') ;
		}

		
	})
}

function emailToFriend(){
	if(!view.designId){
        alert("Please save your design before sharing!");
        return;
    }
	if 	(user.customer_id == null) {
		//popupLoginHandler(emailToFriend) ;
		//return ;
	}
	$('#gray_box').show();
	var popup = createPopup('Email this Design to a Friend' , 500).show();
	var c = popup.find('.simple-popup-content') ;
	$('<div><b>Your Name: </b><br /><input placeholder="Your Name here" id="share_design_sender_name" type="text" style="width: 100%" /></div><br />').appendTo(c);
	$('<div><b>Your friend\'s email address: </b><br /><input placeholder="Received Email here" id="share_design_receiver_email" type="text" style="width: 100%" /></div><br />').appendTo(c);
	$('<div><b>Optional Message: </b><br /><textarea placeholder="Optional Message here!" id="share_design_message" style="width: 100%" ></textarea></div>').appendTo(c);
	$('<br /><div style="text-align: right"><button id="share_design_send_button" class="action-button" >Send</button></div>').appendTo(c);
	//$('#share_design_sender_name').val(user.first_name + ' ' + user.last_name) ;
    $('#share_design_sender_name').val('') ;

	$('#share_design_send_button').click(function(){
		if(! $(this).hasClass('disabled') )
			sendEmailToFriend() ;
	});
}

function sendEmailToFriend(){
	//alert('sendEmailToFriend');
	if(! validateEmail( $.trim ($('#share_design_receiver_email').val() ))){
		alert('Invalid Email address'); 
		$('#share_design_receiver_email').css('border-color' , '#f77').focus() ;
		return ; 
	}
	$('#share_design_send_button').html('Please wait...').addClass('disabled');
	$.ajax({
		url : "email_to_friend.php"  ,
		type: 'post' ,
        data: {
            'design_id' : view.designId  ,
            'sender_name' : $('#share_design_sender_name').val() ,
            'receiver_email' : $('#share_design_receiver_email').val() ,
            'message' : $('#share_design_message').val() } ,
		/*
        data: {'json' : JSON.stringify(view.jdesign) ,
				'template_id' : view.templateId  , 
				'sender_name' : $('#share_design_sender_name').val() , 
				'receiver_email' : $('#share_design_receiver_email').val() , 
				'message' : $('#share_design_message').val() } ,
        */
		dataType: 'json' ,

		success: function(data){
			$('#gray_box').click();
			if(data.success)
				showNotice('Succesfully Sent!'); 
			else 
				showAlert('An error occured while sending the email. Please try again.');
		} ,

		error: function(){
			$('#gray_box').click();
			showAlert('An error occured while sending the email. Please try again.'); 
		}
		
	});
}

function lockElement(){
	var el = getElem($('.selected').attr('id').split('outline_')[1] ) ;
	el.locked = true ; 
	$('.selected').addClass('locked').removeClass('selected'); 
	arrangeOutlineZindex() ;
	updateToolbar();
}

function replacePhImage(){
	showAddImagePopup($('.selected.croppable').attr('id').split('outline_img_').pop().replace(/(^user_|^admin_)/ , '') )
	deselectAll();
}
function zoomInInnerImage(){
	var v = $('#crop_slider').slider('value') ;  
	scaleInnerImage( Math.max(v + 0.05, $('#crop_slider').slider('option','min') )); 
	updateJdesign(); 
	adjustcropSlider() ;
}

function zoomOutInnerImage(){
	var v = $('#crop_slider').slider('value') ;  
	scaleInnerImage( Math.max(v - 0.05, $('#crop_slider').slider('option','min') )); 
	updateJdesign(); 
	adjustcropSlider() ;
}

function moveInnerImage(dir){
	var imgId = $('.selected.croppable').attr('id').split('outline_').pop() ;
	var inner = $('#inner_' + imgId);
	var outline = $('#outline_' + imgId);
	switch(dir){
		case 'L':
			inner.cssLeft(inner.cssLeft() - 10 );
			break;
		case 'R':
			inner.cssLeft(inner.cssLeft() + 10 );
			break;
		case 'U':
			inner.cssTop(inner.cssTop() - 10 );
			break;
		case 'D':
			inner.cssTop(inner.cssTop() + 10 );
			break;
	}

	inner.cssLeft( Math.max ( outline.cssWidth() - inner.cssWidth()   , inner.cssLeft() )) ;
	inner.cssLeft( Math.min ( 0 , inner.cssLeft() )) ;

	inner.cssTop( Math.max ( outline.cssHeight() - inner.cssHeight()   , inner.cssTop() )) ;
	inner.cssTop( Math.min ( 0 , inner.cssTop() )) ;
	
	view.outdatedJdesign = true ;
	updateJdesign();	
	
}

function adjustcropSlider(){
	var jimage = getElem( $('.selected.croppable').attr('id').split('outline_')[1]);
	var cropWidth = typeof jimage.cropWidth == 'undefined' ? 1 : jimage.cropWidth ;
	var cropHeight = typeof jimage.cropHeight == 'undefined' ? 1 : jimage.cropHeight ;
	var sliderValue = Math.min(1 / cropWidth, 1 / cropHeight) ;
	$('#crop_slider').slider('value' , sliderValue) ;
}

function scaleInnerImage(value){
	view.outdatedJdesign = true ;
	var imgName = $('.selected.croppable').attr('id').split('outline_img_')[1] ;

	var jimage = getElem('img_'+imgName);
	var inner = $('#inner_img_' + imgName);
	var outline = $('#outline_img_' + imgName);
	
	var innerRatio = view.imageInfo[jimage.image_id].uploadWidth  / view.imageInfo[jimage.image_id].uploadHeight  ;
	innerRatio = (jimage.rotation % 180 == 0) ? innerRatio  :  1 / innerRatio  ;
	

	var outlineRatio = outline.cssWidth()  / outline.cssHeight() ;
	
	if (innerRatio < outlineRatio) {
		var newW = outline.cssWidth() * value ;
		var newH = newW /  innerRatio  ; 
	}
	else {
		var newH = outline.cssHeight() * value  ;
		var newW = newH * innerRatio  ; 
	}

	inner.cssWidth(newW) ; 
	inner.cssHeight(newH) ; 

	inner.cssLeft( Math.max (Math.min(- inner.cssWidth() * jimage.cropLeft , 0), outline.cssWidth() - inner.cssWidth() )) ; 
	inner.cssTop( Math.max (Math.min(- inner.cssHeight() * jimage.cropTop , 0), outline.cssHeight() - inner.cssHeight()))  ; 

}

function rotateInnerImage(degree){
	var imgId = $('.selected.croppable').attr('id').split('outline_').pop() ;
	var jimage = getElem(imgId);
	var inner = $('#inner_' + imgId);
	var outline = $('#outline_' + imgId);
	outline.addClass('loading');
	
	var temp = inner.cssWidth();
	inner.cssWidth(inner.cssHeight()) ;
	inner.cssHeight(temp);
	jimage.rotation = (jimage.rotation + degree) % 360; ;
	scaleInnerImage(1);
	view.outdatedJdesign = true ;
	updateJdesign();
	inner.attr('src', getMockupSrc(jimage))
			.load(function(){
				outline.removeClass('loading');
			});
	
}

function applySvgColor(color){
	var element_id = $('.selected.svg').attr('id').split('outline_')[1] ;
	var jElement = getElem(element_id) ;
	jElement.svg_colors[$('.svg-color-item.on').index()] = color ; 
	$('#inner_'+element_id).attr('src' ,  getMockupSrc(jElement) ) ;
	view.outdatedJdesign = true ;
	pushState();
	updateToolbar();
}

function addLogoToDesign(){
	showAddImagePopup('logo');
}

function showAddImagePopup(ph_name){
	view.addImagePh = ph_name;
    /*
	if 	(user.customer_id == null && user.visitor_id == null ) {
		popupLoginHandler( function() {showAddImagePopup(ph_name)} ) ; 
		return false;
	}
	
	if(user.admin_mode){
		admin.showImagePanel();
		return;
	}
    */
    //alert($('#add_image_popup').length);
	if($('#add_image_popup').length == 0){
		$('#gray_box').show();
		//$('<iframe />' , {'src': '/panels/addImage.php' , 'frameborder' : 0}).attr('id' , 'add_image_popup').addClass('iframe-popup').appendTo('body').show() ;
        $('<iframe />' , {'src': 'tools.php?action=add_image' , 'frameborder' : 0}).attr('id' , 'add_image_popup').addClass('iframe-popup').appendTo('body').show() ;
	}
	else {
		$('#gray_box , #add_image_popup').show();		
	}

	 $('#add_image_popup')[0].contentWindow.asset_name = (ph_name ? ph_name  : '') ; 
	 $('#add_image_popup').contents().find('#user_tab').click();
	$('body').css('overflow' , 'hidden');
} 

function cancelAddImage(){
	$('#temp_text').focus();
	$('#gray_box , #add_image_popup').hide();
//	$('#temp_text').blur();
	$('body').css('overflow' , 'visible');
}


function useImage(image){
	updateSingleImageInfo(image);
	if(typeof view.addImagePh == 'undefined'){
		placeDroppedImage({} , {} , image.id) ;
    }else{
		putImageInPlaceholderFamily(view.addImagePh, image.id) ;
    }
	cancelAddImage();
}

function loginAndShowAddImagePopup(){
	popupLoginHandler(function(){showAddImagePopup(view.addImagePh)}) ;
}

function placeDroppedImage(e , ui, image_id) {
	if(typeof ui != 'undefined' && typeof ui.helper != 'undefined' )
		ui.helper.hide()  ; 

	if($.browser.msie){
		$('.ui-effects-wrapper').remove() ; 
		$('.tooltip').remove() ; 
	}

	
	if (typeof image_id == 'undefined')
		var image_id = ui.draggable.attr('id').split('user_image_thumb_')[1] * 1;
	
	// In case the the image is dopped on the main canvas not on a placeholder
	if (typeof $(this).attr('id') == 'undefined' ||  $(this).attr('id') == 'droppable_layers' ) {

		var	im = { 	
			"image_id" :  image_id ,
			"name": "image_" + Math.floor(Math.random() * 100000 ),
			"zindex": 299 ,
			"rotation" : 0,
			"fixedratio" : true
		}

		var imgRatio = view.imageInfo[image_id].uploadWidth / view.imageInfo[image_id].uploadHeight ;
		
		var can = view.jdesign.canvases[view.currentPage] ; 

		if(typeof e != 'undefined' && typeof e.pageX != 'undefined'){ // Image is dropped 
			im.width = Math.min(can.width / 2 , can.height / 2 * imgRatio);
			im.height = im.width / imgRatio  ;
			im.left = (event.pageX - getCanvasX() ) / view.dpi - im.width / 2 ;
			im.top = (event.pageY - getCanvasY() ) / view.dpi - im.height / 2 ;
		}
		else {
			im.width = Math.min(can.width / 2 , can.height / 2 * imgRatio);
			im.height = im.width / imgRatio;
			im.left = (can.width - im.width) / 2 ; 
			im.top = (can.height - im.height) / 2 ;
		}

		if (typeof view.imageInfo[image_id].fotoliaId != 'undefined'){
			im.fotoliaId = view.imageInfo[image_id].fotoliaId ;
			im.fotoliaLicense = view.fotoliaLicenses[im.fotoliaId][0].name ;
		}

		if (typeof view.imageInfo[image_id].svg_id != 'undefined'){
			im.svg_id = view.imageInfo[image_id].svg_id  ;
			im.svg_colors = view.imageInfo[image_id].svg_original_colors ;
			im.fixedratio = false ;
		}

		view.jdesign.canvases[view.currentPage].images.push(im) ; 
		drawImageElement(im) ;
		arrangeImageZees() ;
		updateStockImagePrices();
		pushState() ; 
		$('#outline_img_'+im.name).mousedown();
		$('#outline_img_'+im.name).mouseup();
	}
	
	else {
		// In case that the image is dropped in a placeholder
		var phName = $(this).attr('id').split('droppable_')[1] ;
		putImageInPlaceholder(phName , image_id ) ; 
		$('#outline_img_user_' + phName).mousedown();
		$('#outline_img_user_' + phName).mouseup();
	}

	
}

function putImageInPlaceholderFamily(ph_name, image_id){
	var count = 0 ;
	var pattern = new RegExp("^"+ph_name+"(-[0-9]+|)$") ;
	$.each(view.jdesign.canvases[view.currentPage].images, function(k2, i) {
		if(pattern.test(i.name)){
			if(i.placeholder){
				putImageInPlaceholder(i.name , image_id) ;
				count++;
			}
		}
	});

	if(count == 0)
		placeDroppedImage({} , {} , image_id) ;
}


function putImageInPlaceholder(ph_name, image_id){
	var originalJPlace = getElem('img_' + ph_name) ;
	var jPlaceAdminImage = getElem('img_admin_' + ph_name) ;

	var jPlace = JSON.clone(originalJPlace) ; 
	var jPlaceRatio = jPlace.width  / jPlace.height  ;

	//jPlace.invisible
	deleteImageElementByName( ((user.admin_mode) ? 'admin' : 'user') +'_'+  ph_name) ;
	
	im = {} ; 
	im.image_id =  image_id ;
	im.name = ((user.admin_mode) ? 'admin' : 'user') + '_' + ph_name ;
	//im.zindex = originalJPlace.zindex + .1 ;
	im.zindex = originalJPlace.zindex + 10 ;
	
	im.type = originalJPlace.type ;
	im.locked = originalJPlace.locked ;

	// There should be an additional parameter to detemine if border should be transferred from placeholder or not.
	if (originalJPlace.border_width){
		im.border_width = originalJPlace.border_width ;
		im.border_color = originalJPlace.border_color ;
	}
	
	im.rotation = 0 ; 
	im.fixedratio = true ;
	im.noselect = false ;
	droppedImageRatio = view.imageInfo[image_id].uploadWidth / view.imageInfo[image_id].uploadHeight ;

	if (typeof view.imageInfo[image_id].fotoliaId != 'undefined'){
		im.fotoliaId = view.imageInfo[image_id].fotoliaId ;
//		im.fotoliaLicense = view.imageInfo[image_id].licenses[0].name ;
		im.fotoliaLicense = null ; // view.fotoliaLicenses[fotoliaId].name 
	}

	if (jPlace.placeholder == 'filler') {
		im.width = jPlace.width ;
		im.height = jPlace.height ; 
		im.left = jPlace.left ; 
		im.top = jPlace.top ; 
		im.crop = true ;
		im.unmovable = true ;
		im.noresize = true ;
		im.fitted = true ;

		if (droppedImageRatio >  jPlaceRatio) {
				uncroppedWidth =  jPlace.height * droppedImageRatio ;
				im.cropLeft = (uncroppedWidth - jPlace.width ) / uncroppedWidth / 2 ;
				im.cropWidth = jPlace.width / uncroppedWidth ;
				im.cropHeight = 1 ; 
				im.cropTop = 0 ; 
		}
		else {
				uncroppedHeight =  jPlace.width / droppedImageRatio ;
				im.cropTop = (uncroppedHeight - jPlace.height ) / uncroppedHeight / 2 ;
				im.cropHeight = jPlace.height / uncroppedHeight ;
				im.cropWidth = 1 ; 
				im.cropLeft = 0 ;		
		}
	}
	else  if (jPlace.placeholder  == 'fitter' ) {
		if (droppedImageRatio >  jPlaceRatio) {
			im.width = (jPlace.width  + jPlace.height * droppedImageRatio) / 2  ;
			im.height = im.width / droppedImageRatio  ;
			im.left = jPlace.left  ;
			im.top = (jPlace.height -  im.height) / 2 +  jPlace.top ;
		}
		else {
			im.height = (jPlace.height + jPlace.width / droppedImageRatio) / 2 ;
			im.width = im.height * droppedImageRatio  ;
			im.top = jPlace.top ;
			im.left = (jPlace.width  -  im.width) / 2 +  jPlace.left  ;
		}
	}
	
	view.jdesign.canvases[view.currentPage].images.push(im) ; 
	drawImageElement(im) ;
	
	// Makes the placeholder invisble after the image is dropped. We make is visible again in case user removes the dropped image.
	if(! user.admin_mode){
		originalJPlace.invisible = true ;
		$('#img_' + originalJPlace.name).remove() ; 
		$('#outline_img_' + originalJPlace.name).remove() ; 
		
		if(jPlaceAdminImage){
			jPlaceAdminImage.invisible = true ;
			$('#img_' + jPlaceAdminImage.name).remove() ; 
			$('#outline_img_' + jPlaceAdminImage.name).remove() ; 
		}
	}

	updateStockImagePrices(); 
	arrangeImageZees() ;
	pushState() ;
	
	$('#outline_img_' + im.name).mousedown().mouseup();
} 
 

function applyThemeToToolbar(theme){
		$('#selected_theme').remove();
		if(theme){
			view.theme_id = theme.id ;
			$(".color-palette").each(function(){
				$(this).find('.theme-colors').remove();
				var th = $('<div />').addClass('theme-colors').prependTo($(this));
				$.each(theme.colors, function(k,v){
					$('<div />').addClass('color-pick').addClass('color-'+v+'_'+k).css('background-color' , '#' + v ).css('width', 100 / theme.colors.length - 1 + '%'  ).data('color' , v).data('color_index' , k ).appendTo(th);
				});
			});
			getThemeThumb(theme).attr('id', 'selected_theme').prependTo('#theme_options');
		}
}

function applyThemeToDesign(theme){
		view.theme_id = theme.id ;
    //alert(theme.id);
        if(theme.bg_images){
            for(var i=0; i<view.jdesign.canvases.length; i++){
                for(var j=0; j<view.jdesign.canvases[i].images.length;j++){
                    if(view.jdesign.canvases[i].images[j].image_id+''!='undefined'){
                        var image_id = view.jdesign.canvases[i].images[j].image_id;
                        image_id = parseInt(image_id, 10);
                        var found = -1;
                        var k = 0;
                        while(k<theme.bg_images.length && found==-1){
                            if(image_id == theme.bg_images[k]) found = k;
                            k++;
                        }
                        if(found>=0) {
                            view.jdesign.canvases[i].images[j].image_id = theme.bg_images[0];
                        }
                    }
                }
            }
        }
		var jsons = JSON.stringify(view.jdesign);


		var matches = jsons.match(/[0-9a-f]{6}#[0-9]/gi) ;
		if(! matches) return ; 
		$.each(matches , function(k,v) {
			var colorIndex = v.split('#')[1] ; 
			var regex = new RegExp( v , 'gi' ) ; 
			jsons = jsons.replace( regex , theme.colors[colorIndex] + '#' + colorIndex);
			
		});
		view.jdesign = $.parseJSON(jsons) ;
		applyThemeToToolbar(theme);

		pushState();
		refreshView();
}


function drawImageElement(newJimage) {
	//var imageClone = JSON.clone(newJimage);
	
	// In case of re-draw the original image and outline should be deleted first
	if ($('#img_' + newJimage.name).length > 0 ) {
		$('#img_' + newJimage.name).remove() ; 
		$('#outline_img_' + newJimage.name).remove() ; 
	}
	
	if(newJimage.invisible && ! user.admin_mode){ return; }

	// limiting SVG colors to 15
	if(newJimage.svg_colors){
		newJimage.svg_colors = newJimage.svg_colors.slice(0, config.maxSvgColorSupport );
	}
	
	var cropWidth = newJimage.crop ? newJimage.cropWidth : 1 ;
	var cropHeight = newJimage.crop ? newJimage.cropHeight : 1 ;
	var cropTop = newJimage.crop ? newJimage.cropTop : 0 ;
	var cropLeft = newJimage.crop ? newJimage.cropLeft : 0 ;
	
	var newImgMask = $('<div />').attr('id' , 'img_' + newJimage.name )
				.addClass('image-mask')
				.cssLeft(newJimage.left * view.dpi)
				.cssTop(newJimage.top * view.dpi )
				.cssWidth(newJimage.width * view.dpi)
				.cssHeight(newJimage.height * view.dpi)
				.css('z-index' , newJimage.zindex )
				.appendTo('#' + (newJimage.type == 'overlay' ? 'overlay_layers' :'image_layers')) ;
				
	if (newJimage.image_id){	
		var newImgBorder = $('<div />').attr('id' , 'border_img_' + newJimage.name )
										.addClass('image-border')
										.appendTo(newImgMask) 
	
		if(newJimage.border_width){
			newImgBorder.css('border-width' ,  .013836 * newJimage.border_width * view.dpi + 'px');
			newImgBorder.css('border-color' , '#' + newJimage.border_color.split('#')[0]);
		}
	}
	var newImg = $('<img />').attr('id' , 'inner_img_' + newJimage.name )

				.cssLeft(- cropLeft * newJimage.width / cropWidth * view.dpi )
				.cssTop(- cropTop * newJimage.height / cropHeight * view.dpi ) 
				.cssHeight(newJimage.height / cropHeight * view.dpi)
				.cssWidth(newJimage.width / cropWidth * view.dpi)
				.css('z-index' , '1' )
				.css('display' , 'none')
				.appendTo(newImgMask) ;
				 
	// ------------------ Inserting Outlines for Image Elements ----------------
	if ($('#outline_img_' + newJimage.name).length > 0 )
		$('#outline_img_' + newJimage.name).remove() ; 

	var outline = $('<div />').addClass('outline2 loading')
					.attr('id' , 'outline_img_' + newJimage.name)
					.cssLeft (newJimage.left * view.dpi)
					.cssTop (newJimage.top * view.dpi)
					.cssWidth(newJimage.width * view.dpi)
					.cssHeight(newJimage.height * view.dpi)
					.css('z-index' ,  newJimage.zindex ) 
					.addClass(newJimage.noselect ? 'non-selectable' : '' )
					.addClass(newJimage.unmovable ? 'unmovable' : '' )
					.addClass(newJimage.locked ? 'locked' : '' )
					.addClass(newJimage.noresize ? 'non-resizable' : '' )
					.addClass(newJimage.fixedratio ? 'fixed-ratio' : '')
					.addClass(newJimage.placeholder ? 'pholder' : '')
					.addClass(newJimage.noprint ? 'noprint' : '')
					.addClass(newJimage.fitted ? 'fitted croppable' : '')
					.addClass(newJimage.svg_id ? 'svg' : '')
					.addClass(newJimage.image_id ? 'image' : '')
					.addClass(newJimage.shape_type ? 'shape' : '')
					.addClass((newJimage.placeholder && ! user.admin_mode )? 'non-resizable unmovable' : '')
					.addClass(newJimage.image_id && view.imageInfo[newJimage.image_id].is_public ? 'public' : 'private')
					.addClass(user.admin_mode ? 'admin' : 'user')
					.appendTo('#outline_layers') ; 
					
					if (newJimage.name.search(/^admin/) == 0  && ! user.admin_mode) {
						outline.addClass('non-selectable') ; 
//						newJimage.noprint = true ;
					}
	$('<div />').addClass('selected-dash l').appendTo(outline);
	$('<div />').addClass('selected-dash t').appendTo(outline);
	$('<div />').addClass('selected-dash b').appendTo(outline);
	$('<div />').addClass('selected-dash r').appendTo(outline);
	
	var smw = $('<div />').addClass('size-measure w').appendTo(outline)
	$('<div />').addClass('value').appendTo(smw).html(newJimage.width.toFixed(2) + '&quot;' );
	$('<div />').addClass('guide').appendTo(smw);


	var smh = $('<div />').addClass('size-measure h').appendTo(outline);
	$('<div />').addClass('value').appendTo(smh).html(newJimage.height.toFixed(2) + '&quot;' );
	$('<div />').addClass('guide').appendTo(smh);


//	$('<div />').addClass('position-measure').appendTo(outline).html('X: ' + (newJimage.left - view.jdesign.canvases[view.currentPage].bleed).toFixed(2) + '&quot; , Y: ' + (newJimage.top - view.jdesign.canvases[view.currentPage].bleed).toFixed(2) + '&quot;' );
	
	if(! outline.hasClass('pholder')){		
		$('<div />').addClass('outline-hint').html('Click to Select').appendTo(outline);
		$('<div />').addClass('lock-hint').html('Double-click<br />to Unlock').appendTo(outline);
	}

	if (outline.hasClass('pholder') && ! outline.hasClass('admin')){
			$('<div />').addClass('outline-hint').html(newJimage.noprint ? 'Click to Insert Image' : 'Click to Replace Image').appendTo(outline);
			outline.click(function(){showAddImagePopup(newJimage.name)}) ;
	}
 
	
	
	// --- add nw, ne, sw, se handles for all elements excepts those that are not resizable 

		$('<div />').addClass('handle handle-nw').appendTo(outline) ; 	
		$('<div />').addClass('handle handle-ne').appendTo(outline) ; 	
		$('<div />').addClass('handle handle-sw').appendTo(outline) ; 	
		$('<div />').addClass('handle handle-se').appendTo(outline) ; 	

	// ----- Does not put N S W E handles for elements with fixed aspect ratio -------------
	if ( ! newJimage.noresize) {
		$('<div />').addClass('handle handle-w').appendTo(outline) ; 
		$('<div />').addClass('handle handle-e').appendTo(outline) ; 
		$('<div />').addClass('handle handle-n').appendTo(outline) ; 
		$('<div />').addClass('handle handle-s').appendTo(outline) ; 	
	}
	
	outline.bind("contextmenu", function(e) {
//		$('#inline_menu_img_' + newJimage.name).trigger('mousedown');
//		return false;
	}); 
	
	newImg.attr('src' , getMockupSrc(newJimage) ) ; 

	if ( document.getElementById('inner_img_' + newJimage.name).complete) 
	{
		$('#inner_img_' + newJimage.name).show() ;
		outline.removeClass('loading') ; 
	}
	else 
		document.getElementById('inner_img_' + newJimage.name).onload = function() { 
																			$(this).show() ;
																			$('#outline_' + $(this).attr('id').split('inner_')[1]).removeClass('loading') ; 
																		} ;

	if(newJimage.fotoliaId){
		$('<div />').addClass('stock-image-price-outline')
					.html('Price')
					.appendTo(outline) ; 
		
		updateFotoliaLicense(newJimage) ;
	}
	else if(newJimage.image_id){
		checkImageResolution(newJimage) ;
	}

}


function adjustProfileEQ(product){
	view.profileEQ = JSON.clone(config.urlProfileEQ) ;
	if(products[product].profile != null){
		for (var i in view.profileEQ){
			if (products[product].profile[i] != null)
			view.profileEQ[i] = products[product].profile[i] ;
		}
	}
}

function getProductDesc(){
	var desc = '';
	if(view.jdesign.product == 'foldedGreetingCard'){
		desc += products[view.jdesign.product].name + ' ';
		desc += view.jdesign.width + '&quot; x ' + view.jdesign.height + '&quot; ';
		desc += '(folds to '+(view.jdesign.canvases[0].width - (view.jdesign.canvases[0].bleed * 2))+'&quot; x '+(view.jdesign.canvases[0].height - (view.jdesign.canvases[0].bleed * 2))+'&quot;) <br>';
		desc += (view.jdesign.width > view.jdesign.height) ? 'Vertically folded' : 'Horizontally folded' ;
	}
	else {
		desc += view.jdesign.width + '&quot; x ' + view.jdesign.height + '&quot; ';
		if(view.jdesign.dieCutType != 'none') desc += getDiecutName(view.jdesign.dieCutType) + ' ';
		//if(view.jdesign.folding != 'none') desc += getFoldingName(view.jdesign.folding) + ' ';
        if(view.jdesign.folding != 'none') desc += view.jdesign.folding + ' ';
		desc += products[view.jdesign.product].title;
	}
	
	return desc ;
}

function resetView(){

	view = {} ;  // general namespace
	view.isDesignSaved = true ;
	view.callbacks = [] ; 
	view.textInsertCount = 0 ; 
	view.clipboard = [];
	view.zoomLevel = 0 ;
	view.outdatedJdesign = false ; 
	view.stateIndex = 0 ;
	view.states = [];
	view.customFontSizes = [];
	view.imageInfo = {} ; 
	view.fotoliaLicenses = {} ; 
	view.snapEnabled = true ;
	view.duplicate = getFragmentValue('dup') == 1 ; 
	view.showAdvancedToolbar = true ;
	view.showBleed = !! user.admin_mode ; 
	view.showSafeZone = false ; 
	view.showRuler = ! false ; 
	view.showGrid = false ;
	view.userTexts = [];
	view.userImages = [];
	view.hSnapBuffer = [];
	view.vSnapBuffer = [];
	view.fontSizes = [];
    view.saveAction = "";
	$('.theme-colors').remove();
}
function zoomOut(){
	// The min zoom level is -1
	if (view.zoomLevel <= config.minZoomLevel ) return ; 
	view.zoomLevel -=  1 ;  
	refreshView(); 
}

function zoomIn(){
	// The max zoon level is 6
	if (view.zoomLevel >= config.maxZoomLevel) return ; 
	view.zoomLevel += 1 ;  
	refreshView(); 
}

function distributeElements(direction) {
		
	// Setting the intial value for minLeft, minTop, MaxRight, maxBottom
	var firstElement = $('.selected').not('.unmovable').eq(0) ; 
	var minLeft = firstElement.cssLeft();
	var minTop = firstElement.cssTop() ; 
	var maxRight = firstElement.cssLeft() + firstElement.cssWidth()  ; 
	var maxBottom  = firstElement.cssTop() + firstElement.cssHeight()  ; 
	var elementsTotalHeight = elementsTotalWidth = 0 ;
	var elementsArray = [];
	// Finding the final value of above variables
	$('.selected').not('.unmovable').each(function() {
			if ($(this).cssLeft() < minLeft) 
				minLeft = $(this).cssLeft() ; 
			
			if ($(this).cssTop() < minTop) 
				minTop = $(this).cssTop() ; 
			
			if ($(this).cssLeft() + $(this).cssWidth() > maxRight) 
				maxRight = $(this).cssLeft() + $(this).cssWidth()  ; 
			
			if ($(this).cssTop() + $(this).cssHeight() > maxBottom ) 
				maxBottom  = $(this).cssTop() + $(this).cssHeight()  ; 
				
			elementsTotalHeight += $(this).cssHeight() ;
			elementsTotalWidth += $(this).cssWidth() ;
				
			elementsArray.push($(this)) ; 
			
	}) ; 
	

	
	if (direction == 'V') {
		elementsArray.sort(function(a, b){ return a.cssTop() - b.cssTop(); } ) ;
		var d = ((maxBottom - minTop) - elementsTotalHeight) /  	($('.selected').not('.movable').length - 1);
		var nextPosition = minTop ; 

		for (var i in elementsArray) {
			(elementsArray[i]).cssTop(nextPosition) ; 
			nextPosition += elementsArray[i].cssHeight() + d ;
		}
	}
	
	else if (direction == 'H') {
		elementsArray.sort(function(a, b){ return a.cssLeft() - b.cssLeft(); } ) ;
		var d = ((maxRight - minLeft) - elementsTotalWidth) /  	($('.selected').not('.movable').length - 1);
		var nextPosition = minLeft ; 

		for (var i in elementsArray) {
			(elementsArray[i]).cssLeft(nextPosition) ; 
			nextPosition += elementsArray[i].cssWidth() + d ;
		}

	}
	
	// Apply the new outline positions to the main elements
	$('.selected').each(function() {
			$('#' +	$(this).attr('id').split('outline_')[1]).cssTop($(this).cssTop()) ;
			$('#' +	$(this).attr('id').split('outline_')[1]).cssLeft($(this).cssLeft()) ;
	});

//	$('#align_control').removeClass('open') ; 
	view.outdatedJdesign = true ;
	updateJdesign();

}


function alignEdges(direction) {
	var minLeft, minTop, maxRight, maxBottom ;
	var es = $('.selected') ;
	// Setting the intial value for minLeft, minTop, MaxRight, maxBottom
	minLeft = es.eq(0).cssLeft();
	minTop = es.eq(0).cssTop() ; 
	maxRight = es.eq(0).cssLeft() + es.eq(0).cssWidth()  ; 
	maxBottom  = es.eq(0).cssTop() + es.eq(0).cssHeight()  ; 
	// Finding the final value of above variables
	es.each(function() {
			if ($(this).cssLeft() < minLeft) 
				minLeft = $(this).cssLeft() ; 
			
			if ($(this).cssTop() < minTop) 
				minTop = $(this).cssTop() ; 
			
			if ($(this).cssLeft() + $(this).cssWidth() > maxRight) 
				maxRight = $(this).cssLeft() + $(this).cssWidth()  ; 
			
			if ($(this).cssTop() + $(this).cssHeight() > maxBottom ) 
				maxBottom  = $(this).cssTop() + $(this).cssHeight()  ; 
	}) ; 
	
	if(es.length == 1){
		var canvas = view.jdesign.canvases[view.currentPage] ;
//		var margin = canvas.bleed + Math.max( (canvas.width - canvas.bleed * 2) / 20  , canvas.bleed)  ;
		var margin = 0.25 ; 
		minLeft = margin * view.dpi ;
		maxRight = (canvas.width -  margin) * view.dpi; 
		minTop = margin * view.dpi;
		maxBottom = (canvas.height - margin) * view.dpi ; 
	}
	
	if (direction == 'L') {
		applyTextAttr('gravity' , 'west') ;
		es.not('.unmovable').cssLeft(minLeft) ;
	}
	else if (direction == 'C') {
		applyTextAttr('gravity' , 'center') ;
		es.not('.unmovable').each(function() { 
											$(this).cssLeft( (minLeft + maxRight) / 2   - $(this).cssWidth() / 2 ) ;
										}) ;
	}
	else if (direction == 'R') {
		applyTextAttr('gravity' , 'east') ;
		es.not('.unmovable').each(function() { $(this).cssLeft( maxRight - $(this).cssWidth() ) }) ;
	}
	else if (direction == 'T') 
		es.not('.unmovable').cssTop(minTop) ; 

	else if (direction == 'M') 
		es.not('.unmovable').each(function() { $(this).cssTop( (minTop + maxBottom) / 2   - $(this).cssHeight() / 2 ) }) ;
		
	else if (direction == 'B') 
		es.not('.unmovable').each(function() { $(this).cssTop( maxBottom - $(this).cssHeight() ) }) ;
	
	// Apply the new outline positions to the main elements
	es.each(function() {
			$('#' +	$(this).attr('id').split('outline_')[1]).cssTop($(this).cssTop()) ;
			$('#' +	$(this).attr('id').split('outline_')[1]).cssLeft($(this).cssLeft()) ;
	 } );
	 
//	$('#align_control').removeClass('open') ; 
	view.outdatedJdesign = true ;
	updateJdesign();
 
}

function centerElementsToCanvas(){
	$('.outline2.selected').not('.unmovable').each(function(){
		$(this).cssLeft( ($('#canvas').cssWidth() - $(this).cssWidth()) / 2  );
		$('#' +	$(this).attr('id').split('outline_')[1]).cssLeft($(this).cssLeft()) ;
	});
	view.outdatedJdesign = true ;
	updateJdesign();
}

function showPreview(){
	$('#gray_box').show()  ;
    var preview_design = $('div#preview_design');

	//var preview_bar = $('<div />').addClass('preview-bar').appendTo('body');
    var preview_bar = $('<div />').addClass('preview-bar').appendTo(preview_design);
	//var c = $('<div />').addClass('overlay-container').appendTo('body');
	var c = $('<div />').addClass('overlay-container').appendTo(preview_design);

	var btn = $('<div />').addClass('toolbar-button type7 gray-gradient close-preview').appendTo(preview_bar);
	$('<div />').addClass('button-text').html('Close Preview').appendTo(btn);
	btn.click(function(){$('#gray_box').click()});
	
	$('<div />').attr('id' , 'preview_wait').addClass('wait').html('Generating Preview...').appendTo(c);
	var preview = $('<div />').attr('id' , 'preview_container').draggable().appendTo(c);
	getPreview(view.currentPage, preview, false,'png');

	if (view.jdesign.canvases.length > 1) {
		getPreview(view.currentPage == 0 ? 1 : 0, preview, true,'png');
		var btn0 = $('<div />').addClass('preview-btn gray-gradient with-active').attr('id', 'preview_btn0').appendTo(preview_bar).hide();
		$('<div />').addClass('button-text').html(getSideNameByFolding(view.jdesign.folding , 0 , view.jdesign.product)).appendTo(btn0);	
		btn0.click(function(){
            $('.large-preview').hide();
            $('#preview_0').fadeIn(300);		
			$('#preview_btn1').removeClass('on');
			$(this).addClass('on');
		});	
		var btn1 = $('<div />').addClass('preview-btn gray-gradient with-active').attr('id', 'preview_btn1').appendTo(preview_bar).hide();
		$('<div />').addClass('button-text').html(getSideNameByFolding(view.jdesign.folding , 1 , view.jdesign.product)).appendTo(btn1);
		btn1.click(function(){
            $('.large-preview').hide();
            $('#preview_1').fadeIn(300);
			$('#preview_btn0').removeClass('on');
			$(this).addClass('on');
		});
	}
}


function getPreview(page, preview, hide, ext){
	//alert('getPreview: ');
	if (hide) $('<img />').attr('src' , config.preloadImages[1]).attr('id', 'preview_' + page).addClass('preview-load').appendTo(preview).hide();

	$.ajax({
        //url: "/ajax/raster/getJsonPreview.php" ,
        url: "/tools.php?action=assets&action_type=preview_design" ,
		type: "post" ,  
		data: {"json" : JSON.stringify(view.jdesign) , "page" : page , "dpi" : view.baseDpi, cross: is_cross_domain, ext:ext},
		// asycn: false,
		dataType: is_cross_domain? 'json':'jsonp' ,
		jsonpCallback: is_cross_domain?null:'jsonp_callback' ,
		success: function(data){
            //alert(data.file);
            if(!is_cross_domain) data = jsonp_data;
			$('#preview_' + page).remove();
			$('<img />').attr('src' , data.file).attr('id', 'preview_' + page).addClass('large-preview').appendTo(preview).hide()
						.load(function(){					
							if (!hide) {
								if (!$('#preview_btn' + page).siblings().hasClass('on')) {
									$(this).fadeIn(300);
									$('#preview_btn' + page).addClass('on');
								}
								$('#preview_wait').hide();
								$('.overlay-container').click(function(){
									$('#gray_box').click();
								});
								$('.preview-btn').show();
							} else if ($('#preview_btn' + page).hasClass('on')) {
								$(this).fadeIn(300);
							}
							preview.height($(this).height()).width($(this).width());
						})
						.click(stopPropag) ;			
		} ,
        beforeSend: function(){
            //alert('SEnd');
        },
		error: function(xhr, textStatus, error){
            //alert(textStatus+' --- '+ error);
			$('#gray_box').click();
			showAlert('An error occured while generating the preview. Please try again.');
		}
	}) ;	
}



function showPreview3D(){
	$.ajax({
        url: "/tools.php?action=assets&action_type=preview_design" ,
		type: "post" ,  
		data: {"json" : JSON.stringify(view.jdesign), "page" : '0', "dpi" : view.baseDpi, cross: is_cross_domain, ext:'jpg',see_canvas:1},
		dataType: is_cross_domain? 'json':'jsonp' ,
		jsonpCallback: is_cross_domain?null:'jsonp_callback' ,
		success: function(data){
            
            if(data.file!=undefined){
            	var url = data.file;
            	url = url.split("/");
            	url = url[(url.length -1)];
            	url = url.replace(".jpg","");

            	getPreview3D(url);
            	// console.log(url);
            }
		} ,
        beforeSend: function(){
            //alert('SEnd');
        },
		error: function(xhr, textStatus, error){
			showAlert('An error occured while generating the preview. Please try again.');
		}
	}) ;	
}

function getPreview3D(img){
	$('#gray_box').show()  ;
    var preview_design = $('div#preview_design');
    var preview_bar = $('<div />').addClass('preview-bar').appendTo(preview_design);
	var c = $('<div />').addClass('overlay-container').appendTo(preview_design);

	var btn = $('<div />').addClass('toolbar-button type7 gray-gradient close-preview').appendTo(preview_bar);
	$('<div />').addClass('button-text').html('Close Preview').appendTo(btn);
	btn.click(function(){$('#gray_box').click()});
	
	$('<div />').attr('id' , 'preview_wait').addClass('wait').html('Generating Preview...').appendTo(c);
	var preview = $('<div />').attr('id' , 'preview_container').draggable().appendTo(c);

	preview.css('top', '0');
	
	var w,h,b,edge;
	var cvobj = view.jdesign.canvases;
	w=cvobj[0].width;
	h=cvobj[0].height;
	b=cvobj[0].bleed;
	edge = 'natural';
	if($("#inp_2").val()!=undefined)
		edge = $("#inp_2").val();

/*console.log('================');	
console.log(view);*/
	
	$('<iframe />').attr('src' , '/tools.php?action=canvas3d&img='+img+'&w='+w+'&h='+h+'&b='+b+'&edge='+edge).attr('id', 'preview_3d')
		.css('width', $(window).width()).css('height', $(window).height()).css('display', 'block').
		addClass('large-preview').appendTo(preview)
						.load(function(){
							
							$('.preview-btn').show();
							preview.height($(this).height()).width($(this).width());
							$('.overlay-container').click(function(){
									$('#gray_box').click();
							});
						});

}




function refreshView(){
	view.defaultFontSize = Math.max( Math.floor(view.jdesign.width * 2 + 2 ), config.fontSizes[0]) ;
	var currentScrollTop = getPageScrollTop()
	var jdesign = view.jdesign  ; 
	var page = view.currentPage ;
	
	jdesign.canvases[page].bleed *= 1 ;
	view.profileEQ.show_text_panel *= 1 ;
	
	if (! view.profileEQ.show_text_panel) {
		$('#text_fields').hide();
		$('#design_tool').css('margin-left' , '0');
		$('#show_text_panel_control').removeClass('on');
	} else {
		$('#text_fields').show();
		$('#design_tool').css('margin-left' , '225px');
		$('#show_text_panel_control').addClass('on');
	}


	$('#tool_placeholder').empty() ;
	$('#text_fields').empty() ; 

	view.DesignToolHeight = jdesign.canvases[page].height > 5 ? 850 : 450 ;

	if (view.profileEQ.show_text_panel){
		view.DesignToolWidth = 730 ;
	}
	else {
		view.DesignToolWidth = 955 ;
	}

	view.labelSize = 0 ; //25 ; 
	view.baseDpi = Math.min( (view.DesignToolWidth - 5 - 2 * view.labelSize) / jdesign.canvases[page].width , 	view.DesignToolHeight / jdesign.canvases[page].height ) ;
	view.baseDpi = Math.min(200 , view.baseDpi ) ;
	view.dpi = Math.floor (view.baseDpi * Math.pow (1.25 ,  view.zoomLevel) /  1) * 1 ;
	view.canvasShiftX = Math.max((view.labelSize), (view.DesignToolWidth - jdesign.canvases[page].width * view.dpi) / 2  ) 
	view.canvasShiftY = Math.max((2 + view.labelSize), ($('#tool_placeholder').css('min-height').split('px')[0] * 1 - jdesign.canvases[page].height * view.dpi) / 2  )	;
	var currentWidth = jdesign.canvases[page].width  * view.dpi;
	if(currentWidth > 960) {
		var leftToCenter = -(currentWidth - 960)/2;
		$("#div_wrapper").css("width",currentWidth);
	} else {
		var leftToCenter = view.canvasShiftX;
		$("#div_wrapper").removeAttr("style");
	}
	 $('<div />').attr('id','canvas')
						.cssWidth(currentWidth) 
						.cssHeight(jdesign.canvases[page].height * view.dpi) 
						.cssTop(view.canvasShiftY)
						.cssLeft(leftToCenter)
						.css('background-color' , '#' + jdesign.canvases[page].bg_color.split('#')[0])
						.appendTo('#tool_placeholder') ; 
						
	$('#tool_placeholder').cssWidth( Math.max(view.DesignToolWidth , $('#canvas').cssWidth() + view.canvasShiftX +  view.labelSize ) ) ;


	// ------------------ Inserting Layer Wrappers to the page -----------------------
	$('<div />').attr('id','image_layers').css('z-index' , 10).appendTo('#canvas') ;  
	$('<div />').attr('id','text_layers').css('z-index' , 20).appendTo('#canvas') ;  
	$('<div />').attr('id','overlay_layers').css('z-index' , 30).appendTo('#canvas') ;  
	$('<div />').attr('id','mask_layers').css('z-index' , 40).appendTo('#canvas') ;  
	$('<div />').attr('id','outline_layers').css('z-index' , 50).appendTo('#canvas') ;  
	$('<div />').attr('id','droppable_layers').css('z-index' , 60).appendTo('#canvas') ;  
	$('<div />').attr('id','loading').css('z-index' , 80).css('background-color' , $('body').css('background-color') ).appendTo('#canvas') ;  

	$('<div />').attr('id', 'snap_guide_x').addClass('snap-guide').appendTo('#mask_layers');
	$('<div />').attr('id', 'snap_guide_y').addClass('snap-guide').appendTo('#mask_layers');
	// ------ Inserting Ruler ----- //
	/*
	$('<img />').addClass('vertical-ruler ruler')
				.attr('src' , 'getRuler.php?ori=ver&dpi='+view.dpi+'&len='+ (jdesign.canvases[page].height - jdesign.canvases[page].bleed * 2) )
				.cssLeft(-15).appendTo('#mask_layers') ;  

	$('<img />').addClass('horizontal-ruler ruler')
				.attr('src' , '/ajax/bitmap/getRuler.php?ori=hor&dpi='+view.dpi+'&len='+ (jdesign.canvases[page].width - jdesign.canvases[page].bleed * 2) )
				.cssTop(-15).appendTo('#mask_layers') ;  
	*/			
	// Inserting Images
	for (i in jdesign.canvases[page].images)
				drawImageElement(jdesign.canvases[page].images[i]) ; 
				
	// Inserting Texts
	for (i in jdesign.canvases[page].texts) {
		addTextElement(jdesign.canvases[page].texts[i]) ; 
	}
	// Adding drop-masks
	updatePlaceholders() ;

	
	// ------------ Adding deselector -----------------------------------------
	$('<div />').addClass('outline2')
				.attr('id' , 'deselector' )
				.addClass('non-selectable')
				.css('z-index', -1)
				.appendTo('#outline_layers')
				.mousedown(function(e) {		
											
					view.selectorX = e.pageX - getCanvasX() ;
					view.selectorY = e.pageY - getCanvasY() ;
					deselectAll(); 
											
											
				})
				.bind("contextmenu", function(e) { 
					// return false;
				}) ; 

	$('<div />').attr('id' , 'multi_selector').appendTo('#outline_layers') ; 

	$('#page_navigation').empty();
	if(jdesign.canvases.length > 1){
		for(i = 0 ; i < jdesign.canvases.length ; i++){
			$('<div />').addClass('page-navigation-item gray-gradient with-active').attr('id' , 'page_navigation_' + i)
						.html(getSideNameByFolding(jdesign.folding, i, jdesign.product))
						.appendTo($('#page_navigation'));
		}
	}
	createBleedMask();
	createSafeZoneMask();
	arrangeImageZees();
	showLoading();
	setBleedAreaVisibility(0);
	setRulerVisibility(0); 
	setGridVisibility(0); 
	updateThemeOptions();	
	updateStockImagePrices();
	$('html,body').scrollTop(currentScrollTop);
	updateToolbar() ; 

}

function updateThemeOptions(){
	$('#theme_menu').empty();
	$('#theme_options').hide();
	if(! view.themes) {return} ; 
	$.each(view.themes , function(k,v){
		var th = $('<div />').addClass('toolbar-button type4 ez-theme-option').attr('id' , 'theme_' + v.id).appendTo('#theme_menu');
		if (k < view.themes.length - 1 ) 
			$('<hr />').appendTo('#theme_menu')
		getThemeThumb(v).appendTo(th);
	});
	if(view.themes.length > 1)
		$('#theme_options').show();
}

function getThemeThumb(theme, w, h){
	var tt = $('<div />').addClass('ez-theme-thumb');
	$.each(theme.colors, function(k1,v1){
		if(k1 < 5)
			$('<div />').addClass('ez-theme-thumb-color').css('background-color' , '#' + v1).appendTo(tt);
	})
	return tt ;
	
}

function getCanvasX() {
	return findPos($('#canvas')[0])[0] ;
}
function getCanvasY() {
	return findPos($('#canvas')[0])[1] ;
}

function updatePlaceholders() {
	return ; // Disabled since there is no drag-n-drop feature in Easy 2.0 currently
	
	$('#droppable_layers').empty() ; 

	$('#droppable_layers').droppable('destroy') ; 

	for (i in view.jdesign.canvases[view.currentPage].images){
		var im = view.jdesign.canvases[view.currentPage].images[i] ;
		if (typeof im.placeholder != 'undefined' && im.placeholder != false){
			
			// Ignore placeholder with set one-time-drop attribute that are already used
			if (im.oneTimeDrop == false || im.invisible == false || typeof im.invisible == 'undefined' || typeof im.oneTimeDrop == 'undefined' ) {
				$('<div />').addClass('drop-mask')
						.attr('id' , 'droppable_'+im.name)
						.cssLeft(im.left * view.dpi)
						.cssTop(im.top * view.dpi)
						.cssWidth(im.width * view.dpi)
						.cssHeight(im.height * view.dpi)
						.css('z-index' ,  im.zindex )
						.addClass(im.placeholder)
						.appendTo('#droppable_layers')
						.droppable({
								drop: placeDroppedImage ,
								over: function() {$(this).addClass('over')} ,
								out: function() {$(this).removeClass('over')}
						})

			}
		}
	}

	if (view.jdesign.canvases[view.currentPage].droppable) {
		$('#droppable_layers')
				.droppable({
						drop: placeDroppedImage
				});
	}
}


function addShapeToDesign(shape_type) {
	var jShape = {
		"zindex": 299 ,
		"fixedratio" : false , 
		"noselect" : false ,
		"noresize" : false ,
		"border_width" : 2 ,
		"border_color" : "797979" ,
		"fill_color" : "fdf6bb" ,
		"border_stroke" : "solid"
	}
	jShape.shape_type = shape_type ;
	jShape.name = "shape_" + Math.floor(Math.random() * 100000 ) ;
	jShape.width = jShape.height = Math.min(view.jdesign.canvases[view.currentPage].width, view.jdesign.canvases[view.currentPage].height) / 3 ;

	if (shape_type == 'hline')
		jShape.height = jShape.width / 4 ;
	else if (shape_type == 'vline')
		jShape.width = jShape.height / 4 ;
		
	jShape.left = (view.jdesign.canvases[view.currentPage].width - jShape.width) / 2 ,
	jShape.top = (view.jdesign.canvases[view.currentPage].height - jShape.height) / 2 ,
	
	view.jdesign.canvases[view.currentPage].images.push(JSON.clone(jShape)) ; 
	drawImageElement(jShape) ;
	deselectAll();
	$('#outline_img_' + jShape.name).mousedown();
	$('#outline_img_' + jShape.name).mouseup();

	arrangeImageZees() ;
	pushState() ; 
}


function deleteImageElementByName(name, restore_placeholder) {
	var element_index = getElementIndex(view.jdesign.canvases[view.currentPage].images, "name" , name ) ;

	if (typeof element_index != "number")
		return ;
	
	$('#img_'+name).remove() ; 
	$('#outline_img_'+name).remove() ; 
	view.jdesign.canvases[view.currentPage].images.splice(element_index, 1) ;
	delete view.userImages[name] ;
	

	// When a dropped image is deleted the corresponding placeholder will become visible again if available
	if(restore_placeholder && name.search(/^user_/) == 0){
			var parentPlaceHolder = getElem('img_' + name.split('user_')[1]) ;
			if (parentPlaceHolder){
				parentPlaceHolder.invisible = false ;
				drawImageElement(parentPlaceHolder) ; 
			}
			// If there has been an admin image in the placeholder, it will be restored
			var parentPlaceHolderAdminImage = getElem('img_admin_' + name.split('user_')[1]) ;
			if (parentPlaceHolderAdminImage){
				parentPlaceHolderAdminImage.invisible = false ;
				drawImageElement(parentPlaceHolderAdminImage) ; 
			}
	}
	
	// When a placeholder is deleted the corresponsing admin image will also be deleted if available
	deleteImageElementByName("admin_"+name) ; 

	updateStockImagePrices(); 
}

function createBleedMask() {
	var jdesign = view.jdesign ;
	var page = view.currentPage ;
	var bleed = '';
	if(products[jdesign.product].bleed!=undefined)
		bleed = products[jdesign.product].bleed;

	var link1 = '/tools.php?action=asstes&action_type=create_grid'
										+'&width=' + jdesign.canvases[page].width
										+'&height=' + jdesign.canvases[page].height 
										+'&dpi=' + view.dpi
										+'&page='+page
										+'&type=minor'
										+'&bleed=' + bleed;

    //alert(link1);
	//var link1 = 'images/design/getGridLines.png';
	// ------------------------------ Creating grid lines --------------------------------
	if((view.dpi * 0.25) >= 10){
		$('<div />').addClass('grid-area-mask')
					.addClass('grid-mask')
					.css('background' , 'url('+ link1
										+ ')'  )
					.css('opacity' , .4) 
					.appendTo('#mask_layers') ;		
	}

	link1 = '/tools.php?action=assets&action_type=create_grid'
									+'&width=' + jdesign.canvases[page].width
									+'&height=' + jdesign.canvases[page].height 
									+'&dpi=' + view.dpi
									+'&page='+page
									+'&type=major'
									+'&bleed=' + bleed;

	//link1 = 'images/design/getGridLines1.png';
    //alert(link1);
	$('<div />').addClass('grid-area-mask')
				.addClass('grid-mask')
				.css('background' , 'url('+link1
									+ ')'  )
				.appendTo('#mask_layers') ;					
					
	if(typeof jdesign.wrap_size != 'undefined' && jdesign.wrap_size > 0){
		createWrapMask();
		return;		
	}	
	// ----------------------------- Creating the bleed mask --------------------------------

	if (! jdesign.dieCutType || jdesign.dieCutType == 'none') {
		
		$('<div />').addClass('bleed-area-mask')
					.addClass('opacity50') 
					.css('width' , '100%')
					.css('height' , '100%')
					.css('border-width' , Math.round(jdesign.canvases[page].bleed * view.dpi) + 'px' )
					.css('border-color' , '#FFF')
					.css('border-style' , 'solid')
					.css('z-index' , 100)
					.appendTo('#mask_layers') ;
	
		$('<div />').addClass('bleed-area-mask')
					.css('width' , '100%')
					.css('height' , '100%')
					.css('border' , '1px solid gray' )
					.css('z-index' , 101)
					.appendTo('#mask_layers') ;
	
		$('<div />').addClass('bleed-area-mask')
					.cssWidth( (jdesign.canvases[page].width - jdesign.canvases[page].bleed *2 ) * view.dpi )
					.cssHeight( (jdesign.canvases[page].height - jdesign.canvases[page].bleed *2 ) * view.dpi)
					.css('margin' ,  Math.round(jdesign.canvases[page].bleed  * view.dpi) + 'px' )
					.css('border' , '1px dashed red' )
					.css('z-index' , 101)
					.appendTo('#mask_layers') ;
					
		$('<img />').addClass('bleed-area-mask')
					.attr('src' , 'images/scissors.png')
					.css('position' , 'absolute')
					.css('z-index' , 110)
					.cssTop(Math.round(jdesign.canvases[page].bleed  * view.dpi) - 15)
					.css('left' , '20%')
					.appendTo('#mask_layers')
					.hide();


		
		// -------------------------- Masks NO BLEED AREA --------------------------------
		
		$('<div />').addClass('no-bleed-area-mask canvas-shadow')
					.cssWidth( (jdesign.canvases[page].width - 2 * jdesign.canvases[page].bleed ) * view.dpi )
					.cssHeight( (jdesign.canvases[page].height - 2 * jdesign.canvases[page].bleed ) * view.dpi )
					.css('border' , '1px solid gray' )
					.css('margin' , Math.round(jdesign.canvases[page].bleed * view.dpi) + 'px' )
					.css('z-index' , 101)
					.appendTo('#mask_layers') ;
											
		$('<div />').addClass('no-bleed-area-mask')
					.css('width' ,'100%')
					.css('height' , '100%')
					.css('border-color' ,  $('body').css('background-color'))
					.css('border-style' , 'solid')
					.css('border-width' , Math.round(jdesign.canvases[page].bleed * view.dpi) + 'px' )
					.css('z-index' , 100)
					.appendTo('#mask_layers') ;								
	
	}
	// die-cut exist 
	
	
	else {
		//link1 = 'getDieCutMask.php?'
        link1 = '/tools.php?action=assets&action_type=create_die_cut'
										+'&width=' + (jdesign.canvases[page].width - jdesign.canvases[page].bleed * 2)
										+'&height=' + (jdesign.canvases[page].height - jdesign.canvases[page].bleed * 2) 
										+'&dpi=' + view.dpi
										+'&bleed=' + jdesign.canvases[page].bleed
										+'&dieCutType=' + jdesign.dieCutType
										+'&showBleed=0'
										+'&page='+page;
		//alert('A: '+link1);
		$('<div />').addClass('no-bleed-area-mask')
					.css('width' , '100%')
					.css('height' , '100%')
                    .css('background-repeat' , 'no-repeat')
					.css('background' , 'url('+link1 
										+ ')'  )
					.appendTo('#mask_layers') ;
					
		$('<div />').addClass('bleed-area-mask')
					.css('width' , '100%')
					.css('height' , '100%')
					.css('border' , '1px solid gray' )
					.css('z-index' , 101)
					.appendTo('#mask_layers') ;								
        //link1 = 'getDieCutMask.php?'
        link1 = '/tools.php?action=asstes&action_type=create_die_cut'
										+'&width=' + (jdesign.canvases[page].width - jdesign.canvases[page].bleed * 2)
										+'&height=' + (jdesign.canvases[page].height - jdesign.canvases[page].bleed * 2) 
										+'&dpi=' + view.dpi
										+'&bleed=' + jdesign.canvases[page].bleed
										+'&dieCutType=' + jdesign.dieCutType
										+'&showBleed=1' 
										+'&page='+page;
		$('<div />').addClass('bleed-area-mask')
					.css('width' , '100%')
					.css('height' , '100%')
                    .css('background-repeat' , 'no-repeat')
					.css('background' , 'url('+link1
										+ ')'  )
					.appendTo('#mask_layers') ;

		$('<img />').addClass('bleed-area-mask')
					.attr('src' , 'images/scissors.png')
					.css('position' , 'absolute')
					.css('z-index' , 110)

					.cssTop(Math.round(jdesign.canvases[page].bleed  * view.dpi) - 12)
					.css('left' , '45%')
					.appendTo('#mask_layers')
					.hide();					
					
	}
	//link1 = '/ajax/image/getShape.php?shape_type=circle&dpi='+view.dpi+'&width=1.35&height=1.35&border_width=1&border_color=777777&fill_color=F1F2F2&border_stroke=solid';
	//alert(link1);
	//link1= 'images/design/getShape.png';
	if(jdesign.product == 'doorHanger'){
			$('<img />')
					.attr('src' , link1)
					.css('position' , 'absolute')
					.css('z-index' , 120)
					.cssTop(0.5 * view.dpi)
					.cssLeft( (jdesign.canvases[page].width - 1.35)  / 2  * view.dpi)
					.appendTo('#mask_layers');
		
	}
	// ------------------------------ Creating panel mask labels --------------------------------

	$('<div />').addClass('bleed-area-mask')
			.addClass('panel-mask')
			.cssWidth(jdesign.canvases[page].width * view.dpi + 2 * view.labelSize )
			.cssHeight(jdesign.canvases[page].height * view.dpi + 2 * view.labelSize)				
			.cssLeft(- view.labelSize)
			.cssTop(- view.labelSize)
			// .css('background' , 'url(/ajax/bitmap/getPanelMask.php?'
			// 					+'width=' + jdesign.canvases[page].width 
			// 					+'&height=' + jdesign.canvases[page].height 
			// 					+'&dpi=' + view.dpi
			// 					+'&folding=' + jdesign.folding
			// 					+'&productType=' + jdesign.product
			// 					+'&page='+page
			// 					+'&bleed=' + products[jdesign.product].bleed
			// 					+ ')'  )
			.appendTo('#mask_layers') ;	

}

function createSafeZoneMask() {

	var jdesign = view.jdesign ;
	var page = view.currentPage ;
	var panel_count, ver_panel_count, hor_panel_count;
	if(typeof jdesign.wrap_size  != 'undefined' && jdesign.wrap_size > 0) return;
	if(jdesign.canvases[page].bleed == 0) return ;
	if (typeof jdesign.dieCutType === 'undefined' || jdesign.dieCutType == '' || jdesign.dieCutType == 'none'  )  {
	
		if (typeof view.jdesign.folding === 'undefined' || view.jdesign.folding == 'none' || view.jdesign.folding == '' ) 
			panel_count = 1 ;
		else if (view.jdesign.folding == 'halfFold' ) {
			if(view.jdesign.product == "foldedGreetingCard")
				panel_count = 1 ;
			else
				panel_count = 2 ;
		}
		else if (view.jdesign.folding == 'zFold' || view.jdesign.folding == 'triFold' || view.jdesign.folding == 'letterFold')	
			panel_count = 3 ;
		else if (view.jdesign.folding == 'accordionFold' || view.jdesign.folding == 'rollFold' ) 
			panel_count = 4 ;
		
		if (typeof view.jdesign.foldingDirection === 'undefined' || view.jdesign.foldingDirection == 'vertical' || view.jdesign.foldingDirection == '' ) {
			ver_panel_count = panel_count ; 
			hor_panel_count = 1 ;
		} else if (view.jdesign.foldingDirection == 'horizontal') {
			hor_panel_count = panel_count ; 
			ver_panel_count = 1 ;
		}
 		var panel_width = (jdesign.canvases[page].width - (4 + (ver_panel_count - 1) * 2 ) * jdesign.canvases[page].bleed) / ver_panel_count  ;
		var panel_height = (jdesign.canvases[page].height - (4 + (hor_panel_count -1) * 2 ) * jdesign.canvases[page].bleed) /  hor_panel_count  ;
	
		for (v = 1 ; v <= ver_panel_count ; v++ ){
			for (h = 1 ; h <= hor_panel_count ; h++ ){
				$('<div />').addClass('safe-zone-mask dashed')
							.cssWidth(panel_width * view.dpi)
							.cssHeight(panel_height * view.dpi)
							.cssLeft( ( 2 * v  * jdesign.canvases[page].bleed + (v - 1 ) * panel_width) * view.dpi )
							.cssTop( ( 2 * h  * jdesign.canvases[page].bleed + (h - 1 ) * panel_height) * view.dpi )
							.appendTo('#mask_layers') ;

				$('<div />').addClass('safe-zone-mask solid')
							.cssWidth(panel_width * view.dpi)
							.cssHeight(panel_height * view.dpi)
							.cssLeft( ( 2 * v  * jdesign.canvases[page].bleed + (v - 1 ) * panel_width) * view.dpi )
							.cssTop( ( 2 * h  * jdesign.canvases[page].bleed + (h - 1 ) * panel_height) * view.dpi )
							.appendTo('#mask_layers') ;
			}
		}
		/*
		$('<img />').addClass('safe-zone-mask')
					.attr('src' , '/images/easy/safezonealert.png')
					.cssRight((jdesign.canvases[page].width - jdesign.canvases[page].bleed * 2) * view.dpi )
					.cssTop(jdesign.canvases[page].bleed * 4 * view.dpi )
					.appendTo('#mask_layers') ;
		*/				
		if (ver_panel_count > 1)
			for (v = 1 ; v < ver_panel_count ; v++)
					$('<div />').addClass('folding-line-ver')
							.cssLeft( ( (2 * v + 1) * jdesign.canvases[page].bleed + v * panel_width ) * view.dpi )
							.appendTo('#mask_layers') ;

		if (hor_panel_count > 1)
			for (h = 1 ; h < hor_panel_count ; h++)
					$('<div />').addClass('folding-line-hor')
							.cssTop( ( (2 * h + 1) * jdesign.canvases[page].bleed + h * panel_height ) * view.dpi )
							.appendTo('#mask_layers') ;

	
	} else {
		$('<div />').addClass('safe-zone-mask')
					.css('width' , '100%')
					.css('height' , '100%')
					.css('background' , 'url(getDieCutSafeZone.php?' + 
														'width=' + (jdesign.canvases[page].width - 2 * jdesign.canvases[page].bleed)
														+'&height=' + (jdesign.canvases[page].height - 2 * jdesign.canvases[page].bleed)
														+'&dpi=' + view.dpi
														+'&bleed=' + jdesign.canvases[page].bleed
														+'&dieCutType=' + jdesign.dieCutType
														+'&page=' + page
														+ ')'  )
					.appendTo('#mask_layers') ;
	}

}

function createWrapMask(){
	var wrapWidth = view.jdesign.wrap_size * 1 ;
	var width = view.jdesign.canvases[view.currentPage].width * 1 ;
	var height = view.jdesign.canvases[view.currentPage].height * 1;
	var bleed = view.jdesign.canvases[view.currentPage].bleed * 1;

	$('.canvas-wrap-mask').remove();
	$('<div />').addClass('bleed-area-mask canvas-wrap-mask')
					.cssWidth((width - 2 * wrapWidth) * view.dpi - 1)
					.cssHeight(wrapWidth * view.dpi)
					.cssLeft(wrapWidth * view.dpi)
					.cssTop(0)
					.appendTo('#mask_layers') ;

	$('<div />').addClass('bleed-area-mask canvas-wrap-mask')
					.cssWidth((width - 2 * wrapWidth) * view.dpi - 1)
					.cssHeight(wrapWidth * view.dpi)
					.cssLeft(wrapWidth * view.dpi)
					.cssBottom(0)
					.appendTo('#mask_layers') ;

	$('<div />').addClass('bleed-area-mask canvas-wrap-mask')
					.cssWidth(wrapWidth * view.dpi)
					.cssHeight((height - 2 * wrapWidth) * view.dpi - 1)
					.cssLeft(0)
					.cssTop(wrapWidth * view.dpi)
					.appendTo('#mask_layers') ;

	$('<div />').addClass('bleed-area-mask canvas-wrap-mask')
					.cssWidth(wrapWidth * view.dpi)
					.cssHeight((height - 2 * wrapWidth) * view.dpi - 1)
					.cssRight(0)
					.cssTop(wrapWidth * view.dpi)
					.appendTo('#mask_layers') ;

	if (view.jdesign.wrap_color == 'transparent')
		$('.canvas-wrap-mask').addClass('gallery-wrap');
	else {
		$('.canvas-wrap-mask').removeClass('gallery-wrap');
		$('.canvas-wrap-mask').addClass('museum-wrap');
		$('.canvas-wrap-mask').css('background-color' , '#' + view.jdesign.wrap_color);
	}
// ---- 4 corners ----- //
		$('<div />').addClass('bleed-area-mask')
					.cssWidth(wrapWidth * view.dpi )
					.cssHeight(wrapWidth * view.dpi )
					.cssLeft(0)
					.cssTop(0)
					.css('z-index' , 102)
					.css('background-color' , '#eee')
					.appendTo('#mask_layers') ;

		$('<div />').addClass('bleed-area-mask')
					.cssWidth(wrapWidth * view.dpi)
					.cssHeight(wrapWidth * view.dpi)
					.cssRight(0)
					.cssTop(0)
					.css('z-index' , 103)
					.css('background-color' , '#eee')
					.appendTo('#mask_layers') ;

		$('<div />').addClass('bleed-area-mask')
					.cssWidth(wrapWidth * view.dpi )
					.cssHeight(wrapWidth * view.dpi )
					.cssLeft(0)
					.cssBottom(0)
					.css('z-index' , 104)
					.css('background-color' , '#eee')
					.appendTo('#mask_layers') ;

		$('<div />').addClass('bleed-area-mask')
					.cssWidth(wrapWidth * view.dpi )
					.cssHeight(wrapWidth * view.dpi )
					.cssRight(0)
					.cssBottom(0)
					.css('z-index' , 105)
					.css('background-color' , '#eee')
					.appendTo('#mask_layers') ;

}

function showLoading() {
	var isComplete = true ; 
	$("#image_layers img , #text_layers img , #mask_layers img").each(function () {isComplete = isComplete && this.complete; }) ; 
	for (i in tempTextImages){
		isComplete = isComplete && tempTextImages[i].complete ;
	}

	if (isComplete){
		$('#loading').hide() ; 
	}
	else {
		$('#loading').show() ; 
		view.loadingTimer = window.setTimeout("showLoading()" , 50) ;
	}
}

function getMockupSrc(jimage) {
	var url;
	if (typeof jimage.image_id != 'undefined' ){
		if(typeof jimage.svg_id != 'undefined' && jimage.svg_id != null)
			url = "/tools.php?action=assets&action_type=create_svg&svg_id="+jimage.svg_id+"&rotation="+jimage.rotation+"&colors="+jimage.svg_colors.join(',').replace(/#[0-9]/ig , '') ;
		else 
			url = "/tools.php?action=assets&action_type=create_mock_up&image_id="+jimage.image_id+"&rotation="+jimage.rotation + '&filter='+jimage.filter+"&flip=" + jimage.flip;
	}
	else if (typeof jimage.shape_type != 'undefined')
		url = "/tools.php?action=assets&action_type=create_shape"+
					"&shape_type="+jimage.shape_type +
					"&dpi="+view.dpi+
					"&width="+jimage.width+
					"&height="+jimage.height +
					"&border_width="+jimage.border_width + 
					"&border_color="+jimage.border_color.split('#')[0] +
					"&fill_color="+jimage.fill_color.split('#')[0] +
					"&border_stroke="+jimage.border_stroke+"&flip=" + jimage.flip ;

	//var url = 'images/design/getMockup.png';
    //alert(url);
	return url;
}

function updateJdesign() {
//	console.log('updateJdesign',view.outdatedJdesign);
	if(! view.outdatedJdesign) return;
	view.outdatedJdesign = false ;
	
	$('#text_layers img').each(function() {
			jtext = getElem($(this).attr('id')); 
			jtext.left = Math.round ($(this).cssLeft() /view.dpi * 1000 ) / 1000  ;
			jtext.top = Math.round ($(this).cssTop() /view.dpi * 1000 ) / 1000  ;
			
			// update the text if the width is changed after mouse release
			if (Math.abs(jtext.width * view.dpi - $('#outline_'+$(this).attr('id')).cssWidth())  > 1 || Math.abs(jtext.height * view.dpi - $('#outline_'+$(this).attr('id')).cssHeight()) > 1 )
			{
				jtext.width = Math.round ( $('#outline_'+$(this).attr('id')).cssWidth()  / view.dpi * 1000 ) / 1000  ;
				jtext.height = Math.round ( $('#outline_'+$(this).attr('id')).cssHeight()  / view.dpi * 1000 ) / 1000  ;				
				updateTextElement('txt_'+jtext.name) ; 
			}				
			
	}) ; 
	
	$('#image_layers .image-mask , #overlay_layers .image-mask').each(function(i) {
			
			var jimage = getElem($(this).attr('id')) ; 
			var outline = $('#outline_img_' + jimage.name);
			var inner = $('#inner_img_' + jimage.name);

			jimage.left = 1 * (outline.cssLeft() / view.dpi).toFixed(3) ;
			jimage.top = 1 * (outline.cssTop() / view.dpi).toFixed(3) ;
			jimage.width = 1 * (outline.cssWidth() / view.dpi).toFixed(3) ;
			jimage.height = 1 * (outline.cssHeight() / view.dpi).toFixed(3) ;
			
			var newCropWidth = 1 * (outline.cssWidth() / inner.cssWidth()).toFixed(4);
			var newCropHeight = 1 * (outline.cssHeight() / inner.cssHeight() ).toFixed(4);

			if (newCropWidth  !=1 || newCropHeight != 1){
				jimage.crop = true ;
				jimage.cropWidth = newCropWidth ;
				jimage.cropHeight = newCropHeight ;
				jimage.cropTop = -1 * (inner.cssTop() / inner.cssHeight()).toFixed(4);
				jimage.cropLeft = -1 * (inner.cssLeft() / inner.cssWidth()).toFixed(4) ;
			} 
			else {
				jimage.crop = false ;
				delete jimage.cropWidth ;
				delete jimage.cropHeight ;
				delete jimage.cropLeft ;
				delete jimage.cropTop ;

			}

			updateFotoliaLicense(jimage);

			var resAlert = outline.find('.resolution-notice') ;
			if (resAlert.length > 0) {
				resAlert.cssTop(Math.max (-1 * outline.cssTop() , 5) ) ; 
				resAlert.cssRight(Math.max ( outline.cssLeft() + outline.cssWidth() - $('#canvas').cssWidth() , 5) ) ; 
			}
			
//			stockImagePrice = outline.find('.stock-image-price-outline') ;
//			stockImagePrice.cssTop(Math.max (-1 * outline.cssTop() , -8 ) ) ; 
//			stockImagePrice.cssLeft(Math.max (-1 * outline.cssLeft() + 40 , outline.cssWidth() / 2 - 10 ) ) ; 
//			outline.find('.stock-image-thumb-info').cssTop(Math.max (-1 * outline.cssTop() , -8 ) ).cssLeft(outline.cssWidth() / 2 + 20) ;

	}) ; 
	updateStockImagePrices();
	deleteOffCanvasElements() ; 	
	pushState() ;
	updateToolbar() ; 
}

function updateSnapPoints(){
	var cn = view.jdesign.canvases[view.currentPage] ;
	view.snapX = [0 , cn.bleed * 2 * view.dpi , (cn.width - cn.bleed * 2) * view.dpi , cn.width * view.dpi ] ;
	view.snapY = [0 , 0.25 * view.dpi , (cn.height - cn.bleed * 2) * view.dpi , cn.height * view.dpi ] ;
	$('.outline2').not('.selected').each(function(){
		view.snapX.push($(this).cssLeft()) ;
		view.snapX.push($(this).cssLeft() + $(this).cssWidth()) ;
	});
	$('.outline2').not('.selected').each(function(){
		view.snapY.push($(this).cssTop()) ;
		view.snapY.push($(this).cssTop() + $(this).cssHeight()) ;
	});
}
function deleteOffCanvasElements() {
	var toBeDeletedImages = [];
	var toBeDeletedTexts = [];

	var cnv = view.jdesign.canvases[view.currentPage] ;

	for (i in cnv.images){
		var im = cnv.images[i] ;
		if (im.top + im.height < cnv.bleed || im.left + im.width < cnv.bleed || im.left > cnv.width - cnv.bleed || im.top  > cnv.height - cnv.bleed)
			toBeDeletedImages.push(im.name) ;
	}

	for (i in toBeDeletedImages)
		deleteImageElementByName(toBeDeletedImages[i] , false) ; 

	for (i in view.jdesign.canvases[view.currentPage].texts){
		var im = cnv.texts[i] ;
		if (im.top + im.height < cnv.bleed || im.left + im.width < cnv.bleed || im.left > cnv.width - cnv.bleed || im.top  > cnv.height - cnv.bleed) {
			toBeDeletedTexts.push(im.name) ;
		}
	}

	for (i in toBeDeletedTexts)
		deleteTextElementByName(toBeDeletedTexts[i]) ; 

		
}

/* -----When a resize handle is pressed, this registers the resize direction and put the elements in resizing mode ------- */
function triggerResizing(e){	
	ie8SafePreventEvent(e) ; 
	stopPropag(e);
	
	if ($(this).parent().hasClass('non-resizable'))
		return ;
	
	$('.grabbed').removeClass('grabbed') ; 
	$('.qcrop').removeClass('qcrop') ; 

	$('.selected').removeClass('selected') ; 

	$(this).parent().addClass('selected resizing');  
	
	view.resizing = true ;
	
	resize = {} ; 
	
	resize.startX = e.pageX ;
	resize.startY = e.pageY ;

	resize.minW = 20 ; 
	resize.minH = 20 ; 
	
	resize.dir = $(this).attr('class').match(/handle-[a-z]+/)[0].split('-')[1] ;
	resize.outline = $(this).closest('.outline2');
	resize.free = resize.outline.hasClass('text') || resize.outline.hasClass('croppable') ;

	resize.fixedRatio = resize.outline.hasClass('fixed-ratio') ; 
	if (resize.outline.hasClass('croppable')){
		resize.type = 'crop2' ;
		resize.fixedRatio = false ;
		$('#'+resize.outline.attr('id').split('outline_')[1]).addClass('crop');
		var inner = $('#inner_'+resize.outline.attr('id').split('outline_')[1]);
		resize.newW = resize.initialW = resize.outline.cssWidth() ;
		resize.newH = resize.initialH = resize.outline.cssHeight(); 
		resize.newT = resize.initialT = resize.outline.cssTop(); 
		resize.newL = resize.initialL = resize.outline.cssLeft() ; 

		resize.minT = resize.initialT + inner.cssTop() ; 
		resize.minL = resize.initialL +  inner.cssLeft() ; 
		resize.maxR = resize.minL + inner.cssWidth();
		resize.maxB = resize.minT + inner.cssHeight();
	}

	else {	
		resize.minT = - Number.MAX_VALUE ; 
		resize.minL = - Number.MAX_VALUE ; 
		resize.maxR = Number.MAX_VALUE ; 
		resize.maxB = Number.MAX_VALUE ; 

		resize.type = 'resize' ;
		resize.jel =  getElem(resize.outline.attr('id').split('outline_')[1]) ; 
		if (typeof resize.jel.image_id != 'undefined')
			resize.elType = 'img' ;
		else if (typeof resize.jel.body != 'undefined' || typeof resize.jel.preset_text != 'undefined')
			resize.elType = 'txt' ;
		else if (typeof resize.jel.shape_type != 'undefined' )
			resize.elType = 'shape' ;


		resize.el = $('#' + resize.outline.attr('id').split('outline_')[1] ) ;
		resize.inner = resize.el.find('img');
		resize.newW = resize.initialW = resize.jel.width * view.dpi ; 

		if(resize.elType != 'txt')
			resize.newH = resize.initialH = resize.jel.height * view.dpi ; 
		else 
			resize.newH = resize.initialH = resize.outline.cssHeight() ; 
			
		resize.newT = resize.initialT = resize.jel.top * view.dpi ; 
		resize.newL = resize.initialL = resize.jel.left * view.dpi ; 
		resize.ratio = resize.initialW / resize.initialH ;
	}
	

	if (resize.elType == 'txt')
		resize.fixedRatio = false ; 
		
	$('.outline2').css("cursor" , resize.dir + "-resize") ; 

}


function resizeElement(e){
 	$('.float-toolbar').hide();
 	$('.float-toolbar').hide();

	var resizeX = e.pageX - resize.startX ;
	var resizeY = e.pageY  - resize.startY ;
	view.outdatedJdesign = true ;
	// corner resizes are always proportional
	if(resize.dir == 'ne' || resize.dir == 'nw' || resize.dir == 'sw' || resize.dir == 'se' ){
		if(resize.free) {
			if(resize.dir == 'ne'){
				resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
				resize.newH = Math.min(resize.initialH - resizeY, resize.initialT - resize.minT + resize.initialH ) ; 
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = Math.max(resize.newH, resize.minH);
		
				resize.newL = resize.initialL ;
				resize.newT = resize.initialT + (resize.initialH - resize.newH) ;
			}
			else if(resize.dir == 'nw'){

				resize.newW = Math.min(resize.initialW - resizeX, resize.initialL - resize.minL + resize.initialW) ; 
				
				resize.newH = Math.min(resize.initialH - resizeY, resize.initialT - resize.minT + resize.initialH ) ; 
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = Math.max(resize.newH, resize.minH);
		
				resize.newL = resize.initialL + resize.initialW - resize.newW;
				resize.newT = resize.initialT + resize.initialH - resize.newH ;
			}
			else if(resize.dir == 'sw'){
				resize.newW = Math.min(resize.initialW - resizeX, resize.initialL - resize.minL + resize.initialW) ; 
				resize.newH = Math.min(resize.initialH + resizeY, resize.maxB - resize.initialT ) ; 
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = Math.max(resize.newH, resize.minH);
		
				resize.newL = resize.initialL - (resize.newW - resize.initialW);
				resize.newT = resize.initialT ;
			}
		
			else if(resize.dir == 'se'){
				resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
				resize.newH = Math.min(resize.initialH + resizeY, resize.maxB - resize.initialT ) ; 
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = Math.max(resize.newH, resize.minH);
		
				resize.newL = resize.initialL ;
				resize.newT = resize.initialT ; 
			}
		}
		else {
			if(resize.dir == 'ne'){
				resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
				resize.newW = Math.min(resize.newW, (resize.maxB - resize.minT) * resize.ratio);
				resize.newW = Math.min(resize.newW, (resize.initialH - resize.minT + resize.initialT) * resize.ratio);
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = resize.newW / resize.ratio ;
		
				resize.newL = resize.initialL ;
				resize.newT = resize.initialT + (resize.initialH - resize.newH) ;
			}
			else if(resize.dir == 'nw'){
				resize.newW = Math.min(resize.initialW - resizeX, resize.initialL - resize.minL + resize.initialW) ; 
				resize.newW = Math.min(resize.newW, (resize.initialH - resize.minT + resize.initialT) * resize.ratio);
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = resize.newW / resize.ratio ;
		
				resize.newL = resize.initialL - (resize.newW - resize.initialW);
				resize.newT = resize.initialT + (resize.initialH - resize.newH) ;
			}
			else if(resize.dir == 'sw'){
				resize.newW = Math.min(resize.initialW - resizeX, resize.initialL - resize.minL + resize.initialW) ; 
				resize.newW = Math.min(resize.newW, (resize.maxB - resize.initialT) * resize.ratio);
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = resize.newW / resize.ratio ;
		
				resize.newL = resize.initialL - (resize.newW - resize.initialW);
				resize.newT = resize.initialT ;
			}
		
			else if(resize.dir == 'se'){
				resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
				resize.newW = Math.min(resize.newW, (resize.maxB - resize.initialT) * resize.ratio);
				resize.newW = Math.max(resize.newW, resize.minW);
				resize.newH = resize.newW / resize.ratio ;
		
				resize.newL = resize.initialL ;
				resize.newT = resize.initialT ; 
			}
		}
	}


	if(resize.fixedRatio){
		if(resize.dir == 'e'){
			resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
			resize.newW = Math.min(resize.newW, (resize.maxB - resize.minT) * resize.ratio);
			resize.newW = Math.max(resize.newW, resize.minW);
			resize.newH = resize.newW / resize.ratio ;
	
			resize.newL = resize.initialL ;
			resize.newT = resize.initialT - (resize.newW - resize.initialW) /2 / resize.ratio;
			resize.newT = Math.max(resize.newT, resize.minT) ;
			resize.newT = Math.min(resize.newT, resize.maxB - resize.newH ) ;
			
		}
	
		else if(resize.dir == 'w'){
			resize.newW = Math.min(resize.initialW - resizeX, resize.initialL + resize.initialW - resize.minL);
			resize.newW = Math.min(resize.newW, (resize.maxB - resize.minT) * resize.ratio);
			resize.newW = Math.max(resize.newW, resize.minW);
			resize.newH = resize.newW / resize.ratio ;
	
			resize.newL = resize.initialL - (resize.newW - resize.initialW);
			resize.newT = resize.initialT - (resize.newW - resize.initialW) / 2 / resize.ratio ;
			resize.newT = Math.max(resize.newT, resize.minT) ;
			resize.newT = Math.min(resize.newT, resize.maxB - resize.newH ) ;
		}
	
		else if(resize.dir == 'n'){
			resize.newH = Math.min(resize.initialH - resizeY , resize.initialT + resize.initialH - resize.minT);
			resize.newH = Math.min(resize.newH, (resize.maxR - resize.minL) / resize.ratio);
			resize.newH = Math.max(resize.newH, resize.minH);
			resize.newW = resize.newH * resize.ratio ;
	
			resize.newT = resize.initialT - (resize.newH - resize.initialH);
			resize.newL = resize.initialL - (resize.newH - resize.initialH) / 2 * resize.ratio ;
			resize.newL = Math.max(resize.newL, resize.minL) ;
			resize.newL = Math.min(resize.newL, resize.maxR - resize.newW) ;
		}
	
		else if(resize.dir == 's'){
			resize.newH = Math.min(resize.initialH + resizeY , resize.maxB - resize.initialT) ; 
			resize.newH = Math.min(resize.newH, (resize.maxR - resize.minL) / resize.ratio);
			resize.newH = Math.max(resize.newH, resize.minH);
			resize.newW = resize.newH * resize.ratio ;
	
			resize.newT = resize.initialT ;
			resize.newL = resize.initialL - (resize.newH - resize.initialH) / 2 * resize.ratio ;
			resize.newL = Math.max(resize.newL, resize.minL) ;
			resize.newL = Math.min(resize.newL, resize.maxR - resize.newW ) ;		
		}

	}
	else {
		if(resize.dir == 'e'){
			resize.newW = Math.min(resize.initialW + resizeX, resize.maxR - resize.initialL) ; 
			resize.newW = Math.max(resize.newW, resize.minW);
	
			resize.newL = resize.initialL ;
			resize.newT = resize.initialT ; 			
		}
	
		else if(resize.dir == 'w'){
			resize.newW = Math.min(resize.initialW - resizeX, resize.initialL + resize.initialW - resize.minL);
			resize.newW = Math.max(resize.newW, resize.minW);
	
			resize.newL = resize.initialL - (resize.newW - resize.initialW);
			resize.newT = resize.initialT ;
		}
	
		else if(resize.dir == 'n'){
			resize.newH = Math.min(resize.initialH - resizeY , resize.initialT + resize.initialH - resize.minT);
			resize.newH = Math.max(resize.newH, resize.minH);
	
			resize.newT = resize.initialT - (resize.newH - resize.initialH);
			resize.newL = resize.initialL ;
		}
	
		else if(resize.dir == 's'){
			resize.newH = Math.min(resize.initialH + resizeY , resize.maxB - resize.initialT) ; 
			resize.newH = Math.max(resize.newH, resize.minH);
	
			resize.newT = resize.initialT ;
			resize.newL = resize.initialL ;
		}
	}
	
	resize.outline.cssLeft(resize.newL) ; 
	resize.outline.cssTop(resize.newT) ; 
	resize.outline.cssWidth(resize.newW) ; 
	resize.outline.cssHeight(resize.newH) ; 
	resize.outline.find('.size-measure.w .value').html((resize.newW / view.dpi).toFixed(2) + '&quot;' );
	resize.outline.find('.size-measure.h .value').html((resize.newH / view.dpi).toFixed(2) + '&quot;' );

	if (resize.type == 'resize') {	
		resize.el.cssLeft(resize.newL) ; 
		resize.el.cssTop(resize.newT) ; 

		var cropWidth = resize.jel.crop ? resize.jel.cropWidth : 1 ;
		var cropHeight = resize.jel.crop ? resize.jel.cropHeight : 1 ;
		var cropTop = resize.jel.crop ? resize.jel.cropTop : 0 ;
		var cropLeft = resize.jel.crop ? resize.jel.cropLeft : 0 ;		

		if(typeof resize.inner != 'undefined' && resize.inner.length > 0){
			resize.inner.cssLeft(- cropLeft * resize.newW / cropWidth);
			resize.inner.cssTop(- cropTop * resize.newH / cropHeight ) ;
			resize.inner.cssWidth(resize.newW / cropWidth);
			resize.inner.cssHeight(resize.newH / cropHeight);
		}
		if (resize.elType != 'txt' ) {
			resize.el.cssWidth(resize.newW).cssHeight(resize.newH) ; 
		}

		if (typeof resize.jel.image_id != 'undefined' && typeof resize.jel.fotoliaId == 'undefined' ){
			checkImageResolution(resize.jel);
		}
	}
	else if(resize.type == 'crop2'){
		var imgMask = $('#' + resize.outline.attr('id').split('outline_')[1]) ;
		var inner = $('#inner_' + resize.outline.attr('id').split('outline_')[1]) ;
		imgMask.cssWidth(resize.newW).cssHeight(resize.newH).cssLeft(resize.newL).cssTop(resize.newT) ; 
		inner.cssLeft( resize.minL - resize.newL);
		inner.cssTop( resize.minT - resize.newT);
		
	}
}



function checkImageResolution(jimage) {
	if(view.imageInfo[jimage.image_id].is_vector || !! jimage.noprint) 
		return ;

	var outline = $('#outline_img_' + jimage.name);
	outline.find('.resolution-notice').remove();
	
	currentWidthPixel = view.imageInfo[jimage.image_id].uploadWidth ;
	outputUncroppedWidthInch = (jimage.rotation % 180 == 0 ?  outline.cssWidth() : outline.cssHeight() ) / view.dpi ;

	if (jimage.crop)
		outputUncroppedWidthInch /= (jimage.rotation % 180 == 0 ? jimage.cropWidth : jimage.cropHeight)  ;
		
	var outputResolution = currentWidthPixel / outputUncroppedWidthInch ;

	
	if (outputResolution < config.warningRes){
		var noticeIcon = $('<div />').addClass('general-icon resolution-notice')
					.css('background-position' , '-1056px 0px')
					.cssTop(Math.max (-1 * outline.cssTop() , 5) ) 
					.cssRight(Math.max ( outline.cssLeft() + outline.cssWidth() - $('#canvas').cssWidth() , 5) )
					.appendTo(outline) ;
		$('<div />').html('This image will look pixelated or blurry when printed due to its low resolution. You may want to resize it smaller to achieve a better result.')
					.css('background-color'  , '#ffd5cf')
					.appendTo(noticeIcon) ;

	}
	else if (outputResolution < config.noticeRes){
		var noticeIcon = $('<div />').addClass('general-icon resolution-notice')
					.css('background-position' , '-1072px 0px')
					.cssTop(Math.max (-1 * outline.cssTop() , 5) )
					.cssRight(Math.max ( outline.cssLeft() + outline.cssWidth() - $('#canvas').cssWidth() , 5) )
					.appendTo(outline) ;
		$('<div />').html('This image may look pixelated or blurry when printed due to its low resolution. You may want to resize it smaller to achieve the best result.')
					.appendTo(noticeIcon) ;
	}


}

function updateFotoliaLicense(jimage) {
	if(! jimage.fotoliaId) return ;
	var currentLicenseIndex = getElementIndex(view.fotoliaLicenses[jimage.fotoliaId] , 'name' , jimage.fotoliaLicense) ;

	if(currentLicenseIndex === false) currentLicenseIndex  = 0 ; // return ;
	
	var currentLicenseWidthPixel = view.fotoliaLicenses[jimage.fotoliaId][currentLicenseIndex].width ;
	var outputUncroppedWidthInch = jimage.rotation % 180 == 0 ?  jimage.width : jimage.height  ;

	if (jimage.crop)
		outputUncroppedWidthInch /= (jimage.rotation % 180 == 0 ? jimage.cropWidth : jimage.cropHeight)  ;
		
	var outputResolution = currentLicenseWidthPixel / outputUncroppedWidthInch ;
	var newLicenseIndex = currentLicenseIndex ;
	
	// increase the license until the resolution hits the critria or there is no bigger license available
	while (outputResolution < config.minFotoliaRes && typeof view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex + 1] != 'undefined' && view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex + 1].width * 1 > view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex].width *1   && view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex + 1].name != "X" ){
		newLicenseIndex++ ;
		outputResolution = view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex].width / outputUncroppedWidthInch ;
	}
	
	// decrease the license until the minimum necessary resolution or there is no smaller license available
	while (typeof view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex-1] != 'undefined' && (view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex-1].width / outputUncroppedWidthInch >= config.minFotoliaRes || view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex].name == 'X' )){
		newLicenseIndex-- ;
		outputResolution = view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex].width / outputUncroppedWidthInch ;
	}

	jimage.fotoliaLicense = view.fotoliaLicenses[jimage.fotoliaId][newLicenseIndex].name ;

}

function updateStockImagePrices() {
	if (typeof view.jdesign == 'undefined')
		return ;

	view.jdesign.stockImages = {} ; 
	for (c in view.jdesign.canvases){
		for(iii in view.jdesign.canvases[c].images){
			var im = view.jdesign.canvases[c].images[iii] ;

			if (typeof im.fotoliaId != 'undefined')	{
				if(typeof view.jdesign.stockImages[im.fotoliaId] == 'undefined') {
					view.jdesign.stockImages[im.fotoliaId] = {} ; 
					view.jdesign.stockImages[im.fotoliaId].fotoliaId = im.fotoliaId ;
					view.jdesign.stockImages[im.fotoliaId].maxLicense = im.fotoliaLicense ;
					view.jdesign.stockImages[im.fotoliaId].assigned = false ;
				}
				
				if(licenseValue(im.fotoliaLicense)  > licenseValue(view.jdesign.stockImages[im.fotoliaId].maxLicense))
					view.jdesign.stockImages[im.fotoliaId].maxLicense = im.fotoliaLicense ;
			}
		}
	}

	var totalStockImagePrice = 0;
	var totalStockImageCount = 0;

	for (c in view.jdesign.canvases){
		for(ii in view.jdesign.canvases[c].images){
			var im = view.jdesign.canvases[c].images[ii] ;

			if (typeof im.fotoliaId != 'undefined'){
				var priceTag = $('#outline_img_' + im.name).find('.stock-image-price-outline') ;
				if (im.fotoliaLicense == view.jdesign.stockImages[im.fotoliaId].maxLicense && ! view.jdesign.stockImages[im.fotoliaId].assigned){
					var price = getStockImagePrice(im.fotoliaId , im.fotoliaLicense) ;
					priceTag.html('$' + price) ;
					view.jdesign.stockImages[im.fotoliaId].assigned = true ;
					totalStockImagePrice +=	price;
					totalStockImageCount++ ;
				}
				else {
					priceTag.html('$0'); 
				}
			}
		}
	}

	$('#admin_stock_image_cost').html('Image Cost: $' + totalStockImagePrice);
	
	$('#status_stock_image_info').empty() ;
	if(totalStockImageCount > 0 && totalStockImagePrice > 0){
		$('#status_stock_image_info').html('Design contains <strong>'+totalStockImageCount+'</strong> stock image'+(totalStockImageCount > 1 ? 's' : '')+'. <strong>$'+totalStockImagePrice * 1 +'</strong> will be added to this order.') ;
		$('<span />').addClass('general-icon').css('background-position' , '-1040px 0px').css('cursor' , 'pointer')
		.appendTo('#status_stock_image_info').click(showStockImageHelp);
	}
}

function showStockImageHelp(){
	$('#gray_box').show() ;
	popup = createPopup('About Stock Images' , 550 ).fadeIn(300) ; 
	content = popup.find('.simple-popup-content').css('margin' , '30px') ; 
	content.html('<h3 style="margin: 10px 0 0 0">Buy it once, use it forever</h3>\
		Price of stock images used in your design will be added to your invoice. Once you pay for a stock image, it is saved to your account and you can use it in future designs free of charge.\
		<h3 style="margin: 20px 0 0 0">Price of a stock image depends on its size</h3>\
		Different sizes of a stock image cost differently. The larger the image, the higher its price. \
		That is why price changes as you resize a stock image.\
		<h3 style="margin: 20px 0 0 0">Watermarks and Resolution</h3>\
		When you add a stock image to your design you see some watermarks over it and \
		the resolution may seem to be low. \
		Upon completing your order, we replace watermarked versions with high-resolution versions \
		and your job will be printed without any watermarks and in high-resolution.');
}
function getStockImagePrice(fotolia_id , license){
	for (i in view.fotoliaLicenses[fotolia_id]){
		if(view.fotoliaLicenses[fotolia_id][i].name == license){
			return view.fotoliaLicenses[fotolia_id][i].price * 1 ;
		}
	}
	return 0 ; 
}
function licenseValue(l){
	switch(l){
		case 'XS':
			return 10;
		case 'S':
			return 20;
		case 'M':
			return 30;
		case 'L':
			return 40;
		case 'XL':
			return 50;
		case 'XXL':
			return 60;
		case 'XXXL':
			return 70;
	}
	return 0;
}
function arrangeImageZees(){
	var zArray = [];

	for (i in view.jdesign.canvases[view.currentPage].images ) {
		zArray.push(view.jdesign.canvases[view.currentPage].images[i].zindex) ;
	}

	zArray.sort(function(a,b) {return a - b});
	var jcanvasClone = JSON.clone(view.jdesign.canvases[view.currentPage]) ; 
	
	var bgIndex = 0 ; 
	var maskIndex = 0 ;
	var regularIndex = 0 ; 
	
	for(j in zArray) {
		elementIndex = getElementIndex(	view.jdesign.canvases[view.currentPage].images , "zindex" ,  zArray[j] * 1  ) ;
		jimageClone =jcanvasClone.images[elementIndex ] ; 
		
		if (jimageClone.type == 'bg') {
			jimageClone.zindex = bgIndex + 100 ;
			bgIndex++ ;
		}
		else if (jimageClone.type == 'mask') {
			jimageClone.zindex = maskIndex + 300 ;
			maskIndex++ ;
		}
		else {
			jimageClone.zindex = regularIndex + 200 ;
			regularIndex++ ;
		}

		$("#outline_img_" + jimageClone.name).css('z-index' , jimageClone.zindex * 1 ) ;
		$("#img_" + jimageClone.name).css('z-index' , jimageClone.zindex * 1 ) ;
	}

	arrangeOutlineZindex() ;
	view.jdesign.canvases[view.currentPage] = jcanvasClone ; 
}


function bringForward() {
	$('.selected').each( function() {
		getElem( $(this).attr('id').split('outline_')[1] ).zindex +=  1.1 ;
		arrangeImageZees() ;
	}) ; 
	pushState();
}

function sendBackward() {
	$('.selected').each( function() {
		getElem( $(this).attr('id').split('outline_')[1] ).zindex -=  1.1 ;
		arrangeImageZees() ;
	}) ; 
	pushState();
}
function sendToBack() {
	$('.selected').each( function() {
		getElem( $(this).attr('id').split('outline_')[1] ).zindex =  0.1 ;
		arrangeImageZees() ;
	}) ; 
	pushState();
}
function bringToFront() {
	$('.selected').each( function() {
		getElem( $(this).attr('id').split('outline_')[1] ).zindex =  500 ;
		arrangeImageZees() ;
	}) ; 
	pushState();
}

function highlight(e) {
	$(this).not('.non-selectable').not('.selected').addClass('highlighted') ; 
}

function lowlight() {
	$(this).removeClass('highlighted') ; 
}

/* ------------ Handles selection / multi-selection of elements upon a (crtl) click/ ------------ */

function selectElement(e) {
//	$('#temp_text').blur();
	ie8SafePreventEvent(e) ; 
	$('.dragged').removeClass('dragged')
	view.initialMousePosX =  e.pageX ; 
	view.initialMousePosY =  e.pageY ; 
	view.mouseDownX = e.pageX ;
	view.mouseDownY = e.pageY ;

	if ($(this).hasClass('fitted')) 
		$(this).addClass('croppable');		

	if ($(this).hasClass('croppable')){
 			$('.selected').removeClass('selected') ; 
			$(this).removeClass('highlighted').addClass('selected').addClass('qcrop');
			if(! $(this).hasClass('fitted'))
				$('#'+$(this).attr('id').split('outline_')[1]).addClass('crop');
	}
	else {
		exitInlineCrop();
		if (! e.ctrlKey && ! e.shiftKey) {    //  Clicking on an elements without holding the CTRL key
			if($(this).hasClass('selected') && ! $(this).hasClass('unmovable')) {
//				$('.selected').not('.unmovable').addClass('grabbed') ;
			}
			else {
				$('.selected').removeClass('selected') ;
				$(this).removeClass('highlighted').addClass('selected') ;			
//				$('.selected').not('.unmovable').addClass('grabbed') ;
			}
		}
	
		else {		//  Clicking on an elements while holding the CTRL key
			stopPropag(e);
			view.alignToolAutoOpen = true ;
			if($(this).hasClass('selected')) 
			{	
				$(this).removeClass('selected') ;
			}
			else {
				//stopPropag(e);
				//view.alignToolAutoOpen = true ;
				$(this).removeClass('highlighted').addClass('selected') ;
				$(".selected").not('.unmovable').addClass('grabbed') ;
			}
		}
	}
	// In case the element is non-selectable, it cancels the selection
	if ($(this).hasClass('non-selectable') || $(this).hasClass('locked') ) {
		$(this).removeClass('selected').removeClass('grabbed') ;   
		view.selectorX = e.pageX - getCanvasX() ;
		view.selectorY = e.pageY - getCanvasY() ;
	}
	 if ($(this).hasClass('locked') )
		$(this).addClass('highlighted') ; 

	arrangeOutlineZindex() ;
	updateToolbar() ; 	
	setActiveTextboxPosition() ; 
	updateSnapPoints();


}

function grabElement(e){
	$('.selected').not('.unmovable').addClass('grabbed') ;
}

function createColorPicker(){
	toolbar.colorPicker = $('<div />') ;
	$.each(config.color_palette, function(k,v){
		$('<div />').addClass('color-pick')
					.css('background-color' , '#'+ v )
					.appendTo(toolbar.colorPicker) ;
	})
}

function updateToolbar(){
//	console.log('updateToolbar');
//	$('.ztoolbar').css('z-index', 2500);
	$('.float-toolbar, #textbox_tools').hide() ; // .css('z-index' , 2510);
	if($('.left-textarea:focus').length == 0){
		$('#temp_text').focus();
	}
	//$('#btn_top_toolbar_toggle').html( view.showAdvancedToolbar ? 'Hide Options' : 'Show Options');
    $('#btn_top_toolbar_toggle').removeClass('open').addClass( view.showAdvancedToolbar ? 'open' : '');
	if(view.theme_id){
		$('#theme_' + view.theme_id).hide().siblings().show();
	}
	if(user.download_pdf)
		$('#btn_save_pdf').show();

	var o = $('#outline_layers>.selected') ;
	$('.toolbar-button').removeClass('on').removeClass('disabled');
	$('.button-textbox').removeAttr('disabled');
	
	$('.textbox-n-friends').removeClass('active2');
	$('.color-pick').removeClass('on');
	
	if (o.filter('.text').length == 0){
		$('#text_tools_section .toolbar-button').addClass('disabled');
		$('#text_tools_section .button-textbox').attr('disabled' , 'disabled');
		$('input#txt_selected_font_color').spectrum('disable', true);
    }else{
        $('#text_tools_section').css('display', '');
        $('input#txt_selected_font_color').spectrum('enable', true);
    }
	if(view.snapEnabled)
		$('#btn_view_snap').addClass('on');

	if(view.zoomLevel >= config.maxZoomLevel) 
		$('#btn_view_zoom_in').addClass('disabled') ;
	
	if(view.zoomLevel <= config.minZoomLevel) 
		$('#btn_view_zoom_out').addClass('disabled') ;
	
	if(view.stateIndex == 0) 
		$('#btn_edit_undo').addClass('disabled') ;
	
	if(view.stateIndex == view.states.length - 1 || view.states.length ==0) 
		$('#btn_edit_redo').addClass('disabled') ;

	if(view.showBleed)
		$('#btn_view_bleed').addClass('on');

	if(view.showGrid)
		$('#btn_view_grid').addClass('on');

	if(view.profileEQ.show_text_panel)
		$('#btn_view_quick_edit').addClass('on');

	if(view.clipboard.length == 0)	
		$('#btn_edit_paste').addClass('disabled') ;
		
	if(o.length == 0)
		$('#btn_edit_delete').addClass('disabled') ;

	if(o.not('.pholder').length  == 0)
		$('#btn_edit_copy').addClass('disabled') ;

	if(o.length != 1 || o.filter('.image').length != 1)
		$('#btn_advance_crop').addClass('disabled') ;

	if(o.not('.text').not('.pholder').length == 0)
		$('#btn_advance_layers').addClass('disabled');

	if(o.not('.unmovable').length == 0)
		$('#btn_advance_align').addClass('disabled');

	if(o.filter('.text , .image').not('.unmovable').length != 1 || o.length > 1)
		$('#btn_advance_rotate').addClass('disabled');
	
	if(o.length != 1 || o.filter('.image , .shape').not('.pholder').length != 1)
		$('#btn_edit_lock').addClass('disabled');

	var attrs = {"font" : [], "pointsize" : [], "color" : [], "gravity" : [], "bold" : [], "italic" : [] , "fit_to_box" : [] , "bullet_style": []} ;
	o.filter('.text').each(function(){
		var jtext = getElem($(this).attr('id').split('outline_').pop()) ; 

		if ($.inArray(jtext.font, attrs.font) == -1)
			attrs.font.push(jtext.font);

		if ($.inArray(jtext.pointsize, attrs.pointsize) == -1)
			attrs.pointsize.push(jtext.pointsize);

		if ($.inArray(jtext.bold, attrs.bold) == -1)
			attrs.bold.push(jtext.bold);

		if ($.inArray(jtext.italic, attrs.italic) == -1)
			attrs.italic.push(jtext.italic);

		if ($.inArray(jtext.gravity, attrs.gravity) == -1)
			attrs.gravity.push(jtext.gravity);

		if ($.inArray(jtext.color, attrs.color) == -1)
			attrs.color.push(jtext.color);

		if ($.inArray(!! jtext.fit_to_box , attrs.fit_to_box) == -1)
			attrs.fit_to_box.push(!! jtext.fit_to_box );

		if ($.inArray(jtext.bullet_style , attrs.bullet_style) == -1)
			attrs.bullet_style.push(jtext.bullet_style);

		$('#tbox_txt_' + jtext.name).closest('.textbox-n-friends').addClass('active2');
	});
	
	
	var selectedFontFace = attrs.font.length == 1 ? attrs.font[0] : '' ;
	$('#selected_font_face').html(selectedFontFace) ; 
	$('#selected_font_size').val((attrs.pointsize.length == 1 && attrs.fit_to_box.length == 1) ? attrs.pointsize[0] : '') ; 
	$('#selected_font_color').css('background-color' , attrs.color.length == 1 ?  '#'+attrs.color[0].split('#')[0] : '' ) ; 
	$('#btn_text_bold').addClass(attrs.bold.length == 1 && attrs.bold[0] ? 'on' : ''  ) ;
	$('#btn_text_italic').addClass(attrs.italic.length == 1 && attrs.italic[0] ? 'on' : ''  ) ;
	$('#btn_text_gravity_' + attrs.gravity[0]).addClass(attrs.gravity.length == 1 ? 'on' : '');
	if(attrs['bullet_style'].length == 1){
		$('#btn_text_bullet_' + attrs['bullet_style'][0] ).addClass('on').closest('.toolbar-button.has-menu').addClass('on');
		if(attrs['bullet_style'][0] === undefined){
			$('#btn_text_bullet_none, #btn_text_number_none').addClass('on');
		}
	}
	$('#btn_text_bold').addClass( (config.fonts[selectedFontFace] && config.fonts[selectedFontFace].bold === false) ? 'disabled' : '') ;
	$('#btn_text_italic').addClass( (config.fonts[selectedFontFace] && config.fonts[selectedFontFace].italic === false) ? 'disabled' : '') ;

	$.each(attrs.color, function(k,v){
		$('#font_color_menu .color-'+ v.replace('#' , '_')).addClass('on');
	});

	if(attrs.fit_to_box.length == 1 && attrs.fit_to_box[0] === true){
		$('#selected_font_size').val('auto');
		$('#btn_text_auto_fit').addClass('on');
		$('#btn_text_bullet , #btn_text_number').addClass('disabled');
//		$('#text_tools_section .button-textbox').attr('disabled' , 'disabled');
//		$('#btn_text_font_size').addClass('disabled');
	}
		
	if (! view.profileEQ.show_text_panel && o.length == 1 && o.filter('.text').length == 1){
		var tarea = $('#inline_textarea') ;
		var tarea_preset = $('#inline_textarea_preset') ;
		var tarea_title = $('#inline_textarea_title') ;
		var jText = getElem(o.attr('id').split('outline_').pop()) ;
		tarea.val( decodeURIComponent(jText.body ));
        tarea_preset.val(decodeURIComponent(jText.preset_text ));
        tarea_title.val(decodeURIComponent(jText.title ));
		// $('#textbox_tools').show();
		$('#image_toolbar').hide();
		$('#shape_toolbar').hide();
		$('#textbox_tools_text_title').html( (jText.title.search(/text/i) == -1 ? decodeURIComponent(jText.title)+":" : 'Enter Text:'));

		$('#inline_text_name').val(jText.name);
		// tarea.cssHeight(18);
		// tarea.cssHeight(tarea[0].scrollHeight + 2);
		// tarea.removeClass('fit').addClass(jText.fit_to_box ? 'fit' : '');
	}
	else {
		$('#inline_textarea').blur();
		$('#textbox_tools').hide();
	}

	var focusedLeftTextarea = $('.left-textarea:focus');
	if(focusedLeftTextarea.length > 0 && ! $('#' + focusedLeftTextarea.attr('id').replace('tbox_' , 'outline_')).is('.selected') ){
		$('.left-textarea:focus').blur();
	}
	
	if(o.length == 1 && o.filter('.shape').length == 1 && ( o.filter('.pholder').length == 0 || o.filter('.admin').length   == 1) ){
		var el = getElem(o.attr('id').split('outline_')[1]) ;
		if(el.shape_type.search('line') == -1) 
			$('#btn_shape_fill_color').show();
		else
			$('#btn_shape_fill_color').hide();
			
		$('#text_tools_section').hide();
		$('#image_toolbar').hide();
		$('#shape_toolbar').show();
		$('#shape_fill_color_menu .color-'+ el.fill_color.replace('#' , '_')).addClass('on');
		$('#shape_border_color_menu .color-'+ el.border_color.replace('#' , '_')).addClass('on');
		$('#btn_shape_border_color .selected-color').css('background-color' , '#' + el.border_color.split('#')[0]);
		$('#btn_shape_fill_color .selected-color').css('background-color' ,'transparent').css('background-color' , '#' + el.fill_color.split('#')[0]);
		$("#txt_selected_shape_border_color").spectrum("set" , "#"+el.border_color);
		$("#txt_selected_shape_fill_color").spectrum("set" , "#"+el.fill_color);
	}

	if(o.length == 1 && o.filter('.image').length == 1 && o.filter('.croppable').length == 0 && ( o.filter('.pholder').length == 0 || o.filter('.admin').length  == 1) ){
		var el = getElem(o.attr('id').split('outline_')[1]) ;
		var el_border_color =  el.border_color ? el.border_color : 'transparent' ;
		$('#image_toolbar').show();
		$('#text_tools_section').hide();
		$('#shape_toolbar').hide();

        /*Add some */

        var mask = el.type;
        if(mask=='mask') $('#btn_image_mask').addClass('on');
        var placeholder = el.placeholder;
        if(placeholder=='fitter') $('#btn_image_holder_fitter').addClass('on');
        var noprint = el.noprint;
        if(noprint) $('#btn_image_noprint').addClass('on');
        //var uploader = el.uploader;
        //if(uploader) $('#btn_image_upload').addClass('on');
        if(placeholder=='filler') $('#btn_image_holder_filler').addClass('on');
        if(placeholder=='none' || placeholder=='' || placeholder+''=='undefined') $('#btn_image_holder_none').addClass('on');
        var noselect = el.noselect;
        if(noselect) $('#btn_image_noselect').addClass('on');
        var noresize = el.noresize;
        if(noresize) $('#btn_image_noresize').addClass('on');
        var unmovable = el.unmovable;
        if(unmovable) $('#btn_image_unmovable').addClass('on');

        /*End Add some */

		$('#image_border_color_menu .color-'+ el_border_color.replace('#' , '_')).addClass('on');
		$('#btn_image_border_color .selected-color').css('background-color' , 'transparent').css('background-color' , '#' + el_border_color.split('#')[0]);
		$('#svg_colors').empty();
		if(o.hasClass('svg') && (el.svg_colors.length < 8 || user.admin_mode) ){
			$.each(el.svg_colors, function(k,v){
				$('<div />').addClass('selected-color svg-color-item menu-opener').css('background-color' , '#' + v.split('#')[0] ).appendTo('#svg_colors');
			});
		}
		$("#txt_selected_image_border_color").spectrum("set" , "#"+el.border_color);
	}
	
	if(o.length == 1 && o.filter('.image').length == 1 && o.filter('.croppable').length == 1 ){
		$('#crop_toolbar').show();
	}
	
	$('#bg_color_menu .color-' + view.jdesign.canvases[view.currentPage].bg_color.replace('#' , '_')).addClass('on');
	$('#selected_bg_color').css('background-color' , '#' + view.jdesign.canvases[view.currentPage].bg_color.split('#').shift() );

	if (o.filter('.admin').length == 1 && o.filter('.text').length == 0 && o.length == 1){
		var pos = findPos(o[0]);
		$('#admin_tools').show().cssTop(pos[1] + 20 ).cssLeft(pos[0] + o.cssWidth()) ;
		$('#admin_tools_menu').hide().siblings().show();
		var el = getElem(o.attr('id').split('outline_')[1]) ;
		$('#admin_tools_name').val(el.name);
		$('#admin_tools_ph').val(el.placeholder);
		$('#admin_tools_type').val(el.type);

		$('#admin_tools_width').val(el.width);
		$('#admin_tools_height').val(el.height);
		$('#admin_tools_left').val(el.left);
		$('#admin_tools_top').val(el.top);
		
		$('#admin_tools_noprint').attr('checked' , !! el.noprint ) ; 
		$('#admin_tools_noselect').attr('checked' , !! el.noselect ) ; 
		$('#admin_tools_unmovable').attr('checked' , !! el.unmovable) ; 
		$('#admin_tools_noresize').attr('checked' , !! el.noresize ) ; 
		$('#admin_tools_invisible').attr('checked' , !! el.invisible ) ; 
		$('#admin_tools_uploader').attr('checked' , !! el.uploader) ; 
	}
	
	$('#page_navigation_' + view.currentPage).addClass('on');
	
	$('#info_design_title').html(view.designName);
	$('#info_product_title').html(getProductDesc());
	
	positionToolbars();
}


function updateTextToolbars(){
//	console.log('updateTextToolbars');
	var tarea = $('#inline_textarea') ;
	var o = $('.selected') ;
	if(! view.profileEQ.show_text_panel && o.length == 1 && o.filter('.text').length == 1){
		// $('#textbox_tools').show();
		if(o.hasClass('dragged') || user.admin_mode === 1)
			toggleTextboxTool(false);
		else 
			toggleTextboxTool(true);
	}
	else{
		$('#textbox_tools').hide();
	}

}

function toggleTextboxTool(value){
//	console.log('toggleTextboxTool' , value);

	if(typeof value == 'undefined'){
		value = $('#textbox_tools').hasClass('close');
	}
	
	if(value){
		$('#textbox_tools').removeClass('close').addClass('open');
		var tarea = $('#inline_textarea') ;
			
		if(! tarea.is(':focus'))
			tarea.focus()
	
		tarea.cssHeight(18);
		tarea.cssHeight(tarea[0].scrollHeight + 2)
		tarea.cssWidth(200);
	}
	else{
		$('#inline_textarea').blur();
		$('#textbox_tools').addClass('close').removeClass('open');
	}
	positionToolbars();
}

function setActiveTextboxPosition() {
	var o = $('.selected.text');
	if (o.length == 1) {
		var activeTxtbox = $('#tbox_' + o.attr('id').split('outline_').pop()) ;
		$('#text_fields')[0].scrollTop = 0
		var txtboxTop =  activeTxtbox.parent().position().top ; 
		$('#text_fields')[0].scrollTop = $('#text_fields')[0].scrollTop + ( txtboxTop - $('#text_fields').cssHeight() ) - 100 ;
	}
}


/* ---- This function puts the selected outline on top of all other outlines and put it back when it is de-selected ---- */		

function arrangeOutlineZindex() {
	$(".outline2").each(function() {
		if ($(this).hasClass('selected') && $(this).css('z-index') * 1 < 1000 )
			$(this).css('z-index' , $(this).css('z-index') * 1 + 1000)  ;
		if ( ! $(this).hasClass('selected') && $(this).css('z-index') * 1  >= 1000 )
			$(this).css('z-index' , $(this).css('z-index') * 1 % 1000)  ;
	}) ;
}


function dragElement(e) {
	ie8SafePreventEvent(e) ; 
	view.lastMousePosX = e.pageX ; 
	view.lastMousePosY = e.pageY ; 
	var dragX = view.lastMousePosX - view.initialMousePosX ;
	var dragY = view.lastMousePosY - view.initialMousePosY ;

	view.outdatedJdesign = true ;
		
	view.initialMousePosX = view.lastMousePosX ;
	view.initialMousePosY = view.lastMousePosY ;

	var cropEl = $('.qcrop') ;
	if (cropEl.length > 0){
		var inner = $('#' + cropEl.attr('id').replace('outline' , 'inner') ) ;
		var oldLeft = inner.cssLeft() ; 
		var oldTop = inner.cssTop() ;
		var newLeft = oldLeft + dragX ; 
		var newTop = oldTop + dragY  ; 

		inner.cssLeft(Math.max(cropEl.cssWidth() - inner.cssWidth() ,Math.min(newLeft, 0)));
		inner.cssTop(Math.max(cropEl.cssHeight() - inner.cssHeight() ,Math.min(newTop, 0)));
		return;
	};

	var dragX = view.mousePosX - view.mouseDownX ;
	var dragY = view.mousePosY - view.mouseDownY ;
	if(Math.abs(dragX) > config.dragLockMargin ){
		view.dragLockX = false ;
	}
	if(Math.abs(dragY) > config.dragLockMargin){
		view.dragLockY = false ;
	}
	if(view.dragLockX && view.dragLockY)
		return;
	else{
	 	$('.float-toolbar, #textbox_tools').hide();
	}


	$('.safe-zone-mask').show();
	var grabbed = $('.grabbed') ;
	$('.snap-guide').hide();
	grabbed.each(function(){
		$(this).addClass('dragged');
		var elementId = $(this).attr('id').split('outline_').pop() ;
		var el = getElem(elementId);
		var newLeft = el.left * view.dpi  + (view.dragLockX ? 0 : dragX) ;
		var newTop = el.top * view.dpi  + (view.dragLockY ? 0 : dragY) ;
		if(view.snapEnabled && grabbed.length == 1 && ! e.ctrlKey ){
			var currentEl = $(this) ;
			$.each(view.snapX, function(k,v){
				if(Math.abs(v - newLeft) < config.snapMargin ){
					newLeft = v ; 
					$('#snap_guide_x').cssLeft(v).show();
					return false;
				}
				if(Math.abs(v - newLeft - currentEl.cssWidth() ) < config.snapMargin ){
					newLeft = v - currentEl.cssWidth() ; 
					$('#snap_guide_x').cssLeft(v).show();
					return false;
				}

			});
			$.each(view.snapY, function(k,v){
				if(Math.abs(v - newTop) < config.snapMargin ){
					newTop = v ; 
					$('#snap_guide_y').cssTop(v).show();
					return false;
				}
				if(Math.abs(v - newTop - currentEl.cssHeight() ) < config.snapMargin ){
					newTop = v - currentEl.cssHeight() ; 
					$('#snap_guide_y').cssTop(v).show();
					return false;
				}
			});
		}
		$(this).cssLeft(newLeft).cssTop(newTop) ; 
		$('#'+elementId).cssLeft(newLeft).cssTop(newTop);

	});	

}

/* --- This function deletes the selected elements on canvas and their corresponding outlines --- */
function deleteSelectedElements() {
	if ($('.selected').length == 0) return ;
	$(".selected").each(function() {
		var element_id = $(this).attr('id').split('outline_')[1] ;
		var element_name = element_id.substr(4) ;

		if (element_id.split('_').shift() == 'txt') { // if it's a text element
			deleteTextElementByName(element_name) ; 			
		}
		else if (element_id.split('_').shift() == 'img') { // if it's an image element
			deleteImageElementByName(element_name, true) ; 			
		}
		
	}) ;

	view.outdatedJdesign = true ;
	pushState() ;
	updateToolbar();
}

function deleteTextElementByName(name){
	$("#txt_" + name).remove() ; 
	$("#outline_txt_" + name).remove() ; 
	var element_index = getElementIndex(view.jdesign.canvases[view.currentPage].texts , "name" , name )  ;
	view.jdesign.canvases[view.currentPage].texts.splice(element_index, 1) ;
	$('#tbox_txt_' + name).closest('.textbox-n-friends').remove() ; 
}

function createTextbox(jtext) {
	var temp = $('<div />').addClass('textbox-n-friends').appendTo('#text_fields') ;

	if(!! jtext.hide_in_panel)
		temp.hide() ; 

	var wrapper = $('<div />').addClass('tarea-wrapper').appendTo(temp);
	var tarea = $('<textarea />').addClass('left-textarea').attr('id' , 'tbox_txt_' + view.jdesign.canvases[view.currentPage].texts[i].name ).appendTo(wrapper);
	tarea.addClass(jtext.fit_to_box ? 'fit' : '');
	var bodyText = decodeURIComponent(view.jdesign.canvases[view.currentPage].texts[i].body) ;

	var label = $('<div />').addClass('tarea-label').html(decodeURIComponent(jtext.title)).click(function(){$(this).siblings('textarea').focus()}).appendTo(wrapper);

	var titleText = decodeURIComponent(view.jdesign.canvases[view.currentPage].texts[i].title) ;
	if (bodyText.length > 0){
		tarea.html(bodyText);
		tarea.closest('.tarea-wrapper').removeClass('empty') ; 
	} 
	else {
		tarea.closest('.tarea-wrapper').addClass('empty') ; 
	}

	tarea.keydown(function(e){
		if(e.keyCode == 13 && $(this).hasClass('fit'))
			return false ;

		if($(this).val().length > 0 )
			$(this).closest('.tarea-wrapper').removeClass('empty');
		else
			$(this).closest('.tarea-wrapper').addClass('empty');
	});

	tarea.keyup(function(e) { 
//		stopPropag(e);
		var element_id = $(this).attr('id').split('tbox_').pop() ;
		var jtext = getElem(element_id) ;
		updateTextBody(jtext, $(this).val());
	}) ; 
	
	// When user blur it won't wait for timer to update the text (does it immediately)
	tarea.blur(function() {
		
		
//		var text_id = $(this).attr('id').split('tbox_')[1];
//		var jtext = getElem(text_id) ;
//		$(this).closest('.active2').removeClass('active2') ;
		
//		if (jtext.body.length == 0){
//				$(this).addClass('empty').val(getElem(text_id).title) ;
/*
			if (! jtext.show_preset)
				$('#outline_' + text_id).addClass('non-selectable');
*/
//		}

	}) ; 

	tarea.focus(function(e) {
		selectElement.call(document.getElementById('outline_'+this.id.split('tbox_').pop()), e); 
		$(this).cssHeight(18);
		$(this).cssHeight(tarea[0].scrollHeight + 2);
	}) ; 
	
	//Binding paste cut and copy of browser's context menu
	tarea.bind('paste cut', function() { 
		var el = $(this); 
		setTimeout(function() { 
			$(el).keyup(); 
		}, 100); 
	}); 
	
}

function updateTextElement(element_id) {
	var jtext = getElem(element_id) ; 
	var link1 = '';
	
	if(jtext.pointsize == null || isNaN(jtext.pointsize))
		jtext.pointsize = config.fontSizes[0];
		
	if(typeof jtext.rotation == 'undefined')
		jtext.rotation = 0 ;
		
	var textEl = $('#' + element_id) ; 
	
	textEl.cssTop(jtext.top * view.dpi ) ; 
	textEl.cssLeft(jtext.left * view.dpi ) ; 

	tempTextImages[element_id] = new Image() ;
	tempTextTimestamps[element_id] = (new Date()).getTime(); ;

	$(tempTextImages[element_id]).attr('id' , 'temp_' + element_id) ;
	
	var txtBodyToShow = ((typeof jtext.show_preset == 'undefined' || jtext.show_preset === true ) && jtext.body.length == 0) ? jtext.preset_text : jtext.body ;
	
	if(! jtext.fit_to_box){
		//link1 = 'ajax/text/getText.php?body=' +  txtBodyToShow
		link1 = '/tools.php?action=assets&action_type=create_text&body=' +  txtBodyToShow
								+ '&pointsize=' + jtext.pointsize
								+ '&font=' +  jtext.font 
								+ '&fill=' +  jtext.color.split('#')[0]
								+ ((jtext.rotation == 0 || jtext.rotation == 180) ? '&width=' +  jtext.width.toFixed(3) : '')
								+ ((jtext.rotation == 90 || jtext.rotation == 270) ? '&height=' +  jtext.height.toFixed(3) : '' )
								+ '&gravity=' +  jtext.gravity
								+ '&density=' +  view.dpi 
								+ '&bold=' +  jtext.bold
								+ '&italic=' +  jtext.italic
								+ '&rotation=' + jtext.rotation
								+ '&bullet_style=' +  jtext.bullet_style  ;
		tempTextImages[element_id].src = link1  ;
		}
	else {
		//link1 = 'ajax/text/getText.php?body=' +  txtBodyToShow
		link1 = '/tools.php?action=assets&action_type&body=' +  txtBodyToShow
							+ '&font=' +  jtext.font 
							+ '&fill=' +  jtext.color.split('#')[0]
							+ '&width=' +  jtext.width.toFixed(3)
							+ '&height=' +  jtext.height.toFixed(3)
							+ '&gravity=' +  jtext.gravity
							+ '&density=' +  view.dpi 
							+ '&bold=' +  jtext.bold
							+ '&italic=' +  jtext.italic
							+ '&rotation=' + jtext.rotation ;	
	    tempTextImages[element_id].src = link1;
    }
    //alert(element_id);

	$(tempTextImages[element_id]).data( 'timestamp' , tempTextTimestamps[element_id]);
	

	if (tempTextImages[element_id].complete)
		replaceText(tempTextImages[element_id]) ;
	else {
		tempTextImages[element_id].onload = function() {replaceText(this)} ; 
	}
/*
	if (txtBodyToShow.length == 0 && user.admin_mode)
		if (! $('#tbox_txt_' + jtext.name).closest('.textbox-n-freinds').hasClass('active2'))
			$('#outline_txt_' + jtext.name).addClass('non-selectable') ; 
	else {
		$('#outline_txt_' + jtext.name).removeClass('non-selectable') ; 
	}
*/
}

function replaceText(temp_txt) { 

	var element_id = $(temp_txt).attr('id').split('temp_')[1] ; 
	
	if($(temp_txt).data('timestamp') * 1 != tempTextTimestamps[element_id] * 1){
		$(temp_txt).remove();
		return ;
	}
	$('#' + element_id).attr('src' , temp_txt.src )  ;

	var jtext = getElem(element_id) ; 

	jtext.width = temp_txt.width / view.dpi ; 
	jtext.height = temp_txt.height / view.dpi ; 
	
	var newW = Math.max(temp_txt.width, 10) ;
	var newH = Math.max(temp_txt.height, 10) ;

	var oldW = $('#outline_' + element_id).cssWidth() ; 			
	var oldH = $('#outline_' + element_id).cssHeight() ; 
	
	if(jtext.rotation == 90){
		jtext.left -= (newW - oldW) / view.dpi ;
	}

	if(jtext.rotation == 180 && oldH > 0){
		jtext.top -= (newH - oldH) / view.dpi ;
	}

	$('#' + element_id).cssWidth(temp_txt.width).cssHeight(temp_txt.height).cssLeft(jtext.left * view.dpi ).cssTop(jtext.top * view.dpi) ;
		
	$('#outline_' + element_id).cssWidth(newW).cssHeight(newH).cssLeft(jtext.left * view.dpi).cssTop(jtext.top * view.dpi) ; 
//	$('#outline_' + element_id).find('.size-measure').html( 'W: ' + (newW / view.dpi).toFixed(2) + '&quot; x H:' + (newH / view.dpi).toFixed(2) + '&quot;' );;
	$('#outline_' + element_id).find('.size-measure.w .value').html((newW / view.dpi).toFixed(2) + '&quot;' );
	$('#outline_' + element_id).find('.size-measure.h .value').html((newH / view.dpi).toFixed(2) + '&quot;' );

	$(temp_txt).remove();
} 

function rotateElements(){
	$('.selected.text').each(function() {
		var t = getElem($(this).attr('id').split('outline_')[1]) ;
		if (typeof t.rotation == 'undefined')
			t.rotation = 0 ;
			
		t.rotation = (t.rotation + 90) % 360; 
		t.left = t.left + (t.width - t.height) / 2 ; 
		t.top  = t.top + (t.height - t.width) / 2 ; 
		temp = t.width ;
		t.width = t.height ;
		t.height = temp ; 
		$(this).cssWidth(t.width * view.dpi) ; 
		$(this).cssHeight(t.height * view.dpi) ; 
		$(this).cssLeft(t.left * view.dpi) ; 
		$(this).cssTop(t.top * view.dpi); 
		updateTextElement("txt_" + t.name) ; 
		
	}) ; 

	$('.selected.image').each(function() {

		var jimage = getElem($(this).attr('id').split('outline_').pop() ) ;
			
		var temp = JSON.clone(jimage) ;
		jimage.rotation = ( (temp.rotation ? temp.rotation : 0) + 90) % 360 ; 
		jimage.width = temp.height ;
		jimage.height = temp.width ; 
		jimage.cropTop = temp.cropLeft ; 
		jimage.cropLeft = 1 - temp.cropTop - temp.cropHeight ; 
		jimage.cropWidth = temp.cropHeight  ; 
		jimage.cropHeight = temp.cropWidth ;

		jimage.left = jimage.left + (jimage.width - temp.width) / 2 ;
		jimage.top = jimage.top + (jimage.height - temp.height) / 2 ;
	
		drawImageElement(jimage) ; 
		updateStockImagePrices();
		$("#outline_img_"+jimage.name).addClass("selected") ;
	});
	
	pushState();
	updateToolbar();
}

function applyTextAttr(attr_name , attr_value) {
	$('.selected.text').each(function() {
		var jtext = getElem($(this).attr('id').split('outline_')[1]) ;
		
		jtext[attr_name] =  attr_value ;
		if(attr_name=='pointsize' && jtext.fit_to_box){
			jtext.fit_to_box = false ;
		}

		// Rremoves tha bold and italic attributes from the text whose new font does not support it
		if (jtext.bold && ! config.fonts[jtext.font]['bold'] ) { jtext.bold = false ;  }
		if (jtext.italic && ! config.fonts[jtext.font]['italic'] ) { jtext.italic = false ; }

		updateTextElement($(this).attr('id').split('outline_')[1]) ; 
		view.outdatedJdesign = true ;

	}) ;
	pushState() ;
	updateToolbar();
}

/*Add some*/
function applyImageAttribute(attr_name, id){
    $('.selected[id^="outline_img_"]').each(function() {
        var element_id = $(this).attr('id').split('outline_')[1] ;
        var jElement = getElem(element_id) ;
        var attr_value ;
        switch(attr_name){
            case 'type':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = 'regular';
                attr_value = attr_value=='regular'?'mask':'regular';
                jElement[attr_name] = attr_value;
                break;
            case 'placeholder':
                //attr_value = jElement[attr_name];
                //if(typeof attr_value == 'undefined') attr_value = "none";
                //attr_value = attr_value==''?'fitter':'';
                //jElement[attr_name] = attr_value;
                attr_value = id.replace('btn_image_holder_','');
                if(attr_value=='none') attr_value = '';
                jElement['placeholder'] = attr_value;
                break;
            case 'noselect':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = false;
                attr_value = !attr_value;
                jElement[attr_name] = attr_value;
                break;
            case 'noresize':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = false;
                attr_value = !attr_value;
                jElement[attr_name] = attr_value;
                break;
            case 'unmovable':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = false;
                attr_value = !attr_value;
                jElement[attr_name] = attr_value;
                break;
            case 'noprint':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = false;
                attr_value = !attr_value;
                jElement[attr_name] = attr_value;
                break;
            case 'uploader':
                attr_value = jElement[attr_name];
                if(typeof attr_value == 'undefined') attr_value = false;
                attr_value = !attr_value;
                jElement[attr_name] = attr_value;
                break;
        }
        //alert(attr_value);
        //jElement[attr_name] = attr_value ;
    }) ;
    pushState() ;
    updateToolbar();

}

function applyImageAttr(attr_name , attr_value, colorIndex) {
	$('.selected[id^="outline_img_"]').each(function() {
		var element_id = $(this).attr('id').split('outline_')[1] ;
		var jElement = getElem(element_id) ;
		jElement[attr_name] = attr_value + (typeof colorIndex != 'undefined' ? '#' + colorIndex  : '' ) ; 
		
		if(typeof(jElement.border_color)=='undefined' && attr_name == 'border_width' && attr_value > 0)
			jElement.border_color = '000000' ;
			
		if(typeof(jElement.border_width)=='undefined') jElement['border_width'] = 0 ;
		if(typeof(jElement.border_color)=='undefined') jElement['border_color'] = 'FFFFFF';
		
		if(attr_name == 'border_color' && jElement.border_width == 0){
			jElement.border_width = 1 ;
		}

		
		var imgBorder = $('#border_'+element_id) ;
		imgBorder.css('border-width' , .013836 * jElement.border_width * view.dpi + 'px');
		imgBorder.css('border-color' , '#' + jElement.border_color.split('#')[0]);
	}) ;
	pushState() ;
	updateToolbar();
}

function applyImageFilter(attr_value) {
    var element_id = $('.selected[id^="outline_img_"]').attr('id').split('outline_')[1] ;
    //console.log(element_id);
    var jElement = getElem(element_id) ;
    jElement['filter'] = attr_value;
    //console.log(jElement);
    $('#inner_'+element_id).attr('src' ,  getMockupSrc(jElement) ) ;
    pushState();
}

function applyShapeAttr(attr_name , attr_value, colorIndex ) {
	$('.shape.selected[id^="outline_img_"]').each(function() {
		var element_id = $(this).attr('id').split('outline_')[1] ;
		jElement = getElem(element_id) ;
		jElement[attr_name] = attr_value +  (typeof colorIndex != 'undefined' ? '#' + colorIndex  : '' ) ;
		$('#inner_'+element_id).attr('src' ,  getMockupSrc(jElement) ) ;
		view.outdatedJdesign = true ;

	}) ;
	pushState() ;
	updateToolbar();
}

function getElem(element_id) {
	var element_type = element_id.split('_')[0] ;
	var element_name = element_id.split(element_type + '_')[1] ;
	if (element_type == 'txt') {
		var element_index = getElementIndex(view.jdesign.canvases[view.currentPage].texts , "name" , element_name) ; 
		if (element_index != null)		
			return view.jdesign.canvases[view.currentPage].texts[element_index] ; 
	}
	else if(element_type == 'img') {
		element_index = getElementIndex(view.jdesign.canvases[view.currentPage].images, "name" , element_name) ; 
		if (element_index != null)		
			return view.jdesign.canvases[view.currentPage].images[element_index] ; 
	}
	return false ;
}

function pushState() {

	var designClone = JSON.clone(view.jdesign) ;
	// Returns if the requested state is equal to the last satate in queue
	if (view.stateIndex > 0 && JSON.stringify(view.states[view.stateIndex].design) == JSON.stringify(designClone) ) return ;
	
	// In case that an UNDO happened before this action, the rest of the array will be removed
	if (view.stateIndex != view.states.length - 1 )
		view.states.splice(view.stateIndex + 1 , view.states.length - view.stateIndex - 1 ) ;
	
	view.states.push({ "page" : view.currentPage , "design" : designClone}) ;
	view.stateIndex = view.states.length - 1 ;
	view.isDesignSaved = false ; 
	view.userWarned = false ; 
//	updateToolbar();
}

function undo() {
	if (view.stateIndex == 0 ) return ;

	view.stateIndex-- ;
	var stateClone = JSON.clone(view.states[view.stateIndex].design) ;
	view.jdesign = stateClone ;
	view.currentPage = view.states[view.stateIndex].page ;
	refreshView() ; 
}

function redo() {
	if (view.stateIndex == view.states.length - 1) return ;

	view.stateIndex++ ;
	var stateClone = JSON.clone(view.states[view.stateIndex].design) ;
	view.jdesign = stateClone ;
	view.currentPage = view.states[view.stateIndex].page
	refreshView() ; 
}


/* -------- Switch the whole view to the required canvas page ---------- */
function switchView(design , page) {
	view.currentPage = page ; 
	view.jdesign = JSON.clone(design) ; 
	refreshView() ; 
}

function setRulerVisibility(){
	view.profileEQ.show_ruler *= 1 ;
	if (view.profileEQ.show_ruler) {
		$('.ruler').show();
		$('#show_ruler_control').addClass('on') ;
	}
	else {
		$('.ruler').hide();
		$('#show_ruler_control').removeClass('on') ;
	}
}

function setGridVisibility(anim_duration){
	if (view.showGrid) {
		$('.grid-area-mask').fadeIn(anim_duration); 
	}
	else {
		$('.grid-area-mask').fadeOut(anim_duration) ; 
	}
	updateToolbar();
}

function setBleedAreaVisibility(anim_duration){
	if (view.showBleed) {
		$('.bleed-area-mask').fadeIn(anim_duration);
		$('.no-bleed-area-mask').fadeOut(anim_duration);
		$('.safe-zone-mask').fadeIn(anim_duration);
	}
	else{
		$('.bleed-area-mask').fadeOut(anim_duration);
		$('.no-bleed-area-mask').fadeIn(anim_duration);
		$('.safe-zone-mask').fadeOut(anim_duration);
	}
	updateToolbar();
}

function setSafeZoneVisibility(showSafeZone , anim_duration){
	view.showSafeZone = showSafeZone ;
	if (view.showSafeZone) {
		$('.safe-zone-mask').fadeIn(anim_duration); 
	}
	else{
		$('.safe-zone-mask').fadeOut(anim_duration); 	}

	// ---- IE  Compatibility workaround -------------------
	$('.opacity50').css( 'filter' , 'alpha(opacity=50)' )  ;
}



function getBlankJtext() {
	var bg_intensity = (parseInt(view.jdesign.canvases[view.currentPage].bg_color.substr(0 , 2) , 16) + parseInt(view.jdesign.canvases[view.currentPage].bg_color.substr(2 , 2) , 16) + parseInt(view.jdesign.canvases[view.currentPage].bg_color.substr(4 , 2) , 16)) / 3 ; 
	view.textInsertCount %= 20 ;
	var newText = {	"body": "" ,
				"name": "user_text_" + Math.floor(Math.random() * 100000) ,
				"title": "Enter Text",
				"left": Math.min(view.jdesign.canvases[view.currentPage].width / 5 ,  view.jdesign.canvases[view.currentPage].width - view.jdesign.canvases[0].bleed * 4 ) ,
				"top":  Math.min(view.jdesign.canvases[view.currentPage].height / 4 + (view.defaultFontSize / 50).toFixed(1) * (view.textInsertCount  % 5),  view.jdesign.canvases[view.currentPage].width - view.jdesign.canvases[0].bleed * 4 ) ,
				"width": Math.max(1.6, view.jdesign.canvases[view.currentPage].width / 2) ,
				"height": Math.max(1.6, view.jdesign.canvases[view.currentPage].width / 2) / 8  ,
				"gravity" : "west" ,
				"preset_text": "New Text...",
				"pointsize": view.defaultFontSize ,
				"font": "Arial",
				"color": bg_intensity < 128 ? 'ffffff' : '000000'
			}
	var selectedText = $('.selected.text') ;
	if(selectedText.length == 1){
		var jtext = getElem(selectedText.attr('id').split('outline_').pop());
		newText.color = jtext.color ;
		newText.gravity = jtext.gravity ;
		newText.pointsize = jtext.pointsize ;
		newText.font = jtext.font ;
		newText.bold = jtext.bold;
		newText.italic = jtext.italic ; 
		newText.rotaion = jtext.rotation ;
	}
	view.jdesign.canvases[view.currentPage].texts.push(newText);
	view.textInsertCount++ ;
	return newText ;
}

function insertText(){
	addTextElement(getBlankJtext(), true);  
	pushState();
}

function addTextElement(newJtext, select_after_add) {
		
	if(newJtext.top > (view.jdesign.canvases[view.currentPage].height - 0.25)){	//if text position is outside the canvas, text will move to the top.	
		newJtext.left = Math.min(view.jdesign.canvases[view.currentPage].width / 5 ,  view.jdesign.canvases[view.currentPage].width - view.jdesign.canvases[0].bleed * 4 );		
		newJtext.top = Math.min(view.jdesign.canvases[view.currentPage].height / 4 + (newJtext.pointsize / 50).toFixed(1) * (view.textInsertCount  % 5),  view.jdesign.canvases[view.currentPage].width - view.jdesign.canvases[0].bleed * 4 );
		//newJtext.top = Math.min(view.jdesign.canvases[view.currentPage].height / 4 + 0.2 * (view.textInsertCount  % 5),  view.jdesign.canvases[view.currentPage].width - view.jdesign.canvases[0].bleed * 4 );
	}
		
	var outline = $('<div />').attr('id' , 'outline_txt_' + newJtext.name)
					.addClass('outline2 text') 
//					.addClass(newJtext.body.length == 0 ? 'preset-text' : '')
					.cssTop(newJtext.top * view.dpi)
					.cssLeft(newJtext.left * view.dpi)
					.cssHeight(0)
					.cssWidth(newJtext.width * view.dpi)
					.css('z-index' , 500)
					.appendTo('#outline_layers')
					.addClass(user.admin_mode ? 'admin' : 'user') ;


	$('<div />').addClass('selected-dash l').appendTo(outline);
	$('<div />').addClass('selected-dash t').appendTo(outline);
	$('<div />').addClass('selected-dash b').appendTo(outline);
	$('<div />').addClass('selected-dash r').appendTo(outline);
	
	$('<div />').addClass('outline-hint').html('Click to Edit Text').appendTo(outline);
					
	$('<img />').attr('id' , 'txt_' + newJtext.name)
					.css('left', newJtext.left * view.dpi + 'px')
					.css('top', newJtext.top * view.dpi + 'px' )
					.appendTo('#text_layers') ;
					
	var smw = $('<div />').addClass('size-measure w').appendTo(outline);
	$('<div />').addClass('value').appendTo(smw);
	$('<div />').addClass('guide').appendTo(smw);
	var smh = $('<div />').addClass('size-measure h').appendTo(outline);
	$('<div />').addClass('value').appendTo(smh);
	$('<div />').addClass('guide').appendTo(smh);

//	$('<div />').addClass('size-measure').appendTo(outline); //.html( 'W: ' + (newJtext.width).toFixed(2) + '&quot; x H:' + (newJtext.height).toFixed(2) + '&quot;' );;
//	$('<div />').addClass('position-measure').appendTo(outline).html('X: ' + (newJtext.left - view.jdesign.canvases[view.currentPage].bleed).toFixed(2) + '&quot; , Y: ' + (newJtext.top - view.jdesign.canvases[view.currentPage].bleed).toFixed(2) + '&quot;' );
	
	// --- add nw, ne, sw, se handles for all elements excepts those that are not resizable 
	$('<div />').addClass('handle handle-nw').appendTo(outline) ; 	
	$('<div />').addClass('handle handle-ne').appendTo(outline) ; 	
	$('<div />').addClass('handle handle-sw').appendTo(outline) ; 	
	$('<div />').addClass('handle handle-se').appendTo(outline) ; 	

	
	// Disable Right Click
	outline.bind("contextmenu", function(e) {
		// $('#inline_menu_txt_' + newJtext.name).trigger('mousedown');
		// return false;
	}); 
	
	updateTextElement('txt_' + newJtext.name) ; 
	createTextbox(newJtext) ;
	
	// simulates clicking on a textbox
	if(select_after_add){
		outline.mousedown() ;
		outline.mouseup() ;
	}

}

function stopPropag(e) {
	e.cancelBubble = true; 
	if(e.stopPropagation) e.stopPropagation()
}

function updateFotoliaInfo(licenses){
	$.each(licenses, function(k, v) {
		view.fotoliaLicenses[k] = JSON.clone(v);
	})
}

function updateImageInfo(imageInfo){
	$.each(imageInfo, function(k, v){
		updateSingleImageInfo(v);
	})
}

function updateSingleImageInfo(data){
	view.imageInfo[data.id] = {};
	view.imageInfo[data.id].uploadWidth = data.width * 1 ;
	view.imageInfo[data.id].uploadHeight = data.height * 1  ;
	view.imageInfo[data.id].is_vector = data.is_vector * 1  ;
	view.imageInfo[data.id].is_public = data.is_public * 1  ;
	
	if(typeof data.fotolia_id != 'undefined' && data.fotolia_id != null ){
		view.imageInfo[data.id].fotoliaId = data.fotolia_id * 1 ;
		view.fotoliaLicenses[data.fotolia_id] = JSON.clone(data.licenses) ;
	}
	if(typeof data.svg_id != 'undefined' && data.svg_id != null ){
		view.imageInfo[data.id].svg_id = data.svg_id * 1  ;
		view.imageInfo[data.id].svg_original_colors = data.svg_original_colors ;
	}


}

function startInlineCrop(img_name){
	$('.selected').removeClass('selected');
	$('.croppable').not('.fitted').removeClass('croppable');
	$('#outline_img_'+img_name).addClass('croppable selected');
	updateToolbar();
}
function exitInlineCrop(){
	$('.croppable').not('.fitted').removeClass('croppable');	
	$('.croppable').removeClass('selected');
	updateStockImagePrices() ;
	
}

function getElementIndex(arr , attr , val) {
	for (i in arr) {
		if (arr[i][attr] == val)
			return i * 1 ;
	}
	return false ;
}


/*function positionToolbars() {
		var toolbarTopScroll = findPos($('#top_toolbar_placeholder')[0])[1]  -  getPageScrollTop() ;
		if (toolbarTopScroll < 0){
			$('#top_toolbar , #text_fields').addClass('sticky');
		}
		else {
			$('#top_toolbar , #text_fields').removeClass('sticky');
		}

		var o = $('.selected');
		if(o.length == 1){
			var pos = findPos(o[0]);
			var cvpos = findPos($('#canvas')[0]);

			$('.float-toolbar').each(function(){
				$(this).cssLeft(pos[0] - ($(this).outerWidth(true) - o.cssWidth()) / 2 );
				if(pos[1] < cvpos[1] + $(this).outerHeight(true) ||  pos[1] < getPageScrollTop() + 85){
					$(this).cssTop ( Math.min( pos[1] + o.cssHeight() + 5 , $(window).height() + getPageScrollTop() -  $(this).outerHeight(true) - 5 )) ;
				}
				else{
					$(this).cssTop(pos[1] - $(this).outerHeight(true) - 5 );
				}
			});
			
			if(pos[0] > $('#textbox_tools').outerWidth(true) + 5 )
				$('#textbox_tools').cssLeft(Math.max(5 , pos[0] - $('#textbox_tools').outerWidth(true) - 5));
			else
				$('#textbox_tools').cssLeft(Math.min( pos[0]+ o.cssWidth() + 10 ,$(window).width() - $('#textbox_tools').outerWidth(true) - 5 ));

//			if(pos[1] - getPageScrollTop() > $(window).height() / 2 )
//				$('#textbox_tools').cssTop(pos[1] - $('#textbox_tools').outerHeight(true) + o.cssHeight() + 10 )  ;
//			else
			$('#textbox_tools').cssTop(pos[1] - 10 ) ;
				
			if(! $('#textbox_tools').hasClass('open'))
				$('#textbox_tools').cssTop(pos[1]) ;


			$('#textbox_tools').cssTop(Math.max($('#textbox_tools').cssTop() , getPageScrollTop() + 125 ) ) ;
			$('#textbox_tools').cssTop(Math.min($('#textbox_tools').cssTop() , $(window).height() +  getPageScrollTop() - $('#textbox_tools').outerHeight(true) - 10 ) ) ;

		}
}*/

function positionToolbars() {
    var toolbarTopScroll = findPos($('#top_toolbar_placeholder')[0])[1]  -  getPageScrollTop() ;
    if (toolbarTopScroll < 0){
        $('#top_toolbar , #text_fields').addClass('sticky');
        if( $('#tour_ph').is(':visible')){
            closePopups();
        }
    }
    else {
        $('#top_toolbar , #text_fields').removeClass('sticky');
    }

    var o = $('.selected');
    if(o.length == 1){
        var pos = findPos(o[0]);
        var cvpos = findPos($('#canvas')[0]);

        $('.float-toolbar').each(function(){
            $(this).cssLeft(pos[0] - ($(this).outerWidth(true) - o.cssWidth()) / 2 );
            if(pos[1] < cvpos[1] + $(this).outerHeight(true) ||  pos[1] < getPageScrollTop() + 85){
                $(this).cssTop ( Math.min( pos[1] + o.cssHeight() + 5 , $(window).height() + getPageScrollTop() -  $(this).outerHeight(true) - 5 )) ;
            }
            else{
                $(this).cssTop(pos[1] - $(this).outerHeight(true) - 5 );
            }
        });
        /* Hai disable this code */
        /*
        if(pos[0] > $('#textbox_tools').outerWidth(true) + 5 )
            $('#textbox_tools').cssLeft(Math.max(5 , pos[0] - $('#textbox_tools').outerWidth(true) - 5));
        else
            $('#textbox_tools').cssLeft(Math.min( pos[0]+ o.cssWidth() + 10 ,$(window).width() - $('#textbox_tools').outerWidth(true) - 5 ));

        $('#textbox_tools').cssTop(pos[1] - 10 ) ;

        if(! $('#textbox_tools').hasClass('open'))
            $('#textbox_tools').cssTop(pos[1]) ;


        $('#textbox_tools').cssTop(Math.max($('#textbox_tools').cssTop() , getPageScrollTop() + 125 ) ) ;
        $('#textbox_tools').cssTop(Math.min($('#textbox_tools').cssTop() , $(window).height() +  getPageScrollTop() - $('#textbox_tools').outerHeight(true) - 10 ) ) ;
        */
        /* End disable */
        /* Hai open following code */
        var t_id = o.attr('id');
        if(typeof t_id!='undefined' && o.filter('.text').length==1){
            var fix_id = t_id;
            //$('div#'+fix_id).show();
            t_id = t_id.split('outline_')[1];
            var el = getElem(t_id);
            var p_size = Math.round(el.pointsize * view.dpi/76);
            var t_editor = $('#textbox_tools').find('textarea');
            $('#textbox_tools').cssLeft(pos[0]);
            $('#textbox_tools').cssTop(pos[1]);
            $('#textbox_tools').cssWidth(o.cssWidth());
            $('#textbox_tools').cssHeight(o.cssHeight());
            t_editor.cssWidth(o.cssWidth());
            t_editor.cssHeight(o.cssHeight());
            t_editor.css('color', '#'+el.color);
            t_editor.css('font-size', p_size+'px');
            t_editor.attr('data-outline', fix_id);
            if(typeof el.font!='undefined'){
                var ft = el.font;
                if(typeof config.fonts[ft].embed!='undefined' && config.fonts[ft].embed){
                    if((typeof el.italic!='undefined' && el.italic) && (typeof el.bold!='undefined' && el.bold))
                        ft = el.font+' Both';
                    else if(typeof el.italic!='undefined' && el.italic)
                        ft = el.font+' Italic';
                    else if(typeof el.italic!='undefined' && el.italic)
                        ft = el.font+' Italic';
                    else
                        ft = el.font+' Regular';
                }
                if(el.font.indexOf(' ')>=0)
                    t_editor.css('font-family', '"'+ft+'"');
                else
                    t_editor.css('font-family', ft);
                if(typeof el.italic!='undefined' && el.italic)
                    t_editor.css('font-style', 'italic');
                else
                    t_editor.css('font-style', 'normal');

                if(typeof el.bold!='undefined' && el.bold)
                    t_editor.css('font-weight', 'bold');
                else
                    t_editor.css('font-weight', 'normal');
            }
            if(typeof el.line!='undefined'){
                if(el.line=='underline'){
                    t_editor.css('text-decoration', 'underline');
                }else if(el.line=='strike'){
                    t_editor.css('text-decoration', 'line-through');
                }else{
                    t_editor.css('text-decoration', 'none');
                }
            }else{
                t_editor.css('text-decoration', 'none');
            }
            if(typeof el.gravity!='undefined'){
                if(el.gravity=='center'){
                    t_editor.css('text-align', 'center');
                }else if(el.gravity=='east'){
                    t_editor.css('text-align', 'right');
                }else{
                    t_editor.css('text-align', 'left');
                }
            }else{
                t_editor.css('text-align', 'left');
            }

            if(typeof el.character!='undefined'){
                if(el.character=='lower')
                    t_editor.css('text-transform', 'lowercase');
                else if(el.character=='upper')
                    t_editor.css('text-transform', 'uppercase');
                else if(el.character=='capitalize')
                    t_editor.css('text-transform', 'capitalize');
                else
                    t_editor.css('text-transform', 'none');
            }else{
                t_editor.css('text-transform', 'none');
            }
            if(t_editor.hasClass('tb_flipV'))
                t_editor.removeClass('tb_flipV');
            if(t_editor.hasClass('tb_flipH'))
                t_editor.removeClass('tb_flipH');
            if(t_editor.hasClass('tb_flipB'))
                t_editor.removeClass('tb_flipB');
            if(typeof el.flip!='undefined'){
                if(el.flip=='vertical'){
                    t_editor.addClass('tb_flipV');
                }else if(el.flip=='horizontal'){
                    t_editor.addClass('tb_flipH');
                }else if(el.flip=='both'){
                    t_editor.addClass('tb_flipB');
                }
            }

            /*
            if(typeof el.rotation!='undefined'){
                t_editor.css('transform', 'rotate('+el.rotation+'deg)');
            }else t_editor.css('transform', 'none');
            */
            //$('div#'+fix_id).hide();
            $('#textbox_tools').css('z-index', 200);
            //alert($('img#'+t_id).css('display'));
        }
        /* end Hai open following code */

        var resAlert = o.find('.resolution-notice') ;
        if (resAlert.length > 0) {
            resAlert.cssTop(Math.max (-1 * o.cssTop() , 5) ) ;
//              resAlert.cssRight(Math.max ( outline.cssLeft() + outline.cssWidth() - $('#canvas').cssWidth() , 5) ) ; 
        }

    }
}

function updateTextBody(jtext, value){
	jtext.body =  encodeURIComponent(value) ; 
	clearTimeout(tempTextTimeouts[jtext.name]) ; 
	tempTextTimeouts[jtext.name] = setTimeout(function() { updateTextElement('txt_'+jtext.name) ; pushState()} , 300) ; 

	var panel_textbox = $('#tbox_txt_' + jtext.name) ;
	var inline_textbox = $('#inline_textarea') ;

	inline_textbox.cssHeight(18) ;
	inline_textbox.cssHeight(inline_textbox[0].scrollHeight + 2) ;
	panel_textbox.val(value) ;
	panel_textbox.cssHeight(panel_textbox[0].scrollHeight + 2) ;

	if(value.length > 0 )
		panel_textbox.closest('.tarea-wrapper').removeClass('empty');
	else
		panel_textbox.closest('.tarea-wrapper').addClass('empty');
	
	positionToolbars();
}

function startMultiSelect(e) {
	var targetX =  e.pageX - getCanvasX() ; 
	var targetY =  e.pageY - getCanvasY() ; 
	var multiSelector = $('#multi_selector') ;
	multiSelector.show()
				.cssLeft(Math.min (view.selectorX , targetX )) 
				.cssTop(Math.min(view.selectorY , targetY )) 
				.cssWidth(Math.abs(targetX - view.selectorX)) 
				.cssHeight(Math.abs(targetY - view.selectorY)) ; 
	var selectorLeft = multiSelector.cssLeft() ;
	var selectorTop = multiSelector.cssTop() ;
	var selectorWidth = multiSelector.cssWidth() ;
	var selectorHeight = multiSelector.cssHeight() ;
	var selectorRight = selectorLeft + selectorWidth ;
	var selectorBottom = selectorTop + selectorHeight ;


// This algorithm select any elements which is touched by selector
	$("#outline_layers .outline2").not(".non-selectable").not(".locked").each(function() {
		var shapeTop = $(this).cssTop() ; 
		var shapeBottom = $(this).cssTop() + $(this).cssHeight(); 
		var shapeLeft = $(this).cssLeft() ; 
		var shapeRight = $(this).cssLeft() + $(this).cssWidth(); 
			
		// X Intersection of selector and shape
		var xx = Math.max (Math.max (selectorRight - shapeLeft , 0) - Math.max (selectorLeft - shapeLeft , 0) - Math.max(selectorRight - shapeRight, 0) ,0) ;
		// Y intersection of selector and shape
		var yy = Math.max  (Math.max (selectorBottom - shapeTop , 0) - Math.max (selectorTop - shapeTop , 0) - Math.max(selectorBottom - shapeBottom, 0) ,0) ;
		
		if (xx * yy > 0) // Intersection area
			$(this).addClass("selected") ; 
		else 
			$(this).removeClass("selected") ; 
	}) ; 


}

function deselectAll() {
		$('#image_toolbar').hide();
		$('#text_tools_section').show();
		$('#shape_toolbar').hide();
		var o = $('.outline2.selected') ;
		if(o.length > 0){
			exitInlineCrop();
			$('.outline2.selected').removeClass('selected') ; 
			arrangeOutlineZindex() ;
			updateToolbar() ;
		}
}

function applyUrlHash() {
	//if (! user.admin_mode && (window.location.hash == '#' || window.location.hash == '') && typeof view.jdesign == 'undefined') window.location = "/" ;

	$('.ui-dialog-titlebar-close').click();
	$('.iframe-popup').hide();

	var newTemplateId = getFragmentValue('template_id') ;

	var newDesignId = getFragmentValue('design_id')  ;
	var themeId = getFragmentValue('theme_id') ;
	var page = getFragmentValue('page') || 0 ;
	var option = getFragmentValue('option') || 0 ;
	if (newTemplateId){
		if (newTemplateId != view.templateId)
			loadTemplate(newTemplateId, themeId, page, option) ;
		else if (page != view.currentPage)	
			switchView(view.jdesign, page);
		return ;
	}
	if (newDesignId){
		if(newDesignId != view.designId)
			loadDesign(newDesignId, page) ;		
		else if (page != view.currentPage)	
			switchView(view.jdesign, page);
		return ;
	}

	if (typeof view.jdesign == 'undefined' && getFragmentValue('product') ){
		var product = {} ;
		product.product = getFragmentValue('product') != null ? getFragmentValue('product') : "businessCard" ;
		product.width = getFragmentValue('width') != null ? getFragmentValue('width') * 1 : 3.5 ;
		product.height = getFragmentValue('height') != null ? getFragmentValue('height') * 1 : 2 ;
		product.folding = getFragmentValue('folding') != null ? getFragmentValue('folding') : 'none' ;
		product.dieCutType = getFragmentValue('dieCutType') != null ? getFragmentValue('dieCutType') : 'none' ;
		product.wrap_color = getFragmentValue('wrap_color') != null ? getFragmentValue('wrap_color') : 'none' ;
		startNewProduct(product);
	}	
}

function applyFontSize(){
	var s = parseInt($('#selected_font_size').val()) ;
	if (isNaN(s)){
		updateToolbar();
		return;
	}
	s = Math.max(s, config.fontSizes[0]);
	s= Math.min(s, config.fontSizes[config.fontSizes.length -1]);
	applyTextAttr('pointsize' , s);
	$('#selected_font_size').blur().closest('.toolbar-button').removeClass('open');
}

function populateDropDowns(){
	//alert('populateDropDowns');
	var link1 = '';
	var fontSizeMenu = $('#font_size_menu');
	var fontFaceMenu = $('#font_face_menu');
	$.each(config.fontSizes , function(k, v){
		var menuItem = $('<div />').addClass('toolbar-button type4 font-size-option').appendTo(fontSizeMenu);
		$('<div />').addClass('button-menu-text').html(v+'pt').appendTo(menuItem)
	});
	
	$.each(config.fonts, function(k,v){
        link1 = '/tools.php?action=assets&action_type=font_sample&name=' + k;
		var menuItem = $('<div />').addClass('toolbar-button type4 font-face-option').appendTo(fontFaceMenu).data('font_name' , k);
		$('<img />').addClass('button-menu-image').attr('src', link1).appendTo(menuItem);
	})

							
	for (i in config.color_palette) {
		$('<div />').data('color' , config.color_palette[i])
					.addClass('color-pick').addClass('color-'+ config.color_palette[i])
					.css('background-color' , '#'+ config.color_palette[i] )
					.appendTo('.color-palette') ; 
		
	}

	$.each(config.borderSizes, function(k,v){
		var t = $('<div />').addClass('toolbar-button type4 shape-border-width-option').appendTo('#shape_border_width_menu');
		$('<div />').addClass('button-menu-text').html(v + 'pt').appendTo(t);

		var t = $('<div />').addClass('toolbar-button type4 image-border-width-option').appendTo('#image_border_width_menu');
		$('<div />').addClass('button-menu-text').html(v + 'pt').appendTo(t);
	});
							
}

function applyBgColor(color) {
	view.jdesign.canvases[view.currentPage].bg_color = color ;

	$('#canvas').css('background' , '#' + color.split('#')[0]) ; 
	pushState() ; 
	updateToolbar();
}

JSON.clone = function (obj) {
  return $.parseJSON( JSON.stringify( obj ) );
}

function startNewProduct(jProduct) {
	resetView() ;

	jProduct.foldingDirection = jProduct.width >= jProduct.height ? 'vertical' : 'horizontal' ;
	jProduct.folding = (typeof jProduct.folding !== 'undefined' )? jProduct.folding : "none";
	jProduct.dieCutType = (typeof jProduct.dieCutType !== 'undefined')? jProduct.dieCutType : "none" ;
	
	var bleed = products[jProduct.product].bleed;
	
	
	var newJdesign = {} ;
	newJdesign.version = "2.0" ;
	newJdesign.canvases = [{texts:[] , images:[]}];
	
	if(jProduct.product == 'foldedGreetingCard'){
		if(jProduct.width < jProduct.height){
			newJdesign.canvases[0].width = jProduct.width + bleed * 2 ;
			newJdesign.canvases[0].height = (jProduct.height / 2) + bleed * 2;
		}else{
			newJdesign.canvases[0].width = (jProduct.width / 2) + bleed * 2;
			newJdesign.canvases[0].height = jProduct.height + bleed * 2 ;
		}
	}else{
		newJdesign.canvases[0].width = jProduct.width + bleed * 2 ;
		newJdesign.canvases[0].height = jProduct.height + bleed * 2 ;
		
	}
	
	newJdesign.canvases[0].name = getSideNameByFolding(jProduct.folding, 0, jProduct.product) ;
	newJdesign.canvases[0].bleed = bleed ;
	newJdesign.canvases[0].droppable = true ;

	newJdesign.canvases[0].bg_color = 'ffffff';

	newJdesign.width = jProduct.width ;
	newJdesign.height = jProduct.height ;
	newJdesign.folding = jProduct.folding ;
	newJdesign.foldingDirection = jProduct.foldingDirection ;
	newJdesign.dieCutType = jProduct.dieCutType ;
	newJdesign.wrap_size = products[jProduct.product].wrap_size * 1 ;
	newJdesign.wrap_color = jProduct.wrap_color ;
	newJdesign.product = jProduct.product ;	
	
	jProduct.sides = products[jProduct.product].sides;
	
	// Inserting the second page (backside) exculude all the one sided products 
	if (jProduct.sides == 2){
		newJdesign.canvases.push(JSON.clone(newJdesign.canvases[0])) ;
		newJdesign.canvases[1].name = getSideNameByFolding(jProduct.folding , 1, jProduct.product) ;
	}
	adjustProfileEQ(newJdesign.product);
	switchView(newJdesign, 0) ;
	$('#search_result_wrapper').hide() ;
//	window.location.hash = '#' ;
	pushState() ; 
	view.isDesignSaved = true ; // // override the false value caused by push State
}

function getSideNameByFolding(f , s , p) {

	if (f == 'halfFold' || f == 'triFold' || f == 'letterFold' || f == 'rollFold' ) {
		if(p == "foldedGreetingCard"){
			if (s==0) return 'Front' ;
			else if (s==1) return 'Inside' ;
			
		}else{
			if (s==0) return 'Outside' ;
			else if (s==1) return 'Inside' ;
		}
	}
	else {
		if (s==0) return 'Front Side' ;
		else if (s==1) return 'Back Side' ;
	}
}

function copyElements(){
	if ($('#outline_layers .selected').not('.pholder').length == 0) return ;
	view.clipboard = [];
	view.pasteCount = 0 ; 
	view.copySide = view.currentPage ;
	
	$('#outline_layers .selected').not('.pholder').each(function() {
		view.clipboard.push( JSON.clone(getElem( $(this).attr('id').split('outline_').pop() )) ) ; 
	}) ;
	updateToolbar();
}

function pasteElements() {
	if (view.clipboard.length == 0) return ;
	deselectAll();
	view.pasteCount++ ;
	view.pasteCount %= 10 ;

	// If pasting at a different page the will be no shift for the first paste
	pasteShift = view.pasteCount - (view.copySide == view.currentPage ? 0 : 1 )  ;

	$.each(view.clipboard, function(k,v) {
		jPaste = JSON.clone(v);
		jPaste.name = jPaste.name + '_' + Math.floor(Math.random() * 1000) ;
		jPaste.left += 0.2 * pasteShift ;
		jPaste.top += 0.2 * (pasteShift % 5) ;
		if(typeof v.body != 'undefined'){ // text element
			view.jdesign.canvases[view.currentPage].texts.push(jPaste) ;
			addTextElement(jPaste);
			$('#outline_txt_' + jPaste.name).addClass('selected');
		}

		else {
			delete jPaste.fitted ;
			delete jPaste.unmovable ;
			delete jPaste.noresize ;
			jPaste.zindex = 299 +  jPaste.zindex ;
			view.jdesign.canvases[view.currentPage].images.push(jPaste) ;
			drawImageElement(jPaste) ;
			$('#outline_img_' + jPaste.name).addClass('selected');
		}

	}) ; 
	arrangeImageZees();
	pushState() ;
	updateToolbar();
}

function loadTemplate(id, theme_id, page, option){
    if(! view.isDesignSaved){
        if(! confirm('You have unsaved work. Continue anyway?'))
            return ;
    }
    $('#gray_box').hide();
    //reset_view() ;
    $.ajax({url:"/tools.php?action=load_design" ,
        data: {"template_id" : id  ,"theme_id" : theme_id ?  theme_id : 0, option: option, cross: is_cross_domain } ,
        dataType: is_cross_domain? 'json':'jsonp',
        jsonpCallback: is_cross_domain?null:'jsonp_callback',
        beforeSend: function(){
          //alert('SSS');
        },
        success: function(data){
            if(!is_cross_domain) data = jsonp_data;
            if (data.success != 1) {
                alert('Error loading template: ' + data.error);
                return ;
            }
            //var data = create_data();

            resetView() ;
            view.themes = data.themes ;
            applyThemeToToolbar(data.theme);
            // //var json = $.parseJSON(data.json) ;
            var json = data.json;
            updateImageInfo(data.images);
            updateFotoliaInfo(data.fotoliaLicenses);
            view.templateId = data.id * 1 ;
            view.themeId = theme_id ;
            view.designName = data.set_title ;
            adjustProfileEQ(json.product);
            switchView(json , page) ;
            pushState() ;
            view.isDesignSaved = true ;
            if(data.save_action+''!='undefined')
                view.saveAction = data.save_action;
            /*
            resetView() ;
            view.themes = data.themes ;
            applyThemeToToolbar(data.theme);
            data.json = dat.json;
            data.set_title = dat.name;
            var json = data.json ;
            var images = dat.images;
            update_image(data, images);

            updateImageInfo(data.images);
            updateFotoliaInfo(data.fotoliaLicenses);
            view.templateId = data.id * 1 ;
            view.designName = data.set_title ;

            adjustProfileEQ(json.product);
            switchView(json , page) ;
            pushState() ;
            view.isDesignSaved = true ;
            */
        }
    });
}

/*
BACKUP
 function loadTemplate(id, theme_id, page){

     var data = create_data();
     resetView() ;
     view.themes = data.themes ;
     applyThemeToToolbar(data.theme);
     var json = data.json ;
     updateImageInfo(data.images);
     updateFotoliaInfo(data.fotoliaLicenses);
     view.templateId = data.id * 1 ;
     view.designName = data.set_title ;
     adjustProfileEQ(json.product);
     switchView(json , page) ;
     pushState() ;
     view.isDesignSaved = true ;
 }

*/

/*
function loadTemplate(id, theme_id, page) {
	if(! view.isDesignSaved){
		if(! confirm('You have unsaved work. Continue anyway?'))
				return ;
	}
	$('#gray_box').hide();
	//reset_view() ;

	$.ajax({url:"https://www.youprint.com/ajax/template/getTemplateById.php" ,
			data: {"template_id" : id , "with_json" : 1 , "with_image_info" : 1 ,"theme_id" : theme_id ?  theme_id : undefined } , 
			dataType: 'json' ,
			crossDomain: true,
			beforeSend: function(){
				alert('E');
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert(jqXHR.innerHTML);
				alert(textStatus);
				alert(errorThrown);
			},
			success: function(data) {
				if (data.success != 1) {
					alert('Error loading template: ' + data.error);
					return ;
				}
				resetView() ;
				view.themes = data.themes ; 
				applyThemeToToolbar(data.theme);
				var json = $.parseJSON(data.json) ;
				updateImageInfo(data.images);
				updateFotoliaInfo(data.fotoliaLicenses);
				view.templateId = data.id * 1 ; 
				view.designName = data.set_title ;
				adjustProfileEQ(json.product);
				switchView(json , page) ; 
				pushState() ;
				view.isDesignSaved = true ; 
				
			}
	}) ;
}
*/

function applyLayout(id, callbackFunc){
	applyLayoutCallback = typeof callbackFunc == 'function' ? callbackFunc : null ;
	updateUserTexts() ;
	updateUserImages() ; 

	$.ajax({url:"cms/getTemplateJson.php" ,
			data: {"id" : id } , 
			dataType: 'json' ,
			success: function(data) {
				if (data.success == 1) {
					view.jdesign.template_id = view.templateId = data.id ; 
					/* EASY-2115
					if (view.designId == 0)
						window.location.hash = "#template_id=" + data.id ;
					*/
					
//					for (c in data.json.canvases)
						c = view.currentPage ; 
						for(t in data.json.canvases[c].texts)
							if (typeof view.userTexts[data.json.canvases[c].texts[t].name] !== 'undefined')
									data.json.canvases[c].texts[t].body = view.userTexts[data.json.canvases[c].texts[t].name] ;
					// apply the same page of layout to the current page of design
					view.jdesign.canvases[c] = data.json.canvases[c] ; 
					refreshView(); 
					updatePlaceholders() ;

//					for (c in view.jdesign.canvases)
						c = view.currentPage ;
						for(i2 in view.jdesign.canvases[c].images){
							if (typeof view.jdesign.canvases[c].images[i2] != 'undefined' && typeof view.userImages['user_' + view.jdesign.canvases[c].images[i2].name] !== 'undefined') {
								putImageInPlaceholder(view.jdesign.canvases[c].images[i2].name , view.userImages['user_' + view.jdesign.canvases[c].images[i2].name]  ) ;
							}
						}

					pushState() ;
					
					if (typeof applyLayoutCallback == 'function'){
						applyLayoutCallback(); 
					}
					
					
				}
				else {
					alert('Error: ' + data.error) ; 
				}
			}
	}) ;
}
function showNotice(text) {
	$('#notice_box').html(text).addClass('notice').removeClass('alert') ;
	$('#notice_bar').show() ;
	window.setTimeout("$('#notice_bar').fadeOut(300)" , 3000) ; 
} 

function showAlert(text) {
	$('#notice_box').html(text).addClass('alert').removeClass('notice');
	$('#notice_bar').show() ;
	window.setTimeout("$('#notice_bar').fadeOut(300)" , 8000) ; 
} 

function hideAlert(){
	$('#notice_bar').fadeOut(300);
}

function loadDesign(id, page){
	if(! view.isDesignSaved){
		if(! confirm('You have unsaved work. Continue anyway?'))
				return ;
	}
//	if (! warnUnsaved(function() {loadDesign(id)} )) return ;
	 
	$.ajax({ 	url : "/tools.php?action=load_design" ,
				data: {"design_id" : id, cross: is_cross_domain} ,
				dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
				success: function(data) {
                    if(!is_cross_domain) data = jsonp_data;
					if (data.success != 1) {
						showAlert('Error: ' + data.error);
						return;
					}
                    resetView() ;
                    view.themes = data.themes ;
                    applyThemeToToolbar(data.theme);
                    //var json = $.parseJSON(data.json) ;
                    var json = data.json;
                    updateImageInfo(data.images);
                    updateFotoliaInfo(data.fotoliaLicenses);
                    view.templateId = data.id * 1 ;
                    view.themeId = data.theme_id ;

                    view.designId = id ;
                    adjustProfileEQ(json.product);
                    switchView(json , page) ;
                    pushState() ;
                    view.isDesignSaved = true ;
                    if(data.save_action) view.saveAction = data.save_action;
				}
	}) ;
}

function updateUserTexts() {
	if (! view.jdesign) return ;
	for (i in view.jdesign.canvases)
		for (j in view.jdesign.canvases[i].texts)
			if (view.jdesign.canvases[i].texts[j].body.length > 0)
				view.userTexts[view.jdesign.canvases[i].texts[j].name] = view.jdesign.canvases[i].texts[j].body ;
}

function updateUserImages() {
	if (typeof view.jdesign == 'undefined') return ;
	for (c in view.jdesign.canvases)
		for (i in view.jdesign.canvases[c].images)
			if (typeof view.jdesign.canvases[c].images[i].image_id !== 'undefined')
				view.userImages[view.jdesign.canvases[c].images[i].name] = 1 * view.jdesign.canvases[c].images[i].image_id ;
}

function saveDesign(callbackFunc, skipLogin) {
    if(view.saveAction==""){
        if(! view.designId) {
            saveDesignAs(callbackFunc, skipLogin) ;
            return false;
        }
    }
	showNotice("Working...") ; 

	updateStockImagePrices();

	$.ajax({
        //url : "save_design.php" ,
        url : "/tools.php?action=save_design",
				data: {"json" : JSON.stringify(view.jdesign) , "design_id" :view.designId , "template_id" : view.templateId, "theme_id" : view.themeId, "dpi": view.dpi, "save_action":view.saveAction, cross: is_cross_domain } ,
				dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
				type: 'POST' ,
				success: function(data) {
                    if(!is_cross_domain) data = jsonp_data;
					if (data.success == 1) {
						view.isDesignSaved = true ;
						if (typeof view.callbacks.save == 'function')
							view.callbacks.save() ; 
							showNotice("Successfully saved.") ; 
							updateToolbar();
					}
					else {
						showAlert("An error occurred while saving your design. Please try again.") ; 
					}
				} ,
				error: function() {
					showAlert("An error occurred while saving your design. Please try again.") ; 
				}
	})  ;

}

function saveDesign_Backup(callbackFunc, skipLogin) {
    //alert('saveDesign');
    /*
     view.callbacks.save = callbackFunc ;

     if 	(! user.customer_id && ! skipLogin || ! user.customer_id && ! user.visitor_id) {
     popupLoginHandler( function() {saveDesign(view.callbacks.save)} ) ;
     return false;
     }
     */
    // In case design_id is null (i.e. new design) uses save as.
    //alert(view.designId);

    if(! view.designId) {
        saveDesignAs(callbackFunc, skipLogin) ;
        return false;
    }

    showNotice("Working...") ;

    updateStockImagePrices();

    $.ajax({
        //url : "save_design.php" ,
        url : "/tools/index.php?action=save_design",
        data: {"json" : JSON.stringify(view.jdesign) , "design_id" :view.designId , "template_id" : view.templateId, "theme_id" : view.theme_id, "dpi": view.dpi, "save_action":view.saveAction } ,
        dataType: 'json' ,
        type: 'POST' ,
        success: function(data) {
            //alert(data);
            if (data.success == 1) {
                view.isDesignSaved = true ;
                if (typeof view.callbacks.save == 'function')
                    view.callbacks.save() ;
                showNotice("Successfully saved.") ;
                updateToolbar();
            }
            else {
                showAlert("An error occurred while saving your design. Please try again.") ;
            }
        } ,
        error: function() {
            showAlert("An error occurred while saving your design. Please try again.") ;
        }
    })  ;

}

function saveDesignAs(callbackFunc, skipLogin) {
	//alert('saveDesignAs');
	view.callbacks.save = callbackFunc ;

	//if 	(! user.customer_id && ! skipLogin || ! user.customer_id && ! user.visitor_id) {
	//	popupLoginHandler( function() {saveDesignAs(view.callbacks.save)} ) ;
	//	return false;
	//}
		
	if (! view.saveAsName) {
		var p = createPopup('Save Design' , 400).attr('id' , 'save_as_popup' ).show() ;
		var b = p.find('.simple-popup-content') ;
		$('<h5 />').css('margin' , '20px 0px 10px 0px').html('Please enter a name for your design:').appendTo(b) ; 
		$('#gray_box').show() ;
		$('<input type="text" id="save_as_name" maxlength="50" />')
						.val(typeof view.designName == 'undefined' ? "Untitled Design" : view.designName)
						.appendTo(b).focus().select()
						.keydown(function(e){
							if(e.keyCode == 13){
								$('#save_popup_button').click();
							}
						});

		var buttonWrapper =  $('<div />').css('text-align' , 'right').css('margin' , '12px 0').appendTo(b) ; 
		$('<input />').attr('type' , 'button').val('Save')
					.attr('id', 'save_popup_button')
					.addClass('action-button')
					.appendTo(buttonWrapper)
					.click(function() {  
							if($('#save_as_name').val().length == 0) 
								return ; 
							view.saveAsName = $('#save_as_name').val() ; 
							$('#gray_box').hide(); 
							$('#save_as_popup').remove() ; 
							saveDesignAs(callbackFunc , skipLogin) ;
							//_gaq.push(['_trackPageview', '/easy-design-saved.html']);
							}) ;
		return ;
	}

	showNotice("Working...") ; 
//alert('1');
	view.designName = view.saveAsName ; 
	$.ajax({
            //url : "save_as_design.php" ,
        url : "/tools/index.php?action=save_as_design" ,
				data: {"json" : JSON.stringify(view.jdesign) , "name" : view.saveAsName , "template_id" : view.templateId, "theme_id" : view.theme_id, "dpi": view.dpi, cross:is_cross_domain } ,
				dataType: is_cross_domain? 'json':'jsonp' ,
                jsonpCallback: is_cross_domain?null:'jsonp_callback',
				type: 'POST' ,
                beforeSend: function(){
                    //alert('2');
                },
				success: function(data) {
                    if(!is_cross_domain) data = jsonp_data;
					if (data.success == 1) {
						//window.location.hash = '#design_id=' + data.design_id ;
                        //var url = window.location+'';
                        //window.location = url + '&design_id=' + data.design_id ;
						view.designId = data.design_id ;
						view.themeId = data.theme_id ;

						view.isDesignSaved = true ;
						if (typeof view.callbacks.save == 'function')
							view.callbacks.save() ;
							showNotice("Your design has been saved.") ; 
							updateToolbar();
                            //var $p = $('<p style="margin: 3px; padding-left: 10px"></p>');
                            //var $link = $('<a class="your-work" href="tool.php?design_id='+data.design_id+'">'+view.designName+'</a>');
                            //$p.append($link);
                            //var $div = $('div#yp_feedback_box div div:last');
                            //$div.append($p);
					}
					else {
						alert("An error occurred while saving your design. Please try again.") ; 
					}
				} ,
				error: function() {
					showAlert("An error occurred while saving your design. Please try again.") ; 
				}

	})  ;
	view.saveAsName = null ; 
}

/*
function warnUnsaved(callbackFunc) {
	if (view.isDesignSaved || view.userWarned) {
		view.userWarned = false ;
		return true ;
	}

	view.userWarned = false ;
//	view.callbacks.warn = callbackFunc ;
	var p = createPopup('Warning' , 420).attr('id' , 'save_warning' ).show() ;
	var b = p.find('.simple-popup-content')
	$('<h4 />').css('margin' , '15px 0px').html('The current design is not saved. Do you want to save it?').appendTo(b) ; 
	$('#gray_box').show() ;
	var buttonWrapper =  $('<div />').css('text-align' , 'center').appendTo(b) ; 
	$('<input />').attr('type' , 'button').val('Save').addClass('action-button medium green').appendTo(buttonWrapper).click(function() {$('#gray_box').hide(); $('#save_warning').remove() ; saveDesign(callbackFunc)}) 
	$('<input />').attr('type' , 'button').val("Don't save").addClass('action-button medium green').appendTo(buttonWrapper).click(function() {$('#gray_box').hide();$('#save_warning').remove() ; view.userWarned = true ; callbackFunc() }) 
	$('<input />').attr('type' , 'button').val("Cancel").addClass('action-button medium green').appendTo(buttonWrapper).click(function() {$('#gray_box').hide(); $('#save_warning').remove() }) 
	
//	return  confirm("Continue without saving?") ;
}
*/

function updateInlineTextEdit(){
	var el = $('.selected.text') ;
	if(el.length != 1) { return } ;
	var element_id = el.attr('id').split('outline_').pop();
	var jtext = getElem(element_id) ;
	updateTextBody(jtext, $(this).val());
}

function updateInlinePresetEdit(){
    var el = $('.selected.text') ;
    if(el.length != 1) { return } ;
    var element_id = el.attr('id').split('outline_').pop();
    var jtext = getElem(element_id) ;
    jtext.preset_text = encodeURIComponent($(this).val());
}

function updateInlineTitleEdit(){
    var el = $('.selected.text') ;
    if(el.length != 1) { return } ;
    var element_id = el.attr('id').split('outline_').pop();
    var jtext = getElem(element_id) ;
    jtext.title = encodeURIComponent($(this).val());
}

// Show the login popup
function popupLoginHandler( callbackFunc ) {
	//alert('popupLoginHandler');
	showNotice('Please sign in to continue...') ;
	$('#gray_box').show() ;		
	view.callbacks.login = callbackFunc ;
	var loginPopup = createPopup('Sign in or Register', 700).attr('id' , 'login_popup_new').addClass('transparent-popup').fadeIn(300) ;	
	loginPopup.find('.simple-popup-content').remove();
	$('#add_image_popup').remove();
	
	$('<iframe />').attr('id', 'api_login_iframe')
						.attr('src', config.popupLoginUrl + '?url=' + encodeURIComponent('http://' + document.location.hostname + '/ajax/user/easyLoginRedirect.php') ) 
						.attr('frameborder',0)
						.attr('allowtransparency' , true)
						.attr('scrolling' ,'no')
						.height(500)
						.width(700)
						.appendTo(loginPopup);
}

function easyPostLoginHandler(userInfo) {

	if(typeof userInfo.admin_mode != 'undefined' && userInfo.admin_mode * 1 == 1 )
		window.location.reload();
	
	user = userInfo ;
	showNotice('Successfully logged in.') ; 
	$('#gray_box').hide() ;
	$('#login_popup_new').remove();
	$('#add_image_popup').remove();
	
	if (typeof view.callbacks.login == 'function')
		view.callbacks.login(userInfo) ;

	if (typeof headerPostLoginHandler == 'function')
		headerPostLoginHandler(userInfo) ;

	updateToolbar();
}

function isActiveCanvas(){
	return ! ($('#gray_box').is(':visible') || $("input[type='text'] , textarea").not('#temp_text').is(':focus').length > 0 )  ;
}

$(document).on('click', '.menu-opener' , function(e){
	e.stopPropagation();
});

$(document).on('click' , '.toolbar-button' , function(e){
	$(this).parents('.has-menu').removeClass('open');
	e.stopPropagation();

	if( $(this).hasClass('disabled')) {
		return;
	}
	switch ($(this).attr('id')){
		case 'btn_flip_vertical':
            flip('vertical');
            break;
        case 'btn_flip_horizontal':
            flip('horizontal');
            break;
		case 'btn_insert_text':
			insertText() ;
			break ;

		case 'btn_text_bold':
			applyTextAttr('bold' , ! $(this).hasClass('on') ); 			
			break ;

		case 'btn_text_italic':
			applyTextAttr('italic' , ! $(this).hasClass('on') );
			break ;

		case 'btn_text_gravity_west':
			applyTextAttr('gravity' ,'west');
			break ;

		case 'btn_text_gravity_center':
			applyTextAttr('gravity' ,'center');
			break ;

		case 'btn_text_gravity_east':
			applyTextAttr('gravity' ,'east');
			break ;

		case 'btn_text_auto_fit':
			applyTextAttr('fit_to_box' , ! $(this).hasClass('on') );
			break ;

		case 'btn_edit_undo':

			undo() ;
			break ;
			
		case 'btn_edit_redo':
			redo() ;
			break ;

		case 'btn_edit_lock':
			lockElement() ;
			break ;

		case 'btn_edit_copy':
			copyElements() ;
			break ;

		case 'btn_edit_paste':
			pasteElements() ;
			break ;

		case 'btn_shape_delete':
		case 'btn_image_delete':
		case 'btn_edit_delete':
			deleteSelectedElements() ;
			break ;

        /*Add some*/
        case 'btn_image_mask':
            applyImageAttribute('type');
            break;
        case 'btn_image_noprint':
            applyImageAttribute('noprint');
            break;
        /*
        case 'btn_image_holder':
            applyImageAttribute('placeholder');
            break;
        case 'btn_image_upload':
            applyImageAttribute('uploader');
            break;
        */
        case 'btn_image_holder_none':
        case 'btn_image_holder_fitter':
        case 'btn_image_holder_filler':
            applyImageAttribute('placeholder', $(this).attr('id'));
            break;
        case 'btn_image_noselect':
            applyImageAttribute('noselect');
            break;
        case 'btn_image_noresize':
            applyImageAttribute('noresize');
            break;
        case 'btn_image_unmovable':
            applyImageAttribute('unmovable');
            break;
        /* End add*/
		case 'btn_align_left':
			alignEdges('L') ;
			break ;

		case 'btn_align_center':
			alignEdges('C') ;
			break ;

		case 'btn_align_right':
			alignEdges('R') ;
			break ;

		case 'btn_align_top':
			alignEdges('T') ;
			break ;

		case 'btn_align_middle':
			alignEdges('M') ;
			break ;

		case 'btn_align_bottom':
			alignEdges('B') ;
			break ;

		case 'btn_dist_ver':
			distributeElements('V') ;
			break ;

		case 'btn_dist_hor':
			distributeElements('H') ;
			break ;

		case 'btn_arrange_bring_front':
			bringToFront() ;
			break ;

		case 'btn_arrange_send_back':
			sendToBack() ;
			break ;

		case 'btn_arrange_bring_forward':
			bringForward() ;
			break ;

		case 'btn_arrange_send_backward':
			sendBackward() ;
			break ;

		case 'btn_advance_rotate':
			rotateElements() ;
			break ;

		case 'btn_advance_crop':
		case 'btn_image_crop':
			startInlineCrop($('.selected').attr('id').split('outline_img_')[1] ) ;
			break ;

		case 'btn_view_bleed':
			view.showBleed = ! view.showBleed ;
			setBleedAreaVisibility() ;
			break ;

		case 'btn_view_grid':
			view.showGrid = ! view.showGrid ;
			setGridVisibility() ;
			break ;

		case 'btn_view_snap':
			view.snapEnabled = ! view.snapEnabled ; 
			updateToolbar();
			break ;

		case 'btn_view_zoom_out':
			zoomOut() ;
			break ;

		case 'btn_view_zoom_in':
			zoomIn() ;
			break ;

		case 'btn_view_quick_edit':
			view.profileEQ.show_text_panel = ! view.profileEQ.show_text_panel ;
			view.zoomLevel = 0 ;
			refreshView();
			break ;
			
		case 'btn_insert_upload':
			showAddImagePopup();
			break;
		
		case 'btn_proceed':
			proceedToNextPage();
			break;
		
		case 'btn_preview':
			showPreview();
			break;

		case 'btn_preview_3d':
			showPreview3D();
			break;
			
		case 'btn_save':
			saveDesign();
			break;

		case 'btn_save_2':
			saveDesign(undefined , false);
			break;

		case 'btn_save_as':
			saveDesignAs(undefined, false);
			break;
			
		case 'btn_share_email' :
			emailToFriend() ;
			break;
			
		case 'btn_share_facebook' :
			shareOnFacebook() ;
			break;


		case 'btn_save_pdf' :
			downloadPdfProof() ;
			break;

		case 'btn_text_edit' :
			toggleTextboxTool();
			break;

		case 'btn_text_done' :
			toggleTextboxTool(false);
			break;

		case 'btn_text_remove' :
			deleteSelectedElements();
			break;

		case 'btn_crop_done' :
			deselectAll();
			break;

		case 'btn_shape_vline' :
			addShapeToDesign('vline');
			break;

		case 'btn_shape_rect' :
			addShapeToDesign('rect');
			break;
		
		case 'btn_shape_circle' :
			addShapeToDesign('circle');
			break;
		
		case 'btn_shape_hline' :
			addShapeToDesign('hline');
			break;
		
		case 'btn_shape_triangle' :
			addShapeToDesign('triangle');
			break;
		
		case 'btn_text_bullet_white_circle' :
			applyTextAttr('bullet_style' ,  'white_circle');
			break;

		case 'btn_text_bullet_black_circle' :
			applyTextAttr('bullet_style' ,  'black_circle');
			break;

		case 'btn_text_bullet_white_square' :
			applyTextAttr('bullet_style' ,  'white_square');
			break;

		case 'btn_text_bullet_black_square' :
			applyTextAttr('bullet_style' ,  'black_square');
			break;

		case 'btn_text_bullet_dash' :
			applyTextAttr('bullet_style' ,  'dash');
			break;

		case 'btn_text_bullet_number_dot' :
			applyTextAttr('bullet_style' ,  'number_dot');
			break;

		case 'btn_text_bullet_number_dash' :
			applyTextAttr('bullet_style' ,  'number_dash');
			break;

		case 'btn_text_bullet_number_parenth' :
			applyTextAttr('bullet_style' ,  'number_parenth');
			break;

		case 'btn_text_bullet_none' :
		case 'btn_text_number_none' :
			applyTextAttr('bullet_style' , undefined);
			break;

		
		case 'btn_top_toolbar_toggle':
			view.showAdvancedToolbar = !view.showAdvancedToolbar ;
			if(! view.showAdvancedToolbar) 
				$('#top_toolbar_placeholder').slideUp(300, positionToolbars); 
			else 
				$('#top_toolbar_placeholder').slideDown(300, function(){$(this).css('overflow' , 'visible'); positionToolbars() ;});
			updateToolbar();
			break;

	}
	

});

$(document).on('dblclick', '.text', function(e){
    var id = $(this).attr('id');
    id = id.split('outline_')[1];
    $('#textbox_tools').show();
    $('div#outline_'+id).find('div.handle').hide();
    positionToolbars();
    $('#inline_textarea').trigger('focus');
});

$(document).on('click' , '.page-navigation-item' , function(e) {
	switchView(view.jdesign, $(this).attr('id').split('_').pop());
});

$(document).on('mouseover' , '.left-textarea' , function(e) {
	highlight.call(  document.getElementById(this.id.replace('tbox_' , 'outline_' )) , e)
}) ; 

$(document).on('mouseout' , '.left-textarea' , function(e) {
	lowlight.call(  document.getElementById(this.id.replace('tbox_' , 'outline_' )) , e)
}) ; 

$(document).on('mousedown' , '.stock-image-price-outline' , showStockImageHelp)

$('#selected_font_size').keydown(function(e){
	if(e.keyCode == 13)
		applyFontSize();
})

$('#design_tool').on('dblclick' , '.outline2.non-selectable.admin' , function(){
	$(this).addClass('selected');
	arrangeImageZees();
	updateToolbar();
});

$('#design_tool').on('dblclick' , '.outline2.locked' , function(e){
	$(this).removeClass('locked');
	selectElement.call(this, e);
});




$(document).on('mousedown', '.outline2' , selectElement);
$(document).on('mousedown', '.outline2' , grabElement);
$(document).on('mouseover', '.outline2' , highlight);
$(document).on('mouseout', '.outline2' , lowlight);
$(document).on('mousedown', '.handle' , triggerResizing);
$(document).on('keydown' , 'textarea , input' , function(e){if (e.keyCode != 27 && $(this).attr('id') != 'temp_text' ) stopPropag(e);});
$(document).on('keyup' , 'textarea , input' , function(e){if ($(this).attr('id') != 'temp_text' ) stopPropag(e);});
$(document).on('keypress' , 'textarea , input' , function(e){if ($(this).attr('id') != 'temp_text' ) stopPropag(e);});
$(document).on( 'mouseup mousedown click' , 'textarea , input' ,stopPropag);

$(document).on('mousedown' , '.menu-opener' , function(e){
	var btn = $(this).closest('.toolbar-button')
	var menu = btn.find('.button-menu');
	if( $(e.target).closest('.button-menu').length == 0 && ! btn.hasClass('disabled') ){ // mousedown on menu items won't close the menu
		btn.toggleClass('open');
	}
	if(btn.hasClass('open') && btn.closest('.float-toolbar').length > 0){
		menu.css('top' , 'auto') ;
		if (findPos(menu[0])[1] + menu.outerHeight(true) > $(window).height() + getPageScrollTop() ){
			menu.cssTop( - menu.outerHeight(true) );
		}
	}
});
$('.ztoolbar').on('mousedown' , function(){
	$('.ztoolbar').css('z-index' , 2500);
	$(this).css('z-index' , 2550);
});
$(document).on('mousedown' , function(e){
	$('.toolbar-button.has-menu').not( $(e.target).closest('.toolbar-button.has-menu') ).removeClass('open');
});


$('#design_tool').on('mouseup', function(e){
	view.mouseUpX = e.pageX ;
	view.mouseUpY = e.pageY ; 
	updateTextToolbars();
});

$(document).on('click' , '.font-size-option' , function(e){
	$('#selected_font_size').val( $(this).text() ) ;
	applyFontSize();
});

$(document).on('click' , '.shape-border-stroke-option' , function(e){
	applyShapeAttr('border_stroke' , $(this).attr('id').split('shape_border_stroke_').pop() ) ; 
});

$(document).on('click' , '.shape-border-width-option' , function(e){
	applyShapeAttr('border_width' , parseInt( $(this).text() ) ) ; 
});

$(document).on('click' , '.image-border-width-option' , function(e){
	applyImageAttr('border_width' , parseInt( $(this).text() ) ) ; 
});

$(document).on('click' , '.font-face-option' , function(e){
	applyTextAttr('font' , $(this).data('font_name') );
});
//Apply Filter
$(document).on('click' , '.image_filter_option' , function(e){
    var filter_name = $(this).attr('id').split('image_filter_').pop();
    console.log(filter_name);
    applyImageFilter(filter_name);
});

$('#gray_box').click(function(){
	if($('#add_image_popup').is(':visible') )
		return ;
		
	$('.simple-popup-close').click() ; 
	$('.iframe-popup , #gray_box').hide() ;
	$('.overlay-container').remove() ;
	$('.preview-bar').remove();
	$('#temp_text').focus();
});

function selectNextTextElement(e){
	ie8SafePreventEvent(e) ; 
	e.stopPropagation();

	var current = $('.selected.text') ;
	if(e.shiftKey){
		if(current.prev('.text').length > 0 )
			current.prev('.text').mousedown().mouseup();
		else
			current.parent().find('.text').eq(0).mousedown().mouseup() ;
	}
	else {
		if(current.next('.text').length > 0){
			current.next('.text').mousedown().mouseup();
		}
		else{
			current.parent().find('.text').eq(0).mousedown().mouseup();
		}
	}
}

$(document).ready(function() {
	var without_transparent = [];
	$.each(config.color_palette, function(k,v){
        if(v!='transparent')
            without_transparent.push(v);
    })
    $('input#txt_selected_bg_color').spectrum({
        color: "#FFF",
        flat: false,
        //showAlpha: true,
        showInput: true,
        dataId: "txt_selected_bg_color",
        showInitial: true,
        showPalette: true,
        togglePaletteOnly: true,
        revertTogglePaletteOnly: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        extraPreferredFormat: ["rgb","hsv","cmyk","name"],
        palette: [/*typeof config.colors!='undefined'?config.colors:*/ without_transparent],
        change: function(color){
            var t = color.toRgbString();
            var c = color.toHexString();
            if(t=='rgba(0, 0, 0, 0)') c = 'ffffff';
            c = c.replace('#','');
            view.changeColor = true;
            applyBgColor(c) ;
        }
    });
	$('input#txt_selected_font_color').spectrum({
        color: "#000",
        flat: false,
        //showAlpha: true,
        showInput: true,
        dataId: "txt_selected_font_color",
        showInitial: true,
        showPalette: true,
        togglePaletteOnly: true,
        revertTogglePaletteOnly: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        extraPreferredFormat: ["rgb","hsv","cmyk","name"],
        palette: [/*typeof config.colors!='undefined'?config.colors:*/ without_transparent],
        change: function(color){
            var t = color.toRgbString();
            var c = color.toHexString();
            if(t=='rgba(0, 0, 0, 0)') c = 'ffffff';
            c = c.replace('#','');
            view.changeColor = true;
            applyTextAttr('color',c);
        }
    });
	$('input#txt_selected_shape_border_color').spectrum({
        color: "#000",
        flat: false,
        //showAlpha: true,
        showInput: true,
        dataId: "txt_selected_shape_border_color",
        showInitial: true,
        showPalette: true,
        togglePaletteOnly: true,
        revertTogglePaletteOnly: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        extraPreferredFormat: ["rgb","hsv","cmyk","name"],
        localStorageKey: "spectrum.txt_selected_shape_border_color",
        palette: [config.color_palette],
        change: function(color){
            var t = color.toRgbString();
            var c = color.toHexString();
            if(t=='rgba(0, 0, 0, 0)') c = 'transparent';
            c = c.replace('#','');
            view.changeColor = true;
            applyShapeAttr('border_color',c);
        }
    });
    $('input#txt_selected_shape_fill_color').spectrum({
        color: "#fff",
        flat: false,
        //showAlpha: true,
        showInput: true,
        dataId: "txt_selected_shape_fill_color",
        showInitial: true,
        showPalette: true,
        togglePaletteOnly: true,
        revertTogglePaletteOnly: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.txt_selected_shape_fill_color",
        extraPreferredFormat: ["rgb","hsv","cmyk","name"],
        palette: [config.color_palette],
        change: function(color){
            var t = color.toRgbString();
            var c = color.toHexString();
            if(t=='rgba(0, 0, 0, 0)') c = 'transparent';
            c = c.replace('#','');
            view.changeColor = true;
            applyShapeAttr('fill_color',c);
        }
    });
    $('input#txt_selected_image_border_color').spectrum({
        color: "#000",
        flat: false,
        //showAlpha: true,
        showInput: true,
        dataId: "txt_selected_image_border_color",
        showInitial: true,
        showPalette: true,
        togglePaletteOnly: true,
        revertTogglePaletteOnly: true,
        clickoutFiresChange: true,
        showSelectionPalette: true,
        maxPaletteSize: 10,
        preferredFormat: "hex",
        localStorageKey: "spectrum.txt_selected_image_border_color",
        palette: [config.color_palette],
        change: function(color){
            var t = color.toRgbString();
            var c = color.toHexString();
            if(t=='rgba(0, 0, 0, 0)') c = 'transparent';
            c = c.replace('#','');
            view.changeColor = true;
            applyImageAttr('border_color' , c) ;
        }
    });
    $(".sp-button-container",".sp-container").before("<a onclick=\"pickerTool(this)\" style=\"margin-left: 5px;\" title=\"Color picker\"><img  src=\"/lib/design/images/color-picker.png\" /></a><br />");
    //Shape
    $("body").on("mouseup",".shape",function(event){
    	if(event.which == 3){
	    	var $this = $(this);
	        // store a callback on the trigger
	        $this.data('runCallbackThingie', function(){
	        	return {
		            callback: function(key, options) {
		                switch(key){
		                	case "lock":
		                		$("#btn_edit_lock").trigger("click");
		                		break;
		                	case "copy":
		                		$("#btn_edit_copy").trigger("click");
		                		break;
		                	case "delete":
		                		$("#btn_edit_delete").trigger("click");
		                		break;
		                }
		            },
		            items: {
		                "lock": {name: "Lock", icon: "lock"},
		                "copy": {name: "Copy", icon: "copy"},
		                "delete": {name: "Delete", icon: "delete"}
		            }
		        };
	        });
	        var position = {
	                x: view.mousePosX + 10, 
	                y: view.mousePosY
	            }
	        // open the contextMenu asynchronously
	        setTimeout(function(){ $this.contextMenu(position); }, 200);
    	}
    });
    $.contextMenu({
        selector: '.shape',
        trigger: 'none',
        build: function($trigger, e) {
            // pull a callback from the trigger
            return $trigger.data('runCallbackThingie')();
        }
    });
    //Image
    $("body").on("mouseup",".image",function(event){
    	if(event.which == 3){
	    	var $this = $(this);
	        // store a callback on the trigger
	        $this.data('runCallbackThingie', function(){
	        	return {
		            callback: function(key, options) {
		                switch(key){
		                	case "lock":
		                		$("#btn_edit_lock").trigger("click");
		                		break;
		                	case "copy":
		                		$("#btn_edit_copy").trigger("click");
		                		break;
		                	case "delete":
		                		$("#btn_edit_delete").trigger("click");
		                		break;
		                }
		            },
		            items: {
		                "lock": {name: "Lock", icon: "lock"},
		                "copy": {name: "Copy", icon: "copy"},
		                "delete": {name: "Delete", icon: "delete"}
		            }
		        };
	        });
	        var position = {
	                x: view.mousePosX + 10, 
	                y: view.mousePosY
	            }
	        // open the contextMenu asynchronously
	        setTimeout(function(){ $this.contextMenu(position); }, 200);
    	}
    });
    $.contextMenu({
        selector: '.image',
        trigger: 'none',
        build: function($trigger, e) {
            // pull a callback from the trigger
            return $trigger.data('runCallbackThingie')();
        }
    });
	resetView();
	$('*').not('input').not('textarea').attr('unselectable', 'on').on('selectstart', false);

//	$('input,textarea').keydown(stopPropag).keyup(stopPropag);

//	$('#textbox_tools').append($('<div />').addClass('float-toolbar-handle open-only')).draggable({'handle' : '.float-toolbar-handle'});
	$('#textbox_tools').draggable({'handle' : '#textbox_tools_text_title'});
	$('#text_fields').mouseenter(function(){$(this).css('overflow' , 'auto') });
	$('#text_fields').mouseleave(function(){$(this).css('overflow' , 'hidden') });

	$('#inline_textarea')
		.keyup(updateInlineTextEdit)
		.bind('paste cut', function() { 
				var _this = this ;
				setTimeout(function() { 
					updateInlineTextEdit.call(_this);
				}, 100); 
		})
		.bind('keydown' , function(e){
			if(e.keyCode == 13 && ( e.ctrlKey || e.shiftKey )){
				$('#btn_text_done').click();
			}
			if(e.keyCode == 13 && ( $(this).val().length == 0 || $(this).hasClass('fit') ) ){
				return false ;
			}
			if(e.keyCode == 9){
				//if(user.admin_mode!=1) selectNextTextElement(e);
			}
		}) ;


    $('#inline_textarea_preset')
        .keyup(updateInlinePresetEdit)
        .bind('paste cut', function() {
            var _this = this ;
            setTimeout(function() {
                updateInlinePresetEdit.call(_this);
            }, 100);
        })
        .bind('keydown' , function(e){
            if(e.keyCode == 13 && ( e.ctrlKey || e.shiftKey )){
                $('#btn_text_done').click();
            }
            if(e.keyCode == 13 && ( $(this).val().length == 0 || $(this).hasClass('fit') ) ){
                return false ;
            }
            if(e.keyCode == 9){
                //selectNextTextElement(e);
            }
        }) ;

    $('#inline_textarea_title')
        .keyup(updateInlineTitleEdit)
        .bind('paste cut', function() {
            var _this = this ;
            setTimeout(function() {
                updateInlineTitleEdit.call(_this);
            }, 100);
        })
        .bind('keydown' , function(e){
            if(e.keyCode == 13 && ( e.ctrlKey || e.shiftKey )){
                $('#btn_text_done').click();
            }
            if(e.keyCode == 13 && ( $(this).val().length == 0 || $(this).hasClass('fit') ) ){
                return false ;
            }
            if(e.keyCode == 9){
                selectNextTextElement(e);
            }
        }) ;



    window.onbeforeunload = function (e) {
		if (view.isDesignSaved )
			return ; 
		
		var e = e || window.event;
		  // For IE and Firefox
		  if (e) {
			e.returnValue = 'You will lose your design if you leave this page.';
		  }
		  // For Safari
		  return 'You will lose your design if you leave this page.';
		
	}

//	applyUserSettings() ; 
	applyUrlHash() ;
	populateDropDowns() ;

	$('#main_loading').remove() ;
		
	$('#crop_slider').slider({
		min: 1 ,
		max: 2 ,
		step: 0.01,
		slide: function(event, ui) { 
			scaleInnerImage(ui.value);
		} ,
		stop: function(event, ui){
			updateJdesign();
			adjustcropSlider() ;
		}
	});
	
    $('div#btn_text_font_size').find('div.button-arrow').on('click', function(e){
        var parentOffset = $(this).parent().offset();
        var relativeYPosition = (e.pageY - parentOffset.top);
        var size = $('div#btn_text_font_size').find('input#selected_font_size').val();
        size = parseInt(size, 10);
        if(isNaN(size) || size < 0) size = 0;
        if(relativeYPosition > 9){
            if(size > 0) size--;
        }else{
            size++;
        }
        applyTextAttr('pointsize' , size);
    });
}) ;  

window.onhashchange = applyUrlHash ;
$(document).on('mouseup' , '.color-pick' , stopPropag) ;
$(document).on('click' , '.color-pick' , function(e){
	var btn = $(this).closest('.toolbar-button') ;
	btn.removeClass('open');
	var colorCode = $(this).data('color') + (typeof $(this).data('color_index')!= 'undefined' ? ('#' +  $(this).data('color_index')) : '' ) ;

	switch(btn.attr('id')){
		case 'btn_text_font_color': 
			applyTextAttr('color' , colorCode ) ;
			break;

		case 'btn_shape_fill_color': 
			applyShapeAttr('fill_color' ,  colorCode) ;
			break;

		case 'btn_shape_border_color': 
			applyShapeAttr('border_color' ,  colorCode) ;
			break;

		case 'btn_image_border_color':
			applyImageAttr('border_color' ,  colorCode) ;
			break;

		case 'btn_bg_color': 
			applyBgColor(colorCode) ;
			break;

		case 'svg_tools': 
			applySvgColor(colorCode) ;
			break;

	}
});

$(document).on('mouseup', '.svg-color-item' , stopPropag) ; 
$(document).on('mousedown', '.svg-color-item' , function(){
	var el= getElem($('.selected.svg').attr('id').split('outline_')[1]) ;
	$(this).addClass('on').siblings().removeClass('on');
	$('#svg_color_control').addClass('open');
	$('#svg_color_menu .color-pick').removeClass('on');
	$('#svg_color_menu .color-'+ el.svg_colors[$(this).index()].replace('#' , '_') ).addClass('on');
});

//document.oncontextmenu = function(){return false};
$(document).mousedown(function(e) {
	view.mouseDownX = e.pageX ;
	view.mouseDownY = e.pageY ;
	view.dragLockX = true;
	view.dragLockY = true;
	if ( e.target  == document.body || ($.inArray(e.target.id , ['tool_placeholder','workspace','left_panel','page_navigation','div_wrapper'] ) != -1 )) {
		view.selectorX = e.pageX - getCanvasX() ;
		view.selectorY = e.pageY - getCanvasY() ;
		deselectAll() ;
	}
}) ; 


$(document).mouseup(function(e) {
		view.mouseUpX = e.pageX ;
		view.mouseUpY = e.pageY ; 
		if(! view.showBleed) 
			$('.safe-zone-mask').hide();
		$('.snap-guide').hide();
		$('.image-mask').removeClass('crop');
		$('.grabbed').removeClass('grabbed') ; 
		$('.qcrop').removeClass('qcrop') ; 
		$('.resizing').removeClass('resizing') ; 
		$('.outline2').css("cursor" , "") ; 
		$('#droppable_layers').hide() ;
		arrangeOutlineZindex() ; 
		
		for (i in view.hSnapPoints)
			view.hSnapBuffer[i] = 0 ;

		for (i in view.hSnapPoints)
			view.vSnapBuffer[i] = 0 ;

		updateJdesign() ;

		// Exist the tool from resizing state
		if (view.resizing ){
			view.resizing = false ; 

			if (typeof resize.jel != 'undefined') {
				if (typeof resize.jel.fotoliaId != 'undefined'){
					updateFotoliaLicense(resize.jel) ;
					updateStockImagePrices() ;
				}
				// Regular Images
				else if (typeof resize.jel.image_id != 'undefined'){
					checkImageResolution(resize.jel) ;
				}
				else if (typeof resize.jel.shape_type!= 'undefined'){
					$('#inner_img_'+resize.jel.name).attr('src' , getMockupSrc(resize.jel));
				}
			
			}
			
		}

		if (typeof view.selectorX != 'undefined')  {
			delete view.selectorX ;
			delete view.selectorY ; 
			$('#multi_selector').hide() ; 
			updateToolbar() ; 
		}

}) ;

$(window).resize(positionToolbars) ;
$(window).scroll(positionToolbars) ;

$(document).keyup(function(e) {
//	console.log(e.keyCode);
	// push movement with cursor when the key is released 
	if (isActiveCanvas() || e.keyCode == 83 && e.ctrlKey) {
		ie8SafePreventEvent(e) ; 
		if (e.keyCode == 37 ||  e.keyCode == 38 || e.keyCode == 39 || e.keyCode == 40 ) {
				updateJdesign() ; 
		}
	}

	if(e.keyCode == 13 && ! e.ctrlKey && !  e.shiftKey && $('#textbox_tools').is(':visible') &&  $('#textbox_tools').hasClass('close')){
		toggleTextboxTool(true);
	}


}) ; 

$(document).keypress(function(e) {
	// disabling browser save action on Ctrl + S
//	console.log('keypress', e.charCode);
	if (e.ctrlKey  && (e.charCode == 115 || e.charCode == 83 )) {
		ie8SafePreventEvent(e) ; 
	}

	// CTRL + A
	if (e.ctrlKey && (e.charCode == 65 || e.charCode == 97) ) {
		ie8SafePreventEvent(e) ; 
		if(isActiveCanvas()){
			$('#outline_layers .outline2').not('.locked').not('.non-selectable').addClass('selected') ; 
			updateToolbar() ; 
		}
	}

}) ;

$(document).keydown(function(e) { 
//	console.log('keydown' , e.keyCode);
	// CTRL + A
	if (e.ctrlKey && e.keyCode == 65) {
		ie8SafePreventEvent(e) ; 
		if(isActiveCanvas()){
			$('#outline_layers .outline2').not('.locked').not('.non-selectable').not('.pholder').addClass('selected') ; 
			updateToolbar() ; 
		}
	}
	
	// disable browser zoom in and out 
	if (!window.ActiveXObject && e.ctrlKey && (e.keyCode == 187 || e.keyCode == 107 || e.keyCode == 61) ){
		ie8SafePreventEvent(e) ; 
		zoomIn();
	}

	if (!window.ActiveXObject && e.ctrlKey && ( e.keyCode == 189 || e.keyCode == 109 || e.keyCode == 173)) {
		ie8SafePreventEvent(e) ; 
		zoomOut();
	}

	// ESC key
		
	if(e.keyCode == 27){
		$('#gray_box').click() ; 
		$('.drop-down').removeClass('open') ; 
		deselectAll();
		$('#loading').hide();
		clearTimeout(view.loadingTimer);
	}
	
	// Ctrl + S
	if (e.keyCode == 83 && e.ctrlKey) {
		ie8SafePreventEvent(e) ; 
		if (user.admin_mode === 1){
			if(e.shiftKey){
				admin.saveTemplate();
			} else {
				admin.updateTemplate();
			}
		}
		else {
			if(e.shiftKey){
				saveDesignAs();
			} else {
				saveDesign();
			}
		}
	}

	if (isActiveCanvas()) {
		if (e.keyCode == 9){
			selectNextTextElement(e);
		}
		// CTRL + Z
		else if (e.keyCode == 90 && e.ctrlKey){
			ie8SafePreventEvent(e) ; 
			undo() ;
		}
		// CTRL + Y
		else if (e.keyCode == 89 && e.ctrlKey){
			ie8SafePreventEvent(e) ; 
			redo() ;
		}
		// CTRL + C
		else if (e.keyCode == 67 && e.ctrlKey){
			ie8SafePreventEvent(e) ; 
			copyElements() ;
		}
		// CTRL + V
		else if (e.keyCode == 86 && e.ctrlKey){
			ie8SafePreventEvent(e) ; 
			pasteElements() ;
		}

		// DEL key and Backspace key
		else if (e.keyCode == 46 || e.keyCode == 8) {
			ie8SafePreventEvent(e) ;
			deleteSelectedElements() ; 
			updateToolbar() ; 
			return ;
		}


		// left cursor
		else if (e.keyCode == 37) {
			ie8SafePreventEvent(e) ; 
			$(".selected").not('.unmovable').not('.crop-box').each(function() {
				view.outdatedJdesign = true ;
				newL = $(this).cssLeft() - 1 ;
				$(this).cssLeft(newL) ;
				$('#' +	$(this).attr('id').split('outline_')[1]).cssLeft(newL) ;
			}) ; 
		}
		// up cursor
		else if (e.keyCode == 38) {
			ie8SafePreventEvent(e) ; 
			$(".selected").not('.unmovable').not('.crop-box').each(function() {
				view.outdatedJdesign = true ;					
				newT = $(this).cssTop() - 1 ;
				$(this).cssTop(newT) ;
				$('#' +	$(this).attr('id').split('outline_')[1]).cssTop(newT) ;
			}) ; 
		}
		// right cursor
		else if (e.keyCode == 39) {
			ie8SafePreventEvent(e) ; 
			$(".selected").not('.unmovable').not('.crop-box').each(function() {
				view.outdatedJdesign = true ;					
				var newL = $(this).cssLeft() + 1 ;
				$(this).cssLeft(newL) ;
				$('#' +	$(this).attr('id').split('outline_')[1]).cssLeft(newL) ;
			}) ; 
		}

		// down cursor
		else if (e.keyCode == 40) {
			ie8SafePreventEvent(e) ; 
			$(".selected").not('.unmovable').not('.crop-box').each(function() {
				view.outdatedJdesign = true ;					
				var newT = $(this).cssTop() + 1 ;
				$(this).cssTop(newT) ;
				$('#' +	$(this).attr('id').split('outline_')[1]).cssTop(newT) ;
			}) ; 
		}
	}
	
}) ;

$(document).mousemove(function(e) {
	view.mousePosX = e.pageX ;
	view.mousePosY = e.pageY ;
	
	if (typeof view.selectorX !== 'undefined'){
		ie8SafePreventEvent(e) ;
		startMultiSelect(e) ; 
	}
	else if (view.resizing) {
		resizeElement(e) ;
	}
	else if ($(".grabbed").length > 0 || $(".qcrop").length > 0 ) {
		dragElement(e) ; 
	}
}) ;

$(document).on('click' , '.ez-theme-option' , function(e){
	var theme_id = $(this).attr('id').split('_').pop() ;
	var theme ;
	$.each(view.themes, function(k,v){
		if(v.id * 1 == theme_id *1 ){
			theme = v ;
		}
	});
	applyThemeToDesign(theme);
});

/*
function easyLogout(){
	if (typeof warnUnsaved != 'undefined' && ! warnUnsaved(easyLogout)) return ;
	window.location = env.logoutURL ;
}
*/


//My function
function update_image(data, images){
    var found, j;
    for(var i=0; i<images.length; i++){
        var image_id = images[i].id;
        image_id = parseInt(image_id, 10);
        found = false;
        j=0;
        while(j<data.images.length && !found){
            var tmp_image_id = data.images[j].id;
            tmp_image_id = parseInt(tmp_image_id, 10);
            found = tmp_image_id == image_id;
            j++;
        }
        if(!found) data.images.push(images[i]);
    }
}

function jsonp_callback(response){
    jsonp_data = response;
}

function close_submenu(response){
	$("#btn_preview_s").removeClass('open');
}

function pickerTool(obj){
	var data = {"json" : JSON.stringify(view.jdesign), "page" : '0', "dpi" : view.baseDpi, cross: is_cross_domain, ext:'jpg'};
	var data_key = JSON.stringify(data);
	var getColor = window['getColor'];
	if(localStorage[data_key] != undefined){
		getColor(localStorage[data_key],obj);
	} else {
		$.ajax({
	        url: "/tools.php?action=assets&action_type=preview_design" ,
			type: "post" ,
			data: data,
			dataType: is_cross_domain? 'json':'jsonp' ,
			jsonpCallback: is_cross_domain?null:'jsonp_callback' ,
			success: function(data){
	            if(data.file!=undefined){
	            	var url = data.file;
	            	url = url.split("/");
	            	url = url[(url.length -1)];
	            	url = url.replace(".jpg","");
	            	localStorage.setItem(data_key,url);
	            	getColor(url,obj);
	            }
			} ,
	        beforeSend: function(){
	            //alert('SEnd');
	        },
			error: function(xhr, textStatus, error){
				showAlert('An error occured while generating the preview. Please try again.');
			}
		}) ;
	}
}

function getColor(url,obj){
	$('#loading').show() ; 
	var inputId = $(obj).parent().parent().attr("data-id");
	$.ajax({
        //url: "/ajax/raster/getJsonPreview.php" ,
        url: "/tools.php?action=assets&action_type=preview_design" ,
		type: "post" ,  
		data: {"json" : JSON.stringify(view.jdesign) , "page" : view.currentPage , "dpi" : view.baseDpi, cross: is_cross_domain, ext:'png'},
		asycn: false,
		dataType: is_cross_domain? 'json':'jsonp' ,
		jsonpCallback: is_cross_domain?null:'jsonp_callback' ,
		success: function(data){
            //alert(data.file);
			$('<img />').attr('src' , data.file).appendTo($("body")).hide()
						.load(function(){
							$('#loading').hide() ; 
							$(this).hide();
							$(".sp-container").hide();
							var margin = $("div.bleed-area-mask:eq(2)","#canvas").css("margin");
							if(!$("#canvas-preview","#canvas #mask_layers").length)
								$("#canvas #mask_layers").append('<canvas id="canvas-preview" width="'+$(this).width()+'" height="'+$(this).height()+'" style="margin: '+margin+'" ></canvas>');
							var img = new Image();
							img.src = $(this).attr("src");
							var context = document.getElementById('canvas-preview').getContext('2d');
							context.drawImage(img, 0, 0);
							$("#outline_layers").hide();
							$(".safe-zone-mask").hide();
							$(".no-bleed-area-mask").hide();
							$("div.bleed-area-mask").hide();
							$('#canvas-preview').mousemove(function(e) {
							    var pos = findPosColorPicker(this);
							    var x = e.pageX - pos.x;
							    var y = e.pageY - pos.y;
							    var coord = "x=" + x + ", y=" + y;
							    var c = this.getContext('2d');
							    var p = c.getImageData(x, y, 1, 1).data; 
							    var hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);
							    $('#'+inputId).spectrum("set",hex);
							    hex = hex.replace("#","");
							    switch(inputId){
							    	case 'txt_selected_bg_color':
								    	applyBgColor(hex);
								    	break;
								    case 'txt_selected_font_color':
								    	applyTextAttr('color',hex);
								    	break;
								    case 'txt_selected_shape_border_color':
								    	applyShapeAttr('border_color',hex);
								    	break;
								    case 'txt_selected_shape_fill_color':
								    	applyShapeAttr('fill_color',hex);
								    	break;
								    case 'txt_selected_image_border_color':
								    	applyImageAttr('border_color' ,hex);
								    	break;
							    }
							});
							$('#canvas-preview').click(function(){
								$(".sp-container").show();
								$("#outline_layers").show();
								$(".safe-zone-mask").show();
								$(".no-bleed-area-mask").show();
								$("div.bleed-area-mask").show();
								$(this).remove();
							});
						})
						.click(stopPropag) ;
		} ,
        beforeSend: function(){
            //alert('SEnd');
        },
		error: function(xhr, textStatus, error){
            //alert(textStatus+' --- '+ error);
			$('#gray_box').click();
			showAlert('An error occured while generating the preview. Please try again.');
		}
	}) ;	

}
function getPreviewForColorPicker(page, preview, hide, ext, obj){
	var inputId = $(obj).parent().parent().attr("data-id");
	if (hide) $('<img />').attr('src' , config.preloadImages[1]).attr('id', 'preview_' + page).addClass('preview-load').appendTo(preview).hide();

	$.ajax({
        //url: "/ajax/raster/getJsonPreview.php" ,
        url: "/tools.php?action=assets&action_type=preview_design" ,
		type: "post" ,  
		data: {"json" : JSON.stringify(view.jdesign) , "page" : page , "dpi" : view.baseDpi, cross: is_cross_domain, ext:ext},
		asycn: false,
		dataType: is_cross_domain? 'json':'jsonp' ,
		jsonpCallback: is_cross_domain?null:'jsonp_callback' ,
		success: function(data){
            //alert(data.file);
            if(!is_cross_domain) data = jsonp_data;
			$('#preview_' + page).remove();
			$('<img />').attr('src' , data.file).attr('id', 'preview_' + page).addClass('large-preview').appendTo(preview).hide()
						.load(function(){
							if (!hide) {
								if (!$('#preview_btn' + page).siblings().hasClass('on')) {
									$(this).fadeIn(300);
									$('#preview_btn' + page).addClass('on');
								}
								$('#preview_wait').hide();
								$('.overlay-container').click(function(){
									$('#gray_box').click();
								});
								$('.preview-btn').show();
							} else if ($('#preview_btn' + page).hasClass('on')) {
								$(this).fadeIn(300);
							}
							preview.height($(this).height()).width($(this).width());
							$(this).hide();
							if(!$("#canvas-preview","#preview_container").length)
								$("#preview_container").append('<canvas id="canvas-preview" width="700" height="840" style="margin: 80px 0 0 -2px" ></canvas>');
							var img = new Image();
							img.src = $(this).attr("src");
							var context = document.getElementById('canvas-preview').getContext('2d');
							context.drawImage(img, 0, 0);
							$('#canvas-preview').mousemove(function(e) {
							    var pos = findPosColorPicker(this);
							    var x = e.pageX - pos.x;
							    var y = e.pageY - pos.y;
							    var coord = "x=" + x + ", y=" + y;
							    var c = this.getContext('2d');
							    var p = c.getImageData(x, y, 1, 1).data; 
							    var hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);
							    $('#'+inputId).spectrum("set",hex);
							    hex = hex.replace("#","");
							    switch(inputId){
							    	case 'txt_selected_bg_color':
								    	applyBgColor(hex);
								    	break;
								    case 'txt_selected_font_color':
								    	applyTextAttr('color',hex);
								    	break;
								    case 'txt_selected_shape_border_color':
								    	applyShapeAttr('border_color',hex);
								    	break;
								    case 'txt_selected_shape_fill_color':
								    	applyShapeAttr('fill_color',hex);
								    	break;
								    case 'txt_selected_image_border_color':
								    	applyImageAttr('border_color' ,hex);
								    	break;
							    }
							});
						})
						.click(stopPropag) ;
		} ,
        beforeSend: function(){
            //alert('SEnd');
        },
		error: function(xhr, textStatus, error){
            //alert(textStatus+' --- '+ error);
			$('#gray_box').click();
			showAlert('An error occured while generating the preview. Please try again.');
		}
	}) ;	
}

function findPosColorPicker(obj) {
    var curleft = 0, curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { x: curleft, y: curtop };
    }
    return undefined;
}

function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}

function flip(type) {
    var element_id;
    $('.selected.text').each(function() {
        element_id = $(this).attr('id').split('outline_')[1];
        var jtext = getElem(element_id) ;
        if(typeof jtext['flip']=='undefined') jtext['flip'] = 'none';
        if(jtext['flip']=='none')
            jtext['flip'] = type;
        else if(jtext['flip']==type)
            jtext['flip']='none';
        else if(jtext['flip']=='both'){
            jtext['flip'] = type=='horizontal'?'vertical':'horizontal';
        }else if(jtext['flip'] != type)
            jtext['flip'] = 'both';
        updateTextElement(element_id);
    });
    $('.selected[id^="outline_img_"]').each(function() {
        element_id = $(this).attr('id').split('outline_')[1] ;
        var jElement = getElem(element_id) ;
        if(typeof jElement['flip']=='undefined') jElement['flip'] = 'none';
        if(jElement['flip']=='none')
            jElement['flip'] = type;
        else if(jElement['flip']==type)
            jElement['flip'] = 'none';
        else if(jElement['flip']=='both'){
            jElement['flip'] = type=='horizontal'?'vertical':'horizontal';
        }else if(jElement['flip']!=type)
            jElement['flip'] = 'both';
        $('#inner_'+element_id).attr('src' ,  getMockupSrc(jElement) ) ;
    });
    pushState() ;
}
