/**
 * Copyright (c) 2005 - 2010, James Auldridge
 * All rights reserved.
 *
 * Licensed under the BSD, MIT, and GPL (your choice!) Licenses:
 *  http://code.google.com/p/cookies/wiki/License
 *
 */
var jaaulde=window.jaaulde||{};jaaulde.utils=jaaulde.utils||{};jaaulde.utils.cookies=(function(){var resolveOptions,assembleOptionsString,parseCookies,constructor,defaultOptions={expiresAt:null,path:'/',domain:null,secure:false};resolveOptions=function(options){var returnValue,expireDate;if(typeof options!=='object'||options===null){returnValue=defaultOptions;}else
{returnValue={expiresAt:defaultOptions.expiresAt,path:defaultOptions.path,domain:defaultOptions.domain,secure:defaultOptions.secure};if(typeof options.expiresAt==='object'&&options.expiresAt instanceof Date){returnValue.expiresAt=options.expiresAt;}else if(typeof options.hoursToLive==='number'&&options.hoursToLive!==0){expireDate=new Date();expireDate.setTime(expireDate.getTime()+(options.hoursToLive*60*60*1000));returnValue.expiresAt=expireDate;}if(typeof options.path==='string'&&options.path!==''){returnValue.path=options.path;}if(typeof options.domain==='string'&&options.domain!==''){returnValue.domain=options.domain;}if(options.secure===true){returnValue.secure=options.secure;}}return returnValue;};assembleOptionsString=function(options){options=resolveOptions(options);return((typeof options.expiresAt==='object'&&options.expiresAt instanceof Date?'; expires='+options.expiresAt.toGMTString():'')+'; path='+options.path+(typeof options.domain==='string'?'; domain='+options.domain:'')+(options.secure===true?'; secure':''));};parseCookies=function(){var cookies={},i,pair,name,value,separated=document.cookie.split(';'),unparsedValue;for(i=0;i<separated.length;i=i+1){pair=separated[i].split('=');name=pair[0].replace(/^\s*/,'').replace(/\s*$/,'');try
{value=decodeURIComponent(pair[1]);}catch(e1){value=pair[1];}if(typeof JSON==='object'&&JSON!==null&&typeof JSON.parse==='function'){try
{unparsedValue=value;value=JSON.parse(value);}catch(e2){value=unparsedValue;}}cookies[name]=value;}return cookies;};constructor=function(){};constructor.prototype.get=function(cookieName){var returnValue,item,cookies=parseCookies();if(typeof cookieName==='string'){returnValue=(typeof cookies[cookieName]!=='undefined')?cookies[cookieName]:null;}else if(typeof cookieName==='object'&&cookieName!==null){returnValue={};for(item in cookieName){if(typeof cookies[cookieName[item]]!=='undefined'){returnValue[cookieName[item]]=cookies[cookieName[item]];}else
{returnValue[cookieName[item]]=null;}}}else
{returnValue=cookies;}return returnValue;};constructor.prototype.filter=function(cookieNameRegExp){var cookieName,returnValue={},cookies=parseCookies();if(typeof cookieNameRegExp==='string'){cookieNameRegExp=new RegExp(cookieNameRegExp);}for(cookieName in cookies){if(cookieName.match(cookieNameRegExp)){returnValue[cookieName]=cookies[cookieName];}}return returnValue;};constructor.prototype.set=function(cookieName,value,options){if(typeof options!=='object'||options===null){options={};}if(typeof value==='undefined'||value===null){value='';options.hoursToLive=-8760;}else if(typeof value!=='string'){if(typeof JSON==='object'&&JSON!==null&&typeof JSON.stringify==='function'){value=JSON.stringify(value);}else
{throw new Error('cookies.set() received non-string value and could not serialize.');}}var optionsString=assembleOptionsString(options);document.cookie=cookieName+'='+encodeURIComponent(value)+optionsString;};constructor.prototype.del=function(cookieName,options){var allCookies={},name;if(typeof options!=='object'||options===null){options={};}if(typeof cookieName==='boolean'&&cookieName===true){allCookies=this.get();}else if(typeof cookieName==='string'){allCookies[cookieName]=true;}for(name in allCookies){if(typeof name==='string'&&name!==''){this.set(name,null,options);}}};constructor.prototype.test=function(){var returnValue=false,testName='cT',testValue='data';this.set(testName,testValue);if(this.get(testName)===testValue){this.del(testName);returnValue=true;}return returnValue;};constructor.prototype.setOptions=function(options){if(typeof options!=='object'){options=null;}defaultOptions=resolveOptions(options);};return new constructor();})();(function(){if(window.jQuery){(function($){$.cookies=jaaulde.utils.cookies;var extensions={cookify:function(options){return this.each(function(){var i,nameAttrs=['name','id'],name,$this=$(this),value;for(i in nameAttrs){if(!isNaN(i)){name=$this.attr(nameAttrs[i]);if(typeof name==='string'&&name!==''){if($this.is(':checkbox, :radio')){if($this.attr('checked')){value=$this.val();}}else if($this.is(':input')){value=$this.val();}else
{value=$this.html();}if(typeof value!=='string'||value===''){value=null;}$.cookies.set(name,value,options);break;}}}});},cookieFill:function(){return this.each(function(){var n,getN,nameAttrs=['name','id'],name,$this=$(this),value;getN=function(){n=nameAttrs.pop();return!!n;};while(getN()){name=$this.attr(n);if(typeof name==='string'&&name!==''){value=$.cookies.get(name);if(value!==null){if($this.is(':checkbox, :radio')){if($this.val()===value){$this.attr('checked','checked');}else
{$this.removeAttr('checked');}}else if($this.is(':input')){$this.val(value);}else
{$this.html(value);}}break;}}});},cookieBind:function(options){return this.each(function(){var $this=$(this);$this.cookieFill().change(function(){$this.cookify(options);});});}};$.each(extensions,function(i){$.fn[i]=this;});})(window.jQuery);}})();

/*
 Masked Input plugin for jQuery
 Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
 Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
 Version: 1.3.1
 */
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);

/*!
 jQuery ColorBox v1.3.34 - 2013-02-04
 (c) 2013 Jack Moore - jacklmoore.com/colorbox
 license: http://www.opensource.org/licenses/mit-license.php
 */
(function(e,t,i){function o(i,o,n){var h=t.createElement(i);return o&&(h.id=V+o),n&&(h.style.cssText=n),e(h)}function n(e){var t=T.length,i=(N+e)%t;return 0>i?t+i:i}function h(e,t){return Math.round((/%/.test(e)?("x"===t?C.width():C.height())/100:1)*parseInt(e,10))}function r(e){return P.photo||P.photoRegex.test(e)}function l(e){return P.retinaUrl&&i.devicePixelRatio>1?e.replace(P.photoRegex,P.retinaSuffix):e}function s(){var t,i=e.data(B,J);null==i?(P=e.extend({},X),console&&console.log&&console.log("Error: cboxElement missing settings object")):P=e.extend({},i);for(t in P)e.isFunction(P[t])&&"on"!==t.slice(0,2)&&(P[t]=P[t].call(B));P.rel=P.rel||B.rel||e(B).data("rel")||"nofollow",P.href=P.href||e(B).attr("href"),P.title=P.title||B.title,"string"==typeof P.href&&(P.href=e.trim(P.href))}function a(i,o){e(t).trigger(i),st.trigger(i),e.isFunction(o)&&o.call(B)}function d(){var e,t,i,o,n,h=V+"Slideshow_",r="click."+V;P.slideshow&&T[1]?(t=function(){clearTimeout(e)},i=function(){(P.loop||T[N+1])&&(e=setTimeout($.next,P.slideshowSpeed))},o=function(){L.html(P.slideshowStop).unbind(r).one(r,n),st.bind(tt,i).bind(et,t).bind(it,n),m.removeClass(h+"off").addClass(h+"on")},n=function(){t(),st.unbind(tt,i).unbind(et,t).unbind(it,n),L.html(P.slideshowStart).unbind(r).one(r,function(){$.next(),o()}),m.removeClass(h+"on").addClass(h+"off")},P.slideshowAuto?o():n()):m.removeClass(h+"off "+h+"on")}function c(t){q||(B=t,s(),T=e(B),N=0,"nofollow"!==P.rel&&(T=e("."+Y).filter(function(){var t,i=e.data(this,J);return i&&(t=e(this).data("rel")||i.rel||this.rel),t===P.rel}),N=T.index(B),-1===N&&(T=T.add(B),N=T.length-1)),O||(O=j=!0,m.css({visibility:"hidden",display:"block"}),k=o(at,"LoadedContent","width:0; height:0; overflow:hidden").appendTo(g),_=x.height()+b.height()+g.outerHeight(!0)-g.height(),K=v.width()+y.width()+g.outerWidth(!0)-g.width(),z=k.outerHeight(!0),D=k.outerWidth(!0),P.returnFocus&&(e(B).blur(),st.one(ot,function(){e(B).focus()})),p.css({opacity:parseFloat(P.opacity),cursor:P.overlayClose?"pointer":"auto",visibility:"visible"}).show(),P.w=h(P.initialWidth,"x"),P.h=h(P.initialHeight,"y"),$.position(),rt&&C.bind("resize."+lt+" scroll."+lt,function(){p.css({width:C.width(),height:C.height(),top:C.scrollTop(),left:C.scrollLeft()})}).trigger("resize."+lt),d(),a(Z,P.onOpen),R.add(H).hide(),F.html(P.close).show()),$.load(!0))}function u(){!m&&t.body&&(Q=!1,C=e(i),m=o(at).attr({id:J,"class":ht?V+(rt?"IE6":"IE"):""}).hide(),p=o(at,"Overlay",rt?"position:absolute":"").hide(),W=o(at,"LoadingOverlay").add(o(at,"LoadingGraphic")),w=o(at,"Wrapper"),g=o(at,"Content").append(H=o(at,"Title"),E=o(at,"Current"),M=o(at,"Next"),S=o(at,"Previous"),L=o(at,"Slideshow"),F=o(at,"Close")),w.append(o(at).append(o(at,"TopLeft"),x=o(at,"TopCenter"),o(at,"TopRight")),o(at,!1,"clear:left").append(v=o(at,"MiddleLeft"),g,y=o(at,"MiddleRight")),o(at,!1,"clear:left").append(o(at,"BottomLeft"),b=o(at,"BottomCenter"),o(at,"BottomRight"))).find("div div").css({"float":"left"}),I=o(at,!1,"position:absolute; width:9999px; visibility:hidden; display:none"),R=M.add(S).add(E).add(L),e(t.body).append(p,m.append(w,I)))}function f(){function i(e){e.which>1||e.shiftKey||e.altKey||e.metaKey||(e.preventDefault(),c(this))}return m?(Q||(Q=!0,M.click(function(){$.next()}),S.click(function(){$.prev()}),F.click(function(){$.close()}),p.click(function(){P.overlayClose&&$.close()}),e(t).bind("keydown."+V,function(e){var t=e.keyCode;O&&P.escKey&&27===t&&(e.preventDefault(),$.close()),O&&P.arrowKey&&T[1]&&(37===t?(e.preventDefault(),S.click()):39===t&&(e.preventDefault(),M.click()))}),e.isFunction(e.fn.on)?e(t).on("click."+V,"."+Y,i):e("."+Y).live("click."+V,i)),!0):!1}var p,m,w,g,x,v,y,b,T,C,k,I,W,H,E,L,M,S,F,R,P,_,K,z,D,B,N,A,O,j,q,U,$,G,Q,X={transition:"elastic",speed:300,width:!1,initialWidth:"600",innerWidth:!1,maxWidth:!1,height:!1,initialHeight:"450",innerHeight:!1,maxHeight:!1,scalePhotos:!0,scrolling:!0,inline:!1,html:!1,iframe:!1,fastIframe:!0,photo:!1,href:!1,title:!1,rel:!1,opacity:.9,preloading:!0,className:!1,retinaImage:!1,retinaUrl:!1,retinaSuffix:"@2x.$1",current:"image {current} of {total}",previous:"previous",next:"next",close:"close",xhrError:"This content failed to load.",imgError:"This image failed to load.",open:!1,returnFocus:!0,reposition:!0,loop:!0,slideshow:!1,slideshowAuto:!0,slideshowSpeed:2500,slideshowStart:"start slideshow",slideshowStop:"stop slideshow",photoRegex:/\.(gif|png|jp(e|g|eg)|bmp|ico)((#|\?).*)?$/i,onOpen:!1,onLoad:!1,onComplete:!1,onCleanup:!1,onClosed:!1,overlayClose:!0,escKey:!0,arrowKey:!0,top:!1,bottom:!1,left:!1,right:!1,fixed:!1,data:void 0},J="colorbox",V="cbox",Y=V+"Element",Z=V+"_open",et=V+"_load",tt=V+"_complete",it=V+"_cleanup",ot=V+"_closed",nt=V+"_purge",ht=!e.support.leadingWhitespace,rt=ht&&!i.XMLHttpRequest,lt=V+"_IE6",st=e({}),at="div";e.colorbox||(e(u),$=e.fn[J]=e[J]=function(t,i){var o=this;if(t=t||{},u(),f()){if(e.isFunction(o))o=e("<a/>"),t.open=!0;else if(!o[0])return o;i&&(t.onComplete=i),o.each(function(){e.data(this,J,e.extend({},e.data(this,J)||X,t))}).addClass(Y),(e.isFunction(t.open)&&t.open.call(o)||t.open)&&c(o[0])}return o},$.position=function(e,t){function i(e){x[0].style.width=b[0].style.width=g[0].style.width=parseInt(e.style.width,10)-K+"px",g[0].style.height=v[0].style.height=y[0].style.height=parseInt(e.style.height,10)-_+"px"}var o,n,r,l=0,s=0,a=m.offset();C.unbind("resize."+V),m.css({top:-9e4,left:-9e4}),n=C.scrollTop(),r=C.scrollLeft(),P.fixed&&!rt?(a.top-=n,a.left-=r,m.css({position:"fixed"})):(l=n,s=r,m.css({position:"absolute"})),s+=P.right!==!1?Math.max(C.width()-P.w-D-K-h(P.right,"x"),0):P.left!==!1?h(P.left,"x"):Math.round(Math.max(C.width()-P.w-D-K,0)/2),l+=P.bottom!==!1?Math.max(C.height()-P.h-z-_-h(P.bottom,"y"),0):P.top!==!1?h(P.top,"y"):Math.round(Math.max(C.height()-P.h-z-_,0)/2),m.css({top:a.top,left:a.left,visibility:"visible"}),e=m.width()===P.w+D&&m.height()===P.h+z?0:e||0,w[0].style.width=w[0].style.height="9999px",o={width:P.w+D+K,height:P.h+z+_,top:l,left:s},0===e&&m.css(o),m.dequeue().animate(o,{duration:e,complete:function(){i(this),j=!1,w[0].style.width=P.w+D+K+"px",w[0].style.height=P.h+z+_+"px",P.reposition&&setTimeout(function(){C.bind("resize."+V,$.position)},1),t&&t()},step:function(){i(this)}})},$.resize=function(e){O&&(e=e||{},e.width&&(P.w=h(e.width,"x")-D-K),e.innerWidth&&(P.w=h(e.innerWidth,"x")),k.css({width:P.w}),e.height&&(P.h=h(e.height,"y")-z-_),e.innerHeight&&(P.h=h(e.innerHeight,"y")),e.innerHeight||e.height||(k.css({height:"auto"}),P.h=k.height()),k.css({height:P.h}),$.position("none"===P.transition?0:P.speed))},$.prep=function(t){function i(){return P.w=P.w||k.width(),P.w=P.mw&&P.mw<P.w?P.mw:P.w,P.w}function h(){return P.h=P.h||k.height(),P.h=P.mh&&P.mh<P.h?P.mh:P.h,P.h}if(O){var l,s="none"===P.transition?0:P.speed;k.empty().remove(),k=o(at,"LoadedContent").append(t),k.hide().appendTo(I.show()).css({width:i(),overflow:P.scrolling?"auto":"hidden"}).css({height:h()}).prependTo(g),I.hide(),e(A).css({"float":"none"}),l=function(){function t(){ht&&m[0].style.removeAttribute("filter")}var i,h,l=T.length,d="frameBorder",c="allowTransparency";O&&(h=function(){clearTimeout(U),W.remove(),a(tt,P.onComplete)},ht&&A&&k.fadeIn(100),H.html(P.title).add(k).show(),l>1?("string"==typeof P.current&&E.html(P.current.replace("{current}",N+1).replace("{total}",l)).show(),M[P.loop||l-1>N?"show":"hide"]().html(P.next),S[P.loop||N?"show":"hide"]().html(P.previous),P.slideshow&&L.show(),P.preloading&&e.each([n(-1),n(1)],function(){var t,i,o=T[this],n=e.data(o,J);n&&n.href?(t=n.href,e.isFunction(t)&&(t=t.call(o))):t=e(o).attr("href"),t&&(r(t)||n.photo)&&(i=new Image,i.src=t)})):R.hide(),P.iframe?(i=o("iframe")[0],d in i&&(i[d]=0),c in i&&(i[c]="true"),P.scrolling||(i.scrolling="no"),e(i).attr({src:P.href,name:(new Date).getTime(),"class":V+"Iframe",allowFullScreen:!0,webkitAllowFullScreen:!0,mozallowfullscreen:!0}).one("load",h).appendTo(k),st.one(nt,function(){i.src="//about:blank"}),P.fastIframe&&e(i).trigger("load")):h(),"fade"===P.transition?m.fadeTo(s,1,t):t())},"fade"===P.transition?m.fadeTo(s,0,function(){$.position(0,l)}):$.position(s,l)}},$.load=function(t){var n,d,c,u=$.prep;j=!0,A=!1,B=T[N],t||s(),G&&m.add(p).removeClass(G),P.className&&m.add(p).addClass(P.className),G=P.className,a(nt),a(et,P.onLoad),P.h=P.height?h(P.height,"y")-z-_:P.innerHeight&&h(P.innerHeight,"y"),P.w=P.width?h(P.width,"x")-D-K:P.innerWidth&&h(P.innerWidth,"x"),P.mw=P.w,P.mh=P.h,P.maxWidth&&(P.mw=h(P.maxWidth,"x")-D-K,P.mw=P.w&&P.w<P.mw?P.w:P.mw),P.maxHeight&&(P.mh=h(P.maxHeight,"y")-z-_,P.mh=P.h&&P.h<P.mh?P.h:P.mh),n=P.href,U=setTimeout(function(){W.appendTo(g)},100),P.inline?(c=o(at).hide().insertBefore(e(n)[0]),st.one(nt,function(){c.replaceWith(k.children())}),u(e(n))):P.iframe?u(" "):P.html?u(P.html):r(n)?(n=l(n),e(A=new Image).addClass(V+"Photo").bind("error",function(){P.title=!1,u(o(at,"Error").html(P.imgError))}).one("load",function(){var e;P.retinaImage&&i.devicePixelRatio>1&&(A.height=A.height/i.devicePixelRatio,A.width=A.width/i.devicePixelRatio),P.scalePhotos&&(d=function(){A.height-=A.height*e,A.width-=A.width*e},P.mw&&A.width>P.mw&&(e=(A.width-P.mw)/A.width,d()),P.mh&&A.height>P.mh&&(e=(A.height-P.mh)/A.height,d())),P.h&&(A.style.marginTop=Math.max(P.mh-A.height,0)/2+"px"),T[1]&&(P.loop||T[N+1])&&(A.style.cursor="pointer",A.onclick=function(){$.next()}),ht&&(A.style.msInterpolationMode="bicubic"),setTimeout(function(){u(A)},1)}),setTimeout(function(){A.src=n},1)):n&&I.load(n,P.data,function(t,i){u("error"===i?o(at,"Error").html(P.xhrError):e(this).contents())})},$.next=function(){!j&&T[1]&&(P.loop||T[N+1])&&(N=n(1),$.load())},$.prev=function(){!j&&T[1]&&(P.loop||N)&&(N=n(-1),$.load())},$.close=function(){O&&!q&&(q=!0,O=!1,a(it,P.onCleanup),C.unbind("."+V+" ."+lt),p.fadeTo(200,0),m.stop().fadeTo(300,0,function(){m.add(p).css({opacity:1,cursor:"auto"}).hide(),a(nt),k.empty().remove(),setTimeout(function(){q=!1,a(ot,P.onClosed)},1)}))},$.remove=function(){e([]).add(m).add(p).remove(),m=null,e("."+Y).removeData(J).removeClass(Y),e(t).unbind("click."+V)},$.element=function(){return e(B)},$.settings=X)})(jQuery,document,window);


(function($)
{
    /**
     * Auto-growing textareas; technique ripped from Facebook
     *
     * http://github.com/jaz303/jquery-grab-bag/tree/master/javascripts/jquery.autogrow-textarea.js
     */
    $.fn.autogrow = function(options)
    {
        return this.filter('textarea').each(function()
        {
            var self         = this;
            var $self        = $(self);
            var minHeight    = $self.height();
            var noFlickerPad = $self.hasClass('autogrow-short') ? 0 : parseInt($self.css('lineHeight')) || 0;

            var shadow = $('<div></div>').css({
                position:    'absolute',
                top:         -10000,
                left:        -10000,
                width:       $self.width(),
                fontSize:    $self.css('fontSize'),
                fontFamily:  $self.css('fontFamily'),
                fontWeight:  $self.css('fontWeight'),
                lineHeight:  $self.css('lineHeight'),
                resize:      'none',
                'word-wrap': 'break-word'
            }).appendTo(document.body);

            var update = function(event)
            {
                var times = function(string, number)
                {
                    for (var i=0, r=''; i<number; i++) r += string;
                    return r;
                };

                var val = self.value.replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/&/g, '&amp;')
                    .replace(/\n$/, '<br/>&nbsp;')
                    .replace(/\n/g, '<br/>')
                    .replace(/ {2,}/g, function(space){ return times('&nbsp;', space.length - 1) + ' ' });

                // Did enter get pressed?  Resize in this keydown event so that the flicker doesn't occur.
                if (event && event.data && event.data.event === 'keydown' && event.keyCode === 13) {
                    val += '<br />';
                }

                shadow.css('width', $self.width());
                shadow.html(val + (noFlickerPad === 0 ? '...' : '')); // Append '...' to resize pre-emptively.
                $self.height(Math.max(shadow.height() + noFlickerPad, minHeight));
            }

            $self.change(update).keyup(update).keydown({event:'keydown'},update);
            $(window).resize(update);

            update();
        });
    };
})(jQuery);

if (typeof console == "undefined") var console = { log: function() {} };

jQuery.fn.centerFixed = function()
{
    this.cssLeft(Math.max ( 10 , ($(window).width() - this.outerWidth() ) /2 )) ;
    this.cssTop( Math.min(150 , Math.max ( 20 ,  ($(window).height() - this.outerHeight() ) / 2) ) ) ;
    // + getPageScrollTop()) ;
    return this;
}
function createPopup(title , width , height, with_footer) {
    var popup = $('<div />').addClass('simple-popup').appendTo('body') ;
    popup.css('width', 	width ? width + 'px' : 'auto').css('height', height ? height + 'px' : 'auto');

    $('<div />').addClass('simple-popup-header').html(title).appendTo(popup) ;
    $('<div />').addClass('simple-popup-content').appendTo(popup) ;
    if(with_footer){
        $('<div />').addClass('simple-popup-footer').appendTo(popup) ;
    }
    $('<div />').addClass('simple-popup-close').appendTo(popup).click(function() { $(this).closest('.simple-popup').remove() ; $('.screen-shield').hide() ; }) ;

    return popup.centerFixed() ;
}

jQuery.fn.cssWidth = function(value) {
    if (typeof value == 'undefined') {
        return this[0].style.width.split('px')[0] * 1 ;
    }
    else {
        this[0].style.width = Math.round(value) + 'px' ;
        return this ;
    }
} ;

jQuery.fn.cssHeight = function(value) {
    if (typeof value == 'undefined')
        return this[0].style.height.split('px')[0] * 1 ;
    else {
        this[0].style.height = Math.round(value) + 'px' ;
        return this ;
    }
} ;


jQuery.fn.cssLeft = function(value) {
    if (typeof value == 'undefined')
        return this.position().left * 1 ;
    else {
        this.css('left' , Math.round(value) + 'px' ) ;
        return this ;
    }
} ;

jQuery.fn.cssRight = function(value) {
    if (typeof value == 'undefined')
        return this[0].style.right.split('px')[0] * 1 ;
    else {
        this[0].style.right = Math.round(value) + 'px' ;
        return this ;
    }
} ;

jQuery.fn.cssTop = function(value) {
    if (typeof value == 'undefined')
        return this.position().top * 1 ;
    else {
        $(this).css('top' , Math.round(value) + 'px' ) ;
        return this ;
    }
} ;

jQuery.fn.cssBottom = function(value) {
    if (typeof value == 'undefined')
        return this[0].style.bottom.split('px')[0] * 1 ;
    else {
        this[0].style.bottom = Math.round(value) + 'px' ;
        return this;
    }
} ;



function ie8SafePreventEvent(e){
    if (e &&e.preventDefault) e.preventDefault();
    else if (window.event && window.event.returnValue)
        window.eventReturnValue = false;
}

function safeLog(txt) {
    if ( typeof console != 'undefined' )
        console.log(txt) ;
}


function findPos(obj) {
    var curleft = curtop = 0;
    if (typeof obj.offsetParent!='undefined') {
        do {
            curleft += obj.offsetLeft;
//			curleft -= obj.scrollLeft;
            curtop += obj.offsetTop;
//			curtop -= obj.scrollTop;

        } while (obj = obj.offsetParent);
        return [curleft,curtop];
    }else return [0,0];

}
//pads left
String.prototype.lpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = padString + str;
    return str;
};

//pads right
String.prototype.rpad = function(padString, length) {
    var str = this;
    while (str.length < length)
        str = str + padString;
    return str;
} ;

function getPageScrollTop() {
    if ($(window).scrollTop() != 0)
        XbrowserScrollTop = $(window).scrollTop() ;
    else if ($('html').scrollTop() != 0)
        XbrowserScrollTop = $('html').scrollTop() ;
    else if ($('body').scrollTop() != 0)
        XbrowserScrollTop = $('body').scrollTop() ;
    else
        XbrowserScrollTop = 0 ;

    return 	XbrowserScrollTop ;
}


function getAllFragmentValues(){
    var items = {} ;

    $.each(window.location.hash.split('#').pop().split('&') , function(k,v){
        var pair = v.split('=') ;
        console.log(pair);
        items[pair[0]] = decodeURIComponent(pair[1]);
    });

    return items ;
}

function setFragmentValues(pairs){
    var h = '';

    $.each(pairs , function(k,v){
        console.log(k,v);
        h += k + '=' + v + '&' ;
    });
    h = h.substr(0 , h.length -1);
    window.location.hash = h ;
}

function getFragmentValue(attr) {
    //var temp = window.location.hash.split(attr + '=')[1] ;
    var url = window.location+'';
    var split_str = (url.indexOf('#')>0 && attr=='design_id')?'#design_id=':attr + '=';
    var temp = url.split(split_str)[1] ;
    if (typeof temp == 'undefined'){
        split_str = attr=='design_id'?'design_id=':attr + '=';
        temp = url.split(split_str)[1] ;
        if(typeof temp == 'undefined')
            return false ;
        else{
            alert(temp.split('&')[0]);
            return temp.split('&')[0] ;
        }
    }else{
        return temp.split('&')[0] ;
        //var p1 = temp.indexOf('#');
        //var p2 = temp.indexOf('&');
        /*
        if(p2>0){
            if(p1>0 && p1<p2)
                return temp.split('#')[0] ;
            else
                return temp.split('&')[0] ;
        }else{
            if(p1>0)
                return temp.split('#')[0] ;
            else
                return temp.split('#')[0] ;
        }
        */
        /*
        if(attr=='design_id'){
            if(p1>0)
                return temp.split('#')[0] ;
            else
                return temp.split('&')[0] ;
        }else{
            return temp.split('&')[0] ;
        }
        */
    }
}

function updateUrlFragmentValue(name, value){
    var f = getAllFragmentValues();
    f[name] = value ;
    console.log(f);
    setFragmentValues(f);
}

function deleteUrlFragmentValue(name){
    var f = getAllFragmentValues();
    delete f[name] ;
    setFragmentValues(f);
}

function setFragmentValue(attr, value) {
    window.location.hash = "?" + attr + "=" + value ;
}


function getFoldingName(code){
    if (code == 'none')
        return 'None' ;
    else if (code == 'halfFold')
        return 'Half-Fold' ;
    else if (code == 'zFold')
        return 'Z-Fold';
    else if (code == 'triFold')
        return 'Tri-Fold';
    else if (code == 'letterFold')
        return 'Letter-Fold';
    else if (code == 'rollFold')
        return 'Roll-Fold';
    else if (code == 'accordionFold')
        return 'Accordion Fold';
}


function getDiecutName(code){
    if (code == 'none')
        return 'None' ;
    else if (code == 'halfCircleSide')
        return 'Half Circle Side' ;
    else if (code == 'roundedCorners')
        return 'Rounded Corners';
    else if (code == 'leaf')
        return 'Leaf';
    else if (code == 'roundedSingleCorner')
        return 'Rounded Single Corner';
    else if (code == 'circle')
        return 'Circle';
}

function eventTrack(category, action, opt_label, opt_value){
//	safeLog(autoclick) ;
    if(typeof autoclick != 'undefined' && autoclick === false){
        if (typeof(opt_value)!='undefined')
            _gaq.push(['_trackEvent', category, action, opt_label, opt_value]);
        else if (typeof(opt_label)!='undefined')
            _gaq.push(['_trackEvent', category, action, opt_label]);
        else if (typeof(action)!='undefined')
            _gaq.push(['_trackEvent', category, action]);
        else
            _gaq.push(['_trackEvent', category]);
    }
    autoclick = false;

}
function keyEnter(e){
    var key = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
    if (key==13) {
        return true;
    } else {
        return false;
    }
};

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function openLiveChat(){
    window.open('https://server.iad.liveperson.net/hc/77577663/?cmd=file&file=visitorWantsToChat&site=77577663&SESSIONVAR!skill=YouPrint&imageUrl=https://server.iad.liveperson.net/hcp/Gallery/ChatButton-Gallery/English/General/1a/&referrer='+escape(document.location),'chat77577663','width=475,height=400,resizable=yes');
}
function seePickupMap(city) {
    switch (city) {
        case 'LA' : window.open("http://maps.google.com/maps?q=10932+Santa+Monica+Blvd+Los+Angeles,+CA+90025+%E2%80%8E&hl=en&ie=UTF8&ll=34.048713,-118.439183&spn=0.01289,0.022531&sll=34.048923,-118.439043&sspn=0.006445,0.011265&hnear=10932+Santa+Monica+Blvd,+Los+Angeles,+California+90025&t=m&z=16", "_blank"); break;
        case 'VAN NUYS' : window.open("http://maps.google.com/maps?q=8000+Haskell+Ave+Los+Angeles,+CA+91406+%E2%80%8E&hl=en&ie=UTF8&ll=34.216824,-118.47564&spn=0.012864,0.022531&sll=34.218101,-118.479073&sspn=0.012864,0.022531&hnear=8000+Haskell+Ave,+Los+Angeles,+California+91406&t=m&z=16", "_blank");break;
    }
}

function getEmailAddress(str)
{
    var email = ''
    var emailsArray = str.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
    if (emailsArray) {
        email = emailsArray[0];
    }
    return email;
}

function getPhone(str)
{
    str = str.replace(/^[0-9]{1}[-.]/, "");
    var phone = str.match(/[0-9]+/g);

    if (phone) {
        if (phone.join('').length >= 10) {
            var pnum = phone.join('').replace(/^(.{10}).*/, "$1").match(/(.{1,3})/g);
            return '(' + pnum[0] + ') ' + pnum[1] + '-' + pnum[2] + pnum[3];
        }
    }
    return '';
}

function getWordCount(str)
{
    str = cleanString(str);
    return str.split(' ').length;
}

function cleanString(str)
{
    str = str.replace(/(^\s*)|(\s*$)/gi,"");
    str = str.replace(/[ ]{2,}/gi," ");
    str = str.replace(/\n /,"\n");
    return str;
}

function cacheTemplateSet(template_id,theme_id,set_id){
    return;
    $.ajax({
        type: 'post' ,
        url: '/ajax/raster/cacheTemplateSet.php?nocache='+((new Date).getTime()) ,
        data: {"template_id" : template_id, "theme_id" : theme_id, "set_id" : set_id},
        type: 'POST',
        success: function(data){
            console.log(data)
        } ,
        error: function(data){
            console.log('Could not cache template');
        }
    });

}

/*
 * Selecter Plugin [Formstone Library]
 * @author Ben Plum
 * @version 2.1.5
 *
 * Copyright © 2013 Ben Plum <mr@benplum.com>
 * Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */

if(jQuery)(function(f){function u(a){a=f.extend({},v,a||{});for(var b=f(this),e=0,d=b.length;e<d;e++){var h=b.eq(e),g=a;if(!h.data("selecter")){g.externalLinks&&(g.links=!0);f.extend(g,h.data("selecter-options"));var k=h.find("option, optgroup"),n=k.filter("option"),l=n.filter(":selected"),s=g.defaultLabel?-1:n.index(l),t=k.length-1,w=g.links?"nav":"div",x=g.links?"a":"span";g.multiple=h.prop("multiple");g.disabled=h.is(":disabled");var c="<"+w+' class="selecter '+g.customClass;m?c+=" mobile":g.cover&&(c+= " cover");c=g.multiple?c+" multiple":c+" closed";g.disabled&&(c+=" disabled");c+='">';g.multiple||(c+='<span class="selecter-selected">',c+=y(g.trimOptions,!1!=g.defaultLabel?g.defaultLabel:l.text()),c+="</span>");for(var c=c+'<div class="selecter-options">',l=0,p=null,q=0,u=k.length;q<u;q++)p=f(k[q]),"OPTGROUP"==p[0].tagName?c+='<span class="selecter-group">'+p.attr("label")+"</span>":(c+="<"+x+' class="selecter-item',p.is(":selected")&&!g.defaultLabel&&(c+=" selected"),0==q&&(c+=" first"),q==t&& (c+=" last"),c+='" ',c=g.links?c+('href="'+p.val()+'"'):c+('data-value="'+p.val()+'"'),c+=">"+y(g.trimOptions,p.text())+"</"+x+">",l++);c+="</div>";c+="</"+w+">";h.addClass("selecter-element").after(c);k=h.next(".selecter");g=f.extend({$selectEl:h,$optionEls:n,$selecter:k,$selected:k.find(".selecter-selected"),$itemsWrapper:k.find(".selecter-options"),$items:k.find(".selecter-item"),index:s,guid:z},g);void 0!=f.fn.scroller&&g.$itemsWrapper.scroller();k.on("click.selecter",".selecter-selected",g,A).on("click.selecter", ".selecter-item",g,B).on("selecter-close",g,r).data("selecter",g);if(!g.links&&!m||m){if(h.on("change",g,C).on("blur.selecter",g,D),!m)h.on("focus.selecter",g,E)}else h.hide();z++}}return b}function A(a){a.preventDefault();a.stopPropagation();var b=a.data;if(!b.$selectEl.is(":disabled"))if(f(".selecter").not(b.$selecter).trigger("selecter-close",[b]),m)a=b.$selectEl[0],document.createEvent?(b=document.createEvent("MouseEvents"),b.initMouseEvent("mousedown",!0,!0,window,0,0,0,0,0,!1,!1,!1,!1,0,null), a.dispatchEvent(b)):element.fireEvent&&a.fireEvent("onmousedown");else if(b.$selecter.hasClass("closed")){if(a.preventDefault(),a.stopPropagation(),a=a.data,!a.$selecter.hasClass("open")){var b=a.$selecter.offset(),e=f("body").outerHeight(),d=a.$itemsWrapper.outerHeight(!0);b.top+d>e&&m?a.$selecter.addClass("bottom"):a.$selecter.removeClass("bottom");a.$itemsWrapper.show();a.$selecter.removeClass("closed").addClass("open");f(document).on("click.selecter-"+a.guid,":not(.selecter-options)",a,F);void 0!= f.fn.scroller&&a.$itemsWrapper.scroller("reset")}}else b.$selecter.hasClass("open")&&r(a)}function r(a){a.preventDefault();a.stopPropagation();a=a.data;a.$selecter.hasClass("open")&&(a.$itemsWrapper.hide(),a.$selecter.removeClass("open").addClass("closed"),f(document).off(".selecter-"+a.guid))}function F(a){a.preventDefault();a.stopPropagation();0==f(a.currentTarget).parents(".selecter").length&&r(a)}function B(a){a.preventDefault();a.stopPropagation();var b=f(this),e=a.data;e.$selectEl.is(":disabled")|| (e.$itemsWrapper.is(":visible")&&(b=e.$items.index(b),n(b,e,!1)),e.multiple||r(a))}function C(a,b){if(!b){var e=f(this),d=a.data;d.links?m?l(e.val(),d.externalLinks):l(e.attr("href"),d.externalLinks):(e=d.$optionEls.index(d.$optionEls.filter("[value='"+G(e.val())+"']")),n(e,d,!1))}}function E(a){a.preventDefault();a.stopPropagation();a=a.data;a.$selectEl.is(":disabled")||a.multiple||(a.$selecter.addClass("focus"),f(".selecter").not(a.$selecter).trigger("selecter-close",[a]),f("body").on("keydown.selecter-"+ a.guid,a,H))}function D(a){a.preventDefault();a.stopPropagation();a=a.data;a.$selecter.removeClass("focus");f(".selecter").not(a.$selecter).trigger("selecter-close",[a]);f(document).off(".selecter-"+a.guid)}function H(a){var b=a.data;if(b.$selecter.hasClass("open")&&13==a.keyCode)n(b.index,b,!1),r(a);else if(9!=a.keyCode&&!(a.metaKey||a.altKey||a.ctrlKey||a.shiftKey)){a.preventDefault();a.stopPropagation();var e=b.$items.length-1,d=-1;if(-1<f.inArray(a.keyCode,s?[38,40,37,39]:[38,40]))d=b.index+(38== a.keyCode||s&&37==a.keyCode?-1:1),0>d&&(d=0),d>e&&(d=e);else{a=String.fromCharCode(a.keyCode).toUpperCase();for(i=b.index+1;i<=e;i++){var h=b.$optionEls.eq(i).text().charAt(0).toUpperCase();if(h==a){d=i;break}}if(0>d)for(i=0;i<=e;i++)if(h=b.$optionEls.eq(i).text().charAt(0).toUpperCase(),h==a){d=i;break}}0<=d&&n(d,b,!0)}}function n(a,b,e){var d=b.$items.eq(a),f=d.hasClass("selected");console.log(a);if(f)b.multiple&&(b.$optionEls.eq(a).prop("selected",null),d.removeClass("selected"));else{var g=d.html(); d.data("value");if(b.multiple)b.$optionEls.eq(a).prop("selected",!0);else if(b.$selected.html(g),b.$items.filter(".selected").removeClass("selected"),e||(b.$selectEl[0].selectedIndex=a),b.links&&!e){m?l(b.$selectEl.val(),b.externalLinks):l(d.attr("href"),b.externalLinks);return}b.$selectEl.trigger("change",[!0]);d.addClass("selected")}if(!f||b.multiple)b.callback.call(b.$selecter,b.$selectEl.val(),a),b.index=a}function y(a,b){return!1===a?b:b.length>a?b.substring(0,a)+"...":b}function l(a,b){b?window.open(a): window.location.href=a}function G(a){return a.replace(/([;&,\.\+\*\~':"\!\^#$%@\[\]\(\)=>\|])/g,"\\$1")}var s=-1<navigator.userAgent.toLowerCase().indexOf("firefox"),m=/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent||navigator.vendor||window.opera),v={callback:function(){},cover:!1,customClass:"",defaultLabel:!1,externalLinks:!1,links:!1,trimOptions:!1},z=0,t={defaults:function(a){v=f.extend(v,a||{});return f(this)},disable:function(){return f(this).each(function(a,b){var e= f(b),d=e.next(".selecter");d.hasClass("open")&&d.find(".selecter-selected").trigger("click");e.prop("disabled",!0);d.addClass("disabled")})},enable:function(){return f(this).each(function(a,b){var e=f(b),d=e.next(".selecter");e.prop("disabled",null);d.removeClass("disabled")})},destroy:function(){return f(this).each(function(a,b){var e=f(b),d=e.next(".selecter");d.hasClass("open")&&d.find(".selecter-selected").trigger("click");void 0!=f.fn.scroller&&d.find(".selecter-options").scroller("destroy"); e.off(".selecter").removeClass("selecter-element").show();d.off(".selecter").remove()})}};f.fn.selecter=function(a){return t[a]?t[a].apply(this,Array.prototype.slice.call(arguments,1)):"object"!==typeof a&&a?this:u.apply(this,arguments)}})(jQuery);