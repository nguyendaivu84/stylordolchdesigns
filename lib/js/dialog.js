var popupResult = "Cancel";
var popupResultValue = "0";
var popupButtonType = "View";
var popupWindow;
var popupCallback;

function popupClose(BtnResult,value,buttontType){
    popupResult = BtnResult;
    popupResultValue = value;
    popupButtonType = buttontType;
    popupWindow.close();
};

function popuploseCallBack(e){
    popupWindow.unbind("close", popuploseCallBack);
    if (popupCallback !== null){
        popupCallback(popupResult,popupResultValue,popupButtonType);
    }
}

function popupOpen(Title,Message,Type,Buttons,theFunction,value,buttontype){
    var popupData = '<table cellpadding="0" cellspacing="0"><tr><td><div class="table_parent ' + Type + '"></div></td>'+
        '<td><div class="td_class">'+Message+'</div></td></tr></table><div id="pop_up">';
    for(var i in Buttons){
        var s = Buttons[i];
        popupData += '<input class="class_input" data-contact-id="'+value+'" type="button" onclick="popupClose(\''+s+'\' ,\''+value+'\' , \''+buttontype+'\')" value="'+s+'">';
    }
    popupData += '</div>';
    popupResult = "Cancel";
    if (theFunction !== undefined){
        popupCallback = theFunction;
    } else {
        popupCallback = null;
    }
    popupWindow.bind("close", popuploseCallBack);
    popupWindow.title(Title);
    popupWindow.center();
    popupWindow.content(popupData);
    popupWindow.open();
}
//$('<div id="popupWindow"></div>').appendTo('body');
$(document).ready(function(){
    popupWindow = $("#popupWindow").kendoWindow({
        actions: ["Close"],
        draggable: true,
        modal: true,
        resizable: false,
        visible: false,
        title: "Confirm"
    }).data("kendoWindow");
});
