<?php
if(!isset($v_sval)) die();

$v_tmp_dir = DESIGN_TEMP_DIR.DS;
$v_file = isset($_GET['file'])?$_GET['file']:'';

if($v_file!=''){
    if (substr($v_file, 0, 1) == '.' || strpos($v_file, '..') > 0 || substr($v_file, 0, 1) == '/' || strpos($v_file, '/') > 0){
        echo("Hack attempt detected!");
        die();
    }else{
        $type = "application/force-download";
        $v_download_file = $v_tmp_dir.$v_file;
        if(file_exists($v_download_file)){
            $v_extension = strtolower(substr(strrchr($v_file, "."), 1));
            switch($v_extension){
                case "asf":     $type = "video/x-ms-asf";                break;
                case "avi":     $type = "video/x-msvideo";               break;
                case "exe":     $type = "application/octet-stream";      break;
                case "mov":     $type = "video/quicktime";               break;
                case "mp3":     $type = "audio/mp3";                    break;
                case "mpg":     $type = "video/mpeg";                    break;
                case "flv":     $type = "video/flv";                    break;
                case "mpeg":    $type = "video/mpeg";                    break;
                case "rar":     $type = "encoding/x-compress";           break;
                case "txt":     $type = "text/plain";                    break;
                case "wav":     $type = "audio/wav";                     break;
                case "wma":     $type = "audio/x-ms-wma";                break;
                case "wmv":     $type = "video/x-ms-wmv";                break;
                case "zip":     $type = "application/x-zip-compressed";  break;
                default:        $type = "application/force-download";    break;
            }
            $v_header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $v_file, substr_count($v_file, '.') - 1) : $v_file;
            //header("Pragma: public");
            //header("Expires: 0");
            //header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            //header("Cache-Control: public", false);
            //header("Content-Description: File Transfer");
            header("Content-Type: " . $type);
            //header("Accept-Ranges: bytes");
            header("Content-Disposition: attachment; filename=\"" . $v_header_file . "\";");
            //header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($v_download_file));
            //Cap nhat download

            $stream = @fopen($v_download_file,"r");
            if ($stream) {
                while(!feof($stream)) {
                    print(fread($stream, 1024*4));
                    flush();
                    if (connection_status()!=0) {
                        @fclose($stream);
                        die();
                    }
                }
                @fclose($stream);
            }

        }else{
            die('Resource is not found');
        }
    }

}else{
    die('Unknown request');
}