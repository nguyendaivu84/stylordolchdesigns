<?php
if(!isset($v_sval)) die();
//die('SITE: '.$v_site_id);
add_class('cls_tb_color');
$cls_color = new cls_tb_color($db, LOG_DIR);
$arr_where = array('color_status'=>0);
$arr_color = $cls_color->select($arr_where, array('color_order'=>1));
$arr_colors = array();
$arr_colors[] = 'transparent';

foreach($arr_color as $arr){
    $arr_colors[] = str_replace('#','',$arr['color_code_hex']);
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_colors;

$cls_output->output($arr_return, true, false);