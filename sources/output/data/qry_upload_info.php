<?php
if(!isset($v_sval)) die();

$arr_data = array();
$arr_accept_ext = array('jpeg','png','gif','pdf');

$arr_data['max_size_upload'] = (DESIGN_MAX_SIZE_UPLOAD / (1024*1024)) . ' MB';
$arr_data['accept_extension'] = strtoupper(implode(',', $arr_accept_ext));

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);