<?php
if(!isset($v_sval)) die();

$v_data = isset($_GET['data'])?$_GET['data']:'';
if(isset($v_action) && $v_action=='DL') $v_data = 'DOWNLOAD';

switch($v_data){
    case 'UPLOAD':
        include 'qry_upload_info.php';
        break;
    case 'DOWNLOAD':
        include 'qry_download_file.php';
        break;
    case 'CSS':
        include 'qry_get_font_css.php';
        break;
    case 'COLOR':
        include 'qry_get_colors.php';
        break;
    default:
        $cls_output->output($arr_return, true, false);
        break;
}