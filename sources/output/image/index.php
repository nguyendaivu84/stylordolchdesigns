<?php
if(!isset($v_sval)) die();

add_class('cls_tb_design_image');
$cls_images = new cls_tb_design_image($db, LOG_DIR);
$v_image = isset($_GET['image'])?$_GET['image']:'';


if($v_site_id>0){
    $arr_switch = array(
        'INFO'=>'Get Image\'s Info'
        ,'UPLOAD'=>'Upload an image'
        ,'L_UPLOAD'=>'Multi-upload images'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot()
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_design])?$arr_switch[$v_design]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_image){
    case 'INFO':
        include 'qry_image_info.php';
        break;
    case 'UPLOAD':
        include 'qry_image_upload.php';
        break;
    case 'L_UPLOAD':
        include 'qry_image_list_upload.php';
        break;
    default:
        $cls_output->output($arr_return);
        break;
}