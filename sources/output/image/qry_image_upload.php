<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template'])?$_POST['template']:0;
$v_design_id = isset($_POST['design'])?$_POST['design']:0;
$v_theme_id = isset($_POST['theme'])?$_POST['theme']:0;
$v_url = isset($_POST['url'])?$_POST['url']:'';
$arr_image_user = isset($_POST['user'])?$_POST['user']:array();
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');

if(!is_array($arr_image_user)) $arr_image_user = array();
$arr_data = array();
$v_data = '';
$v_success = 1;
$v_message = '';

add_class('cls_draw');
$cls_draw = new cls_draw();

$arr_ext = array('', 'gif', 'jpg', 'png', 'pdf');

if($v_url!=''){
    if(filter_var($v_url, FILTER_VALIDATE_URL)){
        add_class('cls_file');
        $mf = new ManageFile(DESIGN_TEMP_DIR, DS);
        $v_upload_dir = DESIGN_USER_DIR;
        if($v_design_id>0){
            if(file_exists($v_upload_dir.DS.$v_design_id) || @mkdir($v_upload_dir.DS.$v_design_id)){
                if(is_dir($v_upload_dir.DS.$v_design_id) && is_writable($v_upload_dir.DS.$v_design_id))
                    $v_upload_dir .= DS.$v_design_id;
            }
        }

        $v_name = basename($v_url);
        if($v_name!=''){
            list($name, $ext) =  explode('.', $v_name);
            $v_content = file_get_contents($v_url);
            $ext = strtolower($ext);
            if($v_content){
                $v_count = 0;
                $v_new_full_path = $v_upload_dir . DS . $name . '.'. $ext;
                $v_new_image_file = $name . '.'. $ext;
                $v_is_exist = file_exists($v_new_full_path);
                while($v_is_exist){
                    $v_count++;
                    $v_new_full_path = $v_upload_dir . DS . $name . '('.$v_count.')'. $ext;
                    $v_new_image_file = $name . '('.$v_count.')'. $ext;
                    $v_is_exist = file_exists($v_new_full_path);
                }
                //$v_image_original_file = $v_new_image_file;
                if(file_put_contents($v_new_full_path, $v_content)){
                    list($width, $height, $type) = getimagesize($v_new_full_path);
                    $v_image_key = seo_friendly_url($v_name);
                    $v_image_key = str_replace('-','_', $v_image_key);

                    $v_image_key = str_replace('img','pic', $v_image_key);

                    $i = 0;
                    $v_tmp_image_key = $v_image_key;
                    do{
                        $arr_tmp_where = array('image_key'=>$v_tmp_image_key/*, 'image_id'=>array('$ne'=>$v_image_id)*/);
                        $v_tmp_row = $cls_images->select_one($arr_tmp_where);
                        if($v_tmp_row>0){
                            $i++;
                            $v_tmp_image_key = $v_image_key.'_'.$i;
                        }
                    }while($v_tmp_row>0);
                    $v_image_key = $v_tmp_image_key;

                    $v_image_original_file = $cls_images->backup_original_image($v_new_full_path, $v_new_image_file, $arr_ext[$type], IMAGE_MAX_SIZE_USE, array('w'=>IMAGE_MAX_WIDTH_USE, 'h'=>IMAGE_MAX_HEIGHT_USE), IMAGE_ORIGINAL_PREFIX, DS);

                    $v_saved_dir = str_replace(ROOT_DIR . DS,'', $v_upload_dir);
                    $v_saved_dir = str_replace(DS,'/', $v_saved_dir) . '/';
                    $v_size = strlen($v_content);
                    $cls_images->set_image_name($v_name);
                    $cls_images->set_image_file($v_new_image_file);
                    $cls_images->set_image_original_file($v_image_original_file);
                    $cls_images->set_image_key($v_image_key);
                    $cls_images->set_image_type($type);
                    $cls_images->set_image_dpi($v_dpi);
                    $cls_images->set_image_extension($arr_ext[$type]);
                    $cls_images->set_image_height($height);
                    $cls_images->set_image_width($width);
                    $cls_images->set_image_cost(0);
                    $cls_images->set_image_size($v_size);
                    $cls_images->set_image_status(0);
                    $cls_images->set_is_public(0);
                    $cls_images->set_image_stock_id(0);
                    $cls_images->set_design_id($v_design_id);
                    $cls_images->set_fotolia_id('');
                    $cls_images->set_fotolia_size('');
                    $cls_images->set_template_id($v_template_id);
                    $cls_images->set_theme_id($v_theme_id);
                    $cls_images->set_saved_dir($v_saved_dir);
                    $cls_images->set_created_time(date('Y-m-d H:i:s'), false);
                    $cls_images->set_user_id(isset($arr_image_user['user_id'])?$arr_image_user['user_id']:0);
                    $cls_images->set_is_admin(0);
                    $cls_images->set_user_ip(isset($arr_image_user['user_ip'])?$arr_image_user['user_ip']:'');
                    $cls_images->set_user_name(isset($arr_image_user['user_name'])?$arr_image_user['user_name']:'');
                    $cls_images->set_user_agent(isset($arr_image_user['user_agent'])?$arr_image_user['user_agent']:'');
                    $cls_images->set_company_id(isset($arr_image_user['company_id'])?$arr_image_user['company_id']:0);
                    $cls_images->set_site_id($v_site_id);
                    $cls_images->set_location_id(isset($arr_image_user['location_id'])?$arr_image_user['location_id']:0);
                    $v_image_id = $cls_images->insert();
                    $v_result = $v_image_id > 0;
                    if($v_result){
                        $v_image_thumb = $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
                        if($v_image_thumb==''){
                            $v_image_thumb = URL . $v_saved_dir . $v_new_image_file;
                        }
                        $arr_data[$v_image_id] = array(
                            'name'=>$cls_images->get_image_name()
                            ,'key'=>$cls_images->get_image_key()
                            ,'ext'=>$cls_images->get_image_extension()
                            ,'width'=>$cls_images->get_image_width()
                            ,'height'=>$cls_images->get_image_height()
                            ,'url'=>$v_image_thumb
                        );
                    }
                }else{
                    $v_message = 'Cannot upload.';
                }
            }else{
                $v_message = 'The file could not be get.';
            }
        }else{
            $v_message = 'The file is invalid.';
        }
    }else{
        $v_message = 'The URL is invalid.';
    }
}else{
    $v_message = 'The URL is empty.';
}

$arr_return = array(
    'success'=>$v_success
    ,'message'=>$v_message
    ,'data'=>$arr_data
);

$cls_output->output($arr_return);