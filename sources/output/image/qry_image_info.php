<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template'])?$_POST['template']:0;
$v_design_id = isset($_POST['design'])?$_POST['design']:0;
settype($v_template_id, 'int');
settype($v_design_id, 'int');

$arr_data = array();
$v_data = '';
$v_success = 1;
$v_message = '';
$v_dpi = 0;
$v_missing_path = DESIGN_DIR. DS. 'missing.png';
$v_missing_url = str_replace(ROOT_DIR.DS, '', DESIGN_DESIGN_DIR);
$v_missing_url = str_replace(DS, '/', $v_missing_url);
$v_missing_url = URL . $v_missing_url . '/missing.png';
add_class('cls_draw');
$cls_draw = new cls_draw();
if($v_design_id>0){
    add_class('cls_tb_design_design');
    $cls_design = new cls_tb_design_design($db, LOG_DIR);
    $v_row = $cls_design->select_one(array('design_id'=>$v_design_id));
    $v_dpi = $cls_design->get_design_dpi();
    if($v_row==1) $v_data = $cls_design->get_design_data();
}else if($v_template_id>0){
    add_class('cls_tb_design_template');
    $cls_template = new cls_tb_design_template($db, LOG_DIR);
    $v_row = $cls_template->select_one(array('template_id'=>$v_template_id));
    $v_dpi = $cls_template->get_template_dpi();
    if($v_row==1) $v_data = $cls_template->get_template_data();
}

if($v_data!=''){
    $arr_json = json_decode($v_data, true);
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    if(is_array($arr_canvases)){

        $v_size = sizeof($arr_canvases);
        for($j=0; $j< $v_size; $j++){
            $arr_images = isset($arr_canvases[$j]['images'])?$arr_canvases[$j]['images']:array();
            if(is_array($arr_images)){
                $v_image_size = sizeof($arr_images);
                for($i=0; $i < $v_image_size; $i++){
                    $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
                    settype($v_image_id, 'int');
                    if($v_image_id > 0){
                        $v_image_name = isset($arr_images[$i]['name'])?$arr_images[$i]['name']:'';
                        $v_width = isset($arr_images[$i]['width'])?$arr_images[$i]['width']:0;
                        $v_height = isset($arr_images[$i]['height'])?$arr_images[$i]['height']:0;
                        $v_rotation = isset($arr_images[$i]['rotation'])?$arr_images[$i]['rotation']:0;
                        $v_left = isset($arr_images[$i]['left'])?$arr_images[$i]['left']:0;
                        $v_top = isset($arr_images[$i]['top'])?$arr_images[$i]['top']:0;
                        $v_crop = isset($arr_images[$i]['crop'])?$arr_images[$i]['crop']:false;
                        $v_flip = isset($arr_images[$i]['flip'])?$arr_images[$i]['flip']:'none';
                        $v_filter = isset($arr_images[$i]['filter'])?$arr_images[$i]['filter']:'undefined';
                        $v_noprint = isset($arr_images[$i]['noprint']);
                        if($v_noprint) $v_noprint = $arr_images[$i]['noprint'];
                        $v_noselect = isset($arr_images[$i]['noselect']);
                        if($v_noselect) $v_noselect = $arr_images[$i]['noselect'];

                        $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
                        $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
                        $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
                        $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;
                        $v_border_width = isset($arr_images[$i]['border_width'])?$arr_images[$i]['border_width']:0;
                        $v_border_color = isset($arr_images[$i]['border_color'])?$arr_images[$i]['border_color']:'transparent';
                        $v_svg_id = isset($arr_images[$i]['svg_id'])?intval($arr_images[$i]['svg_id']):0;
                        $v_svg_colors = isset($arr_images[$i]['svg_colors'])?$arr_images[$i]['svg_colors']:array();
                        if(!is_array($v_svg_colors)){
                            if($v_svg_colors!='') $arr_svg_colors = json_decode($v_svg_colors, true);
                        }else $arr_svg_colors = $v_svg_colors;
                        if(!isset($arr_svg_colors) || !is_array($arr_svg_colors)) $arr_svg_colors = array();

                        if($v_filter.''=='undefined') $v_filter = 'none';
                        if(strlen($v_border_color)<6)
                            $v_border_color = 'transparent';
                        else{
                            $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
                        }
                        $v_border_width = ceil($v_border_width);
                        settype($v_border_width, 'float');
                        settype($v_cropLeft, 'float');
                        settype($v_cropTop, 'float');
                        settype($v_cropWidth, 'float');
                        settype($v_cropHeight, 'float');

                        $v_top *= $v_dpi;
                        $v_left *= $v_dpi;
                        $v_width *= $v_dpi;
                        $v_height *= $v_dpi;
                        $v_width = ceil($v_width);
                        $v_height = ceil($v_height);
                        $v_top = ceil($v_top);
                        $v_left = ceil($v_left);

                        $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
                        $v_image_url = '';

                        if($v_row == 1){
                            $v_image_file = $cls_images->get_image_file();
                            $v_saved_dir = $cls_images->get_saved_dir();
                            $v_image_extension = $cls_images->get_image_extension();
                            $v_image_thumb_file = DESIGN_IMAGE_THUMB_SIZE . '_' . $v_image_file;
                            if(!file_exists($v_saved_dir . $v_image_thumb_file)){
                                if(file_exists($v_saved_dir . $v_image_file)){
                                    $v_thumb_url = $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
                                    if($v_thumb_url=='') $v_thumb_url = $v_missing_url;
                                }else{
                                    $v_image_url = $v_missing_url;
                                }
                            }else{
                                $v_image_url = URL . $v_saved_dir . $v_image_thumb_file;
                            }
                            if(!$v_noselect && !$v_noprint){
                                $arr_data[$v_image_id] = array(
                                    'name'=>$v_image_name
                                    ,'width'=>$v_width
                                    ,'height'=>$v_height
                                    ,'url'=>$v_image_url
                                    ,'extension'=>$v_image_extension
                                );
                            }
                        }
                    }
                }
            }
        }
    }
}

$arr_return = array(
    'success'=>$v_success
    ,'message'=>$v_message
    ,'data'=>$arr_data
);

$cls_output->output($arr_return);