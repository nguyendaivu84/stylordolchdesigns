<?php
if(!isset($v_sval)) die();

$v_user_name = isset($_POST['user_name'])?$_POST['user_name']:'';
$v_password = isset($_POST['password'])?$_POST['password']:'';
$v_key = isset($_REQUEST['license_key'])?$_REQUEST['license_key']:'';

$v_success = 0;
$v_message = '';
$arr_user = array();
$arr_return = array('success'=>0, 'message'=>'', 'data'=>array());
if($v_site_id>0){
    if($v_user_name!=''){
        $arr_where = array('user_name'=>$v_user_name);
        $v_row = $cls_tb_user->select_one($arr_where);
        if($v_row==1){
            $arr_user_site = $cls_tb_user->get_site();
            if(in_array($v_site_id, $arr_user_site)){
                $v_user_password = $cls_tb_user->get_user_password();
                if($v_user_password==md5($v_password)){
                    $v_user_locked = $cls_tb_user->get_user_status();
                    if($v_user_locked==0){
                        $v_success = 1;
                        $v_message = 'OK';
                        $v_contact_id = $cls_tb_user->get_contact_id();
                        $v_contact_row = $cls_tb_contact->select_one(array('contact_id'=>$v_contact_id));
                        $v_email = '';
                        $v_full_name = '';
                        if($v_contact_id==1){
                            $v_full_name = $cls_tb_contact->get_full_name();
                            $v_email = $cls_tb_contact->get_email();
                        }
                        $arr_user = array(
                            'user_name'=>$v_user_name
                            ,'full_name'=>$v_full_name
                            ,'email'=>$v_email
                        );
                    }else{
                        $v_message = 'This account has been locked!';
                    }
                }else{
                    $v_message = 'Invalid password!';
                }
            }else{
                $v_message = 'This user was not assigned to site: '.$arr_check_key['site_name'];
            }
        }else{
            $v_message = 'Invalid username';
        }
    }else{
        $v_message = 'Lost username';
    }
}else{
    $v_message = 'Lost license key';
}

$arr_data = array('success'=>$v_success, 'message'=>$v_message, 'user'=>$arr_user);
$arr_return['success'] = $v_success;
$arr_return['message'] = $v_message;
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);