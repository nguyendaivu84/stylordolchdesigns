<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ANVYINC
 * Date: 9/25/13
 * Time: 5:04 PM
 * To change this template use File | Settings | File Templates.
 */
if(!isset($v_sval)) die();

add_class('cls_tb_user');
add_class('cls_tb_contact');
$cls_tb_user = new cls_tb_user($db, LOG_DIR);
$cls_tb_contact = new cls_tb_contact($db, LOG_DIR);

$v_permission = isset($_GET['permission'])?$_GET['permission']:'';

if($v_site_id>0){
    $arr_switch = array(
        'LOGIN'=>'Try login'
        ,'FEATURE'=>'Get site\'s Features'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot()
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_permission])?$arr_switch[$v_permission]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_permission){
    case 'LOGIN':
        include 'qry_permission_login.php';
        break;
    case 'FEATURE':
        include 'qry_permission_feature.php';
        break;
    case 'SITE':
        include 'qry_permission_site.php';
        break;
    default:
        $cls_output->output($arr_return);
        break;
}