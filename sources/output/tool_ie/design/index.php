<?php
if(!isset($v_sval)) die();

$v_design = isset($_GET['design'])?$_GET['design']:'';

switch($v_design){
    case 'GET':
        include 'get/index.php';
        break;
    case 'PREVIEW':
        include 'preview/index.php';
        break;
    case 'SAVE':
        include 'save/index.php';
        break;
    case 'RE-GET':
        include 'reload/index.php';
        break;
    default:
        break;
}