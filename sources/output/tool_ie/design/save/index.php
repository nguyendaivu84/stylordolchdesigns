<?php
if(!isset($v_sval)) die();

$v_json = isset($_REQUEST['json'])?$_REQUEST['json']:'';
$v_dpi = isset($_REQUEST['dpi'])?$_REQUEST['dpi']:'0';
$v_design_id = isset($_REQUEST['design_id'])?$_REQUEST['design_id']:'0';
$v_template_id = isset($_REQUEST['template_id'])?$_REQUEST['template_id']:'0';
$v_theme_id = isset($_REQUEST['theme_id'])?$_REQUEST['theme_id']:'0';
$v_design_name = isset($_REQUEST['name'])?$_REQUEST['name']:'';

$arr_design_product = isset($_POST['product'])?$_POST['product']:array();
$arr_design_user = isset($_POST['user'])?$_POST['user']:array();
if(!is_array($arr_design_product)) $arr_design_product = array();

$arr_product = array(
    'id'=>isset($arr_design_product['id'])?$arr_design_product['id']:0,
    'code'=>isset($arr_design_product['code'])?$arr_design_product['code']:'blankDesign',
    'title'=>isset($arr_design_product['title'])?$arr_design_product['title']:'Blank Design'
);

$v_product_id = $arr_product['id'];


$v_design_user_id = isset($arr_design_user['id'])?$arr_design_user['id']:0;
$v_design_user_name = isset($arr_design_user['name'])?$arr_design_user['name']:'';
$v_design_user_company = isset($arr_design_user['company'])?$arr_design_user['company']:'0';
$v_design_user_location = isset($arr_design_user['company'])?$arr_design_user['company']:'0';
$v_design_user_ip = isset($arr_design_user['ip'])?$arr_design_user['ip']:'';

$arr_device = isset($arr_design_user['device'])?$arr_design_user['device']:array();
if(!is_array($arr_device)) $arr_device = array();
$v_design_user_agent = isset($arr_device['agent'])?$arr_device['agent']:'';

settype($v_dpi, 'int');
settype($v_design_id, 'int');
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_product_id, 'int');

$v_save_as = $v_design_name!='';

if($v_save_as){
    $v_old_design_id = $v_design_id;
    include 'qry_save_as_user_design.php';
}else
    include 'qry_save_user_design.php';
