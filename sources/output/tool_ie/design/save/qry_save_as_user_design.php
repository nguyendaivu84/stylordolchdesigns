<?php
if(!isset($v_sval)) die();
?>
<?php
add_class('cls_tb_design_template');
add_class('cls_tb_design_image');
add_class('cls_tb_design_design');

$cls_templates = new cls_tb_design_template($db,LOG_DIR);
$cls_images = new cls_tb_design_image($db,LOG_DIR);
$cls_designs = new cls_tb_design_design($db,LOG_DIR);

if($v_template_id<=0 && $v_old_design_id>0){
    $v_template_id = $cls_designs->select_scalar('template_id', array('design_id'=>$v_old_design_id));
    if(is_null($v_template_id)) $v_template_id = 0;
    settype($v_template_id, 'int');
}


$v_row = $cls_templates->select_one(array('template_id'=>$v_template_id));

$v_success = 0;
$v_design_id = 0;

$v_str = '';
$v_image_data = '';
$v_sample_image = '';

$v_stock_cost = 0;
$v_markup_cost = 0;
$v_print_cost = 0;

$arr_tmp_images = array();
$v_json = stripslashes($v_json);
$arr_json = json_decode($v_json, true);
$arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();

$v_total_page = count($arr_canvases);
$v_bleed = 0.0;

if($v_row==1){
    $v_template_name = $cls_templates->get_template_name();
    $v_design_width = $cls_templates->get_template_width();
    $v_design_height = $cls_templates->get_template_height();
    $v_markup_cost = $cls_templates->get_markup_cost();
    $v_print_cost = $cls_templates->get_print_cost();
    $v_folding_type = $cls_templates->get_folding_type();
    $v_folding_direction = $cls_templates->get_folding_direction();
    $v_die_cut_type = $cls_templates->get_die_cut_type();
    $v_bleed = $cls_templates->get_template_bleed();
    //$v_folding_type = $cls_settings->get_option_key_by_id('folding_type', $v_folding_type, 'none');
    //$v_folding_direction = $cls_settings->get_option_key_by_id('folding_direction', $v_folding_direction, 'vertical');
    //$v_die_cut_type = $cls_settings->get_option_key_by_id('die_cut_type', $v_die_cut_type, 'none');


    $arr_tmp = array(
        'texts'=>array()
        ,'images'=>array()
        ,'width'=>$v_design_width
        ,'height'=>$v_design_height
        ,'bg_color'=> "FFFFFF"
        ,'bleed'=>	$v_bleed
        ,'droppable'=> true
        ,'name'=> "Front Side"
        ,"isViewed"=>false
    );

    $v_total_page = count($arr_canvases);

    if($v_folding_type<=0){
        if($v_total_page>=1){
            $arr_canvas = array();
            $i = 0;
            foreach($arr_canvases as $idx=>$arr){
                if($i==0) $arr_canvas[] = $arr;
                $i++;
            }
        }else{
            $arr_canvas[] = $arr_tmp;
        }
        $arr_canvases = $arr_canvas;
    }else{
        if($v_total_page<1){
            $arr_canvases = array($arr_tmp, $arr_tmp);
        }else if($v_total_page==1){
            $i=0;
            $arr_canvas = array();
            foreach($arr_canvases as $idx=>$arr){
                if($i==0) $arr_canvas[0] = $arr;
                $i++;
            }
            $arr_canvas[1] = $arr_tmp;
        }else{
            $i=0;
            $arr_canvas = array();
            foreach($arr_canvases as $idx=>$arr){
                if($i<=1) $arr_canvas[$i] = $arr;
                $i++;
            }
        }
        if($v_folding_type==1)
            $arr_canvases[1]['name'] = 'Back Side';
        else{
            $arr_canvases[1]['name'] = 'InSide';
            $arr_canvases[0]['name'] = 'OutSide';
        }
    }

    $arr_json['canvases'] = $arr_canvases;
    $v_total_page = count($arr_canvases);


    $v_page = 0;

    $v_prefix = ''; //Prefix for text name

    if($arr_json['width']) $v_design_width = $arr_json['width'];
    if($arr_json['height']) $v_design_height = $arr_json['height'];

    settype($v_design_height, 'float');
    settype($v_design_width, 'float');


    $v_ext = 'png';
    $v_sample_image = 'template'.$v_template_id.'_theme'.$v_theme_id.'_'.date('YmdHis').'.'.$v_ext;
    $v_dir = DESIGN_DESIGN_DIR.DS;

    if(file_exists($v_dir.$v_template_id) || @mkdir($v_dir.$v_template_id)){
        $v_dir .= $v_template_id .DS;
    }

    $v_saved_dir = str_replace($v_root_dir,'',$v_dir);
    $v_saved_dir = str_replace('\\','/', $v_saved_dir);

    $v_full_path = $v_dir.$v_sample_image;
    $v_accept = true;
    if(file_exists($v_full_path)){
        $v_accept = @unlink($v_full_path);
    }

    $cls_tmp_draw = new cls_draw();
    $image = new Imagick();
    if($v_total_page<=1){
        $v_page = 0;
        $v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
        $cls_draw->create_preview($image, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
        if($v_accept){
            $image = $cls_tmp_draw->shadow_image($image, DESIGN_IMAGE_THUMB_SIZE);
            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
        }
    }else{
        if($v_accept){
            $v_page = 1;
            $image2 = new Imagick();
            $cls_draw->create_preview($image2, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
            $v_page = 0;
            $image1 = new Imagick();
            $cls_draw->create_preview($image1, $cls_images, $cls_instagraph, $arr_json, $v_dpi, $v_page, is_admin());
            $image = $cls_tmp_draw->create_sample($image1, $image2, DESIGN_IMAGE_THUMB_SIZE);

            $image->setimageformat('png');
            $image->writeimage($v_full_path);
            $image->clear();
            $image->destroy();
            $image1->clear();
            $image1->destroy();
            $image2->clear();
            $image2->destroy();

        }
    }
    $arr_text_name = array();
    for($i=0; $i<count($arr_canvases); $i++){
        $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();

        for($j=0;$j<count($arr_images);$j++){
            $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
            $v_noprint = isset($arr_images[$j]['noprint'])?$arr_images[$j]['noprint']:'false';
            settype($v_image_id, 'int');
            if($v_image_id>0){
                $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
                if($v_row==1){
                    $v_image_name = $cls_images->get_image_name();
                    $v_image_width = $cls_images->get_image_width();
                    $v_image_height = $cls_images->get_image_height();
                    $v_image_dpi = $cls_images->get_image_dpi();
                    $v_image_extension = $cls_images->get_image_extension();
                    $v_is_public = $cls_images->get_is_public();
                    $v_is_vector = $cls_images->get_is_vector();
                    $v_customer_id = $cls_images->get_user_id();
                    $v_created_time = $cls_images->get_created_time();
                    $v_fotolia_id = $cls_images->get_fotolia_id();
                    $v_fotolia_size = $cls_images->get_fotolia_size();
                    $v_image_stock_id = $cls_images->get_image_stock_id();
                    $arr_license = array();

                    if($v_image_stock_id>0){
                        $v_stock_row = $cls_stocks->select_one(array('stock_id'=>$v_image_stock_id));
                        if($v_stock_row==1){
                            $v_change = true;
                            $arr_images[$j]['fotoliaId'] = $v_fotolia_id;
                            $arr_images[$j]['fotoliaLicense'] = $v_fotolia_size;
                            $license = $cls_stocks->get_licenses();
                            if(is_array($license)){
                                for($k=0; $k<sizeof($license);$k++){
                                    if(isset($license[$k]['dimensions'])) unset($license[$k]['dimensions']);
                                    $license[$k]['id'] = ($v_image_id * 100) + $k;
                                    $license[$k]['fotolia_id'] = $v_fotolia_id;
                                    $license[$k]['license_value'] = $license[$k]['price'];
                                    $license[$k]['is_on_disk'] = 0;
                                    $license[$k]['use_for_comp'] = 0;
                                }
                                $arr_license[$v_fotolia_id] = $license;
                            }
                        }
                    }

                    if(is_object($v_created_time))
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time->sec);
                    else
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time);

                    $v_stock_cost += $cls_images->get_image_cost();
                    $arr_tmp_images[$v_image_id] = array(
                        "id"=>$v_image_id.''
                        ,'customer_id'=> "".$v_customer_id
                        ,'session_id'=> "".session_id()
                        ,'site_code'=> DESIGN_SITE_CODE
                        ,'asset_name'=> ""
                        ,'is_vector'=> $v_is_vector
                        ,'name'=> $v_image_name
                        ,'extension'=> $v_image_extension
                        ,'width'=> $v_image_width.''
                        ,'height'=> $v_image_height.''
                        ,'dpi'=> $v_image_dpi
                        ,'added_on'=> $v_created_time
                        ,'is_public'=> $v_is_public
                        ,'delete_flag'=> "1"
                        ,'licenses'=>$arr_license
                        ,'svg_original_colors'=>null
                        ,'svg_id'=>null
                    );
                }
            }
        }

        for($j=0;$j<count($arr_texts);$j++){
            $v_title = isset($arr_texts[$j]['title'])?$arr_texts[$j]['title']:'';
            if($v_title=='') $v_title = 'Enter Text';
            if($v_title!=''){
                $v_title = html_entity_decode($v_title);
                $v_title = urldecode($v_title);
                $v_title = seo_friendly_url($v_title);
                $v_title = $v_prefix.str_replace('-','_', $v_title);
                if(isset($arr_text_name[$v_title])){
                    $k = 1;
                    do{
                        $v_new_title = $v_title.'_'.$k;
                        if(!isset($arr_text_name[$v_new_title])){
                            $arr_texts[$j]['name'] = $v_new_title;
                            $arr_text_name[$v_new_title] = 1;
                            $k = -1;
                        }else{
                            $k++;
                        }
                    }while($k>0);
                }else{
                    $arr_texts[$j]['name'] = $v_title;
                    $arr_text_name[$v_title] = 1;
                }
            }

        }
        $arr_canvases[$i]['texts'] = $arr_texts;
    }
    $v_image_data = json_encode($arr_tmp_images);
    $arr_json['canvases'] = $arr_canvases;
    $v_json = json_encode($arr_json);

    //$v_design_id = $cls_designs->select_next('design_id');
    $cls_designs->set_design_name($v_name);
    $cls_designs->set_user_agent($v_design_user_agent);
    $cls_designs->set_user_ip($v_design_user_ip);
    $cls_designs->set_user_id($v_design_user_id);
    $cls_designs->set_created_time(date('Y-m-d H:i:s'));
    $cls_designs->set_design_data($v_json);
    $cls_designs->set_design_image($v_image_data);
    $cls_designs->set_design_status(0);
    $cls_designs->set_product($arr_product);
    $cls_designs->set_product_id($v_product_id);
    $cls_designs->set_theme_id($v_theme_id);
    $cls_designs->set_template_id($v_template_id);
    //$cls_designs->set_design_id($v_design_id);
    $cls_designs->set_sample_image($v_sample_image);
    $cls_designs->set_design_width($v_design_width);
    $cls_designs->set_design_height($v_design_height);
    $cls_designs->set_design_folding($v_folding_type);
    $cls_designs->set_design_direction($v_folding_direction);
    $cls_designs->set_design_die_cut($v_die_cut_type);
    $cls_designs->set_stock_cost($v_stock_cost);
    $cls_designs->set_markup_cost($v_markup_cost);
    $cls_designs->set_print_cost($v_print_cost);
    $cls_designs->set_company_id($v_user_company_id);
    $cls_designs->set_location_id($v_user_location_id);
    $cls_designs->set_user_name($v_design_user_name);
    $cls_designs->set_saved_dir($v_saved_dir);
    $cls_designs->set_design_bleed($v_bleed);
    $cls_designs->set_design_dpi($v_dpi);
    $cls_designs->set_site_id($v_site_id);
    $cls_designs->set_old_design_id($v_old_design_id);

    $v_design_id = $cls_designs->insert();
    $v_result = $v_design_id > 0;
    if($v_result) $v_success = 1;
    $arr_data = array('success'=>$v_success, 'design_id'=>$v_design_id, 'theme_id'=>$v_theme_id);
}else{
    $arr_data = array('success'=>$v_success, 'design_id'=>0, 'theme_id'=>0);
}

//header("Content-type: application/json");

$arr_return = array(
    'success'=>1,
    'message'=>'',
    'data'=>$arr_data
);
$cls_output->output($arr_return, true, false);
//echo json_encode(array('success'=>$v_success, 'design_id'=>$v_design_id, 'theme_id'=>$v_theme_id));