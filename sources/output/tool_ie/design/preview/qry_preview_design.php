<?php
if(!isset($v_sval)) die();?>
<?php

$v_json = isset($_REQUEST['json'])?$_REQUEST['json']:'';
$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:'0';
$v_dpi = isset($_REQUEST['dpi'])?$_REQUEST['dpi']:'0';
$v_hide = isset($_REQUEST['hide'])?$_REQUEST['hide']:'0';

settype($v_page, 'int');
settype($v_dpi, 'int');
settype($v_hide, 'int');

$v_hide = $v_hide==1;

$arr_json = json_decode($v_json, true);
add_class('cls_tb_design_image');
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$v_is_admin = is_admin() || (isset($_SESSION['ss_design_side']) && $_SESSION['ss_design_side']==DESIGN_SIDE_FACE);
$image = new Imagick();

$cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);

$v_ext = 'png';
$v_original_name = 'tmp_'.session_id().'_'.date('YmdHis');
$v_dir = DESIGN_TEMP_DIR.DS;
$v_url_dir = ROOT_DIR.DS;
$v_url_dir = str_replace($v_url_dir,'',$v_dir);

$v_file_name = $v_original_name.'.'.$v_ext;
$v_full_path = $v_dir.$v_file_name;
$v_accept = false;
$i = 1;
while(!$v_accept){
    $v_accept = !file_exists($v_full_path);
    if(!$v_accept){
        $v_file_name = $v_original_name.'('.($i++).').'.$v_ext;
        $v_full_path = $v_dir.$v_file_name;
    }
}

$image->setimageformat($v_ext);
$image->writeimage($v_full_path);

$v_width = $image->getimagewidth();
$v_height = $image->getimageheight();
$image->clear();
$image->destroy();
$arr_data = array('file'=>URL.str_replace(DS,'/', $v_url_dir).$v_file_name, 'page'=>$v_page, 'hide'=>$v_hide);

$arr_return = array();
$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);