<?php
if(!isset($v_sval)) die();
add_class('cls_settings');
add_class('cls_tb_site_log');
add_class('cls_draw');
add_class('cls_output');
add_class('cls_instagraph');
$cls_instagraph = new cls_instagraph();
$cls_settings = new  cls_settings($db, LOG_DIR);
$cls_output = new  cls_output($db);
$cls_draw = new cls_draw();
$cls_site_log = new cls_tb_site_log($db, LOG_DIR);
//Remove temporary file
$v_count = $cls_draw->remove_temp_file(DESIGN_TEMP_DIR, time(), 1);
$v_root_dir = ROOT_DIR.DS;

$v_tool = isset($_GET['tool'])?$_GET['tool']:'';
$arr_design_user = isset($_POST['user'])?$_POST['user']:array();
if(!is_array($arr_design_user)) $arr_design_user = array();

switch($v_tool){
    case 'TEMPLATE':
        include 'template/index.php';
        break;
    case 'DESIGN':
        include 'design/index.php';
        break;
    case 'save_design':
        include 'save_design/index.php';
        break;
    case 'product_design':
        include 'product_design/index.php';
        break;
    case 'assets':
    default:
        include 'assets/index.php';
        break;
}