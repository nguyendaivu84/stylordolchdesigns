<?php
if(!isset($v_sval)) die();?>
<?php
$arr_option = isset($_GET['option'])?$_GET['option']:array();
if(!is_array($arr_option)) $arr_option = array();

$v_folding_type = "none";


$v_product_code = isset($arr_product['code'])?$arr_product['code']:'blankDesign';
$v_product_title = isset($arr_product['title'])?$arr_product['title']:'Blank Design';
$v_product_id = isset($arr_product['id'])?$arr_product['id']:'0';
settype($v_product_id, 'int');


$arr_data = array('success'=>0);

$v_width = isset($arr_option['w'])?$arr_option['w']:'10';
$v_height = isset($arr_option['h'])?$arr_option['h']:'10';
settype($v_height, 'float');
settype($v_width, 'float');
if($v_width<=0) $v_width = 10;
if($v_height<=0) $v_height = 10;

$arr_data['width'] = $v_width;
$arr_data['height'] = $v_height;
$arr_data['folding'] = $v_folding_type;
$arr_data['diecut'] = "none";
$arr_data['title'] = $v_product_title;
$arr_data['id'] = $v_template_id;
$arr_data['theme_id'] = 0;
$arr_data['price_markup'] = 0;
$arr_data['assign_to'] = 0;
$arr_data['product_id'] = isset($v_product_id)?$v_product_id:'0';
$arr_data['product_title'] = $v_product_title;
$arr_data['product_url_code'] = $v_product_code;
$arr_data['stock_image_code'] = 0;
$arr_data['size_title'] = $v_width.'" &times; '.$v_height.'" ';
$arr_data['set_id'] = $v_template_id;
$arr_data['set_title'] = "Blank Design";
$arr_data['status_id'] = 4;
$arr_data['success'] = 1;
$arr_data['images']= array();//
$arr_data['json']= array();//
$arr_data['fotoliaLicenses']= array();
$arr_data['themes']= array();

//$v_where_clause = " AND `template_id`='{$v_template_id}' AND `theme_id`='{$v_theme_id}'";
//add_class('cls_design_themes');
//$cls_themes = new cls_design_themes($cn, '', _PREFIX_TBL, false);
//$v_row = $cls_themes->select_one($v_where_clause);

$arr_images = array();
$arr_json = array();
$v_is_template = 0;
$v_current_text_color = '';
$v_current_theme_name = '';
$v_current_theme_color = '';

$v_theme_data = '';
$v_theme_image = '';

$arr_images = array();

if($v_theme_data!='') $arr_json = json_decode($v_theme_data, true);
if($v_theme_data=='' || !is_array($arr_json)){
    $arr_tmp = array(
        'texts'=>array()
        ,'images'=>array()
        ,'width'=>$v_width
        ,'height'=>$v_height
        ,'bg_color'=> "FFFFFF"
        ,'bleed'=>	0.125
        ,'droppable'=> true
        ,'name'=> "Front Side"
        ,"isViewed"=>false
    );
    $arr_canvases = array();
    $arr_canvases = array($arr_tmp);

    $arr_json = array(
        'version'=>'2',
        'canvases'=>$arr_canvases

        ,'width'=>$v_width
        ,'height'=>$v_height
        ,'dieCutType'=>"none"
        ,'folding'=>$v_folding_type
        ,'foldingDirection'=>"vertical"
        ,'product'=>$v_product_code
        ,'stockImages'=>array()
        ,'wrap_size'=>0
    );

}else{
    $arr_json['product'] = $v_product_code;
}

$arr_json['folding'] = $v_folding_type;
$arr_data['images'] = $arr_images;
$arr_data['json'] = $arr_json;

$arr_return = array();
$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);