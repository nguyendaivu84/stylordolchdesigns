<?php
if(!isset($v_sval)) die();
$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_theme_id = isset($_POST['theme_id'])?$_POST['theme_id']:'0';
$arr_product = isset($_POST['product'])?$_POST['product']:array();
if(!is_array($arr_product)) $arr_product = array();

$v_product_code = isset($arr_product['code'])?$arr_product['code']:'blankDesign';
$v_product_title = isset($arr_product['title'])?$arr_product['title']:'Blank Design';
$v_product_id = isset($arr_product['id'])?$arr_product['id']:'0';
settype($v_product_id, 'int');
settype($v_template_id, 'int');
settype($v_theme_id, 'int');

if($v_template_id>0){
    $v_site_index = $cls_key->select_scalar('site_index', array('site_id'=>$v_site_id));
    switch($v_site_index){
        /*
        case 'worktraq':
        case 'worktraq-dev':
            include 'qry_load_worktraq_user_template.php';
            break;
        case 'gotodisplay';
            include 'qry_load_gotodisplay_user_template.php';
            break;
        case 'golf-imagestylor':
            include 'qry_load_golf_user_template.php';
            break;
        */
        default:
            include 'qry_load_user_template.php';
            break;
    }
}else{
    include 'qry_load_user_empty_template.php';
}