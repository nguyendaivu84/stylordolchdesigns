<?php
if(!isset($v_sval)) die();

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';
$v_user_id = isset($_POST['user_id'])?$_POST['user_id']:'0';

settype($v_design_id, 'int');
settype($v_user_id, 'int');
if($v_design_id<0) $v_design_id = 0;
if($v_user_id<0) $v_user_id = 0;

$arr_where_clause = array('design_id'=>$v_design_id);

$v_design_row = $cls_design->select_one($arr_where_clause);
$arr_data = array('success'=>1, 'message'=>'OK');

$v_success = 1;
$v_message = 'OK';
if($v_design_row==1){
    $v_saved_dir = $cls_design->get_saved_dir();
    $v_sample_image = $cls_design->get_sample_image();
    $v_design_user_id = $cls_design->get_user_id();
    $v_design_site_id = $cls_design->get_site_id();
    $v_design_data = $cls_design->get_design_data();
    if($v_design_site_id==$v_site_id && $v_design_site_id!=0){
        if($v_design_user_id==$v_user_id && $v_design_user_id!=0){
            $v_file = ROOT_DIR.DS.$v_saved_dir.$v_sample_image;
            if(file_exists($v_file) && is_file($v_file)) @unlink($v_file);
            $v_result = $cls_design->delete($arr_where_clause);
            if(! $v_result){
                $v_success = 0;
                $v_message = 'This design could not be deleted!';
            }
        }else{
            $v_success = 0;
            $v_message = 'This design does not belong to you!';

            $arr_json = json_decode($v_design_data, true);
            $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_size = sizeof($arr_canvases);
            $arr_image_remove = array();
            for($i=0; $i<$v_size; $i++){
                $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                $v_image_size = sizeof($arr_images);
                for($j=0; $j < $v_image_size; $j++){
                    $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                    settype($v_image_id, 'int');
                    if($v_image_id > 0){
                        if(!isset($arr_image_remove[$v_image_id])){
                            $arr_image_remove[$v_image_id] = array('template'=>0, 'design'=>-1);
                        }
                    }
                }
            }
            add_class('cls_tb_design_image');
            $cls_image = new cls_tb_design_image($db, LOG_DIR);
            $v_total = $cls_image->update_image_used($arr_image_remove);
        }
    }else{
        $v_success = 0;
        $v_message = 'This design does not belong to the current site!';
    }
}
$arr_data['success'] = $v_success;
$arr_data['message'] = $v_message;
$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);