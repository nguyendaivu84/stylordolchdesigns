<?php
if(!isset($v_sval)) die();

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';

settype($v_design_id, 'int');
if($v_design_id<0) $v_design_id = 0;

$arr_where_clause = array('design_id'=>$v_design_id);

$v_design_image = $cls_design->select_scalar('sample_image',$arr_where_clause);
if(!is_null($v_design_image)) $v_design_image = URL.$v_design_image;
$arr_data = array();

$arr_data = array(
	'id'=>$v_design_id
	,'image'=>$v_design_image
);

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);