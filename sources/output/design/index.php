<?php
if(!isset($v_sval)) die();

add_class('cls_tb_design_design');
$cls_design = new cls_tb_design_design($db, LOG_DIR);
$v_design = isset($_GET['design'])?$_GET['design']:'';


if($v_site_id>0){
    $arr_switch = array(
        'LIST'=>'List All Designs'
        ,'INFO'=>'Get Design\'s Info'
        ,'COUNT'=>'Count All Designs'
        ,'PREVIEW'=>'Design\'s Preview'
        ,'PREVIEW3D'=>'Design\'s Preview3D'
        ,'LABEL'=>'Get Design\'s Text Label'
        ,'TEXT'=>'Get Design\'s Text Data'
        ,'CLONE'=>'Clone Design'
        ,'ZIP'=>'Zip Designs'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot()
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_design])?$arr_switch[$v_design]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_design){
    case 'LIST':
        include 'qry_design_list.php';
        break;
    case 'INFO':
        include 'qry_design_info.php';
        break;
    case 'COUNT':
        include 'qry_design_count.php';
        break;
    case 'PREVIEW':
        include 'qry_design_preview.php';
        break;
    case 'PREVIEW3D':
        include 'qry_design_preview3d.php';
        break;
    case 'LABEL':
        include 'qry_design_label.php';
        break;
    case 'TEXT':
        include 'qry_design_label_data.php';
        break;
    case 'DELETE':
        include 'qry_design_delete.php';
        break;
    case 'CLONE':
        include 'qry_design_clone.php';
        break;
    case 'ZIP':
        include 'qry_design_zip.php';
        break;
    case 'GENERATE':
        include 'qry_design_generate.php';
        break;
    default:
        $cls_output->output($arr_return);
        break;
}