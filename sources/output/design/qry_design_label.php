<?php
if(!isset($v_sval)) die();

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';

settype($v_design_id, 'int');
if($v_design_id<0) $v_design_id = 0;

$arr_where_clause = array('design_id'=>$v_design_id);

$v_design_row = $cls_design->select_one($arr_where_clause);
$arr_data = array();

if($v_design_row==1){
    $v_design_data = $cls_design->get_design_data();// isset($arr['design_name'])?$arr['design_name']:'';

    $arr_json = json_decode($v_design_data, true);
    if(!is_array($arr_json)) $arr_json = array();
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    $v_size_canvases = sizeof($arr_canvases);

    for($i=0; $i<$v_size_canvases;$i++){
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();

        $v_size_texts = sizeof($arr_texts);
        for($j=0; $j<$v_size_texts;$j++){
            $v_name = isset($arr_texts[$j]['name'])?$arr_texts[$j]['name']:'';
            if($v_name!='' && !in_array($v_name, $arr_data)){
                $arr_data[] = $v_name;
            }
        }
    }
}
$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);