<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template'])?$_POST['template']:0;
$v_design_id = isset($_POST['design'])?$_POST['design']:0;
$v_theme_id = isset($_POST['theme'])?$_POST['theme']:0;
$arr_url = isset($_POST['url'])?$_POST['url']:array();
$arr_image_user = isset($_POST['user'])?$_POST['user']:array();
$arr_product = isset($_POST['product'])?$_POST['product']:array();
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_design_id, 'int');

if(!is_array($arr_image_user)) $arr_image_user = array();
$arr_data = array();
$v_data = '';
$v_success = 1;
$v_message = '';

add_class('cls_draw');
$cls_draw = new cls_draw();

$arr_ext = array('', 'gif', 'jpg', 'png', 'pdf');

$v_design_data = '';
$v_folding_type = 0;
$v_template_width = 10;
$v_template_height = 10;
$v_template_bleed =  0;
$v_total_image_cost = 0;
$v_got_design = false;

add_class('cls_tb_design_image');
add_class('cls_tb_design_stock');
$cls_images = new cls_tb_design_image($db, LOG_DIR);
$cls_stocks = new cls_tb_design_stock($db, LOG_DIR);
$v_template_row = 0;
if($v_design_id>0){
    $v_row = $cls_design->select_one(array('design_id'=>$v_design_id));
    if($v_row==1){
        $v_got_design = true;
        $v_design_data = $cls_design->get_design_data();
        $v_folding_type = $cls_design->get_design_folding();
        $v_template_width = $cls_design->get_design_width();
        $v_template_height = $cls_design->get_design_height();
        $v_template_bleed = $cls_design->get_design_bleed();
    }
}else if($v_template_id>0){
    add_class('cls_tb_design_template');
    $cls_template = new cls_tb_design_template($db, LOG_DIR);
    $v_template_row = $cls_template->select_one(array('template_id'=>$v_template_id));
    if($v_template_row==1){
        $v_design_data = $cls_template->get_template_data();
        $v_folding_type = $cls_template->get_folding_type();
        $v_template_width = $cls_template->get_template_width();
        $v_template_height = $cls_template->get_template_height();
        $v_template_bleed = $cls_template->get_template_bleed();
    }
}

$arr_tmp = array(
    'texts'=>array()
    ,'images'=>array()
    ,'width'=>($v_template_width + 2 * $v_template_width)
    ,'height'=>($v_template_height + 2 * $v_template_bleed)
    ,'bg_color'=> "FFFFFF"
    ,'bleed'=>	$v_template_bleed
    ,'droppable'=> true
    ,'name'=> "Front Side"
    ,"isViewed"=>false
);

$v_work_width = $v_template_width - 2 * $v_template_bleed;
$v_work_height = $v_template_height - 2 * $v_template_bleed;
$v_x = $v_template_width / 2;
$v_y = $v_template_height / 2;



function check_image_exist($image_id, array & $arr_data){
    $i = 0;
    $found = false;
    do{
        $v_image_id = isset($arr_data[$i]['image_id'])?$arr_data[$i]['image_id']:0;
        if($v_image_id==$image_id && $image_id>0){
            $found = true;
            //$arr_data[$i]['used'] = true;
        }
        $i++;
    }while($i < sizeof($arr_data) && !$found);
    return $found?$arr_data[$i-1]:null;
}
add_class('cls_file');
$mf = new ManageFile(DESIGN_TEMP_DIR, DS);

if(is_array($arr_url)){
    //foreach($arr_url as $image_id=> $url){
    for($l=0; $l<sizeof($arr_url);$l++){
        $v_url = isset($arr_url[$l]['url'])?trim($arr_url[$l]['url']):'';
        $image_id = isset($arr_url[$l]['id'])?trim($arr_url[$l]['id']):0;
        $v_change = isset($arr_url[$l]['change'])?trim($arr_url[$l]['change']):0;
        settype($image_id, 'int');
        settype($v_change, 'int');

        $arr_data[$l] = array(
            'url'=>$v_url!=''?$v_url:''
            ,'valid'=>filter_var($v_url, FILTER_VALIDATE_URL)?true:false
            ,'upload'=>false
            ,'image_id'=>$image_id
            ,'upload_id'=>$v_change?0:$image_id
            ,'change'=>$v_change==1
            //,'used'=>false
        );
        if($v_change==0) continue;
        if($v_url!=''){
            if(filter_var($v_url, FILTER_VALIDATE_URL)){
                $v_upload_dir = DESIGN_USER_DIR;
                if($v_design_id>0){
                    if(file_exists($v_upload_dir.DS.$v_design_id) || @mkdir($v_upload_dir.DS.$v_design_id)){
                        if(is_dir($v_upload_dir.DS.$v_design_id) && is_writable($v_upload_dir.DS.$v_design_id))
                            $v_upload_dir .= DS.$v_design_id;
                    }
                }

                $v_name = basename($v_url);
                if($v_name!=''){
                    list($name, $ext) =  explode('.', $v_name);
                    $v_content = file_get_contents($v_url);
                    $ext = strtolower($ext);
                    if($v_content){
                        $v_count = 0;
                        $v_new_full_path = $v_upload_dir . DS . $name . '.'. $ext;
                        $v_new_image_file = $name . '.'. $ext;
                        $v_is_exist = file_exists($v_new_full_path);
                        while($v_is_exist){
                            $v_count++;
                            $v_new_full_path = $v_upload_dir . DS . $name . '('.$v_count.')'. $ext;
                            $v_new_image_file = $name . '('.$v_count.')'. $ext;
                            $v_is_exist = file_exists($v_new_full_path);
                        }
                        //$v_image_original_file = $v_new_image_file;
                        if(file_put_contents($v_new_full_path, $v_content)){
                            list($width, $height, $type) = getimagesize($v_new_full_path);
                            $v_dpi = ceil($width*$height / pow(96,2));
                            $v_image_key = seo_friendly_url($v_name);
                            $v_image_key = str_replace('-','_', $v_image_key);

                            $v_image_key = str_replace('img','pic', $v_image_key);

                            $i = 0;
                            $v_tmp_image_key = $v_image_key;
                            do{
                                $arr_tmp_where = array('image_key'=>$v_tmp_image_key/*, 'image_id'=>array('$ne'=>$v_image_id)*/);
                                $v_tmp_row = $cls_images->select_one($arr_tmp_where);
                                if($v_tmp_row>0){
                                    $i++;
                                    $v_tmp_image_key = $v_image_key.'_'.$i;
                                }
                            }while($v_tmp_row>0);
                            $v_image_key = $v_tmp_image_key;

                            $v_image_original_file = $cls_images->backup_original_image($v_new_full_path, $v_new_image_file, $arr_ext[$type], IMAGE_MAX_SIZE_USE, array('w'=>IMAGE_MAX_WIDTH_USE, 'h'=>IMAGE_MAX_HEIGHT_USE), IMAGE_ORIGINAL_PREFIX, DS);

                            $v_saved_dir = str_replace(ROOT_DIR . DS,'', $v_upload_dir);
                            $v_saved_dir = str_replace(DS,'/', $v_saved_dir) . '/';
                            $v_size = strlen($v_content);
                            $cls_images->set_image_name($v_name);
                            $cls_images->set_image_file($v_new_image_file);
                            $cls_images->set_image_original_file($v_image_original_file);
                            $cls_images->set_image_key($v_image_key);
                            $cls_images->set_image_type($type);
                            $cls_images->set_image_dpi($v_dpi);
                            $cls_images->set_image_extension($arr_ext[$type]);
                            $cls_images->set_image_height($height);
                            $cls_images->set_image_width($width);
                            $cls_images->set_image_cost(0);
                            $cls_images->set_image_size($v_size);
                            $cls_images->set_image_status(0);
                            $cls_images->set_is_public(0);
                            $cls_images->set_image_stock_id(0);
                            $cls_images->set_design_id($v_design_id);
                            $cls_images->set_fotolia_id('');
                            $cls_images->set_fotolia_size('');
                            $cls_images->set_template_id($v_template_id);
                            $cls_images->set_theme_id($v_theme_id);
                            $cls_images->set_saved_dir($v_saved_dir);
                            $cls_images->set_created_time(date('Y-m-d H:i:s'), false);
                            $cls_images->set_user_id(isset($arr_image_user['user_id'])?$arr_image_user['user_id']:0);
                            $cls_images->set_is_admin(0);
                            $cls_images->set_user_ip(isset($arr_image_user['user_ip'])?$arr_image_user['user_ip']:'');
                            $cls_images->set_user_name(isset($arr_image_user['user_name'])?$arr_image_user['user_name']:'');
                            $cls_images->set_user_agent(isset($arr_image_user['user_agent'])?$arr_image_user['user_agent']:'');
                            $cls_images->set_company_id(isset($arr_image_user['company_id'])?$arr_image_user['company_id']:0);
                            $cls_images->set_site_id($v_site_id);
                            $cls_images->set_location_id(isset($arr_image_user['location_id'])?$arr_image_user['location_id']:0);
                            $v_image_id = $cls_images->insert();
                            $v_result = $v_image_id > 0;
                            if($v_result){
                                $v_image_thumb = $cls_images->create_thumb($cls_draw, DESIGN_IMAGE_THUMB_SIZE, ROOT_DIR, DS, URL);
                                if($v_image_thumb==''){
                                    $v_image_thumb = URL . $v_saved_dir . $v_new_image_file;
                                }
                                $arr_data[$l]['upload'] = true;
                                $arr_data[$l]['upload_id'] = $v_image_id;
                            }

                        }
                    }
                }
            }
        }
    }
}else{
    $v_message = 'Not retrieve url data';
}

if($v_design_data!=''){
    $arr_json = json_decode($v_design_data, true);
}else{
    $arr_json = array(
        'version'=>'1.0',
        'canvases'=>array()
        ,'width'=>$v_template_width
        ,'height'=>$v_template_height
        ,'dieCutType'=>"none"
        ,'folding'=>$v_folding_type
        ,'foldingDirection'=>"vertical"
        ,'product'=>isset($arr_product['code'])?$arr_product['code']:'blankDesign'
        ,'stockImages'=>array()
        ,'wrap_size'=>0
    );

}

if(!isset($arr_json) || !is_array($arr_json)) $arr_json = array();
$arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
$v_total_page = sizeof($arr_canvases);

if($v_folding_type<=0){
    $arr_canvas = array();
    if($v_total_page>=1){
        $i = 0;
        foreach($arr_canvases as $idx=>$arr){
            if($i==0) $arr_canvas[] = $arr;
            $i++;
        }
    }else{
        $arr_canvas[] = $arr_tmp;
    }
    $arr_canvases = $arr_canvas;
}else{
    if($v_total_page<1){
        $arr_canvases = array($arr_tmp, $arr_tmp);
    }else if($v_total_page==1){
        $i=0;
        $arr_canvas = array();
        foreach($arr_canvases as $idx=>$arr){
            if($i==0) $arr_canvas[0] = $arr;
            $i++;
        }
        $arr_canvas[1] = $arr_tmp;
    }else{
        $i=0;
        $arr_canvas = array();
        foreach($arr_canvases as $idx=>$arr){
            if($i<=1) $arr_canvas[$i] = $arr;
            $i++;
        }
    }
    if($v_folding_type==1)
        $arr_canvases[1]['name'] = 'Back Side';
    else{
        $arr_canvases[1]['name'] = 'InSide';
        $arr_canvases[0]['name'] = 'OutSide';
    }
}

$arr_tmp_images = array();
$v_change = false;
$arr_json['canvases'] = $arr_canvases;
$v_total_page = count($arr_canvases);
for($i=0; $i<$v_total_page; $i++){
    $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
    $v_min_index = 0;
    for($j=0;$j<count($arr_images);$j++){
        $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
        $v_noprint = isset($arr_images[$j]['noprint'])?$arr_images[$j]['noprint']:'false';
        $v_zindex = isset($arr_images[$j]['zindex'])?$arr_images[$j]['zindex']:0;
        settype($v_image_id, 'int');
        settype($v_zindex, 'int');

        if($v_min_index <= $v_zindex) $v_min_index = $v_zindex + 1;
        $v_image_change = false;

        $arr_tmp = check_image_exist($v_image_id, $arr_data);

        if(!is_null($arr_tmp) && is_array($arr_tmp)){
            if($arr_tmp['change']) {
                $v_new_image_id = isset($arr_tmp['upload_id']) ? $arr_tmp['upload_id'] : 0;
                settype($v_new_image_id, 'int');
                if($v_image_id!=$v_new_image_id) {
                    $v_image_id = $v_new_image_id;
                    $v_image_change = true;
                }
            }
        }

        if($v_image_id>0){
            $v_row = $cls_images->select_one(array('image_id'=>$v_image_id));
            if($v_row==1){
                $v_image_name = $cls_images->get_image_name();
                $v_image_width = $cls_images->get_image_width();
                $v_image_height = $cls_images->get_image_height();
                $v_image_dpi = $cls_images->get_image_dpi();
                $v_image_extension = $cls_images->get_image_extension();
                $v_image_stock_id = $cls_images->get_image_stock_id();
                $v_image_file = $cls_images->get_image_file();
                $v_created_time = $cls_images->get_created_time();
                $v_fotolia_id = $cls_images->get_fotolia_id();
                $v_fotolia_size = $cls_images->get_fotolia_size();
                $v_is_public = $cls_images->get_is_public();
                $v_is_vector = $cls_images->get_is_vector();
                $v_svg_id = $cls_images->get_svg_id();
                $arr_svg_colors = $cls_images->get_svg_original_colors();
                if(!is_array($arr_svg_colors)) $arr_svg_colors = array();


                if($v_image_change){
                    if(!$v_change) $v_change = $v_image_change;
                    $v_width = isset($arr_images[$j]['width'])?$arr_images[$j]['width']:0;
                    $v_height = isset($arr_images[$j]['height'])?$arr_images[$j]['height']:0;
                    $v_ratio = $v_height>0? $v_width / $v_height:0;
                    $v_image_ratio = $v_image_height > 0? $v_image_width / $v_image_height:0;
                    $arr_images[$j]['image_id'] = $v_image_id;
                    if($v_image_ratio != $v_ratio){
                        $arr_images[$j]['crop'] = true;
                        if($v_ratio > $v_image_ratio){ //$v_width > $v_image_width: by image width
                            $v_ratio_width = $v_width / $v_image_width;

                            $v_tmp_height = $v_width_ratio * $v_image_height;
                            $v_cropHeight = $v_height / $v_tmp_height;
                            $v_cropTop = (1 - $v_cropHeight)/2;
                            $arr_images[$j]['cropTop'] = $v_cropTop;
                            $arr_images[$j]['cropHeight'] = $v_cropHeight;
                            $arr_images[$j]['cropLeft'] = 0;
                            $arr_images[$j]['cropWidth'] = 1;
                        }else{ //$v_width < $v_image_width: by image height
                            $v_ratio_height = $v_height / $v_image_height;
                            $v_tmp_width = $v_ratio_height * $v_image_width;
                            $v_cropWidth = $v_width / $v_tmp_width;
                            $v_cropLeft = (1 - $v_cropWidth) / 2;
                            $arr_images[$j]['cropLeft'] = $v_cropLeft;
                            $arr_images[$j]['cropWidth'] = $v_cropWidth;
                            $arr_images[$j]['cropTop'] = 0;
                            $arr_images[$j]['cropHeight'] = 1;
                        }
                    }else{
                        $arr_images[$j]['crop'] = false;
                        if(isset($arr_images['cropTop'])) unset($arr_images['cropTop']);
                        if(isset($arr_images['cropLeft'])) unset($arr_images['cropLeft']);
                        if(isset($arr_images['cropHeight'])) unset($arr_images['cropHeight']);
                        if(isset($arr_images['cropWidth'])) unset($arr_images['cropWidth']);
                    }
                }


                $arr_license = array();

                if($v_image_stock_id>0){
                    $v_stock_row = $cls_stocks->select_one(array('stock_id'=>$v_image_stock_id));
                    if($v_stock_row==1){
                        $arr_images[$j]['fotoliaId'] = $v_fotolia_id;
                        $arr_images[$j]['fotoliaLicense'] = $v_fotolia_size;
                        $license = $cls_stocks->get_licenses();
                        if(is_array($license)){
                            for($k=0; $k<sizeof($license);$k++){
                                if(isset($license[$k]['dimensions'])) unset($license[$k]['dimensions']);
                                $license[$k]['id'] = (intval($v_image_id) * 100) + $k;
                                $license[$k]['fotolia_id'] = $v_fotolia_id;
                                $license[$k]['license_value'] = $license[$k]['price'];
                                $license[$k]['is_on_disk'] = 0;
                                $license[$k]['use_for_comp'] = 0;
                            }
                            $arr_license[$v_fotolia_id] = $license;
                        }
                        $v_total_image_cost += $cls_images->get_image_cost();
                    }
                }

                if(is_object($v_created_time))
                    $v_created_time = date('Y-m-d H:i:s', $v_created_time->sec);
                else
                    $v_created_time = date('Y-m-d H:i:s', $v_created_time);



                $v_total_image_cost += $cls_images->get_image_cost();
                $arr_tmp_images[$v_image_id] = array(
                    "id"=>$v_image_id.''
                    ,'customer_id'=> "".isset($arr_image_user['user_id'])?$arr_image_user['user_id']:0
                    ,'session_id'=> "".session_id()
                    ,'site_code'=> DESIGN_SITE_CODE
                    ,'asset_name'=> ""
                    ,'is_vector'=> $v_is_vector
                    ,'name'=> $v_image_file
                    ,'extension'=> $v_image_extension
                    ,'width'=> $v_image_width.''
                    ,'height'=> $v_image_height.''
                    ,'dpi'=> $v_image_dpi
                    ,'added_on'=> $v_created_time
                    ,'is_public'=> $v_is_public
                    ,'delete_flag'=> "1"
                    ,'licenses'=>$arr_license
                    ,'svg_original_colors'=>$arr_svg_colors
                    ,'svg_id'=>$v_svg_id

                );

            }
        }


    }

    if($i==0){
        if($v_min_index==0) $v_min_index = 100;
        $l = sizeof($arr_images);
        for($k=0; $k<sizeof($arr_data); $k++){
            $v_image_id = $arr_data[$k]['image_id'];
            $v_upload_id = $arr_data[$k]['upload_id'];
            if($v_image_id==0 && $v_upload_id>0){
                $v_row = $cls_images->select_one(array('image_id'=>$v_upload_id));
                if($v_row==1) {
                    $v_image_name = $cls_images->get_image_name();
                    $v_image_key = $cls_images->get_image_key();
                    $v_image_width = $cls_images->get_image_width();
                    $v_image_height = $cls_images->get_image_height();
                    $v_image_dpi = $cls_images->get_image_dpi();
                    $v_image_extension = $cls_images->get_image_extension();
                    $v_image_stock_id = $cls_images->get_image_stock_id();
                    $v_image_file = $cls_images->get_image_file();
                    $v_created_time = $cls_images->get_created_time();
                    $v_fotolia_id = $cls_images->get_fotolia_id();
                    $v_fotolia_size = $cls_images->get_fotolia_size();
                    $v_is_public = $cls_images->get_is_public();
                    $v_is_vector = $cls_images->get_is_vector();
                    $v_svg_id = $cls_images->get_svg_id();
                    $arr_svg_colors = $cls_images->get_svg_original_colors();
                    if (!is_array($arr_svg_colors)) $arr_svg_colors = array();
                    if(is_object($v_created_time))
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time->sec);
                    else
                        $v_created_time = date('Y-m-d H:i:s', $v_created_time);


                    $arr_images[$l]['image_id'] = $v_upload_id;
                    $arr_images[$l]['name'] = $v_image_key;
                    $arr_images[$l]['rotation'] = 0;
                    $arr_images[$l]['fixedratio'] = true;
                    $arr_images[$l]['crop'] = false;


                    $v_ratio_width = $v_work_width / $v_image_width;
                    $v_ratio_height = $v_work_height / $v_image_height;

                    if($v_ratio_width > $v_ratio_height){
                        $arr_images[$l]['height'] = $v_work_height;
                        $arr_images[$l]['width'] = $v_image_width * $v_ratio_height;
                    }else{
                        $arr_images[$l]['width'] = $v_work_width;
                        $arr_images[$l]['height'] = $v_image_height * $v_ratio_width;
                    }
                    $arr_images[$l]['left'] = $v_x - ($arr_images[$l]['width'] / 2);
                    $arr_images[$l]['top'] = $v_y - ($arr_images[$l]['height'] / 2);
                    $arr_images[$l]['zindex'] = $v_min_index;
                    $l++;
                    $v_min_index++;

                    $arr_tmp_images[$v_upload_id] = array(
                        "id"=>$v_upload_id.''
                        ,'customer_id'=> "".isset($arr_image_user['user_id'])?$arr_image_user['user_id']:0
                        ,'session_id'=> "".session_id()
                        ,'site_code'=> DESIGN_SITE_CODE
                        ,'asset_name'=> ""
                        ,'is_vector'=> 0
                        ,'name'=> $v_image_file
                        ,'extension'=> $v_image_extension
                        ,'width'=> $v_image_width.''
                        ,'height'=> $v_image_height.''
                        ,'dpi'=> $v_image_dpi
                        ,'added_on'=> $v_created_time
                        ,'is_public'=> $v_is_public
                        ,'delete_flag'=> "1"
                        ,'licenses'=>array()
                        ,'svg_original_colors'=>array()
                        ,'svg_id'=>0
                    );

                    if(!$v_change) $v_change = true;
                }
            }
        }
    }

    $arr_canvases[$i]['images'] = $arr_images;
}

if($v_change) $arr_json['canvases'] = $arr_canvases;

//$v_design_id = 0;
$v_product_id = isset($arr_product['id'])?$arr_product['id']:0;
$v_product_code = isset($arr_product['code'])?$arr_product['code']:'blankDesign';
$v_product_title = isset($arr_product['title'])?$arr_product['title']:'Blank Design';
$arr_product = array(
    'id'=>$v_product_id, 'code'=>$v_product_code, 'title'=>$v_product_title
);
$v_json = json_encode($arr_json);
$v_image_data = json_encode($arr_tmp_images);


if($v_design_id > 0){
    $arr_fields = array('design_data', 'design_image', 'stock_cost', 'last_modified');
    $arr_values = array($v_json, $v_image_data, $v_total_image_cost, new MongoDate(time()));
    $cls_design->update_fields($arr_fields, $arr_values, array('design_id'=>$v_design_id));

}else if($v_template_id>0 && $v_template_row==1){

    $cls_design->set_design_name('From: '.$cls_template->get_template_name());
    $cls_design->set_user_agent(isset($arr_image_user['user_agent'])?$arr_image_user['user_agent']:'');
    $cls_design->set_user_ip(isset($arr_image_user['user_ip'])?$arr_image_user['user_ip']:'');
    $cls_design->set_user_id(isset($arr_image_user['user_id'])?$arr_image_user['user_id']:0);
    $cls_design->set_created_time(date('Y-m-d H:i:s'));
    $cls_design->set_design_data($v_json);
    $cls_design->set_design_image($v_image_data);
    $cls_design->set_design_status(0);
    $cls_design->set_product($arr_product);
    $cls_design->set_product_id($v_product_id);
    $cls_design->set_theme_id($v_theme_id);
    $cls_design->set_template_id($v_template_id);
    //$cls_design->set_design_id($v_design_id);
    $cls_design->set_sample_image($cls_template->get_sample_image());
    $cls_design->set_design_width($cls_template->get_template_width());
    $cls_design->set_design_height($cls_template->get_template_height());
    $cls_design->set_design_folding($cls_template->get_folding_type());
    $cls_design->set_design_direction($cls_template->get_folding_direction());
    $cls_design->set_design_die_cut($cls_template->get_die_cut_type());
    $cls_design->set_stock_cost($v_total_image_cost);
    $cls_design->set_markup_cost($cls_template->get_markup_cost());
    $cls_design->set_print_cost($cls_template->get_print_cost());
    $cls_design->set_company_id(isset($arr_image_user['company_id'])?$arr_image_user['company_id']:0);
    $cls_design->set_location_id(isset($arr_image_user['location_id'])?$arr_image_user['location_id']:0);
    $cls_design->set_user_name(isset($arr_image_user['user_name'])?$arr_image_user['user_name']:'');
    $cls_design->set_saved_dir($cls_template->get_saved_dir());
    $cls_design->set_design_bleed($cls_template->get_template_bleed());
    $cls_design->set_design_dpi($cls_template->get_template_dpi());
    $cls_design->set_site_id(isset($v_site_id)?$v_site_id:0);
    $cls_design->set_old_design_id(0);
    $v_design_id = $cls_design->insert();

}



$arr_data = array('design_id'=>$v_design_id);


$arr_return = array(
    'success'=>$v_success
    ,'message'=>$v_message
    ,'data'=>$arr_data
);

$cls_output->output($arr_return);