<?php
if(!isset($v_sval)) die();

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';
$arr_design_user = isset($_POST['user'])?$_POST['user']:'';

settype($v_design_id, 'int');
if($v_design_id<0) $v_design_id = 0;

if(!is_array($arr_design_user)){
    if($arr_design_user!=''){
        $arr_design_user = stripslashes($arr_design_user);
        $arr_design_user = json_decode($arr_design_user, true);
    }
}
if(!is_array($arr_design_user)) $arr_design_user = array();
if(!isset($arr_design_user['user_id']))
    $arr_design_user['user_id'] = 0;
else
    $arr_design_user['user_id'] = (int) $arr_design_user['user_id'];
$v_user_id = $arr_design_user['user_id'];
if(!isset($arr_design_user['user_name']))
    $arr_design_user['user_name'] = '';
if(!isset($arr_design_user['company_id']))
    $arr_design_user['company_id'] = 0;
else
    $arr_design_user['company_id'] = (int) $arr_design_user['company_id'];


$arr_where_clause = array('design_id'=>$v_design_id);

$v_design_row = $cls_design->select_one($arr_where_clause);
$arr_data = array('success'=>1, 'message'=>'OK');

$v_success = 1;
$v_message = 'OK';
if($v_design_row==1){
    $v_saved_dir = $cls_design->get_saved_dir();
    $v_sample_image = $cls_design->get_sample_image();
    $v_design_user_id = $cls_design->get_user_id();
    $v_design_site_id = $cls_design->get_site_id();
    $v_design_data = $cls_design->get_design_data();
    if($v_design_site_id==$v_site_id && $v_design_site_id!=0){
        if($v_design_user_id==$v_user_id && $v_design_user_id!=0){
            $v_file = ROOT_DIR.DS.$v_saved_dir.$v_sample_image;

            $v_created_date = date('Y-m-d H:i:s');
            $v_design_name = 'Clone: '.$cls_design->get_design_name();
            //$v_new_design_id = $cls_design->select_next('design_id');

            $v_new_design_id = 0;
            $v_root_dir = ROOT_DIR.DS;
            $v_design_dir = DESIGN_DESIGN_DIR.DS.$v_design_id.'/';

            //if(!file_exists($v_design_dir)) mkdir($v_design_dir);
            //copy($v_file, $v_design_dir.DS.$v_sample_image);
            //$v_design_dir = str_replace($v_root_dir,'',$v_design_dir);
            //$v_design_dir = str_replace("\\",'/',$v_design_dir).'/';


            //$cls_design->set_design_id($v_new_design_id);
            $cls_design->set_design_name($v_design_name);
            $cls_design->set_created_time($v_created_date);
            $cls_design->set_last_modified($v_created_date);
            $cls_design->set_last_used($v_created_date);
            $cls_design->set_sample_image($v_sample_image);
            $cls_design->set_saved_dir($v_design_dir);
            $cls_design->set_design_share(array());


            $v_new_design_id = $cls_design->insert();
            $v_result = $v_new_design_id > 0;

            if(! $v_result){
                $v_success = 0;
                $v_message = 'The new design has not been cloned!';
            }else{
                if(!file_exists($v_design_dir)) mkdir($v_design_dir);
                copy($v_file, $v_design_dir.DS.$v_sample_image);
                $v_design_dir = str_replace($v_root_dir,'',$v_design_dir);
                $v_design_dir = str_replace("\\",'/',$v_design_dir).'/';
                $cls_design->update_field('saved_dir', $v_design_dir, array('design_id'=>$v_new_design_id));

                $arr_data = array(
                    'id'=>$v_new_design_id
                    ,'name'=>$v_design_name
                    ,'image'=>URL.$v_design_dir.$v_sample_image
                    ,'stock'=>$cls_design->get_stock_cost()
                    ,'markup'=>$cls_design->get_markup_cost()
                    ,'print'=>$cls_design->get_print_cost()
                    ,'folding'=>$cls_settings->get_option_name_by_id('folding_type', $cls_design->get_design_folding(),'')
                    ,'direction'=>$cls_settings->get_option_name_by_id('folding_direction', $cls_design->get_design_direction(),'')
                    ,'diecut'=>$cls_settings->get_option_name_by_id('die_cut_type', $cls_design->get_design_die_cut(),'')
                    ,'width'=>$cls_design->get_design_width()
                    ,'height'=>$cls_design->get_design_height()
                    ,'template'=>$cls_design->get_template_id()
                    ,'product_id'=>$cls_design->get_product_id()
                    ,'product'=>$cls_design->get_product()
                );

                $arr_json = json_decode($v_design_data, true);
                $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
                $v_size = sizeof($arr_canvases);
                $arr_image_remove = array();
                for($i=0; $i<$v_size; $i++){
                    $arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
                    $v_image_size = sizeof($arr_images);
                    for($j=0; $j < $v_image_size; $j++){
                        $v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:0;
                        settype($v_image_id, 'int');
                        if($v_image_id > 0){
                            if(!isset($arr_image_remove[$v_image_id])){
                                $arr_image_remove[$v_image_id] = array('template'=>0, 'design'=>1);
                            }
                        }
                    }
                }
                add_class('cls_tb_design_image');
                $cls_image = new cls_tb_design_image($db, LOG_DIR);
                $v_total = $cls_image->update_image_used($arr_image_remove);

            }
        }else{
            $v_success = 0;
            $v_message = 'This design does not belong to you!';
        }
    }else{
        $v_success = 0;
        $v_message = 'This design does not belong to the current site!';
    }
}
$arr_return['success'] = $v_success;
$arr_return['message'] = $v_message;
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);