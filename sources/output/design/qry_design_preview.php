<?php
if(!isset($v_sval)) die();

$v_ext = isset($_POST['extension'])?$_POST['extension']:'';

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';
$v_max_size = isset($_POST['max_size'])?$_POST['max_size']:'0';
$v_refresh = isset($_POST['refresh'])?$_POST['refresh']:'0';
$v_option = isset($_POST['option'])?$_POST['option']:'';
if(!is_array($v_option)){
    if(get_magic_quotes_gpc()) $v_option = stripslashes($v_option);
    $arr_option = json_decode($v_option, true);
    if(!is_array($arr_option)) $arr_option = array();
}else{
    $arr_option = $v_option;
}
settype($v_design_id, 'int');
settype($v_max_size, 'int');
settype($v_refresh, 'int');
if($v_max_size<0) $v_max_size = 0;
$v_refresh = $v_refresh==1;
//REPLACE LABEL
$v_preview_slide = false;
$v_record = isset($_POST['record'])?$_POST['record']:'';
$arr_record = array();
if(is_array($v_record))
    $arr_record = $v_record;
else{
    if($v_record!=''){
        $v_record = stripcslashes($v_record);
        $arr_record = json_decode($v_record, true);
        if(!is_array($arr_record)) $arr_record = array();
    }
}
if(sizeof($arr_record)>0) $v_preview_slide = true;
//END REPLACE LABEL
$v_output_extension = $cls_settings->get_option_name_by_key('output_extension', 'output_extension', 'jpg,png,gif');
$v_default_extension = $cls_settings->get_option_name_by_key('output_extension', 'default_extension', 'jpg');
$v_ext = strpos($v_output_extension.',', $v_ext.',')!==false?$v_ext:$v_default_extension;

add_class('cls_tb_design_design');
add_class('cls_tb_design_image');
add_class('cls_draw');
add_class('cls_instagraph');

$cls_design = new cls_tb_design_design($db, LOG_DIR);
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$cls_draw = new cls_draw();
$cls_draw->set_no_background(isset($arr_option['background']) && ($arr_option['background']==0));
$cls_draw->set_no_image(isset($arr_option['image']) && ($arr_option['image']==0));
$cls_draw->set_no_text(isset($arr_option['text']) && ($arr_option['text']==0));
$cls_draw->set_no_shape(isset($arr_option['shape']) && ($arr_option['shape']==0));
$cls_instagraph = new cls_instagraph();

$v_image = '';
$arr_data = array('image'=>'');
$arr_where_clause = array('design_id'=>$v_design_id, 'design_status'=>0, 'site_id'=>array('$in'=>array(0, $v_site_id)));
$v_row = $cls_design->select_one($arr_where_clause);


if($v_row==1){
    $v_saved_dir = $cls_design->get_saved_dir();
    $v_sample_image = $cls_design->get_sample_image();
    $v_json = $cls_design->get_design_data();
	$v_dpi = $cls_design->get_design_dpi();
    $v_prefix = $v_max_size.'_3d_';

    if($v_preview_slide)
        $v_full_path = DESIGN_TEMP_DIR.DS.date('YmdHis_').$v_sample_image;
    else
        $v_full_path = ROOT_DIR.DS.$v_saved_dir.$v_prefix.$v_sample_image;

    if($v_refresh){
        if(file_exists($v_full_path))
            @unlink($v_full_path);
    }else if(!file_exists($v_full_path)) $v_refresh = true;
    if($v_refresh){
        $arr_json = json_decode($v_json, true);
        if(is_array($arr_json)){
            $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_total_page = count($arr_canvases);
			

            $v_accept = true;
            //if(file_exists($v_full_path)) @unlink($v_full_path);

            $image = new Imagick();
            $v_is_admin = true;
            $v_page = 0;

            if($v_preview_slide){
                $arr_texts = isset($arr_canvases[0]['texts'])?$arr_canvases[0]['texts']:array();
                $v_size_texts = sizeof($arr_texts);
                for($i=0; $i<$v_size_texts; $i++){
                    $v_name = isset($arr_texts[$i]['name'])?$arr_texts[$i]['name']:'';
                    if($v_name!='' && isset($arr_record[$v_name])) $arr_texts[$i]['body'] = $arr_record[$v_name];
                }
                $arr_canvases[0]['texts'] = $arr_texts;
                $arr_json['canvases'] = $arr_canvases;
            }


            $cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
            if($v_accept){
                if($v_preview_slide && $v_max_size>0)
                    $image->thumbnailimage($v_max_size, NULL);

                $image->setimageformat($v_ext);
                $image->writeimage($v_full_path);
                $image->clear();
                $image->destroy();
            }
        }

    }
    if(file_exists($v_full_path)){
        if($v_preview_slide){
            $v_image_url = str_replace(ROOT_DIR.DS,'',$v_full_path);
            $v_image_url = URL.str_replace("\\", '/', $v_image_url);
            $arr_data['image'] = $v_image_url;
        }else
            $arr_data['image'] = URL.$v_saved_dir.$v_prefix.$v_sample_image;
    }


}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);