<?php
if(!isset($v_sval)) die();

$v_company_id = isset($_POST['company_id'])?$_POST['company_id']:'0';
$v_user_id = isset($_POST['user_id'])?$_POST['user_id']:'0';
$v_product_id = isset($_POST['product_id'])?$_POST['product_id']:'0';
$v_offset = isset($_POST['offset'])?$_POST['offset']:'0';
$v_row = isset($_POST['row'])?$_POST['row']:'0';

$v_site_id = $cls_key->select_scalar('site_id', array('site_key'=>$v_key));
if(is_null($v_site_id)) $v_site_id = 0;
settype($v_site_id, 'int');
$v_site_id = 0;

settype($v_offset, 'int');
if($v_offset<0) $v_offset = 0;

settype($v_row, 'int');
if($v_row<0) $v_row = 0;

settype($v_user_id, 'int');
if($v_user_id<0) $v_user_id = 0;

settype($v_company_id, 'int');
if($v_company_id<0) $v_company_id = 0;

settype($v_product_id, 'int');
if($v_product_id<0) $v_product_id = 0;

$arr_where_clause = array();
$arr_where_clause['design_status'] = 0;

if($v_company_id>0)
    $arr_where_clause['company_id'] = $v_company_id;
if($v_site_id>0){
    $arr_where_clause['site_id'] = array('$in'=>array($v_site_id));
}
if($v_user_id>0)
    $arr_where_clause['user_id'] = $v_user_id;
if($v_product_id>0)
    $arr_where_clause['product_id'] = $v_product_id;
if($v_row==0)
    $arr_designs = $cls_design->select($arr_where_clause, array('design_order'=>-1));
else
    $arr_designs = $cls_design->select_limit($v_offset, $v_row, $arr_where_clause, array('design_order'=>-1));

$arr_data = array();
foreach($arr_designs as $arr){
    $v_design_id = isset($arr['design_id'])?$arr['design_id']:0;
    $v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
    $v_product_id = isset($arr['product_id'])?$arr['product_id']:0;
    $v_design_name = isset($arr['design_name'])?$arr['design_name']:'';
    $v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
    $v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
    $v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
    $v_design_width = isset($arr['design_width'])?$arr['design_width']:0;
    $v_design_height = isset($arr['design_height'])?$arr['design_height']:0;
    $v_folding_type = isset($arr['design_folding'])?$arr['design_folding']:0;
    $v_folding_direction = isset($arr['design_direction'])?$arr['design_direction']:0;
    $v_die_cut_type = isset($arr['design_die_cut'])?$arr['design_die_cut']:0;

    $arr_data[] = array(
        'id'=>$v_design_id
        ,'name'=>$v_design_name
        ,'image'=>URL.$v_saved_dir.$v_sample_image
        ,'stock'=>$v_stock_cost
        ,'markup'=>$v_markup_cost
        ,'print'=>$v_print_cost
        ,'folding'=>$v_folding_type
        ,'direction'=>$v_folding_direction
        ,'diecut'=>$v_die_cut_type
        ,'width'=>$v_design_width
        ,'height'=>$v_design_height
        ,'template'=>$v_template_id
        ,'product'=>$v_product_id
    );
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);