<?php
if(!isset($v_sval)) die();

$v_design_id = isset($_POST['design_id'])?$_POST['design_id']:'0';

settype($v_design_id, 'int');
if($v_design_id<0) $v_design_id = 0;

$arr_where_clause = array('design_id'=>$v_design_id);

$v_design_row = $cls_design->select_one($arr_where_clause);
$arr_data = array();

if($v_design_row==1){
    //$v_design_id =  isset($arr['design_id'])?$arr['design_id']:0;
    $v_design_name = $cls_design->get_design_name();// isset($arr['design_name'])?$arr['design_name']:'';
    $v_saved_dir = $cls_design->get_saved_dir() ;//isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_sample_image = $cls_design->get_sample_image();// isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_markup_cost = $cls_design->get_markup_cost();//isset($arr['markup_cost'])?$arr['markup_cost']:0;
    $v_stock_cost = $cls_design->get_stock_cost();//isset($arr['stock_cost'])?$arr['stock_cost']:0;
    $v_print_cost = $cls_design->get_print_cost();//isset($arr['print_cost'])?$arr['print_cost']:0;
    $v_design_width = $cls_design->get_design_width();//isset($arr['design_width'])?$arr['design_width']:0;
    $v_design_height = $cls_design->get_design_height();//isset($arr['design_height'])?$arr['design_height']:0;
    $v_folding_type = $cls_design->get_design_folding();//isset($arr['folding_type'])?$arr['folding_type']:0;
    $v_folding_direction = $cls_design->get_design_direction();//isset($arr['folding_direction'])?$arr['folding_direction']:0;
    $v_die_cut_type = $cls_design->get_design_die_cut();//isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
	$v_template_id = $cls_design->get_template_id();
	$v_theme_id = $cls_design->get_theme_id();
	$v_product_id = $cls_design->get_product_id();
	$arr_product = $cls_design->get_product();
	
    $arr_data = array(
        'id'=>$v_design_id
        ,'name'=>$v_design_name
        ,'image'=>URL.$v_saved_dir.$v_sample_image
        ,'stock'=>$v_stock_cost
        ,'markup'=>$v_markup_cost
        ,'print'=>$v_print_cost
        ,'folding'=>$cls_settings->get_option_name_by_id('folding_type', $v_folding_type,'')
        ,'direction'=>$cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction,'')
        ,'diecut'=>$cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type,'')
        ,'width'=>$v_design_width
        ,'height'=>$v_design_height
		,'template'=>$v_template_id
		,'theme'=>$v_theme_id
		,'product_id'=>$v_product_id
		,'product'=>$arr_product
    );
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);