<?php
if(!isset($v_sval)) die();

$v_company_id = isset($_POST['company_id'])?$_POST['company_id']:'0';
$v_user_id = isset($_POST['user_id'])?$_POST['user_id']:'0';
$v_product_id = isset($_POST['product_id'])?$_POST['product_id']:'0';

$v_site_id = $cls_key->select_scalar('site_id', array('site_key'=>$v_key));
if(is_null($v_site_id)) $v_site_id = 0;
settype($v_site_id, 'int');
$v_site_id = 0;

settype($v_user_id, 'int');
if($v_user_id<0) $v_user_id = 0;

settype($v_company_id, 'int');
if($v_company_id<0) $v_company_id = 0;

settype($v_product_id, 'int');
if($v_product_id<0) $v_product_id = 0;

$arr_where_clause = array();
$arr_where_clause['design_status'] = 0;

if($v_company_id>0)
    $arr_where_clause['company_id'] = $v_company_id;
if($v_site_id>0){
    $arr_where_clause['site_id'] = array('$in'=>array($v_site_id));
}
if($v_user_id>0)
    $arr_where_clause['user_id'] = $v_user_id;
if($v_product_id>0)
    $arr_where_clause['product_id'] = $v_product_id;

$v_count = $cls_design->count($arr_where_clause);

$arr_data = array('count'=>$v_count);

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);