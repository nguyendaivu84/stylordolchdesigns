<?php
if(!isset($v_sval)) die();
$v_api = isset($_GET['api'])?$_GET['api']:'';
$v_key = isset($_REQUEST['license_key'])?$_REQUEST['license_key']:'';

add_class('cls_tb_design_key');
$cls_key = new cls_tb_design_key($db, LOG_DIR);

$arr_check_key = $cls_key->check_key($v_key);
$v_site_id = isset($arr_check_key['site_id'])?$arr_check_key['site_id']:'0';
settype($v_site_id, 'int');

if($v_site_id<=0){
    $arr_return['success'] = 0;
    $arr_return['success'] = isset($arr_check_key['message'])?$arr_check_key['message']:'Unknown Request!!!';
    $v_api = STRANGER_KEY;
}

switch($v_api){
	case 'TEMPLATE':
		include 'template/index.php';
		break;
    case 'THEME':
        include 'theme/index.php';
        break;
	case 'TOKEN':
		include 'token/index.php';
		break;
    case 'DESIGN':
        include 'design/index.php';
        break;
    case 'TEST':
        include 'test/qry_test.php';
        break;
    case 'TOOL':
        include 'tool_ie/index.php';
        break;
    case 'DATA':
        include 'data/index.php';
        break;
    case 'IMAGE':
        include 'image/index.php';
        break;
    default:
        $cls_output->output($arr_return, true, false);
        break;
}