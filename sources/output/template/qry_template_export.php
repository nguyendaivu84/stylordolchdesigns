<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_row_id = isset($_POST['row'])?$_POST['row']:'0';
$v_session_id = isset($_POST['session'])?$_POST['session']:'session';
$v_design_data = isset($_POST['record'])?$_POST['record']:'';
$v_send_data = isset($_POST['send'])?$_POST['send']:'0';
$v_ext = isset($_POST['extension'])?$_POST['extension']:'jpg';
$v_is_background = isset($_POST['background'])?$_POST['background']:0;

settype($v_template_id, 'int');
settype($v_row_id, 'int');
settype($v_send_data, 'int');
if($v_send_data!=1) $v_send_data=0;

$arr_upload = array();
$v_work_dir = DESIGN_TEMP_DIR;
add_class('ManageFile', 'cls_file.php');

add_class('Template', 'xtemplate.class.php');

$v_svg_template_data = 'svg_template_data_cmyk.svg';
$v_svg_template_text = 'svg_template_text_cmyk.svg';

$mf = new ManageFile($v_work_dir, DS);
$v_data_send_file = 'data_send_'.$v_session_id.'.txt';

if($v_send_data==0){
    $v_design_data = '';
    if($mf->read_file($v_data_send_file, $v_work_dir)==1){
        $v_design_data = $mf->get_file_content();
    }
    if($v_design_data!=''){
        $arr_upload = json_decode($v_design_data, true);
    }
    if(!is_array($arr_upload)) $arr_upload = array();
}else{
    if(is_array($v_design_data)){
        $arr_upload = $v_design_data;
    }else{
        if($v_design_data!=''){
            if(get_magic_quotes_gpc()) $v_design_data = stripslashes($v_design_data);
            $arr_upload = json_decode($v_design_data, true);
        }
    }
    $v_design_data = json_encode($arr_upload);
    $mf->set_file_content($v_design_data);
    $mf->write_file($v_data_send_file, $v_work_dir);
}

//$v_session_id = session_id();
$v_finish = 0;
$v_total_row = 0;


$v_success = 0;
$v_message = 'Cannot create zip file';
$arr_data = array(
    'url'=>'',
    'message'=>'',
    'success'=>$v_success,
    'finish'=>$v_finish,
    'total'=>0
);
$v_data_row_file = 'data_row_'.$v_session_id.'.txt';

$v_row = $cls_template->select_one(array('template_id'=>$v_template_id));
if($v_row==1){
    $v_memory_limit = ini_get('memory_limit');
    $v_max_execution_time = ini_get('max_execution_time');
    //ini_set('memory_limit', '128M');
    ini_set('max_execution_time', 600);
    require ('lib/svglib/svglib.php');
    require ('lib/svglib/inkscape.php');
    $v_time = date('YmdHis').'_';
    $arr_files = array();
    add_class('ManageFile', 'cls_file.php');
    $mf = new ManageFile($v_work_dir);


    add_class('cls_tb_design_image');
    add_class('cls_tb_design_font');
    add_class('cls_draw');
    add_class('cls_instagraph');

    $cls_image = new cls_tb_design_image($db, LOG_DIR);
    $cls_font = new cls_tb_design_font($db, LOG_DIR);
    $cls_draw = new cls_draw();
    $cls_instagraph = new cls_instagraph();

    $v_design_data = $cls_template->get_template_data();
    $v_dpi = $cls_template->get_template_dpi();

    $v_dpi=96;

    $v_width_inch = $cls_template->get_template_width();
    $v_height_inch = $cls_template->get_template_height();

    $v_width = ceil($v_width_inch*$v_dpi);
    $v_height = ceil($v_height_inch*$v_dpi);
    $v_total_row = sizeof($arr_upload);
    if($v_total_row>0){
        $v_file_array = '';
        if($mf->read_file($v_data_row_file, $v_work_dir)==1){
            $v_file_array = $mf->get_file_content();
        }

        if($v_file_array!='') $arr_files = json_decode($v_file_array, true);
        if(!is_array($arr_files)) $arr_files = array();
        if($v_row_id >= $v_total_row){
            $v_finish = 1;
        }else{
            $j = $v_row_id;
        //}
        //for($j=0;$j<sizeof($arr_upload);$j++){
            $arr_row = $arr_upload[$j];

            $arr_json = json_decode($v_design_data, true);
            $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_pages = sizeof($arr_canvas);
            if(!in_array($v_pages, array(1,2))) $v_pages = 0;

            for($k=0; $k<$v_pages; $k++){
                $arr_texts = isset($arr_canvas[$k]['texts'])?$arr_canvas[$k]['texts']:array();
                for($i=0; $i<sizeof($arr_texts);$i++){
                    $v_name = isset($arr_texts[$i]['name'])?$arr_texts[$i]['name']:'';
                    if($v_name!=''){
                        if(isset($arr_row[$v_name])){
                            $arr_texts[$i]['body'] = $arr_row[$v_name];
                        }
                    }
                }
                $arr_canvas[$k]['texts'] = $arr_texts;
            }
            $arr_json['canvases'] = $arr_canvas;

            $v_quantity = isset($arr_row['quantity'])?$arr_row['quantity']:1;
            $v_stt = isset($arr_row['stt'])?$arr_row['stt']:1;

            for($k=0; $k<$v_pages; $k++){
                $v_bg_color = isset($arr_canvas[$k]['bg_color'])?$arr_canvas[$k]['bg_color']:'ffffff';
                if(strlen($v_bg_color)!=6) $v_bg_color = 'ffffff';
                $arr_cmyk = $cls_draw->hex2cymk($v_bg_color);
                $v_cmyk = $arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'];
                $v_bg_color = '#'.$v_bg_color;

                $v_side = '';
                if($v_pages==2){
                    $v_side = $k==0?'_SIDE_A':'_SIDE_B';
                }

                $v_file_name = $v_time.substr('000000'.$v_stt, 3).$v_side.'_qty_'.$v_quantity.'.'.$v_ext;
                $v_full_path = $v_work_dir.DS.$v_file_name;

                //$svg = SVGDocument::getInstance();
                //$svg->setWidth($v_width);
                //$svg->setHeight($v_height);
                $tpl = new Template($v_svg_template_data, DESIGN_DATA_DIR);
                $tpl->set('WIDTH_PLACEHOLDER', $v_width);
                $tpl->set('HEIGHT_PLACEHOLDER', $v_height);
                $tpl->set('WIDTH_IN_PLACEHOLDER', $v_width_inch);
                $tpl->set('HEIGHT_IN_PLACEHOLDER', $v_height_inch);
                $tpl->set('BACKGROUND_PLACEHOLDER', $v_bg_color);
                $tpl->set('BACKGROUND_CMYK_PLACEHOLDER', $v_cmyk);
                $svg = new SVGDocument($tpl->output());

                $tpl = new Template($v_svg_template_text, DESIGN_DATA_DIR);
                $svg = $cls_draw->create_svg($svg, $tpl, $cls_image,$cls_font, $cls_instagraph, $arr_json, $v_dpi, $k, true);
                $svg->asXML($v_full_path, true);
                if(file_exists($v_full_path)) $arr_files[] = $v_full_path;

                //Convert from SVG to vector PDF
                if(file_exists($v_full_path)){
                    $ink = new Inkscape($v_full_path);
                    $v_full_path = str_replace($v_ext, 'pdf', $v_full_path);
                    $ink->addParam('export-ignore-filters');
                    //$ink->addParam('export-area-drawing');
                    $ink->addParam('export-text-to-path');
                    $arr_result_export = $ink->export('pdf', $v_full_path);
                    if($arr_result_export['error']===0) $arr_files[] = $v_full_path;
                }
                //End: Convert from SVG to vector PDF
            }
            $mf->set_file_content(json_encode($arr_files));
            $mf->write_file($v_data_row_file, $v_work_dir);
        }
    }else{
        $arr_json = json_decode($v_design_data, true);
        if(!is_array($arr_json)) $arr_json = array();
        $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $v_pages = sizeof($arr_canvas);
        if(!in_array($v_pages, array(1,2))) $v_pages = 0;
        for($k=0; $k<$v_pages; $k++){
            $v_bg_color = isset($arr_canvas[$k]['bg_color'])?$arr_canvas[$k]['bg_color']:'ffffff';
            if(strlen($v_bg_color)!=6) $v_bg_color = 'ffffff';
            $arr_cmyk = $cls_draw->hex2cymk($v_bg_color);
            $v_cmyk = $arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'];
            $v_bg_color = '#'.$v_bg_color;

            $v_side = '';
            if($v_pages==2){
                $v_side = $k==0?'_SIDE_A':'_SIDE_B';
            }

            $v_file_name = $v_time.$v_side.'.'.$v_ext;
            $v_full_path = $v_work_dir.DS.$v_file_name;

            $tpl = new Template($v_svg_template_data, DESIGN_DATA_DIR);
            $tpl->set('WIDTH_PLACEHOLDER', $v_width);
            $tpl->set('HEIGHT_PLACEHOLDER', $v_height);
            $tpl->set('WIDTH_IN_PLACEHOLDER', $v_width_inch);
            $tpl->set('HEIGHT_IN_PLACEHOLDER', $v_height_inch);
            $tpl->set('BACKGROUND_PLACEHOLDER', $v_bg_color);
            $tpl->set('BACKGROUND_CMYK_PLACEHOLDER', $v_cmyk);
            $svg = new SVGDocument($tpl->output());

            $tpl = new Template($v_svg_template_text, DESIGN_DATA_DIR);
            $svg = $cls_draw->create_svg($svg, $tpl, $cls_image, $cls_font, $cls_instagraph, $arr_json, $v_dpi, $k, true);
            $svg->asXML($v_full_path, true);
            if(file_exists($v_full_path)) $arr_files[] = $v_full_path;

            //Convert from SVG to vector PDF
            if(file_exists($v_full_path)){
                $ink = new Inkscape($v_full_path);
                $v_full_path = str_replace($v_ext, 'pdf', $v_full_path);
                $ink->addParam('export-ignore-filters');
                //$ink->addParam('export-area-drawing');
                $ink->addParam('export-text-to-path');
                $arr_result_export = $ink->export('pdf', $v_full_path);
                if($arr_result_export['error']===0) $arr_files[] = $v_full_path;
            }
            //End: Convert from SVG to vector PDF
        }
        $v_finish = 1;
        $v_total_row = 1;
    }

    if($v_finish==1){
        $mf->remove_file($v_data_row_file, $v_work_dir);
        $mf->remove_file($v_data_send_file, $v_work_dir);
        if(sizeof($arr_files)>0){
            $v_zip_file = $v_time.'output.zip';
            $mf->create_zip($arr_files, $v_work_dir.DS.$v_zip_file, true, array('pdf'));

            //$v_url = str_replace(ROOT_DIR.DS,'', $v_work_dir);
            //$v_url = str_replace('\\','/', $v_url);
            $v_url = URL.'download/'.$v_zip_file;

            $v_success = 1;
            $v_message = 'OK';
        }else{
            $v_success = 0;
            $v_message = 'Empty data!';
            $v_url='';
        }
    }else{
        $v_url = '';
        $v_success = 1;
    }
    $arr_data['url'] = $v_url;
    $arr_data['success'] = $v_success;
    $arr_data['message'] = $v_message;
    $arr_data['finish'] = $v_finish;
    $arr_data['total'] = $v_total_row;
    //ini_set('memory_limit', $v_memory_limit);
    ini_set('max_execution_time', $v_max_execution_time);
}


$arr_return = array(
    'success'=>$v_success
    ,'message'=>$v_message
    ,'data'=>$arr_data
);

$cls_output->output($arr_return);