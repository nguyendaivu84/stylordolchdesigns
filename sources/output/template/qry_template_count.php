<?php
if(!isset($v_sval)) die();

$v_filter = isset($_POST['filter'])?$_POST['filter']:'';
$v_company_id = isset($_POST['company_id'])?$_POST['company_id']:'0';
$v_template = isset($_POST['template'])?$_POST['template']:'';


$v_site_id = $cls_key->select_scalar('site_id', array('site_key'=>$v_key));
if(is_null($v_site_id)) $v_site_id = 0;
settype($v_site_id, 'int');

settype($v_company_id, 'int');
if($v_company_id<0) $v_company_id = 0;
if($v_filter!=''){
    if(!is_array($v_filter))
        $arr_filter = json_decode($v_filter, true);
    else
        $arr_filter = $v_filter;
}

if($v_template!=''){
    if(!is_array($v_template))
        $arr_request_template = json_decode($v_template, true);
    else
        $arr_request_template = $v_template;
}
if(!isset($arr_filter) || !is_array($arr_filter)) $arr_filter = array();
if(!isset($arr_request_template) || !is_array($arr_request_template)) $arr_request_template = array();

$arr_category = isset($arr_filter['category'])?$arr_filter['category']:array();
$arr_style = isset($arr_filter['style'])?$arr_filter['style']:array();
$arr_industry = isset($arr_filter['industry'])?$arr_filter['industry']:array();

$arr_where_clause = array();
$arr_where_clause['template_status'] = 0;
if(is_array($arr_category) && sizeof($arr_category)>0){
    for($i=0; $i<sizeof($arr_category);$i++){
        $arr_category[$i] = (int) $arr_category[$i];
    }
    $arr_where_clause['category_id'] = array('$in'=>$arr_category);
}
if(is_array($arr_style) && sizeof($arr_style)>0){
    for($i=0; $i<sizeof($arr_style);$i++){
        $arr_style[$i] = (int) $arr_style[$i];
    }
    $arr_where_clause['style_id'] = array('$in'=>$arr_style);
}
if(is_array($arr_industry) && sizeof($arr_industry)>0){
    for($i=0; $i<sizeof($arr_industry);$i++){
        $arr_industry[$i] = (int) $arr_industry[$i];
    }
    $arr_where_clause['industry_id'] = array('$in'=>$arr_industry);
}
if($v_company_id>0){
    $arr_where_clause['company_id'] = array('$in'=>array(0,$v_company_id));
}
if($v_site_id>0){
    $arr_where_clause['site_id'] = array('$in'=>array(0,$v_site_id));
}

$v_count_request_template = sizeof($arr_request_template);
if($v_count_request_template>0){
    for($i=0; $i<$v_count_request_template; $i++){
        $arr_request_template[$i] = (int) $arr_request_template[$i];
    }
    $arr_where_clause['template_id'] = array('$in'=>$arr_request_template);
}

$v_count = $arr_templates = $cls_template->count($arr_where_clause);

$arr_data = array('count'=>$v_count);

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);