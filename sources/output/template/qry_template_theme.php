<?php
if(!isset($v_sval)) die();

$v_filter = isset($_POST['filter'])?$_POST['filter']:'';
$v_company_id = isset($_POST['company_id'])?$_POST['company_id']:'0';
$v_template = isset($_POST['template'])?$_POST['template']:'';
settype($v_company_id, 'int');
if($v_company_id<0) $v_company_id = 0;

$v_site_id = $cls_key->select_scalar('site_id', array('site_key'=>$v_key));
if(is_null($v_site_id)) $v_site_id = 0;
settype($v_site_id, 'int');

if($v_filter!=''){
    if(!is_array($v_filter))
        $arr_filter = json_decode($v_filter, true);
    else
        $arr_filter = $v_filter;
}

if($v_template!=''){
    if(!is_array($v_template))
        $arr_request_template = json_decode($v_template, true);
    else
        $arr_request_template = $v_template;
}
if(!isset($arr_request_template) || !is_array($arr_request_template)) $arr_request_template = array();
if(!isset($arr_filter) || !is_array($arr_filter)) $arr_filter = array();

$arr_category = isset($arr_filter['category'])?$arr_filter['category']:array();
$arr_style = isset($arr_filter['style'])?$arr_filter['style']:array();
$arr_industry = isset($arr_filter['industry'])?$arr_filter['industry']:array();

$arr_where_clause = array();
$arr_where_clause['template_status'] = 0;
if(is_array($arr_category) && sizeof($arr_category)>0){
    for($i=0; $i<sizeof($arr_category);$i++){
        $arr_category[$i] = (int) $arr_category[$i];
    }
    $arr_where_clause['category_id'] = array('$in'=>$arr_category);
}
if(is_array($arr_style) && sizeof($arr_style)>0){
    for($i=0; $i<sizeof($arr_style);$i++){
        $arr_style[$i] = (int) $arr_style[$i];
    }
    $arr_where_clause['style_id'] = array('$in'=>$arr_style);
}
if(is_array($arr_industry) && sizeof($arr_industry)>0){
    for($i=0; $i<sizeof($arr_industry);$i++){
        $arr_industry[$i] = (int) $arr_industry[$i];
    }
    $arr_where_clause['industry_id'] = array('$in'=>$arr_industry);
}

if($v_company_id>0){
    $arr_where_clause['company_id'] = array('$in'=>array(0,$v_company_id));
}
if($v_site_id>0){
    $arr_where_clause['site_id'] = array('$in'=>array(0,$v_site_id));
}

$v_count_request_template = sizeof($arr_request_template);
if($v_count_request_template>0){
    for($i=0; $i<$v_count_request_template; $i++){
        $arr_request_template[$i] = (int) $arr_request_template[$i];
    }
    $arr_where_clause['template_id'] = array('$in'=>$arr_request_template);
}


add_class('cls_tb_design_theme');
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);

$arr_templates = $cls_template->select($arr_where_clause, array('template_order'=>1));
$arr_data = array();
$arr_folding = array();
$arr_direction = array();
$arr_die_cut = array();

foreach($arr_templates as $arr){
    $v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
    $v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
    $v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_markup_cost = isset($arr['markup_cost'])?$arr['markup_cost']:0;
    $v_stock_cost = isset($arr['stock_cost'])?$arr['stock_cost']:0;
    $v_print_cost = isset($arr['print_cost'])?$arr['print_cost']:0;
    $v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
    $v_template_height = isset($arr['template_height'])?$arr['template_height']:0;
    $v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
    $v_folding_direction = isset($arr['folding_direction'])?$arr['folding_direction']:0;
    $v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;

    if(!isset($arr_folding[$v_folding_type])) $arr_folding[$v_folding_type] = $cls_settings->get_option_name_by_id('folding_type', $v_folding_type,'');
    if(!isset($arr_direction[$v_folding_direction])) $arr_direction[$v_folding_direction] = $cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction,'');
    if(!isset($arr_die_cut[$v_die_cut_type])) $arr_die_cut[$v_die_cut_type] = $cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type,'');

    $arr_themes = $cls_theme->select(array('theme_status'=>0, 'template_id'=>$v_template_id), array('theme_order'=>1));
    $arr_theme = array();
    foreach($arr_themes as $a){
        $v_theme_id = $a['theme_id'];
        $v_theme_name = $a['theme_name'];
        $v_theme_color = $a['theme_color'];
        $v_saved_d = $a['saved_dir'];
        $v_sample_i = $a['sample_image'];
        $arr_theme[] = array(
            'id'=>$v_theme_id
            ,'name'=>$v_theme_name
            ,'color'=>$v_theme_color
            ,'image'=>URL.$v_saved_d.$v_sample_i
        );
    }
    $arr_data[] = array(
        'id'=>$v_template_id
        ,'name'=>$v_template_name
        ,'image'=>URL.$v_saved_dir.$v_sample_image
        ,'stock'=>$v_stock_cost
        ,'markup'=>$v_markup_cost
        ,'print'=>$v_print_cost
        ,'folding'=>$arr_folding[$v_folding_type]
        ,'direction'=>$arr_direction[$v_folding_direction]
        ,'diecut'=>$arr_die_cut[$v_die_cut_type]
        ,'width'=>$v_template_width
        ,'height'=>$v_template_height
        ,'theme'=>$arr_theme
    );
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);