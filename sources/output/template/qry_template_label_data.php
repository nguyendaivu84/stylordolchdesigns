<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';

settype($v_template_id, 'int');
if($v_template_id<0) $v_template_id = 0;

$arr_where_clause = array('template_id'=>$v_template_id);

$v_template_row = $cls_template->select_one($arr_where_clause);
$arr_data = array();

if($v_template_row==1){
    $v_template_data = $cls_template->get_template_data();// isset($arr['template_name'])?$arr['template_name']:'';

    $arr_json = json_decode($v_template_data, true);
    if(!is_array($arr_json)) $arr_json = array();
    $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
    $v_size_canvases = sizeof($arr_canvases);

    for($i=0; $i<$v_size_canvases;$i++){
        $arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();

        $v_size_texts = sizeof($arr_texts);
        for($j=0; $j<$v_size_texts;$j++){
            $v_name = isset($arr_texts[$j]['name'])?$arr_texts[$j]['name']:'';
            $v_preset = isset($arr_texts[$j]['preset_text'])?$arr_texts[$j]['preset_text']:'';
            $v_title = isset($arr_texts[$j]['title'])?$arr_texts[$j]['title']:'';
            $v_body = isset($arr_texts[$j]['body'])?$arr_texts[$j]['body']:'';
            if($v_name!='' && !isset($arr_data[$v_name])){
                $arr_data[$v_name] = array('title'=>$v_title, 'preset'=>$v_preset, 'body'=>$v_body);
            }
        }
    }
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);