<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_max_size = isset($_POST['max_size'])?$_POST['max_size']:'500';
$v_refresh = isset($_POST['refresh'])?$_POST['refresh']:'0';
$v_option = isset($_POST['option'])?$_POST['option']:'';
if(!is_array($v_option)){
    if(get_magic_quotes_gpc()) $v_option = stripslashes($v_option);
    $arr_option = json_decode($v_option, true);
    if(!is_array($arr_option)) $arr_option = array();
}else{
    $arr_option = $v_option;
}
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_max_size, 'int');
if($v_max_size<0) $v_max_size = 0;
settype($v_refresh, 'int');
$v_refresh = $v_refresh==1;

add_class('cls_tb_design_template');
add_class('cls_tb_design_image');
add_class('cls_draw');
add_class('cls_instagraph');

$cls_template = new cls_tb_design_template($db, LOG_DIR);
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$cls_draw = new cls_draw();
$cls_draw->set_no_background(isset($arr_option['background']) && ($arr_option['background']==0));
$cls_draw->set_no_image(isset($arr_option['image']) && ($arr_option['image']==0));
$cls_draw->set_no_text(isset($arr_option['text']) && ($arr_option['text']==0));
$cls_draw->set_no_shape(isset($arr_option['shape']) && ($arr_option['shape']==0));
$cls_instagraph = new cls_instagraph();

$v_image = '';
$arr_data = array('image'=>'');
$arr_where_clause = array('template_id'=>$v_template_id, 'template_status'=>0, 'site_id'=>array('$in'=>array(0, $v_site_id)));
$v_row = $cls_template->select_one($arr_where_clause);


if($v_row==1){
    $v_saved_dir = $cls_template->get_saved_dir();
    $v_sample_image = $cls_template->get_sample_image();
    $v_json = $cls_template->get_template_data();
	$v_dpi = $cls_template->get_template_dpi();
    $v_prefix = $v_max_size.'_3d_';

    $v_full_path = ROOT_DIR.DS.$v_saved_dir.$v_prefix.$v_sample_image;
    if(!file_exists($v_full_path)){
        $arr_json = json_decode($v_json, true);
        if(is_array($arr_json)){
            $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_total_page = count($arr_canvases);
			

            $v_accept = true;
            //if(file_exists($v_full_path)) @unlink($v_full_path);

            $image = new Imagick();
            $v_is_admin = true;
            $v_page = 0;
            $cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
            if($v_accept){

                $image = $cls_draw->preview_3d_object($image, $v_max_size);

                $image->setimageformat('png');
                $image->writeimage($v_full_path);
                $image->clear();
                $image->destroy();
            }
        }

    }
    if(file_exists($v_full_path)){
        $arr_data['image'] = URL.$v_saved_dir.$v_prefix.$v_sample_image;
    }


}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);