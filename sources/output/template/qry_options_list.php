<?php
if(!isset($v_sval)) die();

$v_selected = isset($_POST['selected'])?$_POST['selected']:'';
if($v_selected!=''){
    if(!is_array($v_selected))
        $arr_selected = json_decode($v_selected, true);
    else
        $arr_selected = $v_selected;
}
if(!isset($arr_selected) || !is_array($arr_selected)) $arr_selected = array();

$arr_category = isset($arr_selected['category'])?$arr_selected['category']:array();
$arr_style = isset($arr_selected['style'])?$arr_selected['style']:array();
$arr_industry = isset($arr_selected['industry'])?$arr_selected['industry']:array();

$arr_where_clause = array();
if(is_array($arr_category) && sizeof($arr_category)>0){
    for($i=0; $i<sizeof($arr_category);$i++){
        $arr_category[$i] = (int) $arr_category[$i];
    }
}else{
    $arr_category = array();
}
    $arr_where_clause = array('setting_name'=>'template_category');

    $arr_option = $cls_settings->select_scalar('option', $arr_where_clause);

    $arr_tmp_category = array();
    for($i=0; $i<count($arr_option);$i++){
        if($arr_option[$i]['status']==0){
            $arr_tmp_category[] = array(
                'id'=>$arr_option[$i]['id']
                ,'name'=>$arr_option[$i]['name']
                ,'key'=>$arr_option[$i]['key']
                ,'selected'=>in_array($arr_option[$i]['id'], $arr_category)
            );
        }
    }

if(is_array($arr_style) && sizeof($arr_style)>0){
    for($i=0; $i<sizeof($arr_style);$i++){
        $arr_style[$i] = (int) $arr_style[$i];
    }
}else{
    $arr_style = array();
}
    $arr_where_clause = array('setting_name'=>'template_style');
    $arr_option = $cls_settings->select_scalar('option', $arr_where_clause);
    $arr_tmp_style = array();
    for($i=0; $i<count($arr_option);$i++){
        if($arr_option[$i]['status']==0){
            $arr_tmp_style[] = array(
                'id'=>$arr_option[$i]['id']
                ,'name'=>$arr_option[$i]['name']
                ,'key'=>$arr_option[$i]['key']
                ,'selected'=>in_array($arr_option[$i]['id'], $arr_style)
            );
        }
    }

if(is_array($arr_industry) && sizeof($arr_industry)>0){
    for($i=0; $i<sizeof($arr_industry);$i++){
        $arr_industry[$i] = (int) $arr_industry[$i];
    }
    $arr_where_clause['industry_id'] = array('$in'=>$arr_industry);
}else{
    $arr_industry = array();
}

$arr_where_clause = array('setting_name'=>'template_industry');
$arr_option = $cls_settings->select_scalar('option', $arr_where_clause);
$arr_tmp_industry = array();
for($i=0; $i<count($arr_option);$i++){
    if($arr_option[$i]['status']==0){
        $arr_tmp_industry[] = array(
            'id'=>$arr_option[$i]['id']
            ,'name'=>$arr_option[$i]['name']
            ,'key'=>$arr_option[$i]['key']
            ,'selected'=>in_array($arr_option[$i]['id'], $arr_industry)
        );
    }
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = array(
    'category'=>$arr_tmp_category
    ,'style'=>$arr_tmp_style
    ,'industry'=>$arr_tmp_industry
);

$cls_output->output($arr_return,true, false);