<?php
if(!isset($v_sval)) die();

$v_selected = isset($_POST['selected'])?$_POST['selected']:'';
if($v_selected!=''){
    if(!is_array($v_selected))
        $arr_selected = json_decode($v_selected, true);
    else
        $arr_selected = $v_selected;
}
if(!isset($arr_selected) || !is_array($arr_selected)) $arr_selected = array();

$arr_style = isset($arr_selected['style'])?$arr_selected['style']:array();

$arr_where_clause = array();
if(is_array($arr_style) && sizeof($arr_style)>0){
    for($i=0; $i<sizeof($arr_style);$i++){
        $arr_style[$i] = (int) $arr_style[$i];
    }
}else{
    $arr_style = array();
}

    $arr_where_clause = array('setting_name'=>'template_style');

    $arr_option = $cls_settings->select_scalar('option', $arr_where_clause);
    if(is_array($arr_option)) $arr_option = $cls_settings->sort_by($arr_option);
    $arr_tmp_style = array();
    for($i=0; $i<count($arr_option);$i++){
        if($arr_option[$i]['status']==0){
            $arr_tmp_style[] = array(
                'id'=>$arr_option[$i]['id']
                ,'name'=>$arr_option[$i]['name']
                ,'key'=>$arr_option[$i]['key']
                ,'selected'=>in_array($arr_option[$i]['id'], $arr_style)
            );
        }
    }


$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_tmp_style;

$cls_output->output($arr_return, true, false);