<?php
if(!isset($v_sval)) die();
add_class('cls_tb_design_font');

$cls_font = new cls_tb_design_font($db, LOG_DIR);

$arr_where = array();
$arr_where['font_status'] = 0;

$arr_or1 = array('font_embed'=>1, 'site_id'=>array('$in'=>array($v_site_id)));
$arr_or2 = array('font_embed'=>0);

$arr_where['$or'] = array($arr_or1, $arr_or2);

//$arr_fonts = $cls_font->select(array('font_status'=>0), array('font_order'=>1));
$arr_fonts = $cls_font->select($arr_where, array('font_name'=>1));
$arr_font = array();
foreach($arr_fonts as $arr){
    $v_embed = isset($arr['font_embed']) && ($arr['font_embed']==1);
    $arr_font[$arr['font_name']] = array('bold'=>$arr['font_bold']==1, 'italic'=>$arr['font_italic']==1, 'embed'=>$v_embed);
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_font;

$cls_output->output($arr_return, true, false);