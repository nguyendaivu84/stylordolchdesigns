<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';

settype($v_template_id, 'int');
if($v_template_id<0) $v_template_id = 0;

$arr_where_clause = array('template_id'=>$v_template_id);

$v_template_row = $cls_template->select_one($arr_where_clause);
$arr_data = array();

if($v_template_row==1){
    //$v_template_id =  isset($arr['template_id'])?$arr['template_id']:0;
    $v_template_name = $cls_template->get_template_name();// isset($arr['template_name'])?$arr['template_name']:'';
    $v_saved_dir = $cls_template->get_saved_dir() ;//isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_sample_image = $cls_template->get_sample_image();// isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_markup_cost = $cls_template->get_markup_cost();//isset($arr['markup_cost'])?$arr['markup_cost']:0;
    $v_stock_cost = $cls_template->get_stock_cost();//isset($arr['stock_cost'])?$arr['stock_cost']:0;
    $v_print_cost = $cls_template->get_print_cost();//isset($arr['print_cost'])?$arr['print_cost']:0;
    $v_template_width = $cls_template->get_template_width();//isset($arr['template_width'])?$arr['template_width']:0;
    $v_template_height = $cls_template->get_template_height();//isset($arr['template_height'])?$arr['template_height']:0;
    $v_folding_type = $cls_template->get_folding_type();//isset($arr['folding_type'])?$arr['folding_type']:0;
    $v_folding_direction = $cls_template->get_folding_direction();//isset($arr['folding_direction'])?$arr['folding_direction']:0;
    $v_die_cut_type = $cls_template->get_die_cut_type();//isset($arr['die_cut_type'])?$arr['die_cut_type']:0;

    $arr_data = array(
        'id'=>$v_template_id
        ,'name'=>$v_template_name
        ,'image'=>URL.$v_saved_dir.$v_sample_image
        ,'stock'=>$v_stock_cost
        ,'markup'=>$v_markup_cost
        ,'print'=>$v_print_cost
        ,'folding'=>$cls_settings->get_option_name_by_id('folding_type', $v_folding_type,'')
        ,'direction'=>$cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction,'')
        ,'diecut'=>$cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type,'')
        ,'width'=>$v_template_width
        ,'height'=>$v_template_height
    );
}

$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;
$cls_output->output($arr_return, true, false);