<?php
if(!isset($v_sval)) die();

add_class('cls_tb_design_template');
$cls_template = new cls_tb_design_template($db, LOG_DIR);
$v_template = isset($_GET['template'])?$_GET['template']:'';

if($v_site_id>0){
    $arr_switch = array(
        'LIST'=>'List All Templates'
        ,'INFO'=>'Get Template\'s Info'
        ,'COUNT'=>'Count All Templates'
        ,'THEME'=>'Get Template\' Themes'
        ,'DATA'=>'Get Template\' data'
        ,'CATEGORY'=>'Get Template\' Categories'
        ,'STYLE'=>'Get Template\' Styles'
        ,'INDUSTRY'=>'Get Template\' Industries'
        ,'FONT'=>'Get Template\' Fonts'
        ,'OPTIONS'=>'Get Template\' Options'
        ,'PREVIEW'=>'Design\'s Preview'
        ,'PREVIEW3D'=>'Design\'s Preview3D'
        ,'LABEL'=>'Get Template Text Label'
        ,'TEXT'=>'Get Template Text Data'
        ,'ZIP'=>'Zip Template'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot()
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_template])?$arr_switch[$v_template]:'Unknown', get_real_ip_address(), $arr_device);
}


switch($v_template){
	case 'LIST':
        include 'qry_template_list.php';
		break;
    case 'COUNT':
        include 'qry_template_count.php';
        break;
    case 'INFO':
        include 'qry_template_info.php';
        break;
	case 'THEME':
        include 'qry_template_theme.php';
		break;
	case 'DATA':
        include 'qry_template_data.php';
		break;
    case 'CATEGORY':
        include 'qry_category_list.php';
        break;
    case 'STYLE':
        include 'qry_style_list.php';
        break;
    case 'FONT':
        include 'qry_font_list.php';
        break;
    case 'INDUSTRY':
        include 'qry_industry_list.php';
        break;
    case 'OPTIONS':
        include 'qry_options_list.php';
        break;
    case 'PREVIEW':
        include 'qry_template_preview.php';
        break;
    case 'PREVIEW3D':
        include 'qry_template_preview3d.php';
        break;
    case 'LABEL':
        include 'qry_template_label.php';
        break;
    case 'TEXT':
        include 'qry_template_label_data.php';
        break;
    case 'ZIP':
        include 'qry_template_zip.php';
        break;
	default:
        $cls_output->output($arr_return);
		break;
}