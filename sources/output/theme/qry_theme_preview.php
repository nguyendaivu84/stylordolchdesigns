<?php
if(!isset($v_sval)) die();

$v_template_id = isset($_POST['template_id'])?$_POST['template_id']:'0';
$v_theme_id = isset($_POST['theme_id'])?$_POST['theme_id']:'0';
$v_max_size = isset($_POST['max_size'])?$_POST['max_size']:'500';
$v_refresh = isset($_POST['refresh'])?$_POST['refresh']:'0';
$v_option = isset($_POST['option'])?$_POST['option']:'';
if(!is_array($v_option)){
    if(get_magic_quotes_gpc()) $v_option = stripslashes($v_option);
    $arr_option = json_decode($v_option, true);
    if(!is_array($arr_option)) $arr_option = array();
}else{
    $arr_option = $v_option;
}
settype($v_template_id, 'int');
settype($v_theme_id, 'int');
settype($v_max_size, 'int');
if($v_max_size < 0) $v_max_size = 0;
settype($v_refresh, 'int');
$v_refresh = $v_refresh==1;

add_class('cls_tb_design_template');
add_class('cls_tb_design_theme');
add_class('cls_tb_design_image');
add_class('cls_draw');
add_class('cls_instagraph');

$cls_template = new cls_tb_design_template($db, LOG_DIR);
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$cls_draw = new cls_draw();
$cls_draw->set_no_background(isset($arr_option['background']) && ($arr_option['background']==0));
$cls_draw->set_no_image(isset($arr_option['image']) && ($arr_option['image']==0));
$cls_draw->set_no_text(isset($arr_option['text']) && ($arr_option['text']==0));
$cls_draw->set_no_shape(isset($arr_option['shape']) && ($arr_option['shape']==0));
$cls_instagraph = new cls_instagraph();

$v_image = '';
$arr_data = array('image'=>'');
$arr_where_clause = array('template_id'=>$v_template_id, 'template_status'=>0, 'site_id'=>array('$in'=>array(0, $v_site_id)));
$v_row = $cls_template->select_one($arr_where_clause);
//$v_theme_id = 0;

if($v_row==1){
    $v_saved_dir = $cls_template->get_saved_dir();
    $v_sample_image = $cls_template->get_sample_image();
    $v_json = $cls_template->get_template_data();
	$v_dpi = $cls_template->get_template_dpi();
    $v_prefix = $v_max_size.'_3d_';

	$arr_theme_color = array();
	if($v_theme_id>0){
		//$arr_where_clause = array('template_id'=>$v_template_id, 'theme_name'=>$v_theme_name);
		$arr_where_clause = array('template_id'=>$v_template_id, 'theme_id'=>$v_theme_id);
		$v_row = $cls_theme->select_one($arr_where_clause);
		if($v_row==1){
			$v_saved_dir = $cls_theme->get_saved_dir();
			$v_sample_image = $cls_theme->get_sample_image();
			$v_theme_id = $cls_theme->get_theme_id();
			$arr_theme_color = $cls_theme->get_list_color();
			if(!isset($arr_theme_color) || !is_array($arr_theme_color)) $arr_theme_color = array();
		}
	}
	
    $v_full_path = ROOT_DIR.DS.$v_saved_dir.$v_prefix.$v_sample_image;

    if($v_refresh){
        if(file_exists($v_full_path)) @unlink($v_full_path);
    }else if(!file_exists($v_full_path)) $v_refresh = true;
    if($v_refresh){
        $arr_json = json_decode($v_json, true);
        if(is_array($arr_json)){
            $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
            $v_total_page = count($arr_canvases);
			
			if(count($arr_theme_color)>0){
				for($i=0; $i<count($arr_canvases);$i++){
	
	
					$arr_images = isset($arr_canvases[$i]['images'])?$arr_canvases[$i]['images']:array();
					for($j=0;$j<count($arr_images);$j++){
						$v_image_id = isset($arr_images[$j]['image_id'])?$arr_images[$j]['image_id']:'0';
						settype($v_image_id, 'int');
						if($v_image_id>0){
	
							$v_key = 'name_'.$arr_images[$j]['name'];
							if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
								$arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'];
							}
	
						}else{
							$v_key = 'fill_'.$arr_images[$j]['name'];
							if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
								$arr_images[$j]['fill_color'] = $arr_theme_color[$v_key]['color'];
							}
							$v_key = 'border_'.$arr_images[$j]['name'];
							if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='image'){
								$arr_images[$j]['border_color'] = $arr_theme_color[$v_key]['color'];
							}
						}
					}
					$arr_canvases[$i]['images'] = $arr_images;
	
					$arr_texts = isset($arr_canvases[$i]['texts'])?$arr_canvases[$i]['texts']:array();
					for($j=0;$j<count($arr_texts);$j++){
						$v_key = 'text_'.$arr_texts[$j]['name'];
						if(isset($arr_theme_color[$v_key]) && isset($arr_theme_color[$v_key]['type']) && $arr_theme_color[$v_key]['type']=='text'){
							$arr_texts[$j]['color'] = $arr_theme_color[$v_key]['color'];
						}
					}
					$arr_canvases[$i]['texts'] = $arr_texts;
				}
				$arr_json['canvases'] = $arr_canvases;
			}


            $v_accept = true;
            //if(file_exists($v_full_path)) @unlink($v_full_path);

            $image = new Imagick();
            $v_is_admin = true;
            $v_page = 0;
            $cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
            if($v_accept){

                $image->setimageformat('png');
                $image->writeimage($v_full_path);
                $image->clear();
                $image->destroy();
            }
        }

    }
    if(file_exists($v_full_path)){
        $arr_data['image'] = URL.$v_saved_dir.$v_prefix.$v_sample_image;
    }


}
$arr_data['theme'] = $v_theme_id;
$arr_return['success'] = 1;
$arr_return['message'] = '';
$arr_return['data'] = $arr_data;

$cls_output->output($arr_return, true, false);