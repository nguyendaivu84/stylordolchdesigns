<?php
if(!isset($v_sval)) die();

$v_theme = isset($_GET['theme'])?$_GET['theme']:'';

if($v_site_id>0){
    $arr_switch = array(
        'PREVIEW'=>'Theme\'s Preview'
        ,'PREVIEW3D'=>'Theme\'s Preview3D'
    );
    add_class('cls_tb_site_log');
    $cls_site_log = new cls_tb_site_log($db, LOG_DIR);
    $arr_device = array(
        "platform"=>$browser->getPlatform(),
        "browser"=>$browser->getBrowser(),
        "version"=>$browser->getVersion(),
        "agent"=>$browser->getUserAgent(),
        "mobile"=>$browser->isMobile(),
        "tablet"=>$browser->isTablet(),
        "robot"=> $browser->isRobot()
    );
    $cls_site_log->save_log($v_site_id, isset($arr_switch[$v_theme])?$arr_switch[$v_theme]:'Unknown', get_real_ip_address(), $arr_device);
}

switch($v_theme){
    case 'PREVIEW':
        include 'qry_theme_preview.php';
        break;
    case 'PREVIEW3D':
        include 'qry_theme_preview3d.php';
        break;
	default:
        $cls_output->output($arr_return);
		break;
}