<?php
if(!isset($v_sval)) die();

$v_page = isset($_REQUEST['page'])?$_REQUEST['page']:1;
settype($v_page, 'int');
if($v_page<=0) $v_page = 1;
$arr_where_clause = array('template_status'=>0);

add_class('cls_tb_user');
add_class('cls_tb_contact');
add_class('cls_tb_design_template');
add_class('cls_tb_design_theme');
add_class('cls_tb_design_design');
add_class('cls_tb_design_key');

$cls_template = new cls_tb_design_template($db, LOG_DIR);
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);
$cls_design = new cls_tb_design_design($db, LOG_DIR);
$cls_key = new cls_tb_design_key($db, LOG_DIR);
$cls_user = new cls_tb_user($db, LOG_DIR);
$cls_contact = new cls_tb_contact($db, LOG_DIR);

$v_total_rows = $cls_template->count($arr_where_clause);
$v_rows = 100;
$v_total_page = ceil($v_total_rows/$v_rows);
if($v_page>$v_total_page) $v_page = $v_total_page;
$v_offset = ($v_page-1)*$v_rows;


$arr_fields = array('template_id', 'template_name', 'template_width', 'template_height', 'template_desc', 'category_id', 'created_time',
'sample_image', 'saved_dir', 'folding_type', 'folding_direction', 'die_cut_type', 'user_id', 'user_name', 'site_id');

$arr_template_category = array();
$arr_folding_type = array();
$arr_folding_direction = array();
$arr_die_cut_type = array();
$arr_data = array();
$arr_users = array();
$arr_templates = array();
$arr_sites = array();
$arr_template = $cls_template->select_limit_fields($v_offset, $v_rows, $arr_fields, $arr_where_clause);
foreach($arr_template as $arr){
    $v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
    $v_template_width = isset($arr['template_width'])?$arr['template_width']:0;
    $v_template_height = isset($arr['template_height'])?$arr['template_height']:0;
    $v_template_name = isset($arr['template_name'])?$arr['template_name']:'';
    $v_template_desc = isset($arr['template_desc'])?$arr['template_desc']:'';
    $v_category_id = isset($arr['category_id'])?$arr['category_id']:0;
    $v_created_time = isset($arr['created_time'])?$arr['created_time']:NULL;
    $v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
    $v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
    $v_folding_type = isset($arr['folding_type'])?$arr['folding_type']:0;
    $v_folding_direction = isset($arr['folding_direction'])?$arr['folding_direction']:0;
    $v_die_cut_type = isset($arr['die_cut_type'])?$arr['die_cut_type']:0;
    $v_assigned_to = isset($arr['user_id'])?$arr['user_id']:0;
    $arr_site_id = isset($arr['site_id'])?$arr['site_id']:array();
    $v_assigned_name = isset($arr['user_name'])?$arr['user_name']:'';

    if(!is_array($arr_site_id)) $arr_site_id = array();

    if(!is_null($v_created_time))
        $v_created_time = $v_created_time->sec;
    else
        $v_created_time = time();
    if(!isset($arr_template_category[$v_category_id])) $arr_template_category[$v_category_id] = $cls_settings->get_option_name_by_id('template_category', $v_category_id);
    if(!isset($arr_folding_type[$v_folding_type])) $arr_folding_type[$v_folding_type] = $cls_settings->get_option_name_by_id('folding_type', $v_folding_type);
    if(!isset($arr_folding_direction[$v_folding_direction])) $arr_folding_direction[$v_folding_direction] = $cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction);
    if(!isset($arr_die_cut_type[$v_die_cut_type])) $arr_die_cut_type[$v_die_cut_type] = $cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type);

    /*
    if(count($arr_site_id)>0){
        for($i=0;$i<count($arr_site_id);$i++){
            if(!isset($arr_sites[$v_site_id])){
                if($v_site_id>0)
                    $arr_sites[$v_site_id] = $cls_key->select_scalar('site_name', array('site_id'=>$v_site_id));
            }
        }
    }else{
        $arr_sites[0] = 'All Sites';
    }
    */
    $v_template_desc .= ' - Size: '.$v_template_width.'" &times; '.$v_template_height.'" - Folding: '.$arr_folding_type[$v_folding_type].' '.$arr_folding_direction[$v_folding_direction].' - Die Cut: '.$arr_die_cut_type[$v_die_cut_type];
    $arr_images = array();
    $arr_images[] = array(
        'size'=>1
        ,'url'=>URL.$v_saved_dir.$v_sample_image
    );

    $arr_theme = $cls_theme->select(array('template_id'=>$v_template_id, 'theme_status'=>0));
    foreach($arr_theme as $a){
        $arr_images[] = array(
            'size'=>1
            ,'url'=>URL.$a['saved_dir'].$a['sample_image']
        );
    }

    $arr_user = array(
        'username'=>$v_assigned_name
        ,'affection'=>0
        ,'city'=>''
        ,'country'=>''
        ,'firstname'=>$v_assigned_name
        ,'followers_count'=>0
        ,'fullname'=>$v_assigned_name
        ,'id'=>$v_assigned_to
        ,'upgrade_status'=>0
        ,'userpic_url'=>''
    );
    if(!isset($arr_users[$v_assigned_to])){
        $v_contact_id = $cls_user->select_scalar('contact_id',array('user_id'=>$v_assigned_to));
        settype($v_contact_id, 'int');
        if($v_contact_id>0){
            $v_row = $cls_contact->select_one(array('contact_id'=>$v_contact_id));
            if($v_row==1){
                $arr_user = array(
                    'username'=>$v_assigned_name
                    ,'affection'=>0
                    ,'city'=>$cls_contact->get_address_city()
                    ,'country'=>$cls_contact->get_address_country()
                    ,'firstname'=>$cls_contact->get_first_name()
                    ,'followers_count'=>0
                    ,'fullname'=>$cls_contact->get_first_name().' '.$cls_contact->get_last_name()
                    ,'id'=>$v_assigned_to
                    ,'upgrade_status'=>0
                    ,'userpic_url'=>''
                );
                $arr_users[$v_assigned_to] = 1;
            }
        }
    }
    $arr_templates[] = array(
        'id'=>$v_template_id
        ,'name'=>$v_template_name
        ,'category'=>$v_category_id
        ,'comments_count'=>0
        ,'created_at'=>date('Y-m-d H:i:s')
        ,'description'=>$v_template_desc
        ,'favorites_count'=>0
        ,'height'=>$v_template_height
        ,'width'=>$v_template_width
        ,'image_url'=>URL.$v_saved_dir.'thumb_'.$v_sample_image
        ,'images'=>$arr_images
        ,'lisence_type'=>0
        ,'nsfw'=>false
        ,'privacy'=>false
        ,'votes_count'=>0
        ,'rating'=>100
        ,'site'=>''
        ,'times_viewed'=>$cls_design->count(array('template_id'=>$v_template_id))
        ,'user'=>$arr_user
    );
}
$arr_data = array(
    'current_page'=>1
    ,'feature'=>'popular'
    ,'filters'=>array('category'=>false, 'exclude'=>0)
    ,'total_items'=>$v_total_rows
    ,'total_pages'=>$v_total_page
    ,'templates'=>$arr_templates
);

$arr_return = array(
    'data'=>$arr_data
    ,'error'=>false
    ,'status'=>200
    ,'success'=>true
);

$cls_output->output($arr_templates, true, false);