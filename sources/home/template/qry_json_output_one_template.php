<?php
if(!isset($v_sval)) die();

$arr_where_clause = array('template_id'=>$v_template_id);
add_class('cls_draw');
add_class('cls_instagraph');

add_class('cls_tb_user');
add_class('cls_tb_contact');
add_class('cls_tb_design_template');
add_class('cls_tb_design_theme');
add_class('cls_tb_design_image');
add_class('cls_tb_design_design');
add_class('cls_tb_design_key');

$cls_draw = new cls_draw();
$cls_instagraph = new cls_instagraph();

$cls_template = new cls_tb_design_template($db, LOG_DIR);
$cls_theme = new cls_tb_design_theme($db, LOG_DIR);
$cls_design = new cls_tb_design_design($db, LOG_DIR);
$cls_image = new cls_tb_design_image($db, LOG_DIR);
$cls_user = new cls_tb_user($db, LOG_DIR);
$cls_contact = new cls_tb_contact($db, LOG_DIR);

$arr_template_category = array();
$arr_folding_type = array();
$arr_folding_direction = array();
$arr_die_cut_type = array();
$arr_data = array();
$arr_users = array();
$arr_templates = array();
$v_json = '';
$v_dpi = 72;
$v_prefix = DESIGN_NORMAL_PREFIX;
$v_template_row = $cls_template->select_one($arr_where_clause);
if($v_template_row==1){
    $v_template_id = $cls_template->get_template_id();
    $v_template_width = $cls_template->get_template_width();
    $v_template_height = $cls_template->get_template_height();
    $v_template_name = $cls_template->get_template_name();
    $v_template_desc = $cls_template->get_template_desc();
    $v_category_id = $cls_template->get_category_id();
    $v_created_time = $cls_template->get_created_time();
    $v_sample_image = $cls_template->get_sample_image();
    $v_saved_dir = $cls_template->get_saved_dir();
    $v_folding_type = $cls_template->get_folding_type();
    $v_folding_direction = $cls_template->get_folding_direction();
    $v_die_cut_type = $cls_template->get_die_cut_type();
    $v_assigned_to = $cls_template->get_user_id();
    $v_assigned_name = $cls_template->get_user_name();
    $v_json = $cls_template->get_template_data();
    $v_dpi = $cls_template->get_template_dpi();
    $arr_site_id = $cls_template->get_site_id();
    if(!is_array($arr_site_id)) $arr_site_id = array();
    $v_total_site = count($arr_site_id);
    $v_site_name = '';
    if($v_total_site>0){
        $cls_key = new cls_tb_design_key($db, LOG_DIR);
        for($i=0; $i<$v_total_site; $i++){
            $v_site_id = (int) $arr_site_id[$i];
            $v_one_name = $cls_key->select_scalar('site_name',array('site_id'=>$v_site_id));
            if(!is_null($v_one_name) && $v_one_name!='') $v_site_name .= $v_one_name .', ';
        }
        if($v_site_name!=''){
            $v_site_name = trim($v_site_name);
            $v_site_name = substr($v_site_name,0,strlen($v_site_name)-1);
        }
    }else
        $v_site_name = 'All Sites';

    if(!isset($arr_template_category[$v_category_id])) $arr_template_category[$v_category_id] = $cls_settings->get_option_name_by_id('template_category', $v_category_id);
    if(!isset($arr_folding_type[$v_folding_type])) $arr_folding_type[$v_folding_type] = $cls_settings->get_option_name_by_id('folding_type', $v_folding_type);
    if(!isset($arr_folding_direction[$v_folding_direction])) $arr_folding_direction[$v_folding_direction] = $cls_settings->get_option_name_by_id('folding_direction', $v_folding_direction);
    if(!isset($arr_die_cut_type[$v_die_cut_type])) $arr_die_cut_type[$v_die_cut_type] = $cls_settings->get_option_name_by_id('die_cut_type', $v_die_cut_type);

    //$v_template_desc .= ' Size: '.$v_template_width.'" &times; '.$v_template_height.'"';
    $arr_images = array();
    $arr_images[] = array(
        'size'=>1
        ,'url'=>URL.$v_saved_dir.$v_sample_image
    );

    $arr_theme = $cls_theme->select(array('template_id'=>$v_template_id, 'theme_status'=>0));
    foreach($arr_theme as $a){
        $arr_images[] = array(
            'size'=>1
            ,'url'=>URL.$a['saved_dir'].$a['sample_image']
        );
    }

    $arr_user = array(
        'username'=>$v_assigned_name
        ,'affection'=>0
        ,'city'=>''
        ,'country'=>''
        ,'firstname'=>$v_assigned_name
        ,'followers_count'=>0
        ,'fullname'=>$v_assigned_name
        ,'id'=>$v_assigned_to
        ,'upgrade_status'=>0
        ,'userpic_url'=>''
    );
    if(!isset($arr_users[$v_assigned_to])){
        $v_contact_id = $cls_user->select_scalar('contact_id',array('user_id'=>$v_assigned_to));
        settype($v_contact_id, 'int');
        if($v_contact_id>0){
            $v_row = $cls_contact->select_one(array('contact_id'=>$v_contact_id));
            if($v_row==1){
                $arr_user = array(
                    'username'=>$v_assigned_name
                    ,'affection'=>0
                    ,'city'=>$cls_contact->get_address_city()
                    ,'country'=>$cls_contact->get_address_country()
                    ,'firstname'=>$cls_contact->get_first_name()
                    ,'followers_count'=>0
                    ,'fullname'=>$cls_contact->get_first_name().' '.$cls_contact->get_last_name()
                    ,'id'=>$v_assigned_to
                    ,'upgrade_status'=>0
                    ,'userpic_url'=>''
                );
                $arr_users[$v_assigned_to] = 1;
            }
        }
    }
    $arr_templates = array(
        'id'=>$v_template_id
        ,'name'=>$v_template_name
        ,'category'=>$v_category_id
        ,'comments_count'=>0
        ,'created_at'=>date('d-M-Y H:i:s')
        ,'description'=>$v_template_desc
        ,'favorites_count'=>0
        ,'height'=>$v_template_height
        ,'width'=>$v_template_width
        ,'image_url'=>URL.$v_saved_dir.$v_sample_image
        ,'size'=>$v_template_width.'" x '.$v_template_height.'"'
        ,'images'=>$arr_images
        ,'lisence_type'=>0
        ,'nsfw'=>false
        ,'privacy'=>false
        ,'votes_count'=>0
        ,'rating'=>100
        ,'site_name'=>$v_site_name
        ,'times_viewed'=>$cls_design->count(array('template_id'=>$v_template_id)).''
        ,'category_name'=>$arr_template_category[$v_category_id]
        ,'folding_type'=>$arr_folding_type[$v_folding_type]
        ,'folding_direction'=>$arr_folding_direction[$v_folding_direction]
        ,'die_cute_type'=>$arr_die_cut_type[$v_die_cut_type]
        ,'user'=>$arr_user
    );
}

if($v_json!=''){
    $arr_json = json_decode($v_json, true);
    if(is_array($arr_json)){
        $arr_canvases = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $v_total_page = count($arr_canvases);
        //$v_ext = 'png';
        //$v_image_sample = 'template_'.$v_template_id.'_theme_'.$v_theme_id.'.'.$v_ext;

        $v_dir = DESIGN_THEME_DIR.DS;
        if(file_exists($v_dir.$v_template_id) || @mkdir($v_dir.$v_template_id)){
            $v_dir .= $v_template_id .DS;
        }

        $v_max_width = DESIGN_IMAGE_NORMAL_SIZE;
        $v_max_height = DESIGN_IMAGE_NORMAL_SIZE;
        $v_full_path = $v_dir.$v_prefix.$v_sample_image;
        $v_accept = true;
        //if(file_exists($v_full_path)) @unlink($v_full_path);

        if(!file_exists($v_full_path)){
            $cls_tmp_draw = new cls_draw();
            $image = new Imagick();
            $v_is_admin = true;
            if($v_total_page<=1){
                $v_page = 0;

                $cls_draw->create_preview($image, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
                if($v_accept){
                    $v_image_width = $image->getimagewidth();
                    $v_image_height = $image->getimageheight();
                    if($v_image_height>$v_max_height || $v_image_width>$v_max_width){
                        if($v_image_height > $v_image_width){
                            $v_ratio = $v_max_height / $v_image_height;
                            $v_max_width = ceil($v_ratio * $v_image_width);
                        }else{
                            $v_ratio = $v_max_width / $v_image_width;
                        }
                    }

                    $image = $cls_tmp_draw->shadow_image($image, $v_max_width);

                    $image->setimageformat('png');
                    $image->writeimage($v_full_path);

                    $image->clear();
                    $image->destroy();
                }
            }else{
                if($v_accept){
                    $v_page = 1;
                    $image2 = new Imagick();
                    $cls_draw->create_preview($image2, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);
                    $v_page = 0;
                    $image1 = new Imagick();
                    $cls_draw->create_preview($image1, $cls_image, $cls_instagraph, $arr_json, $v_dpi, $v_page, $v_is_admin);


                    $v_image_width = $image1->getimagewidth();
                    $v_image_height = $image1->getimageheight();
                    if($v_image_height>$v_max_height || $v_image_width>$v_max_width){
                        if($v_image_height > $v_image_width){
                            $v_ratio = $v_max_height / $v_image_height;
                            $v_max_width = ceil($v_ratio * $v_image_width);
                        }else{
                            $v_ratio = $v_max_width / $v_image_width;
                        }
                    }


                    $image = $cls_tmp_draw->create_sample($image1, $image2, $v_max_width);

                    $image->setimageformat('png');
                    $image->writeimage($v_full_path);
                    $image->clear();
                    $image->destroy();
                    $image1->clear();
                    $image1->destroy();
                    $image2->clear();
                    $image2->destroy();

                }
            }
        }
        if(file_exists($v_full_path)){
            list($width, $height) = getimagesize($v_full_path);
            $arr_templates['width'] = $width;
            $arr_templates['height'] = $height;
            $arr_templates['image_url'] = URL.$v_saved_dir.$v_prefix.$v_sample_image;
        }

    }
}
$cls_output->output(array('template'=>$arr_templates, true, false));
