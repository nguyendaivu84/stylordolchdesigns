<script type="text/javascript" >
    var products = <?php echo json_encode($arr_products);?>;
	var current_product = <?php echo json_encode($arr_current_product);?>;
    var user = <?php echo json_encode($arr_user);?>;
    var config = {};
    config.designURL = '<?php echo $v_design_url;?>';
    config.addImageURL = '<?php echo $v_add_image_url;?>';
    config.fonts = <?php echo json_encode($arr_fonts);?> ;
    config.nextPageUrl =  '<?php echo $v_next_url;?>';
    config.homeUrl = '<?php echo $v_home_url;?>' ;
    config.popupLoginUrl = '<?php echo $v_login_url;?>' ;
    config.maxSvgColorSupport = 300;
	config.siteKey = '<?php echo $v_site_key;?>';
    config.urlProfileEQ = {"id":"0","description":"<?php echo $v_home_url;?>","show_text_panel":"0","show_ruler":"0"} ;
</script>

<script type="text/javascript" src="<?php echo $v_design_url;?>lib/js/jquery.1.10.2.min.js"></script>
<script type="text/javascript" src="<?php echo $v_design_url;?>lib/design/js/helpers.js"></script>
<script type="text/javascript" src="<?php echo $v_design_url;?>lib/design/js/jquery-ui.1.9.2.min.js"></script>

<link href="<?php echo $v_design_url;?>lib/design/css/design_tool.css" rel="stylesheet" type="text/css" />

<div id="main_loading" style="font-size: 15px ; font-weight: bold ; position:fixed; top: 0 ; left: 0 ; width: 100% ; height: 100% ; background-color: #fff ; z-index: 100000 ; padding: 25px 30px;">Loading...</div>
<div id="preview_design"></div>
<div id="gray_box" style="display:none" ></div>
<div id="notice_bar" style="display:none"><div id="notice_box"></div></div>

<div id="workspace">
<div class="toolbar ztoolbar" id="nav_toolbar">
    <div class="top-nav-left" >
        <div id="theme_options" class="toolbar-button type5 has-menu has-menu menu-opener" style="line-height:normal ; display: none">
            <div class="button-icon button-arrow" style="vertical-align:text-bottom"></div>
            <div class="button-menu with-divider" id="theme_menu">
            </div>
        </div>

        <div id="info_ph">
            <div id="info_design_title"></div>
            <div id="info_product_title"></div>
        </div>

    </div>
    <div id="page_navigation"></div>
    <div class="top-nav-right" >
        <?php if($v_allow_preview){?>
        <div id="btn_preview" class="toolbar-button type5 gray-gradient">
            <div class="button-icon" style="background-position: -66px -16px"></div>
            <div class="button-text">Preview</div>
        </div>
        <?php }?>
        <div class="toolbar-button type5 has-menu menu-opener gray-gradient">
            <?php if($v_allow_share){?>
            <div class="button-icon" style="background-position: -24px -16px"></div>
            <div class="button-text">Share</div>
            <div class="button-icon button-arrow" ></div>
            <div class="button-menu with-divider thick" >
                <div id="btn_share_email" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -576px 0px"></div>
                    <div class="button-menu-text">Send by Email</div>
                </div>
                <!--
                <div id="btn_share_facebook" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -1088px 0px"></div>
                    <div class="button-menu-text">Share on Facebook</div>
                </div>
                -->
            </div>
            <?php }?>
        </div>

        <div id="btn_save" class="toolbar-button type5 has-menu has-optional-menu gray-gradient">
            <div class="button-icon" style="background-position: 0px -16px"></div>
            <div class="button-text">Save</div>
            <div class="button-arrow-full-height menu-opener">
                <div class="button-icon button-arrow"></div>
            </div>

            <div class="button-menu with-divider thick" >
                <div id="btn_save_2" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -592px 0px"></div>
                    <div class="button-menu-text">Save</div>
                </div>
                <div id="btn_save_as" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -608px 0px"></div>
                    <div class="button-menu-text">Save As...</div>
                </div>
                <!--
                <div id="btn_save_pdf" class="toolbar-button type4" style="display:none" >
                    <div class="button-menu-icon" style="background-position: -384px 0px"></div>
                    <div class="button-menu-text">Download PDF</div>
                </div>
                -->
            </div>

        </div>
        <?php
        if($v_allow_proceed){
        if(isset($v_product_id) && $v_product_id>0){
            ?>
            <div id="btn_proceed_order" class="toolbar-button type5 action-button red">
                <div class="button-text">Proceed to Order</div>
            </div>
        <?php
        }
        }
        ?>
    </div>
    <div class="clearer"></div>
</div>

<div id="top_toolbar_placeholder_wrapper" class="ztoolbar" >
<?php if($v_allow_product_list){?>
<div id="btn_top_toolbar_product" class="toolbar-button">
    <a id="btn_product" href="<?php echo $_SERVER['HTTP_REFERER'];?>">Product List</a>
</div>
<?php }?>

<div id="btn_top_toolbar_toggle" class="toolbar-button" ></div>
<div id="top_toolbar_placeholder" class="ztoolbar" >
<div id="top_toolbar" class="toolbar ztoolbar">
<div class="toolbar-section">
    <div class="toolbar-section-title">Insert</div>
    <div class="toolbar-section-body">
        <?php if($v_allow_add_text){?>
        <div id="btn_insert_text" class="toolbar-button type1 gray-gradient has-menu menu-opener" >
            <div class="button-icon" style="background-position: -48px -40px"></div>
            <div class="button-text">Text</div>


            <div class="button-menu with-divider insert-text-menu">
                <div class="menu-label">Select a text type:</div><br/>
                <div class="toolbar-button type2 btn-insert-text" id="insert_company_name" data-body="Company Name" data-title="Company Name">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Company Name</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_slogan" data-body="Slogan/Message" data-title="Slogan/Message">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Slogan</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_your_name" data-body="Full Name" data-title="Full Name">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Full Name</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_job_title" data-body="Job Title" data-title="Job Title">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Job Title</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_phone" data-body="P: 800.555.1234" data-title="Phone No.">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Phone</div>
                </div><br/>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_fax" data-body="F: 800.555.2345" data-title="Fax No.">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Fax</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_email_address" data-body="name@email.com" data-title="Email Address">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Email</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_web_address" data-body="www.companyname.com" data-title="Web Address">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Website</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_address_line_1" data-body="12345 Main Street" data-title="Address Line 1">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Address</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_address_line_2" data-body="City Name, State 98765" data-title="Address Line 2">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">Address</div>
                </div>                                                        <div class="toolbar-button type2 btn-insert-text" id="insert_user_text" data-body="New Text Box..." data-title="Enter text here">
                    <div class="insert-text-item-icon"></div>
                    <div class="insert-text-item-title">General Text</div>
                </div>
            </div>


        </div>
        <?php }else{?>
        <div id="btn_no_insert_text" class="toolbar-button type1 gray-gradient" >
            <div class="button-icons" style="background-position: -48px -40px"></div>
            <div class="button-text">No Text</div>
        </div>
        <?php }?>
        <!--
	                -->
        <?php if($v_allow_add_image){?>
        <div id="btn_insert_upload" class="toolbar-button type1 gray-gradient">
            <div class="button-icon" style="background-position: 0 -40px"></div>
            <div class="button-text">Image</div>
        </div>
        <?php }else{?>
        <div id="btn_no_insert_upload" class="toolbar-button type1 gray-gradient">
            <div class="button-icons" style="background-position: 0 -40px"></div>
            <div class="button-text">No Image</div>
        </div>
		<?php }?>        
        <!--
	                    <div id="btn_insert_stock" class="toolbar-button type1 gray-gradient">
                    	<div class="button-icon"></div>
                    	<div class="button-text">Stock Image</div>
                    </div>
	                -->
        <?php if($v_allow_add_shape){?>
        <div id="btn_insert_shape" class="toolbar-button type1 gray-gradient has-menu menu-opener">
            <div class="button-icon" style="background-position: -96px -40px"></div>
            <div class="button-text">Shape</div>
            <div class="button-menu with-divider">
                <div id="btn_shape_rect" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -832px 0px"></div>
                    <div class="button-menu-text">Rectangle</div>
                </div>
                <div id="btn_shape_circle" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -848px 0px"></div>
                    <div class="button-menu-text">Circle</div>
                </div>
                <div id="btn_shape_triangle" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -864px 0px"></div>
                    <div class="button-menu-text">Triangle</div>
                </div>
                <div id="btn_shape_hline" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -816px 0px"></div>
                    <div class="button-menu-text">Horizontal Line</div>
                </div>
                <div id="btn_shape_vline" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -802px 0px"></div>
                    <div class="button-menu-text">Vertical Line</div>
                </div>
            </div>
        </div>
        <?php }else{?>
        <div id="btn_no_insert_shape" class="toolbar-button type1 gray-gradient has-menu menu-opener">
            <div class="button-icons" style="background-position: -96px -40px"></div>
            <div class="button-text">No Shape</div>
        </div>
        <?php }?>

    </div>
</div>


<div class="toolbar-section-divider"></div>
<div class="toolbar-section" id="text_tools_section">
    <div class="toolbar-section-title">Text</div>
    <div class="toolbar-section-body">
        <div class="toolbar-section-row">
            <div id="btn_text_font_face" class="toolbar-button type3 has-menu menu-opener" >
                <div class="button-text">Font</div>
                <div class="bordered">
                    <div id="selected_font_face" class="button-selected-text" style="width:80px" ></div>
                    <div class="button-arrow" ></div>
                </div>
                <div id="font_face_menu" class="button-menu">

                    <div id="recent_fonts_section">
                        <div class="font-section-header">Recent Fonts</div>
                        <div id="recent_fonts"></div>
                        <div class="font-section-header">All Fonts</div>
                    </div>

                </div>
            </div>
            <div id="btn_text_font_size" class="toolbar-button type3 has-menu menu-opener">
                <div class="button-text">Size</div>
                <div class="bordered">
                    <input id="selected_font_size" class="button-textbox" type="text"  />
                    <div class="button-arrow" ></div>
                </div>
                <div id="font_size_menu" class="button-menu"></div>
            </div>
            <div id="btn_text_font_color" class="toolbar-button type3">
                <div class="button-text">Color</div>
                <div id="selected_font_color"></div>
                <!--
                <div class="bordered">
                    <div id="selected_font_color" class="selected-color"></div>
                    <div class="button-arrow" ></div>
                </div>
                <div id="font_color_menu" class="button-menu color-palette"></div>
                -->
            </div>

        </div>
        <div class="toolbar-section-row">
            <div  id="btn_text_bold" class="toolbar-button type2" >
                <div class="button-icon" style="background-position: -32px 0px"></div>
            </div>

            <div  id="btn_text_italic" class="toolbar-button type2" >
                <div class="button-icon" style="background-position: -48px 0px"></div>
            </div>

            <div id="btn_text_gravity_west" class="toolbar-button type2 grouped " >
                <div class="button-icon" style="background-position: -64px 0px"></div>
            </div>

            <div id="btn_text_gravity_center" class="toolbar-button type2 grouped " >
                <div class="button-icon" style="background-position: -80px 0px"></div>
            </div>

            <div id="btn_text_gravity_east" class="toolbar-button type2 grouped " >
                <div class="button-icon" style="background-position: -96px 0px"></div>
            </div>

            <div id="btn_text_bullet" class="toolbar-button type2 has-menu menu-opener" >
                <div class="button-icon" style="background-position: -96px -16px"></div>
                <div class="button-menu">
                    <div id="btn_text_bullet_none" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: 0 0"></div>
                    </div>

                    <div id="btn_text_bullet_black_circle" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: 0 -60px"></div>
                    </div>
                    <div id="btn_text_bullet_white_circle" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -60px -60px"></div>
                    </div>
                    <br />
                    <div id="btn_text_bullet_black_square" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -120px -60px"></div>
                    </div>
                    <div id="btn_text_bullet_white_square" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -180px -60px"></div>
                    </div>
                    <div id="btn_text_bullet_dash" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -240px -60px"></div>
                    </div>
                </div>
            </div>

            <div id="btn_text_number" class="toolbar-button type2 has-menu menu-opener" >
                <div class="button-icon" style="background-position: -112px -16px"></div>
                <div class="button-menu">
                    <div id="btn_text_number_none" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: 0 0"></div>
                    </div>
                    <div id="btn_text_bullet_number_dot" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -60px 0"></div>
                    </div>
                    <br />
                    <div id="btn_text_bullet_number_dash" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -120px 0"></div>
                    </div>
                    <div id="btn_text_bullet_number_parenth" class="toolbar-button type2">
                        <div class="bullet-text-item" style="background-position: -180px 0"></div>
                    </div>
                </div>
            </div>

            <div id="btn_text_auto_fit" class="toolbar-button type2 admin-only" >
                <div class="button-text">Auto Fit</div>
            </div>



        </div>

    </div>
</div>

<div class="toolbar-section-divider"></div>
<div class="toolbar-section" id="bg_tools_section" style="width: 80px">
    <div class="toolbar-section-title">Background</div>
    <div class="toolbar-section-body">

        <div id="btn_bg_color" class="toolbar-button type3">
            <div class="button-text">Color</div>
            <div id="selected_bg_color"></div>
            <!--
            <div class="bordered">
                <div id="selected_bg_color" class="selected-color"></div>
                <div class="button-arrow" ></div>
            </div>
            <div id="bg_color_menu" class="button-menu color-palette"></div>
            -->
        </div>

    </div>
</div>

<div class="toolbar-section-divider"></div>
<div class="toolbar-section" >
    <div class="toolbar-section-title">Edit</div>
    <div class="toolbar-section-body">
        <div id="btn_edit_undo" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: 0px 0px"></div>
            <div class="button-text">Undo</div>
        </div>
        <div id="btn_edit_redo" class="toolbar-button type2" >
            <div class="button-icon"  style="background-position: -16px 0px"></div>
            <div class="button-text">Redo</div>
        </div>
        <div id="btn_edit_lock" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -720px 0px"></div>
            <div class="button-text">Lock</div>
        </div>
        <hr />
        <div  id="btn_edit_copy" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -480px 0px"></div>
            <div class="button-text">Copy</div>
        </div>
        <div id="btn_edit_paste" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -496px 0px"></div>
            <div class="button-text">Paste</div>
        </div>
        <div id="btn_edit_delete" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -400px 0px"></div>
            <div class="button-text">Delete</div>
        </div>
    </div>
</div>
<div class="toolbar-section-divider"></div>
<div class="toolbar-section" >
    <div class="toolbar-section-title">Advanced</div>
    <div class="toolbar-section-body">
        <div id="btn_advance_align" class="toolbar-button type2 has-menu menu-opener">
            <div class="button-icon" style="background-position: -144px 0px"></div>
            <div class="button-text">Align</div>
            <div class="button-menu with-divider" >
                <div id="btn_align_left" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -144px 0px"></div>
                    <div class="button-menu-text">Align Left</div>
                </div>
                <div id="btn_align_center" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -192px 0px"></div>
                    <div class="button-menu-text">Align Center</div>
                </div>
                <div id="btn_align_right" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -176px 0px"></div>
                    <div class="button-menu-text">Align Right</div>
                </div>
                <hr />
                <div id="btn_align_top" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -128px 0px"></div>
                    <div class="button-menu-text">Align Top</div>
                </div>
                <div id="btn_align_middle" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -208px 0px" ></div>
                    <div class="button-menu-text">Align Middle</div>
                </div>
                <div id="btn_align_bottom" class="toolbar-button type4"  >
                    <div class="button-menu-icon" style="background-position: -160px 0px"></div>
                    <div class="button-menu-text">Align Bottom</div>
                </div>
                <hr />
                <div id="btn_dist_ver" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -896px 0px" ></div>
                    <div class="button-menu-text">Distribute Vertically</div>
                </div>
                <div id="btn_dist_hor" class="toolbar-button type4" >
                    <div class="button-menu-icon"  style="background-position: -880px 0px" ></div>
                    <div class="button-menu-text">Distribute Horizontally</div>
                </div>
            </div>
        </div>
        <div id="btn_advance_layers" class="toolbar-button type2 has-menu menu-opener">
            <div class="button-icon" style="background-position: -320px 0px"></div>
            <div class="button-text">Layers</div>
            <div class="button-menu with-divider" >
                <div id="btn_arrange_bring_front" class="toolbar-button type4" >
                    <div class="button-menu-icon" style="background-position: -432px 0px"></div>
                    <div class="button-menu-text">Bring to Front</div>
                </div>
                <div id="btn_arrange_send_back" class="toolbar-button type4"  >
                    <div class="button-menu-icon" style="background-position: -464px 0px"></div>
                    <div class="button-menu-text">Send to Back</div>
                </div>
                <div id="btn_arrange_bring_forward" class="toolbar-button type4" >
                    <div class="button-menu-icon"  style="background-position: -416px 0px"></div>
                    <div class="button-menu-text">Bring Forward</div>
                </div>
                <div id="btn_arrange_send_backward" class="toolbar-button type4" >
                    <div class="button-menu-icon"  style="background-position: -448px 0px"></div>
                    <div class="button-menu-text">Send Backward</div>
                </div>
            </div>
        </div>
        <hr />
        <!--
        <div id="btn_advance_rotate" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -688px 0px"></div>
            <div class="button-text">Rotate</div>
        </div>
        -->
        <!--
        <div id="btn_advance_rotate" class="toolbar-button type2" >
            <div class="button-text"><input type="text" value="0" style="width: 30px; height:20px; font-size: 11px" /></div>
            <div class="button-text">Rotate</div>
        </div>
        -->
        <div id="btn_advance_rotate" class="toolbar-button type3 has-menu menu-opener" >
            <div class="bordered">
                <input id="selected_rotate_degree" class="button-textbox" type="text"  />
                <div class="button-arrow" ></div>
            </div>
            <div id="rotate_degree_menu" class="button-menu"></div>
            <div class="button-text">Rotate</div>
		</div>        
        
        <div id="btn_advance_crop" class="toolbar-button type2"   >
            <div class="button-icon" style="background-position: -336px 0px"></div>
            <div class="button-text">Crop</div>
        </div>

    </div>
</div>
<div class="toolbar-section-divider"></div>
<div id="view_tools_section" class="toolbar-section" >
    <div class="toolbar-section-title">View</div>
    <div class="toolbar-section-body">
        <div id="btn_view_bleed" class="toolbar-button type2" style="width:50px">
            <div class="button-icon" style="background-position: -256px 0px"></div>
            <div class="button-text">Trim Box</div>
        </div>
        <div id="btn_view_grid" class="toolbar-button type2" style="width:45px">
            <div class="button-icon" style="background-position: -272px 0px"></div>
            <div class="button-text">Grid</div>
        </div>

        <div id="btn_view_snap" class="toolbar-button type2" >
            <div class="button-icon" style="background-position: -368px 0px"></div>
            <div class="button-text">Snap</div>
        </div>
        <hr />
        <div id="btn_view_zoom_out" class="toolbar-button type2" style="width:50px" >
            <div class="button-icon" style="background-position: -304px 0px"></div>
            <div class="button-text">Zoom out</div>
        </div>
        <div id="btn_view_zoom_in" class="toolbar-button type2" style="width:45px">
            <div class="button-icon" style="background-position: -288px 0px"></div>
            <div class="button-text">Zoom in</div>
        </div>

        <div id="btn_view_quick_edit" class="toolbar-button type2" >
            <div class="button-text" style="line-height:14px">Quick<br>Input</div>
        </div>

    </div>
</div>
</div>


</div>
</div>


<div id="left_panel"  >
    <div id="text_fields" ></div>
</div>


<div id="design_tool" >
    <!--    <div id="page_navigation"></div>-->
    <div id="tool_placeholder">




    </div>
</div>

<!-- Start: Bottom toolbar placeholder -->
<div id="bottom_toolbar_placeholder">
</div>
<!-- End Bottom toolbar placeholder -->
</div>
<!-- End: workspace -->
<div class="float-toolbar ztoolbar" id="crop_toolbar">
    <div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1120px 0px ;" onclick="moveInnerImage('U')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
    </div>
    <div>
        <div class="crop-toolbar-item" style="background-position: -1104px 0px ;" onclick="moveInnerImage('L')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1136px 0px ;" onclick="moveInnerImage('R')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1168px 0px ;" onclick="zoomOutInnerImage()"><span>Zoom Out</span></div>
        <div class="crop-toolbar-item" style="width:auto; background-position: 100px 100px ; cursor:default" ><div id="crop_slider"></div></div>
        <div class="crop-toolbar-item" style="background-position: -1184px 0px ;" onclick="zoomInInnerImage()"><span>Zoom In</span></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -688px 0px ;" onclick="rotateInnerImage(90)"><span>Rotate 90&deg;</span></div>
        <div class="crop-toolbar-item" style="background-position: -704px 0px ;" onclick="rotateInnerImage(-90)"><span>Rotate 90&deg;</span></div>
    </div>
    <div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
        <div class="crop-toolbar-item" style="background-position: -1152px 0px ;" onclick="moveInnerImage('D')"></div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>
    </div>

    <div id="extra_crop_option" >
        <div id="btn_text_remove" class="toolbar-button type6 gray-gradient" >
            <div class="button-icon" style="background-position: -400px 0 " ></div>
        </div>

        <div id="btn_text_remove" class="toolbar-button type6 gray-gradient"  onclick="replacePhImage()">
            <div class="button-text">Replace Image</div>
        </div>
        <div class="crop-toolbar-item" style="background-position: 100px 100px ;"></div>

        <div id="btn_crop_done" class="toolbar-button type6 green-gradient" >
            <div class="button-text">Done</div>
        </div>

    </div>

</div>

<div class="ztoolbar close" id="textbox_tools" >
    <div id="btn_text_edit" class="toolbar-button type2 close-only">
        <div class="button-icon" style="background-position: -352px 0px"></div>
        <div class="button-text">Edit</div>
    </div>

    <div class="open-only">
        <div id="textbox_edit_area" style="display:block; margin-bottom: 10px">
            <div id="textbox_tools_text_title" ></div>
            <textarea id="inline_textarea" ></textarea>
        </div>

        <div style="border-top: 0px solid #ddd; text-align: center; padding-top: 4px">


            <div id="btn_text_remove" class="toolbar-button type6 gray-gradient" >
                <div class="button-icon" style="background-position: -400px 0 " ></div>
                <div class="button-text">Delete</div>
            </div>

            <div id="btn_text_done" class="toolbar-button type6 green-gradient" >
                <div class="button-text">Done</div>
            </div>

        </div>
    </div>

</div>


<div class="float-toolbar ztoolbar" id="image_toolbar">
    <div id="btn_image_delete" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -400px 0px"></div>
        <div class="button-text">Delete</div>
    </div>

    <div id="btn_image_crop" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -336px 0px"></div>
        <div class="button-text">Crop</div>
    </div>

    <div id="btn_image_border_color" class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
        <div class="selected-color"></div>
        <div class="button-text">Border</div>
        <div id="image_border_color_menu" class="button-menu color-palette"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -512px 0px" ></div>
        <div class="button-text">Weight</div>
        <div id="image_border_width_menu" class="button-menu"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener gray-gradientt with-active" >
        <div class="image_filter_option" style="background-position: 0px 0px"></div>
        <div class="button-text">Filter</div>
        <div id="image_filter_width_menu" class="button-menu">
            <div id="image_filter_none" class="toolbar-button type4 image_filter_option" style="background-position: -16px 0px"></div>
            <div class="button-text">None</div>
            <div id="image_filter_gotham" class="toolbar-button type4 image_filter_option" style="background-position: -32px 0px"></div>
            <div class="button-text">Gotham</div>
            <div id="image_filter_kelvin" class="toolbar-button type4 image_filter_option" style="background-position: -48px 0px"></div>
            <div class="button-text">Kelvin</div>
            <div id="image_filter_lomo" class="toolbar-button type4 image_filter_option" style="background-position: -64px 0px"></div>
            <div class="button-text">Lomo</div>
            <div id="image_filter_nashville" class="toolbar-button type4 image_filter_option" style="background-position: -80px 0px"></div>
            <div class="button-text">Nashville</div>
            <div id="image_filter_tiltshift" class="toolbar-button type4 image_filter_option" style="background-position: -96px 0px"></div>
            <div class="button-text">TiltShift</div>
            <div id="image_filter_toaster" class="toolbar-button type4 image_filter_option" style="background-position: -112px center"></div>
            <div class="button-text">Toaster</div>

        </div>
    </div>


    <div id="svg_tools" class="toolbar-button type3 has-menu">
        <div id="svg_colors"></div>
        <div id="svg_color_menu" class="button-menu color-palette with-transparent"></div>
    </div>

</div>

<div class="float-toolbar ztoolbar" id="shape_toolbar">
    <div id="btn_shape_delete" class="toolbar-button type2 gray-gradientt with-active" >
        <div class="button-icon" style="background-position: -400px 0px"></div>
        <div class="button-text">Delete</div>
    </div>

    <div id="btn_shape_fill_color" class="toolbar-button type2 has-menu menu-opener" >
        <div class="selected-color"></div>
        <div class="button-text">Fill</div>
        <div id="shape_fill_color_menu" class="button-menu color-palette with-transparent"></div>
    </div>

    <div id="btn_shape_border_color" class="toolbar-button type2 has-menu menu-opener" >
        <div class="selected-color"></div>
        <div class="button-text">Border</div>
        <div id="shape_border_color_menu" class="button-menu color-palette"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener" >
        <div class="button-icon" style="background-position: -512px 0px" ></div>
        <div class="button-text">Weight</div>
        <div id="shape_border_width_menu" class="button-menu"></div>
    </div>

    <div class="toolbar-button type2 has-menu menu-opener" >
        <div class="button-icon" style="background-position: -528px 0px" ></div>
        <div class="button-text">Stroke</div>
        <div id="shape_border_stroke_menu" class="button-menu">
            <div id="shape_border_stroke_solid" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -1px"></div>
            <div id="shape_border_stroke_round_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -15px"></div>
            <div id="shape_border_stroke_square_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -29px"></div>
            <div id="shape_border_stroke_dash" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -43px"></div>
            <div id="shape_border_stroke_dash_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -58px"></div>
            <div id="shape_border_stroke_long_dash" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -72px"></div>
            <div id="shape_border_stroke_long_dash_dot" class="toolbar-button type4 shape-border-stroke-option" style="background-position: center -86px"></div>
        </div>
    </div>

</div>

<iframe id="pdf_proof_iframe" name="pdf_proof_iframe" src="about:blank" frameborder="0"></iframe>
<form id= "pdf_proof_form" action="/ajax/design/getPdfProofFromJson.php" target="pdf_proof_iframe" method="post" >
    <input id="pdf_proof_json" type="hidden" name="json" value="{}" />
</form>
<link rel="stylesheet" href="<?php echo $v_design_url;?>lib/jpicker/css/jPicker-1.1.6.css" type="text/css" />
<link rel="stylesheet" href="<?php echo $v_design_url;?>lib/jpicker/css/jPicker.css" type="text/css" />
<script type="text/javascript" src="<?php echo $v_design_url;?>lib/jpicker/js/jpicker-1.1.6.min.js"></script>
<script type="text/javascript">
    $.fn.jPicker.defaults.images.clientPath='<?php echo $v_design_url;?>lib/jpicker/images/';
</script>

<script type="text/javascript" src="<?php echo $v_design_url;?>lib/design/js/stylist.js"></script>