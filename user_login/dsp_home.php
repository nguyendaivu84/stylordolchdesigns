<?php if(!isset($v_sval)) die();?>
<!DOCTYPE html>
<html>
<head>
    <title>ImageStylor | Home - Login</title>
    <base href="<?php echo URL;?>" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" href="lib/css/login.css" />
    <script src="lib/js/jquery.1.10.2.min.js"></script>
    
</head>
<body>
    <!-- <div id="wrapper">
    </div> -->
    <div id="wrapper">
    <div id="blog-content">
        <div class="blog-top-bg">
            <div class="img_design">
                <img src="images/login/web_design_slide.png" alt="design" />
            </div>
            <div class="post" id="post-40">
                <h2>Please login for edit</h2>
                <div class="note">&nbsp</div>
                <div class="blog-content-bg">
                    
                    <div class="form_login">
                        <form method="POST" action="/login"><br />
                            <span>Username</span><br />
                            <p><input type="text" name="txt_user_name" id="txt_user_name" value="" style="width:200px;" /></p>
                            <span>Password</span><br />
                            <p><input type="password" name="txt_user_password" id="txt_user_password" value="" style="width:200px;" /></p><br />
                            <input type="button" id="login" value=" Login " /><br />
                        </form>
                    </div>

                </div>
                <div class="blog-bottom-bg"> </div>
            </div>
        </div>
    </div>
    <div id="wrapper">

    <script type="text/javascript">
        $(document).ready(function(){

            $("#login").click(function(){    
                  var username=$("#txt_user_name").val();
                  var password=$("#txt_user_password").val();
                  $.ajax({
                       type: "POST",
                       url: "/",
                       data: {txt_user_name:username, txt_user_password:password,txt_ajax_action:'login'},
                       success: function(html){ 
                          if(html.error>0){
                             $(".note").html(html.message);
                          }else{
                              window.location.assign("<?php echo URL?>admin/design/templates");
                          }
                       },
                       beforeSend:function(){
                            $(".note").html("Checking...");
                       }
                  });
            });
        });
    </script>


</body>
</html>
