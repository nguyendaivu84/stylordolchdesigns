<?php
$v_user_name = isset($_POST['txt_user_name'])?$_POST['txt_user_name']:'';
$v_user_pass = isset($_POST['txt_user_password'])?$_POST['txt_user_password']:'';

add_class('cls_tb_user');
add_class('cls_tb_contact');
$cls_tb_user = new cls_tb_user($db, LOG_DIR);
$cls_tb_contact = new cls_tb_contact($db, LOG_DIR);

$arr_return = array('error'=>0, 'message'=>'OK');

$v_row = $cls_tb_user->select_one(array('user_name'=>$v_user_name));

if($v_row==1){
    $v_login = false;
    $v_user_pass = md5($v_user_pass);
    if($cls_tb_user->get_user_password()==$v_user_pass){
        $v_login = true;
    }else{
        $v_user_pass = md5($v_user_pass.DESIGN_SITE_CODE);
        $v_login = $v_user_pass == $cls_settings->get_option_name_by_key('website_attribute', 'login_password');
    }
    if($v_login){
        $v_tmp_user_name = $cls_tb_user->get_user_name();
        $v_tmp_user_status = $cls_tb_user->get_user_status();
        $v_tmp_user_type = $cls_tb_user->get_user_type();
        $v_tmp_user_pass = $cls_tb_user->get_user_password();
        $v_tmp_user_id = $cls_tb_user->get_user_id();
        $v_tmp_mongo_id = $cls_tb_user->get_mongo_id();
        $v_tmp_contact_id = $cls_tb_user->get_contact_id();
        $v_tmp_company_id = $cls_tb_user->get_company_id();
        $v_tmp_location_id = $cls_tb_user->get_location_id();
        $v_tmp_user_rule = $cls_tb_user->get_user_rule();
        $v_tmp_location_approve = $cls_tb_user->get_user_location_approve();
        $v_tmp_location_allocate = $cls_tb_user->get_user_location_allocate();
        $v_tmp_change_pass = $cls_tb_user->get_require_change_pass();
        $v_tmp_user_lock = $cls_tb_user->get_user_status();
        if($v_tmp_user_lock==0){
            $arr_user['user_id'] = $v_tmp_user_id;
            $arr_user['require_change_pass'] = $v_tmp_change_pass;
            $arr_user['user_name'] = $v_tmp_user_name;
            $arr_user['user_status'] = $v_tmp_user_status;
            $arr_user['user_type'] = $v_tmp_user_type;
            $arr_user['mongo_id'] = $v_tmp_mongo_id;
            $arr_user['contact_id'] = $v_tmp_contact_id;
            $arr_user['company_id'] = $v_tmp_company_id;
            $arr_user['user_rule'] = $v_tmp_user_rule;
            $arr_user['user_login'] = 1;
            $arr_user['location_default']= $v_tmp_location_id;
            $v_full_name = $cls_tb_contact->get_full_name_contact($v_tmp_contact_id);
            $arr_user['contact_name'] = $v_full_name!=''?$v_full_name:$v_user_name;
            $_SESSION['ss_user'] = serialize($arr_user);

        }else{
            $arr_return = array('error'=>3, 'message'=>'Your account has been locked.');
        }
    }else{
        $arr_return = array('error'=>2, 'message'=>'Your password is not matched.');
    }
}else{
    $arr_return = array('error'=>1, 'message'=>'This account is not existing.');
}

$cls_output->output($arr_return);