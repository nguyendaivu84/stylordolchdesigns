<?php if(!isset($v_sval)) die();?>
<!DOCTYPE html>
<html>
<head>
    <title>ImageStylor | Home - Login</title>
    <base href="<?php echo URL;?>" />
    <meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport" />
    <meta content="This rich HTML5 demo shows how Kendo UI framework interacts with Flickr photo streams. The demo features some HTML5-powered controls and Kendo UI DataSource." name="description" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <link rel="shortcut icon" href="favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="images/touch-icon-57-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/touch-icon-72-precomposed.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/touch-icon-114-precomposed.png" />
    <link rel="apple-touch-startup-image" href="images/splash.png" />

    <script type="text/javascript">
        var template_url = '<?php echo URL;?>';
    </script>

    <link rel="stylesheet" href="lib/aeroviewer/aeroviewer.css" />
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>

    <link href="lib/kendo/css/kendo.common.min.css" rel="stylesheet" />
    <link href="lib/kendo/css/kendo.default.min.css" rel="stylesheet" />

    <script src="lib/js/jquery.1.10.2.min.js"></script>
    <script src="lib/kendo/js/kendo.web.min.js"></script>
    <script src="lib/aeroviewer/500px.js"></script>
    <script src="lib/aeroviewer/aeroviewer.js"></script>

</head>
<body>
<div id="root"></div>

<script type="text/x-kendo-template" id="main">
    <header>
        <?php if(!isset($arr_user['login']) || $arr_user['login']==0){?>
            <!--
        <form data-bind="events: { submit: performSearch }">
            <label>Search: <input type="search" data-bind="value: query" /></label>
        </form>
            !-->
            <form>
                <label><input style="width: 100px; cursor:pointer" y type="button" value="LOGIN" data-bind="events: {click: performLogin}" /></label>
                <label>Password: <input type="password" data-bind="value: password" /></label>
                <label>User Name: <input type="text" data-bind="value: username" /></label>
            </form>

        <?php }?>
        <h1 class="aeroLogo"><a href="">Image<span>Stylor</span></a></h1>&nbsp;
        <!--button id="slideshow-button" data-bind="text: slideShowButtonText, click: toggleSlideShow"></button-->
    </header>

    <section data-bind="events: { click: toggleFocus }" id="image-container">
        <div id="image-wrap">
            <div id="image-inner-wrap" data-bind="events: { click: toggleShowDetails }">
                <section data-bind="attr: { class: detailsClass }" id="details">
                    <div>
                        <h2 data-bind="text: currentPhoto.name, visible: currentPhoto.name"></h2>
                        <p data-bind="html: currentPhoto.description, visible: currentPhoto.description"></p>
                        <ul>

                            <li data-bind="visible: currentPhoto.created_at"><span>Created at</span><div data-bind="text: currentPhoto.created_at"></div></li>
                            <li data-bind="visible: currentPhoto.folding_type"><span>Folding Type</span><div data-bind="text: currentPhoto.folding_type"></div></li>
                            <li data-bind="visible: currentPhoto.folding_direction"><span>Direction</span><div data-bind="text: currentPhoto.folding_direction"></div></li>
                            <li data-bind="visible: currentPhoto.die_cute_type"><span>Die Cut</span><div data-bind="text: currentPhoto.die_cut_type"></div></li>
                            <!--
                            <li data-bind="visible: currentPhoto.iso"><span>ISO</span><div data-bind="text: currentPhoto.iso"></div></li>
                            <li data-bind="visible: currentPhoto.times_viewed"><span>Times Viewed</span><div data-bind="text: currentPhoto.times_viewed"></div></li>
                            <li data-bind="visible: currentPhoto.rating"><span>Rating</span><div data-bind="text: currentPhoto.rating + '%'"></div></li>
                            -->
                            <li data-bind="visible: currentPhoto.category_name"><span>Category</span><div data-bind="text: currentPhoto.category_name"></div></li>
                            <li data-bind="visible: currentPhoto.user.fullname"><span>Assigned to</span><div data-bind="text: currentPhoto.user.fullname"></div></li>
                            <!--
                            <li data-bind="invisible: currentPhoto.noAvatar"><span>Avatar</span><div><img data-bind="attr: { src: currentPhoto.user.userpic_url }"/></div></li>
                            -->
                        </ul>
                    </div>
                </section>
                <div class="loading" data-bind="style: { width: currentPhoto.calculatedWidth }">
                    <div class="image" data-bind="style: { backgroundImage: currentPhoto.backgroundPhoto, width: currentPhoto.calculatedWidth, visibility: currentPhoto.image_url ? 'visible' : 'hidden' } "></div>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <a data-bind="click: scrollLeft" id="scroll-left" class="scroll-arrow">&#x2039;</a>
        <div data-role="listview" data-auto-bind="false" data-bind="{ source: photos }" data-template="photo-thumb" id="photo-thumbs"></div>
        <a data-bind="click: scrollRight" id="scroll-right" class="scroll-arrow">&#x203A;</a>
    </footer>
</script>

<script type="text/x-kendo-template" id="photo-thumb">
    <a data-bind="attr: { href: thumbHref, class: thumbClass }"><img data-bind="attr: { src: image_url }"></a>
</script>
</body>
</html>
