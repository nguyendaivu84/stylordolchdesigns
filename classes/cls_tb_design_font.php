<?php

/**
 * Install font on Linux for converting from SVG to PDF using Inkscape
 * Copy the .TTF fonts into the /usr/share/fonts folder
 * ie: /usr/share/fonts/Sacramento-Regular.ttf
 *  Then, flush the fonts cache:
 *      sudo fc-cache -f -v
 *  See: http://www.inkscapeforum.com/viewtopic.php?f=5&t=16534
 */
class cls_tb_design_font{

	private $v_font_id = 0;
	private $v_font_name = '';
	private $arr_font_name = array();
	private $v_font_key = '';
	private $v_font_regular = 0;
	private $v_font_bold = 0;
	private $v_font_italic = 0;
	private $v_font_both = 0;
	private $v_created_time = '0000-00-00 00:00:00';
	private $v_font_status = 0;
	private $v_font_embed = 0;
	private $v_font_order = 0;
	private $v_location_id = 0;
	private $v_company_id = 0;
	private $v_user_id = 0;
	private $v_user_name = '';
	private $v_user_ip = '';
	private $v_user_agent = '';
	private $v_font_desc = '';
    private $arr_site_id = array();
    private $arr_font_face = array();
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
    private $db = null;
    private $v_collection_name = 'tb_design_font';

	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection($this->v_collection_name);
		$this->v_created_time = new MongoDate(time());
        $this->db = $db;
		$this->collection->ensureIndex(array("font_id"=>1), array('name'=>"font_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = $this->v_collection_name;
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "font_id" value
	 * @return int value
	 */
	public function get_font_id(){
		return (int) $this->v_font_id;
	}

	
	/**
	 * function allow change properties "font_id" value
	 * @param $p_font_id: int value
	 */
	public function set_font_id($p_font_id){
		$this->v_font_id = (int) $p_font_id;
	}

    /**
     * function return properties "site_id" value
     * @return array
     */
    public function get_site_id(){
        return $this->arr_site_id;
    }


    /**
     * function allow change properties "site_id" value
     * @param $arr_site_id: array
     */
    public function set_site_id(array $arr_site_id = array()){
        $this->arr_site_id =  $arr_site_id;
    }

    /**
	 * function return properties "font_name" value
	 * @return string value
	 */
	public function get_font_name(){
		return $this->v_font_name;
	}

	
	/**
	 * function allow change properties "font_name" value
	 * @param $p_font_name: string value
	 */
	public function set_font_name($p_font_name){
		$this->v_font_name = $p_font_name;
	}

    /**
     * function return properties "array_font_name" value
     * @return string value
     */
    public function get_array_font_name(){
        return $this->arr_font_name;
    }


    /**
     * function allow change properties "array_font_name" value
     * @param $arr_font_name: string value
     */
    public function set_array_font_name(array $arr_font_name=array()){
        $this->arr_font_name = $arr_font_name;
    }

    /**
     * function return properties "array_font_face" value
     * @return array value
     */
    public function get_font_face(){
        return $this->arr_font_face;
    }


    /**
     * function allow change properties "array_font_face" value
     * @param $arr_font_face: array value
     */
    public function set_font_face(array $arr_font_face=array()){
        $this->arr_font_face = $arr_font_face;
    }


    /**
	 * function return properties "font_key" value
	 * @return string value
	 */
	public function get_font_key(){
		return $this->v_font_key;
	}

	
	/**
	 * function allow change properties "font_key" value
	 * @param $p_font_key: string value
	 */
	public function set_font_key($p_font_key){
		$this->v_font_key = $p_font_key;
	}

	
	/**
	 * function return properties "font_regular" value
	 * @return int value
	 */
	public function get_font_regular(){
		return (int) $this->v_font_regular;
	}

	
	/**
	 * function allow change properties "font_regular" value
	 * @param $p_font_regular: int value
	 */
	public function set_font_regular($p_font_regular){
		$this->v_font_regular = (int) $p_font_regular;
	}

	
	/**
	 * function return properties "font_bold" value
	 * @return int value
	 */
	public function get_font_bold(){
		return (int) $this->v_font_bold;
	}

	
	/**
	 * function allow change properties "font_bold" value
	 * @param $p_font_bold: int value
	 */
	public function set_font_bold($p_font_bold){
		$this->v_font_bold = (int) $p_font_bold;
	}

	
	/**
	 * function return properties "font_italic" value
	 * @return int value
	 */
	public function get_font_italic(){
		return (int) $this->v_font_italic;
	}

	
	/**
	 * function allow change properties "font_italic" value
	 * @param $p_font_italic: int value
	 */
	public function set_font_italic($p_font_italic){
		$this->v_font_italic = (int) $p_font_italic;
	}

	
	/**
	 * function return properties "font_both" value
	 * @return int value
	 */
	public function get_font_both(){
		return (int) $this->v_font_both;
	}

	
	/**
	 * function allow change properties "font_both" value
	 * @param $p_font_both: int value
	 */
	public function set_font_both($p_font_both){
		$this->v_font_both = (int) $p_font_both;
	}

	
	/**
	 * function return properties "created_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_created_time(){
		return  $this->v_created_time->sec;
	}

	
	/**
	 * function allow change properties "created_time" value
	 * @param $p_created_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_created_time($p_created_time){
		if($p_created_time=='') $p_created_time = NULL;
		if(!is_null($p_created_time)){
			try{
				$this->v_created_time = new MongoDate(strtotime($p_created_time));
			}catch(MongoException $me){
				$this->v_created_time = NULL;
			}
		}else{
			$this->v_created_time = NULL;
		}
	}

	
	/**
	 * function return properties "font_status" value
	 * @return int value
	 */
	public function get_font_status(){
		return (int) $this->v_font_status;
	}

	
	/**
	 * function allow change properties "font_status" value
	 * @param $p_font_status: int value
	 */
	public function set_font_status($p_font_status){
		$this->v_font_status = (int) $p_font_status;
	}

    /**
     * function return properties "font_embed" value
     * @return int value
     */
    public function get_font_embed(){
        return (int) $this->v_font_embed;
    }


    /**
     * function allow change properties "font_embed" value
     * @param $p_font_embed: int value
     */
    public function set_font_embed($p_font_embed){
        $this->v_font_embed = (int) $p_font_embed;
    }


    /**
	 * function return properties "font_order" value
	 * @return int value
	 */
	public function get_font_order(){
		return (int) $this->v_font_order;
	}

	
	/**
	 * function allow change properties "font_order" value
	 * @param $p_font_order: int value
	 */
	public function set_font_order($p_font_order){
		$this->v_font_order = (int) $p_font_order;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "user_id" value
	 * @return int value
	 */
	public function get_user_id(){
		return (int) $this->v_user_id;
	}

	
	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: int value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = (int) $p_user_id;
	}

	
	/**
	 * function return properties "user_name" value
	 * @return string value
	 */
	public function get_user_name(){
		return $this->v_user_name;
	}

	
	/**
	 * function allow change properties "user_name" value
	 * @param $p_user_name: string value
	 */
	public function set_user_name($p_user_name){
		$this->v_user_name = $p_user_name;
	}

	
	/**
	 * function return properties "user_ip" value
	 * @return string value
	 */
	public function get_user_ip(){
		return $this->v_user_ip;
	}

	
	/**
	 * function allow change properties "user_ip" value
	 * @param $p_user_ip: string value
	 */
	public function set_user_ip($p_user_ip){
		$this->v_user_ip = $p_user_ip;
	}

	
	/**
	 * function return properties "user_agent" value
	 * @return string value
	 */
	public function get_user_agent(){
		return $this->v_user_agent;
	}

	
	/**
	 * function allow change properties "user_agent" value
	 * @param $p_user_agent: string value
	 */
	public function set_user_agent($p_user_agent){
		$this->v_user_agent = $p_user_agent;
	}

	
	/**
	 * function return properties "font_desc" value
	 * @return string value
	 */
	public function get_font_desc(){
		return $this->v_font_desc;
	}

	
	/**
	 * function allow change properties "font_desc" value
	 * @param $p_font_desc: string value
	 */
	public function set_font_desc($p_font_desc){
		$this->v_font_desc = $p_font_desc;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
        $this->v_font_id = $this->select_next('font_id');
		$arr = array('font_id' => $this->v_font_id
					,'font_name' => $this->v_font_name
					,'array_font_name' => $this->arr_font_name
					,'font_face' => $this->arr_font_face
					,'font_key' => $this->v_font_key
					,'font_regular' => $this->v_font_regular
					,'font_bold' => $this->v_font_bold
					,'font_italic' => $this->v_font_italic
					,'font_both' => $this->v_font_both
					,'created_time' => $this->v_created_time
					,'font_status' => $this->v_font_status
					,'font_embed' => $this->v_font_embed
					,'font_order' => $this->v_font_order
					,'location_id' => $this->v_location_id
					,'company_id' => $this->v_company_id
					,'user_id' => $this->v_user_id
                    ,'site_id' => $this->arr_site_id
					,'user_name' => $this->v_user_name
					,'user_ip' => $this->v_user_ip
					,'user_agent' => $this->v_user_agent
					,'font_desc' => $this->v_font_desc);
		try{
            if($this->v_font_id > 0)
			    $this->collection->insert($arr, array('safe'=>true));
			//$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_font_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return 0;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_design_font` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_font_id = isset($arr['font_id'])?$arr['font_id']:0;
			$this->v_font_name = isset($arr['font_name'])?$arr['font_name']:'';
			$this->arr_font_name = isset($arr['array_font_name'])?$arr['array_font_name']:array();
			$this->font_face = isset($arr['font_name'])?$arr['font_name']:array();
			$this->v_font_key = isset($arr['font_key'])?$arr['font_key']:'';
			$this->v_font_regular = isset($arr['font_regular'])?$arr['font_regular']:0;
			$this->v_font_bold = isset($arr['font_bold'])?$arr['font_bold']:0;
			$this->v_font_italic = isset($arr['font_italic'])?$arr['font_italic']:0;
			$this->v_font_both = isset($arr['font_both'])?$arr['font_both']:0;
			$this->v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
			$this->v_font_status = isset($arr['font_status'])?$arr['font_status']:0;
			$this->v_font_embed = isset($arr['font_embed'])?$arr['font_embed']:0;
			$this->v_font_order = isset($arr['font_order'])?$arr['font_order']:0;
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
			$this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
			$this->v_user_ip = isset($arr['user_ip'])?$arr['user_ip']:'';
			$this->v_user_agent = isset($arr['user_agent'])?$arr['user_agent']:'';
			$this->v_font_desc = isset($arr['font_desc'])?$arr['font_desc']:'';
            $this->arr_site_id = isset($arr['site_id'])?$arr['site_id']:array();
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `font_id` FROM `tb_design_font` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_scalar('font_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `font_id` FROM `tb_design_font` WHERE `user_id`=2 ORDER BY `font_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_next('font_id',array('user_id'=>2), array('font_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
        add_class('cls_tb_next_id');
        $cls_next = new cls_tb_next_id($this->db, $this->v_dir);
        $v_update_id = ((int) $v_ret) + 1;
        return $cls_next->calculate_next_id($this->v_collection_name, $p_field_name, $v_update_id);
    }
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}
	
	/**
	 * function select distinct
	 * @param $p_field_name string, name of selected field
	 * @param $db MongoDB
	 * @param $arr_where array
	 * @example:
	 * <code>
	 *         SELECT DISTINCT `name` FROM `tbl_users`
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_distinct('nam')
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_distinct(MongoDB $db, $p_field_name, array $arr_where = array()){
		//return $this->collection->command(array("distinct"=>"tb_design_font", "key"=>$p_field_name));
        return $db->command(array("distinct"=>"tb_design_template", "key"=>$p_field_name, 'query'=>$arr_where));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_font($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('font_id' => $this->v_font_id,'font_name' => $this->v_font_name, 'array_font_name'=>$this->arr_font_name, 'font_face'=>$this->arr_font_face,'font_key' => $this->v_font_key,'font_regular' => $this->v_font_regular,'font_bold' => $this->v_font_bold,'font_italic' => $this->v_font_italic,'font_both' => $this->v_font_both,'created_time' => $this->v_created_time,'font_status' => $this->v_font_status,'font_order' => $this->v_font_order,'location_id' => $this->v_location_id,'company_id' => $this->v_company_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'user_ip' => $this->v_user_ip,'user_agent' => $this->v_user_agent,'font_desc' => $this->v_font_desc, 'font_embed'=>$this->v_font_embed,'site_id' => $this->arr_site_id));
		 else 
			$arr = array('$set' => array('font_name' => $this->v_font_name, 'array_font_name'=>$this->arr_font_name, 'font_face'=>$this->arr_font_face,'font_key' => $this->v_font_key,'font_regular' => $this->v_font_regular,'font_bold' => $this->v_font_bold,'font_italic' => $this->v_font_italic,'font_both' => $this->v_font_both,'created_time' => $this->v_created_time,'font_status' => $this->v_font_status,'font_order' => $this->v_font_order,'location_id' => $this->v_location_id,'company_id' => $this->v_company_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'user_ip' => $this->v_user_ip,'user_agent' => $this->v_user_agent,'font_desc' => $this->v_font_desc, 'font_embed'=>$this->v_font_embed,'site_id' => $this->arr_site_id));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}

    /**
     * @param cls_font_attribute $cls_att
     * @param int $p_font_id
     * @param string $p_font_dir
     * @param string $p_ds
     * @return array
     */
    public function update_font_name(cls_font_attribute $cls_att, $p_font_id=0 , $p_font_dir = DESIGN_FONT_DIR, $p_ds = DS){
        $v_row = 1;
        $arr_return = array();
        if($p_font_id!=0) $v_row = $this->select_one(array('font_id'=>$p_font_id));
        if($v_row==1){
            $v_font_name = $this->get_font_name();
            $v_font_id = $this->get_font_id();
            $v_font_key = $this->get_font_key();
            $v_font_regular = $this->get_font_regular();
            $v_font_italic = $this->get_font_italic();
            $v_font_bold = $this->get_font_bold();
            $v_font_both = $this->get_font_both();
            $v_full = true;
            if($v_font_regular==1){
                if(!isset($arr_font_name['regular'])){
                    $v_font = $p_font_dir.$p_ds.$v_font_key.$p_ds.'regular.ttf';
                    if(file_exists($v_font)){
                        $cls_att->setFontFile($v_font);
                        if($cls_att->readFontAttributes()){
                            $arr_return['regular'] = $cls_att->getFontFamily();
                            $v_full = false;
                        }else $arr_return['regular'] = $v_font_name;
                    }
                }else $arr_return['regular'] = $v_font_name;
            }
            if($v_font_italic==1){
                if(!isset($arr_font_name['italic'])){
                    $v_font = $p_font_dir.$p_ds.$v_font_key.$p_ds.'italic.ttf';
                    if(file_exists($v_font)){
                        $cls_att->setFontFile($v_font);
                        if($cls_att->readFontAttributes()){
                            $arr_return['italic'] = $cls_att->getFontFamily();
                            $v_full = false;
                        }else $arr_return['italic'] = $v_font_name;
                    }
                }else $arr_return['italic'] = $v_font_name;
            }
            if($v_font_bold==1){
                if(!isset($arr_font_name['bold'])){
                    $v_font = $p_font_dir.$p_ds.$v_font_key.$p_ds.'bold.ttf';
                    if(file_exists($v_font)){
                        $cls_att->setFontFile($v_font);
                        if($cls_att->readFontAttributes()){
                            $arr_return['bold'] = $cls_att->getFontFamily();
                            $v_full = false;
                        }else $arr_return['bold'] = $v_font_name;
                    }
                }else $arr_return['bold'] = $v_font_name;
            }
            if($v_font_both==1){
                if(!isset($arr_font_name['both'])){
                    $v_font = $p_font_dir.$p_ds.$v_font_key.$p_ds.'both.ttf';
                    if(file_exists($v_font)){
                        $cls_att->setFontFile($v_font);
                        if($cls_att->readFontAttributes()){
                            $arr_return['both'] = $cls_att->getFontFamily();
                            $v_full = false;
                        }else $arr_return['both'] = $v_font_name;
                    }
                }else $arr_return['both'] = $v_font_name;
            }
            if(!$v_full){
                $this->update_field('array_font_name', $arr_return, array('font_id'=>$v_font_id));
            }
        }
        return $arr_return;
    }

    public function update_upload_font_face(array $arr_font, $p_font_dir = DESIGN_FONT_DIR, $p_root_dir = ROOT_DIR, $p_ds = DS){
        $v_font_id = $this->get_font_id();
        $v_font_embed = $this->get_font_embed();
        $v_name = $this->get_font_name();
        $v_font_key = $this->get_font_key();
        $v_font_dir = $p_font_dir.$p_ds.$v_font_key.$p_ds;
        $v_font_url = str_replace($p_root_dir.$p_ds,'', $v_font_dir);
        $v_font_url = str_replace($p_ds, '/', $v_font_url);
        $arr_font_face = array();
        if($v_font_embed==1){
            $v_regular = isset($arr_font['regular'])?($arr_font['regular'] || $this->get_font_regular()):0;
            $v_italic = isset($arr_font['italic'])?($arr_font['italic'] || $this->get_font_italic()):0;
            $v_bold = isset($arr_font['bold'])?($arr_font['bold'] || $this->get_font_bold()):0;
            $v_both = isset($arr_font['both'])?($arr_font['both'] || $this->get_font_both()):0;

            if($v_regular){
                $arr_font_face['regular'] = $this->generate_font_face($v_name.' Regular','regular', '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'regular.ttf'),  file_exists($v_font_dir.'regular.eot'));
            }
            if($v_italic){
                $arr_font_face['italic'] = $this->generate_font_face($v_name.' Italic','italic' , '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'italic.ttf'),  file_exists($v_font_dir.'regular.eot'), array('italic'=>1));
            }
            if($v_bold){
                $arr_font_face['bold'] = $this->generate_font_face($v_name.' Bold', 'bold',  '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'bold.ttf'),  file_exists($v_font_dir.'bold.eot'), array('bold'=>1));
            }
            if($v_both){
                $arr_font_face['both'] = $this->generate_font_face($v_name.' Both', 'both', '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'both.ttf'),  file_exists($v_font_dir.'both.eot'), array('italic'=>1, 'bold'=>1));
            }
        }
        $this->update_field('font_face', $arr_font_face, array('font_id'=>$v_font_id));
    }

    public function update_font_face($p_convert_path, $p_font_dir = DESIGN_FONT_DIR, $p_root_dir = ROOT_DIR, $p_ds = DS){
        $v_font_id = $this->get_font_id();
        $v_font_embed = $this->get_font_embed();
        $v_italic = $this->get_font_italic();
        $v_regular = $this->get_font_regular();
        $v_name = $this->get_font_name();
        $v_bold = $this->get_font_bold();
        $v_both = $this->get_font_both();
        $v_font_key = $this->get_font_key();
        $v_font_dir = $p_font_dir.$p_ds.$v_font_key.$p_ds;
        $v_font_url = str_replace($p_root_dir.$p_ds,'', $v_font_dir);
        $v_font_url = str_replace($p_ds, '/', $v_font_url);
        if($v_font_embed==1){
            $arr_font_name = $this->get_array_font_name();
            $arr_font_face = array();
            if($v_regular){
                if(file_exists($v_font_dir.'regular.eot')) @unlink($v_font_dir.'regular.eot');
                shell_exec('"'.$p_convert_path.'" "'.$v_font_dir.'regular.ttf" "'.$v_font_dir.'regular.eot"');
                $arr_font_face['regular'] = $this->generate_font_face($v_name.' Regular','regular', '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'regular.eot'),  file_exists($v_font_dir.'regular.ttf'));
            }
            if($v_italic){
                if(file_exists($v_font_dir.'italic.eot')) @unlink($v_font_dir.'italic.eot');
                shell_exec('"'.$p_convert_path.'" "'.$v_font_dir.'italic.ttf" "'.$v_font_dir.'italic.eot"');
                $arr_font_face['italic'] = $this->generate_font_face($v_name.' Italic','italic' , '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'italic.eot'),  file_exists($v_font_dir.'italic.ttf'), array('italic'=>1));
            }
            if($v_bold){
                if(file_exists($v_font_dir.'bold.eot')) @unlink($v_font_dir.'bold.eot');
                shell_exec('"'.$p_convert_path.'" "'.$v_font_dir.'bold.ttf" "'.$v_font_dir.'bold.eot"');
                $arr_font_face['bold'] = $this->generate_font_face($v_name.' Bold', 'bold',  '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'bold.eot'),  file_exists($v_font_dir.'bold.ttf'), array('bold'=>1));
            }
            if($v_both){
                if(file_exists($v_font_dir.'both.eot')) @unlink($v_font_dir.'both.eot');
                shell_exec('"'.$p_convert_path.'" "'.$v_font_dir.'both.ttf" "'.$v_font_dir.'both.eot"');
                $arr_font_face['both'] = $this->generate_font_face($v_name.' Both', 'both', '[@URL]'.$v_font_url, '[@URL]'.$v_font_url, file_exists($v_font_dir.'both.eot'),  file_exists($v_font_dir.'both.ttf'), array('italic'=>1, 'bold'=>1));
            }
        }else{
            /*
            if($v_regular){
                if(file_exists($v_font_dir.'regular.eot')) @unlink($v_font_dir.'regular.eot');
            }
            if($v_italic){
                if(file_exists($v_font_dir.'italic.eot')) @unlink($v_font_dir.'italic.eot');
            }
            if($v_bold){
                if(file_exists($v_font_dir.'bold.eot')) @unlink($v_font_dir.'bold.eot');
            }
            if($v_both){
                if(file_exists($v_font_dir.'both.eot')) @unlink($v_font_dir.'both.eot');
            }
            */
            $arr_font_face = array();
        }
        $this->update_field('font_face', $arr_font_face, array('font_id'=>$v_font_id));
    }
    public function generate_font_face($p_font_name, $p_font_file, $p_ttf_path, $p_eot_path, $p_tff = true, $p_eot = true, array $arr_style = array()){
        $v_font_face = "\r\n@font-face{";
        $v_font_face .= "\r\n\tfont-family: \"".$p_font_name."\";";
        if(sizeof($arr_style)==0){
            $v_font_face .= "\r\n\tfont-style: normal;";
            $v_font_face .= "\r\n\tfont-weight: normal;";
        }else{
            if(isset($arr_style['italic']) && isset($arr_style['bold'])){
                $v_font_face .= "\r\n\tfont-style: italic;";
                $v_font_face .= "\r\n\tfont-weight: bold;";
            }else if(isset($arr_style['italic'])){
                $v_font_face .= "\r\n\tfont-style: italic;";
                $v_font_face .= "\r\n\tfont-weight: normal;";
            }else if(isset($arr_style['bold'])){
                $v_font_face .= "\r\n\tfont-style: normal;";
                $v_font_face .= "\r\n\tfont-weight: bold;";
            }
        }
        if($p_eot)
            $v_font_face .= "\r\n\tsrc: url('".$p_eot_path. $p_font_file. ".eot');";
        if($p_tff)
            $v_font_face .= "\r\n\tsrc: local('".$p_font_name."'), local('".trim(str_ireplace($p_font_file,'',$p_font_name))."'), url('".$p_ttf_path. $p_font_file. ".ttf') format(\"truetype\");";
        $v_font_face .= "\r\n}";
        return $v_font_face;
    }

    public function get_selected_font_face($p_site_id){
        $v_return = '';
        $arr_where = array();
        $arr_where['site_id'] = array('$in'=>array($p_site_id));
        $arr_fonts = $this->select($arr_where);
        foreach($arr_fonts as $arr){
            $arr_font_face = isset($arr['font_face'])?$arr['font_face']:array();
            if(is_array($arr_font_face)){
                foreach($arr_font_face as $key=>$value){
                    $v_return .= $value;
                }
            }
        }
        return $v_return;
    }
}
?>