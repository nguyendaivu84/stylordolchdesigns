<?php
/**
 * Created by PhpStorm.
 * Creator: Ho Thanh Hai
 * Date: 10/14/2014
 * Time: 4:31 PM
 * Left Top Corner is (0, 0), Left Bottom is (w, h), Right Top Corner is (w, 0)
 */

class Point{
    private $x0 = 0;
    private $y0 = 0;

    public function __construct($x, $y){
        $this->x0 = $x;
        $this->y0 = $y;
    }

    public function getX(){
        return $this->x0;
    }

    public function getY(){
        return $this->y0;
    }

    public function setX($x){
        $this->x0 = floatval($x);
    }

    public function setY($y){
        $this->y0 = floatval($y);
    }

    public function setPoint(Point $p){
        $this->x0 = $p->x0;
        $this->y0 = $p->y0;
    }

    public function distance(Point $p){
        return pow(pow($this->x0 - $p->getX(), 2) + pow($this->y0 - $p->getY(), 2), 0.5);
    }

    public function getSymmetricAxisXY(Point $p){
        $dx = $p->getX() - $this->getX();
        $dy = $p->getY() - $this->getY();
        $new_x = $this->getX() - $dx;
        $new_y = $this->getY() - $dy;
        return new Point($new_x, $new_y);
    }

    public function getSymmetricAxisX(Point $p){
        $dx = $p->getX() - $this->getX();
        $new_x = $this->getX() - $dx;
        return new Point($new_x, $p->getY());
    }

    public function getSymmetricAxisY(Point $p){
        $dy = $p->getY() - $this->getY();
        $new_y = $this->getY() - $dy;
        return new Point($p->getX(), $new_y);
    }

    /*
     * Rotate clockwise
     */
    public function getRotatePoint($angle = 30){
        $rad = $angle * M_PI / 180;
        $cos = cos($rad);
        $sin = sin($rad);
        //return new Point(round($this->getY() * $sin - $this->getX() * $cos, 5), - round($this->getX() * $sin + $this->getY() * $cos, 5));
        return new Point(round($this->getX() * $cos - $this->getY() * $sin, 20), round($this->getX() * $sin + $this->getY() * $cos, 20));
    }

    public function getRotatePointAroundThis(Point $rotate, $angle = 30){
        $p1 = new Point($rotate->getX() - $this->getX(), $rotate->getY() - $this->getY());
        $p = $p1->getRotatePoint($angle);
        return new Point($this->getX() + $p->getX(), $this->getY() + $p->getY());
    }
}

class Ellipse{
    private $x = 0;
    private $y = 0;
    private $rx = 10;
    private $ry = 20;
    private $point = null;

    public function __construct(Point $p, $rx, $ry){
        $this->point = $p;
        $this->x = $p->getX();
        $this->y = $p->getY();
        $this->rx = $rx;
        $this->ry = $ry;
    }

    public function getPoint($deg){
        $rad = $deg * M_PI / 180;
        $t = pow(tan($rad),2);
        $tmp = 1 / pow($this->rx, 2) + $t / pow($this->ry ,2);
        //$x = pow(1 / $tmp, 0.5);
        $x = $this->rx * $this->ry / pow(pow($this->ry, 2) + pow($this->rx, 2) * $t, 0.5);
        $y = $this->rx * $this->ry / pow(pow($this->rx, 2) + pow($this->ry, 2) / $t, 0.5);
        //$y = pow((1 - pow($x, 2) / pow($this->rx, 2)) * pow($this->ry, 2), 0.5);
        return new Point($x + $this->point->getX(), $y + $this->point->getY());
    }

    public function get360Points($to_array = false){
        $p = array();
        $j = 0;
        for($i=0; $i<=90; $i++){
            if($i==0){
                $p[$j++] = new Point($this->rx + $this->point->getX(), $this->point->getY());
                continue;
            }else if($i==90){
                $p[$j++] = new Point($this->point->getX(), $this->point->getY() + $this->ry);
                continue;
            }
            $node = $this->getPoint($i);
            $p[$j++] = $node;
            $tmp = $this->point->getSymmetricAxisX($node);// getAxisX($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            $tmp = $this->point->getSymmetricAxisY($node);// getAxisY($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            $tmp = $this->point->getSymmetricAxisXY($node);// getAxisXY($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            /*
            $tmp = $this->getAxisX($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            $tmp = $this->getAxisY($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            $tmp = $this->getAxisXY($node);
            if(!$this->find($p, $tmp)) {
                $p[$j++] = $tmp;
            }
            */
        }
        if($to_array)
            return $this->toArray($p);
        else
            return $p;
    }

    private function getAxisXY(Point $p){
        $dx = $p->getX() - $this->x;
        $dy = $p->getY() - $this->y;
        $new_x = $this->x - $dx;
        $new_y = $this->y - $dy;
        return new Point($new_x, $new_y);
    }

    private function getAxisX(Point $p){
        $dx = $p->getX() - $this->x;
        $new_x = $this->x - $dx;
        return new Point($new_x, $p->getY());
    }

    private function getAxisY(Point $p){
        $dy = $p->getY() - $this->y;
        $new_y = $this->y - $dy;
        return new Point($p->getX(), $new_y);
    }

    private function  find(array $p, Point $point){
        $found = false;
        $i=0;
        while(!$found && $i<sizeof($p)){
            $found = $p[$i]->getX()==$point->getX() && $p[$i]->getY()==$point->getY();
            $i++;
        }
        return $found;
    }

    public function toArray($p){
        $arr_return = array();
        for($i=0; $i<sizeof($p);$i++){
            if($p[$i] instanceof Point){
                $arr_return[] = array('x'=>$p[$i]->getX(), 'y'=>$p[$i]->getY());
            }
        }
        return $arr_return;
    }

    public function get360PointRotate($deg, $to_array = false){
        $p = $this->get360Points();
        for($i=0; $i<sizeof($p);$i++){
            $p[$i] = $this->point->getRotatePointAroundThis($p[$i], $deg);
        }
        if($to_array)
            return $this->toArray($p);
        else
            return $p;
    }

    public function getMaxSize($deg){
        $v_max_width = 0;
        $v_max_height = 0;
        $p = $this->get360PointRotate($deg);
        for($i=0; $i <sizeof($p) - 1; $i++){
            for($j=$i+1; $j<sizeof($p); $j++){
                if($v_max_width < abs($p[$j]->getX() - $p[$i]->getX())){
                    $v_max_width = abs($p[$j]->getX() - $p[$i]->getX());
                }

                if($v_max_height < abs($p[$j]->getY() - $p[$i]->getY())){
                    $v_max_height = abs($p[$j]->getY() - $p[$i]->getY());
                }

            }
        }
        return array('max_x'=>$v_max_width, 'max_y'=>$v_max_height);
    }

}