<?php
class cls_validation{
    private $v_file_name_length = 50;
    private $v_max_file_size = 2097152;//2 MB
    private $arr_file_extension = array('jpg', 'png', 'gif');

    public function set_file_name_length($p_length = 50){
        $this->v_file_name_length = $p_length;
    }
    public function set_max_file_size($p_max_file_size = 2097152){
        $this->v_max_file_size = $p_max_file_size;
    }

    public  function set_file_extension(array $arr_extension = array('jpg', 'png', 'gif')){
        for($i=0; $i<count($arr_extension); $i++)
            $arr_extension[$i] = strtolower($arr_extension[$i]);
        $this->arr_file_extension = $arr_extension;
    }

    public function check_file_extension($object){
        $v_file_name = $object->file['original_filename'].'';
        $v_pos = strrpos($v_file_name,'.');
        $v_ext = $v_pos>0?substr($v_file_name, $v_pos+1):'';
        $v_ext = strtolower($v_ext);
        if(!in_array($v_ext, $this->arr_file_extension)){
            $object->set_error('File extension not allow.');
        }
    }

    public function check_max_file_size($object){
        $v_full_path = $object->destination.$object->filename;
        if(file_exists($v_full_path)){
            $v_size = filesize($v_full_path);
            if($v_size > $this->v_max_file_size){
                $object->set_error('File size is bigger.');
            }
        }
    }
    public function check_file_name_length($object){
        if (strlen($object->file['original_filename']) > $this->v_file_name_length) {
            $object->set_error('File name is too long.');
        }
    }
}