<?php
class cls_tb_design_theme{

	private $v_theme_id = 0;
	private $v_theme_name = '';
	private $v_theme_status = 0;
	private $v_theme_color = '';
	private $arr_list_color = array();
	private $arr_list_image = array();
	private $v_add_markup_cost = 0;
	private $v_created_time = '0000-00-00 00:00:00';
	private $v_last_modified = '0000-00-00 00:00:00';
	private $v_last_used = '0000-00-00 00:00:00';
	private $v_template_id = 0;
	private $v_sample_image = '';
	private $v_saved_dir = '';
	private $v_company_id = 0;
	private $v_location_id = 0;
	private $v_user_id = 0;
	private $v_theme_order = 0;
	private $v_user_name = '';
	private $v_theme_desc = '';
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
    private $db = null;
    private $v_collection_name = 'tb_design_theme';

	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection($this->v_collection_name);
		$this->v_created_time = new MongoDate(time());
        $this->db = $db;
		$this->collection->ensureIndex(array("theme_id"=>1), array('name'=>"theme_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = $this->v_collection_name;
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}
	
	/**
	 * function return properties "theme_id" value
	 * @return int value
	 */
	public function get_theme_id(){
		return (int) $this->v_theme_id;
	}

	
	/**
	 * function allow change properties "theme_id" value
	 * @param $p_theme_id: int value
	 */
	public function set_theme_id($p_theme_id){
		$this->v_theme_id = (int) $p_theme_id;
	}

    /**
     * function return properties "theme_order" value
     * @return int value
     */
    public function get_theme_order(){
        return (int) $this->v_theme_order;
    }


    /**
     * function allow change properties "theme_order" value
     * @param $p_theme_order: int value
     */
    public function set_theme_order($p_theme_order){
        $this->v_theme_order = (int) $p_theme_order;
    }


    /**
	 * function return properties "theme_name" value
	 * @return string value
	 */
	public function get_theme_name(){
		return $this->v_theme_name;
	}

	
	/**
	 * function allow change properties "theme_name" value
	 * @param $p_theme_name: string value
	 */
	public function set_theme_name($p_theme_name){
		$this->v_theme_name = $p_theme_name;
	}

	
	/**
	 * function return properties "theme_status" value
	 * @return int value
	 */
	public function get_theme_status(){
		return (int) $this->v_theme_status;
	}

	
	/**
	 * function allow change properties "theme_status" value
	 * @param $p_theme_status: int value
	 */
	public function set_theme_status($p_theme_status){
		$this->v_theme_status = (int) $p_theme_status;
	}

	
	/**
	 * function return properties "theme_color" value
	 * @return string value
	 */
	public function get_theme_color(){
		return $this->v_theme_color;
	}

	
	/**
	 * function allow change properties "theme_color" value
	 * @param $p_theme_color: string value
	 */
	public function set_theme_color($p_theme_color){
		$this->v_theme_color = $p_theme_color;
	}

	
	/**
	 * function return properties "list_color" value
	 * @return array value
	 */
	public function get_list_color(){
		return  $this->arr_list_color;
	}

	
	/**
	 * function allow change properties "list_color" value
	 * @param $arr_list_color array
	 */
	public function set_list_color(array $arr_list_color = array()){
		$this->arr_list_color = $arr_list_color;
	}

	
	/**
	 * function return properties "list_image" value
	 * @return array value
	 */
	public function get_list_image(){
		return  $this->arr_list_image;
	}

	
	/**
	 * function allow change properties "list_image" value
	 * @param $arr_list_image array
	 */
	public function set_list_image(array $arr_list_image = array()){
		$this->arr_list_image = $arr_list_image;
	}

	
	/**
	 * function return properties "add_markup_cost" value
	 * @return float value
	 */
	public function get_add_markup_cost(){
		return (float) $this->v_add_markup_cost;
	}

	
	/**
	 * function allow change properties "add_markup_cost" value
	 * @param $p_add_markup_cost: float value
	 */
	public function set_add_markup_cost($p_add_markup_cost){
		$this->v_add_markup_cost = (float) $p_add_markup_cost;
	}

	
	/**
	 * function return properties "created_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_created_time(){
		return  $this->v_created_time->sec;
	}

	
	/**
	 * function allow change properties "created_time" value
	 * @param $p_created_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_created_time($p_created_time){
		if($p_created_time=='') $p_created_time = NULL;
		if(!is_null($p_created_time)){
			try{
				$this->v_created_time = new MongoDate(strtotime($p_created_time));
			}catch(MongoException $me){
				$this->v_created_time = NULL;
			}
		}else{
			$this->v_created_time = NULL;
		}
	}

    /**
     * function return properties "last_used" value
     * @return int value indicates amount of seconds
     */
    public function get_last_used(){
        return  $this->v_last_used->sec;
    }


    /**
     * function allow change properties "last_used" value
     * @param $p_last_used: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_last_used($p_last_used){
        if($p_last_used=='') $p_last_used = NULL;
        if(!is_null($p_last_used)){
            try{
                $this->v_last_used = new MongoDate(strtotime($p_last_used));
            }catch(MongoException $me){
                $this->v_last_used = NULL;
            }
        }else{
            $this->v_last_used = NULL;
        }
    }

    /**
     * function return properties "last_modified" value
     * @return int value indicates amount of seconds
     */
    public function get_last_modified(){
        return  $this->v_last_modified->sec;
    }


    /**
     * function allow change properties "last_modified" value
     * @param $p_last_modified: string value format type: yyyy-mm-dd H:i:s
     */
    public function set_last_modified($p_last_modified){
        if($p_last_modified=='') $p_last_modified = NULL;
        if(!is_null($p_last_modified)){
            try{
                $this->v_last_modified = new MongoDate(strtotime($p_last_modified));
            }catch(MongoException $me){
                $this->v_last_modified = NULL;
            }
        }else{
            $this->v_last_modified = NULL;
        }
    }


    /**
	 * function return properties "template_id" value
	 * @return int value
	 */
	public function get_template_id(){
		return (int) $this->v_template_id;
	}

	
	/**
	 * function allow change properties "template_id" value
	 * @param $p_template_id: int value
	 */
	public function set_template_id($p_template_id){
		$this->v_template_id = (int) $p_template_id;
	}

	
	/**
	 * function return properties "sample_image" value
	 * @return string value
	 */
	public function get_sample_image(){
		return $this->v_sample_image;
	}

	
	/**
	 * function allow change properties "sample_image" value
	 * @param $p_sample_image: string value
	 */
	public function set_sample_image($p_sample_image){
		$this->v_sample_image = $p_sample_image;
	}

	
	/**
	 * function return properties "saved_dir" value
	 * @return string value
	 */
	public function get_saved_dir(){
		return $this->v_saved_dir;
	}

	
	/**
	 * function allow change properties "saved_dir" value
	 * @param $p_saved_dir: string value
	 */
	public function set_saved_dir($p_saved_dir){
		$this->v_saved_dir = $p_saved_dir;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return properties "user_id" value
	 * @return int value
	 */
	public function get_user_id(){
		return (int) $this->v_user_id;
	}

	
	/**
	 * function allow change properties "user_id" value
	 * @param $p_user_id: int value
	 */
	public function set_user_id($p_user_id){
		$this->v_user_id = (int) $p_user_id;
	}

	
	/**
	 * function return properties "user_name" value
	 * @return string value
	 */
	public function get_user_name(){
		return $this->v_user_name;
	}

	
	/**
	 * function allow change properties "user_name" value
	 * @param $p_user_name: string value
	 */
	public function set_user_name($p_user_name){
		$this->v_user_name = $p_user_name;
	}

	
	/**
	 * function return properties "theme_desc" value
	 * @return string value
	 */
	public function get_theme_desc(){
		return $this->v_theme_desc;
	}

	
	/**
	 * function allow change properties "theme_desc" value
	 * @param $p_theme_desc: string value
	 */
	public function set_theme_desc($p_theme_desc){
		$this->v_theme_desc = $p_theme_desc;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
        $this->v_theme_id = $this->select_next('theme_id');
		$arr = array('theme_id' => $this->v_theme_id
					,'theme_name' => $this->v_theme_name
					,'theme_status' => $this->v_theme_status
					,'theme_color' => $this->v_theme_color
					,'list_color' => $this->arr_list_color
					,'list_image' => $this->arr_list_image
					,'add_markup_cost' => $this->v_add_markup_cost
					,'created_time' => $this->v_created_time
                    ,'last_used' => $this->v_last_used
                    ,'last_modified' => $this->v_last_modified
					,'template_id' => $this->v_template_id
					,'sample_image' => $this->v_sample_image
					,'saved_dir' => $this->v_saved_dir
					,'company_id' => $this->v_company_id
					,'location_id' => $this->v_location_id
					,'theme_order' => $this->v_theme_order
					,'user_id' => $this->v_user_id
					,'user_name' => $this->v_user_name
					,'theme_desc' => $this->v_theme_desc);
		try{
            if($this->v_theme_id > 0)
			    $this->collection->insert($arr, array('safe'=>true));
			//$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_theme_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return 0;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_design_theme` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_theme_id = isset($arr['theme_id'])?$arr['theme_id']:0;
			$this->v_theme_name = isset($arr['theme_name'])?$arr['theme_name']:'';
			$this->v_theme_status = isset($arr['theme_status'])?$arr['theme_status']:0;
			$this->v_theme_color = isset($arr['theme_color'])?$arr['theme_color']:'';
			$this->arr_list_color = isset($arr['list_color'])?$arr['list_color']:array();
			$this->arr_list_image = isset($arr['list_image'])?$arr['list_image']:array();
			$this->v_add_markup_cost = isset($arr['add_markup_cost'])?$arr['add_markup_cost']:0;
			$this->v_created_time = isset($arr['created_time'])?$arr['created_time']:(new MongoDate(time()));
            $this->v_last_used = isset($arr['last_used'])?$arr['last_used']:(new MongoDate(time()));
            $this->v_last_modified = isset($arr['last_modified'])?$arr['last_modified']:(new MongoDate(time()));
			$this->v_template_id = isset($arr['template_id'])?$arr['template_id']:0;
			$this->v_sample_image = isset($arr['sample_image'])?$arr['sample_image']:'';
			$this->v_saved_dir = isset($arr['saved_dir'])?$arr['saved_dir']:'';
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->v_user_id = isset($arr['user_id'])?$arr['user_id']:0;
			$this->v_theme_order = isset($arr['theme_order'])?$arr['theme_order']:0;
			$this->v_user_name = isset($arr['user_name'])?$arr['user_name']:'';
			$this->v_theme_desc = isset($arr['theme_desc'])?$arr['theme_desc']:'';
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `theme_id` FROM `tb_design_theme` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select_scalar('theme_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `theme_id` FROM `tb_design_theme` WHERE `user_id`=2 ORDER BY `theme_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select_next('theme_id',array('user_id'=>2), array('theme_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
        add_class('cls_tb_next_id');
        $cls_next = new cls_tb_next_id($this->db, $this->v_dir);
        $v_update_id = ((int) $v_ret) + 1;
        return $cls_next->calculate_next_id($this->v_collection_name, $p_field_name, $v_update_id);
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}

    /**
     * @param MongoDB $db
     * @param $p_field_name
     * @param array $arr_where
     * @return array
     */
    public function select_distinct(MongoDB $db, $p_field_name, array $arr_where = array()){
		return $db->command(array("distinct"=>"tb_design_theme", "key"=>$p_field_name, 'query'=>$arr_where));
		//return $this->collection->command(array("distinct"=>"tb_design_theme", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_theme($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('theme_id' => $this->v_theme_id,'theme_name' => $this->v_theme_name,'theme_status' => $this->v_theme_status,'theme_color' => $this->v_theme_color,'list_color' => $this->arr_list_color,'list_image' => $this->arr_list_image,'add_markup_cost' => $this->v_add_markup_cost,'created_time' => $this->v_created_time,'template_id' => $this->v_template_id,'sample_image' => $this->v_sample_image,'saved_dir' => $this->v_saved_dir,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'theme_desc' => $this->v_theme_desc, 'last_used'=>$this->v_last_used, 'last_modified'=>$this->v_last_modified, 'theme_order'=>$this->v_theme_order));
		 else 
			$arr = array('$set' => array('theme_name' => $this->v_theme_name,'theme_status' => $this->v_theme_status,'theme_color' => $this->v_theme_color,'list_color' => $this->arr_list_color,'list_image' => $this->arr_list_image,'add_markup_cost' => $this->v_add_markup_cost,'created_time' => $this->v_created_time,'template_id' => $this->v_template_id,'sample_image' => $this->v_sample_image,'saved_dir' => $this->v_saved_dir,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id,'user_id' => $this->v_user_id,'user_name' => $this->v_user_name,'theme_desc' => $this->v_theme_desc, 'last_used'=>$this->v_last_used, 'last_modified'=>$this->v_last_modified, 'theme_order'=>$this->v_theme_order));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}
}
?>