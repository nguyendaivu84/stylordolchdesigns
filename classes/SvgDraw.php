<?php
/**
 * Created by PhpStorm.
 * Creator: Ho Thanh Hai
 * Date: 10/16/2014
 * Time: 2:29 PM
 */
require_once 'Point.php';

class SvgDraw {
    private $svg_data = '';
    private $title = 'SVG Type';
    private $width = 200;
    private $height = 200;
    private $transform = '';
    private $used_cmyk = false;

    public function __construct($title){
        $this->title = $title;
    }

    public function setUsedCMYK($used_cmyk = true){
        $this->used_cmyk = $used_cmyk;
    }

    private function createSVG(SimpleXMLElement $body){
        $svg = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"></svg>');
        $svg->addAttribute('width', $this->width);
        $svg->addAttribute('height', $this->height);
        $svg->addChild('title', $this->title);
        $g = $svg->addChild('g');
        if($this->transform!=''){
            $g->addAttribute('transform', $this->transform);
        }
        $v_name = $body->getName();
        $node = $g->addChild($v_name);
        foreach($body->attributes() as $attr=>$value)
            $node->addAttribute($attr, $value);
        $this->svg_data = $svg->asXML();
        return $this->svg_data;
    }

    public function getSvgData(){
        return $this->svg_data;
    }

    private function createImage($data){
        try{
            $image = new Imagick();
            $image->setBackgroundColor(new ImagickPixel('transparent'));
            $image->setResolution(90, 90);
            $image->readImageBlob($data);
            $image->setImageFormat('png32');
            return $image;
        }catch (Exception $e){
            return null;
        }
    }

    private function hex2CYMK($p_hex, $p_default_hex='000000'){
        $arr_rgb = $this->hex2RGB($p_hex, $p_default_hex);
        return $this->rgb2CMYK($arr_rgb['r'], $arr_rgb['g'], $arr_rgb['b']);
    }

    private function hex2RGB($p_hex, $p_default_hex='000000'){
        $v_check = '0123456789ABCDEF';
        $v_hex = strtoupper($p_hex);
        if(strlen($v_hex)!=6)
            $v_hex = $p_default_hex;
        else{
            $v_flag = false;
            $i = 0;
            while($i<6 && !$v_flag){
                $c = substr($v_hex, $i, 1);
                $v_flag = strpos($v_check, $c)===false;
                $i++;
            }
            if($v_flag) $v_hex = $p_default_hex;
        }
        if(strlen($p_hex)==3){
            $v_red = $this->hex2Decimal(substr($v_hex,0, 1));
            $v_green = $this->hex2Decimal(substr($v_hex,1, 1));
            $v_blue = $this->hex2Decimal(substr($v_hex,2, 1));
        }else if(strlen($p_hex)==6){
            $v_red = $this->hex2Decimal(substr($v_hex,0, 2));
            $v_green = $this->hex2Decimal(substr($v_hex,3, 2));
            $v_blue = $this->hex2Decimal(substr($v_hex,5, 2));
        }else{
            $v_red = null;
            $v_green = null;
            $v_blue = null;
        }
        return array('r'=>$v_red, 'g'=>$v_green, 'b'=>$v_blue);
    }

    private function hex2Decimal($p_hex){
        $arr_hex = array(
            '0'=>0, '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, 'A'=>10, 'B'=>11, 'C'=>12, 'D'=>13, 'E'=>14, 'F'=>15
        );
        $p_hex = strtoupper($p_hex);
        $L = strlen($p_hex);
        $r = 0;
        $i = 0;
        while($i<$L){
            $p = $L - $i - 1;
            $c = substr($p_hex, $i, 1);
            if(isset($arr_hex[$c]))
                $r += pow(16, $p) * $arr_hex[$c];
            $i++;
        }
        return $r;
    }

    private function rgb2CMYK($p_red=255, $p_green = 255, $p_blue = 255){
        $cyan = 255 - $p_red;
        $magenta = 255 - $p_green;
        $yellow = 255 - $p_blue;
        $black = min($cyan, $magenta, $yellow);
        if($black!=255){
            $cyan = @(($cyan - $black) / (255 - $black)) * 255;
            $magenta = @(($magenta - $black) / (255 - $black)) * 255;
            $yellow = @(($yellow - $black) / (255 - $black)) * 255;
        }
        return array('c' => round($cyan / 255,0), 'm' => round($magenta / 255,0), 'y' => round($yellow / 255,0), 'k' => round($black / 255,0));
    }

    private function createDashArray($type, $width){
        $v_dash = 'none';
        $v_cap = 'butt';
        switch($type){
            case 'round_dot':
                $v_dash = '1 '.($width * 2);
                $v_cap = 'round';
                break;
            case 'square_dot':
                $v_dash = $width.' '.$width;
                break;
            case 'dash':
                $v_dash = ($width*6) .' ' . ($width*2);
                break;
            case 'dash_dot':
                $v_dash = ($width*6) . ' ' . ($width*2) . ' ' . $width . ' ' . ($width*2);
                break;
            case 'long_dash':
                $v_dash = ($width*8) . ' ' . ($width*2);
                break;
            case 'long_dash_dot':
                $v_dash = ($width*8) . ' ' . ($width*2) . ' ' . ($width*4) . ' ' . ($width*2) . ' ' . $width  . ' ' . ($width*2);
                break;
            default:
                break;
        }
        return array('dasharray'=>$v_dash, 'linecap'=>$v_cap);
    }

    public function createSVGTriangle(array $arr_info){
        $v_name = isset($arr_info['name']) ? $arr_info['name'] : 'triangle_' . rand(0, 1000);
        $v_width = isset($arr_info['width']) ? $arr_info['width'] : 400;
        $v_height = isset($arr_info['height']) ? $arr_info['height'] : 200;
        $v_rotation = isset($arr_info['rotation']) ? $arr_info['rotation'] : 40;
        $v_fill = isset($arr_info['fill']) ? $arr_info['fill'] : 'none';
        $v_flip = isset($arr_info['flip']) ? $arr_info['flip'] : 'none';
        $arr_border = isset($arr_info['border']) ? $arr_info['border'] : array();
        if (!is_array($arr_border)) $arr_border = array();
        $v_border_color = isset($arr_border['color']) ? $arr_border['color'] : '#000000';
        $v_border_width = isset($arr_border['width']) ? $arr_border['width'] : 1;
        $v_border_linecap = isset($arr_border['linecap']) ? $arr_border['linecap'] : 'round';
        $v_border_dasharray = isset($arr_border['dasharray']) ? $arr_border['dasharray'] : 'none';

        $arr_border_type = $this->createDashArray($v_border_dasharray, $v_border_width);
        $v_border_dasharray = $arr_border_type['dasharray'];
        $v_border_linecap = $arr_border_type['linecap'];
        if($v_fill=='transparent') $v_fill = 'none';
        if($v_border_color=='transparent') $v_border_color = 'none';

        if($v_fill!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_fill), 'FFFFFF');
            $v_fill = $v_fill.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }
        if($v_border_color!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_border_color), '000000');
            $v_border_color = $v_border_color.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }

        //Calculate center rotate of triangle
        $v_center_x = $v_width / 2;
        $v_side_length = pow(pow($v_width/2, 2) + pow($v_height, 2)  ,0.5);
        $v_center_y = pow($v_side_length, 2) / (2 * $v_height);

        $center = new Point($v_center_x, $v_center_y);
        $p = array();
        $p[0] = new Point($v_width/2, 0);
        $p[1] = new Point(0, $v_height);
        $p[2] = new Point($v_width, $v_height);
        $v_point = '';
        for($i = 0; $i< sizeof($p); $i++){
            $p[$i] = $center->getRotatePointAroundThis($p[$i], $v_rotation);
            $v_point .= ' '.$p[$i]->getX().','.$p[$i]->getY();
        }

        $v_max_width = max(abs($p[0]->getX() - $p[1]->getX()), abs($p[0]->getX() - $p[2]->getX()), abs($p[2]->getX() - $p[1]->getX()));
        $v_max_height = max(abs($p[0]->getY() - $p[1]->getY()), abs($p[0]->getY() - $p[2]->getY()), abs($p[2]->getY() - $p[1]->getY()));
        $v_max_width += 2 * $v_border_width;
        $v_max_height += 2 * $v_border_width;

        $v_min_x = min($p[0]->getX(), $p[1]->getX(), $p[2]->getX());
        $v_min_y = min($p[0]->getY(), $p[1]->getY(), $p[2]->getY());
        $v_point = ' '.($p[0]->getX() - $v_min_x).','.($p[0]->getY()-$v_min_y);
        $v_point .= ' '.($p[1]->getX() - $v_min_x).','.($p[1]->getY()-$v_min_y);
        $v_point .= ' '.($p[2]->getX() - $v_min_x).','.($p[2]->getY()-$v_min_y);
        $transform = '';
        $this->transform = $transform;
        $this->title = 'Triangle';
        $this->width = $v_max_width;
        $this->height = $v_max_height;
        $body = new SimpleXMLElement('<polygon />');
        $body->addAttribute('id', $v_name);
        $body->addAttribute('points', $v_point);
        $body->addAttribute('stroke-linecap', $v_border_linecap);
        $body->addAttribute('stroke-dasharray', $v_border_dasharray);
        $body->addAttribute('stroke-width', $v_border_width);
        $body->addAttribute('stroke', $v_border_color);
        $body->addAttribute('fill', $v_fill);
        $v_svg = $this->createSVG($body);
        return $v_svg;
    }

    public function createSVGPolygon(array $arr_info){
        $v_name = isset($arr_info['name'])?$arr_info['name']:'polygon_'.rand(0,1000);
        $v_width = isset($arr_info['width'])?$arr_info['width']:400;
        $v_side = isset($arr_info['side'])?$arr_info['side']:6;
        $v_height = isset($arr_info['height'])?$arr_info['height']:200;
        $v_rotation = isset($arr_info['rotation'])?$arr_info['rotation']:40;
        $v_fill = isset($arr_info['fill'])?$arr_info['fill']:'none';
        $arr_border = isset($arr_info['border'])?$arr_info['border']:array();
        if(!is_array($arr_border)) $arr_border = array();
        $v_border_color = isset($arr_border['color'])?$arr_border['color']:'#000000';
        $v_border_width = isset($arr_border['width'])?$arr_border['width']:1;
        $v_border_linecap = isset($arr_border['linecap'])?$arr_border['linecap']:'round';
        $v_border_dasharray = isset($arr_border['dasharray'])?$arr_border['dasharray']:'none';

        $arr_border_type = $this->createDashArray($v_border_dasharray, $v_border_width);
        $v_border_dasharray = $arr_border_type['dasharray'];
        $v_border_linecap = $arr_border_type['linecap'];

        $v_size = $v_width > $v_height?$v_width:$v_height;
        if($v_fill=='transparent') $v_fill = 'none';
        if($v_border_color=='transparent') $v_border_color = 'none';

        if($v_fill!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_fill), 'FFFFFF');
            $v_fill = $v_fill.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }
        if($v_border_color!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_border_color), '000000');
            $v_border_color = $v_border_color.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }

        $center = new Point($v_size/2, $v_size/2);//initial center point
        if($v_side<3) $v_side = 3;
        $p = array();
        $p[0] = new Point($center->getX(), 0);
        $angle = 360 / $v_side; // angle by one side

        $v_point = $p[0]->getX().','.$p[0]->getY();

        for($i = 1; $i< $v_side; $i++){
            $p[$i] = $center->getRotatePointAroundThis($p[$i-1], $angle);
            $v_point .= ' '.$p[$i]->getX().','.$p[$i]->getY();
        }
        $v_translate = '';
        $v_rotate = 'rotate('.$v_rotation.','.($v_size/2).','.($v_size/2).')';
        $v_transform = '';
        $v_transform = ($v_transform==''?'':' ').$v_translate;
        $v_transform = ($v_transform==''?'':' ').$v_rotate;

        $this->transform = $v_transform;
        $this->title = 'Polygon';
        $this->width = $v_size + $v_border_width;
        $this->height = $v_size + $v_border_width;
        $body = new SimpleXMLElement('<polygon />');
        $body->addAttribute('id', $v_name);
        $body->addAttribute('points', $v_point);
        $body->addAttribute('stroke-linecap', $v_border_linecap);
        $body->addAttribute('stroke-dasharray', $v_border_dasharray);
        $body->addAttribute('stroke-width', $v_border_width);
        $body->addAttribute('stroke', $v_border_color);
        $body->addAttribute('fill', $v_fill);
        $v_svg = $this->createSVG($body);
        return $v_svg;
        //return $this->createImage($v_svg);
    }

    public function createSVGEllipse(array $arr_info){
        $v_name = isset($arr_info['name'])?$arr_info['name']:'ellipse_'.rand(0,1000);
        $v_width = isset($arr_info['width'])?$arr_info['width']:400;
        $v_height = isset($arr_info['height'])?$arr_info['height']:200;
        $v_rotation = isset($arr_info['rotation'])?$arr_info['rotation']:40;
        $v_fill = isset($arr_info['fill'])?$arr_info['fill']:'none';
        $arr_border = isset($arr_info['border'])?$arr_info['border']:array();
        if(!is_array($arr_border)) $arr_border = array();
        $v_border_color = isset($arr_border['color'])?$arr_border['color']:'#000000';
        $v_border_width = isset($arr_border['width'])?$arr_border['width']:1;
        $v_border_linecap = isset($arr_border['linecap'])?$arr_border['linecap']:'round';
        $v_border_dasharray = isset($arr_border['dasharray'])?$arr_border['dasharray']:'none';

        $arr_border_type = $this->createDashArray($v_border_dasharray, $v_border_width);
        $v_border_dasharray = $arr_border_type['dasharray'];
        $v_border_linecap = $arr_border_type['linecap'];

        if($v_fill=='transparent') $v_fill = 'none';
        if($v_border_color=='transparent') $v_border_color = 'none';
        if($v_fill!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_fill), 'FFFFFF');
            $v_fill = $v_fill.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }
        if($v_border_color!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_border_color), '000000');
            $v_border_color = $v_border_color.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }

        $v_width -= $v_border_width;
        $v_height -= $v_border_width;

        if($v_width!=$v_height) {
            $center = new Point($v_width / 2, $v_height / 2);
            $ellipse = new Ellipse($center, $v_width / 2, $v_height / 2);
            $arr_size = $ellipse->getMaxSize($v_rotation);
            $v_max_width = isset($arr_size['max_x'])?$arr_size['max_x']:$v_width;
            $v_max_height = isset($arr_size['max_y'])?$arr_size['max_y']:$v_height;
        }else{
            $v_max_width = $v_max_height = $v_width;
        }
        $v_max_width += $v_border_width;
        $v_max_height += $v_border_width;

        $v_center_x = $v_max_width /2;
        $v_center_y = $v_max_height / 2;
        $v_rx = $v_width / 2;
        $v_ry = $v_height / 2;

        $v_translate = '';
        $v_rotate = 'rotate('.$v_rotation.','.($v_center_x).','.($v_center_y).')';
        $v_transform = '';
        $v_transform = ($v_transform==''?'':' ').$v_translate;
        $v_transform = ($v_transform==''?'':' ').$v_rotate;

        $this->transform = $v_transform;
        $this->title = 'Polygon';
        $this->width = $v_max_width;
        $this->height = $v_max_height;
        $body = new SimpleXMLElement('<ellipse />');
        $body->addAttribute('id', $v_name);
        $body->addAttribute('stroke-linecap', $v_border_linecap);
        $body->addAttribute('stroke-dasharray', $v_border_dasharray);
        $body->addAttribute('stroke-width', $v_border_width);
        $body->addAttribute('stroke', $v_border_color);
        $body->addAttribute('fill', $v_fill);
        $body->addAttribute('cx', $v_center_x);
        $body->addAttribute('cy', $v_center_y);
        $body->addAttribute('rx', $v_rx);
        $body->addAttribute('ry', $v_ry);
        $v_svg = $this->createSVG($body);
        return $v_svg;
        //return $this->createImage($v_svg);
    }

    public function createSVGRectangle(array $arr_info){
        $v_name = isset($arr_info['name'])?$arr_info['name']:'rectangle_'.rand(0,1000);
        $v_width = isset($arr_info['width']) ? $arr_info['width'] : 400;
        $v_height = isset($arr_info['height']) ? $arr_info['height'] : 200;
        $v_rx = $v_ry = isset($arr_info['round'])?$arr_info['round']:0;
        $v_rotation = isset($arr_info['rotation']) ? $arr_info['rotation'] : 40;
        $v_fill = isset($arr_info['fill']) ? $arr_info['fill'] : 'none';
        $arr_border = isset($arr_info['border']) ? $arr_info['border'] : array();
        if (!is_array($arr_border)) $arr_border = array();
        $v_border_color = isset($arr_border['color']) ? $arr_border['color'] : '#000000';
        $v_border_width = isset($arr_border['width']) ? $arr_border['width'] : 1;
        $v_border_linecap = isset($arr_border['linecap']) ? $arr_border['linecap'] : 'round';
        $v_border_dasharray = isset($arr_border['dasharray']) ? $arr_border['dasharray'] : 'none';

        $v_width -= $v_border_width;
        $v_height -= $v_border_width;

        $center = new Point($v_width / 2, $v_height / 2);
        if($v_fill=='transparent') $v_fill = 'none';
        if($v_border_color=='transparent') $v_border_color = 'none';
        if($v_fill!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_fill), 'FFFFFF');
            $v_fill = $v_fill.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }
        if($v_border_color!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_border_color), '000000');
            $v_border_color = $v_border_color.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }

        $arr_border_type = $this->createDashArray($v_border_dasharray, $v_border_width);
        $v_border_dasharray = $arr_border_type['dasharray'];
        $v_border_linecap = $arr_border_type['linecap'];

        $v_max_width = $v_width;
        $v_max_height = $v_height;
        if($v_rotation > 0) {
            if($v_rotation % 90 != 0) {
                $p = array();
                $p[0] = new Point(0, 0);
                $p[1] = new Point($v_width, 0);
                $p[2] = new Point($v_width, $v_height);
                $p[3] = new Point(0, $v_height);
                $p[0] = $center->getRotatePointAroundThis($p[0], $v_rotation);
                $p[1] = $center->getRotatePointAroundThis($p[1], $v_rotation);
                $p[2] = $center->getRotatePointAroundThis($p[2], $v_rotation);
                $p[3] = $center->getRotatePointAroundThis($p[3], $v_rotation);
                $v_max_width = 0;
                $v_max_height = 0;
                for ($i = 0; $i < sizeof($p) - 1; $i++) {
                    for ($j = $i + 1; $j < sizeof($p); $j++) {
                        if ($v_max_width < abs($p[$j]->getX() - $p[$i]->getX())) {
                            $v_max_width = abs($p[$j]->getX() - $p[$i]->getX());
                        }

                        if ($v_max_height < abs($p[$j]->getY() - $p[$i]->getY())) {
                            $v_max_height = abs($p[$j]->getY() - $p[$i]->getY());
                        }
                    }
                }
            }else{
                if($v_rotation % 180 != 0){
                    $v_max_width = $v_height;
                    $v_max_height = $v_width;
                }
            }
        }
        $v_max_width += $v_border_width;
        $v_max_height += $v_border_width;

        $v_center_x = $v_max_width / 2 ;
        $v_center_y = $v_max_height / 2;
        //$v_width -= $v_border_width;
        //$v_height -= $v_border_width;
        $v_x = $v_center_x - $v_width / 2 ;//- $v_border_width / 2;
        $v_y = $v_center_y - $v_height / 2;// - $v_border_width / 2;
        $v_translate = 'translate('.($v_border_width/2).','.($v_border_width/2).')';
        $v_translate = '';
        $v_rotate = 'rotate('.$v_rotation.','.($v_center_x).','.($v_center_y).')';
        $v_transform = '';
        $v_transform = ($v_transform==''?'':' ').$v_rotate;
        $v_transform = $v_transform.($v_transform==''?'':' ').$v_translate;

        $this->transform = $v_transform;
        $this->title = 'Rectangle';
        $this->width = $v_max_width;
        $this->height = $v_max_height;
        $body = new SimpleXMLElement('<rect />');
        $body->addAttribute('id', $v_name);
        $body->addAttribute('stroke-linecap', $v_border_linecap);
        $body->addAttribute('stroke-dasharray', $v_border_dasharray);
        $body->addAttribute('stroke-width', $v_border_width);
        $body->addAttribute('stroke', $v_border_color);
        $body->addAttribute('fill', $v_fill);
        $body->addAttribute('width', $v_width);
        $body->addAttribute('height', $v_height);
        $body->addAttribute('x', $v_x);
        $body->addAttribute('y', $v_y);
        $body->addAttribute('rx', $v_rx);
        $body->addAttribute('ry', $v_ry);
        $v_svg = $this->createSVG($body);write($v_svg);
        return $v_svg;
        //return $this->createImage($v_svg);
    }

    public function createSVGLine(array $arr_info){
        $v_name = isset($arr_info['name'])?$arr_info['name']:'line_'.rand(0,1000);
        $v_width = isset($arr_info['width']) ? $arr_info['width'] : 400;
        $v_height = isset($arr_info['height']) ? $arr_info['height'] : 200;
        $v_rotation = isset($arr_info['rotation']) ? $arr_info['rotation'] : 40;
        $v_fill = isset($arr_info['fill']) ? $arr_info['fill'] : 'none';
        $arr_border = isset($arr_info['border']) ? $arr_info['border'] : array();
        if (!is_array($arr_border)) $arr_border = array();
        $v_border_color = isset($arr_border['color']) ? $arr_border['color'] : '#000000';
        $v_border_width = isset($arr_border['width']) ? $arr_border['width'] : 1;
        $v_border_linecap = isset($arr_border['linecap']) ? $arr_border['linecap'] : 'round';
        $v_border_dasharray = isset($arr_border['dasharray']) ? $arr_border['dasharray'] : 'none';

        $v_type = isset($arr_info['type']) ? $arr_info['type'] : 'hline';

        if($v_fill=='transparent') $v_fill = 'none';
        if($v_border_color=='transparent') $v_border_color = 'none';
        if($v_fill!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_fill), 'FFFFFF');
            $v_fill = $v_fill.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }
        if($v_border_color!='none' && $this->used_cmyk){
            $arr_cmyk = $this->hex2CYMK(str_replace('#','', $v_border_color), '000000');
            $v_border_color = $v_border_color.' icc-color(acmecmyk, '.$arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'].')';
        }

        $arr_border_type = $this->createDashArray($v_border_dasharray, $v_border_width);
        $v_border_dasharray = $arr_border_type['dasharray'];
        $v_border_linecap = $arr_border_type['linecap'];

        $center = new Point($v_width / 2, $v_height / 2);
        if($v_type=='hline') {
            $p1 = new Point(0, $center->getY());
            $p2 = new Point($v_width, $center->getY());
        }else{
            $p1 = new Point($center->getX(), 0);
            $p2 = new Point($center->getX(), $v_height);
        }
        $pt1 = $center->getRotatePointAroundThis($p1, $v_rotation);
        $pt2 = $center->getRotatePointAroundThis($p2, $v_rotation);

        $v_new_width = abs($pt1->getX() - $pt2->getX());
        $v_new_height = abs($pt1->getY() - $pt2->getY());

        if($v_new_width<2 * $v_border_width) $v_new_width = $v_width;
        if($v_new_height<2 * $v_border_width) $v_new_height = $v_height;
        $v_new_width += 2 * $v_border_width;
        $v_new_height += 2 * $v_border_width;

        $v_center_x = $v_new_width/2;
        $v_center_y = $v_new_height/2;

        $v_translate = '';
        $v_rotate = '';
        if($v_type=='hline') {
            $v_x1 = $v_center_x - ($p1->getX() + $p2->getX())/2;
            $v_y1 = $v_center_y;
            $v_x2 = $v_center_x + ($p1->getX()+$p2->getX())/2;
            $v_y2 = $v_center_y;
        }else{
            $v_x1 = $v_center_x;
            $v_y1 = $v_center_y - ($p1->getY() + $p2->getY())/2;
            $v_x2 = $v_center_x;
            $v_y2 = $v_center_y + ($p1->getY()+$p2->getY()/2);
        }

        if($v_rotation!=0) {
            $v_rotate = 'rotate(' . $v_rotation . ',' . ($v_center_x) . ',' . ($v_center_y) . ')';
        }
        $v_transform = '';
        $v_transform = ($v_transform==''?'':' ').$v_translate;
        $v_transform = ($v_transform==''?'':' ').$v_rotate;

        $this->transform = $v_transform;
        $this->title = 'Line';
        $this->width = $v_new_width;
        $this->height = $v_new_height;
        $body = new SimpleXMLElement('<line />');
        $body->addAttribute('id', $v_name);
        $body->addAttribute('stroke-linecap', $v_border_linecap);
        $body->addAttribute('stroke-dasharray', $v_border_dasharray);
        $body->addAttribute('stroke-width', $v_border_width);
        $body->addAttribute('stroke', $v_border_color);
        $body->addAttribute('fill', $v_fill);
        //$body->addAttribute('width', $v_width);
        //$body->addAttribute('height', $v_height);
        $body->addAttribute('x1', $v_x1);
        $body->addAttribute('y1', $v_y1);
        $body->addAttribute('x2', $v_x2);
        $body->addAttribute('y2', $v_y2);
        $v_svg = $this->createSVG($body);
        return $v_svg;
        //return $this->createImage($v_svg);
    }

    public function createSVGShape(array $arr_info, $type='line'){
        switch($type){
            case 'line':
                $v_svg = $this->createSVGLine($arr_info);
                break;
            case 'triangle':
                $v_svg = $this->createSVGTriangle($arr_info);
                break;
            case 'circle':
            case 'ellipse':
                $v_svg = $this->createSVGEllipse($arr_info);
                break;
            case 'polygon':
                $v_svg = $this->createSVGPolygon($arr_info);
                break;
            case 'rounded':
            case 'rect':
            default:
                $v_svg = $this->createSVGRectangle($arr_info);
                break;

        }
        return $v_svg;
    }

    public function drawShape(array $arr_info, $type='line'){
        $v_svg = $this->createSVGShape($arr_info, $type);
        return $this->createImage($v_svg);
    }
}