<?php
class cls_output {
    private $db = null;
    public function __construct(MongoDB $db){
        $this->db = $db;
    }
    public function output($arr_output, $p_json_data=true, $p_cross_domain=true){
        @ob_end_clean();
        if($this->db instanceof MongoDB) $this->db->command(array('logout'=>1));
        if($p_cross_domain){
            header('Access-Control-Allow-Origin: *');
            header('Content-Type: text/html; charset=utf-8');
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Expose-Headers: FooBar');
            header('Access-Control-Request-Headers: X-PINGOTHER');
            header('Content-Type: application/json');
        }

        if($p_json_data){
            //header('X-UA-Compatible: IE=edge,chrome=1');
            header('Access-Control-Allow-Origin: *');
            header('Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS');
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Expose-Headers: FooBar');
            header('Content-Type: text/html; charset=utf-8');
            header('Access-Control-Request-Headers: X-PINGOTHER');
            header('Content-Type: application/json');
        }
        echo json_encode($arr_output);
        die();
    }
    public function output_jsonp(array $arr_output, $p_callback_function = 'jsonp_callback'){
        @ob_end_clean();
        @ob_start();
        //header('Content-Type: text/javascript; charset=utf-8');
        if($this->db instanceof MongoDB) $this->db->command(array('logout'=>1));
        echo $p_callback_function.'('.json_encode($arr_output).');';die();
    }
}
?>
