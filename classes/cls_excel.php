<?php

class cls_excel{

    private $objPHPExcel = null;
    private $creator = 'Anvy ImageStylor';
    private $last_modify_by = 'ImageStylor';
    private $title = 'Office 2007 XLSX Test Document';
    private $subject = 'Office 2007 XLSX Test Document';
    private $description = 'Document for Office 2007 XLSX, generated using PHP classes.';
    private $saved_dir = '';

    public function __construct($savedDir = DESIGN_TEMP_DIR){
        require_once ('lib/PHPExcel/PHPExcel.php');
        require_once ('lib/PHPExcel/PHPExcel/Writer/Excel2007.php');
        $this->objPHPExcel = new PHPExcel();
        $this->updateHeader();
        if(file_exists($savedDir) && is_dir($savedDir) && is_writable($savedDir)) $this->saved_dir = $savedDir;
    }

    public function setCreator($creator = 'Anvy ImageStylor'){
        $this->creator = $creator;
    }

    public function setLastModifyBy($lastModifyBy = 'ImageStylor'){
        $this->last_modify_by = $lastModifyBy;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function setSubject($subject){
        $this->subject = $subject;
    }

    public function setDescription($description){
        $this->description = $description;
    }

    public function setSavedDir($savedDir = DESIGN_TEMP_DIR){
        if(file_exists($savedDir) && is_dir($savedDir) && is_writable($savedDir)) $this->saved_dir = $savedDir;
    }

    public function updateHeader(){
        if(!is_null($this->objPHPExcel)){
            $this->objPHPExcel->getProperties()->setCreator($this->creator);
            $this->objPHPExcel->getProperties()->setLastModifiedBy($this->last_modify_by);
            $this->objPHPExcel->getProperties()->setTitle($this->title);
            $this->objPHPExcel->getProperties()->setSubject($this->subject);
            $this->objPHPExcel->getProperties()->setDescription($this->description);
        }
    }

    public function useJson(array $arr_json, $title = true, $filename = '', $download = true){
        $arr_title = array();
        $arr_data = array();

        if($title){
            for($i=0; $i<sizeof($arr_json); $i++){
                $j=0;
                foreach($arr_json[$i] as $key=>$value){
                    if(!in_array($key, $arr_title)) $arr_title[] = $key;
                    $arr_data[$i][$j++] = $value;
                }
            }
        }else{
            $arr_data = $arr_json;
        }
        $v_col_length = sizeof($arr_title);
        if($v_col_length==0 && isset($arr_data[0])) $v_col_length = sizeof($arr_data[0]);

        $sheet = $this->objPHPExcel->getActiveSheet();
        $sheet->setTitle($this->title);
        $sheet->getDefaultRowDimension()->setRowHeight(20);
        $v_excel_col = 1;
        $v_excel_row = 1;
        if(sizeof($arr_title)>0){
            $this->createOneCellFull($sheet, $v_excel_col++, $v_excel_row, 'Ord.', 'center', true, true, 5, '', true);
            for($i=0; $i<$v_col_length;$i++)
                $this->createOneCellFull($sheet, $v_excel_col++, $v_excel_row, $arr_title[$i], 'center', true, true, 20, '', true);
            $v_excel_row++;
        }
        for($i=0; $i<sizeof($arr_data);$i++){
            $v_excel_col = 1;
            $this->createOneCellFull($sheet, $v_excel_col++, $v_excel_row, ($i+1), 'right');
            for($j=0; $j<sizeof($arr_data[$i]); $j++){
                $v_value = $arr_data[$i][$j];
                $this->createOneCellFull($sheet, $v_excel_col++, $v_excel_row, $v_value, 'left');
            }
            $v_excel_row++;
        }
        $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_PORTRAIT);
        $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
        $sheet->getPageSetup()->setHorizontalCentered(true);
        $sheet->getPageSetup()->setFitToPage(true);
        $sheet->getPageSetup()->setFitToWidth(1);
        $sheet->getPageSetup()->setFitToHeight(0);
        $sheet->setShowGridlines(false);

        if($filename==''){
            $filename = 'excel_download_'.date('Y_m_d_H_i_s').'.xlsx';
        }

        $objWriter = new PHPExcel_Writer_Excel2007($this->objPHPExcel);
        if($download){
            @ob_end_clean();
            @ob_start();
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$filename.'"');
            header('Cache-Control: max-age=0');
            $objWriter->save('php://output');
            die;
        }else{
            $v_saved_dir = $this->saved_dir . DIRECTORY_SEPARATOR . $filename;
            $objWriter->save($v_saved_dir);
            return $v_saved_dir;
        }
    }

    public function applyBordersToCellFull(PHPExcel_Worksheet & $sheet, $cell, $border=PHPExcel_Style_Border::BORDER_THIN, $fill_color='', $text_color=''){
        if($border=='') $border=PHPExcel_Style_Border::BORDER_THIN;
        $sheet->getStyle($cell)->getBorders()->getTop()->setBorderStyle($border);
        $sheet->getStyle($cell)->getBorders()->getBottom()->setBorderStyle($border);
        $sheet->getStyle($cell)->getBorders()->getLeft()->setBorderStyle($border);
        $sheet->getStyle($cell)->getBorders()->getRight()->setBorderStyle($border);
        if($fill_color!=''){
            $sheet->getStyle($cell)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle($cell)->getFill()->getStartColor()->setRGB($fill_color);
        }
        if($text_color!=''){
            $sheet->getStyle($cell)->getFont()->getColor()->applyFromArray(array("rgb"=>$text_color));
        }
    }

    public function createOneCellFull(PHPExcel_Worksheet & $sheet, $col, $row, $value, $align, $bold=false, $wrap=false, $width=0, $format=PHPExcel_Style_NumberFormat::FORMAT_GENERAL, $border=1, $fill_color='', $text_color = '', $p_rotation=0, array $arr_comment = array() ){
        //PHPExcel_Style_NumberFormat::FORMAT_PERCENTAGE
        if($format=='') $format=PHPExcel_Style_NumberFormat::FORMAT_GENERAL;
        $cell = excel_col($col).$row;
        $left=PHPExcel_Style_Alignment::HORIZONTAL_LEFT;
        $right=PHPExcel_Style_Alignment::HORIZONTAL_RIGHT;
        $center=PHPExcel_Style_Alignment::HORIZONTAL_CENTER;
        $vertical=PHPExcel_Style_Alignment::VERTICAL_CENTER;
        if($border==1){
            if($fill_color!='')
                $this->applyBordersToCellFull($sheet, $cell,'',$fill_color);
            else
                $this->applyBordersToCellFull($sheet, $cell);
        }
        if($align=='center'){
            $sheet->getStyle($cell)->getAlignment()->setHorizontal($center);
        }else if($align=='left'){
            $sheet->getStyle($cell)->getAlignment()->setHorizontal($left);
        }else if($align=='right'){
            $sheet->getStyle($cell)->getAlignment()->setHorizontal($right);
        }
        $sheet->getStyle($cell)->getAlignment()->setVertical($vertical);
        if($wrap) $sheet->getStyle($cell)->getAlignment()->setWrapText(true);
        //if(is_numeric($value))
        $sheet->getStyle($cell)->getNumberFormat()->setFormatCode($format);
        if($text_color!='') $sheet->getStyle($cell)->getFont()->getColor()->applyFromArray(array("rgb" => $text_color));
        //else
        //if(isset($arr_value[1]))
        //	$sheet->setCellFormula($cell,$value);
        //else
        $sheet->setCellValue($cell,$value);
        if($bold) $sheet->getStyle($cell)->getFont()->setBold(true);

        if($p_rotation!=0) $sheet->getStyle($cell)->getAlignment()->setTextRotation($p_rotation);
        if($width>0) $sheet->getColumnDimension(excel_col($col))->setWidth($width);

        if(count($arr_comment)>0){
            $sheet->getComment($cell)->setAuthor($this->creator);
            for($i=0; $i<count($arr_comment);$i++){
                $v_text = $arr_comment[$i]['text'];
                $v_bold = isset($arr_comment[$i]['bold']) && $arr_comment[$i]['bold'];
                $v_italic = isset($arr_comment[$i]['italic']) && $arr_comment[$i]['italic'];
                $objCommentRichText  = $sheet->getComment($cell)->getText()->createTextRun($v_text);
                if($v_bold){
                    $objCommentRichText->getFont()->setBold(true);
                }
                if($v_italic)
                    $objCommentRichText->getFont()->setItalic(true);
                if($i<count($arr_comment)-1)
                    $sheet->getComment($cell)->getText()->createTextRun("\r\n");
            }
        }
    }


    public function excelCol($p_num){
        $numeric = ($p_num - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($p_num - 1) / 26);
        if ($num2 > 0) {
            return $this->excelCol($num2) . $letter;
        } else {
            return $letter;
        }
    }

    public function colExcel($p_excel_col){
        $v_cols = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $v_excel_col = strtoupper($p_excel_col);
        $v_excel_col = preg_replace('/[^A-Z]/','', $v_excel_col);
        $v_len = strlen($v_cols);
        $v_return = 0;
        $v_p_excel = strlen($v_excel_col);
        $j=0;
        for($i=$v_p_excel-1;$i>=0;$i--){
            $c = substr($v_excel_col, $i, 1);
            $p = strpos($v_cols, $c);
            if($p!==false){
                if($i==$v_p_excel-1){
                    $p++;
                    $v_return += $p;
                }else{
                    $p++;
                    $pp = pow($v_len, $j) * $p;
                    $v_return += $pp;
                }
            }
            $j++;
        }
        return $v_return;
    }
}