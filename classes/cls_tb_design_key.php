<?php
class cls_tb_design_key{

	private $v_site_id = 0;
	private $v_site_key = '';
	private $v_site_name = '';
	private $v_site_index = '';
	private $v_site_url = '';
	private $v_contact_name = '';
	private $v_contact_phone = '';
	private $v_contact_email = '';
	private $v_site_ip = '';
	private $v_site_logo = '';
	private $v_started_time = '0000-00-00 00:00:00';
	private $v_ended_time = '0000-00-00 00:00:00';
	private $v_site_forever = 0;
	private $v_site_status = 0;
	private $v_company_id = 0;
	private $v_location_id = 0;
    private $arr_account = array();
    private $v_concurrent_session = 0;
    private $arr_feature = array();
    private $v_full_feature = 0;
	private $collection = NULL;
	private $v_mongo_id = NULL;
	private $v_error_code = 0;
	private $v_error_message = '';
	private $v_is_log = false;
	private $v_dir = '';
    private $db = null;
    private $v_collection_name = 'tb_design_key';

	/**
	 *  constructor function
	 *  @param $db: instance of Mongo
	 *  @param $p_log_dir string: directory contains its log file
	 */
	public function __construct(MongoDB $db, $p_log_dir = ""){
		$this->v_is_log = $p_log_dir!='' && file_exists($p_log_dir) && is_writable($p_log_dir);
		if($this->v_is_log) $this->v_dir = $p_log_dir.DIRECTORY_SEPARATOR;
		$this->collection = $db->selectCollection($this->v_collection_name);
		$this->v_started_time = new MongoDate(time());
		$this->v_ended_time = new MongoDate(time());
        $this->db = $db;
		$this->collection->ensureIndex(array("site_id"=>1), array('name'=>"site_id_key", "unique"=>1, "dropDups" => 1));
	}
	
	/**
	 *  function get current MongoDB collection
	 *  @return Object: current MongoDB collection
	 */
	public function get_collection(){
		return $this->collection;
	}
	
	/**
	 *  function write log
	 */
	private function my_error(){
		if(! $this->v_is_log) return;
		global $_SERVER;
		$v_filename = $this->v_collection_name;
		$v_ext = '.log';
		$v_log_str = '--------------Log: '.date('Y-m-d H:i:s');
		$v_log_str .= "\r\n".(isset($_SERVER['QUERY_STRING'])?$_SERVER['QUERY_STRING']:'No QUERY STRING');
		$v_log_str .= "\r\n".(isset($_SERVER['REQUEST_URI'])?$_SERVER['REQUEST_URI']:' No REQUEST URI');
		$v_log_str .= "\r\n".$this->v_error_message.' ['.$this->v_error_code.']';
		$v_log_str .= "\r\n----------------End Log-----------------";
		$v_log_str .= "\r\n";
		$v_new_file = false;
		if(file_exists($this->v_dir.$v_filename.$v_ext)){
			if(filesize($this->v_dir.$v_filename.$v_ext) > 1024000){
				rename($this->v_dir.$v_filename.$v_ext, $this->v_dir.$v_filename.'_'.date('Y-m-d_H:i:s').$v_ext);
				$v_new_file = true;
				@unlink($this->v_dir.$v_filename.$v_ext);
			}
		}
		$fp = fopen($this->v_dir.$v_filename.$v_ext,$v_new_file?'w':'a+');
		if($fp){
			fwrite($fp, $v_log_str, strlen($v_log_str));
			fflush($fp);
			fclose($fp);
		}
	}

    /**
     * function return properties "feature" value
     * @return array
     */
    public function get_feature(){
        return $this->arr_feature;
    }


    /**
     * function allow change properties "feature" value
     * @param $arr_feature array
     */
    public function set_feature(array $arr_feature = array()){
        $this->arr_feature = $arr_feature;
    }

    /**
     * function return properties "account" value
     * @return array value
     */
    public function get_account(){
        return $this->arr_account;
    }


    /**
     * function allow change properties "account" value
     * @param $arr_account array
     */
    public function set_account(array $arr_account = array()){
        $this->arr_account = $arr_account;
    }

    /**
     * function return properties "concurrent_session" value
     * @return int value
     */
    public function get_concurrent_session(){
        return (int) $this->v_concurrent_session;
    }


    /**
     * function allow change properties "concurrent_session" value
     * @param $p_concurrent_session: int value
     */
    public function set_concurrent_session($p_concurrent_session){
        $this->v_concurrent_session = (int) $p_concurrent_session;
    }

    /**
	 * function return properties "site_id" value
	 * @return int value
	 */
	public function get_site_id(){
		return (int) $this->v_site_id;
	}

	
	/**
	 * function allow change properties "site_id" value
	 * @param $p_site_id: int value
	 */
	public function set_site_id($p_site_id){
		$this->v_site_id = (int) $p_site_id;
	}

    /**
     * function return properties "full_feature" value
     * @return int value
     */
    public function get_is_full_feature(){
        return (int) $this->v_full_feature;
    }


    /**
     * function allow change properties "full_feature" value
     * @param $p_full_feature: int value
     */
    public function set_is_full_feature($p_full_feature){
        $this->v_full_feature = (int) $p_full_feature;
    }


    /**
	 * function return properties "site_key" value
	 * @return string value
	 */
	public function get_site_key(){
		return $this->v_site_key;
	}

	
	/**
	 * function allow change properties "site_key" value
	 * @param $p_site_key: string value
	 */
	public function set_site_key($p_site_key){
		$this->v_site_key = $p_site_key;
	}

	
	/**
	 * function return properties "site_name" value
	 * @return string value
	 */
	public function get_site_name(){
		return $this->v_site_name;
	}

	
	/**
	 * function allow change properties "site_name" value
	 * @param $p_site_name: string value
	 */
	public function set_site_name($p_site_name){
		$this->v_site_name = $p_site_name;
	}

    /**
     * function return properties "site_index" value
     * @return string value
     */
    public function get_site_index(){
        return $this->v_site_index;
    }


    /**
     * function allow change properties "site_index" value
     * @param $p_site_index: string value
     */
    public function set_site_index($p_site_index){
        $this->v_site_index = $p_site_index;
    }


    /**
	 * function return properties "site_url" value
	 * @return string value
	 */
	public function get_site_url(){
		return $this->v_site_url;
	}

	
	/**
	 * function allow change properties "site_url" value
	 * @param $p_site_url: string value
	 */
	public function set_site_url($p_site_url){
		$this->v_site_url = $p_site_url;
	}


	/**
	 * function return properties "contact_name" value
	 * @return string value
	 */
	public function get_contact_name(){
		return $this->v_contact_name;
	}

	
	/**
	 * function allow change properties "contact_name" value
	 * @param $p_contact_name: string value
	 */
	public function set_contact_name($p_contact_name){
		$this->v_contact_name = $p_contact_name;
	}

	
	/**
	 * function return properties "contact_phone" value
	 * @return string value
	 */
	public function get_contact_phone(){
		return $this->v_contact_phone;
	}

	
	/**
	 * function allow change properties "contact_phone" value
	 * @param $p_contact_phone: string value
	 */
	public function set_contact_phone($p_contact_phone){
		$this->v_contact_phone = $p_contact_phone;
	}

	
	/**
	 * function return properties "contact_email" value
	 * @return string value
	 */
	public function get_contact_email(){
		return $this->v_contact_email;
	}

	
	/**
	 * function allow change properties "contact_email" value
	 * @param $p_contact_email: string value
	 */
	public function set_contact_email($p_contact_email){
		$this->v_contact_email = $p_contact_email;
	}

	
	/**
	 * function return properties "site_ip" value
	 * @return string value
	 */
	public function get_site_ip(){
		return $this->v_site_ip;
	}

	
	/**
	 * function allow change properties "site_ip" value
	 * @param $p_site_ip: string value
	 */
	public function set_site_ip($p_site_ip){
		$this->v_site_ip = $p_site_ip;
	}

    /**
     * function return properties "site_logo" value
     * @return string value
     */
    public function get_site_logo(){
        return $this->v_site_logo;
    }


    /**
     * function allow change properties "site_logo" value
     * @param $p_site_logo: string value
     */
    public function set_site_logo($p_site_logo){
        $this->v_site_logo = $p_site_logo;
    }



    /**
	 * function return properties "started_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_started_time(){
		return  $this->v_started_time->sec;
	}

	
	/**
	 * function allow change properties "started_time" value
	 * @param $p_started_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_started_time($p_started_time){
		if($p_started_time=='') $p_started_time = NULL;
		if(!is_null($p_started_time)){
			try{
				$this->v_started_time = new MongoDate(strtotime($p_started_time));
			}catch(MongoException $me){
				$this->v_started_time = NULL;
			}
		}else{
			$this->v_started_time = NULL;
		}
	}

	
	/**
	 * function return properties "ended_time" value
	 * @return int value indicates amount of seconds
	 */
	public function get_ended_time(){
		return  $this->v_ended_time->sec;
	}

	
	/**
	 * function allow change properties "ended_time" value
	 * @param $p_ended_time: string value format type: yyyy-mm-dd H:i:s
	 */
	public function set_ended_time($p_ended_time){
		if($p_ended_time=='') $p_ended_time = NULL;
		if(!is_null($p_ended_time)){
			try{
				$this->v_ended_time = new MongoDate(strtotime($p_ended_time));
			}catch(MongoException $me){
				$this->v_ended_time = NULL;
			}
		}else{
			$this->v_ended_time = NULL;
		}
	}

	
	/**
	 * function return properties "site_forever" value
	 * @return int value
	 */
	public function get_site_forever(){
		return (int) $this->v_site_forever;
	}

	
	/**
	 * function allow change properties "site_forever" value
	 * @param $p_site_forever: int value
	 */
	public function set_site_forever($p_site_forever){
		$this->v_site_forever = (int) $p_site_forever;
	}

	
	/**
	 * function return properties "site_status" value
	 * @return int value
	 */
	public function get_site_status(){
		return (int) $this->v_site_status;
	}

	
	/**
	 * function allow change properties "site_status" value
	 * @param $p_site_status: int value
	 */
	public function set_site_status($p_site_status){
		$this->v_site_status = (int) $p_site_status;
	}

	
	/**
	 * function return properties "company_id" value
	 * @return int value
	 */
	public function get_company_id(){
		return (int) $this->v_company_id;
	}

	
	/**
	 * function allow change properties "company_id" value
	 * @param $p_company_id: int value
	 */
	public function set_company_id($p_company_id){
		$this->v_company_id = (int) $p_company_id;
	}

	
	/**
	 * function return properties "location_id" value
	 * @return int value
	 */
	public function get_location_id(){
		return (int) $this->v_location_id;
	}

	
	/**
	 * function allow change properties "location_id" value
	 * @param $p_location_id: int value
	 */
	public function set_location_id($p_location_id){
		$this->v_location_id = (int) $p_location_id;
	}

	
	/**
	 * function return MongoID value after inserting new record
	 * @return ObjectId: MongoId
	 */
	public function get_mongo_id(){
		return $this->v_mongo_id;
	}

	
	/**
	 * function set MongoID to properties
	 */
	public function set_mongo_id($p_mongo_id){
		$this->v_mongo_id = $p_mongo_id;
	}

	
	/**
	 *  function allow insert one record
	 *  @return MongoID
	 */
	public function insert(){
        $this->v_site_id = $this->select_next('site_id');
		$arr = array('site_id' => $this->v_site_id
					,'site_key' => $this->v_site_key
					,'site_name' => $this->v_site_name
					,'site_index' => $this->v_site_index
					,'site_url' => $this->v_site_url
					,'contact_name' => $this->v_contact_name
					,'contact_phone' => $this->v_contact_phone
					,'contact_email' => $this->v_contact_email
					,'site_ip' => $this->v_site_ip
					,'site_logo' => $this->v_site_logo
					,'account' => $this->arr_account
					,'feature' => $this->arr_feature
					,'full_feature' => $this->v_full_feature
					,'concurrent' => $this->v_concurrent_session
					,'started_time' => $this->v_started_time
					,'ended_time' => $this->v_ended_time
					,'site_forever' => $this->v_site_forever
					,'site_status' => $this->v_site_status
					,'company_id' => $this->v_company_id
					,'location_id' => $this->v_location_id);
		try{
            if($this->v_site_id > 0)
			    $this->collection->insert($arr, array('safe'=>true));
			//$this->v_mongo_id = isset($arr['_id'])?$arr['_id']:'';
			return $this->v_site_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return 0;
		}
	}

	
	/**
	 *  function allow insert array with parameter
	 *  @param array $arr_fields_and_values
	 *  @return MongoID
	 */
	public function insert_array(array $arr_fields_and_values){
		try{
			$this->collection->insert($arr_fields_and_values, array('safe'=>true));
			$this->v_mongo_id = isset($arr_fields_and_values['_id'])?$arr_fields_and_values['_id']:'';
			return $this->v_mongo_id;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return NULL;
		}
	}

	
	/**
	 * function select_one_record
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order = array(), example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: all values will assign to this instance properties
	 * @example:
	 * <code>
	 *       SELECT * FROM `tb_design_key` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select_one(array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_one(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_count = 0;
		foreach($rss as $arr){
			$this->v_site_id = isset($arr['site_id'])?$arr['site_id']:0;
			$this->v_site_key = isset($arr['site_key'])?$arr['site_key']:'';
			$this->v_site_name = isset($arr['site_name'])?$arr['site_name']:'';
			$this->v_site_index = isset($arr['site_index'])?$arr['site_index']:'';
			$this->v_site_url = isset($arr['site_url'])?$arr['site_url']:'';
			$this->v_contact_name = isset($arr['contact_name'])?$arr['contact_name']:'';
			$this->v_contact_phone = isset($arr['contact_phone'])?$arr['contact_phone']:'';
			$this->v_contact_email = isset($arr['contact_email'])?$arr['contact_email']:'';
			$this->v_site_ip = isset($arr['site_ip'])?$arr['site_ip']:'';
			$this->arr_account = isset($arr['account'])?$arr['account']:array();
			$this->arr_feature = isset($arr['feature'])?$arr['feature']:array();
			$this->v_full_feature = isset($arr['full_feature'])?$arr['full_feature']:0;
			$this->v_concurrent_session = isset($arr['concurrent'])?$arr['concurrent']:0;
			$this->v_site_logo = isset($arr['site_logo'])?$arr['site_logo']:'';
			$this->v_started_time = isset($arr['started_time'])?$arr['started_time']:(new MongoDate(time()));
			$this->v_ended_time = isset($arr['ended_time'])?$arr['ended_time']:(new MongoDate(time()));
			$this->v_site_forever = isset($arr['site_forever'])?$arr['site_forever']:'0';
			$this->v_site_status = isset($arr['site_status'])?$arr['site_status']:0;
			$this->v_company_id = isset($arr['company_id'])?$arr['company_id']:0;
			$this->v_location_id = isset($arr['location_id'])?$arr['location_id']:0;
			$this->v_mongo_id = $arr['_id'];
			$v_count++;
		}
		return $v_count;
	}
	
	/**
	 * function select scalar value
	 * @param $p_field_name string, name of field
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 * SELECT `site_id` FROM `tb_design_key` WHERE `user_id`=2 ORDER BY `user_email` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select_scalar('site_id',array('user_id'=>2), array('user_email'=>-1))
	 * </code>
	 * @return mixed
	 */
	public function select_scalar($p_field_name, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = NULL;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
		return $v_ret;
	}
	
	/**
	 * function get next int value for key
	 * @param $p_field_name string, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @result: assign to properties
	 * @example: 
	 * <code>
	 *   SELECT `site_id` FROM `tb_design_key` WHERE `user_id`=2 ORDER BY `site_id` DESC LIMIT 0,1
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select_next('site_id',array('user_id'=>2), array('site_id'=>-1))
	 * </code>
	 * @return int
	 */
	public function select_next($p_field_name, array $arr_where = array()){
		$arr_order = array($p_field_name => -1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_ret = 0;
		foreach($rss as $arr){
			if(isset($arr[$p_field_name])) $v_ret = $arr[$p_field_name];
		}
        add_class('cls_tb_next_id');
        $cls_next = new cls_tb_next_id($this->db, $this->v_dir);
        $v_update_id = ((int) $v_ret) + 1;
        return $cls_next->calculate_next_id($this->v_collection_name, $p_field_name, $v_update_id);
	}
	
	/**
	 * function get missing value
	 * @param $p_field_name array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function select_missing($p_field_name, array $arr_where = array()){
		$arr_order = array(''.$p_field_name.'' => 1);//last insert show first
		$rss = $this->collection->find($arr_where)->sort($arr_order)->limit(1);
		$v_start = 1;
		$v_ret = 1;
		foreach($rss as $arr){
			if($arr[''.$p_field_name.'']!=$v_start){
				$v_ret = $v_start;
				break;
			}
			$v_start++;
		}
		return ((int) $v_ret);
	}
	
	/**
	 * function select limit records
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select_limit(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit($p_offset, $p_row, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr;
	}
	
	/**
	 * function select records
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example: 
	 * <code>
	 *         SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email`
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select(array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select(array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr = $this->collection->find($arr_where)->sort($arr_order);
		return $arr;
	}

    /**
     * @param MongoDB $db
     * @param $p_field_name
     * @param array $arr_where
     * @return array
     */
    public function select_distinct(MongoDB $db, $p_field_name, array $arr_where){
        return $db->command(array("distinct"=>"tb_design_key", "key"=>$p_field_name, 'query'=>$arr_where));
		//return $this->collection->command(array("distinct"=>"tb_design_key", "key"=>$p_field_name));
	}
	
	/**
	 * function select limit fields
	 * @param $p_offset int: start record to select, first record is 0
	 * @param $p_row int: amount of records to select
	 * @param $arr_fields array, array of fields will be selected
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @example:
	 * <code>
	 * SELECT * FROM `tbl_users` WHERE `user_id`>=2 ORDER BY `user_email` DESC LIMIT 10,20
	 * 		 $cls = new cls_tb_design_key($db)
	 * 		 $cls->select_limit_field(10, 20, array('user_id' => array('$gte' => 2), array('user_email' => -1))
	 * </code>
	 * @return array with indexes are names of fields 
	 */
	public function select_limit_fields($p_offset, $p_row, array $arr_fields, array $arr_where = array(), array $arr_order = array()){
		if(is_null($arr_order) || count($arr_order)==0){
			$arr_order = array('_id' => -1);//last insert show first
		}
		$arr_field = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr_field[$arr_fields[$i]] = 1;
		if($p_row <= 0)
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->skip($p_offset);
		 else
			$arr_return = $this->collection->find($arr_where, $arr_field)->sort($arr_order)->limit($p_row)->skip($p_offset);
		return $arr_return;
	}

	/**
	 *  function update one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(isset($v_has_mongo_id) && $v_has_mongo_id)
			$arr = array('$set' => array('site_id' => $this->v_site_id,'site_key' => $this->v_site_key,'site_name' => $this->v_site_name,'site_index' => $this->v_site_index,'site_url' => $this->v_site_url, 'site_logo'=>$this->v_site_logo, 'contact_name' => $this->v_contact_name,'contact_phone' => $this->v_contact_phone,'contact_email' => $this->v_contact_email,'site_ip' => $this->v_site_ip,'started_time' => $this->v_started_time,'ended_time' => $this->v_ended_time,'site_forever' => $this->v_site_forever,'site_status' => $this->v_site_status,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id, 'account'=>$this->arr_account, 'feature'=>$this->arr_feature, 'full_feature'=>$this->v_full_feature, 'concurrent'=>$this->v_concurrent_session));
		 else 
			$arr = array('$set' => array('site_key' => $this->v_site_key,'site_name' => $this->v_site_name,'site_index' => $this->v_site_index,'site_url' => $this->v_site_url, 'site_logo'=>$this->v_site_logo, 'contact_name' => $this->v_contact_name,'contact_phone' => $this->v_contact_phone,'contact_email' => $this->v_contact_email,'site_ip' => $this->v_site_ip,'started_time' => $this->v_started_time,'ended_time' => $this->v_ended_time,'site_forever' => $this->v_site_forever,'site_status' => $this->v_site_status,'company_id' => $this->v_company_id,'location_id' => $this->v_location_id, 'account'=>$this->arr_account, 'feature'=>$this->arr_feature, 'full_feature'=>$this->v_full_feature, 'concurrent'=>$this->v_concurrent_session));
		try{
			$this->collection->update($arr_where, $arr, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function delete one or more records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean 
	 */
	public function delete(array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->remove($arr_where, array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_field($p_field, $p_value, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$set' => array($p_field => $p_value)), array('safe'=>true, 'multiple'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields array, array of selected fields go to updated 
	 * @param $arr_values array, array of selected values go to assigned 
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function update_fields($arr_fields, $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$set' => $arr), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function increase one or more records
	 * @param $p_field string, name of field 
	 * @param $p_value = mix value, assigned to field
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_field($p_field, $p_value = 1, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		try{
			$this->collection->update($arr_where, array('$inc' => array($p_field => $p_value)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function update one or more records
	 * @param $arr_fields = array(), array of selected fields go to updated 
	 * @param $arr_values = array(), array of selected values go to increase 
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return boolean
	 */
	public function increase_fields(array $arr_fields, array $arr_values, array $arr_where = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		$arr = array();
		for($i=0; $i<count($arr_fields); $i++)
			$arr[$arr_fields[$i]] = $arr_values[$i];
		try{
			$this->collection->update($arr_where, array('$inc' => array($arr)), array('safe'=>true));
			return true;
		}catch(MongoCursorException $e){
			$this->v_error_code = $e->getCode();
			$this->v_error_message = $e->getMessage();
			$this->my_error();
			return false;
		}
	}

	/**
	 * function draw option tag
	 * @param $p_field_value string: name of field will be value option tag
	 * @param $p_field_display string: name of field will be display text option tag
	 * @param $p_selected_value mixed: value of field will be display text option tag
	 * @param $arr_where array, example: array('field'=>3), that equal to: WHERE field=3
	 * @param $arr_order array, example: array('field'=>-1), that equal to: ORDER BY field DESC
	 * @param $arr_exclude array: array list value of exclude
	 * @return string
	 */
	public function draw_option($p_field_value, $p_field_display, $p_selected_value, array $arr_where = array(), array $arr_order = array(), array $arr_exclude = array()){
		if(is_null($arr_where) || count($arr_where)==0){
			$v_has_mongo_id = !is_null($this->v_mongo_id);
			if($v_has_mongo_id)
				$arr_where = array('_id' => $this->v_mongo_id);
		}
		if(is_null($arr_order) || count($arr_order)==0) $arr_order = array('_id' => 1);
		$arr = $this->select_limit_fields(0, 0, array($p_field_value, $p_field_display), $arr_where, $arr_order);
		$v_dsp_option = '';
		foreach($arr as $a){
			if(!in_array($a[$p_field_value],$arr_exclude)){
				if($a[$p_field_value] == $p_selected_value)
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'" selected="selected">'.$a[$p_field_display].'</option>';
				 else 
					$v_dsp_option .= '<option value="'.$a[$p_field_value].'">'.$a[$p_field_display].'</option>';
			}
		}
		return $v_dsp_option;
	}

	/**
	 * function count all records
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count(array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->count();
		 else
			return $this->collection->find($arr_where)->count();
	}

	/**
	 * function count all records
	 * @param $p_field string: in field to count
	 * @param $arr_where = array(), example: array('field'=>3), that equal to: WHERE field=3
	 * @return int
	 */
	public function count_field($p_field, array $arr_where = array()){
		if(is_null($arr_where) || (count($arr_where)==0))
			return $this->collection->find(array($p_field => array('$exists' => true)))->count();
		 else
			return $this->collection->find($arr_where, array($p_field => array('$exists' => true)))->count();
	}

    /**
     * @param $p_site_key
     * @param int $p_len
     * @param int $p_site_id
     * @return string
     */
    public function create_key($p_site_key, $p_len=30, $p_site_id=0){
        $p_site_key = trim($p_site_key);
        if($p_site_key=='') $p_site_key = create_random_password($p_len, true, false);
        do{
            if($p_site_id>0){
                $arr_where = array('site_id'=>array('$ne'=>$p_site_id), 'site_key'=>new MongoRegex('/'.$p_site_key.'/i'));
            }else{
                $arr_where = array('site_key'=>new MongoRegex('/'.$p_site_key.'/i'));
            }
            $v_loop = $this->count($arr_where)>=1;
            if($v_loop) $p_site_key = create_random_password($p_len, true, true);
        }while($v_loop);
        return $p_site_key;
    }

    public function check_key($p_site_key, $p_check_domain = false){
        $arr_where = array('site_key'=>$p_site_key);
        $v_row = $this->select_one($arr_where);
        $arr_return = array('site_id'=>0,'site_name'=>'', 'message'=>'Unknown request!', 'error'=>0);
        if($v_row==1){
            $v_site_id = $this->get_site_id();
            $v_site_forever = $this->get_site_forever();
            $v_start_time = $this->get_started_time();
            $v_ended_time = $this->get_ended_time();
            $v_site_url = $this->get_site_url();
            $v_now = time();
            $arr_return['site_name'] = $this->get_site_name();
            $v_continue = true;
            if($v_site_forever==0){
                if($v_start_time > $v_now || $v_ended_time<=$v_now){
                    $arr_return['message'] = 'The registration has been out of range ['.($v_start_time > $v_now?'Started at: '.date('d-M-Y H:i:s', $v_start_time):'Expired at: '.date('d-M-Y H:i:s', $v_ended_time)).']!';
                    $arr_return['error'] = 1; //Out of time
                    $v_continue = false;
                }
            }
            if($v_continue){
                if($p_check_domain){
                    $v_current_site = get_current_page_url();
                    if(strpos($v_current_site, $v_site_url)===false){
                        $arr_return['message'] = 'Stranger Key: Current = '.$v_current_site.' -- Registration: '.$v_site_url;
                        $arr_return['error'] = 2;// Stranger URL
                    }else{
                        $arr_return['site_id'] = $v_site_id;
                        $arr_return['message'] = 'OK';
                    }
                }else{
                    $arr_return['site_id'] = $v_site_id;
                    $arr_return['message'] = 'OK';
                }
            }
        }else{
            $arr_return['message'] = 'Registration is not found';
            $arr_return['error'] = 3;
        }
        return $arr_return;
    }


    public function get_enabled_features(cls_settings $cls_settings, $p_site_id=0){
        $arr_feature_text = $cls_settings->select_scalar('option', array('setting_name'=>'feature_text'));
        $arr_feature_shape = $cls_settings->select_scalar('option', array('setting_name'=>'feature_shape'));
        $arr_feature_image = $cls_settings->select_scalar('option', array('setting_name'=>'feature_image'));
        if(!is_array($arr_feature_text)) $arr_feature_text = array();
        if(!is_array($arr_feature_shape)) $arr_feature_shape = array();
        if(!is_array($arr_feature_image)) $arr_feature_image = array();

        $v_is_full_feature = 0;
        $arr_return = array();
        if($p_site_id==0){
            //$v_site_id = $this->get_site_id();
            $arr_site_features = $this->get_feature();
            $v_is_full_feature = $this->get_is_full_feature();
        }else{
            $v_site_id = $p_site_id;
            $v_row = $this->select_one(array('site_id'=>$v_site_id));
            if($v_row==1){
                $arr_site_features = $this->get_feature();
                $v_is_full_feature = $this->get_is_full_feature();
            }else{
                $arr_site_features = array();
            }
        }
        if($v_is_full_feature){
            for($i=0; $i<sizeof($arr_feature_text);$i++){
                $v_status = isset($arr_feature_text[$i]['status'])?$arr_feature_text[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_text[$i]['add_text'])) $arr_return['text']['add'] = 1;
                    if(isset($arr_feature_text[$i]['change_text'])) $arr_return['text']['edit'] = 1;
                    if(isset($arr_feature_text[$i]['change_font'])) $arr_return['text']['font'] = 1;
                    if(isset($arr_feature_text[$i]['change_color'])) $arr_return['text']['color'] = 1;
                }
            }
            for($i=0; $i<sizeof($arr_feature_shape);$i++){
                $v_status = isset($arr_feature_shape[$i]['status'])?$arr_feature_shape[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_shape[$i]['add_shape'])) $arr_return['shape']['add'] = 1;
                    if(isset($arr_feature_shape[$i]['border_shape'])) $arr_return['shape']['border'] = 1;
                    if(isset($arr_feature_shape[$i]['fill_shape'])) $arr_return['shape']['fill'] = 1;
                }
            }
            for($i=0; $i<sizeof($arr_feature_image);$i++){
                $v_status = isset($arr_feature_image[$i]['status'])?$arr_feature_image[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_image[$i]['add_image'])) $arr_return['image']['add'] = 1;
                    if(isset($arr_feature_text[$i]['change_image'])) $arr_return['image']['edit'] = 1;
                    if(isset($arr_feature_text[$i]['rotate_image'])) $arr_return['image']['rotate'] = 1;
                    if(isset($arr_feature_text[$i]['crop_image'])) $arr_return['image']['crop'] = 1;
                    if(isset($arr_feature_text[$i]['effect_image'])) $arr_return['image']['effect'] = 1;
                }
            }
        }else{
            for($i=0; $i<sizeof($arr_feature_text);$i++){
                $v_status = isset($arr_feature_text[$i]['status'])?$arr_feature_text[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_text[$i]['add_text']) && isset($arr_site_features['text']['add_text'])) $arr_return['text']['add'] = 1;
                    if(isset($arr_feature_text[$i]['change_text']) && isset($arr_site_features['text']['change_text'])) $arr_return['text']['edit'] = 1;
                    if(isset($arr_feature_text[$i]['change_font']) && isset($arr_site_features['text']['change_font'])) $arr_return['text']['font'] = 1;
                    if(isset($arr_feature_text[$i]['change_color']) && isset($arr_site_features['text']['change_color'])) $arr_return['text']['color'] = 1;
                }
            }
            for($i=0; $i<sizeof($arr_feature_shape);$i++){
                $v_status = isset($arr_feature_shape[$i]['status'])?$arr_feature_shape[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_shape[$i]['add_shape']) && isset($arr_site_features['shape']['add_shape'])) $arr_return['shape']['add'] = 1;
                    if(isset($arr_feature_shape[$i]['border_shape']) && isset($arr_site_features['shape']['border_shape'])) $arr_return['shape']['border'] = 1;
                    if(isset($arr_feature_shape[$i]['fill_shape']) && isset($arr_site_features['shape']['fill_shape'])) $arr_return['shape']['fill'] = 1;
                }
            }
            for($i=0; $i<sizeof($arr_feature_image);$i++){
                $v_status = isset($arr_feature_image[$i]['status'])?$arr_feature_image[$i]['status']:1;
                if($v_status==0){
                    if(isset($arr_feature_image[$i]['add_image']) && isset($arr_site_features['image']['add_image'])) $arr_return['image']['add'] = 1;
                    if(isset($arr_feature_text[$i]['change_image']) && isset($arr_site_features['image']['rotate_image'])) $arr_return['image']['edit'] = 1;
                    if(isset($arr_feature_text[$i]['rotate_image'])) $arr_return['image']['rotate'] = 1;
                    if(isset($arr_feature_text[$i]['crop_image'])) $arr_return['image']['crop'] = 1;
                    if(isset($arr_feature_text[$i]['effect_image'])) $arr_return['image']['effect'] = 1;
                }
            }
        }
        return $arr_return;
    }
}
?>