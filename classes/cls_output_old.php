<?php

class cls_output {
    private $db = null;
    public function __construct(MongoDB $db){
        $this->db = $db;
    }
    public function output($arr_output, $p_json_data=true, $p_cross_domain=true){
        @ob_end_clean();
        if($this->db instanceof MongoDB) $this->db->command(array('logout'=>1));
        if($p_cross_domain)
            header('Access-Control-Allow-Origin: *');

        if($p_json_data){
            //header('X-UA-Compatible: IE=edge,chrome=1');
            header('Content-Type: application/json');
        }
        echo json_encode($arr_output);
        die();
    }
}