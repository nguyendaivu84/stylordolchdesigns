<?php
/**
 * Created by JetBrains PhpStorm.
 * User: ThanhHai
 * Date: 14/06/2013
 * Time: 00:05
 * To change this template use File | Settings | File Templates.
 */
require_once 'SvgDraw.php';
class cls_draw{
    private $v_width = 200;
    private $v_height = 200;
    private $v_x_pos = 0;
    private $v_y_pos = 0;
    private $v_fill_color;
    private $v_stroke_color;
    private $v_stroke_style = 'solid';
    private $v_stroke_width = 0.0;
    private $v_rotation = 0.0;
    private $v_transparent;
    private $v_no_background = false;
    private $v_no_image = false;
    private $v_no_text = false;
    private $v_no_shape = false;
    private $v_output_format = 'png';
    private $svg = null;

    public function __construct(){
        $this->v_transparent = new ImagickPixel('transparent');
        $this->v_fill_color = new ImagickPixel('transparent');
        $this->v_stroke_color = new ImagickPixel('black');
        $this->svg = new SvgDraw('SVG');
    }

    public function set_no_background($v_no_background = true){
        $this->v_no_background = $v_no_background;
    }

    public function get_no_background(){
        return $this->v_no_background;
    }

    public function set_no_image($v_no_image = true){
        $this->v_no_image = $v_no_image;
    }

    public function get_no_image(){
        return $this->v_no_image;
    }

    public function set_no_shape($v_no_shape = true){
        $this->v_no_shape = $v_no_shape;
    }

    public function get_no_shape(){
        return $this->v_no_shape;
    }

    public function set_no_text($v_no_text = true){
        $this->v_no_text = $v_no_text;
    }

    public function get_no_text(){
        return $this->v_no_text;
    }

    /**
     * @param float $p_width
     * set width image
     */
    public function set_width($p_width){
        $this->v_width = $p_width;
    }

    /**
     * @param $p_x_pos
     */
    public function set_x_pos($p_x_pos){
        $this->v_x_pos = $p_x_pos;
    }

    /**
     * @param $p_y_pos
     */
    public function set_y_pos($p_y_pos){
        $this->v_y_pos = $p_y_pos;
    }

    /**
     * @param float $p_height
     * set height image
     */
    public function set_height($p_height){
        $this->v_height = $p_height;
    }

    /**
     * @param string $p_fill_color
     * set fill color
     */
    public function set_fill_color($p_fill_color = 'white'){
        if($p_fill_color instanceof ImagickPixel)
            $this->v_fill_color = $p_fill_color;
        else
            $this->v_fill_color = new ImagickPixel($p_fill_color.'');
    }

    /**
     * @param string $p_stroke_color
     * set stroke color
     */
    public function set_stroke_color($p_stroke_color = 'black'){
        if($p_stroke_color instanceof ImagickPixel)
            $this->v_stroke_color = $p_stroke_color;
        else
            $this->v_stroke_color = new ImagickPixel($p_stroke_color.'');
    }

    /**
     * @param string $p_stroke_style
     * set style of stroke
     */
    public function set_stroke_style($p_stroke_style = 'solid'){
        $this->v_stroke_style = $p_stroke_style;
    }

    /**
     * @param float $p_stroke_width
     * set width of stroke
     */
    public function set_stroke_width($p_stroke_width = 0.0){
        $this->v_stroke_width = $p_stroke_width;
    }

    /**
     * @param float $p_rotation
     * set rotation in degree
     */
    public function set_rotation($p_rotation = 0.0){
        $this->v_rotation = $p_rotation;
    }

    /**
     * @param string $p_output_format
     * set output format
     */
    public function set_output_format($p_output_format='png'){
        $this->v_output_format = $p_output_format;
    }

    /**
     * @param ImagickDraw $draw
     * @param $p_border_stroke
     * @param $p_border_width
     * @return array
     */
    function create_line_style(ImagickDraw &$draw, $p_border_stroke, $p_border_width){
        $arr_style = array();
        if($p_border_stroke=='solid'){
        }else if($p_border_stroke=='round_dot'){
            $draw->setstrokelinecap(Imagick::LINECAP_ROUND);
            if($p_border_width*2==20) $p_border_width+=1;
            $draw->setstrokedasharray(array($p_border_width/20, $p_border_width*2));
        }else if($p_border_stroke=='square_dot'){
            $draw->setstrokedasharray(array($p_border_width , $p_border_width));
        }else if($p_border_stroke=='dash'){
            $draw->setstrokedasharray(array($p_border_width*6 , $p_border_width*2));
        }else if($p_border_stroke=='dash_dot'){
            $draw->setstrokedasharray(array($p_border_width*6 , $p_border_width*2, $p_border_width*2, $p_border_width*2));
        }else if($p_border_stroke=='long_dash'){
            $draw->setstrokedasharray(array($p_border_width*8 , $p_border_width*2));
        }else if($p_border_stroke=='long_dash_dot'){
            $draw->setstrokedasharray(array($p_border_width*8 , $p_border_width*2, $p_border_width*4, $p_border_width*2));
        }
        return $arr_style;
    }
    /**
     * @param Imagick $image
     * @param ImagickDraw $draw
     * @param $p_text
     * @param $p_max_width
     * @return array
     */
    public function word_wrap_annotation(Imagick &$image,ImagickDraw &$draw, $p_text, $p_max_width)
    {
        $p_text .=' ';
        $arr_words = explode(" ", $p_text);
        $arr_lines = array();
        $i = 0;
        $j=0;
        $v_line_height = 0;
        $v_line_width = 0;
        while($i < count($arr_words) )
        {
            $v_current_line = $arr_words[$i];
            if($i+1 >= count($arr_words))
            {
                $arr_lines[$j++] = array('text'=>$v_current_line, 'used'=>true);
                break;
            }
            $arr_metrics = $image->queryfontmetrics($draw, $v_current_line . ' ' . $arr_words[$i+1]);

            while($arr_metrics['textWidth'] <= $p_max_width)
            {
                $v_current_line .= ' ' . $arr_words[++$i];
                if($i+1 >= count($arr_words))
                    break;
                $arr_metrics = $image->queryfontmetrics($draw, $v_current_line . ' ' . $arr_words[$i+1]);
            }
            $arr_lines[$j++] = array('text'=>$v_current_line, 'used'=>trim($v_current_line)!='' || $p_text==$v_current_line);
            $i++;
            if($arr_metrics['textHeight'] > $v_line_height)
            //if($arr_metrics['ascender'] > $v_line_height)
                //$v_line_height = $arr_metrics['ascender'];
                $v_line_height = $arr_metrics['textHeight'];
            if($arr_metrics['textWidth'] > $v_line_width)
                $v_line_width = $arr_metrics['textWidth'];
        }
        return array($arr_lines, $v_line_width, $v_line_height);
    }

    /**
     * @param Imagick $image
     * @param string $p_text
     * @param string $p_gravity
     * @param string $p_font
     * @param float $p_font_point
     * @param string $p_bullet_style
     * @param string $p_line
     * @param int $p_rate
     */
    public function text(Imagick & $image, $p_text, $p_gravity = 'west', $p_font = 'Arial', $p_font_point = 15.0, $p_bullet_style = 'undefined', $p_line='none', $p_rate = 1){
        $arr_bullet_style = array(
            'undefined'=>''
            ,'black_circle'=>"● "//"&#9679; "
            ,'white_circle'=>"○ "//"&#9675; "
            ,'white_square'=>"□ "//"&#9633; "
            ,'black_square'=>"■ "//"&#9632; "
            ,'dash'=>"&ndash; "
            ,'number_dot'=>". "
            ,'number_dash'=>"- "
            ,'number_parenth'=>") "
        );
        $v_max_width = 0;
        $v_max_height = 0;
        if(!isset($arr_bullet_style[$p_bullet_style])) $p_bullet_style = 'undefined';
        $arr_gravity = array(
            'west'=>Imagick::ALIGN_LEFT
            ,'center'=>Imagick::ALIGN_CENTER
            ,'east'=>Imagick::ALIGN_RIGHT
        );
        if(!isset($arr_gravity[$p_gravity])) $p_gravity = 'west';

        $draw = new ImagickDraw();
        $draw->setfillcolor($this->v_fill_color);
        //$draw->setstrokecolor($this->v_fill_color);
        if($p_rate > 1){
            //$draw->setstrokewidth($p_rate);
        }
        $draw->setfontsize($p_font_point * $p_rate);
        $draw->setfont($p_font);
        //$draw->setstrokeantialias(true);
        $draw->settextantialias(true);
        $draw->settextencoding('unicode');
        $draw->setgravity($arr_gravity[$p_gravity]);
        //$draw->setTextAlignment($arr_gravity[$p_gravity]);

        $v_text_width = $this->v_width;
        $v_text_height = $this->v_height;
        $v_text_rotation = $this->v_rotation;
        //$arr_text_info = $this->rotate_size($v_text_width, $v_text_height, $v_text_rotation);
        //$v_text_width = $arr_text_info['w'];

        $p_text = html_entity_decode($p_text, ENT_COMPAT | ENT_HTML401, 'UTF-8');
        $p_text = urldecode($p_text);
        $p_text = str_replace("\r", "", $p_text);
        $arr_body = explode("\n", $p_text);
        $v_tmp_line_height = 0;
        $arr_lines = array();

        for($i=0; $i<count($arr_body);$i++){
            if($arr_bullet_style[$p_bullet_style]!=''){
                if(strpos($p_bullet_style,'number')===false){
                    $v_bullet = $arr_bullet_style[$p_bullet_style];
                    $v_bullet = html_entity_decode($v_bullet);
                    $v_bullet = urldecode($v_bullet);
                    $arr_body[$i] = $v_bullet.$arr_body[$i];
                }else{
                    $arr_body[$i] = ($i+1).$arr_bullet_style[$p_bullet_style].$arr_body[$i];
                }
            }
            $arr_body[$i] = array('text'=>$arr_body[$i], 'used'=>true);

            //$arr_body[$i] = html_entity_decode($arr_body[$i]);
            //$arr_body[$i] = urldecode($arr_body[$i]);
            list($arr_tmp_lines, $v_line_width, $v_line_height) = $this->word_wrap_annotation($image, $draw, $arr_body[$i]['text'],$v_text_width/* $this->v_width*/ * $p_rate);
            $v_tmp_line_height = $v_line_height;
            $v_max_width = $v_max_width < $v_line_width? $v_line_width:$v_max_width;
            if(trim($arr_body[$i]['text'])!='') {
                for ($j = 0; $j < count($arr_tmp_lines); $j++) {
                    if(trim($arr_tmp_lines[$j]['text'])!='')
                        $arr_lines[] = $arr_tmp_lines[$j];
                }
            }else{
                $arr_lines[] = array('text'=>'', 'used'=>true);
            }
        }

        $v_height = $v_tmp_line_height * count($arr_lines);
        //$v_text_width = min($v_max_width, $v_text_width);
        $image->newimage($v_text_width /* $this->v_width*/ * $p_rate, $v_height, $this->v_transparent);

        $v_x_pos = 0;

        if($p_line=='underline')
            $draw->settextdecoration(Imagick::DECORATION_UNDERLINE);
        else if($p_line=='strike')
            $draw->settextdecoration(Imagick::DECORATION_LINETROUGH);
        if($v_text_width /* $this->v_width*/>0 && $v_height>0){
            for($i = 0; $i < count($arr_lines); $i++){
                $v_y_pos = $i*$v_tmp_line_height;
                if($arr_lines[$i]['used']) {
                    $v_line = trim($arr_lines[$i]['text']);
                    //if($v_line!='') {
                    $image->annotateimage($draw, $v_x_pos, $v_y_pos, 0, $v_line);
                    //}
                }
            }
        }
        if($this->v_rotation!=0.0)
            $image->rotateimage($this->v_transparent, $this->v_rotation);

        if($p_rate!=1){
            $image->resizeimage($v_text_width/* $this->v_width*/, 0, Imagick::FILTER_CUBIC, .99, false);
        }
        $draw->clear();
        $draw->destroy();
    }


    /**
     * @param SVGDocument $svg
     * @param Template $tpl
     * @param $p_box_width
     * @param $p_bleed
     * @param array $arr_text
     * @param array $arr_size
     * @return SVGDocument
     */
    public function svg_text(SVGDocument $svg, Template $tpl, $p_box_width, $p_bleed = 0, array $arr_text = array(), array $arr_size = array()){
        $arr_bullet_style = array(
            'undefined'=>''
            ,'black_circle'=>"&#9679; "
            ,'white_circle'=>"&#9675; "
            ,'white_square'=>"&#9633; "
            ,'black_square'=>"&#9632; "
            ,'dash'=>"&ndash; "
            ,'number_dot'=>". "
            ,'number_dash'=>"- "
            ,'number_parenth'=>") "
        );
        $v_bullet_style = isset($arr_text['bullet'])?$arr_text['bullet']:'undefined';
        $v_width_in = isset($arr_text['width_in'])?$arr_text['width_in']:'0';
        $v_height_in = isset($arr_text['height_in'])?$arr_text['height_in']:'0';
        $v_text = isset($arr_text['body'])?$arr_text['body']:'';
        $v_gravity = isset($arr_text['gravity'])?$arr_text['gravity']:'west';
        $v_italic = isset($arr_text['italic'])?$arr_text['italic']:'normal';
        $v_bold = isset($arr_text['bold'])?$arr_text['bold']:'normal';
        $v_size = isset($arr_text['size'])?$arr_text['size']:'20';
        $v_fill = isset($arr_text['fill'])?$arr_text['fill']:'#000000';
        $v_font = isset($arr_text['font'])?$arr_text['font']:'Arial';
        $v_font_path = isset($arr_text['path'])?$arr_text['path']:'Arial';
        $v_key = isset($arr_text['key'])?$arr_text['key']:'arial';
        $v_text_left = isset($arr_text['left'])?$arr_text['left']:0;
        $v_text_top = isset($arr_text['top'])?$arr_text['top']:0;
        $v_line = isset($arr_text['line'])?$arr_text['line']:'none';
        $v_flip = isset($arr_text['flip'])?$arr_text['flip']:'none';
        $v_rotation = isset($arr_text['rotation'])?$arr_text['rotation']:0;
        $v_name = isset($arr_text['name'])?$arr_text['name']:'text_'.rand(0,10000);
        settype($v_rotation, 'int');

        $arr_cmyk = $this->hex2cymk(str_replace('#','', $v_fill));
        $v_cmyk = $arr_cmyk['c'].', '.$arr_cmyk['m'].', '.$arr_cmyk['y'].', '.$arr_cmyk['k'];

        if(!isset($arr_bullet_style[$v_bullet_style])) $v_bullet_style = 'undefined';
        $arr_gravity = array(
            'west'=>Imagick::ALIGN_LEFT
            ,'center'=>Imagick::ALIGN_CENTER
            ,'east'=>Imagick::ALIGN_RIGHT
        );
        if(!isset($arr_gravity[$v_gravity])) $v_gravity = 'west';

        $v_text = html_entity_decode($v_text);
        $v_text = urldecode($v_text);

        $v_text = str_replace("\r", "", $v_text);
        $arr_body = explode("\n", $v_text);
        $v_tmp_line_height = 0;
        $arr_lines = array();
        $v_svg_width = isset($arr_size['width'])?$arr_size['width']:'600';
        $v_svg_height = isset($arr_size['height'])?$arr_size['height']:'600';
//$v_str = 'W: '.$v_svg_width.' - H: '.$v_svg_height."\r\n";
//$v_str .= 'TW: '.$p_box_width.' - TL: '.$v_text_left.' - TT: '.$v_text_top."\r\n";
//$v_str .= 'T: '.$v_text.' - J: '.json_encode($arr_body)."\r\n";

        $image = new Imagick();
        $draw = new ImagickDraw();
        $draw->setfontsize($v_size /** 1.3*/);
        $draw->setfont($v_font_path);
        $draw->setgravity($arr_gravity[$v_gravity]);
        for($i=0; $i<count($arr_body);$i++){
            if($arr_bullet_style[$v_bullet_style]!=''){
                if(strpos($v_bullet_style,'number')===false){
                    $v_bullet = $arr_bullet_style[$v_bullet_style];
                    $v_bullet = html_entity_decode($v_bullet);
                    $v_bullet = urldecode($v_bullet);
                    $arr_body[$i] = $v_bullet.$arr_body[$i];
                }else{
                    $arr_body[$i] = ($i+1).$arr_bullet_style[$v_bullet_style].$arr_body[$i];
                }
            }
            $arr_body[$i] = array('text'=>$arr_body[$i], 'used'=>true);

            list($arr_tmp_lines, $v_line_width, $v_line_height) = $this->word_wrap_annotation($image, $draw, $arr_body[$i]['text'], $p_box_width);
            $v_tmp_line_height = $v_line_height;
            if(trim($arr_body[$i]['text'])!='') {
                for ($j = 0; $j < count($arr_tmp_lines); $j++)
                    if(trim($arr_tmp_lines[$j]['text'])!='')
                        $arr_lines[] = array('text'=>$arr_tmp_lines[$j]['text'], 'used'=>true);
            }else{
                $arr_lines[] = array('text'=>'', 'used'=>true);
            }
        }
        //$v_height = $v_tmp_line_height * count($arr_lines);
        //$v_x_pos = 0;

        if($v_line=='underline'){
            $v_decoration = 'underline';
        }else if($v_line=='strike'){
            $v_decoration = 'line-through';
        }else{
            $v_decoration = 'none';
        }
        $v_flip_x = $v_flip=='horizontal' || $v_flip=='both';
        $v_flip_y = $v_flip=='vertical' || $v_flip=='both';
        for($i = 0; $i < count($arr_lines); $i++){
            //$v_y_pos = ($i + 1)*$v_tmp_line_height;
            $v_y_pos = $i*$v_tmp_line_height;
            $v_line = trim($arr_lines[$i]['text']);
            //if($v_line!='') {
                $arr_cal = $this->calculate_text_svg($v_line.' ', $v_font_path, $v_size, $v_rotation, $p_box_width, $v_gravity, array('x'=>$v_text_left ,'y'=>($v_text_top+$v_y_pos)), $v_flip_x, $v_flip_y );
                $v_left = isset($arr_cal['x'])?$arr_cal['x']:0;
                $v_height = isset($arr_cal['h'])?$arr_cal['h']:0;
                $v_width = isset($arr_cal['w'])?$arr_cal['w']:0;
                $v_metrics = isset($arr_cal['m'])?$arr_cal['m']:'';
                $v_top = (isset($arr_cal['y'])?$arr_cal['y']:0) + $v_height /*+ (isset($arr_cal['b'])?$arr_cal['b']:0)*/;
                $v_transform = isset($arr_cal['t'])?$arr_cal['t']:'';//$v_str .= 'Line '.($i+1).': '.$v_line.' - L: '.$v_left.' - TL: '.$v_text_left.' - T: '.$v_top.' - S: '.$v_size.' - B: '.$p_bleed.' - W: '.$v_width."\r\n".$v_metrics;
                $v_space = ceil(strlen($v_line) * 0);

                $arr_text_info = array(
                    'width'=>$v_svg_width,
                    'height'=>$v_svg_height,
                    'width_in'=>$v_width_in,
                    'height_in'=>$v_height_in,
                    'left'=>$v_left /*- $v_space/2*/ /*+ $p_bleed*/,
                    'top'=>$v_top /*+ $v_height/3*/,
                    'text_id'=>'text_id_'.$i.'_'.$v_name,
                    'text-align'=>'left',
                    'font-size'=>$v_size.'pt',
                    'font-style'=> $v_italic,
                    'font-weight'=>$v_bold,
                    'font-family'=>$v_font,
                    'text-decoration'=>$v_decoration,
                    'fill'=>$v_fill,
                    'fill_cmyk'=>$v_cmyk,
                    'transform'=>$v_transform,
                    'tspan_id'=>'tspan_id_'.$i.'_'.$v_name,
                    'text'=>$this->special_replace($v_line)
                );
                foreach($arr_text_info as $key=>$value){
                    $v_key = strtoupper($key).'_PLACEHOLDER';
                    $tpl->set($v_key, $value);
                }
                $v_content = $tpl->output();

                $s = new SVGDocument($v_content);
                if(isset($s->text)){
                    $svg->addShape($s->text);
                }
            //}
        }
        $draw->clear();
        $draw->destroy();
        $image->clear();
        $image->destroy();
        return $svg;
    }


    /**
     * @param Imagick $image
     * @param string $p_text
     * @param string $p_gravity
     * @param string $p_font
     * @param float $p_font_point
     * @param string $p_bullet_style
     */
    public function add_text(Imagick & $image, $p_text = '', $p_gravity = 'west', $p_font = 'Arial', $p_font_point = 15.0, $p_bullet_style = 'undefined'){
        $tmp_image = new Imagick();
        $this->text($tmp_image, $p_text, $p_gravity, $p_font, $p_font_point, $p_bullet_style);
        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }

    /**
     * @param Imagick $image
     * @param string $p_shape_type
     * @param int $p_rotation
     */
    public function add_shape(Imagick & $image, $p_shape_type = 'hline', $p_rotation = 0){
        $tmp_image = new Imagick();
        $tmp_image->newimage($this->v_width, $this->v_height, $this->v_transparent);
        if(in_array($p_shape_type, array('hline', 'vline'))){
            $this->line($tmp_image, $p_shape_type, $p_rotation);
        }else if($p_shape_type=='triangle')
            $this->triangle($tmp_image, $p_rotation);
        else if($p_shape_type=='circle' || $p_shape_type=='ellipse')
            $this->ellipse($tmp_image, $p_rotation);
        else if($p_shape_type=='rounded')
            $this->rounded_rectangle($tmp_image, 10, $p_rotation);
        else if($p_shape_type=='polygon')
            $this->polygon($tmp_image, $this->v_width / 2, $this->v_height/2, min($this->v_width, $this->v_height)/2,  6, $p_rotation);
        else
            $this->rectangle($tmp_image, $p_rotation, $p_rotation);

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }

    public function create_shape($p_shape_type = 'hline', $p_rotation = 0){
        $tmp_image = new Imagick();
        $tmp_image->newimage($this->v_width, $this->v_height, $this->v_transparent);
        if(in_array($p_shape_type, array('hline', 'vline'))){
            $this->line($tmp_image, $p_shape_type, $p_rotation);
        }else if($p_shape_type=='triangle')
            $this->triangle($tmp_image, $p_rotation);
        else if($p_shape_type=='circle' || $p_shape_type=='ellipse')
            $this->ellipse($tmp_image, $p_rotation);
        else if($p_shape_type=='rounded')
            $this->rounded_rectangle($tmp_image, 10, $p_rotation);
        else if($p_shape_type=='polygon')
            $this->polygon($tmp_image, $this->v_width / 2, $this->v_height/2, min($this->v_width, $this->v_height)/2,  6, $p_rotation);
        else
            $this->rectangle($tmp_image, $p_rotation, $p_rotation);

        return $tmp_image;
    }
    /**
     * @param Imagick $image
     * @param $p_full_path
     * @param float $p_crop_left
     * @param float $p_crop_top
     * @param float $p_crop_width
     * @param float $p_crop_height
     */
    public function add_image(Imagick & $image, $p_full_path, $p_crop_left = 0.0, $p_crop_top = 0.0, $p_crop_width=0.0, $p_crop_height=0.0){

        $tmp_image = new Imagick();
        $tmp_image->readimage($p_full_path);

        if($this->v_rotation!=0){
            $tmp_image->rotateimage($this->v_transparent, $this->v_rotation);

            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

            if($p_crop_left>0 || $p_crop_top>0 || $p_crop_width>0 || $p_crop_height>0){
                $v_new_width = ceil($p_crop_width*$v_image_width);
                $v_new_height = ceil($p_crop_height*$v_image_height);
                $v_crop_left = ceil($p_crop_left*$v_image_width);
                $v_crop_top = ceil($p_crop_top*$v_image_height);

                $tmp_image->setimagepage(0,0,0,0);
                $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);

            }
            $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);
            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();
        }else{


            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

            if($p_crop_left>0 || $p_crop_top>0 || $p_crop_width>0 || $p_crop_height>0){
                $v_new_width = ceil($p_crop_width*$v_image_width);
                $v_new_height = ceil($p_crop_height*$v_image_height);
                $v_crop_left = ceil($p_crop_left*$v_image_width);
                $v_crop_top = ceil($p_crop_top*$v_image_height);
                $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);
            }


            $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);
            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

        }

        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->setstrokewidth($this->v_stroke_width);
            $draw->setfillcolor($this->v_transparent);
            $draw->rectangle($this->v_stroke_width/2,$this->v_stroke_width/2,/*$this->v_width*/$v_image_width - $this->v_stroke_width/2, /*$this->v_height*/$v_image_height - $this->v_stroke_width/2);
            $tmp_image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }


        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }


    public function crop_image($p_full_path, $p_crop_left = 0.0, $p_crop_top = 0.0, $p_crop_width=0.0, $p_crop_height=0.0){

        $tmp_image = new Imagick();
        $tmp_image->readimage($p_full_path);

        if($this->v_rotation!=0){
            $tmp_image->rotateimage($this->v_transparent, $this->v_rotation);

            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

            if($p_crop_left>0 || $p_crop_top>0 || $p_crop_width>0 || $p_crop_height>0){
                $v_new_width = ceil($p_crop_width*$v_image_width);
                $v_new_height = ceil($p_crop_height*$v_image_height);
                $v_crop_left = ceil($p_crop_left*$v_image_width);
                $v_crop_top = ceil($p_crop_top*$v_image_height);

                $tmp_image->setimagepage(0,0,0,0);
                $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);

            }
            $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);
            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();
        }else{


            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

            if($p_crop_left>0 || $p_crop_top>0 || $p_crop_width>0 || $p_crop_height>0){
                $v_new_width = ceil($p_crop_width*$v_image_width);
                $v_new_height = ceil($p_crop_height*$v_image_height);
                $v_crop_left = ceil($p_crop_left*$v_image_width);
                $v_crop_top = ceil($p_crop_top*$v_image_height);
                $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);
            }


            $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);
            $v_image_width = $tmp_image->getimagewidth();
            $v_image_height = $tmp_image->getimageheight();

        }

        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->setstrokewidth($this->v_stroke_width);
            $draw->setfillcolor($this->v_transparent);
            $draw->rectangle($this->v_stroke_width/2,$this->v_stroke_width/2,/*$this->v_width*/$v_image_width - $this->v_stroke_width/2, /*$this->v_height*/$v_image_height - $this->v_stroke_width/2);
            $tmp_image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }
        return $tmp_image;
    }


/*
    public function add_image(Imagick & $image, $p_full_path, $p_crop_left = 0.0, $p_crop_top = 0.0, $p_crop_width=0.0, $p_crop_height=0.0){
        $tmp_image = new Imagick();
        $tmp_image->readimage($p_full_path);
        if($this->v_rotation) $tmp_image->rotateimage($this->v_transparent, $this->v_rotation);
        $v_width = $tmp_image->getimagewidth();
        $v_height = $tmp_image->getimageheight();
        $v_new_width = ceil($p_crop_width*$v_width);
        $v_new_height = ceil($p_crop_height*$v_height);
        $v_crop_left = ceil($p_crop_left*$v_width);
        $v_crop_top = ceil($p_crop_top*$v_height);
        $tmp_image->cropimage($v_new_width, $v_new_height, $v_crop_left, $v_crop_top);
        $tmp_image->resizeimage($this->v_width, $this->v_height, Imagick::FILTER_LANCZOS, 1);

        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->setstrokewidth($this->v_stroke_width);
            $draw->setfillcolor($this->v_transparent);
            $draw->rectangle($this->v_stroke_width/2,$this->v_stroke_width/2,$this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
            $tmp_image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }
        //Fix image profile
        $arr_profiles = $tmp_image->getimageprofiles('*', false);
        $v_has_icc = (array_search('icc', $arr_profiles) !== false);
        if($v_has_icc){
            //$icc_cmyk = file_get_contents(ROOT_DIR.DS.'tools'.DS.'data'.'/USWebUncoated.icc');
            //$image->profileImage('icc', $icc_cmyk);
            //unset($icc_cmyk);
            $icc_rgb = file_get_contents(DESIGN_DATA_DIR.DS.'sRGB_v4_ICC_preference.icc');
            $tmp_image->profileimage('icc', $icc_rgb);
            unset($icc_rgb);
        }

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);
        $tmp_image->clear();
        $tmp_image->destroy();
    }
*/

    /**
     * @param Imagick $image
     * @param string $p_direction
     * @param int $p_rotation
     */
    public function line(Imagick & $image, $p_direction='vline', $p_rotation = 0){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_transparent);
        $v_x1 = $p_direction=='vline'?$this->v_width/2:0;
        $v_x2 = $p_direction=='vline'?$this->v_width/2:$this->v_width;
        $v_y1 = $p_direction=='vline'?0:$this->v_height/2;
        $v_y2 = $p_direction=='vline'?$this->v_height:$this->v_height/2;
        $draw->setfillcolor($this->v_transparent);
        $draw->line($v_x1, $v_y1, $v_x2, $v_y2);
        $image->drawimage($draw);
        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $draw->clear();
        $draw->destroy();
    }

    /**
     * @param Imagick $image
     * @param int $p_rotation
     */
    public function triangle(Imagick & $image, $p_rotation = 0){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_fill_color);
        $draw->setfillcolor($this->v_fill_color);
        $arr_coords = array(
            array('x'=>$this->v_width/2 - $this->v_stroke_width/2, 'y'=>$this->v_stroke_width/2),
            array('x'=>$this->v_stroke_width/2, 'y'=>$this->v_height - $this->v_stroke_width/2),
            array('x'=>$this->v_width - $this->v_stroke_width/2, 'y'=>$this->v_height - $this->v_stroke_width/2)
        );
        $draw->polygon($arr_coords);

        $image->drawimage($draw);
        $draw->clear();
        $draw->destroy();

        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
    }

    /**
     * @param Imagick $image
     * @param $p_x
     * @param $p_y
     * @param $p_radius
     * @param int $p_sides
     * @param int $p_rotation
     */
    public function polygon(Imagick & $image, $p_x, $p_y, $p_radius, $p_sides = 3, $p_rotation = 0){
        $v_degree = ceil(360 / $p_sides);

        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_fill_color);
        $draw->setfillcolor($this->v_fill_color);

        $arr_coords = array();

        for($i=0; $i<$p_sides;$i++){
            $v_deg = ($i * $v_degree / 180)*M_PI;
            $y = (int) ($p_y - $p_radius*cos($v_deg));
            $x = (int) ($p_x + $p_radius*sin($v_deg));
            $arr_coords[] = array('x'=>$x, 'y'=>$y);
            /*
            if($i>0){
                $draw->line($arr_coords[$i-1]['x'], $arr_coords[$i-1]['y'], $arr_coords[$i]['x'], $arr_coords[$i]['y']);
            }
            */
        }


        $draw->polygon($arr_coords);

        $image->drawimage($draw);
        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $draw->clear();
        $draw->destroy();
    }

    /**
     * @param Imagick $image
     * @param int $p_radius
     * @param int $p_rotation
     */
    public function rounded_rectangle(Imagick & $image, $p_radius = 10, $p_rotation = 0){
        $draw = new ImagickDraw();
        $v_stroke_color = $this->v_stroke_width>0?$this->v_stroke_color:$this->v_fill_color;
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_fill_color);
        $draw->setfillcolor($this->v_fill_color);
        $v_rect_width = $this->v_width;// ($this->v_width>0?($this->v_width-6):$this->v_width-1);
        $v_rect_height = $this->v_height;// ($this->v_height>0?($this->v_height-6):$this->v_height-1);

        $draw->roundrectangle(.5*$this->v_stroke_width, .5*$this->v_stroke_width,$v_rect_width - .5*$this->v_stroke_width - 1,$v_rect_height - .5*$this->v_stroke_width - 1,  $p_radius, $p_radius);
        $image->drawimage($draw);

        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $draw->clear();
        $draw->destroy();

    }


    /**
     * @param Imagick $image
     * @param int $p_rotation
     */
    public function ellipse(Imagick & $image, $p_rotation = 0){
        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($this->v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_fill_color);
        $draw1 = new ImagickDraw();

        if($this->v_stroke_style=='round_dot'){
            $draw1->setfillcolor($this->v_fill_color);
            $draw1->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2-1, $this->v_height/2-$this->v_stroke_width/2-1, 0, 360);
            $draw->setfillcolor($this->v_transparent);
            $draw->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2-1, $this->v_height/2-$this->v_stroke_width/2-1, 130, 310);
        }else{
            $draw->setfillcolor($this->v_fill_color);
            $draw->ellipse($this->v_width/2, $this->v_height/2, $this->v_width/2 - $this->v_stroke_width/2-1, $this->v_height/2-$this->v_stroke_width/2-1, 0, 360);
        }

        if($this->v_stroke_style=='round_dot'){
            $image->drawimage($draw1);
        }
        $image->drawimage($draw);
        if($this->v_stroke_style=='round_dot'){
            $image->rotateimage($this->v_transparent, 180);
            $image->drawimage($draw);
        }

        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $draw1->clear();
        $draw1->destroy();
        $draw->clear();
        $draw->destroy();
    }

    public function create_die_cut(Imagick & $image, $p_distance, $p_die_type = 'circle', $p_page = 0){
        $v_width = $this->v_width;
        $v_height = $this->v_height;
        //$p_distance = 0;
        $v_stroke_color = $this->v_stroke_color;
        $draw = new ImagickDraw();
        $draw->setfillcolor($this->v_transparent);
        $draw->setstrokecolor($v_stroke_color);
        $v_line = 30;

        switch($p_die_type){
            case 'circle':
                $draw->ellipse($v_width/2 + $p_distance , $v_height/2 +$p_distance , $v_width/2 , $v_height/2 , 0, 360);
                break;
            case 'roundedCorners':
                $draw->roundrectangle($p_distance, $p_distance, $v_width +$p_distance, $v_height+$p_distance , 5, 5);
                break;
            case 'halfCircleSide':

                $v_draw_height = $v_height;// - 2*$p_distance;
                $v_radius = $v_draw_height/2;
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_radius, $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance - $v_radius, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance - $v_radius, $p_distance + $v_radius , $v_radius, $v_radius, -90, 90);

                /*
                $v_draw_height = $v_height - 2*$p_distance;
                $v_radius = $v_draw_height/2;
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_radius, $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance - $v_radius, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance - $v_radius, $p_distance + $v_radius , $v_radius, $v_radius, -90, 90);
                */
                break;
            case 'leaf':
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance - $v_line);
                $draw->line($p_distance + $v_line, $v_height + $p_distance, $v_width + $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_line, $p_distance);
                $draw->line($v_width + $p_distance, $p_distance + $v_line, $v_width + $p_distance, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance-$v_line, $p_distance+$v_line, $v_line, $v_line, -90, 0);
                $draw->ellipse($p_distance + $v_line, $v_height + $p_distance - $v_line, $v_line, $v_line, 90, 180);

                break;
            case 'roundedSingleCorner':
                $draw->line($p_distance, $p_distance, $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $v_height + $p_distance, $v_width + $p_distance, $v_height + $p_distance);
                $draw->line($p_distance, $p_distance, $v_width + $p_distance - $v_line, $p_distance);
                $draw->line($v_width + $p_distance, $p_distance + $v_line, $v_width + $p_distance, $v_height + $p_distance);
                $draw->ellipse($v_width + $p_distance-$v_line, $p_distance+$v_line, $v_line, $v_line, -90, 0);

                break;
            default:
                $draw->rectangle($p_distance, $p_distance, $v_width + $p_distance, $v_height+$p_distance);
                break;
        }
        $image->drawimage($draw);
        if($p_page==1) $image->rotateimage($this->v_transparent, 180);

        $v_stroke_color->clear();
        $v_stroke_color->destroy();
        $draw->clear();
        $draw->destroy();
    }

    public function rectangle(Imagick & $image, $p_rotation = 0){
        $draw = new ImagickDraw();
        $v_stroke_color = $this->v_stroke_width>0?$this->v_stroke_color:new ImagickPixel('transparent');
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $this->v_stroke_style, $this->v_stroke_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor($v_stroke_color);
        if($this->v_stroke_width==0) $draw->setstrokecolor($this->v_fill_color);
        /*
        $draw1 = new ImagickDraw();
        if($this->v_stroke_style=='round_dot'){
            $draw1->setfillcolor($this->v_fill_color);
            $draw1->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width- $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
            $draw->setfillcolor($this->v_transparent);
            $draw->line($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width - $this->v_stroke_width/2, $this->v_stroke_width/2);//line 1
            $draw->line($this->v_width - $this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);//line 2
            $draw->line($this->v_width - $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2, $this->v_stroke_width/2 , $this->v_height - $this->v_stroke_width/2);//line 3
            $draw->line($this->v_stroke_width/2 , $this->v_height - $this->v_stroke_width/2, $this->v_stroke_width/2 , $this->v_stroke_width/2);//line 4
        }else{
            $draw->setfillcolor($this->v_fill_color);
            $draw->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $this->v_width- $this->v_stroke_width/2, $this->v_height - $this->v_stroke_width/2);
        }
        if($this->v_stroke_style=='round_dot'){
            $image->drawimage($draw1);
        }


        $image->drawimage($draw);
        if($this->v_stroke_style=='round_dot'){
            $image->rotateimage($this->v_transparent, 180);
            $image->drawimage($draw);
        }

        $draw1->clear();
        $draw1->destroy();
        */
        $draw->setfillcolor($this->v_fill_color);
        $draw->rectangle(.5*$this->v_stroke_width, .5*$this->v_stroke_width, $this->v_width - .5*$this->v_stroke_width-1, $this->v_height - .5*$this->v_stroke_width-1);
        $image->drawimage($draw);
        $draw->clear();
        $draw->destroy();
        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);

    }

    /**
     * @param $p_filename
     * @param $p_rotation
     * @param float $p_crop_left
     * @param float $p_crop_top
     * @param float $p_crop_right
     * @param float $p_crop_bottom
     * @return Imagick
     */
    public function image($p_filename, $p_rotation, $p_crop_left = 0.0 ,$p_crop_top = 0.0, $p_crop_right = 0.0, $p_crop_bottom = 0.0){
        $image = new Imagick($p_filename);
        $v_width = $this->v_width;
        $v_height = $this->v_height;
        if($p_rotation!=0) $image->rotateimage($this->v_transparent, $p_rotation);
        $image->resizeimage($v_width, $v_height, Imagick::FILTER_LANCZOS, 1, true);
        if($p_crop_bottom!=0 || $p_crop_left!=0 || $p_crop_right!=0 || $p_crop_top!=0){
            $v_width = $this->v_width - $p_crop_left - $p_crop_right;
            $v_height = $this->v_height - $p_crop_top - $p_crop_bottom;
            $image->cropimage($v_width, $v_height, $p_crop_left, $p_crop_top);
        }
        if($this->v_stroke_width>0){
            $draw = new ImagickDraw();
            $draw->setfillcolor($this->v_transparent);
            $draw->setstrokecolor($this->v_stroke_color);
            $draw->rectangle($this->v_stroke_width/2, $this->v_stroke_width/2, $v_width- $this->v_stroke_width/2, $v_height - $this->v_stroke_width/2);
            $image->drawimage($draw);
            $draw->clear();
            $draw->destroy();
        }
        return $image;
    }

    public function create_thumb(Imagick &$image, $p_file, $p_thumb_width=200){
        $image->readimage($p_file);
        $image->setcompression(Imagick::COMPRESSION_JPEG);
        $image->setcompressionquality(75);
        $image->resizeimage($p_thumb_width, 0, Imagick::FILTER_LANCZOS,1);
    }

    public function create_square_thumb(Imagick &$image, $p_thumb_width=100){
        $tmp_image = clone $image;
        $v_width = $tmp_image->getimagewidth();
        $v_height = $tmp_image->getimageheight();

        if($v_width>$v_height){
            $v_crop_left =ceil(($v_width - $v_height)/2);
            $tmp_image->cropimage($v_height, $v_height, $v_crop_left, 0);
        }else{
            $v_crop_top = ceil(($v_height - $v_width)/2);
            $tmp_image->cropimage($v_width, $v_width, 0, $v_crop_top);
        }

        $tmp_image->resizeimage($p_thumb_width, 0, Imagick::FILTER_LANCZOS,1);
        return $tmp_image;
    }

    public function create_thumb_gd($p_file, $p_thumb_width=200){
        list($width, $height, $type) = @getimagesize($p_file);
        $v_percent = $p_thumb_width / $width;
        $v_thumb_height = floor($height * $v_percent);
        switch($type){
            case 1;//Gif
                $im_source = @imagecreatefromgif($p_file);
                break;
            case 2;//Jpg
                $im_source = @imagecreatefromjpeg($p_file);
                break;
            case 3;//Png
                $im_source = @imagecreatefrompng($p_file);
                break;
            default ;
                $im_source = @imagecreatefromjpeg($p_file);
                break;
        }
        $tmp_img = imagecreatetruecolor( $p_thumb_width, $v_thumb_height );
        imagecopyresized($tmp_img, $im_source, 0, 0, 0, 0, $p_thumb_width, $v_thumb_height, $width, $height );
        return $tmp_img;
    }
    /**
     * @param array $arr_array
     * @param $p_field
     * @param bool $p_reverse
     * @return array
     */
    public function record_sort(array $arr_array, $p_field, $p_reverse=false){
        $arr_hash = array();
        foreach($arr_array as $arr){
            $arr_hash[$arr[$p_field]] = $arr;
        }
        ($p_reverse)? krsort($arr_hash) : ksort($arr_hash);
        $arr_return = array();
        foreach($arr_hash as $arr){
            $arr_return []= $arr;
        }
        return $arr_return;
    }

    public function gradient(Imagick & $image, $p_gradient_string = 'gradient:white-lightgray', $p_rotation = 90){
        $tmp_image = new Imagick();
        $tmp_image->newpseudoimage($this->v_width, $this->v_height, $p_gradient_string);
        $tmp_image->rotateimage($this->v_transparent, $p_rotation);

        $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $this->v_x_pos, $this->v_y_pos);

        $tmp_image->clear();
        $tmp_image->destroy();
    }

    public function __destroy(){
        if($this->v_transparent){
            $this->v_transparent->clear();
            $this->v_transparent->destroy();
        }
        if($this->v_fill_color){
            $this->v_fill_color->clear();
            $this->v_fill_color->destroy();
        }
        if($this->v_stroke_color){
            $this->v_stroke_color->clear();
            $this->v_stroke_color->destroy();
        }
    }

    public function shadow_image_file($p_image_file, $p_width = 0){
        $im = new Imagick($p_image_file);
        return $this->shadow_image($im, $p_width);
    }

    public function shadow_image(Imagick $image, $p_width = 0){
        $v_image_width = $image->getimagewidth();
        if($p_width<$v_image_width && $p_width > 20)
            //$image->thumbnailimage($p_width, null);
            $image->resizeimage($p_width,0,Imagick::FILTER_LANCZOS,1);
        //$shadow = $image->getimage();
        $shadow = clone $image;
        $shadow->setimagebackgroundcolor(new ImagickPixel('black'));
        $shadow->shadowimage(80, 3, 5, 5);
        $shadow->compositeimage($image, Imagick::COMPOSITE_OVER, 0, 0);
        $image->clear();
        $image->destroy();
        return $shadow;
    }

    public function create_sample(Imagick $image1,Imagick $image2, $p_size = 200){
        $im = new Imagick();
        $im->newimage($p_size, $p_size, new ImagickPixel('transparent'));
        $v_image_width = $image1->getimagewidth();
        $v_image_height = $image1->getimageheight();

        if($v_image_width>$v_image_height){
            $v_new_height = .6*$p_size;
            $v_new_width = ($v_new_height/$v_image_height)*$v_image_width;
            $shadow1 = $this->shadow_image($image1, $v_new_width);
            $shadow2 = $this->shadow_image($image2, $v_new_width);
            $shadow2->rotateimage($this->v_transparent, 15);
            $im->compositeimage($shadow2, Imagick::COMPOSITE_OVER,0, $v_new_height - .4*$p_size);
            $im->compositeimage($shadow1, Imagick::COMPOSITE_OVER,0, 0);
        }else{
            $v_new_width = .6*$p_size;
            $shadow1 = $this->shadow_image($image1, $v_new_width);
            $shadow2 = $this->shadow_image($image2, $v_new_width);
            $shadow2->rotateimage($this->v_transparent, 15);
            $im->compositeimage($shadow2, Imagick::COMPOSITE_OVER,.4*$p_size, 0);
            $im->compositeimage($shadow1, Imagick::COMPOSITE_OVER,0, 0);
        }
        $shadow1->clear();
        $shadow1->destroy();
        $shadow2->clear();
        $shadow2->destroy();
        return $im;
    }

    /**
     * @param $p_summary_file
     * @param $p_image1
     * @param $p_image2
     * @param int $p_size
     * @param bool $p_delete_source
     */
    public function create_sample_file($p_summary_file, $p_image1, $p_image2, $p_size = 200, $p_delete_source = true){
        $image1 = new Imagick($p_image1);
        $image2 = new Imagick($p_image2);
        $image = $this->create_sample($image1, $image2, $p_size);
        if($p_delete_source){
            @unlink($p_image1);
            @unlink($p_image2);
        }
        $image->setimageformat('png');
        $image->writeimage($p_summary_file);
        $image->clear();
        $image->destroy();
    }

    public function flip(Imagick & $image, $p_type='none'){
        if($p_type=='vertical')
            $v_result = $image->flipimage();
        elseif($p_type=='horizontal')
            $v_result = $image->flopimage();
        elseif($p_type=='both'){
            $v_result = $image->flipimage();
            if($v_result)
                $v_result = $image->flopimage();
        }else $v_result = true;
        return $v_result;
    }
    /**
     * @param $p_image
     * @param int $p_max_size
     * @return Imagick
     */
    public function preview_3d_path($p_image , $p_max_size = 600){
        $im = new Imagick($p_image);
        return $this->preview_3d_object($im, $p_max_size);
    }

    /**
     * @param Imagick $im
     * @param int $p_max_size
     * @return Imagick
     */
    public function preview_3d_object(Imagick $im, $p_max_size = 600){
        $im->setimageformat('png');
        $d = $im->getimagegeometry();
        $w = $d['width'];
        $h = $d['height'];
        if($w > $p_max_size){
            $v_ratio = $p_max_size / $w;
            $w = $p_max_size;
            $h = ceil($v_ratio * $h);
            $im->resizeimage($w, $h, Imagick::FILTER_LANCZOS, 1, true);
        }else if($h>$p_max_size){
            $v_ratio = $p_max_size / $h;
            $h = $p_max_size;
            $w = ceil($v_ratio * $w);
            $im->resizeimage($w,$h, Imagick::FILTER_LANCZOS, 1, true);
        }
        $im3 = new Imagick();
        $im3->newimage(1, $h, 'none', 'png');
        $im3->setimagevirtualpixelmethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);

        $im1 = clone $im;
        $im1->setimagevirtualpixelmethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $im1->setimagematte(true);
        $im1->cropimage(($w - 15), $h, 0, 0);
        $arr_control_points = array(
            0,0,15,15,
            ($w - 10), 0, ($w - 10), 0,
            0, $h, 25, ($h - 20),
            ($w - 10), $h, ($w - 10), $h
        );
        $im1->distortimage(Imagick::DISTORTION_BILINEAR, $arr_control_points, true);

        $im2 = clone $im;
        $im2->setimagevirtualpixelmethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);
        $im2->setimagematte(true);
        $im2->cropimage(15, $h, ($w - 15), 0);
        $arr_control_points = array(
            0, 0, 0, 0,
            10, 0, 10, 10,
            0, $h, 0, $h,
            10, $h, 10, ($h - 10)
        );
        $im2->distortimage(Imagick::DISTORTION_BILINEAR, $arr_control_points, true);
        $image = new Imagick();
        $image->addimage($im1);
        $image->addimage($im3);
        $image->addimage($im2);
        $image->resetiterator();
        $combined = $image->appendimages(false);
        $shadow = clone $combined;
        $shadow->setimagebackgroundcolor(new ImagickPixel('transparent'));
        $shadow->shadowimage(50, 3, 5, 5);
        $shadow->compositeimage($combined, Imagick::COMPOSITE_OVER, 0, 0);

        $im->clear();
        $im->destroy();
        $im1->clear();
        $im1->destroy();
        $im2->clear();
        $im2->destroy();
        $im3->clear();
        $im3->destroy();
        $image->clear();
        $image->destroy();
        return $shadow;
    }


    public function draw_border($p_width, $p_height, $p_border_width = 1, $p_color = 'black', $p_style = 'solid'){
        $im = new Imagick();

        $draw = new ImagickDraw();
        $draw->setstrokeantialias(true);
        $this->create_line_style($draw, $p_style, $p_border_width);
        $draw->setstrokewidth($this->v_stroke_width);
        $draw->setstrokecolor(new ImagickPixel($p_color));
        if($p_border_width==0) $draw->setstrokecolor($this->v_transparent);
        $draw->setfillcolor($this->v_transparent);


        $v_left = 0;
        $v_top = 0;
        $v_add = ceil($p_border_width/2);
        $draw->line($v_left, $v_top + $v_add, $v_left + $p_width , $v_top + $v_add);
        $draw->line($v_left + $p_width - $v_add, $v_top, $v_left + $p_width - $v_add, $v_top + $p_height);
        $draw->line($v_left + $p_width, $v_top + $p_height - $v_add, $v_left , $v_top + $p_height - $v_add);
        $draw->line($v_left + $v_add, $v_top + $p_height, $v_left + $v_add, $v_top);
        $im->newImage($p_width, $p_height, $this->v_transparent);
        $im->drawimage($draw);
        $draw->clear();
        $draw->destroy();
        return $im;
    }

    /**
     * @param Imagick $image
     * @param cls_tb_design_image $cls_image
     * @param cls_instagraph $cls_instagraph
     * @param array $arr_json
     * @param $p_dpi
     * @param int $p_page
     * @param bool $p_is_admin
     */
    public function create_preview(Imagick &$image, cls_tb_design_image $cls_image, cls_instagraph $cls_instagraph, array $arr_json, $p_dpi, $p_page = 0, $p_is_admin=true){
        $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $arr_design = isset($arr_canvas[$p_page])?$arr_canvas[$p_page]:array();

        $v_folding_type = isset($arr_json['folding'])?$arr_json['folding']:'none';
        $v_die_cut_type = isset($arr_json['dieCutType'])?$arr_json['dieCutType']:'none';
        if($v_die_cut_type=='undefined' || $v_die_cut_type=='') $v_die_cut_type = 'none';
        $v_folding_direction = isset($arr_json['foldingDirection'])?$arr_json['foldingDirection']:'none';
        $v_bleed = isset($arr_design['bleed'])?$arr_design['bleed']:'0';
        settype($v_bleed, 'float');
        $v_page_count = 1;
        if($v_bleed>0){
            if($v_folding_type!='none' && $v_die_cut_type=='none'){
                if(in_array($v_folding_type, array('zFold', 'triFold','letterFold'))){
                    $v_page_count = 3;
                }else if(in_array($v_folding_type, array('accordionFold','rollFold'))){
                    $v_page_count = 4;
                }
            }
        }
        //$v_width = isset($arr_design['width'])?$arr_design['width']:0;
        //$v_height = isset($arr_design['height'])?$arr_design['height']:0;
        $v_width = isset($arr_json['width'])?$arr_json['width']:0;
        $v_height = isset($arr_json['height'])?$arr_json['height']:0;

        $v_bg_color = isset($arr_design['bg_color'])?$arr_design['bg_color']:'transparent';
        if($this->v_no_background){
            $v_bg_color = 'transparent';
        }else{
            if(strlen($v_bg_color)!=6)
                $v_bg_color = 'transparent';
            else
                $v_bg_color = '#'.$v_bg_color;
        }
//$p_dpi = 72;
        $v_width = ceil($v_width * $p_dpi);
        $v_height = ceil($v_height * $p_dpi);
        if($v_width<=0) $v_width = 50;
        if($v_height<=0) $v_height = 50;


        $pixel = new ImagickPixel($v_bg_color);
        $image->newimage($v_width, $v_height, $pixel);


//Gradient if exists
        if(!$this->v_no_background){
            if($v_page_count>1){
                $j = 0;
                if($v_folding_direction=='vertical'){
                    $v_distance = ceil($v_width/$v_page_count);
                    for($i=$v_page_count; $i>=0; $i--){
                        $v_tmp_height = ceil($v_distance/6);
                        $v_tmp_width = $v_height;
                        $v_gradient_string = $j%2==0?'gradient:white-lightgray':'gradient:lightgray-white';
                        $v_tmp_x_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
                        $v_tmp_y_pos = 0;
                        $v_rotation = 90;
                        $this->set_x_pos($v_tmp_x_pos);
                        $this->set_y_pos($v_tmp_y_pos);
                        $this->set_width($v_tmp_width);
                        $this->set_height($v_tmp_height);
                        $this->gradient($image, $v_gradient_string, $v_rotation);

                        $j++;
                    }
                }else{
                    $v_distance = ceil($v_height/$v_page_count);
                    for($i=$v_page_count; $i>=0; $i--){
                        $v_tmp_height = ceil($v_distance/6);
                        $v_tmp_width = $v_width;
                        $v_gradient_string = $j%2==0?'gradient:lightgray-white':'gradient:white-lightgray';
                        $v_tmp_y_pos = $j%2==0? $v_distance * ($i-1):$v_distance * ($i-1) - $v_tmp_height;
                        $v_tmp_x_pos = 0;
                        $v_rotation = 0;
                        $this->set_x_pos($v_tmp_x_pos);
                        $this->set_y_pos($v_tmp_y_pos);
                        $this->set_width($v_tmp_width);
                        $this->set_height($v_tmp_height);
                        $this->gradient($image, $v_gradient_string, $v_rotation);

                        $j++;
                    }
                }
            }
        }

//Images
        $arr_images = isset($arr_design['images'])?$arr_design['images']:array();
        if(count($arr_images)>1) $arr_images = $this-> record_sort($arr_images, 'zindex');
//$v_str = '';



        $v_root_dir = ROOT_DIR.DS;
        $v_font_dir = DESIGN_FONT_DIR.DS;

        for($i=0; $i<count($arr_images);$i++){
            $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
            $v_width = isset($arr_images[$i]['width'])?$arr_images[$i]['width']:0;
            $v_height = isset($arr_images[$i]['height'])?$arr_images[$i]['height']:0;
            $v_rotation = isset($arr_images[$i]['rotation'])?$arr_images[$i]['rotation']:0;
            $v_left = isset($arr_images[$i]['left'])?$arr_images[$i]['left']:0;
            $v_top = isset($arr_images[$i]['top'])?$arr_images[$i]['top']:0;
            $v_crop = isset($arr_images[$i]['crop'])?$arr_images[$i]['crop']:false;
            $v_flip = isset($arr_images[$i]['flip'])?$arr_images[$i]['flip']:'none';
            $v_filter = isset($arr_images[$i]['filter'])?$arr_images[$i]['filter']:'undefined';
            $v_noprint = isset($arr_images[$i]['noprint']);
            if($v_noprint) $v_noprint = $arr_images[$i]['noprint'];
            $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
            $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
            $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
            $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;
            $v_border_width = isset($arr_images[$i]['border_width'])?$arr_images[$i]['border_width']:0;
            $v_border_color = isset($arr_images[$i]['border_color'])?$arr_images[$i]['border_color']:'transparent';
            $v_svg_id = isset($arr_images[$i]['svg_id'])?intval($arr_images[$i]['svg_id']):0;
            $v_svg_colors = isset($arr_images[$i]['svg_colors'])?$arr_images[$i]['svg_colors']:array();
            if(!is_array($v_svg_colors)){
                if($v_svg_colors!='') $arr_svg_colors = json_decode($v_svg_colors, true);
            }else $arr_svg_colors = $v_svg_colors;
            if(!isset($arr_svg_colors) || !is_array($arr_svg_colors)) $arr_svg_colors = array();

            if($v_filter.''=='undefined') $v_filter = 'none';
            if(strlen($v_border_color)<6)
                $v_border_color = 'transparent';
            else{
                $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
            }

            //$v_border_width = ceil($v_border_width);
            settype($v_border_width, 'int');
            settype($v_cropLeft, 'float');
            settype($v_cropTop, 'float');
            settype($v_cropWidth, 'float');
            settype($v_cropHeight, 'float');

            $v_left -= $v_bleed;
            $v_top -= $v_bleed;
            $v_top *= $p_dpi;
            $v_left *= $p_dpi;
            $v_width *= $p_dpi;
            $v_height *= $p_dpi;
            $v_width = ceil($v_width);
            $v_height = ceil($v_height);
            $v_top = ceil($v_top);
            $v_left = ceil($v_left);
            $v_noprint = $v_noprint && !$p_is_admin;
            if(!$v_noprint){
                if($v_image_id>0){
                    if($this->get_no_image()) continue;
                    $v_row = $cls_image->select_one(array('image_id'=>$v_image_id));

                    if($v_row==1){
                        $v_directory = $cls_image->get_saved_dir();
                        $v_directory = $v_root_dir.$v_directory;
                        $v_image = $cls_image->get_image_file();
                        $v_image_original = $cls_image->get_image_original_file();
                        $v_image_extension = $cls_image->get_image_extension();
                        $v_image = $v_directory.$v_image;

                        $v_is_svg = false;
                        if($v_svg_id > 0){
                            $arr_original_colors = $cls_image->get_svg_original_colors();
                            $v_svg_file = $v_directory . $v_image_original;
                            if(file_exists($v_svg_file) && is_file($v_svg_file)){
                                $v_svg_data = file_get_contents($v_svg_file);
                                for($k = 0; $k<sizeof($arr_original_colors) && $k<sizeof($arr_svg_colors); $k++){
                                    $v_or_svg_color = $arr_original_colors[$k];
                                    $v_svg_color = $arr_svg_colors[$k];
                                    if(strlen($v_svg_color)==6) $v_svg_color = '#'.$v_svg_color;
                                    if(strlen($v_or_svg_color)==6) $v_or_svg_color = '#'.$v_or_svg_color;
                                    $v_svg_data = str_ireplace($v_or_svg_color, $v_svg_color, $v_svg_data);
                                }
                                $tmp = new Imagick();
                                $tmp->setbackgroundcolor(new ImagickPixel('transparent'));
                                $tmp->readimageblob($v_svg_data);
                                $tmp->setimageformat('png32');
                                $v_tmp = DESIGN_TEMP_DIR . DS . 'svg_'.date('YmdHis').'_'.rand(10000, 99999).'.png';
                                $tmp->writeimage($v_tmp);
                                if(file_exists($v_tmp)){
                                    $v_image = $v_tmp;
                                    $v_is_svg = true;
                                }
                                $tmp->clear();
                                $tmp->destroy();

                            }
                        }

                        if(file_exists($v_image)){
                            if($v_border_width>0){
                                //$v_border_width = ceil($v_border_width*$p_dpi/(72*1.2));
                            }

                            $v_temp = false;
                            if($v_filter!='none'){
                                $cls_instagraph->set_input($v_image);
                                $v_output = DESIGN_TEMP_DIR.DS.date('YmdHis').'.'.$v_image_extension;
                                $cls_instagraph->set_output($v_output);
                                $v_filter = ucwords($v_filter);
                                if($cls_instagraph->process($v_filter)){
                                    $v_image = $v_output;
                                    //$cls_instagraph->output();
                                    $v_temp = true;
                                }
                            }


                            $this->set_stroke_color($v_border_color);
                            $this->set_stroke_width($v_border_width);
                            $this->set_x_pos($v_left);
                            $this->set_y_pos($v_top);
                            $this->set_width($v_width);
                            $this->set_rotation($v_rotation);
                            $this->set_height($v_height);
                            //$this->flip($image, $v_flip);

                            if($v_flip!='none'){
                                $t_image = new Imagick($v_image);
                                $v_image = create_random_password(10, true, true).'_'.date('YmdHis').'png';
                                $this->flip($t_image, $v_flip);
                                $t_image->setimageformat('png');
                                $t_image->writeimage($v_image);
                                $t_image->clear();
                                $t_image->destroy();
                                $v_temp = true;
                            }

                            $this->add_image($image, $v_image, $v_cropLeft, $v_cropTop, $v_cropWidth, $v_cropHeight);
                            if($v_temp){
                                if(file_exists($v_image)) @unlink($v_image);
                            }
                            if($v_is_svg){
                                if(file_exists($v_image)) @unlink($v_image);
                            }
                        }
                    }

                }else{
                    if($this->get_no_shape()) continue;
                    $v_shape_type = isset($arr_images[$i]['shape_type'])?$arr_images[$i]['shape_type']:'hline';
                    $v_border_stroke = isset($arr_images[$i]['border_stroke'])?$arr_images[$i]['border_stroke']:'solid';
                    $v_shape_fill_color = isset($arr_images[$i]['fill_color'])?$arr_images[$i]['fill_color']:'transparent';
                    $v_rounded = isset($arr_images[$i]['rounded'])?$arr_images[$i]['rounded']:'0';

                    $v_shape_fill_color = trim($v_shape_fill_color);
                    if(strlen($v_shape_fill_color)<6)
                        $v_shape_fill_color = 'transparent';
                    else{
                        $v_shape_fill_color = $v_shape_fill_color == 'transparent'?$v_shape_fill_color: '#'.substr($v_shape_fill_color, 0, 6);
                    }
                    //if($v_shape_fill_color == 'transparent') $v_shape_fill_color = 'none';
                    //if($v_border_color=='transparent') $v_border_color = 'none';
                    $arr_info = array(
                        'width'=>$v_width,
                        'height'=>$v_height,
                        'rotation'=>$v_rotation,
                        'fill'=>$v_shape_fill_color,
                        'round'=>$v_rounded,
                        'border'=>array(
                            'width'=>$v_border_width,
                            'color'=>$v_border_color,
                            'linecap'=>$v_border_stroke=='solid'?'butt':$v_border_stroke
                            ,'dasharray'=>$v_border_stroke
                        ),
                        'flip' => $v_flip
                    );

                    $im = $this->svg->drawShape($arr_info, $v_shape_type);
                    $this->flip($im, $v_flip);
                    if($im){
                        $image->compositeimage($im, Imagick::COMPOSITE_DEFAULT, $v_left, $v_top);
                    }
                }
            }

        }
        $arr_texts = isset($arr_design['texts'])?$arr_design['texts']:array();
        if(!$this->v_no_text){
            for($i=0; $i<count($arr_texts);$i++){

                $v_color = isset($arr_texts[$i]['color'])?$arr_texts[$i]['color']:'black';
                if(strlen($v_color)<6)
                    $v_color = 'black';
                else{
                    $v_color = '#'.substr($v_color,0,6);
                }
                $v_name = $arr_texts[$i]['name'];
                $v_top = (float) $arr_texts[$i]['top'];
                $v_left = (float) $arr_texts[$i]['left'];
                $v_width = (float) $arr_texts[$i]['width'];
                $v_height = (float) $arr_texts[$i]['height'];
                $v_body = isset($arr_texts[$i]['body'])?$arr_texts[$i]['body']:'';
                $v_bold = isset($arr_texts[$i]['bold'])?$arr_texts[$i]['bold']:false;
                $v_italic = isset($arr_texts[$i]['italic'])?$arr_texts[$i]['italic']:false;
                $v_gravity = isset($arr_texts[$i]['gravity'])?$arr_texts[$i]['gravity']:'center';
                $v_line = isset($arr_texts[$i]['line'])?$arr_texts[$i]['line']:'none';
                $v_character = isset($arr_texts[$i]['character'])?$arr_texts[$i]['character']:'none';
                $v_flip = isset($arr_texts[$i]['flip'])?$arr_texts[$i]['flip']:'none';
                $v_rotation = isset($arr_texts[$i]['rotation'])?$arr_texts[$i]['rotation']:0;
                $v_point_size = isset($arr_texts[$i]['pointsize'])?$arr_texts[$i]['pointsize']:0;
                settype($v_point_size, 'float');
                $v_bullet_style = isset($arr_texts[$i]['bullet_style'])?$arr_texts[$i]['bullet_style']:'undefined';


                if($v_body==''){
                    if($p_is_admin){
                        $v_body = isset($arr_texts[$i]['preset_text'])?$arr_texts[$i]['preset_text']:'';
                    }
                }
                $v_point = $v_point_size;
                //$v_point_size = ceil($v_point_size * $p_dpi/72);
                //$v_point_size = ceil($v_point_size * $p_dpi/72);
                $v_font_name = $v_font_key = '';
                $v_font_name =  isset($arr_texts[$i]['font'])?$arr_texts[$i]['font']:'Arial';
                $v_font_key = seo_friendly_url($v_font_name);
                $v_default_font = $v_font_dir.'arial'.DS.'regular.ttf';
                if($v_bold && $v_italic){
                    $v_font = $v_font_dir.$v_font_key.DS.'both.ttf';
                }else if($v_bold && !$v_italic)
                    $v_font = $v_font_dir.$v_font_key.DS.'bold.ttf';
                else if(!$v_bold && $v_italic)
                    $v_font = $v_font_dir.$v_font_key.DS.'italic.ttf';
                else
                    $v_font = $v_font_dir.$v_font_key.DS.'regular.ttf';
                if(!file_exists($v_font)) $v_font = $v_default_font;

                $v_left -= $v_bleed;
                $v_top -= $v_bleed;

                $v_top *= $p_dpi;
                $v_left *= $p_dpi;
                //$v_width *= $p_dpi;
                //$v_height *= $p_dpi;

                $v_top = ceil($v_top);
                $v_left = ceil($v_left);
                //$v_width = ceil($v_width);
                //$v_height = ceil($v_height);

                /*
                if($v_rotation==90 || $v_rotation==270){
                    $v_tmp = $v_width;
                    $v_width = $v_height;
                    $v_height = $v_tmp;
                }
                */
                //$this->set_fill_color($v_color);
                //$this->set_height($v_height);
                //$this->set_width($v_width);
                //$this->set_rotation($v_rotation);
                //$this->set_x_pos($v_left);
                //$this->set_y_pos($v_top);

                //$this->add_text($image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style );

                if($v_point_size>0){
                   // $v_point_size = ($v_point * $p_dpi)/72;
                }
                /*
                if($v_rotation>0 && $v_rotation % 90 !=0){
                    $arr_size = $this->rotate_size($v_width, $v_height, deg2rad($v_rotation));
                    $v_width = $arr_size['w'];
                    $v_height = $arr_size['h'];
                }
                */
                $v_width =  ceil($v_width * $p_dpi);
                $v_height = ceil($v_height * $p_dpi);
                if($v_point_size<=0){
                    $v_height = ceil( $v_height * $p_dpi);
                    $v_width = ceil( $v_width * $p_dpi);
                    if($v_height>0){
                        $v_point_size = ceil($v_height/1.3333333333333);
                    }else
                        $v_point_size = ceil($v_width/1.3333333333333);
                    $v_point_size /= 3;
                }

                $v_point_size = ceil($v_point_size);
                //if($v_fill!='transparent') $v_fill = '#'.$v_fill;

                $this->set_fill_color($v_color);
                $this->set_height($v_height);
                $this->set_width($v_width);
                $this->set_rotation($v_rotation);
                $this->set_x_pos($v_left);
                $this->set_y_pos($v_top);

                $tmp_image = new Imagick();
                if($v_character=='upper')
                    $v_body = strtoupper($v_body);
                else if($v_character=='lower')
                    $v_body = strtolower($v_body);
                else if($v_character=='capitalize')
                    $v_body = ucwords($v_body);
                $this->text($tmp_image, $v_body, $v_gravity, $v_font, $v_point_size, $v_bullet_style,$v_line);
                $this->flip($tmp_image, $v_flip);
                $image->compositeimage($tmp_image, Imagick::COMPOSITE_DEFAULT, $v_left, $v_top);
                $tmp_image->clear();
                $tmp_image->destroy();
            }
        }
    }

    public function create_svg(SVGDocument $svg, array $arr_tpl,  cls_tb_design_image $cls_image, cls_tb_design_font $cls_font, cls_instagraph $cls_instagraph, array $arr_json, $p_dpi, $p_page = 0, $p_is_admin=true, $p_rate=1){
        $arr_canvas = isset($arr_json['canvases'])?$arr_json['canvases']:array();
        $arr_design = isset($arr_canvas[$p_page])?$arr_canvas[$p_page]:array();

        $v_folding_type = isset($arr_json['folding'])?$arr_json['folding']:'none';
        $v_die_cut_type = isset($arr_json['dieCutType'])?$arr_json['dieCutType']:'none';
        if($v_die_cut_type=='undefined' || $v_die_cut_type=='') $v_die_cut_type = 'none';
        $v_folding_direction = isset($arr_json['foldingDirection'])?$arr_json['foldingDirection']:'none';
        $v_bleed = isset($arr_design['bleed'])?$arr_design['bleed']:'0';
        settype($v_bleed, 'float');
        $v_page_count = 1;
        if($v_bleed>0){
            if($v_folding_type!='none' && $v_die_cut_type=='none'){
                if(in_array($v_folding_type, array('zFold', 'triFold','letterFold'))){
                    $v_page_count = 3;
                }else if(in_array($v_folding_type, array('accordionFold','rollFold'))){
                    $v_page_count = 4;
                }
            }
        }
        $v_width_inch = isset($arr_json['width'])?$arr_json['width']:0;
        $v_height_inch = isset($arr_json['height'])?$arr_json['height']:0;
        $v_bg_color = isset($arr_design['bg_color'])?$arr_design['bg_color']:'transparent';
        if(strlen($v_bg_color)!=6)
            $v_bg_color = 'transparent';
        else
            $v_bg_color = '#'.$v_bg_color;
//$p_dpi = 72;
        $v_width = ceil($v_width_inch * $p_dpi);
        $v_height = ceil($v_height_inch * $p_dpi);
        if($v_width<=0) $v_width = 50;
        if($v_height<=0) $v_height = 50;


        $arr_size = array(
            'width'=>$v_width, 'height'=>$v_height
        );
        //$white_image = new Imagick();
        //$white_image->newimage($v_width, $v_height, new ImagickPixel('white'));


//Images
        $arr_images = isset($arr_design['images'])?$arr_design['images']:array();
        if(count($arr_images)>1) $arr_images = $this-> record_sort($arr_images, 'zindex');
//$v_str = 'Width: '.$v_width.' --- Height: '.$v_height."\r\n";



        $v_root_dir = ROOT_DIR.DS;
        $v_font_dir = DESIGN_FONT_DIR.DS;
        $v_tmp_dir = DESIGN_TEMP_DIR.DS;
        $this->svg->setUsedCMYK();
        for($i=0; $i<count($arr_images);$i++){
            $v_based = isset($arr_images[$i]['based']) && $arr_images[$i]['based'];
            if($v_based && $this->get_no_background()) continue;
            $v_image_id = isset($arr_images[$i]['image_id'])?$arr_images[$i]['image_id']:0;
            $v_image_name = isset($arr_images[$i]['name'])?$arr_images[$i]['name']:'';
            $v_width = isset($arr_images[$i]['width'])?$arr_images[$i]['width']:0;
            $v_height = isset($arr_images[$i]['height'])?$arr_images[$i]['height']:0;
            $v_rotation = isset($arr_images[$i]['rotation'])?$arr_images[$i]['rotation']:0;
            $v_left = isset($arr_images[$i]['left'])?$arr_images[$i]['left']:0;
            $v_top = isset($arr_images[$i]['top'])?$arr_images[$i]['top']:0;
            $v_crop = isset($arr_images[$i]['crop'])?$arr_images[$i]['crop']:false;
            $v_flip = isset($arr_images[$i]['flip'])?$arr_images[$i]['flip']:'none';
            $v_filter = isset($arr_images[$i]['filter'])?$arr_images[$i]['filter']:'undefined';
            $v_noprint = isset($arr_images[$i]['noprint']);
            if($v_noprint) $v_noprint = $arr_images[$i]['noprint'];
            $v_cropTop = isset($arr_images[$i]['cropTop'])?$arr_images[$i]['cropTop']:0;
            $v_cropLeft = isset($arr_images[$i]['cropLeft'])?$arr_images[$i]['cropLeft']:0;
            $v_cropWidth = isset($arr_images[$i]['cropWidth'])?$arr_images[$i]['cropWidth']:0;
            $v_cropHeight = isset($arr_images[$i]['cropHeight'])?$arr_images[$i]['cropHeight']:0;
            $v_border_width = isset($arr_images[$i]['border_width'])?$arr_images[$i]['border_width']:0;
            $v_border_color = isset($arr_images[$i]['border_color'])?$arr_images[$i]['border_color']:'transparent';
            $v_svg_id = isset($arr_images[$i]['svg_id'])?intval($arr_images[$i]['svg_id']):0;
            $v_svg_colors = isset($arr_images[$i]['svg_colors'])?$arr_images[$i]['svg_colors']:array();

            if(!is_array($v_svg_colors)){
                if($v_svg_colors!='') $arr_svg_colors = json_decode($v_svg_colors, true);
            }else $arr_svg_colors = $v_svg_colors;
            if(!isset($arr_svg_colors) || !is_array($arr_svg_colors)) $arr_svg_colors = array();

            if($v_filter.''=='undefined') $v_filter = 'none';
            if(strlen($v_border_color)<6)
                $v_border_color = 'transparent';
            else{
                $v_border_color = $v_border_color == 'transparent'?$v_border_color:'#'.substr($v_border_color,0,6);
            }
            //$v_border_width = ceil($v_border_width);
            settype($v_border_width, 'int');
            settype($v_cropLeft, 'float');
            settype($v_cropTop, 'float');
            settype($v_cropWidth, 'float');
            settype($v_cropHeight, 'float');

            $v_left -= $v_bleed;
            $v_top -= $v_bleed;

            $v_top *= $p_dpi;
            $v_left *= $p_dpi;
            $v_width *= $p_dpi;
            $v_height *= $p_dpi;
            $v_width = ceil($v_width);
            $v_height = ceil($v_height);
            $v_top = ceil($v_top);
            $v_left = ceil($v_left);
            $v_noprint = $v_noprint && !$p_is_admin;
            if(!$v_noprint){
                if($v_image_id>0){
                    if($this->get_no_image()) continue;
                    $v_row = $cls_image->select_one(array('image_id'=>$v_image_id));
                    if($v_row==1){
                        $v_directory = $cls_image->get_saved_dir();
                        $v_directory = $v_root_dir.$v_directory;
                        $v_image = $cls_image->get_image_file();
                        $v_image_width = $cls_image->get_image_width();
                        $v_image_height = $cls_image->get_image_height();
                        $v_image_original = $cls_image->get_image_original_file();
                        $v_image_extension = $cls_image->get_image_extension();
                        $arr_original_colors = $cls_image->get_svg_original_colors();
                        $v_image = $v_directory.$v_image;


                        if($v_svg_id>0){
                            $v_svg_file = $v_directory . $v_image_original;
                            if(file_exists($v_svg_file)){
                                $v_svg_data = file_get_contents($v_svg_file);
                                for($k = 0; $k<sizeof($arr_original_colors) && $k<sizeof($arr_svg_colors); $k++){
                                    $v_or_svg_color = $arr_original_colors[$k];
                                    $v_svg_color = $arr_svg_colors[$k];
                                    if(strlen($v_svg_color)==6) $v_svg_color = '#'.$v_svg_color;
                                    if(strlen($v_or_svg_color)==6) $v_or_svg_color = '#'.$v_or_svg_color;
                                    $v_svg_data = str_ireplace($v_or_svg_color, $v_svg_color, $v_svg_data);
                                }
                                $tpl = new Template($arr_tpl[1], DESIGN_DATA_DIR);
                                $tpl->set('WIDTH_IN_PLACEHOLDER', $v_width_inch);
                                $tpl->set('HEIGHT_IN_PLACEHOLDER', $v_height_inch);
                                $svg_tmp = new SVGDocument($tpl->output());
                                $svg_image = new SVGDocument($v_svg_data);
                                $svg_tmp->addDefs($svg_image->getDefs());
                                if($svg_image->g){
                                    $v_scale = $v_width / $v_image_width;
                                    $v_transform = "translate({$v_left},{$v_top}) scale({$v_scale},{$v_scale})";
                                    $svg_image->g->setTransform($v_transform);
                                    $svg->addShape($svg_image->g);
                                }
                            }
                        }else if(file_exists($v_image)){
                            if($v_image_original!=''){
                                $v_image_original = $v_directory . $v_image_original;
                                if(file_exists($v_image_original) && is_file($v_image_original)) $v_image = $v_image_original;
                            }
                            if($v_border_width>0){
                            //    $v_border_width = ceil($v_border_width*$p_dpi/(72*1.2));
                            }

                            $v_temp = false;
                            if($v_filter!='none'){
                                $cls_instagraph->set_input($v_image);
                                $v_output = $v_tmp_dir.date('YmdHis').'.'.$v_image_extension;
                                $cls_instagraph->set_output($v_output);
                                $v_filter = ucwords($v_filter);
                                if($cls_instagraph->process($v_filter)){
                                    $v_image = $v_output;
                                    //$cls_instagraph->output();
                                    $v_temp = true;
                                }
                            }


                            $this->set_stroke_color($v_border_color);
                            $this->set_stroke_width($v_border_width * ($v_image_width / $v_width));
                            $this->set_x_pos($v_left);
                            $this->set_y_pos($v_top);
                            $this->set_width($v_image_width);
                            $this->set_rotation($v_rotation);
                            $this->set_height($v_image_height);
                            //$this->flip($image, $v_flip);
                            if($v_flip!='none'){
                                $t_image = new Imagick($v_image);
                                $v_image = $v_tmp_dir . create_random_password(10, true, true).'_'.date('YmdHis').'png';
                                $this->flip($t_image, $v_flip);
                                $t_image->setimageformat('png');
                                $t_image->writeimage($v_image);
                                $t_image->clear();
                                $t_image->destroy();
                                $v_temp = true;
                            }
                            $t_image = $this->crop_image($v_image, $v_cropLeft, $v_cropTop, $v_cropWidth, $v_cropHeight);


                            if($v_temp){
                                if(file_exists($v_image)) @unlink($v_image);
                            }
                            $t_image->setimageformat('png');
                            $v_file_name = $v_tmp_dir.'tmp_'.date('YmdHis').'.png';
                            $t_image->writeimage($v_file_name);
                            $svg_image = SVGImage::getInstance($v_left, $v_top,$v_image_name, $v_file_name);
                            //$style = new SVGStyle(array('width'=>$v_width.'px', 'height'=>$v_height.'px'));
                            //$svg_image->setStyle($style);
                            $svg_image->setWidth($v_width);
                            $svg_image->setHeight($v_height);
                            $svg->addShape($svg_image);
                            @unlink($v_file_name);
                            $t_image->clear();
                            $t_image->destroy();
                        }
                    }

                }else{
                    if($this->get_no_shape()) continue;
                    $v_shape_type = isset($arr_images[$i]['shape_type'])?$arr_images[$i]['shape_type']:'hline';
                    $v_border_stroke = isset($arr_images[$i]['border_stroke'])?$arr_images[$i]['border_stroke']:'solid';
                    $v_shape_fill_color = isset($arr_images[$i]['fill_color'])?$arr_images[$i]['fill_color']:'transparent';
                    $v_rounded = isset($arr_images[$i]['rounded'])?$arr_images[$i]['rounded']:'0';

                    $v_shape_fill_color = trim($v_shape_fill_color);

                    if(strlen($v_shape_fill_color)<6)
                        $v_shape_fill_color = 'transparent';
                    else{
                        $v_shape_fill_color = $v_shape_fill_color == 'transparent'?$v_shape_fill_color: '#'.substr($v_shape_fill_color, 0, 6);
                    }

                    $arr_info = array(
                        'width'=>$v_width,
                        'height'=>$v_height,
                        'rotation'=>$v_rotation,
                        'fill'=>$v_shape_fill_color,
                        'round'=>$v_rounded,
                        'border'=>array(
                            'width'=>$v_border_width,
                            'color'=>$v_border_color,
                            'linecap'=>$v_border_stroke=='solid'?'butt':$v_border_stroke
                            ,'dasharray'=>$v_border_stroke
                        )
                    );
                    $v_svg_data = $this->svg->createSVGShape($arr_info, $v_shape_type);
                    $s = new SVGDocument($v_svg_data);
                    $v_attr = '';
                    if($s->g){
                        $s = $s->g;
                        $v_attr = $s->getAttribute('transform');
                        if($v_attr!=''){
                            $v_attr .= ' translate('.$v_left.','.$v_top.')';
                            $s->setAttribute('transform', $v_attr);
                            $svg->addShape($s);
                        }
                    }
                    //$im = $this->svg->drawShape($arr_info, $v_shape_type);
                    //if($im){
                    //    $image->compositeimage($im, Imagick::COMPOSITE_DEFAULT, $v_left, $v_top);
                    //}

                    /*
                    $this->set_x_pos($v_left);
                    $this->set_y_pos($v_top);
                    $this->set_width($v_width);
                    $this->set_height($v_height);
                    $this->set_fill_color($v_shape_fill_color);
                    $this->set_stroke_color($v_border_color);
                    $this->set_stroke_width($v_border_width);
                    $this->set_stroke_style($v_border_stroke);
                    //$this->set_rotation(0);
                    $t_image = $this->create_shape($v_shape_type, $v_rotation);
                    $t_image->setimageformat('png');
                    $v_file_name = $v_tmp_dir.'tmp_'.date('YmdHis').'.png';
                    $t_image->writeimage($v_file_name);
                    $svg->addShape(SVGImage::getInstance($v_left, $v_top,$v_image_name, $v_file_name));
                    @unlink($v_file_name);
                    $t_image->clear();
                    $t_image->destroy();//$v_str .= "Left: ".$v_left. ' - Top: '.$v_top.' - Width: '.$v_width.' - Height: '.$v_height."\r\n\r\n";
                    */

                }
            }

        }

        $arr_texts = isset($arr_design['texts'])?$arr_design['texts']:array();

        for($i=0; $i<count($arr_texts);$i++){
            $v_based = isset($arr_texts[$i]['based']) && $arr_texts[$i]['based'];
            if($v_based && $this->get_no_background()) continue;
            if($this->get_no_text()) continue;

            $v_color = isset($arr_texts[$i]['color'])?$arr_texts[$i]['color']:'black';
            if(strlen($v_color)!=6)
                $v_color = '#000000';
            else{
                $v_color = '#'.substr($v_color,0,6);
            }
            $v_name = isset($arr_texts[$i]['name'])?$arr_texts[$i]['top']:'';
            $v_top = (float) $arr_texts[$i]['top'];
            $v_left = (float) $arr_texts[$i]['left'];
            $v_width = (float) $arr_texts[$i]['width'];
            $v_height = (float) $arr_texts[$i]['height'];
            $v_body = isset($arr_texts[$i]['body'])?$arr_texts[$i]['body']:'';
            $v_bold = isset($arr_texts[$i]['bold'])?$arr_texts[$i]['bold']:false;
            $v_italic = isset($arr_texts[$i]['italic'])?$arr_texts[$i]['italic']:false;
            $v_gravity = isset($arr_texts[$i]['gravity'])?$arr_texts[$i]['gravity']:'center';
            $v_line = isset($arr_texts[$i]['line'])?$arr_texts[$i]['line']:'none';
            $v_character = isset($arr_texts[$i]['character'])?$arr_texts[$i]['character']:'none';
            $v_flip = isset($arr_texts[$i]['flip'])?$arr_texts[$i]['flip']:'none';
            $v_rotation = isset($arr_texts[$i]['rotation'])?$arr_texts[$i]['rotation']:0;
            $v_point_size = isset($arr_texts[$i]['pointsize'])?$arr_texts[$i]['pointsize']:0;
            settype($v_point_size, 'float');
            $v_bullet_style = isset($arr_texts[$i]['bullet_style'])?$arr_texts[$i]['bullet_style']:'undefined';

            $v_left -= $v_bleed;
            $v_top -= $v_bleed;

            if($v_body==''){
                if($p_is_admin){
                    $v_body = isset($arr_texts[$i]['preset_text'])?$arr_texts[$i]['preset_text']:'';
                }
            }
            $v_point = $v_point_size;
            //$v_point_size = ceil($v_point_size * $p_dpi/72);
            $v_font_key = '';
            $v_font_name =  isset($arr_texts[$i]['font'])?$arr_texts[$i]['font']:'Arial';
            $v_font_key = seo_friendly_url($v_font_name);

            $v_font_row = $cls_font->select_one(array('font_key'=>$v_font_key));
            if($v_font_row==1){
                $arr_font_array = $cls_font->get_array_font_name();
            }else $arr_font_array = array();

            $v_default_font = $v_font_dir.'arial'.DS.'regular.ttf';

            if($v_bold && $v_italic){
                $v_font = $v_font_dir.$v_font_key.DS.'both.ttf';
                if(isset($arr_font_array['both']) && $arr_font_array['both']!='') $v_font_name = $arr_font_array['both'];
            }else if($v_bold && !$v_italic){
                $v_font = $v_font_dir.$v_font_key.DS.'bold.ttf';
                if(isset($arr_font_array['bold']) && $arr_font_array['bold']!='') $v_font_name = $arr_font_array['bold'];
            }else if(!$v_bold && $v_italic){
                $v_font = $v_font_dir.$v_font_key.DS.'italic.ttf';
                if(isset($arr_font_array['italic']) && $arr_font_array['italic']!='') $v_font_name = $arr_font_array['italic'];
            }else{
                $v_font = $v_font_dir.$v_font_key.DS.'regular.ttf';
                if(isset($arr_font_array['regular']) && $arr_font_array['regular']!='') $v_font_name = $arr_font_array['regular'];
            }
            if(!file_exists($v_font)) $v_font = $v_default_font;

            $v_top *= $p_dpi;
            $v_left *= $p_dpi;

            $v_top = ceil($v_top);
            $v_left = ceil($v_left);

            $this->set_height($v_height);
            $this->set_width($v_width);

            //if($v_point_size>0){
            //    $v_point_size = ($v_point * $p_dpi)/72;
            //}
            $v_width =  ceil($v_width * $p_dpi);
            $v_height = ceil($v_height * $p_dpi);
            if($v_point_size<=0){
                $v_height = ceil( $v_height * $p_dpi);
                $v_width = ceil( $v_width * $p_dpi);
                if($v_height>0){
                    $v_point_size = ceil($v_height/1.3333333333333);
                }else
                    $v_point_size = ceil($v_width/1.3333333333333);
                $v_point_size /= 3;
            }

            $v_point_size *= $p_rate;
            $v_width *= $p_rate;
            $v_height *= $p_rate;

            //if($v_fill!='transparent') $v_fill = '#'.$v_fill;

            //$this->set_fill_color($v_color);
            $this->set_fill_color('#000000');
            $this->set_height($v_height);
            $this->set_width($v_width);
            $this->set_rotation($v_rotation);
            $this->set_x_pos($v_left);
            $this->set_y_pos($v_top);


            if($v_character=='upper')
                $v_body = strtoupper($v_body);
            else if($v_character=='lower')
                $v_body = strtolower($v_body);
            else if($v_character=='capitalize')
                $v_body = ucwords($v_body);

            $arr_text_font = array(
                'font'=>strpos(trim($v_font_name),' ')>0?"'".$v_font_name."'":$v_font_name
                ,'path'=>$v_font
                ,'size'=>$v_point_size
                ,'name'=>$v_name
                ,'key'=>$v_font_key
                ,'bold'=>$v_bold?'bold':'normal'
                ,'italic'=>$v_italic?'italic':'normal'
                ,'rotation'=>$v_rotation
                ,'body'=>$v_body
                ,'line'=>$v_line
                ,'flip'=>$v_flip
                ,'gravity'=>$v_gravity
                ,'bullet'=>$v_bullet_style
                ,'fill'=>$v_color
                ,'left'=>$v_left
                ,'top'=>$v_top
                ,'width_in'=>$v_width_inch
                ,'height_in'=>$v_height_inch
            );
            if(!file_exists($v_font)){
                $arr_font['name'] = 'Arial';
                $arr_font['key'] = 'arial';
            }
            $tpl = new Template($arr_tpl[0], DESIGN_DATA_DIR);
            $svg = $this->svg_text($svg, $tpl, $v_width, $v_bleed, $arr_text_font, $arr_size);
            /*Add svg text*/
/*
            $this->set_fill_color('#FF0000');
            $this->set_height($v_height);
            $this->set_width($v_width);
            $this->set_rotation($v_rotation);
            $this->set_x_pos($v_left);
            $this->set_y_pos($v_top);

            $t_image = new Imagick();
            if($v_character=='upper')
                $v_body = strtoupper($v_body);
            else if($v_character=='lower')
                $v_body = strtolower($v_body);
            else if($v_character=='capitalize')
                $v_body = ucwords($v_body);
            $this->text($t_image, $v_body, $v_gravity, $v_font, $v_point_size*1.33333333333333333333333, $v_bullet_style,$v_line,1);
            $this->flip($t_image, $v_flip);


            $t_image->setimageformat('png');

            $v_file_name = DESIGN_TEMP_DIR.DS.'tmp_'.date('YmdHis').'.png';
            $t_image->writeimage($v_file_name);
            list($w, $h) = getimagesize($v_file_name);

            $svg_image = SVGImage::getInstance($v_left, $v_top, $v_name, $v_file_name);
            $svg_image->setHeight(ceil($h / $p_rate));
            $svg_image->setWidth(ceil($w / $p_rate));
            $svg->addShape($svg_image);
            $t_image->clear();
            $t_image->destroy();
*/
            /* End */
        }
        return $svg;
    }

    public function resize_svg($p_svg_data, $p_template_file, $p_temp_dir = DESIGN_TEMP_DIR, $p_scale = 1, array $arr_new_position = array('x'=>0, 'y'=>0)){
        $x = isset($arr_new_position['x'])?$arr_new_position['x']:0;
        $y = isset($arr_new_position['y'])?$arr_new_position['y']:0;
        $im = new Imagick();


    }
    public function remove_temp_file($p_dir, $p_calc_time, $p_after_hours = 1){
        if(!file_exists($p_dir) || !is_dir($p_dir)) return -1;
        $v_return = 0;
        $v_ds = DIRECTORY_SEPARATOR;
        $handle = opendir($p_dir);
        if($handle){
            while(($file=readdir($handle))!==false){
                if($file!='.' && $file!='..'){
                    $v_dir = $p_dir.$v_ds.$file;
                    if(is_file($v_dir)){
                        $v_time = filectime($v_dir);
                        $v_distance = $p_calc_time - $v_time;
                        if($v_distance >= 3600 * $p_after_hours){
                            if(@unlink($v_dir)) $v_return++;
                        }
                    }else if(is_dir($v_dir)){
                        $v_return += $this->remove_temp_file($v_dir, $p_calc_time, $p_after_hours);
                    }
                }
            }
            closedir($handle);
        }
        return $v_return;
    }

    public function convert_pdf2png($p_path, $p_to_ext='png', $p_remove = true){
        $v_ext = '.pdf';
        $v_ext_len = strlen($v_ext);
        $image = new Imagick($p_path."[0]");
        $v_name = basename($p_path);
        $v_path = str_replace($v_name,'', $p_path);

        $v_tmp = substr($v_name,0, strlen($v_name)-$v_ext_len);
        $v_name = $v_tmp.'.'.$p_to_ext;
        $v_file = $v_path . $v_name;
        if(file_exists($v_file)){
            $i=0;
            do{
                $i++;
                $v_name = $v_tmp.'('.$i.').'.$p_to_ext;
                $v_file = $v_path . $v_name;
                $v_loop = file_exists($v_file);
            }while($v_loop);
        }

        $image->setimageformat($p_to_ext);

        if($p_remove) @unlink($p_path);

        $image->writeimage($v_path.$v_name);
        $image->clear();
        $image->destroy();
        //$v_name = '';
        if(!file_exists($v_path.$v_name)){
            $v_name = '';
        }
        return array(
            'ext'=>$p_to_ext
            ,'file'=>$v_path.$v_name
            ,'name'=>$v_name
        );
    }

    public function rotate_size($p_width, $p_height, $p_radian){
        $x1 = -$p_width/2;
        $x2 = $p_width/2;
        $x3 = $p_width/2;
        $x4 = -$p_width/2;
        $y1 = $p_height/2;
        $y2 = $p_height/2;
        $y3 = -$p_height/2;
        $y4 = -$p_height/2;

        $x11 = $x1 * cos($p_radian) + $y1 * sin($p_radian);
        $y11 = -$x1 * sin($p_radian) + $y1 * cos($p_radian);
        $x21 = $x2 * cos($p_radian) + $y2 * sin($p_radian);
        $y21 = -$x2 * sin($p_radian) + $y2 * cos($p_radian);
        $x31 = $x3 * cos($p_radian) + $y3 * sin($p_radian);
        $y31 = -$x3 * sin($p_radian) + $y3 * cos($p_radian);
        $x41 = $x4 * cos($p_radian) + $y4 * sin($p_radian);
        $y41 = -$x4 * sin($p_radian) + $y4 * cos($p_radian);

        $x_min = min($x11,$x21,$x31,$x41);
        $x_max = max($x11,$x21,$x31,$x41);

        $y_min = min($y11,$y21,$y31,$y41);
        $y_max = max($y11,$y21,$y31,$y41);

        return array('w'=>$x_max-$x_min,'h'=>$y_max-$y_min);
    }

    public function rgb2cmyk($p_red=255, $p_green = 255, $p_blue = 255){
        /*
        $r = $p_red / 255;
        $g = $p_green / 255;
        $b = $p_blue / 255;
        $k = 1 - max($r, $g, $b);
        $c = (1 - $r - $k)/(1 - $k);
        $m = (1 - $g - $k)/(1 - $k);
        $y = (1 - $b - $k)/(1 - $k);
        return array('c'=>round($c,2), 'm'=>round($m,2), 'y'=>round($y,0), 'k'=>round($k,0));
        */

        $cyan = 255 - $p_red;
        $magenta = 255 - $p_green;
        $yellow = 255 - $p_blue;
        $black = min($cyan, $magenta, $yellow);
        if($black!=255){
            $cyan = @(($cyan - $black) / (255 - $black)) * 255;
            $magenta = @(($magenta - $black) / (255 - $black)) * 255;
            $yellow = @(($yellow - $black) / (255 - $black)) * 255;
        }
        return array('c' => round($cyan / 255,0), 'm' => round($magenta / 255,0), 'y' => round($yellow / 255,0), 'k' => round($black / 255,0));
    }

    function rgb2cmyk4($var1,$g=0,$b=0) {
        if(is_array($var1)) {
            $r = $var1['r'];
            $g = $var1['g'];
            $b = $var1['b'];
        } else
            $r=$var1;
        $cyan = 255 - $r;
        $magenta = 255 - $g;
        $yellow = 255 - $b;
        $black = min($cyan, $magenta, $yellow);
        $cyan = @(($cyan - $black) / (255 - $black)) * 255;
        $magenta = @(($magenta - $black) / (255 - $black)) * 255;
        $yellow = @(($yellow - $black) / (255 - $black)) * 255;
        return array('c' => $cyan / 255, 'm' => $magenta / 255, 'y' => $yellow / 255, 'k' => $black / 255);
    }
    public function  cmyk2rgb($p_cyan = 1, $p_magenta = 1, $p_yellow = 1, $p_key = 1){
        $r = 255 * (1 - $p_cyan) * (1 - $p_key);
        $g = 255 * (1 - $p_magenta) * (1 - $p_key);
        $b = 255 * (1 - $p_yellow) * (1 - $p_key);
        return array('r'=>$r, 'g'=>$g, 'b'=>$b);
    }

    function rgb2cmyk1($var1,$g=0,$b=0) {
        if(is_array($var1)) {
            $r = $var1['r'];
            $g = $var1['g'];
            $b = $var1['b'];
        } else
            $r=$var1;
        $cyan = 255 - $r;
        $magenta = 255 - $g;
        $yellow = 255 - $b;
        $black = min($cyan, $magenta, $yellow);
        $cyan = @(($cyan - $black) / (255 - $black)) * 255; $magenta = @(($magenta - $black) / (255 - $black)) * 255; $yellow = @(($yellow - $black) / (255 - $black)) * 255; return array('c' => $cyan / 255, 'm' => $magenta / 255, 'y' => $yellow / 255, 'k' => $black / 255);
    }

    public function hex2decimal($p_hex){
        $arr_hex = array(
            '0'=>0, '1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, 'A'=>10, 'B'=>11, 'C'=>12, 'D'=>13, 'E'=>14, 'F'=>15
        );
        $p_hex = strtoupper($p_hex);
        $L = strlen($p_hex);
        $r = 0;
        $i = 0;
        while($i<$L){
            $p = $L - $i - 1;
            $c = substr($p_hex, $i, 1);
            if(isset($arr_hex[$c]))
                $r += pow(16, $p) * $arr_hex[$c];
            $i++;
        }
        return $r;
    }

    public function hex2rgb($p_hex, $p_default_hex='000000'){
        $v_check = '0123456789ABCDEF';
        $v_hex = strtoupper($p_hex);
        if(strlen($v_hex)!=6)
            $v_hex = $p_default_hex;
        else{
            $v_flag = false;
            $i = 0;
            while($i<6 && !$v_flag){
                $c = substr($v_hex, $i, 1);
                $v_flag = strpos($v_check, $c)===false;
                $i++;
            }
            if($v_flag) $v_hex = $p_default_hex;
        }
        if(strlen($p_hex)==3){
            $v_red = $this->hex2decimal(substr($v_hex,0, 1));
            $v_green = $this->hex2decimal(substr($v_hex,1, 1));
            $v_blue = $this->hex2decimal(substr($v_hex,2, 1));
        }else if(strlen($p_hex)==6){
            $v_red = $this->hex2decimal(substr($v_hex,0, 2));
            $v_green = $this->hex2decimal(substr($v_hex,3, 2));
            $v_blue = $this->hex2decimal(substr($v_hex,5, 2));
        }else{
            $v_red = null;
            $v_green = null;
            $v_blue = null;
        }
        return array('r'=>$v_red, 'g'=>$v_green, 'b'=>$v_blue);
    }

    public function rgb2hex( $r, $g, $b ){
        //String padding bug found and the solution put forth by Pete Williams (http://snipplr.com/users/PeteW)
        $hex = "#";
        $hex.= str_pad( dechex( $r ), 2, "0", STR_PAD_LEFT );
        $hex.= str_pad( dechex( $g ), 2, "0", STR_PAD_LEFT );
        $hex.= str_pad( dechex( $b ), 2, "0", STR_PAD_LEFT );

        return $hex;
    }

    public function hex2cymk($p_hex, $p_default_hex='000000'){
        $arr_rgb = $this->hex2rgb($p_hex, $p_default_hex);
        return $this->rgb2cmyk($arr_rgb['r'], $arr_rgb['g'], $arr_rgb['b']);
    }

    private function special_replace($str){
        $specials = array('&', '<', '>');
        $replaces = array('&amp;', '&lt;', '&gt;');
        for($i=0; $i<sizeof($replaces); $i++)
            $str = str_replace($specials[$i], $replaces[$i], $str);
        return $str;
    }

    public function get_font($p_font_name, $p_font_dir = DESIGN_FONT_DIR, array $arr_style = array(), $p_default_font='Arial'){
        $v_ds = DIRECTORY_SEPARATOR;
        $v_font_dir = $p_font_dir;
        if(strrpos($v_font_dir, $v_ds)!==strlen($v_font_dir)-1) $v_font_dir .= $v_ds;
        $v_font_name =  $p_font_name;
        $v_font_key = seo_friendly_url($v_font_name);
        $v_default_font = $v_font_dir.seo_friendly_url($p_default_font).DS.'regular.ttf';
        $v_bold = isset($arr_style['bold']) && $arr_style['bold'];
        $v_italic = isset($arr_style['italic']) && $arr_style['italic'];
        if($v_bold && $v_italic){
            $v_font = $v_font_dir.$v_font_key.DS.'both.ttf';
        }else if($v_bold && !$v_italic)
            $v_font = $v_font_dir.$v_font_key.DS.'bold.ttf';
        else if(!$v_bold && $v_italic)
            $v_font = $v_font_dir.$v_font_key.DS.'italic.ttf';
        else
            $v_font = $v_font_dir.$v_font_key.DS.'regular.ttf';
        if(!file_exists($v_font)){
            $v_font = $v_default_font;
            $v_font_key = seo_friendly_url($p_default_font);
        }
        return array('name'=>$v_font_name, 'key'=>$v_font_key, 'dir'=>$v_font);
    }


    /**
     * @param $p_text
     * @param $p_font
     * @param $p_size
     * @param $p_rotation
     * @param $p_max_width
     * @param string $p_align
     * @param array $arr_position
     * @param bool $p_flip_x
     * @param bool $p_flip_y
     * @return array
     */
    function calculate_text_svg($p_text, $p_font, $p_size, $p_rotation, $p_max_width, $p_align="west",  array $arr_position = array(),  $p_flip_x = false, $p_flip_y = false){
        $v_left = isset($arr_position['x'])?$arr_position['x']:0;
        $v_top = isset($arr_position['y'])?$arr_position['y']:0;

        //$p_size *= 1.3333333333333333333333333;
        $p_size *= (90/72);
/*
        $rect = imagettfbbox($p_size,0,$p_font,$p_text);
        $minX = min(array($rect[0],$rect[2],$rect[4],$rect[6]));
        $maxX = max(array($rect[0],$rect[2],$rect[4],$rect[6]));
        $minY = min(array($rect[1],$rect[3],$rect[5],$rect[7]));
        $maxY = max(array($rect[1],$rect[3],$rect[5],$rect[7]));
        $v_text_length = $maxX - $minX;
        $v_text_height = $maxY - $minY;
*/

        $im = new Imagick();
        $draw = new ImagickDraw();
        $draw->setfont($p_font);
        //$im->setresolution(300, 300);
        $draw->setfontsize($p_size);
        $draw->settextalignment(Imagick::ALIGN_LEFT);
        $draw->setgravity(Imagick::GRAVITY_WEST);
        $metrics = $im->queryfontmetrics($draw, $p_text);
        $v_baseline = $metrics['boundingBox']['y2'] + abs($metrics['descender']);
        $v_text_length = $metrics['textWidth'] + 2 * $metrics['boundingBox']['x1'];
        $v_text_height = $metrics['textHeight'] + $metrics['descender'];

        $v_ratio = 0.87;
        $p_rotation = $p_rotation % 360;

        if($p_align=='center'){
            $v_left = ($p_max_width - $v_text_length)/2 + $v_left;
        }else if($p_align=='east'){
            $v_left = $p_max_width - $v_text_length + $v_left;
        }

        $v_length = $p_flip_x?($v_text_length + $v_text_length/strlen($p_text)):0;
        $v_height = $p_flip_y?$v_text_height:0;
        $v_center_length = $v_text_length/2;
        $v_center_height = $v_text_height/2;

        $v_transform = 'rotate('.$p_rotation.', '.($v_left + $v_center_length).', '.($v_top - $v_center_height).')';
        $v_scale_x = $p_flip_x?-1:1;
        $v_scale_y = $p_flip_y?-1:1;
        $v_transform .= ' translate('.(-($v_scale_x - 1)*$v_left + $v_length).', '.(-($v_scale_y-1)*$v_top - $v_height).')';
        $v_transform .= ' scale('.$v_scale_x.', '.$v_scale_y.')';
        $v_transform = 'transform="'.$v_transform.'"';
        return array('x'=>$v_left,'y'=>$v_top,'w'=>$v_text_length, 'h'=>$v_text_height, 'b'=>$v_baseline, 't'=>$v_transform, 'm'=>json_encode($metrics));
        //return array('x'=>$v_left,'y'=>$v_top,'w'=>$v_text_length, 'h'=>$v_text_height, 'b'=>0, 't'=>$v_transform, 'm'=>json_encode(array()));
    }
}