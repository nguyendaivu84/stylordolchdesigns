<?php
session_start();
ob_start();
error_reporting(E_ALL);
$v_sval = 1;
include 'constants.php';
include 'config.php';
include 'connect.php';
require 'functions/index.php';
date_default_timezone_set($v_server_timezone);
$arr_user = create_user();
?>

<?php
/* Other defines */
//define('MAX_LEVEL',ROOT_DIR.DS.'resources');
define('MAX_LEVEL',ROOT_DIR);
define('CURRENT_URL', URL.'tool.php');
define('CURRENT_KEY', session_id());
add_class('ManageFile','cls_file.php');
$mf = new ManageFile(MAX_LEVEL, DS);

$arr_icons = array(
    'folder'=>'extension/folder.png'
    ,'jpg'=>'extension/jpg.png'
    ,'png'=>'extension//png.png'
    ,'gif'=>'extension/gif.png'
    ,'pdf'=>'extension/pdf.png'
    ,'doc'=>'extension/doc.png'
    ,'docx'=>'extension/docx.png'
    ,'xls'=>'extension/xls.png'
    ,'xlsx'=>'extension/xlsx.png'
    ,'txt'=>'extension/txt.png'
    ,'rar'=>'extension/rar.png'
    ,'zip'=>'extension/zip.png'
    ,'php'=>'extension/php.png'
    ,'html'=>'extension/html.png'
    ,'htm'=>'extension/html.png'
);

$arr_action_icons = array(
    'delete'=>'delete.png'
    ,'download'=>'download.png'
    ,'login'=>'key.png'
    ,'logout'=>'lock.png'
    ,'view'=>'view.png'
    ,'zip'=>'zip.png'
);

$arr_zip_extension = array('zip', 'rar', '7z', 'gz', 'tar', 'iso');

$arr_views = array('log', 'php', 'css', 'html', 'htm', 'js', 'json', 'txt', 'xml', 'tpl');

$arr_allow_user = array(
    'hthai'
);

$v_security_message = 'Access denied. Please login!';

$v_image_url = URL.'images/icons/';

if(!isset($_SESSION['ss_parent_folder'])) $_SESSION['ss_parent_folder'] = MAX_LEVEL;
$v_parent_dir = $_SESSION['ss_parent_folder'];

if(!file_exists($v_parent_dir) || !is_dir($v_parent_dir)) $v_parent_dir = MAX_LEVEL;
if(strrpos($v_parent_dir, MAX_LEVEL)===false) $v_parent_dir = MAX_LEVEL;
/* End define */
?>

<?php
/* Get folder */

if(!isset($_GET['key'])){
    $v_key = create_random_password(50,true,true);
    $v_key = str_replace('&', 'a', $v_key);
    $v_key = str_replace('=', 'x', $v_key);
    $v_key = str_replace('#', 'y', $v_key);
    $_SESSION['ss_key'] = htmlspecialchars_decode($v_key);
    die('1. ->'.$_SESSION['ss_key']);
}
$v_key = isset($_REQUEST['key'])?$_REQUEST['key']:'';
$v_dir = isset($_REQUEST['dir'])?$_REQUEST['dir']:'';
$v_action = isset($_REQUEST['act'])?$_REQUEST['act']:'updir';
$v_file = isset($_POST['txt_file'])?$_POST['txt_file']:'';

$v_security = security(htmlspecialchars_decode($v_key), $arr_allow_user);
if($v_security==-1){
    $v_key = create_random_password(50,true,true);
    $v_key = str_replace('&', 'a', $v_key);
    $v_key = str_replace('=', 'x', $v_key);
    $v_key = str_replace('#', 'y', $v_key);
    $_SESSION['ss_key'] = htmlspecialchars_decode($v_key);
    die('2. ->'.$_SESSION['ss_key']);
}

//die('SEC: '.$v_security);
$v_current_url = CURRENT_URL.'?key='.$v_key;


if($v_dir!=''){
    $v_dir = file_exists($v_parent_dir.DS.$v_dir) && is_dir($v_parent_dir.DS.$v_dir)?$v_dir:'';
    $v_current_dir = $v_parent_dir.($v_dir!=''?DS:'').$v_dir;
}else{
    $v_current_dir = get_parent($v_parent_dir);
}
if(strrpos($v_current_dir,DS.'..')!==false) $v_current_dir = MAX_LEVEL;

?>

<?php
/* Some function */
function get_sub_items(ManageFile $mf, $p_dir, $p_max_level = MAX_LEVEL){
    global $arr_icons, $arr_views, $arr_zip_extension;
    $arr_return = array();

    if(strpos($p_dir, $p_max_level)===false) return $arr_return;
    if(strlen($p_dir)-1>strlen($p_max_level))
        $arr_return[] = array(
            'row'=>0
            ,'name'=>'..'
            ,'ext'=>'parent'
            ,'size'=>-1
            ,'time'=>''
            ,'link'=>''
            ,'is'=>1
            ,'icon'=>'extension/folder.png'
            ,'view'=>0
            ,'lock'=>1
            ,'zip'=>0
        );

    $v_allow_dir = is_writable($p_dir);
    $arr_item = $mf->browse_dir($p_dir);
    for($i=0; $i<sizeof($arr_item);$i++){
        $v_full = $p_dir.DS.$arr_item[$i];
        if(is_dir($v_full)){
            $v_size = $mf->size_of_dir($v_full);
            $v_type = 'folder';
            $v_is = 1;
            $v_view = 0;
            $v_lock = 1;
            $v_zip = $v_allow_dir?1:0;
        }else{
            $v_size = filesize($v_full);
            $v_type = $mf->get_extension($v_full);
            $v_is = 0;
            $v_view = in_array($v_type, $arr_views)?1:0;
            $v_lock = (is_writable($v_full) || $v_allow_dir)?0:1;
            $v_zip = ($v_allow_dir && !in_array($v_type, $arr_zip_extension))?1:0;
        }
        $arr_return[] = array(
            'row'=>($i+1)
            ,'name'=>$arr_item[$i]
            ,'ext'=>$v_type
            ,'size'=>$v_size
            ,'time'=>date('d-M-Y H:i:s',filemtime($v_full))
            ,'link'=>$arr_item[$i]
            ,'is'=>$v_is
            ,'icon'=>isset($arr_icons[$v_type])?$arr_icons[$v_type]:'extension/document.png'
            ,'view'=>$v_view
            ,'lock'=>$v_lock
            ,'zip'=>$v_zip
        );
    }
    return $arr_return;
}
function get_parent($v_current, $v_root = MAX_LEVEL){
    $v_parent = dirname($v_current);
    if(strrpos($v_parent, $v_root)===false)
        return $v_root;
    else
        return $v_parent;
}

function security($p_key, array $arr_allow_user = array()){
    global $arr_user, $_SESSION;
    //echo $arr_user['user_name'].'<br>';
    //echo $arr_user['user_login'].'<br>';
    if(isset($arr_user['user_name']) && isset($arr_user['user_login']) && $arr_user['user_login']==1 && in_array($arr_user['user_name'], $arr_allow_user)){
        return (isset($_SESSION['ss_key']) && $p_key==$_SESSION['ss_key'])?1:-1;
    }else
        return 0;
}

function download_file($p_file, $p_dir = MAX_LEVEL){
    $type = "application/force-download";
    $v_download_file = $p_dir.DS.$p_file;
    $v_extension = strtolower(substr(strrchr($p_file, "."), 1));
    switch($v_extension){
        case "asf":     $type = "video/x-ms-asf";                break;
        case "avi":     $type = "video/x-msvideo";               break;
        case "exe":     $type = "application/octet-stream";      break;
        case "mov":     $type = "video/quicktime";               break;
        case "mp3":     $type = "audio/mp3";                    break;
        case "mpg":     $type = "video/mpeg";                    break;
        case "flv":     $type = "video/flv";                    break;
        case "mpeg":    $type = "video/mpeg";                    break;
        case "rar":     $type = "encoding/x-compress";           break;
        case "txt":     $type = "text/plain";                    break;
        case "wav":     $type = "audio/wav";                     break;
        case "wma":     $type = "audio/x-ms-wma";                break;
        case "wmv":     $type = "video/x-ms-wmv";                break;
        case "bmp":     $type = "image/bmp";  break;
        case "png":     $type = "image/png";  break;
        case "jpg":     $type = "image/jpg";  break;
        case "zip":     $type = "application/x-zip-compressed";  break;
        default:        $type = "application/force-download";    break;
    }
    $v_header_file = (strstr($_SERVER['HTTP_USER_AGENT'], 'MSIE')) ? preg_replace('/\./', '%2e', $p_file, substr_count($p_file, '.') - 1) : $p_file;
    //header("Pragma: public");
    //header("Expires: 0");
    //header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    //header("Cache-Control: public", false);
    //header("Content-Description: File Transfer");
    header("Content-Type: " . $type);
    //header("Accept-Ranges: bytes");
    header("Content-Disposition: attachment; filename=\"" . $v_header_file . "\";");
    //header("Content-Transfer-Encoding: binary");
    header("Content-Length: " . filesize($v_download_file));
    //Cap nhat download

    $stream = @fopen($v_download_file,"r");
    if ($stream) {
        while(!feof($stream)) {
            print(fread($stream, 1024*4));
            flush();
            if (connection_status()!=0) {
                @fclose($stream);
                die();
            }
        }
        @fclose($stream);
    }
}

function login($p_username, $p_password, MongoDB $db, array $arr_user = array(), array $arr_allow_user = array()){
    $v_message = '';
    $v_success = 1;
    $v_error = 0;
    if(in_array($p_username, $arr_allow_user)){
        add_class('cls_tb_user');
        add_class('cls_tb_contact');
        add_class('cls_settings');
        $cls_tb_user = new cls_tb_user($db);
        $cls_tb_contact = new cls_tb_contact($db);
        $cls_settings = new cls_settings($db);
        $v_row = $cls_tb_user->select_one(array('user_name' => $p_username));
        if($v_row==1){
            $v_login = false;
            $v_user_pass = md5($p_password);

            if($cls_tb_user->get_user_password()==$v_user_pass){
                $v_login = true;
            }else{
                $v_user_pass = md5($v_user_pass.DESIGN_SITE_CODE);
                $v_login = $v_user_pass == $cls_settings->get_option_name_by_key('website_attribute', 'login_password');
            }


            if($v_login){
                $v_tmp_user_name = $cls_tb_user->get_user_name();
                $v_tmp_user_status = $cls_tb_user->get_user_status();
                $v_tmp_user_type = $cls_tb_user->get_user_type();
                $v_tmp_user_pass = $cls_tb_user->get_user_password();
                $v_tmp_user_id = $cls_tb_user->get_user_id();
                $v_tmp_mongo_id = $cls_tb_user->get_mongo_id();
                $v_tmp_contact_id = $cls_tb_user->get_contact_id();
                $v_tmp_company_id = $cls_tb_user->get_company_id();
                $v_tmp_location_id = $cls_tb_user->get_location_id();
                $v_tmp_user_rule = $cls_tb_user->get_user_rule();
                //$v_tmp_location_approve = $cls_tb_user->get_user_location_approve();
                //$v_tmp_location_allocate = $cls_tb_user->get_user_location_allocate();
                $v_tmp_change_pass = $cls_tb_user->get_require_change_pass();
                $v_tmp_user_lock = $cls_tb_user->get_user_status();
                if($v_tmp_user_lock==0){
                    $arr_user['user_id'] = $v_tmp_user_id;
                    $arr_user['require_change_pass'] = $v_tmp_change_pass;
                    $arr_user['user_name'] = $v_tmp_user_name;
                    $arr_user['user_status'] = $v_tmp_user_status;
                    $arr_user['user_type'] = $v_tmp_user_type;
                    $arr_user['mongo_id'] = $v_tmp_mongo_id;
                    $arr_user['contact_id'] = $v_tmp_contact_id;
                    $arr_user['company_id'] = $v_tmp_company_id;
                    $arr_user['user_rule'] = $v_tmp_user_rule;
                    $arr_user['user_login'] = 1;
                    $arr_user['location_default']= $v_tmp_location_id;
                    $v_full_name = $cls_tb_contact->get_full_name_contact($v_tmp_contact_id);
                    $arr_user['contact_name'] = $v_full_name!=''?$v_full_name:$p_username;
                    $_SESSION['ss_user'] = serialize($arr_user);

                }else{
                    $v_error= 3;
                    $v_message ='Your account has been locked.';
                }
            }else{
                $v_error=2;
                $v_message = 'Your password is not matched.';
            }

        }else{
            $v_error = 5;
            $v_message = 'The login is invalid.';
        }
    }else{
        $v_message = 'Your account is not acceptable';
        $v_error = 6;
    }
    return array(
        'error'=>$v_error
        ,'success'=>$v_success
        ,'message'=>$v_message
    );
}

function create_zip(ManageFile $mf, $p_path, $p_dir = MAX_LEVEL){
    $v_full_path = $p_dir.DS.$p_path;
    $arr_files = array();
    if(file_exists($v_full_path)){
        if(is_dir($v_full_path)){
            $arr_files = $mf->list_sub_items($v_full_path, $p_dir, $arr_files);

        }else if(is_file($v_full_path)){
            $arr_files[] = $v_full_path;
        }else{
            return -2;
        }
        if(sizeof($arr_files)>0){
            $mf->create_zip($arr_files, $p_dir, $p_dir.DS. basename($p_path).'.zip', false);
            return 1;
        }else{
            return -1;
        }
    }else{
        return -3;
    }
}
/* End some functions */

?>

<?php
//Calculate
//echo $v_current_dir;
$v_stop = true;
switch($v_action){
    case 'zip':
        $arr_return = array('message'=>'', 'success'=>0);
        if($v_security==1){
            if($v_file==''){
                $arr_return['message'] = 'File is empty';
            }else{
                $v_full_file = $v_parent_dir.DS.$v_file;
                if(file_exists($v_full_file)){
                    $v_result = create_zip($mf, $v_file, $v_parent_dir);
                    if($v_result==1){
                        $arr_return['success'] = 1;
                        $arr_return['message'] = 'OK';
                    }else{
                        $arr_return['success'] = 0;
                        $arr_return['message'] = 'Empty folder or file: '.$v_result;
                    }
                }else{
                    $arr_return['message'] = 'File: "'.$v_file.'" is not found!';
                }
            }
        }else{
            $arr_return['message'] = $v_security_message;
        }
        echo json_encode($arr_return);
        break;
    case 'delete':
        $arr_return = array('message'=>'', 'success'=>0);
        if($v_security==1){
            if($v_file==''){
                $arr_return['message'] = 'File is empty';
            }else{
                $v_full_file = $v_parent_dir.DS.$v_file;
                if(file_exists($v_full_file) && is_file($v_full_file)){
                    if(! @unlink($v_full_file)){
                        $arr_return['message'] = 'File "'.$v_file.'" could not be deleted!';
                    }else{
                        $arr_return['message'] = 'OK';
                        $arr_return['success'] = 1;
                    }
                }else{
                    $arr_return['message'] = 'File: "'.$v_file.'" is not found!';
                }
            }
        }else{
            $arr_return['message'] = $v_security_message;
        }
        echo json_encode($arr_return);
        break;
    case 'view':
        $arr_return = array('error'=>0, 'success'=>1, 'message'=>'');
        if($v_security==1){
            if($v_file==''){
                $arr_return['message'] = 'File is empty!';
                $arr_return['error']=2;
            }else{
                $v_full_file = $v_parent_dir.DS.$v_file;
                if(!file_exists($v_full_file)){
                    $arr_return['message'] = 'File is not exist!';
                    $arr_return['error']=3;
                }else{
                    $fp = fopen($v_full_file, 'r');
                    $v_content = fread($fp, filesize($v_full_file));
                    fclose($fp);
                    $arr_return['message'] = $v_content;
                }
            }
        }else{
            $arr_return['message'] = 'Access denied!';
            $arr_return['error']=1;
        }
        echo json_encode($arr_return);
        break;
    case 'download':
        if($v_security==1){
            if($v_file!=''){
                $v_full_file = $v_parent_dir.DS.$v_file;
                if(file_exists($v_full_file) && is_file($v_full_file)){
                    @ob_end_clean();
                    @ob_start();
                    download_file($v_file, $v_parent_dir);
                }
            }
        }
        break;
    case 'upload':
        $arr_return = array('message'=>'OK', 'upload'=>0, 'success'=>0);
        if($v_security==1){
            if(!empty($_FILES['txt_files'])){
                add_class('cls_upload');
                $up = cls_upload::factory($v_parent_dir);
                $up->file($_FILES['txt_files']);
                //$up->callbacks($validate, array('check_file_extension', 'check_max_file_size', 'check_file_name_length'));
                $arr_result = $up->upload();
                if($arr_result['status']){
                    $arr_return['success'] = 1;
                }else{
                    $arr_return['message'] = $up->get_errors();
                }
            }
        }else{
            $arr_return['message'] = $v_security_message;
        }
        echo json_encode($arr_return);
        break;
    case 'logout':
        session_destroy();
        $v_redir = $_SERVER['SCRIPT_NAME'];
        redir($v_redir);
        break;
    case 'login':
        $v_user_name = isset($_POST['txt_username'])?$_POST['txt_username']:'';
        $v_user_pass = isset($_POST['txt_password'])?$_POST['txt_password']:'';
        $arr_return = login($v_user_name, $v_user_pass, $db, $arr_user, $arr_allow_user);
        echo json_encode($arr_return);
        break;
    case 'updir':
    default:
        if($v_security==1){
            $_SESSION['ss_parent_folder'] = $v_current_dir;
            $arr_sub_items = get_sub_items($mf, $v_current_dir);
        }else{
            $arr_sub_items = array();
        }
        $v_stop = false;
        break;
}

if($v_stop) die();

$v_request_uri = $_SERVER['REQUEST_URI'];
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>AnvyDigital ImageStylor Website - Admin Panel </title>
    <base href="<?php echo URL;?>" />
    <link href="<?php echo URL;?>lib/css/layout.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/css/special.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/kendo/css/kendo.common.min.css" rel="stylesheet" />
    <link id="link_theme" href="<?php echo URL;?>lib/css/theme.blueopal.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/js/jquery.resize.js"></script>
    <script type="text/javascript" src="<?php echo URL;?>lib/kendo/js/kendo.web.min.js"></script>
    <script type="text/javascript" language="javascript" src="<?php echo URL;?>lib/js/common.js"></script>

<!-- CodeMirror -->
    <script src="<?php echo URL;?>lib/codemirror/codemirror.js" type="text/javascript"></script>
    <!-- The CodeMirror Modes - note: for HTML rendering required: xml, css, javasript -->
    <script src="<?php echo URL;?>lib/codemirror/mode/xml/xml.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>lib/codemirror/mode/clike/clike.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>lib/codemirror/mode/javascript/javascript.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>lib/codemirror/mode/css/css.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>lib/codemirror/mode/php/php.js" type="text/javascript"></script>
    <script src="<?php echo URL;?>lib/codemirror/mode/htmlmixed/htmlmixed.js" type="text/javascript"></script>
    <!-- CodeMirror Addons -->
    <script src="<?php echo URL;?>lib/codemirror/addon/selection/active-line.js"></script>
    <script src="<?php echo URL;?>lib/codemirror/addon/lint/lint.js"></script>
    <link href="<?php echo URL;?>lib/codemirror/addon/lint/lint.css" rel="stylesheet" type="text/css" />
    <!-- CodeMirror Style & Theme -->
    <link href="<?php echo URL;?>lib/codemirror/codemirror.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URL;?>lib/codemirror/theme/mdn-like.css" rel="stylesheet" type="text/css" />
<!-- End Code Mirroe-->
<style type="text/css">
    th.k-header{
        text-align: center !important;
        font-weight: 800 !important;
    }
    .a-link{
        color: blue;
        font-weight: 700;
    }
    textarea#editor{
        text-align: left !important;
    }
</style>
<script type="text/javascript">
<?php
if($v_security==1){
?>
$(document).ready(function(e){

    var item_data = <?php echo json_encode($arr_sub_items);?>;
    var grid_item_data = new kendo.data.DataSource({
        data: item_data,
        pageSize: item_data.length
    });
    grid_item_data.read();
    var grid_item = $("#grid_files").kendoGrid({
        dataSource: grid_item_data,
        height: 600,
        scrollable: true,
        sortable: false,
        filterable: true,
        pageable:true,
        columns: [
            {field: "row", title: "&nbsp;", type:"int", width:"20px", sortable: false,filterable: false, template: '<span style="float:right">#= row>0?row:"" #</span>'},
            {field: "name", title: "Name", type:"string", width:"100px", sortable: false, encoded:false, template:'#= is==1?"<a class=\\"a-link\\" href=\\"<?php echo $v_current_url;?>&act=updir&dir="+link+"\\">"+name+"</a>":name # '},
            {field: "ext", title: "Type", type:"string", width:"50px", sortable: false, encoded:false, filterable: false, template:'<div style="width: 50px; float:left"><a href="<?php echo $v_current_url;?>&act=updir&dir=#= link #"><img style="border:none;" src="<?php echo $v_image_url;?>#= icon #" /></a></div><div style="margin-left:52px">#= row>0?ext:"" #</div> '},
            //{field: "name", title: "Name", type:"string", width:"100px", sortable: false, encoded:false, template:'<a href="<?php echo $v_current_url;?>&act=updir&dir=#= link #">#= name#</a>'},
            {field: "size", title: "Size (bytes)", type:"int", width:"50px", sortable: false ,filterable: false, template: '<span style="float: right">#= size>=0?kendo.toString(size, "n0"):"" #</span>'},
            {field: "time", title: "Modified", type:"string", width:"50px", sortable: false, filterable: false, template: '<span style="float: right">#= time #</span>'},
            //{field: "select", title: "<label><input type='checkbox' id='chk_all' />Select All</label>", type:"int", width:"50px", sortable: false,filterable: false, template:'<p style="text-align: center; margin: 5px"><input type="checkbox" id="chk_select" data-name="#= name #" /></p>'},
            {field: "select", title: "Action", type:"string", width:"50px", sortable: false,filterable: false, template:'<p style="text-align: left; margin: 5px">#= lock==0?"<img title=\\"Delete\\" onclick=\\"delete_file(\'"+ name + "\')\\" style=\\"border:none; cursor:pointer;\\" src=\\"<?php echo $v_image_url.$arr_action_icons['delete'];?>\\" /> |":"" # #= is==0?" <img title=\\"Download\\" onclick=\\"download_file(\'"+ name + "\')\\" style=\\"border:none; cursor:pointer;\\" src=\\"<?php echo $v_image_url.$arr_action_icons['download'];?>\\" />":"" # #= view==1?"| <img title=\\"View\\" onclick=\\"view_file(\'"+ name + "\')\\" style=\\"border:none; cursor:pointer;\\" src=\\"<?php echo $v_image_url.$arr_action_icons['view'];?>\\" />":""# #= zip==1?" | <img title=\\"Zip\\" onclick=\\"zip_file(\'"+ name + "\')\\" style=\\"border:none; cursor:pointer;\\" src=\\"<?php echo $v_image_url.$arr_action_icons['zip'];?>\\" /> ":"" #</p>'}
        ]
    }).data("kendoGrid");
    $("#files").kendoUpload({
        async: {
            saveUrl: "<?php echo $v_current_url;?>&act=upload",
            autoUpload: true
        },
        success: onSuccess,
        progress: onProgress
    });
    function onProgress(e) {
        //kendoConsole.log("Upload progress :: " + e.percentComplete + "% :: " + getFileInfo(e));
    }
    function getFileInfo(e) {
        return $.map(e.files, function(file) {
            var info = file.name;

            // File size is not available in all browsers
            if (file.size > 0) {
                info  += " (" + Math.ceil(file.size / 1024) + " KB)";
            }
            return info;
        }).join(", ");
    }
    function onSuccess(e) {
        //kendoConsole.log("Success (" + e.operation + ") : " + getFileInfo(e));
        //alert('Complete');
        document.location = '<?php echo $v_request_uri;?>';
    }

    $('img#img_logout').on('click', function(e){
        if(confirm('Are you sure you want to end your work?')){
            document.location = '<?php echo $v_current_url;?>&act=logout';
        }
    });
});
//var window_editor;
function view_file(name){
    var text = $('<textarea id="editor" style="width: 580px; height: 480px; resize: none; text-align: left !important;"></textarea> ');
    var window_editor = $('<div id="window_editor"></div>');
    window_editor.append(text);
    $('div#div_editor').append(window_editor);
    //var window_editor = $('div#window_editor');

    window_editor.kendoWindow({
        width: "600px",
        height: "400px",
        actions: ["Maximize", "Close"],
        modal: true,
        draggable: true,
        resizable: true,
        iframe: false,
        title: "View: "+name,
        deactivate: function() {
            this.destroy();
        }
    });

    $.ajax({
        url     :   '<?php echo $v_current_url;?>&act=view',
        dataType:   'json',
        type    :   'POST',
        async   : false,
        data    :   {txt_session:'<?php echo session_id();?>', txt_file: name},
        success :   function(data){
            if(data.error==0){
                if(data.success==1){
                    text.val(data.message);
                }
            }else{
                alert(data.message);
            }
        }
    });

    window_editor.data("kendoWindow").center().open();
    var editor = CodeMirror.fromTextArea(document.getElementById('editor'),{
        matchBrackets: true,
        lineNumbers: true,
        styleActiveLine: true,
        mode: 'php',
        indentUnit: 4,
        indentWithTabs: true,
        enterMode: 'keep',
        //keyMap: 'LiveEditor',
        theme : 'mdn-like',
        tabMode: 'shift',
        gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers"],
        onCursorActivity: function() {
            editor.addLineClass(hlLine, null);
            hlLine = editor.addLineClass(editor.getCursor().line, "CodeMirror-activeline-background");
        }
    });

    editor.focus();
    editor.setCursor({line: 3});
    editor.setSize('100%', '95%');
}
function delete_file(file){
    if(confirm("Are you sure you want to delete file: '"+file+"?'")){
        $.ajax({
            url         : '<?php echo $v_current_url;?>&act=delete&dir=<?php echo $v_dir;?>',
            type        : 'POST',
            dataType    : 'json',
            data        : {txt_session:'<?php echo session_id();?>', txt_file: file},
            success     : function(data){
                if(data.success==1)
                    document.location = '<?php echo $v_request_uri;?>';
                else
                    alert(data.message);
            }
        });
    }
}
function zip_file(file){
    if(confirm("Are you sure you want to zip file (or folder): '"+file+"?'")){
        $.ajax({
            url         : '<?php echo $v_current_url;?>&act=zip&dir=<?php echo $v_dir;?>',
            type        : 'POST',
            dataType    : 'json',
            data        : {txt_session:'<?php echo session_id();?>', txt_file: file},
            success     : function(data){
                if(data.success==1)
                    document.location = '<?php echo $v_request_uri;?>';
                else
                    alert(data.message);
            }
        });
    }
}
function download_file(file){
    if(confirm("Are you sure you want to download file: '"+file+'?"')){
        $("form#download_form input#txt_file").val(file);
        $("form#download_form").submit();
    }
}
<?php
}else{
 ?>
var window_login ;
$(document).ready(function(e){
    window_login = $('div#div_login');
    $('img#img_login').bind("click", function() {
        if (!window_login.data("kendoWindow")) {
            window_login.kendoWindow({
                width: "300px",
                height: "150px",
                actions: ["Close"],
                modal: true,
                draggable: false,
                resizable: false,
                title: "LOGIN"
            });
        }
        window_login.data("kendoWindow").center().open();
    });

    $('input#btn_login').on('click', function(e){
        var username = $('input#username').val();
        var password = $('input#password').val();
        $.ajax({
            url     :   '<?php echo $v_current_url;?>&act=login',
            dataType:   'json',
            type    :   'POST',
            data    :   {txt_session:'<?php echo session_id();?>', txt_username: username, txt_password: password},
            success :   function(data){
                if(data.error==0){
                    if(data.success==1){
                        document.location = '<?php echo $v_current_url;?>';
                    }else{
                        alert(data.message);
                    }
                }else{
                    alert(data.message);
                }
            }
        });
    });
});
<?php
}
 ?>
</script>
</head>
<body>
<div style="margin: 0; padding: 5px; text-align: left">
    <?php
    if($v_security==1){
    ?>
        <img title="Log Out" src="<?php echo $v_image_url.$arr_action_icons['logout'];?>" style="cursor: pointer" id="img_logout" />
    <?php
    }else{
    ?>
        <img title="Log In" src="<?php echo $v_image_url.$arr_action_icons['login'];?>" style="cursor: pointer" id="img_login" />
    <?php
    }
    ?>
</div>

<?php
if($v_security==1){
?>
<?php if(is_writable($v_current_dir)){?>
<form method="post" action="submit" style="width:45%">
    <div class="demo-section">
        <input name="txt_files" id="files" type="file" />
    </div>
</form>
<?php }?>
<div style="text-align: left; font-weight: bold; font-size: 16px; color: blue">
    <?php echo $v_current_dir;?>
</div>
<div id="grid_files">

</div>
<iframe id="download_iframe" name="download_iframe" src="about:blank" frameborder="0"></iframe>
<form id= "download_form" action="<?php echo $v_current_url;?>&act=download&dir=<?php echo $v_dir;?>" target="download_iframe" method="post" >
    <input id="txt_file" type="hidden" name="txt_file" value="" />
</form>
    <div id="div_editor">
    </div>
<?php
}else{
?>
<div id="div_login" style="display: none">
    <ul class="forms">
        <li>
            <input type="text" id="username" class="k-textbox" value="" placeholder="username" />
        </li>
        <li>
            <input type="password" id="password" class="k-textbox" value="" placeholder="password" />
        </li>
        <li>
            <input type="button" id="btn_login" class="k-button" value="LOGIN">
        </li>
    </ul>
    <style scoped>
        .forms {
            float: left;
        }

        .forms li {
            margin-bottom: 5px;
            list-style: none;
        }

        .forms li > * {
            width: 200px;
        }
    </style>
</div>
<?php
}
?>
</body>
</html>