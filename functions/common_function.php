<?php
function addslash(){
    //global $_GET, $_REQUEST, $_POST;
    if (!get_magic_quotes_gpc() ){
        foreach($_GET as $key=>$val){
            if(is_array($val)){
                $_GET[$key] = array_map ('addslashes', $_GET[$key]) ;
            }else{
                $_GET[$key] = addslashes($val);
            }
        }

        foreach($_POST as $key=>$val){
            if(is_array($val)){
                $_POST[$key] = array_map ('addslashes', $_POST[$key]) ;
            }else{
                $_POST[$key] = addslashes($val);
            }
        }
        foreach($_COOKIE as $key=>$val){
            if(is_array($val)){
                $_COOKIE[$key] = array_map ('addslashes', $_COOKIE[$key]) ;
            }else{
                $_COOKIE[$key] = addslashes($val);
            }
        }
        foreach($_REQUEST as $key=>$val){
            if(is_array($val)){
                $_REQUEST[$key] = array_map ('addslashes', $_REQUEST[$key]) ;
            }else{
                $_REQUEST[$key] = addslashes($val);
            }
        }
    }
}
function draw_option_type($arr_type, $p_selected, $p_start_index=0){
    $v_ret='';
    for($i=$p_start_index;$i<count($arr_type);$i++){
        $v_ret.='<option value="'.$i.'"'.($i==$p_selected?' selected="selected"':'').'">'.$arr_type[$i].'</option>';
    }
    return $v_ret;
}
function seo_friendly_url($string){
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    //$string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(amp|acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    return strtolower(trim($string, '-'));
}

function draw_option_sort_key(array $arr_fields, $p_selected_field='', $p_asc = 1){
    $v_ret = '';
    for($i=0; $i<count($arr_fields); $i++){
        $v_key_value = $arr_fields[$i];
        $v_key_text = ucwords(implode(' ',explode('_', $v_key_value)));
        $v_ret .= '<option value="'.$v_key_value.'"'.($v_key_value==$p_selected_field?' selected="selected"':'').'>'.$v_key_text.'</option>';
    }
    if($v_ret!=''){
        $v_ret = '<select id="txt_sort_by" name="txt_sort_by"><option value="" selected="selected">--- Select One ---</option>'.$v_ret.'</select>&nbsp;&nbsp;';
        $v_ret .= '<label><input type="radio" name="txt_sort_type" value="1"'.($p_asc==1?' checked="checked"':'').' /> Ascending</label> / ';
        $v_ret .= '<label><input type="radio" name="txt_sort_type" value="-1"'.($p_asc!=1?' checked="checked"':'').' /> Descending</label>';
    }
    return $v_ret;
}

function get_value_uri($uri, $key, $default=''){
    $find = $key.'=';
    $v_pos = strpos($uri, $find);
    $v_p = $v_pos;
    if($v_pos!==false){
        $v_pos += strlen($find);
        $v_tmp = substr($uri,$v_pos);
        $v_pos = strpos($v_tmp,'&');
        if($v_pos>0){
            return substr($v_tmp,0,$v_pos);
        }else{
            return $v_tmp;
        }
    }else return $default;
}
function redir($p_url){
    //if(ob_get_length()>0){
    @ob_end_clean();
    @ob_start();
    //}
    if(isset($p_url) && $p_url!='')
        header("location:$p_url");
    else
        header("location:".URL);
    exit;
}
function get_real_ip_address(){
    if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){   //to check ip is pass from proxy
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function remove_invailid_char($p_str)
{
    $p_str = trim($p_str);
    $p_str = str_replace(" ","_",$p_str);
    $p_str = str_replace('"'," &quot; ",$p_str);
    $p_str = str_replace("'"," &quot; ",$p_str);
    $tmp = "";
    for ($i=0;$i<strlen($p_str);$i++){
        $v_code = ord(substr($p_str,$i,1));
        if (($v_code>=48 && $v_code<=57) || ($v_code>=65 && $v_code<=90) || ($v_code>=97 && $v_code<=122) || ($v_code==95)){
            $tmp.=substr($p_str,$i,1);
        }
    }
    while (strpos($tmp,"__")!==false){
        $tmp = str_replace("__","_",$tmp);
    }
    return $tmp;
}

function get_select_one($array_expression,$p_display)
{
    foreach($array_expression as  $key => $value ){
        if($key==$p_display) return $value;
    }
    return "";
}
function get_select_id_one($array_expression,$p_value=1)
{

    foreach($array_expression as  $key => $value ){
        if($p_value==1) return $key;
        else return $value;
    }
    return "0";
}
function get_show_all_array($arr=array())
{
    $v_array = "";

    if(is_array($arr))
    {
        foreach($arr as  $key => $value ){
            $v_array .= $value .'<br>';
        }
    }
    return $v_array;
}
function images_resize_by_width($new_width,$source_file,$dest_file, $delete_source = false){
    list($width, $height, $type, $attr) = @getimagesize($source_file);
    if (($width*$height)==0) return false;
    if ($new_width==0) return false;
    $v_percent = $new_width / $width;
    $new_height = $height * $v_percent;
    settype($new_height, 'int');
    $im_source = false;
    switch($type){
        case 1;//Gif
            $im_source = @imagecreatefromgif($source_file);
            break;
        case 2;//Jpg
            $im_source = @imagecreatefromjpeg($source_file);
            break;
        case 3;//Png
            $im_source = @imagecreatefrompng($source_file);
            break;
        default ;
            $im_source = @imagecreatefromjpeg($source_file);
            break;
    }
    $im_dest = @imagecreatetruecolor($new_width,$new_height);
    if ($im_dest) $v_ok = true;
    else $v_ok = false;

    $background = @imagecolorallocate($im_dest, 255, 255, 255);
    @imagefill($im_dest,0,0,$background);
    imagecopyresized($im_dest, $im_source,0,0,0,0, $new_width, $new_height, $width, $height);
    if($delete_source) @unlink($source_file);
    @imagejpeg($im_dest,$dest_file,100);
    @imagedestroy($im_dest);
    @imagedestroy($im_source);
    return $v_ok;
}
function images_resize_by_height($new_height,$source_file,$dest_file, $delete_source = false){
    list($width, $height, $type, $attr) = @getimagesize($source_file);
    if (($width*$height)==0) return false;
    if ($new_height==0) return false;
    $v_percent = $new_height / $height;
    $new_width = $width * $v_percent;
    settype($new_width, 'int');
    $im_source = false;
    switch($type){
        case 1;//Gif
            $im_source = @imagecreatefromgif($source_file);
            break;
        case 2;//Jpg
            $im_source = @imagecreatefromjpeg($source_file);
            break;
        case 3;//Png
            $im_source = @imagecreatefrompng($source_file);
            break;
        default ;
            $im_source = @imagecreatefromjpeg($source_file);
            break;
    }
    $im_dest = @imagecreatetruecolor($new_width,$new_height);
    if ($im_dest) $v_ok = true;
    else $v_ok = false;

    $background = @imagecolorallocate($im_dest, 255, 255, 255);
    @imagefill($im_dest,0,0,$background);
    imagecopyresized($im_dest, $im_source,0,0,0,0, $new_width, $new_height, $width, $height);
    if($delete_source) @unlink($source_file);
    @imagejpeg($im_dest,$dest_file,100);
    @imagedestroy($im_dest);
    @imagedestroy($im_source);
    return $v_ok;
}

function news_pagination($page_count, $cur_page, $link, $limit=4, $end=".html", $split=""){
    $v_url = $link;
    if(substr($v_url,strlen($v_url),1)!='/') $v_url.='/';
    $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+2));

    $first_page = $cur_page > 3 ? '<li> <a  title="Page 1" href="'.$v_url.'page1'.$end.'">1</a></li> '.($cur_page < $limit+1 ? "{$split} " : ' <a href="#">...</a> ') : null;
    $last_page = $cur_page < $page_count-2 ? ($cur_page > $page_count-$limit ? "{$split} " : ' <li><a href="#">...</a></a> </li> ').'<li> <a title="Page '.$page_count.'" href="'.$v_url."page{$page_count}".$end.'">'.(($page_count<10)?"{$page_count}":$page_count).'</a></li>' : null;

    $previous_page = $cur_page > 1 ? '<li class="previous-off"> <a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'">« Previous</a>' : null .'</li>' ;
    $next_page = $cur_page < $page_count ? ' <a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"> Next » </a>' : null;

    // Display pages that are in range
    $pages = array();
    for ($x=$current_range[0];$x <= $current_range[1]; ++$x){
        if($x==$cur_page)
            $pages[] = '<li class="active">'.$x.'</li>';
        else
            $pages[] = '<li> <a title="Page '.($x).'" href="'.$v_url.'page'.$x.$end.'">'.$x.'</a></li>';
    }

    if ($page_count > 1)
        return '<ul id="pagination"> '.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul>';
    else
        return "";
}

function pagination($page_count, $cur_page, $link, $limit=4, $end="", $split=""){
    $v_url = $link;
    if(substr($v_url,strlen($v_url),1)!='/') $v_url.='/';
    //$v_url.='page';
    $current_range = array(($cur_page-2 < 1 ? 1 : $cur_page-2), ($cur_page+2 > $page_count ? $page_count : $cur_page+2));

    // First and Last pages
    $first_page = $cur_page > 3 ? '<li><a title="Page 1" href="'.$v_url.'page1'.$end.'">1</a></li>'.($cur_page < $limit+1 ? "{$split} " : '<li >...</li>') : null; //class="current"
    $last_page = $cur_page < $page_count-2 ? ($cur_page > $page_count-$limit ? "{$split} " : ' <li >...</li> ').//class="current"
        '<li><a title="Page '.$page_count.'" href="'.$v_url."page{$page_count}".$end.'">'.(($page_count<10)?"{$page_count}":$page_count).'</a></li>' : null;
//
    // Previous and next page
    //$previous_page = $cur_page > 1 ? '<li><a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'"><span   class="icon_prev" ></span></a></li>' : null;
    //$next_page = $cur_page < $page_count ? '<li><a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"><span   class="icon_next" ></span></a></li>' : null;
    $previous_page = $cur_page > 1 ? '<li><a title="Page '.($cur_page-1).'" href="'.$v_url.'page'.($cur_page-1).$end.'"><span class="icon_prev"></span></a></li>' : null;
    $next_page = $cur_page < $page_count ? '<li><a title="Page '.($cur_page+1).'" href="'.$v_url.'page'.($cur_page+1).$end.'"><span class="icon_next"></span></a></li>' : null;

    // Display pages that are in range
    for ($x=$current_range[0];$x <= $current_range[1]; ++$x){
        if($x==$cur_page)
            //$pages[] = '<li class="current">'.$x.'</li>';
            $pages[] = '<li class="active"><a href="'.$v_url.'page'.$x.$end.'"><strong>'.$x.'</strong></a></li>';
        else
            $pages[] = '<li><a title="Page '.($x).'" href="'.$v_url.'page'.$x.$end.'">'.$x.'</a></li>';
    }

    if ($page_count > 1)
        //return '<div class="paging"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
    return '<div class="pagination pagination-centered"><ul>'.$previous_page.$first_page.(is_array($pages)?implode("{$split} ", $pages):"").$last_page.$next_page.'</ul></div>';
    else
        return "";
}

function cut_str($p_str,$p_start,$p_end){

    if ($p_start!=""){
        $v_start_post = strpos($p_str,$p_start);
        if ($v_start_post===false) return "";
        $p_str = substr($p_str,$v_start_post+strlen($p_start));
        if ($p_end=="") return $p_str;
        $v_end_post = strpos($p_str,$p_end);
        if ($v_end_post===false) return "";
        $p_str = substr($p_str,0,$v_end_post);
        return $p_str;
    }else{
        if ($p_end!=""){
            $v_end_post = strpos($p_str,$p_end);
            if ($v_end_post===false) return "";
            $p_str = substr($p_str,0,$v_end_post);
            return $p_str;
        }else{
            return "";
        }
    }
}
function cutString ($p_string, $p_separate){
    if(strlen(trim($p_string))==0){
        return false;
    }
    elseif(strpos($p_string, $p_separate)===false){
        return $p_string;
    }
    else{
        $v_separateLen = strlen($p_separate);
        $v_separatePos = strpos($p_string, $p_separate);

        if($v_separatePos === false || $v_separateLen ==0){
            $part[0] = $p_string;
            $part[1] = '';
        }
        else{
            $part[0] = substr($p_string, 0, $v_separatePos);
            $part[1] = substr($p_string, $v_separatePos + $v_separateLen);
        }
        return $part;
    }
}

function get_current_page_url() {
    $v_page_url = 'http';
    if (isset($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] == "on")) {$v_page_url .= "s";}
    $v_page_url .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $v_page_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $v_page_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $v_page_url;
}

function rewriteUrl(){
    $self = $_SERVER['PHP_SELF'];
    $stringParams = substr($self,strpos($self,'.php')+5);
    $arrayParams = explode('/', $stringParams);
    foreach ($arrayParams as $param){
        $aItem = cutString($param, '-');
        $_GET[$aItem[0]] = $aItem[1];
    }
}

function add_class($p_class_name,$p_class_file="")
{
    if($p_class_file=="") $v_tmp_class_name = $p_class_name .'.php';
    else $v_tmp_class_name = $p_class_file;
    if(!class_exists($p_class_name)){
        if(file_exists('classes/'.$v_tmp_class_name))
            require 'classes/'.$v_tmp_class_name;
        else
        {
            die('Can not require class '. $p_class_name. ' check dir (' .'classes/'.$v_tmp_class_name  .')' );

        }
    }
}
function highlight_text($v_highlight_text , $_text,$v_color_code="#FFFF00"){
    $v_str = str_replace($v_highlight_text,'<span class="highlight" >'. $v_highlight_text .'</span>' ,$_text);
    return $v_str;
}

function create_thumb($p_file_path, $p_thumb_path, $p_pos_tfix, $p_new_width, $p_new_height, $p_old_width, $p_old_height, $p_quality=75)
{

    $gd_formats	= array('jpg','jpeg','png','gif');//web formats
    $file_name	= pathinfo($p_file_path);
    if(empty($p_format)) $p_format = $file_name['extension'];

    if(!in_array(strtolower($file_name['extension']), $gd_formats))
    {
        return false;
    }

    $thumb_name	= $p_pos_tfix.'_'. $file_name['filename'].'.'.$p_format;


    // Get new dimensions
    $newW	= $p_new_width;
    $newH	= $p_new_height;

    // Resample
    $thumb = imagecreatetruecolor($newW, $newH);
    $image = imagecreatefromstring(file_get_contents($p_file_path));

    imagecopyresampled($thumb, $image, 0, 0, 0, 0, $newW, $newH, $p_old_width, $p_old_height);

    // Output
    switch (strtolower($p_format)) {
        case 'png':
            imagepng($thumb, $p_thumb_path.$thumb_name, 9);
            break;

        case 'gif':
            imagegif($thumb, $p_thumb_path.$thumb_name);
            break;

        default:
            imagejpeg($thumb, $p_thumb_path.$thumb_name, $p_quality);
            break;
    }
    imagedestroy($image);
    imagedestroy($thumb);
}

function pad_left($p_str, $p_len = 0, $p_char=' '){
    $v_str = trim($p_str);
    if($p_len>0){
        $v_count = $p_len - strlen($v_str);
        if($v_count > 0){
            for($i=0;$i<$v_count;$i++){
                $v_str = $p_char.$v_str;
            }
        }
    }
    return $v_str;
}

function record_sort($arr_array, $p_field, $p_reverse=false){
    $arr_hash = array();
    foreach($arr_array as $arr){
        $arr_hash[$arr[$p_field]] = $arr;
    }
    ($p_reverse)? krsort($arr_hash) : ksort($arr_hash);
    $arr_return = array();
    foreach($arr_hash as $arr){
        $arr_return []= $arr;
    }

    return $arr_return;
}

function write($p_str = '', $p_write='w', $p_file = 'xem.txt', $p_dir = DESIGN_TEMP_DIR){
    $v_file = $p_dir.DIRECTORY_SEPARATOR.$p_file;
    $fp = fopen($v_file, $p_write);
    if($fp){
        fwrite($fp, $p_str, strlen($p_str));
        fclose($fp);
    }
}

if (!function_exists('boolval')) {
        function boolval($val) {
                return (bool) $val;
        }
}


?>