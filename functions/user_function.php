<?php

function create_customer(cls_tb_design_key $cls_key, $p_api_key){
    global $_SESSION;
    $v_message = '';
    $v_success = 0;
    $v_key = '';
	$v_tmp_key = '';
    if(isset($_SESSION['ss_customer'])){
        $v_json_customer = $_SESSION['ss_customer'];
        $arr_json_customer = json_decode($v_json_customer, true);
		if(isset($arr_json_customer['key_api'])) $v_tmp_key = $arr_json_customer['key_api'];
		if($v_tmp_key!=$p_api_key) $v_tmp_key = '';
	}
	if($v_tmp_key==''){
		$v_now = time();
		$arr_where_clause = array('key_value'=>$p_api_key);
		$v_row = $cls_key->select_one($arr_where_clause);
		if($v_row==1){
			$v_key_id = $cls_key->get_key_id();
			$v_key_name = $cls_key->get_key_name();
			$v_website = $cls_key->get_customer_site();
			$v_forever = $cls_key->get_customer_forever();
			$v_status = $cls_key->get_customer_status();
			$v_start = $cls_key->get_started_time();
			if($v_status==1){
				$v_success = 1;
				$v_message = 'Account is locked!';
			}else{
				if($v_forever==0){
					if($v_start>$v_now){
						$v_success = 2;
						$v_message = 'Started time is far';
					}else if($v_now>$v_now){
						$v_success = 3;
						$v_message = 'Your time is elapsed';
					}else{
						$v_key = $p_api_key;
					}
				}else{
					$v_key = $p_api_key;
				}
			}
		}else{
			$v_success = 4;
			$v_message = 'Account is missed!';
		}
    }else{
		$v_key_name = isset($arr_json_customer['key_name'])?$arr_json_customer['key_name']:'';
		$v_key_id = isset($arr_json_customer['key_id'])?$arr_json_customer['key_id']:0;
	}
    $arr_save = array(
        'key_id'=>isset($v_key_id)?$v_key_id:0
        ,'key_name'=>isset($v_key_name)?$v_key_name:''
        ,'key_api'=>$v_key
    );

    $_SESSION['ss_customer'] = json_encode($arr_save);
    return array('code'=>$v_success, 'error'=>$v_message);
}

/**
 *	function create temporary or resume current function
 *	@return array
 */
function create_user(){
    global $_SESSION;
    $v_result = isset($_SESSION['ss_user']);
    if($v_result){
        $arr_user = unserialize($_SESSION['ss_user']);
    }
    if(!isset($arr_user) || !is_array($arr_user)){
        $arr_user = array(
            'user_id' =>0
            ,'user_name'=>''
            ,'user_sex'=>0
            ,'user_email'=>''
            ,'user_login'=>0
            ,'user_type'=>0
            ,'user_status'=>0
            ,'mongo_id'=>''
            ,'contact_id'=>0
            ,'company_id'=>0
            ,'contact_name'=>''
            ,'company_name'=>''
            ,'system'=>0
            ,'location_default'=>0
            ,'user_rule'=>array()
            ,'user_ip'=>get_real_ip_address()
        );
    }
    $_SESSION['ss_user'] = serialize($arr_user);

    return $arr_user;
}

function get_unserialize_user($p_filter){
    $arr = unserialize($_SESSION['ss_user']);
    if(!isset($arr[$p_filter])) return "";
    return $arr[$p_filter];
}

function is_admin_by_user($p_user_name){
    $arr_admin = array('hthai');
    return in_array($p_user_name, $arr_admin);
}

function is_admin(){
    global $arr_user;
    return ($arr_user['user_type']<=3 && $arr_user['company_id']==10000);
}

function is_administrator(){
    global $arr_user;
    return ($arr_user['user_type']==0 && $arr_user['company_id']==10000);
}

function is_super_admin($arr_excluded){
    global $arr_user;
    return ($arr_user['user_type']==0 &&  in_array($arr_user['user_name'], $arr_excluded));
}

function is_view_template($p_template_right = false){
    return is_admin() || $p_template_right;
}

function check_permission($p_permission, $p_user_rule,$p_title=""){
    if(is_admin()) return true;
    else{
        if($p_user_rule=='') return false;
        if(strpos($p_permission,$p_user_rule)!==false) return true;
    }
    return false;
}

function list_excluded_users(cls_settings $cls_settings){
    global $_SESSION;
    if(!isset($_SESSION['ss_excluded_user'])){
        $v_excluded_users = $cls_settings->get_option_name_by_key('website_attribute', 'excluded_user');
        $_SESSION['ss_excluded_user'] = $v_excluded_users;
    }else{
        $v_excluded_users = $_SESSION['ss_excluded_user'];
    }
    return explode(',', $v_excluded_users);
}
?>